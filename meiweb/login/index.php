<?PHP	session_start();
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	
	
if(isset($_GET["entrar"]) && $_GET["entrar"]==1)
{

	if(isset($_SESSION['errorsistema']))
	{	
		$mensaje=mostrarError($_SESSION['errorsistema']);	
		$error=$_SESSION['errorsistema'];	
	} else { $error = 0; $mensaje=""; }


?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link rel="stylesheet" href="css/style.css">
<title>MEIWEB</title>
	<script Language='JavaScript'>
            <!-- Esconde esto de navegadores antiguos
              if (window != window.top)
                 top.location.href = location.href;
            // --> 
			
			function borrarFormulario()
			{
				document.frm_registro.reset();			
				document.frm_registro.txt_idUsuario.value="";
			}
			
			function display_error_ip()
			{
				alert("Ha iniciado sesion en un equipo diferente");
			}

			function mostrarPassword(){
		//Accedo a los elementos del formulario mediante el DOM
		var chkbox = document.getElementById("mostrar"); 
		var contenido = document.getElementById("txt_clave");
		var atributo = contenido.getAttribute("type");
				
		//Pregunto si el checkbox esta marcado
		if(chkbox.checked){
			contenido.setAttribute("type","text"); //Si el checkbox esta marcado, el atributo type vale password
		}else{
			contenido.setAttribute("type","password"); //Si el checkbox esta sin marcar, el atributo type vale text
		}
	}
	function mostrarUsu(){
		//Accedo a los elementos del formulario mediante el DOM
		var chkbox = document.getElementById("mostrar1"); 
		var contenido = document.getElementById("txt_idUsuario");
		var atributo = contenido.getAttribute("type");
				
		//Pregunto si el checkbox esta marcado
		if(chkbox.checked){
			contenido.setAttribute("type","text"); //Si el checkbox esta marcado, el atributo type vale password
		}else{
			contenido.setAttribute("type","password"); //Si el checkbox esta sin marcar, el atributo type vale text
		}
	}
			
	</script> 
	
    <link rel="stylesheet" type="text/css" href="tecvir/keyboard.css" />
 
    <script type="text/javascript" src="tecvir/keyboard.js" charset="UTF-8"></script> 
 <script type="text/javascript" src="tecvir/kb.js" charset="UTF-8"></script> 
</head>

<body>

<div class="login-page">
	
  <div class="form">
  	<img src="../portal/img/logo-meiweb.png" style="width:120px; height: 70px;"/>

	<br><br>	
	<form class="login-form" name="frm_registro" method="post" action="loginRegistro.php">
      <input type="password" tabindex="1" placeholder="Usuario" class="link keyboardInput" lang="es" id="txt_idUsuario" name="txt_idUsuario" value="" title="Su login registrado"/><br><br>
      <small>Mostrar Login</small>
       <input type="checkbox" id="mostrar1" name="mostrar" onClick="javascript: mostrarUsu()"/><br><br>
      <input type="password" tabindex="2" placeholder="Contraseña" class="link keyboardInput" lang="es" id="txt_clave" name="txt_clave" title="Contraseña registrada"/><br><br>
       <small>Mostrar Contraseña</small>
       <input type="checkbox" id="mostrar" name="mostrar" onClick="javascript: mostrarPassword()"/><br><br>
      <input type="submit" name="sub_registro" value="Ingresar" >
      <br><br>
      <input type="button" name="btn_reset" id="btn_reset" value="Borrar" onClick="javascript: borrarFormulario();">
      <p class="message"><?PHP echo $mensaje; ?></p>
       <?PHP
			  	if($error == 3)
				{
			  ?>
			  	 <a href="../login/desbloquearUsuario.php">Desbloquear Usuario</a>
			  <?PHP
			  }
			  ?>
			  <?PHP
		  if($error != 3)
		  {
		  ?>
      <p class="message">Olvidó la contraseña? <a href="../login/recordarClave.php" title="Recordar Contraseña">Recordar</a></p>
      <?PHP
			}
		?>
    </form>



 </div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>



<?PHP
	if($error == 5)
	{
?>
	<script language="javascript">
		alert("Contacte con el Administrador, el usuario se encuentra desactivado o no se encuentra matriculado en ningún curso.");
	</script>
<?PHP
	}
	else if($error == 3)
	{
	
	?>
	<script language="javascript">
			  alert("Demasiados intentos de ingresos fallidos a MeiWeb. Su cuenta ha sido bloqueada por seguridad.");
	 </script>
	 <?PHP
	}
	else if($error == 6)
	{
	
	?>
	<script language="javascript">
			  alert("Se ha desbloqueado satisfactoriamente el usuario. Puede ingresar normalmente a MeiWeb.");
	 </script>
	 <?PHP
	}
	else if($error == 22)
	{
	
	?>
	<script language="javascript">
			  alert("Sus datos fueron enviados a su correo");
	 </script>
	 <?PHP
	}
?>
</body>
</html>
<?PHP
	if(isset($_GET['error_ip']))
	{
	?>
	<script>
    	display_error_ip();
    </script>
	<?PHP
	}
}
else
{
	redireccionar('../portal/');
}
?>




