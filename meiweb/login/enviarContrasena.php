<?PHP	session_start();
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  
	$baseDatos=new BD();
	
	if (isset($_SESSION['errorsistema'])) {
		$error=$_SESSION['errorsistema'];
	} else {
		$error=0;
	}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link rel="stylesheet" href="css/style.css">
<title>MEIWEB</title>
<script src="jquery.js" type="text/javascript"></script>
<script language="javascript">

		
	function enviarCancelar()
	{
		location.replace("../login/index.php?entrar=1");	
	}
</script>
<script type="text/javascript">

	
	$(document).ready(function(){
		
	
	  	$('#correoE').blur(function(){
	  		var correo=$(this).val();
	  		
	  			 //$(this).css("background","white");
	  			       $('#correo1').css('background','white');
	  			       
	  			       $.ajax({
                           type:"GET",
                           data:"Correo="+correo,
                           url:"peticionPregunta.php",
                           dataType: "json",
                           success: function(data){
                              if(data.Respuesta!="Nulo"){
                                   	$("#pregunta").val(''+data.Respuesta+'');
                                   	$("#buttone").attr("disabled",false);
                               }else{
                               	alert("El usuario con el Correo "+ correo+ " No esta registrado ");
                                    $("#buttone").attr("disabled",true);
								}
                           }
                       });
	  			         
	  		
	  	});
	  		  	
	});
</script>


</head>
<body>
<div class="login-page">
  <div class="form">
	<img src="../portal/imagenes/logo.png" style="width:180px; height: 40px;"/>
	<img src="../portal/imagenes/UIScom.png" style="width: 80px; height: 40px;"/>
	<br><br>	
	
      <p class="message">Si desea que el sistema le envíe sus datos de la nueva contraseña, por favor ingrese su dirección de correo con el que se registro en el sistema</p>
      <br>
      <form class="login-form" action="generarDatos.php" method="post" name="frm_generar" id="frm_generar">
      <input type="text" id="correoE" name="txt_correo" placeholder="Correo Electrónico" />
      <input type="text" id="pregunta" name="txt_pregunta" disabled="true" placeholder="Pregunta Secreta"/>
      <input type="password" name="txt_respuesta" placeholder="Respuesta Secreta"/>
      <input type="submit" name="button" id="buttone" value="Enviar" disabled="true"/>
      <input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onclick="javascript: enviarCancelar();" />
                        
            </form>
       <?PHP
	 if($error == 1)
	{
	
	?>
	<script language="javascript">
			  alert("No existe usuario con este Correo ");
	 </script>
     <?PHP }?>
     <?PHP
	 if($error == 2)
	{
			$_SESSION['errorsistema']=0;
	?>
	<script language="javascript">
			  alert("Sus datos fueron enviados a su correo  ");
	 </script>
     <?PHP }?>
     <?PHP
	 if($error == 3)
	{
	
	?>
	<script language="javascript">
			  alert("No coincide la respuesta");
	 </script>
     <?PHP }?>
      

</div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>    
</body>
</html>
