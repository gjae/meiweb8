<?PHP
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');
	
	session_start();
	$baseDatos=new BD();
	
	if(file_exists("../configuracion/archivos/admin.conf"))
	{
		$cadenaAdmin=file_get_contents("../configuracion/archivos/admin.conf");
		list($administrador,$pregunta,$respuesta,$clave)=explode(";",$cadenaAdmin);
		$preguntaSecreta=$baseDatos->DecodificarClaveBD($pregunta);
	}
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link rel="stylesheet" href="css/style.css">
<title>MEIWEB</title>
<script language="javascript">

	function enviarRegistro()
	{
		if(validarDatos())
		{
			document.frm_recordarAdmin.submit();
		}
	}
	
	function validarDatos()
	{
		if(document.frm_recordarAdmin.txt_respuesta.value ==false || document.frm_recordarAdmin.txt_admin.value ==false || document.frm_recordarAdmin.txt_nuevaClave.value ==false || document.frm_recordarAdmin.txt_confirmacion.value ==false) 
		{
			alert("Es necesario que llene completamente la información solicitada");
			return false;			
		}
		else
		{
			if(document.frm_recordarAdmin.txt_nuevaClave.value == document.frm_recordarAdmin.txt_confirmacion.value)
				return true;
			else
			{
			alert("La contraseña y su confirmación no son y guales");
			return false;			
			}
			
		}
	}
	
	function enviarCancelar()
	{
		location.replace("../login/");	
	}


</script>
<style>
	.login-page {
  width: 360px;
  padding: 4% 0 0;
  margin: auto;
}
</style>


</head>

<body>
<div class="login-page">
  <div class="form">	
  	<img src="../portal/imagenes/logo.png" style="width:180px; height: 40px;"/>
	<img src="../portal/imagenes/UIScom.png" style="width: 80px; height: 40px;"/>
	<br><br>	
	<form action="loginRecordarAdmin.php" method="post" name="frm_recordarAdmin" id="frm_recordarAdmin">
	<input type="text" placeholder="Administrador" name="txt_admin">
    <p class="message">Pregunta Secreta: <?PHP echo $preguntaSecreta; ?></p><br>
    <input type="hidden" name="hid_pregunta" value="<?PHP echo $preguntaSecreta?>">
    <input placeholder="Respuesta secreta" name="txt_respuesta" type="text">
    <input placeholder="Nueva Clave" name="txt_nuevaClave" type="password">
    <input placeholder="Confirmar contraseña" name="txt_confirmacion" type="password">
        
        <?PHP
				if($_SESSION['errorAdminClave']=="0x001")
				{
				?>
        <?PHP
				}
				?>
     <input name="btn_ingresar" type="button" id="btn_ingresar" value="Cambiar" onClick="javascript: enviarRegistro();">
     <input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">                    
     <input name="btn_reset" type="button" id="btn_reset" value="Cancelar" onClick="javascript: enviarCancelar();"></td>
               
 </div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>
	
</body>
</html>
