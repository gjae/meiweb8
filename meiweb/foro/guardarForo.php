<?PHP
	include_once ('../librerias/validar.lib.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  
	$baseDatos=new BD();
	
	if(comprobarSession())
	{ 
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==7)
		{
			list($idmateria,$materia)=explode('*',$_POST['hid_materia']);
			list($codigoGrupo,$grupo)=explode('*',$_POST['hid_grupo']);

			if(!comprobarEditor($_POST['edt_foro']))
			{
				if($_GET['calificable']==1)
				{
					redireccionar("../foro/crearForoCalificable.php?error=0x001&infoMateria=".$_POST['hid_materia']."&infoGrupo=".$_POST['hid_grupo']."&idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
				}
				else
				{
					redireccionar("../foro/crearForo.php?error=0x001&estado=2&infoMateria=".$_POST['hid_materia']."&idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
				}
			}
			else
			{
				registrarBitacora(4,14,false);

                	// Captura de tiempo de activacion
					$hA=$_POST['cbo_horaActivacion'];
                	$mA=$_POST['cbo_minutoActivacion'];
                 	$hC=$_POST['cbo_horaCaducidad'];
                 	$mC=$_POST['cbo_minutoCaducidad'];

                       $activacion=$hA.":".$mA.":00";
                       $caducidad=$hC.":".$mC.":00";

					list($a,$m,$d)=explode('-',date("Y-n-d"));
						$fechaActual= $a.$m.$d;
					list($a,$m,$d)=explode('-',$_POST['hid_fecha']);
						$fechaForo= $a.$m.$d;

					if($fechaForo>$fechaActual)
						$estado='0';
					else
						$estado='1';
					
          			$cadenaBuscador=strip_tags($_POST['edt_foro']);
          			$cadenaBuscador=limpiarCadenaBusqueda($cadenaBuscador);        
      
					if($_GET['calificable']==1)
					{
                    	if($_POST['chk_grupal']!=1)
                        {
                        	$grupal=0;
                        }
                        else
                        {
                        	$grupal=1;
                        }
						$sql="INSERT INTO mei_foro (idforo,idmateria,idusuario,horacreacion,horaactivacion,horacaducidad,fechacreacion,
							fechaactivacion,fechacaducidad,mensaje,estado,calificable,grupal,idevaluacion,valor,buscador,idgrupo,actopcion) 
							VALUES('', '".$idmateria."', '".$_SESSION['idusuario']."','".date("H:i:s")."','".$activacion."','".$caducidad."',
							'".date("Y-n-d")."','".$_POST['txt_fecha']."','".$_POST['txt_fechaC']."', '".base64_encode($_POST['edt_foro'])."',
							 '".$estado."', '".$_GET['calificable']."','".$grupal."','".$_POST['cbo_nota']."', '".$_POST["cbo_valor"]."',
							 '".$cadenaBuscador."', '".$_GET['codigoGrupo']."','".$_POST['chk_opciones']."')";
						$baseDatos->ConsultarBD($sql);
					}
					else
					{
						$sql="INSERT INTO mei_foro (idforo,idmateria,idusuario,horacreacion,horaactivacion,horacaducidad,fechacreacion,
							fechaactivacion,fechacaducidad,mensaje,estado,calificable,buscador,idgrupo,actopcion)
							VALUES('', '".$idmateria."', '".$_SESSION['idusuario']."','".date("H:i:s")."','".$activacion."','".$caducidad."',
							'".date("Y-n-d")."','".$_POST['txt_fecha']."','".$_POST['txt_fechaC']."', '".base64_encode($_POST['edt_foro'])."',
							 '".$estado."', '".$_GET['calificable']."','".$cadenaBuscador."','".$_GET['codigoGrupo']."','".$_POST['chk_opciones']."')";
						$baseDatos->ConsultarBD($sql);
					}
						$sql="SELECT MAX(mei_foro.idforo) FROM mei_foro";
						$consulta=$baseDatos->ConsultarBD($sql);
						list($maxId)=mysql_fetch_array($consulta);
									
						$archivo= $_FILES['fil_archivo']['name']; 
						$nombreArchivo="id".$maxId.$archivo;
						
						if (cargarArchivo('fil_archivo',corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosForo/'))
							{
								$sql="INSERT INTO mei_relforarc(idforo,archivo,localizacion) VALUES('".$maxId."','".corregirCaracteresURL($archivo)."','".corregirCaracteresURL($nombreArchivo)."') ";
								$resultado=$baseDatos->ConsultarBD($sql);						
							}
						else
							{
								if(!empty($archivo))
								{
									$noAdjuntados.=$archivo;
								}
							}
					redireccionar("../foro/index.php?idmateria=".$idmateria);
					}
			}
			else
			{
				redireccionar('../login/');
			}
		}
		else
			redireccionar('../login/');
?>