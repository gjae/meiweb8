<?PHP
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
if (comprobarSession())
{
	registrarBitacora(4,13,false);
	//Actualizar el estado de los foros
	$sql1="SELECT mei_foro.idforo, mei_foro.fechaactivacion, mei_foro.fechacaducidad,
               mei_foro.horaactivacion, mei_foro.horacaducidad, mei_foro.concluir FROM mei_foro";
	$resultado1=$baseDatos->ConsultarBD($sql1);

	while (list($codigoForo,$fechaActivacion, $fechaCaducidad,$horaActivacion, $horaCaducidad, $concluir)=mysql_fetch_array($resultado1))
	{
		list($a,$m,$d)=explode('-',date("Y-m-d"));
			$fechaActual= $a.$m.$d;
		list($a,$m,$d)=explode('-',$fechaActivacion);
			$fechaForo= $a.$m.$d;
		list($a,$m,$d)=explode('-',$fechaCaducidad);
			$fechaCadu= $a.$m.$d;
		list($h,$m,$s)=explode(':',date("H:i:s"));
			$horaActual= $h.$m.$s;
		list($h,$m,$s)=explode(':',$horaActivacion);
			$horaForo= $h.$m.$s;
		list($h,$m,$s)=explode(':',$horaCaducidad);
			$horaCadu= $h.$m.$s;

		if($fechaForo>$fechaActual)
				$estado='0';
		else if($fechaForo==$fechaActual)
		    {
                if($horaForo>$horaActual)
					$estado='0';
                else if ($horaForo<$horaActual & $concluir==0)
					$estado='1';
            }

		else if($fechaActual>$fechaForo & $concluir==0)
				$estado='1';

        	if($fechaActual>$fechaCadu)
				$estado='0';

                else if($fechaActual==$fechaCadu)
				{
                    if($horaActual>$horaCadu)
						$estado='0';
                    else if ($horaForo<$horaActual & $concluir==0)
						$estado='1';
                }

		$sql2="UPDATE mei_foro SET mei_foro.estado='$estado' WHERE mei_foro.idforo=".$codigoForo;
		$resultado2=$baseDatos->ConsultarBD($sql2);
	}
	//fin de actualizacion
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
	<script language="javascript">
		function enviar()
		{
			document.frm_verForo.submit();
		}
		function eliminar(codigoMensaje,idmateria)
		{
			if(confirm("¿Está seguro de eliminar este Foro?"))
			{
			location.replace("eliminarForo.php?codigoMensaje="+codigoMensaje+"&idmateria="+idmateria);
			}
		}
	</script>
</head>
<body>
	<table class="tablaPrincipal">
	   	<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><?PHP menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
		    <td>
				<form name="frm_buscarForo" method="GET" action="<?PHP echo  $_SERVER["PHP_SELF"]; ?>">
				<table class="tablaGeneral" >
                  <tr class="trSubTitulo">
                    <td width="15%"><b>Buscar Foro:</b></td>
                    <td width="85%" ><input type="text" name="txt_buscarForo">
                    <input type="submit" name="sub_buscar" value="Buscar"></td>
                  </tr>
                </table>
				</form>
				<?PHP
				$nommateria=$_GET['materia'];

//INICIO BUSQUEDA
				if(isset($_GET['txt_buscarForo']))
				{
					$cadena=explode(' ',strtoupper($_GET['txt_buscarForo']));
					$contPalabras=0;
					foreach($cadena as $palabra)
					{
					 $palabra=limpiarCadenaBusqueda($palabra);
						if($contPalabras==0)
							$busqueda="mei_foro.buscador LIKE '%".$palabra."%'";
						else
							$busqueda.=" AND mei_foro.buscador LIKE '%".$palabra."%'";
						$contPalabras++;
					}
					if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
					{
						$sql1="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
                           mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
                           mei_foro.calificable,mei_foro.grupal,mei_foro.idmateria FROM mei_foro WHERE mei_foro.calificable=0 AND ".$busqueda;
						
						if ($_SESSION['idtipousuario']==2)
						{
							$sql2="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
                          	 mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
                          	 mei_foro.calificable,mei_foro.grupal,mei_foro.idmateria FROM mei_foro WHERE mei_foro.calificable=1  AND mei_foro.idgrupo IN
							(SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario ='".$_SESSION['idusuario']."')
							AND ".$busqueda;
						}
						else if ($_SESSION['idtipousuario']==5)
						{
							$sql2="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
                          	 mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
                          	 mei_foro.calificable,mei_foro.grupal,mei_foro.idmateria FROM mei_foro WHERE mei_foro.calificable=1  AND mei_foro.idgrupo IN
							(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario ='".$_SESSION['idusuario']."')
							AND ".$busqueda;
						}
					}
					else
					{
						$sql1="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
                            mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
                           mei_foro.calificable,mei_foro.grupal,mei_foro.idmateria FROM mei_foro WHERE mei_foro.calificable=0
						AND mei_foro.estado=1 AND ".$busqueda;
						
						if ($_SESSION['idtipousuario']==3 OR $_SESSION['idtipousuario']==7)
						{
							$sql2="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
	                           mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
	                           mei_foro.calificable,mei_foro.grupal,mei_foro.idmateria FROM mei_foro WHERE mei_foro.calificable=1
								AND mei_foro.estado=1  AND mei_foro.idgrupo IN
								(SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario ='".$_SESSION['idusuario']."')
								AND ".$busqueda;
						}
						else if ($_SESSION['idtipousuario']==6)
						{
							$sql2="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
	                           mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
	                           mei_foro.calificable,mei_foro.grupal,mei_foro.idmateria FROM mei_foro WHERE mei_foro.calificable=1
								AND mei_foro.estado=1  AND mei_foro.idgrupo IN
								(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario ='".$_SESSION['idusuario']."')
								AND ".$busqueda;
						}
					}

						$consulta1=$baseDatos->ConsultarBD($sql1);
						$consulta2=$baseDatos->ConsultarBD($sql2);
						$numeroForos=mysql_num_rows($consulta1)+mysql_num_rows($consulta2);
						$contForos=0;
						unset($listaForos);
						while (list($codigoMensaje,$mensaje,$estado,$codigoAutor,$fecha,$fechaA,$fechaC,$horaA,$horaC,$calificable,$grupal,$idBMateria) = mysql_fetch_array($consulta1))
						{
							$listaForos[$contForos]=$codigoMensaje."[$$$]".$mensaje."[$$$]".$estado."[$$$]".$codigoAutor."[$$$]".$fecha."[$$$]".$fechaA."[$$$]".$fechaC."[$$$]".$horaA."[$$$]".$horaC."[$$$]".$calificable."[$$$]".$grupal."[$$$]".$idBMateria;

							$contForos++;
						}
						while (list($codigoMensaje,$mensaje,$estado,$codigoAutor,$fecha,$fechaA,$fechaC,$horaA,$horaC,$calificable,$grupal,$idBMateria) = mysql_fetch_array($consulta2))
						{
							$listaForos[$contForos]=$codigoMensaje."[$$$]".$mensaje."[$$$]".$estado."[$$$]".$codigoAutor."[$$$]".$fecha."[$$$]".$fechaA."[$$$]".$fechaC."[$$$]".$horaA."[$$$]".$horaC."[$$$]".$calificable."[$$$]".$grupal."[$$$]".$idBMateria;

							$contForos++;
						}

					if ($numeroForos==0)
					{
					?>
						<table class="tablaGeneral" >
						<tr class="trAviso">
								<td width="100%" colspan="2"><div align="center"><b>Resultado de la busqueda:</b> Se Encontraron <?PHP echo $numeroForos; ?> Coincidencias con <i>
								  <?PHP echo $_GET['txt_buscarForo']; ?></i></div></td>
						  </tr>
						</table><br>


					<?PHP
					}
					else
					{
					?>
						<table class="tablaGeneral" >
						<tr class="trAviso">
								<td width="100%" colspan="5"><div align="center"><b>Resultado de la busqueda:</b> Se Encontraron <?PHP echo $numeroForos; ?> Coincidencias con <i><?PHP echo $_GET['txt_buscarForo']; ?></i></div></td>
						</tr>
						<tr class="trSubTitulo">
							<td class="trSubTitulo" width="60%"><div align="center">Foro</div></td>
							<td class="trSubTitulo" width="10%"><div align="center">Comentarios</div></td>
							<td class="trSubTitulo" width="10%"><div align="center">Visitas</div></td>
							<?PHP
							if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==7)
							{
							?>
								<td class="trSubTitulo" width="10%"><div align="center">Estado</div></td>
                                <td class="trSubTitulo" width="10%"><div align="center">Calificable</div></td>

							<?PHP
							}
							?>
						</tr>
						<?PHP
						asort($listaForos);
						foreach ($listaForos as $foro)
						{
								list($codigoMensaje,$mensaje,$estado,$codigoAutor,$fecha,$fechaA,$fechaC,$horaA,$horaC,$calificable,$grupal,$idBMateria) = explode("[$$$]",$foro);

							$mensaje=stripslashes(base64_decode($mensaje));
							if($calificable==1)
							{
								if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==3|| $_SESSION['idtipousuario']==7)
								{
									$sql="SELECT mei_grupo.nombre FROM mei_grupo WHERE mei_grupo.idgrupo=
									(SELECT mei_foro.idgrupo FROM mei_foro WHERE mei_foro.idforo=".$codigoMensaje.")";
								}
								else if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
								{
									$sql="SELECT mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo=
									(SELECT mei_foro.idgrupo FROM mei_foro WHERE mei_foro.idforo=".$codigoMensaje.")";
								}
								$consulta=$baseDatos->ConsultarBD($sql);
								list($grupo) = mysql_fetch_array($consulta);
							}
							$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido  FROM mei_usuario WHERE mei_usuario.idusuario='".$codigoAutor."'";
							$autor=$baseDatos->ConsultarBD($sql);
							list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);

							$sql="SELECT mei_foro.numvisitas FROM mei_foro WHERE mei_foro.idforo=".$codigoMensaje;
							$visita=$baseDatos->ConsultarBD($sql);
							list($visitas) = mysql_fetch_array($visita);

							$sql="SELECT COUNT(*) FROM mei_comentarioforo WHERE mei_comentarioforo.idforo='".$codigoMensaje."' GROUP BY mei_comentarioforo.idforo";
							$cont=$baseDatos->ConsultarBD($sql);
							(list($comentarios) = mysql_fetch_array($cont)) ;
							if (empty($comentarios))
								$comentarios=0;

							$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario='".$codigoAutor."'";
							$autor=$baseDatos->ConsultarBD($sql);
							list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
							if($cont%2==0)
							{
								$color="trListaClaro";
							}
							else
							{
								$color="trListaOscuro";
							}

								?>
							<tr class="<?PHP echo $color; ?>">
							<?PHP
							if($estado==1)
							{
								$alt="Ver los comentarios de este Foro \n";
								$alt.="Autor: ".$nombre1." ". $nombre2." ".$apellido1." ".$apellido2."\n";
								$alt.="Creado el ".$fecha."\n";
								$alt.="Fecha Activacion: ".$fechaA." a las ".$horaA."\n";
								$alt.="Fecha Caducidad: ".$fechaC." a las ".$horaC;

								
								if($calificable==1)
								{
								$alt.="\nGrupo: ".$grupo;
								}
							?>
							<td class="<?PHP echo $color; ?>"><a href="verMensajeForo.php?materia=<?PHP echo $nommateria; ?>&idmateria=<?PHP echo  $idBMateria; ?>&codigoMensaje=<?PHP echo  $codigoMensaje; ?>&cbo_ordenar=2" class="link" title="<?PHP echo $alt; ?>"><?PHP echo  $mensaje; ?></a></td>
							<?PHP
							}
							else
							{
								$alt="Foro Inactivo \n";
								$alt.="Autor: ".$nombre1." ". $nombre2." ".$apellido1." ".$apellido2."\n";
								$alt.="Creado el ".$fecha."\n";
								$alt.="Fecha Activacion: ".$fechaA." a las ".$horaA."\n";
								$alt.="Fecha Caducidad: ".$fechaC." a las ".$horaC;
								if($calificable==1)
								{
								$alt.="\nGrupo: ".$grupo;
								}


							?>
								<td class="linkLeido"><div align="left" title="<?PHP echo $alt; ?>">
								  <?PHP echo  $mensaje; ?>
							  </div></td>
							<?PHP
							}


							if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==7)
							{
							?>
								<td class="<?PHP echo $color; ?>"><div align="center"><a href="verMensajeForo.php?materia=<?PHP echo $nommateria; ?>&idmateria=<?PHP echo  $idBMateria; ?>&codigoMensaje=<?PHP echo  $codigoMensaje; ?>&clave=1" class="link" title="Ver y Calificar Comentarios"><?PHP echo $comentarios; ?></a></div></td>
							<?PHP
							}
							else
							{
							?>
								<td class="<?PHP echo $color; ?>"><div align="center"><?PHP echo $comentarios; ?></div></td>
							<?PHP
							}
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><?PHP echo $visitas; ?></div></td>

							<?PHP
							if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==7)
							{
									if($estado==1)
									{
										if($codigoAutor==$_SESSION['idusuario'])
										{

											?>
											<td class="<?PHP echo $color; ?>"><div align="center"><a href="modificarForo.php?codigoMensaje=<?PHP echo  $codigoMensaje; ?>&idmateria=<?PHP echo  $idmateria; ?>" class="link"><img src="imagenes/activo.gif" width="16" height="16" border="0" alt="Cambiar Fecha de Activación"></a></div></td>

											<?PHP
										}
										else
										{
											?>
											<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/activo.gif" width="16" height="16" border="0" alt="Cambiar Fecha de Activación"></div></td>
											<?PHP
										}
									}
									else if($estado==0)
									{
										if($codigoAutor==$_SESSION['idusuario'])
										{
											?>
											<td class="<?PHP echo $color; ?>"><div align="center"><a href="modificarForo.php?codigoMensaje=<?PHP echo  $codigoMensaje; ?>&idmateria=<?PHP echo  $idmateria; ?>" class="link"><img src="imagenes/inactivo.gif" width="16" height="16" border="0" alt="Cambiar Fecha de Activación"></a></div></td>
											<?PHP
										}
										else
										{
											?>
											<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/inactivo.gif" width="16" height="16" border="0" alt="Cambiar Fecha de Activación"></div></td>
											<?PHP
										}
									}
								if ($calificable==1)
								{
                                                                      if($grupal==1)
                                                                      {
									?>
									<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/calificable.gif" width="16" height="16" border="0" alt="Foro Calificable">Grupal</div></td>
									<?PHP
                                                                      }
                                                                      else
                                                                      {
									?>
									<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/calificable.gif" width="16" height="16" border="0" alt="Foro Calificable"></div></td>
									<?PHP
                                                                      }
								}
								else
								{
									?>
									<td class="<?PHP echo $color; ?>"></td>
									<?PHP
								}

							}
						?>
						</tr>
						<?PHP

							$cont++;
						}//FIN DE FOREACH DE FOROS
						?>
					</table>
	    <br>
					<?PHP
					}
				}
//FIN BUSQUEDA
				?>
<!-- INICIO LISTADO DE FOROS-->
				<table class="tablaGeneral" >
                    	<tr class="trTitulo">
                          	<td colspan="4"><img src="imagenes/foro.gif" width="16" height="16" align="texttop"> Foros</td>
                          		<? if (empty($_GET['idmateria'])) {?>
								<td colspan="2" align="right"> <a href="../scripts/homeUsuario.php" class="link"> Volver  </td>
						<?	} else {?>
							<td colspan="2" align="right"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"> Volver  </td>
							<?}?>
                        </tr>
				</table>
<?PHP
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					$sql = "SELECT idvirgrupo FROM mei_relusuvirgru WHERE idusuario='".$_SESSION['idusuario']."'";
				else
					$sql = "SELECT idgrupo FROM mei_relusugru WHERE idusuario='".$_SESSION['idusuario']."'";
				$grupo=$baseDatos->ConsultarBD($sql);
				list($idgrupo)= mysql_fetch_array($grupo);


				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					$sql = "SELECT idvirmateria FROM mei_virgrupo WHERE idvirgrupo=".$idgrupo['idgrupo'];
				else
					$sql = "SELECT idmateria FROM mei_grupo WHERE idgrupo=".$idgrupo['idgrupo'];
				$materia=$baseDatos->ConsultarBD($sql);
				list($idmateria)= mysql_fetch_array($materia);

				if(empty($_GET['idmateria'])) //si esta vacio
				{
					if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
					{
						if ($_SESSION['idtipousuario']==2)
						{
							$sql="SELECT mei_materia.idmateria,mei_materia.nombre FROM mei_materia
							WHERE mei_materia.idmateria IN  (SELECT mei_foro.idmateria FROM mei_foro)";
						}
						else if ($_SESSION['idtipousuario']==5)
						{
							$sql="SELECT mei_virmateria.idvirmateria,mei_virmateria.nombre FROM mei_virmateria
							WHERE mei_virmateria.idvirmateria IN  (SELECT mei_foro.idmateria FROM mei_foro)";
						}
					}
					else if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
					{
						if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==7)
						{
							$sql="SELECT mei_grupo.idmateria, mei_materia.nombre
								FROM mei_relusugru, mei_grupo, mei_materia
								WHERE mei_relusugru.idusuario ='".$_SESSION['idusuario']."'
								AND mei_relusugru.idgrupo = mei_grupo.idgrupo and mei_grupo.idmateria=mei_materia.idmateria  ";
						}
						else if ($_SESSION['idtipousuario']==6)
						{
							$sql="SELECT mei_virgrupo.idvirmateria, mei_virmateria.nombre
								FROM mei_relusuvirgru, mei_virgrupo, mei_virmateria
								WHERE mei_relusuvirgru.idusuario ='".$_SESSION['idusuario']."'
								AND mei_relusuvirgru.idvirgrupo = mei_virgrupo.idvirgrupo and mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria  ";
						}
					}
				}
				else
				{
					if($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
						$sql="SELECT mei_virmateria.idvirmateria,mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria='".$_GET['idmateria']."'";
					else
						$sql="SELECT mei_materia.idmateria,mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria='".$_GET['idmateria']."'";
				}
				$materias=$baseDatos->ConsultarBD($sql);
				$numeroMaterias=mysql_num_rows($materias);
				if(empty($numeroMaterias))
				{
				?>
					<table class="tablaGeneral" >
						<tr class="trAviso">
						   <td colspan="4">Actualmente no existen Foros </td>
		  			  	</tr>
					</table>
				<?PHP
				}
				else
				{
					while (list($idmateria,$nombreMateria) = mysql_fetch_array($materias))
					{
						if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
						{
							/*$sql1="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
                                    mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
                                    mei_foro.calificable,mei_foro.grupal
								FROM mei_foro WHERE mei_foro.calificable=0 AND mei_foro.idmateria='".$idmateria."' ";*/
							if ($_SESSION['idtipousuario']==2)
							{
								$sql2="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
									mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
									mei_foro.calificable,mei_foro.grupal
									FROM mei_foro WHERE mei_foro.idmateria='".$idmateria."'  AND mei_foro.idgrupo IN
									(SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario ='".$_SESSION['idusuario']."')";
							}
							else if ($_SESSION['idtipousuario']==5)
							{
								$sql2="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
									mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
									mei_foro.calificable,mei_foro.grupal
									FROM mei_foro WHERE mei_foro.idmateria='".$idmateria."'  AND mei_foro.idgrupo IN
									(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario ='".$_SESSION['idusuario']."')";
							}
						}
						else if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
						{
							/*$sql1="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,mei_foro.fechacreacion,
									mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
									mei_foro.calificable,mei_foro.grupal
								FROM mei_foro WHERE mei_foro.calificable=0 AND mei_foro.idmateria='".$idmateria[idmateria]."' ";*/
							if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==7)
							{
								$sql2="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,
									mei_foro.fechacreacion,mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
									mei_foro.calificable,mei_foro.grupal FROM mei_foro WHERE mei_foro.idmateria='".$idmateria."'  AND mei_foro.idgrupo IN
									(SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario ='".$_SESSION['idusuario']."')";
							}
							else if ($_SESSION['idtipousuario']==6)
							{
								$sql2="SELECT mei_foro.idforo,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,
									mei_foro.fechacreacion,mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.horaactivacion,mei_foro.horacaducidad,
									mei_foro.calificable,mei_foro.grupal FROM mei_foro WHERE mei_foro.idmateria='".$idmateria."'  AND mei_foro.idgrupo IN
									(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario ='".$_SESSION['idusuario']."')";
							}
						}
						//$consulta1=$baseDatos->ConsultarBD($sql1);
						$consulta2=$baseDatos->ConsultarBD($sql2);
						$numeroForos=/*mysql_num_rows($consulta1)+*/mysql_num_rows($consulta2);
						$contForos=0;
						unset($listaForos);
						/*while (list($codigoMensaje,$mensaje,$estado,$codigoAutor,$fecha,$fechaA,$fechaC,$horaA,$horaC,$calificable,$grupal) = mysql_fetch_array($consulta1))
						{
							$listaForos[$contForos]=$codigoMensaje."[$$$]".stripslashes(base64_decode($mensaje))."[$$$]".$estado."[$$$]".$codigoAutor."[$$$]".$fecha."[$$$]".$fechaA."[$$$]".$fechaC."[$$$]".$horaA."[$$$]".$horaC."[$$$]".$calificable."[$$$]".$grupal;

							$contForos++;
						}*/
						while (list($codigoMensaje,$mensaje,$estado,$codigoAutor,$fecha,$fechaA,$fechaC,$horaA,$horaC,$calificable,$grupal) = mysql_fetch_array($consulta2))
						{
							$listaForos[$contForos]=$codigoMensaje."[$$$]".stripslashes(base64_decode($mensaje))."[$$$]".$estado."[$$$]".$codigoAutor."[$$$]".$fecha."[$$$]".$fechaA."[$$$]".$fechaC."[$$$]".$horaA."[$$$]".$horaC."[$$$]".$calificable."[$$$]".$grupal;

							$contForos++;
						}
						?>
					<table class="tablaGeneral" >
<?PHP
					if(isset($listaForos) && sizeof($listaForos)>0 )
					{
?>
					<tr class="trSubTitulo">
						<td ><div align="left">
                    	<a href="verMensajeForo.php?materia=<?PHP echo $nommateria; ?>&idmateria=<?PHP echo  $idmateria; ?>" class="link" title="Ver todos los Foros de <?PHP echo $nombreMateria; ?>">Foros de 
                      	<?PHP echo  $nombreMateria; ?></a>
                    	</div></td>
						<td colspan="6"><div align="right">Hay
						<?PHP echo $numeroForos; ?>Foros</div></td>
					</tr>
					<tr class="trSubTitulo">
						<td class="trSubTitulo" width="58%"><div align="center">Foro</div></td>
						<td class="trSubTitulo" width="7%"><div align="center">Comentarios</div></td>
						<td class="trSubTitulo" width="7%"><div align="center">Visitas</div></td>
                    <?PHP
						if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
						{
					?>
						<td class="trSubTitulo" width="7%"><div align="center">Estado</div></td>
						<td class="trSubtitulo" width="7%"><div align="center">Estadisticas</div></td>
						<td class="trSubTitulo" width="7%"><div align="center">Eliminar</div></td>
						<td class="trSubTitulo" width="7%"><div align="center">Calificable</div></td>
                    <?PHP
						}
					?>
					</tr>
<?PHP
						$cont=0;
						asort($listaForos);
						foreach ($listaForos as $foro)
						{
							list($codigoMensaje,$mensaje,$estado,$codigoAutor,$fecha,$fechaA,$fechaC,$horaA,$horaC,$calificable,$grupal) = explode("[$$$]",$foro);
	
							if($calificable==1)
							{
								if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
								{
									$sql="SELECT mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo=
									(SELECT mei_foro.idgrupo FROM mei_foro WHERE mei_foro.idforo=".$codigoMensaje.")";
								}
								else
								{
									$sql="SELECT mei_grupo.nombre FROM mei_grupo WHERE mei_grupo.idgrupo=
									(SELECT mei_foro.idgrupo FROM mei_foro WHERE mei_foro.idforo=".$codigoMensaje.")";
								}
								$consulta=$baseDatos->ConsultarBD($sql);
								list($grupo) = mysql_fetch_array($consulta);
							}
							$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido
								FROM mei_usuario WHERE mei_usuario.idusuario='".$codigoAutor."'";
							$autor=$baseDatos->ConsultarBD($sql);
							list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
	
							$sql="SELECT mei_foro.numvisitas FROM mei_foro WHERE mei_foro.idforo=".$codigoMensaje;
							$resultado=$baseDatos->ConsultarBD($sql);
							list($visitas) = mysql_fetch_array($resultado);
	
							$sql="SELECT COUNT(DISTINCT(mei_comentarioforo.comentario))
								FROM mei_comentarioforo WHERE mei_comentarioforo.idforo='".$codigoMensaje."' GROUP BY mei_comentarioforo.idforo";
							$cont=$baseDatos->ConsultarBD($sql);
							(list($comentarios) = mysql_fetch_array($cont)) ;
							if (empty($comentarios))
							$comentarios=0;
	
							$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido
								FROM mei_usuario WHERE mei_usuario.idusuario='".$codigoAutor."'";
							$autor=$baseDatos->ConsultarBD($sql);
							list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
							if($cont%2==0)
							{
								$color="trListaClaro";
							}
							else
							{
								$color="trListaOscuro";
							}
						?>
						<tr class="<?PHP echo $color; ?>">
						<?PHP
							if($estado==1)
							{
								$alt="Ver los comentarios de este Foro \n";
								$alt.="Autor: ".$nombre1." ". $nombre2." ".$apellido1." ".$apellido2." \n";
								$alt.="Creado el ".$fecha." \n";
								$alt.="Fecha Activacion: ".$fechaA." a las ".$horaA." \n";
								$alt.="Fecha Caducidad: ".$fechaC." a las ".$horaC;
								
								if($calificable==1)
								{
								$alt.=" \nGrupo: ".$grupo;
								}
							?>
							<td class="<?PHP echo $color; ?>">
								<a href="verMensajeForo.php?materia=<?PHP echo $nommateria; ?>&idmateria=<?PHP echo  $idmateria; ?>&codigoMensaje=<?PHP echo  $codigoMensaje; ?>&cbo_ordenar=2" class="link" title="<?PHP echo $alt; ?>">
								<?PHP echo $mensaje; ?>
								<div align="center"></div></a>
							</td>
							<?PHP
							}
							else
							{
								$alt="Foro Inactivo \n";
								$alt.="Autor: ".$nombre1." ". $nombre2." ".$apellido1." ".$apellido2." \n";
								$alt.="Creado el ".$fecha." \n";
								$alt.="Fecha Activacion: ".$fechaA." a las ".$horaA." \n";
								$alt.="Fecha Caducidad: ".$fechaC." a las ".$horaC;
								if($calificable==1)
								{
									$alt.=" \nGrupo: ".$grupo;
								}
							?>
							<td class="linkLeido">
								<a href="verMensajeForo.php?materia=<?PHP echo $nommateria; ?>&idmateria=<?PHP echo  $idmateria; ?>&codigoMensaje=<?PHP echo  $codigoMensaje; ?>" class="link" title="<?PHP echo $alt; ?>">
								<strike><?PHP echo $mensaje; ?></strike>
								<div align="left"></div></a>
							</td>
							<?PHP
							}
	
							if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==7)
							{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center">
							<a href="verMensajeForo.php?materia=<?PHP echo $nommateria; ?>&idmateria=<?PHP echo  $idmateria; ?>&codigoMensaje=<?PHP echo  $codigoMensaje; ?>&clave=1" class="link" title="Ver y Calificar Comentarios"><?PHP echo $comentarios; ?></a>
							</div></td>
							<?PHP
							}
							else
							{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center">
							<?PHP echo $comentarios; ?>
							</div></td>
							<?PHP
							}
							?>
							<td class="<?PHP echo $color; ?>"><div align="center">
							<?PHP echo $visitas; ?>
							</div></td>
							<?PHP
							if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==7)
							{
								if($estado==1)
								{
									if($codigoAutor==$_SESSION['idusuario'])
									{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><a href="modificarForo.php?codigoMensaje=<?PHP echo  $codigoMensaje; ?>&idmateria=<?PHP echo  $idmateria; ?>" class="link"><img src="imagenes/activo.gif" width="16" height="16" border="0" alt="Cambiar Fecha de Activación"></a></div></td>
							<?PHP
									}
									else
									{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/activo.gif" width="16" height="16" border="0" alt="Cambiar Fecha de Activación"></div></td>
							<?PHP
									}
								}
								else if($estado==0)
								{
									if($codigoAutor==$_SESSION['idusuario'])
									{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><a href="modificarForo.php?codigoMensaje= <?PHP echo  $codigoMensaje; ?>&idmateria= <?PHP echo  $idmateria; ?>" class="link"><img src="imagenes/inactivo.gif" width="16" height="16" border="0" alt="Cambiar Fecha de Activación"></a></div></td>
							<?PHP
									}
									else
									{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/inactivo.gif" width="16" height="16" border="0" alt="Cambiar Fecha de Activación"></div></td>
							<?PHP
									}
								}
								if($codigoAutor==$_SESSION['idusuario'])
								{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><a href="estadisticaForo.php?codigoMensaje= <?PHP echo  $codigoMensaje; ?>&idmateria= <?PHP echo  $idmateria; ?>&materia=<?PHP echo $nommateria; ?>&grupo=<?PHP echo $grupo; ?>" class="link"><img src="imagenes/estadistica.gif" alt="Estadistica del Foro" width="16" height="16" border="0"></a></div></td>
							<?PHP
								}
								else
								{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/estadistica.gif" alt="Estadisticas del Foro" width="16" height="16" border="0"></div></td>
							<?PHP
								}
								if($codigoAutor==$_SESSION['idusuario'])
								{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><a href="javascript:eliminar('<?PHP echo  $codigoMensaje; ?>','<?PHP echo  $_GET['idmateria']; ?>')" class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Foro" width="16" height="16" border="0"></a></div></td>
							<?PHP
								}
								else
								{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/eliminar.gif" alt="Eliminar Foro" width="16" height="16" border="0"></div></td>
							<?PHP
								}
								if ($calificable==1)
								{
									if($grupal==1)
									{
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/calificable.gif" width="16" height="16" border="0" alt="Foro Calificable">Grupal</div></td>
							<?PHP
									 }
									 else
									 {
							?>
							<td class="<?PHP echo $color; ?>"><div align="center"><img src="imagenes/calificable.gif" width="16" height="16" border="0" alt="Foro Calificable"></div></td>
							<?PHP
									  }
								}
								else
								{
							?>
							<td class="<?PHP echo $color; ?>"></td>
							<?PHP
								}
							}// FIN MENU ADMINISTRADOR
						?>
					 </tr>
						<?PHP
						$cont++;
						}//FIN FOREACH
?>
                </table><br>
<?PHP                
					}
					else
					{
?>						
					<table class="tablaGeneral" >
						<tr class="trAviso">
						   <td colspan="4">Actualmente no existen Foros para <?PHP echo  $nommateria; ?></td>
		  			  	</tr>
					</table>
<?PHP                        
					}
					} // FIN DEL WHILE DE MATERIAS
				}//FIN DEL ELSE
				?>
          	</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><?PHP menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>            
           </tr>
        </table>
	</body>
	</html>
<?PHP
	}
	else
	{
		redireccionar('../correo/');
	}

?>
