<?PHP
	include_once ('../librerias/validar.lib.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once('../baseDatos/BD.class.php');
	$baseDatos=new BD();

	if(comprobarSession())
	{
				list($idmateria,$materia)=explode('*',$_POST['hid_materia']);

			if(!comprobarEditor($_POST['edt_comentario']))
			{
					redireccionar("../foro/crearComentario.php?error=0x001&idmateria=".$idmateria."&codigoMensaje=".$_POST['hid_mensaje']);
			}
			else
			{
                if (isset($_GET['grupo'])) { $grupo = $_GET['grupo']; } else { $grupo = Null; }
				registrarBitacora(4,17,false, $grupo);
				print ($_GET['cadena']);
				//Registrando si el foro esta concluido
				$concluir = $_POST['concluir'];
				if ($concluir == 1)
				{
					$sql2="UPDATE mei_foro SET mei_foro.estado='0',mei_foro.concluir='1' WHERE mei_foro.idforo=".$_POST['hid_mensaje'];
					$resultado2=$baseDatos->ConsultarBD($sql2);
				}
				// Captura si es grupal
				$sql="SELECT mei_foro.grupal, mei_foro.concluir, estado FROM mei_foro WHERE mei_foro.idforo =".$_POST['hid_mensaje'];
		       	$resultado=$baseDatos->ConsultarBD($sql);
				list($grupal,$concluir,$estado) = mysql_fetch_array($resultado);

					//Inicio insertar comentario individual
					if ($grupal==0)
		          	{
						//Maximo numero de comentario
						$sql="SELECT MAX(mei_relcomarc.idcomentario) FROM mei_relcomarc";
						$consulta=$baseDatos->ConsultarBD($sql);
						list($maxId)=mysql_fetch_array($consulta);
						$maxId=$maxId+1;

						$sql1="SELECT MAX(mei_comentarioforo.idcomentario) FROM mei_comentarioforo";
						$consulta1=$baseDatos->ConsultarBD($sql1);
						list($maxId1)=mysql_fetch_array($consulta1);

						if ($maxId==$maxId1) {
							$maxId=$maxId+1;
						}
						else
						{
							$maxId=$maxId1+1;	
						}
						

						$sql="INSERT INTO mei_comentarioforo (idcomentario, idforo, idusuario, comentario, hora,
                        fecha) VALUES ('".$maxId."', '".$_POST['hid_mensaje']."', '".$_SESSION['idusuario']."',
                        '".base64_encode($_POST['edt_comentario'])."', '".date("H:i:s")."','".date("Y-n-j")."') ";
                        $consulta=$baseDatos->ConsultarBD($sql);

						//Guarda el archivo adjunto de los comentarios del foro
						

						$archivo= $_FILES['fil_archivo']['name'];
						$nombreArchivo="id".$maxId.$archivo;

						if (cargarArchivo('fil_archivo',corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosForo/'))
						{
							$sql="INSERT INTO mei_relcomarc(idcomentario,archivo,localizacion) VALUES('".$maxId."','".corregirCaracteresURL($archivo)."','".corregirCaracteresURL($nombreArchivo)."') ";
							$resultado=$baseDatos->ConsultarBD($sql);
						}
						else if(!empty($archivo))
						{
							$noAdjuntados=$archivo;
						}
					}//fin insertar comentario individual
					else if ($grupal==1 && $concluir==0)
                  	{
							$sql="INSERT INTO mei_comentarioforo (idcomentario, idforo, idusuario, comentario, hora,
                                fecha, grupal) VALUES ('', '".$_POST['hid_mensaje']."', '".$_SESSION['idusuario']."',
                                '".base64_encode($_POST['edt_comentario'])."', '".date("H:i:s")."','".date("Y-n-j")."','".$grupal."')";
                                $consulta=$baseDatos->ConsultarBD($sql);
							
							if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==7)
							{
								$sql="SELECT mei_relususub.idusuario FROM mei_relususub
									WHERE mei_relususub.idsubgrupo IN (SELECT mei_relususub.idsubgrupo FROM mei_relususub
									WHERE mei_relususub.idusuario='".$_SESSION['idusuario']."' AND mei_relususub.idsubgrupo IN
									(SELECT mei_subgrupo.idsubgrupo FROM mei_subgrupo WHERE mei_subgrupo.idtiposubgrupo=1
									AND mei_subgrupo.idgrupo IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru
									WHERE mei_relusugru.idusuario ='".$_SESSION['idusuario']."' AND mei_relusugru.idgrupo IN
									(SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria='".$idmateria."'))))
									AND mei_relususub.idusuario <> '".$_SESSION['idusuario']."'";
							}
							else if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
							{
								$sql="SELECT mei_relususub.idusuario FROM mei_relususub
									WHERE mei_relususub.idsubgrupo IN (SELECT mei_relususub.idsubgrupo FROM mei_relususub
									WHERE mei_relususub.idusuario='".$_SESSION['idusuario']."' AND mei_relususub.idsubgrupo IN
									(SELECT mei_subgrupo.idsubgrupo FROM mei_subgrupo WHERE mei_subgrupo.idtiposubgrupo=1
									AND mei_subgrupo.idgrupo IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru
									WHERE mei_relusuvirgru.idusuario ='".$_SESSION['idusuario']."' AND mei_relusuvirgru.idvirgrupo IN
									(SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria='".$idmateria."'))))
									AND mei_relususub.idusuario <> '".$_SESSION['idusuario']."'";
							}
                            $resultado2=$baseDatos->ConsultarBD($sql);

                            $idcomentariolider = mysql_insert_id();

                            /*$sql="UPDATE mei_comentarioforo SET  mei_comentarioforo.idcomentariolider='".$idcomentariolider."'
                            WHERE mei_comentarioforo.idusuario='".$_SESSION['idusuario']."' ";
            				$consulta=$baseDatos->ConsultarBD($sql);
                           */
							//INICIO Guarda el archivo adjunto de los comentarios del foro
                            $archivo= $_FILES['fil_archivo']['name'];
							$nombreArchivo="id".$idcomentariolider.$archivo;

							if (cargarArchivo('fil_archivo',corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosForo/') || $movido==true)
							{
								$sql="INSERT INTO mei_relcomarc(idcomentario,archivo,localizacion) VALUES('".$idcomentariolider."','".corregirCaracteresURL($archivo)."','".corregirCaracteresURL($nombreArchivo)."') ";
								$resultado=$baseDatos->ConsultarBD($sql);
								$movido=true;
							}
							else if(!empty($archivo))
							{
								$noAdjuntados=$archivo;
							}
                            //FIN Guarda el archivo adjunto de los comentarios del foro

							while (list($codigoUsuario) = mysql_fetch_array($resultado2))
                            {
                        		$sql="INSERT INTO mei_comentarioforo (idcomentario, idforo, idusuario, comentario, hora,
                                fecha, grupal, idcomentariolider) VALUES ('', '".$_POST['hid_mensaje']."', '".$codigoUsuario."',
                                '".base64_encode($_POST['edt_comentario'])."', '".date("H:i:s")."','".date("Y-n-j")."','".$grupal."','".$idcomentariolider."')";
                                $consulta=$baseDatos->ConsultarBD($sql);
                            }
                  	}// Fin de insertar comentario Grupal
					else if ($grupal==1 && $concluir==1)
                  	{print ($estado);
						$sql="INSERT INTO mei_comentarioforo (idcomentario, idforo, idusuario, comentario, hora,
                        fecha, grupal) VALUES ('', '".$_POST['hid_mensaje']."', '".$_SESSION['idusuario']."',
                        '".base64_encode($_POST['edt_comentario'])."', '".date("H:i:s")."','".date("Y-n-j")."','".$grupal."')";
                        $consulta=$baseDatos->ConsultarBD($sql);
						print ($estado);
						//Guarda el archivo adjunto de los comentarios del foro
						$sql2="SELECT MAX(mei_comentarioforo.idcomentario) FROM mei_comentarioforo";
						$consulta2=$baseDatos->ConsultarBD($sql2);
						list($maxId)=mysql_fetch_array($consulta2);

						$archivo= $_FILES['fil_archivo']['name'];
						$nombreArchivo="id".$maxId.$archivo;

						if (cargarArchivo('fil_archivo',corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosForo/'))
							{
								$sql="INSERT INTO mei_relcomarc(idcomentario,archivo,localizacion) VALUES('".$maxId."','".corregirCaracteresURL($archivo)."','".corregirCaracteresURL($nombreArchivo)."') ";
								$resultado=$baseDatos->ConsultarBD($sql);
							}
							else if(!empty($archivo))
							{
								$noAdjuntados=$archivo;
							}
					}
				redireccionar("../foro/verMensajeForo.php?idmateria=".$idmateria."&codigoMensaje=".$_POST['hid_mensaje']."&noAdjuntados=".$noAdjuntados);
			}
	}
	else
	{
		redireccionar('../login/');
	}
?>