<?PHP
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;

	if(comprobarSession())
	{
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{
			//Inicio Eliminar Comentario profesor
			if($_GET['estado']==1)
			{
				registrarBitacora(4,16,false);

				$sql="SELECT mei_comentarioforo.idforo, mei_comentarioforo.comentario,mei_comentarioforo.idusuario FROM mei_comentarioforo WHERE mei_comentarioforo.idcomentario='".$_GET['codigoComentario']."'";
				$consulta=$baseDatos->ConsultarBD($sql);
             	list($codigoMensaje,$comentario,$autor) = mysql_fetch_array($consulta);

						$sql="SELECT mei_foro.grupal FROM mei_foro WHERE mei_foro.idforo=".$codigoMensaje;
						$consulta=$baseDatos->ConsultarBD($sql);
						list($grupal) = mysql_fetch_array($consulta);
						if($grupal==0 || $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
						{
							$sql1="SELECT localizacion FROM mei_relcomarc WHERE idcomentario='".$_GET['codigoComentario']."'";
							$resultado2=$baseDatos->ConsultarBD($sql1);
							list($localizacion)=mysql_fetch_array($resultado2); 
							if (!empty($localizacion))
							{
								unlink ("../../datosMEIWEB/archivosForo/".$localizacion);
							}
							
							$sql="DELETE FROM mei_comentarioforo WHERE mei_comentarioforo.idcomentario='".$_GET['codigoComentario']."'";
							$resultado1=$baseDatos->ConsultarBD($sql);
						
						}
						else
						{
							if ($_SESSION['idtipousuario']==2)
							{
								$sql="SELECT mei_relususub.idusuario FROM mei_relususub
									WHERE mei_relususub.idsubgrupo IN (SELECT mei_relususub.idsubgrupo FROM mei_relususub
									WHERE mei_relususub.idusuario='".$autor."' AND mei_relususub.idsubgrupo IN
									(SELECT mei_subgrupo.idsubgrupo FROM mei_subgrupo WHERE mei_subgrupo.idtiposubgrupo=1
									AND mei_subgrupo.idgrupo IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru
									WHERE mei_relusugru.idusuario ='".$autor."' AND mei_relusugru.idgrupo IN
									(SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria='".$_GET['idmateria']."'))))";
							}
							else if ($_SESSION['idtipousuario']==5)
							{
								$sql="SELECT mei_relususub.idusuario FROM mei_relususub
									WHERE mei_relususub.idsubgrupo IN (SELECT mei_relususub.idsubgrupo FROM mei_relususub
									WHERE mei_relususub.idusuario='".$autor."' AND mei_relususub.idsubgrupo IN
									(SELECT mei_subgrupo.idsubgrupo FROM mei_subgrupo WHERE mei_subgrupo.idtiposubgrupo=1
									AND mei_subgrupo.idgrupo IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru
									WHERE mei_relusuvirgru.idusuario ='".$autor."' AND mei_relusuvirgru.idvirgrupo IN
									(SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria='".$_GET['idmateria']."'))))";
							}
	                      	$resultado=$baseDatos->ConsultarBD($sql);
                           	while (list($codigoUsuario) = mysql_fetch_array($resultado))
                            {
								$sql1="SELECT localizacion FROM mei_relcomarc WHERE idcomentario='".$_GET['codigoComentario']."'";
								$resultado2=$baseDatos->ConsultarBD($sql1);
								list($localizacion)=mysql_fetch_array($resultado2); 
								if (!empty($localizacion))
								{
									unlink ("../../datosMEIWEB/archivosForo/".$localizacion);
								}
								$sql="DELETE FROM mei_comentarioforo WHERE mei_comentarioforo.idusuario='".$codigoUsuario."'
                               	AND mei_comentarioforo.comentario='".$comentario."'";
                                $consulta=$baseDatos->ConsultarBD($sql);
                           	}
						}
						if (!empty($_GET['cadena']))
							redireccionar("buscarForo.php?idmateria=".$_GET['idmateria']."&cadena=".$_GET['cadena']);
						else
							redireccionar("../foro/verMensajeForo.php?codigoMensaje=".$codigoMensaje."&idmateria=".$_GET['idmateria']);
            }// FIN eliminar comentario
			// Inicio Eliminar Foro profesor
			else
			{
				registrarBitacora(4,19,false);
				$sql1="DELETE FROM mei_comentarioforo WHERE mei_comentarioforo.idforo='".$_GET['codigoMensaje']."'";
				$resultado1=$baseDatos->ConsultarBD($sql1);
				
				$sql="SELECT localizacion FROM mei_relforarc WHERE idforo='".$_GET['codigoMensaje']."'";
					$resultado3=$baseDatos->ConsultarBD($sql);
					list($localizacion)=mysql_fetch_array($resultado3); 
					if (!empty($localizacion))
					{
						unlink ("../../datosMEIWEB/archivosForo/".$localizacion);
					}
					
				$sql2="DELETE FROM mei_foro WHERE mei_foro.idusuario='".$_SESSION['idusuario']."' AND mei_foro.idforo='".$_GET['codigoMensaje']."'";
				$resultado2=$baseDatos->ConsultarBD($sql2);

			if (!empty($_GET['cadena']))
				redireccionar("buscarForo.php?idmateria=".$_GET['idmateria']."&cadena=".$_GET['cadena']);
			else
				redireccionar("../foro/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
			}//FIN Eleminar Foro
		}//Inicio eliminar comentario estudiante
		else if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
		{
			if($_GET['estado']==1)
			{
				registrarBitacora(4,16,false);
				
				$sql="SELECT calificacion FROM mei_comentarioforo WHERE idcomentario='".$_GET['codigoComentario']."'";
				$nota=$baseDatos->ConsultarBD($sql);
             	list($calificacion) = mysql_fetch_array($nota);
				
				if ($calificacion==0)
				{
					$sql="SELECT mei_comentarioforo.idforo, mei_comentarioforo.comentario,mei_comentarioforo.idusuario 
					FROM mei_comentarioforo WHERE mei_comentarioforo.idcomentario='".$_GET['codigoComentario']."'";
					$consulta=$baseDatos->ConsultarBD($sql);
             		list($codigoMensaje,$comentario,$autor) = mysql_fetch_array($consulta);

					$sql="SELECT mei_foro.grupal FROM mei_foro WHERE mei_foro.idforo=".$codigoMensaje;
					$consulta=$baseDatos->ConsultarBD($sql);
					list($grupal) = mysql_fetch_array($consulta);
					
					if($grupal==0)
					{
						$sql1="SELECT localizacion FROM mei_relcomarc WHERE idcomentario='".$_GET['codigoComentario']."'";
						$resultado2=$baseDatos->ConsultarBD($sql1);
						list($localizacion)=mysql_fetch_array($resultado2); 
						if (!empty($localizacion))
						{
						unlink ("../../datosMEIWEB/archivosForo/".$localizacion);
						}
						
						$sql="DELETE FROM mei_comentarioforo WHERE mei_comentarioforo.idcomentario='".$_GET['codigoComentario']."'";
						$resultado1=$baseDatos->ConsultarBD($sql);	
					}
					else
					{
						if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==7)
						{
							$sql="SELECT mei_relususub.idusuario FROM mei_relususub
                             WHERE mei_relususub.idsubgrupo IN (SELECT mei_relususub.idsubgrupo FROM mei_relususub
                             WHERE mei_relususub.idusuario='".$autor."' AND mei_relususub.idsubgrupo IN
                             (SELECT mei_subgrupo.idsubgrupo FROM mei_subgrupo WHERE mei_subgrupo.idtiposubgrupo=1
                             AND mei_subgrupo.idgrupo IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru
                             WHERE mei_relusugru.idusuario ='".$autor."' AND mei_relusugru.idgrupo IN
                             (SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria='".$_GET['idmateria']."'))))";
						}
						else if ($_SESSION['idtipousuario']==6)
						{
							$sql="SELECT mei_relususub.idusuario FROM mei_relususub
                             WHERE mei_relususub.idsubgrupo IN (SELECT mei_relususub.idsubgrupo FROM mei_relususub
                             WHERE mei_relususub.idusuario='".$autor."' AND mei_relususub.idsubgrupo IN
                             (SELECT mei_subgrupo.idsubgrupo FROM mei_subgrupo WHERE mei_subgrupo.idtiposubgrupo=1
                             AND mei_subgrupo.idgrupo IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru
                             WHERE mei_relusuvirgru.idusuario ='".$autor."' AND mei_relusuvirgru.idvirgrupo IN
                             (SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria='".$_GET['idmateria']."'))))";
						}
	                  	$resultado=$baseDatos->ConsultarBD($sql);
                       	while (list($codigoUsuario) = mysql_fetch_array($resultado))
                       	{
							$sql1="SELECT localizacion FROM mei_relcomarc WHERE idcomentario='".$_GET['codigoComentario']."'";
							$resultado2=$baseDatos->ConsultarBD($sql1);
							list($localizacion)=mysql_fetch_array($resultado2); 
							if (!empty($localizacion))
							{
								unlink ("../../datosMEIWEB/archivosForo/".$localizacion);
							}
							
							$sql="DELETE FROM mei_comentarioforo WHERE mei_comentarioforo.idusuario='".$codigoUsuario."'
                           	 AND mei_comentarioforo.comentario='".$comentario."'";
                           	$consulta=$baseDatos->ConsultarBD($sql);
	                      }
					}
				}
				else
				{	
					$sql="SELECT mei_comentarioforo.idforo FROM mei_comentarioforo WHERE mei_comentarioforo.idcomentario='".$_GET['codigoComentario']."'";
					$consulta=$baseDatos->ConsultarBD($sql);
             		list($codigoMensaje) = mysql_fetch_array($consulta);
					$_SESSION['ses_error_eliminar_comentario'] = 1;
                }		
			if (!empty($_GET['cadena']))
				redireccionar("buscarForo.php?idmateria=".$_GET['idmateria']."&cadena=".$_GET['cadena']);
			else
				redireccionar("../foro/verMensajeForo.php?codigoMensaje=".$codigoMensaje."&idmateria=".$_GET['idmateria']);
			}
		}
		else
		{
			redireccionar('../login/');
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>
