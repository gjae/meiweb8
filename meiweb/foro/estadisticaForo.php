<?PHP
	
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../estadistica/Grafica.class.php');	
	include_once ('../menu1/Menu1.class.php');	
	
	if(comprobarSession())
	{			
		if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{	
		?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		</head>
		<body>
    	<?PHP
        $idmateria=$_GET['idmateria'];
		$idforo=$_GET['codigoMensaje'];
		$grupo = $_GET['grupo'];
		if($_SESSION['idtipousuario']==5){
		$sql= "SELECT cf.idusuario, COUNT(DISTINCT(idcomentario)) AS comentario, s.idsubgrupo, sub.nombre  AS nombre 
		FROM mei_comentarioforo cf, mei_relususub s, mei_subgrupo sub, mei_virgrupo gr  
		WHERE  sub.idsubgrupo=s.idsubgrupo AND sub.idgrupo=gr.idvirgrupo AND gr.idvirmateria='".$idmateria."' 
		AND cf.idusuario=s.idusuario AND cf.idforo='".$idforo."' group by s.idsubgrupo, cf.idusuario;";	
		}else{
		$sql= "SELECT cf.idusuario, COUNT(DISTINCT(idcomentario)) AS comentario, s.idsubgrupo, sub.nombre  AS nombre 
		FROM mei_comentarioforo cf, mei_relususub s, mei_subgrupo sub, mei_grupo gr  
		WHERE  sub.idsubgrupo=s.idsubgrupo AND sub.idgrupo=gr.idgrupo AND gr.idmateria='".$idmateria."' 
		AND cf.idusuario=s.idusuario AND cf.idforo='".$idforo."' group by s.idsubgrupo, cf.idusuario;";
		}
		$unico=$baseDatos->ConsultarBD($sql);
		$i=0;
			while (list($idunico, $comen, $idsub, $nom)=mysql_fetch_array ($unico))
			{
				
				$sql1="SELECT primernombre, segundonombre, primerapellido, segundoapellido
					FROM mei_usuario WHERE idusuario=".$idunico['idusuario'];
				$nombre=$baseDatos->ConsultarBD($sql1);
				while ($nombreUsuario=mysql_fetch_array ($nombre))
				{
					$nombres[$i]= $nombreUsuario['primernombre']." ".$nombreUsuario['segundonombre']." ".$nombreUsuario['primerapellido']." ".$nombreUsuario['segundoapellido']." - SubG: ".$idunico['nombre'];
				}
				$valores[$i]=$idunico['comentario'];
				$i++;
				//$estudiantes[]=$idunico[idusuario];
			}			
		}
		$sqlcan = mysql_query("SELECT DISTINCT idcomentario FROM mei_comentarioforo WHERE idforo='$idforo'");
		$canti = mysql_num_rows($sqlcan)
		?>
        <table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><?PHP menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
        			<table class="tablaGeneral">
        				<tr class="trTitulo">
							<td><img src="imagenes/estadistica.gif" width="16" height="16" align="texttop"> Estad&iacute;stica de Foros</td>
						</tr>
                        <tr class="trListaOscuro">
                        	<?PHP
							if ($_SESSION['idtipousuario']==5)
	                            $sql= "SELECT nombre FROM mei_virmateria WHERE idvirmateria=".$idmateria;
							else
	                            $sql= "SELECT nombre FROM mei_materia WHERE idmateria=".$idmateria;								

							$nombre= $baseDatos->ConsultarBD($sql);
							list($nombre)=mysql_fetch_array ($nombre);
							
							if ($_SESSION['idtipousuario']==5)							
								$sql= "SELECT nombre FROM mei_virgrupo WHERE idvirmateria=".$idmateria;
							else
								$sql= "SELECT nombre FROM mei_grupo WHERE idmateria=".$idmateria;
							
							$grup= $baseDatos->ConsultarBD($sql);
							list($grup)=mysql_fetch_array ($grup);
							
							$sql1= "SELECT buscador FROM mei_foro WHERE idforo=".$idforo;							
							$comentario= $baseDatos->ConsultarBD($sql1);
							list($comentario)=mysql_fetch_array ($comentario);
							$alt="Foro \n";
							$alt.=$comentario;
							?>
						  	<td bgcolor="#DDDDDD">Estad&iacute;stica de uso del <b>
                            <a href="verMensajeForo.php?idmateria=<?PHP echo  $idmateria; ?>&codigoMensaje=<?PHP echo  $idforo; ?>" class="link" title="<?PHP echo $alt; ?>">Foro 
					       	</a></b>de la asignatura <?PHP echo  $nombre; ?> Grupo <?PHP echo  $grupo; ?>
						  	</td>
                   	  <tr class="trListaOscuro">
						  	<td>
							<br>
                       		<table width="400" align="center" bgcolor="#FFFFFF" class="tablaPrincipal" style="border-collapse:collapse; border: 1px solid #999999;">
                        		<tr>
                              	<td align="center" class="trSubTitulo">Gr&aacute;fica General de comentarios del Foro</td>
                            	</tr>
                                <tr>
                              	<td align="center">&nbsp;</td>
                            	</tr>
                                <tr>
            					<td align="center">
								<?PHP
								$grafica = new BAR_GRAPH("hBar");
								$grafica->values = $canti;
								$grafica->showValues = 1;
								$grafica->absValuesBorder = "0px solid silver";
								$grafica->absValuesBGColor = "#FFFFFF";
								$grafica->labelBGColor = "#DDDDDD";
								$grafica->labelBorder = "1px solid #A0A0A0";
								$grafica->labels = "Foro";
								$grafica->barLevelColor = array(1, "#FFCC66", @$mediaGeneral, "#FF6600");
								$grafica->labelSpace = 5;
								print  $grafica->create();
								?>
								<div align="center"></div></td>
            					</tr>				
          					</table><br>
                			</td>
                        </tr>
                        <tr class="trListaOscuro">
							<td><br>
								<table class="tablaPrincipal" width="400" align="center" style="border-collapse:collapse; border: 1px solid #999999;">
									<tr class="trSubTitulo">
									<td class="trSubTitulo" colspan="2"><div align="center">Estudiantes que no participaron en el Foro</div></td>
                                    </tr>
                                    <tr class="trSubTitulo">
									<td class="trSubTitulo" width="20%"><div align="center">Codigo</div></td>
                                    <td class="trSubTitulo" width="80%"><div align="center">Nombre</div></td>
                                    </tr>
                                    <?PHP
                                    mysql_query("SET NAMES 'utf8'");
                                    $sql= "SELECT idgrupo FROM mei_foro WHERE idforo=".$idforo;
									$idgrupo=$baseDatos->ConsultarBD($sql);
									list($idgrupo)=mysql_fetch_array ($idgrupo);
									
									if ($_SESSION['idtipousuario']==5)
										$sql1= "SELECT distinct(idusuario) FROM mei_relusuvirgru WHERE idvirgrupo=".$idgrupo." AND idusuario NOT IN (".$_SESSION['idusuario'].") ";
										
									else
										$sql1= "SELECT distinct(idusuario) FROM mei_relusugru WHERE idgrupo=".$idgrupo." AND idusuario NOT IN (".$_SESSION['idusuario'].") ";
									
									
									$idusuario=$baseDatos->ConsultarBD($sql1); 
									while (list($usuario)=mysql_fetch_array ($idusuario))
									{
										$sql2= "SELECT distinct(idusuario) FROM mei_comentarioforo WHERE idforo='".$idforo."' AND idusuario=".$usuario;
										$comentor=$baseDatos->ConsultarBD($sql2);
										list($comento)=mysql_fetch_array ($comentor);
										if (empty($comento))
										
										{
											$cont=$cont+1;
											$nocomento[$cont]=$usuario;	
											
										}
									}
									$num= sizeof($nocomento);
									for ($i=1; $i<=$num; $i++)
									{
										$idnocom= $nocomento[$i];
										$sql3= "SELECT primernombre, segundonombre, primerapellido, segundoapellido
												FROM mei_usuario WHERE idusuario=".$idnocom;
										$usuariono=$baseDatos->ConsultarBD($sql3);
										while($nombre=mysql_fetch_array ($usuariono))
										{
											if($i%2==0)
											{
												$color="trListaClaro";
											}
											else
											{
												$color="trListaOscuro";
											}		
											?>
											<tr class="<?PHP echo $color; ?>">		
												<td class="<?PHP echo $color; ?>"><?PHP echo $idnocom; ?></td>
												<td class="<?PHP echo $color; ?>"><?PHP echo $nombre['primernombre']." ".$nombre['segundonombre']." ".$nombre['primerapellido']." ".$nombre['segundoapellido']; ?> </td>
                                   			</tr>
										    <?PHP 
											}
										}//while
										?>                         
                             	</table><br>
                          	</td>
                    	</tr>
                	</table>
                </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><?PHP menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>                
            </tr>
        </table>        
		</body>
		</html>
    <?PHP 
	}
	else
	{
		redireccionar('../login/');					
	} 
?>