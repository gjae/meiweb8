<?PHP
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once ('../menu1/Menu1.class.php');		

	// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , [Barra de herramientas] , [Texto inicial] ) ;
	$editor=new FCKeditor('edt_comentario' , '100%' , '100%' , 'barraBasica' , '' ) ;

	if(comprobarSession())
	{
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
        </style>
		</head>
		<body>

		<script>
			function guardarComentario()
			{
					if(confirm("Esta seguro de concluir este Foro? "))
						document.frm_crearComentario.submit();
			}

			function enviarCancelar(idmateria)
			{
				location.replace("../foro/verMensajeForo.php?idmateria="+idmateria);
			}
		
			function editar()
			{
				document.frm_crearComentario.action="crearComentario.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>";
				document.frm_crearComentario.submit();
			}
			function confirmar(cadena)
			{
				if(document.frm_crearComentario.hid_comentario.value!="")
				{
					document.frm_crearComentario.action="guardarComentario.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&cadena="+cadena;
					document.frm_crearComentario.submit();
				}
				else 
				{
					alert ("Debe llenar completamente la información solicitada");
				}
			}
		</script>
			<style type="text/css">
			<!--
			.Estilo1 {color: #FF0000}
			-->
			</style>
		<?PHP
		$calendario=new FrmCalendario('frm_crearComentario','txt_fecha','formulario','false','');	
		?>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><?PHP menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
		<?PHP
		//FOROS CALIFICABLES
		if($_GET['calificable']==1)
		{
			$sql="SELECT mei_comentarioforo.idcomentario FROM  mei_comentarioforo 
				WHERE mei_comentarioforo.idforo='".$_GET['codigoMensaje']."' AND mei_comentarioforo.idusuario='".$_SESSION['idusuario']."'";
				$resultado=$baseDatos->ConsultarBD($sql);
				$bandera=mysql_num_rows($resultado);
				if(!empty($bandera))
				{
					redireccionar("verMensajeForo.php?error=1&idmateria=".$_GET['idmateria']."&codigoMensaje=".$_GET['codigoMensaje']);
				}
		}
		//FIN FOROS CALIFICABLES
		if (!empty($_GET['idmateria']))
		{		
			$idmateria=$_GET['idmateria'];
			$codigoMensaje=$_GET['codigoMensaje'];	
				
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				$sql="SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$idmateria;
			else
				$sql="SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$idmateria;
			
			$materia=$baseDatos->ConsultarBD($sql);
			list($materia) = mysql_fetch_array($materia);
		
			$sql="SELECT mei_foro.mensaje FROM mei_foro WHERE mei_foro.idforo =".$codigoMensaje;
			$mensaje=$baseDatos->ConsultarBD($sql);
			list($mensaje) = mysql_fetch_array($mensaje);		
		}
		else
		{
			list($idmateria,$materia)=explode('*',$_POST['hid_materia']);	
			list($codigoMensaje,$mensaje)=explode('*',$_POST['hid_mensaje']);	
		}
		?>	
				<table class="tablaGeneral">
				<?PHP
					$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM 						 						mei_usuario WHERE mei_usuario.idusuario='".$_SESSION['idusuario']."'";
						$autor=$baseDatos->ConsultarBD($sql);
						list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
				?>
					<form action="guardarComentario.php?cadena=<?PHP echo $_GET['cadena']; ?>&idmateria=<?PHP echo  $idmateria; ?>" method="post" enctype="multipart/form-data" name="frm_crearComentario">
					<tr class="trTitulo">
						<td colspan="2" class="trTitulo"><img src="imagenes/concluir.gif" width="16" height="16" align="texttop"> Concluir Foro </td>
					</tr>
				<?PHP
					if($_GET['error'] == '0x001')
					{
				?>
					<tr class="trAviso">
						<td colspan="2"><img src="imagenes/error.gif" width="17" height="18" align="texttop"><span class="Estilo1"> Error: Debe escribir un Comentario Final</span></td>
					</tr>
				<?PHP
					}
				?>								
					<tr class="trInformacion">
						<td width="25%"><b>Autor de la Conclusión:</b></td>
						<td width="75%"><?PHP echo  $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2; ?></td>
					</tr>
					<tr class="trInformacion">
<?PHP                        
			if ($_SESSION['idtipousuario']==5)
			{
?>				
						<td><b>Curso:</b></td>
<?PHP                        
			}
			else
			{
?>	
						<td><b>Materia:</b></td>
<?PHP                        
			}
?>	
						<td><?PHP echo  $materia; ?><input type="hidden" name="hid_materia"  value="<?PHP echo  $idmateria.'*'.$materia; ?>">
					</tr>
					<tr class="trInformacion">
						<td><b>Fecha de la Conclusión:</b></td>
						 <td><?PHP echo mostrarFecha(date('Y-m-j'),false); ?></td>
					</tr>
					<tr class="trInformacion">
						<td><b>Conclusión:</b></td>
						<td> 
						</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"> 
				<?PHP 
						if(empty($_POST['hid_comentario']))
							$editor->Value='';
						else
							$editor->Value=$_POST['hid_comentario'];
							$editor->crearEditor();
				?>
						</td>
					</tr>			
					<tr class="trInformacion">
						<td colspan="2"><div align="center">
                	        <label>Adjuntar Archivo
							<input type="file" name="fil_archivo">
                    		</label>
                            <input type="button" name="btn_crearComentario" value="Concluir Foro" onClick="javascript: guardarComentario()">
							<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar('<?PHP echo $idmateria; ?>')">
						</div></td>
					</tr>
							<input type="hidden" name="hid_mensaje"  value="<?PHP echo  $codigoMensaje; ?>">	
                            <input type="hidden" name="concluir" value="<?PHP echo  1; ?>">									
					</form>  
				</table>
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><?PHP menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
		</body>
		</html>
<?PHP 
	}
	else
	{
		redireccionar('../login/');					
	}
?>
