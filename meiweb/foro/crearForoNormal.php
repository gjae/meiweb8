<?PHP
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once ('../menu1/Menu1.class.php');	
	
	// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , [Barra de herramientas] , [Texto inicial] ) ;
	$editor=new FCKeditor('edt_foro' , '100%' , '100%' , 'barraBasica' , '' ) ;

	if(comprobarSession())
	{
	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5  || $_SESSION['idtipousuario']==7)
	{
		//Actualizar el estado de los foros
		$sql1="SELECT mei_foro.idforo, mei_foro.fechaactivacion FROM mei_foro";
		$resultado1=$baseDatos->ConsultarBD($sql1);

		while (list($codigoForo,$fechaActivacion)=mysql_fetch_array($resultado1))
		{
			list($a,$m,$d)=explode('-',date("Y-m-d"));
				$fechaActual= $a.$m.$d;
			list($a,$m,$d)=explode('-',$fechaActivacion);
				$fechaForo= $a.$m.$d;

			if($fechaForo>$fechaActual)
				$estado='0';
			else
				$estado='1';

			$sql2="UPDATE mei_foro SET mei_foro.estado='$estado' WHERE mei_foro.idforo='".$codigoForo."'";
			$resultado2=$baseDatos->ConsultarBD($sql2);

		}//fin de actualizacion
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
        </style>
		</head>
		<body>
        <script language="javascript">
			function enviarCancelar()
			{
				location.replace("../foro/index.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>");
			}
			function validar()
			{
				/*if(comprobarFechaIntervalo())
				{  */
					if(confirm("¿Esta seguro de crear este Foro con la información seleccionada?"))
					{
						document.frm_crearForo.submit();
					}
				/*}
				else
				{
					alert("Debe seleccionar un intervalo correcto de fechas");
				}   */
			}
			/*function comprobarFechaIntervalo()
			{
				var fecha=frm_crearForo.txt_fecha.value.split("-");
				var fechaC=frm_crearForo.txt_fechaC.value.split("-");

				if(parseInt(fecha[0]) <= parseInt(fechaC[0]))
				{
					if(parseInt(fecha[1]) <= parseInt(fechaC[1]))
					{
						if(parseInt(fecha[2]) < parseInt(fechaC[2]))
						{
							return true;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}   */
		</script>
        <?PHP
		$calendario=new FrmCalendario('frm_crearForo','txt_fecha','formulario','false',$_POST['hid_fecha']);
		$calendarioC=new FrmCalendario('frm_crearForo','txt_fechaC','formulario','false',$_POST['hid_fechaC']);
		?>
        <table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><?PHP menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td><?PHP
                	if(!empty($_POST['cbo_grupo']))
					{
						list($idmateria,$materia)=explode('*',$_POST['hid_materia']);
						list($codigoGrupo,$grupo)=explode('*',$_POST['cbo_grupo']);
					}
					else
					{
						list($idmateria,$materia)=explode('*',$_GET['infoMateria']);
						list($codigoGrupo,$grupo)=explode('*',$_GET['infoGrupo']);
					}
					?>
                    <table class="tablaGeneral" >
						<tr class="trTitulo">
							<td colspan="4"><img src="imagenes/foro.gif" width="16" height="16" align="texttop"> Crear Foro</td>
						</tr>
						<?PHP
							$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
								FROM mei_usuario WHERE mei_usuario.idusuario='".$_SESSION['idusuario']."'";
							$autor=$baseDatos->ConsultarBD($sql);
							list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
							
						?>
						<form name="frm_crearForo" method="post" enctype="multipart/form-data" action="guardarForo.php?calificable=0&codigoGrupo=<?PHP echo $codigoGrupo; ?>&idmateria=<?PHP echo  $idmateria; ?>">
						<?PHP
							if($_GET['error'] == '0x001')
							{
						?>
							<tr class="trAviso">
								<td colspan="4"><img src="imagenes/error.gif" width="17" height="18" align="texttop"><span class="Estilo1"> Error: Debe escribir un Mensaje</span></td>
							</tr>
						<?PHP
							}
						?>
							<tr class="trInformacion">
								<td width="25%"><b>Autor Foro:</b></td>
								<td width="75%" colspan="3"><?PHP echo  $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2; ?></td>
							</tr>
							<tr class="trInformacion">
<?PHP                        
			if ($_SESSION['idtipousuario']==5)
			{
?>				
					<td><b>Curso:</b></td>
<?PHP                        
			}
			else
			{
?>	
					<td><b>Materia:</b></td>
<?PHP                        
			}
?>	
								<td colspan="3"><?PHP echo  $materia; ?><input type="hidden" name="hid_materia"  value="<?PHP echo  $idmateria.'*'.$materia; ?>"></td>
							</tr>
                            <tr class="trInformacion">
								<td><b>Grupo:</b></td>
								<td colspan="3"><?PHP echo  $grupo; ?><input type="hidden" name="hid_grupo"  value="<?PHP echo  $codigoGrupo.'*'.$grupo; ?>"></td>
							</tr>
							<tr  class="trInformacion">
								<td width="25%"><b>Fecha de Activación del Foro:</b></td>
								<td width="25%">
								<?PHP
									$calendario->CrearFrmCalendario();
								?>								</td>
                                <td width="25%"><b>Hora de Activación del Foro:</b>								</td>
                                <td width="25%">
       				            <select name="cbo_horaActivacion">
    					    	    <?PHP for ($i=0;$i<10;$i++)
    					               {
                                	?>
    					        		<option value="<?PHP echo $i; ?>">0<?PHP echo $i; ?></option>
                                	<?PHP
                                    	}
                                    ?>
    					            <?PHP for ($i=10;$i<24;$i++)
    					            	{
                                    ?>
    					            	<option value="<?PHP echo $i; ?>"><?PHP echo $i; ?></option>
                                    <?PHP
                                    	}
                                    ?>
                               	</select>:
       				            <select name="cbo_minutoActivacion">
    					        	<option value="0">00</option>
    					            <option value="0">05</option>
                                    <?PHP
                                    	$j=10;
                                    	while ($j<=55)
    					                {
                                    ?>
    					            <option value="<?PHP echo $j; ?>"><?PHP echo $j; ?></option>
                                    <?PHP
                                    	$j=$j+5;
                                    	}
                                    ?>
                                </select>                            	</td>
							</tr>
							<tr  class="trInformacion">
								<td><b>Fecha de Caducidad del Foro:</b></td>
								<td>
								<?PHP
									$calendarioC->CrearFrmCalendario();
								?>								</td>
								<td><b>Hora de Caducidad del Foro:</b>								</td>
                                <td>
       				            <select name="cbo_horaCaducidad">
    					        <?PHP for ($i=0;$i<10;$i++)
    					         	{
                                ?>
    					        	<option value="<?PHP echo $i; ?>">0<?PHP echo $i; ?></option>
                                <?PHP
                                	}
                                ?>
    					        <?PHP for ($i=10;$i<24;$i++)
    					        	{
                                ?>
    					        	<option value="<?PHP echo $i; ?>"><?PHP echo $i; ?></option>
                                <?PHP
                                	}
                                ?>
                                </select>:
       				            <select name="cbo_minutoCaducidad">
    					        	<option value="0">00</option>
    					            <option value="0">05</option>
                                    <?PHP
                                    	$j=10;
                                        while ($j<=55)
    					                {
                                    ?>
    					            <option value="<?PHP echo $j; ?>"><?PHP echo $j; ?></option>
                                    <?PHP
                                    	$j=$j+5;
                                    	}
                                    ?>
                                </select>                                </td>
							</tr>
							<tr  class="trInformacion">
							  <td colspan="4"><b>Permitir que los participantes puedan modificar o eliminar sus comentarios</b> <input name="chk_opciones" type="checkbox" value="1"  ></td>
						  </tr>
							<tr  class="trInformacion">
								<td colspan="4"><b>Mensaje:</b></td>
							</tr>
							<tr  class="trInformacion">
								<td colspan="4"><?PHP $editor->crearEditor();?></td>
							</tr>                          
							<tr  class="trInformacion">
								<td colspan="4"><div align="center">
                                	<input type="file" name="fil_archivo">
                                    <input type="button" name="sub_crearForo" value="Guardar Foro" onClick="javascript: validar()">
									<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()">
								</div></td>
							</tr>
						</form>
					</table>
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><?PHP menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
	</body>
	</html>
<?PHP 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
?>