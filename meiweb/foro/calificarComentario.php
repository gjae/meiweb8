<?PHP
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');

	if(comprobarSession())
	{
		$saludo=mostrarSaludo($_SESSION['idtipousuario'],$_SESSION['sexo']);
		$sistemaTema=recuperarVariableSistema("sistematema");

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?PHP echo $sistemaTema; ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
function enviar()
{
	if(validar())
	{
		document.frm_calificarForo.submit();
		window.close();
	}
		
}

function validar()
{
	if(document.frm_calificarForo.txt_nota.value == false && document.frm_calificarForo.txt_nota.value!= "0")
	{
				alert ("Debe Digitar una calificación al comentario");
				return false;
	}			
	else 
	{
		if(document.frm_calificarForo.txt_nota.value >=0 && document.frm_calificarForo.txt_nota.value <= 5)
		{
			
			return true;
		}
		else
		{
				alert ("Nota Erronea: Debe introducir entre 0 y 5");
				return false;
		}

	}
}
</script>
</head>

<body>
	<table class="tablaPrincipal" width="180px" >
		<tr class="trInformacion">
	 	 	<td align="center" class="trInformacion">
				<div align="center">
				  <table align="center" class="tablaPrincipal" width="100%">
		<tr class="trTitulo" >
			<td class="trTitulo" colspan="2"><div align="center"><img src="imagenes/calificar.gif" width="16" height="15"><b>Calificar </b></div></td>
		</tr>
				  
				  <form name="frm_calificarForo" method="post" action="guardarCalificacion.php?cadena=<?PHP echo $_GET['cadena']; ?>&codigoComentario=<?PHP echo $_GET['codigoComentario']; ?>&codigoMensaje=<?PHP echo $_GET['codigoMensaje']; ?>&idmateria=<?PHP echo $_GET['idmateria']; ?>" target="mensajeForo">
					  <tr class="trInformacion">
					    <td>Digitar Calificaci&oacute;n: 
					    <td><input type="text" name="txt_nota" size="10"></td>
						
				    </tr>
					
					  <tr class="trInformacion">
					  	<td colspan="2"><div align="center">
                          <input type="button" name="btn_calificar" value="Calificar Comentario" onClick="javascript:enviar()">
</div></td>
					  </tr>
  				  </form>
			      </table>
		  </div></td>
		</tr>
	</table>
</body>
</html>
<?PHP 
	}
		else
			redireccionar('../login/');					
?>
