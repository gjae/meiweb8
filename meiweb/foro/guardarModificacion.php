<?PHP
	include_once ('../librerias/validar.lib.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();

	if(comprobarSession())
	{	
		// Inicio guardado de modificación al foro (solo usuarios profesores)
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==7)
		{
			if (!empty($_GET['principal']))
			{
				$sql="UPDATE mei_foro SET mei_foro.fechaactivacion='".date("Y-n-d")."' WHERE mei_foro.idforo='".$_GET['codigoMensaje']."'";
				$baseDatos->ConsultarBD($sql);

				redireccionar("../foro/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
			}
			else
			{
				list($idmateria,$materia)=explode('*',$_POST['hid_materia']);
				if(!comprobarEditor($_POST['edt_foro']))
				{
					redireccionar("modificarForo.php?error=0x001&idmateria=".$_GET['idmateria']."&codigoMensaje=".$_GET['codigoMensaje']);
				}
				else
				{
					registrarBitacora(4,15,false);
				
                	$hA=$_POST['cbo_horaActivacion'];
                    $mA=$_POST['cbo_minutoActivacion'];
                    $hC=$_POST['cbo_horaCaducidad'];
                    $mC=$_POST['cbo_minutoCaducidad'];
					$abrir=$_POST['chk_activar'];
					$activacion=$hA.":".$mA.":00";
                    $caducidad=$hC.":".$mC.":00";
                       
          			$cadenaBuscador=strip_tags($_POST['edt_foro']);
          			$cadenaBuscador=limpiarCadenaBusqueda($cadenaBuscador);

                    $sql="UPDATE mei_foro SET mei_foro.fechaactivacion='".$_POST['txt_fecha']."' ,
                    				mei_foro.fechacaducidad='".$_POST['txt_fechaC']."' ,
                                    mei_foro.horaactivacion='".$activacion."' ,
                                    mei_foro.horacaducidad='".$caducidad."' ,
								    mei_foro.buscador='".$cadenaBuscador."' ,
                                    mei_foro.mensaje='".base64_encode($_POST['edt_foro'])."', 
									mei_foro.actopcion='".$_POST['chk_opciones']."'
									WHERE mei_foro.idforo='".$_GET['codigoMensaje']."'";
		       		$baseDatos->ConsultarBD($sql);
					if ($abrir==1)
					{
						$sql2="UPDATE mei_foro SET mei_foro.concluir=0 WHERE mei_foro.idforo='".$_GET['codigoMensaje']."'";
						$baseDatos->ConsultarBD($sql2);
					}
					//Guarda el archivo adjunto de la modificación del foro				
					
					$sql="SELECT localizacion FROM mei_relforarc WHERE idforo='".$_GET['codigoMensaje']."'";
					$resultado3=$baseDatos->ConsultarBD($sql);
					list($localizacion)=mysql_fetch_array($resultado3); 
					if (!empty($localizacion))
					{
						@unlink ("../../datosMEIWEB/archivosForo/".$localizacion);
						
						$maxId= $_GET['codigoMensaje'];
						$archivo= $_FILES['fil_archivo']['name']; 
						$nombreArchivo="id".$maxId.$archivo;
						
						if (cargarArchivo('fil_archivo',corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosForo/'))
							{
								$sql2="UPDATE mei_relforarc SET archivo='".corregirCaracteresURL($archivo)."',
															localizacion='".corregirCaracteresURL($nombreArchivo)."'
															WHERE idforo='".$maxId."'";
															$baseDatos->ConsultarBD($sql2);					
							}
						else
							{
								if(!empty($archivo))
								{
									$noAdjuntados.=$archivo;
								}
							}
					}
					else if (empty($localizacion))
					{
						$maxId= $_GET['codigoMensaje'];
						$archivo= $_FILES['fil_archivo']['name']; 
						$nombreArchivo="id".$maxId.$archivo;
						
						if (cargarArchivo('fil_archivo',corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosForo/'))
							{
								$sql2="INSERT INTO mei_relforarc(idforo,archivo,localizacion) 
										VALUES('".$maxId."','".corregirCaracteresURL($archivo)."','".corregirCaracteresURL($nombreArchivo)."') ";
															$baseDatos->ConsultarBD($sql2);					
							}
						else
							{
								if(!empty($archivo))
								{
									$noAdjuntados.=$archivo;
								}
							}
					}			
				redireccionar("index.php?idmateria=".$_GET['idmateria']."&noAdjuntados=".$archivo);
				}
			}
		}//Fin de modificacion de foro
		//Inicio modificacion de comentarios
		else if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6)
		{
			list($idmateria,$materia)=explode('*',$_POST['hid_materia']);
			
			if(!comprobarEditor($_POST['edt_comentario']))
			{
				redireccionar("modificarComentario.php?error=0x001&idmateria=".$idmateria."&codigoComentario=".$_GET['codigoComentario']);
			}
			else
			{
				registrarBitacora(4,15,false);
				
				$sql2="SELECT serial FROM mei_comentarioforo WHERE idcomentario='".$_GET['codigoComentario']."'";
				$resultado=$baseDatos->ConsultarBD($sql2);
				list($serial)= mysql_fetch_array($resultado);
				
				if (!empty($serial))
				{
					$sql="UPDATE mei_comentarioforo SET fecha='".date('Y-n-j')."' ,
									hora='".date('H:i:s')."',
                    				comentario='".base64_encode($_POST['edt_comentario'])."' 
									WHERE serial = '".$serial."'";
		    		$baseDatos->ConsultarBD($sql);
				}
				else if (empty($serial))
				{
					$sql="UPDATE mei_comentarioforo SET fecha='".date('Y-n-j')."' ,
									hora='".date('H:i:s')."',
                    				comentario='".base64_encode($_POST['edt_comentario'])."' 
									WHERE idcomentario = '".$_GET['codigoComentario']."'";
		    		$baseDatos->ConsultarBD($sql);
				}	
				//Guarda el archivo adjunto de la modificación del foro				
					
				$sql="SELECT localizacion FROM mei_relcomarc WHERE idcomentario='".$_GET['codigoComentario']."'";
				$resultado3=$baseDatos->ConsultarBD($sql);
				list($localizacion)=mysql_fetch_array($resultado3); 
				
				if (!empty($localizacion))
				{
					@unlink ("../../datosMEIWEB/archivosForo/".$localizacion);
					
					$maxId= $_GET['codigoComentario'];
					$archivo= $_FILES['fil_archivo']['name']; 
					$nombreArchivo="id".$maxId.$archivo;
				
					if (cargarArchivo('fil_archivo',corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosForo/'))
						{
							$sql2="UPDATE mei_relcomarc SET archivo='".corregirCaracteresURL($archivo)."',
														localizacion='".corregirCaracteresURL($nombreArchivo)."'
														WHERE idcomentario='".$maxId."'";
														$baseDatos->ConsultarBD($sql2);	
																		
						}
					else
						{
							if(!empty($archivo))
							{
								$noAdjuntados.=$archivo;
							}
						}
				}
				else if (empty($localizacion))
				{
					$maxId= $_GET['codigoComentario'];
					$archivo= $_FILES['fil_archivo']['name']; 
					$nombreArchivo="id".$maxId.$archivo;
				
					if (cargarArchivo('fil_archivo',corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosForo/'))
						{
							$sql2="INSERT INTO mei_relcomarc(idcomentario,archivo,localizacion) 
									VALUES('".$maxId."','".corregirCaracteresURL($archivo)."','".corregirCaracteresURL($nombreArchivo)."') ";
									$baseDatos->ConsultarBD($sql2);	
																		
						}
					else
						{
							if(!empty($archivo))
							{
								$noAdjuntados.=$archivo;
							}
						}
				}		
			}	
			redireccionar("../foro/verMensajeForo.php?idmateria=".$idmateria."&codigoMensaje=".$_GET['codigoMensaje']."&noAdjuntados=".$noAdjuntados);
		}
		else
		{
			redireccionar('../login/');
		}

	}
	else
	{
		redireccionar('../login/');
	}
?>

