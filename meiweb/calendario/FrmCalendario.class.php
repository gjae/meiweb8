<?
	class FrmCalendario
	{
		var $m_nombreFormulario;
		var	$m_nombreVariable;
		var	$m_permitirPasado;		
		var	$m_agnoActual;
		var	$m_mesActual;
		var	$m_diaActual;
		var $m_mesInicial;
		var $m_valor;
		
		function FrmCalendario($a_nombreFormulario,$a_nombreVariable,$a_tipo,$a_permitirPasado,$a_valor)
		{
			$this->m_agnoActual=false;
			$this->m_mesActual=false;
			$this->m_diaActual=false;
			
			$this->m_nombreFormulario=$a_nombreFormulario;
			$this->m_nombreVariable=$a_nombreVariable;
			$this->m_permitirPasado=$a_permitirPasado;
		
			$this->m_valor=$a_valor;
			
			if(empty($this->m_valor))	
			{
				$agno=date("Y");
				$mes=date("n");
				$dia=date("j");
				
				$this->m_valor=	$agno."-".$mes."-".$dia;
			}
			
				if($a_tipo=='formulario')
				{
					$this->IniciarFrmCalendario();
				}
				else if($a_tipo=='ventana')
				{
					$this->IniciarVentanaFrmCalendario();
				}
		}
								
		function IniciarFrmCalendario()
		{
			print "<script language='javascript'>
				var ventanaFrmCalendario=false
					function frmCalendario".$this->m_nombreVariable."()
					{		
						var fecha=document.getElementById('".$this->m_nombreVariable."');
						
						if (typeof ventanaFrmCalendario.document == 'object')
						 {
							ventanaFrmCalendario.close()
						 }
						 
						if(fecha.disabled== false) 			 
						{
							ventanaFrmCalendario = window.open('../calendario/ventanaFrmCalendario.php?hid_nombreFormulario=".$this->m_nombreFormulario."&hid_nombreVariable=".$this->m_nombreVariable."&hid_permitirPasado=".$this->m_permitirPasado."','Calendario','width=220,height=200,left=10,top=10,scrollbars=NO,menubars=NO,status=NO,statusbar=NO,resizable=NO,location=NO')
						}
					} 
					</script>";			
		}
		
		function CrearFrmCalendario()
		{
			?><input style="text-align: center;" name="<?= $this->m_nombreVariable?>" id="<?= $this->m_nombreVariable?>" readonly="1" class="link" type="text" size="10" value="<?= $this->m_valor?>"><a href="javascript:frmCalendario<?= $this->m_nombreVariable?>();"><img src="../calendario/imagenes/botonCalendario.gif" alt="Selector de fechas" width="20" height="16" border="0"></a><?
		}
		
		function IniciarVentanaFrmCalendario()
		{
			print "<script language='javascript'>
						function devolverFecha(fecha)
						{
							eval (\"opener.document.".$this->m_nombreFormulario.".".$this->m_nombreVariable.".value=fecha\")
							window.close()
						}
					</script>
				 ";
				 
		}
		
		function CrearListaAgnos()
		{
			if($this->m_permitirPasado=='true')
			{
				$agnoInicial=date('Y')-100;
			}
			else if($this->m_permitirPasado=='false')
			{
				$agnoInicial=date('Y');
			}
					
			$agnoFinal=date('Y')+5;
			
			?>
<select name="cbo_agnoCalendario" onChange="javascript: frm_calendario.submit();"><?
			
			for($agno=$agnoInicial;$agno<$agnoFinal;$agno++)
			{
				if($agno==$this->m_agnoActual)
				{
					?><option selected><?= $agno?></option><?
				}
				else
				{
					?><option><?= $agno?></option><?
				}
			}
			?></select><?
		}
		
		function CrearListaMeses()
		{
			$listaMeses=array(	1 	=> 'Enero',	
								2 	=> 'Febrero',		
								3 	=> 'Marzo',
								4 	=> 'Abril',
								5 	=> 'Mayo',
								6 	=> 'Junio',
								7 	=> 'Julio',
								8 	=> 'Agosto',
								9 	=> 'Septiembre',
								10 	=> 'Octubre',
								11 	=> 'Noviembre',
								12 	=> 'Diciembre');
				
			?><select name="cbo_mesCalendario" onChange="javascript: frm_calendario.submit();" ><?
								
			if($this->m_permitirPasado=='true')
			{
				$mesInicial=1;
			}
			else if($this->m_permitirPasado=='false')
			{
				if($this->m_agnoActual == date('Y'))
				{
					$mesInicial=date('n');
				}
				else
				{
					$mesInicial=1;
				}
			}
			
			for($mes=$mesInicial;$mes<=12;$mes++)
			{
			    if($this->m_mesActual==$mes)				
				{
					?><option value ="<?= $mes?>" selected ><?= $listaMeses[$mes]?></option><?
				}
				else
				{
					?><option value ="<?= $mes ?>"><?= $listaMeses[$mes]?></option><?
				}
			}
			
			?></select><?		
		}
		
		function DibujarFrmCalendario($a_agnoActual,$a_mesActual)
		{
			
			$horaUnix=mktime (0,0,0, $a_mesActual, 1, $a_agnoActual); 
			$diaInicioMes=date("w",$horaUnix);
			$numeroDiasMes=date("t",$horaUnix);
			
			$listaDias=array();
			$listaDias=array_pad($listaDias,42,'&nbsp;');
			
			for($i=$diaInicioMes;$i<$numeroDiasMes+$diaInicioMes;$i++)
			{
					$listaDias[$i]=$i+1-$diaInicioMes;
			}
			
			?>
			<table border="0">
			  <tr>
				<td width="25"><center class="Estilo21"><b>Dom</b></center></td>
				<td width="25"><center class="Estilo1"><b>Lun</b></center></td>
				<td width="25"><center class="Estilo1"><b>Mar</b></center></td>
				<td width="25"><center class="Estilo1"><b>Mie</b></center></td>
				<td width="25"><center class="Estilo1"><b>Jue</b></center></td>
				<td width="25"><center class="Estilo1"><b>Vie</b></center></td>
				<td width="25"><center class="Estilo1 Estilo1"><b>Sab</b></center></td>
			 </tr>
			 <?
			  
			$cont=0;
			
			  for($i=0;$i<6;$i++)
				{
					?><tr bgcolor="#FFFFFF"><?
					
					 for($k=0;$k<7;$k++)
					 {
						if($listaDias[$cont]=='&nbsp;')
						{
							?><td><center class="Estilo16"><?= $listaDias[$cont]?></center></td><?
						}
						else
						{
							 if($listaDias[$cont]==date('j'))
							 	{
									 $imagenFondo= "background='imagenes/diaSeleccionado.gif'";
								} 
								else
								{
									$imagenFondo=" ";
								}
								
								if($this->m_permitirPasado=='true')
									{
										$enlaceFecha="<a href=\"javascript:devolverFecha('".$this->m_agnoActual."-".$this->m_mesActual."-".$listaDias[$cont]."');\">".$listaDias[$cont]."</a>";
									}
									else if($this->m_permitirPasado=='false')
									{
										if($listaDias[$cont] < date('j') && $this->m_mesActual==date('n'))
										{
											$enlaceFecha=$listaDias[$cont];
										}
										else
										{
											$enlaceFecha="<a href=\"javascript:devolverFecha('".$this->m_agnoActual."-".$this->m_mesActual."-".$listaDias[$cont]."');\">".$listaDias[$cont]."</a>";
										}
									}
													
							?><td <?= $imagenFondo?> ><center class="Estilo16"><?= $enlaceFecha?></center></td><?
						}
						
						$cont++;
					 }
					 
					?></tr><?
				}
				
			?></table><?
		}
		
		function CrearVentanaFrmCalendario($a_fechaCalendario)
		{
			
			list($this->m_agnoActual,$this->m_mesActual,$this->m_diaActual)=explode('-',$a_fechaCalendario);
		
			if(empty($this->m_agnoActual) || empty($this->m_mesActual) || empty($this->m_diaActual))
			{
				list($this->m_agnoActual,$this->m_mesActual,$this->m_diaActual)=explode('-',date('Y-n-j'));
			}
			
			if($this->m_permitirPasado=='true')
			{
				$mesInicial=1;
			}
			else if($this->m_permitirPasado=='false')
			{
				if($this->m_agnoActual == date('Y'))
				{
					$mesInicial=date('n');
				}
				else
				{
					$mesInicial=1;
				}
			}
			
			if($this->m_mesActual<$mesInicial)
			{
				$this->m_mesActual=$mesInicial;
			}
												
			$mesAnterior=$this->m_mesActual-1;
			$mesSiguente=$this->m_mesActual+1;						
			
			if($mesSiguente > 12)
			{
				$mesSiguente=$mesInicial;
			}
			
			if($mesAnterior < $mesInicial)
			{
				$mesAnterior=12;		
			}
		
			?>	<form action="<? $PHP_SELF ?>" name="frm_calendario" target="_self" method="get">
				<input name="hid_diaCalendario" type="hidden" value="<?= $this->m_diaActual?>">
				<input name="hid_permitirPasado" type="hidden" value="<?= $this->m_permitirPasado?>">
				<input name="hid_nombreFormulario" type="hidden" value="<?= $this->m_nombreFormulario?>">
				<input name="hid_nombreVariable" type="hidden" value="<?= $this->m_nombreVariable?>">
				
				<table width="200"  border="0" align="center" bgcolor="#EFEFEF">
				  <tr align="center" valign="middle" bordercolor="#CCCCCC">
					<td valign="top" bordercolor="#CCCCCC"><table width="200" height="22" border="0">
					  <tr>
						<td valign="middle"><a href="ventanaFrmCalendario.php?hid_nombreFormulario=<?= $this->m_nombreFormulario?>&hid_nombreVariable=<?= $this->m_nombreVariable?>&hid_permitirPasado=<?= $this->m_permitirPasado?>&cbo_agnoCalendario=<?= $this->m_agnoActual?>&cbo_mesCalendario=<?= $mesAnterior?>&hid_diaCalendario=<?= $this->m_diaActual ?>" target="_self"><img src="imagenes/botonAnterior.gif" alt="Mes anterior" width="18" height="18" border="0"></a></td>
						<td>
						<? $this->CrearListaMeses();?>
						</td>
						<td>
						<? $this->CrearListaAgnos();?>
						</td>
						<td valign="middle"><a href="ventanaFrmCalendario.php?hid_nombreFormulario=<?= $this->m_nombreFormulario?>&hid_nombreVariable=<?= $this->m_nombreVariable?>&hid_permitirPasado=<?= $this->m_permitirPasado?>&cbo_agnoCalendario=<?= $this->m_agnoActual?>&cbo_mesCalendario=<?= $mesSiguente?>&hid_diaCalendario=<?= $this->m_diaActual ?>" target="_self"><img src="imagenes/BotonSiguiente.gif" alt="Mes siguiente" width="18" height="18" border="0"></a></td>
					  </tr>
					</table></td>
				  </tr>
				  <tr bordercolor="#CCCCCC">
					<td height="9" align="center" valign="top">
						<? $this->DibujarFrmCalendario($this->m_agnoActual,$this->m_mesActual);?>
					</td>
				  </tr>
				  <tr bordercolor="#CCCCCC">
					<td height="9" bordercolor="#F2F2F2" bgcolor="#F2F2F2"><center class="Estilo6">
					  <a href="javascript:devolverFecha('<?= date('Y-n-j')?>');" class="Estilo16">Hoy: <?= date('Y-n-j')?></a></center></td>
				  </tr>
			   </table>
				<div align="left"></div>
			 </form><?		
		}
			
	}

?>
