<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	

	if(comprobarSession())
	{
		$baseDatos=new BD();

		if(!empty($_GET['idCalendario']))
		{
			$sql="SELECT mei_calendario.mensaje, mei_calendario.fechamensaje, mei_calendario.cartelera, mei_calendario.destino FROM mei_calendario
						WHERE mei_calendario.idcalendario =".$_GET['idCalendario'];

			$consulta=$baseDatos->ConsultarBD($sql);
			list($mensaje,$fechamensaje,$cartelera,$destino)= mysql_fetch_array($consulta);

			if($destino==0)
			{
				$destinoTodos="checked";
				$gruposCheck="checked";
			}
			else if($destino==1)
			{
				$destinoSeleccionados="checked";
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					$sql="SELECT mei_relcalvirgru.idvirgrupo FROM mei_relcalvirgru WHERE mei_relcalvirgru.idcalendario =".$_GET['idCalendario'];
				else
					$sql="SELECT mei_relcalgru.idgrupo FROM mei_relcalgru WHERE mei_relcalgru.idcalendario =".$_GET['idCalendario'];
				$consulta=$baseDatos->ConsultarBD($sql);

				$cont=0;

				while(list($datoGrupo)=mysql_fetch_array($consulta))
				{
					$listaGrupos[$cont++]=$datoGrupo;
				}

			}

			if($cartelera==1)
			{
				$estadoCartelera="checked";
			}
			else
			{
				$estadoCartelera='';
			}

		?>

		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">

		<script language="javascript">

			function enviarCancelar()
			{
				location.replace("../calendario/mostrarCalendarioMensaje.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&cbo_materia=<?=$_GET['cbo_materia']?>&calendarioGeneral=<?='1'?>");
			}

			function chk_habilitar()
			{
				for(i=0;i<document.frm_calendario.elements.length;i++)
				{
					if(document.frm_calendario.elements[i].id=="grupo")
						document.frm_calendario.elements[i].disabled=false;
				}
			}

			function chk_deshabilitar()
			{
				for(i=0;i<document.frm_calendario.elements.length;i++)
				{
					if(document.frm_calendario.elements[i].id=="grupo")
						document.frm_calendario.elements[i].checked=true;
				}
			}

			function chk_click ()
			{
				var radioTodos=document.getElementById('radioT');
				var radioSelec=document.getElementById('radioS');
				radioTodos.checked=false;
				radioSelec.checked=true;
			}

			function enviar()
			{
				var contBandera=0;
				for(i=0;i<document.frm_calendario.elements.length;i++)
				{
					if(document.frm_calendario.elements[i].checked==true && document.frm_calendario.elements[i].id=="grupo")
					contBandera++;
				}

				if(contBandera == 0)
				{
					alert("Debe seleccionar por lo menos un grupo destino");
				}
				else if(document.frm_calendario.txt_mensajeCalendario.value == false)
				{
					alert("Debe llenar completamente la información solicitada");
				}
				else
				{
					document.frm_calendario.submit();
				}
			}

		</script>
		<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
        </style>
		</head>
		<body>
		<?
			$mensaje=stripslashes(base64_decode($mensaje));
			list($tituloMensaje,$descripcion)=explode("[$$$]",$mensaje);
			$calendario=new FrmCalendario('frm_calendario','txt_fecha','formulario','false',$fechamensaje);
			$editor=new FCKeditor('edt_descripcionCalendario' , '100%' , '200' , 'barraBasica' , $descripcion) ;
		?>

		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td colspan="4" class="trTitulo"><img src="imagenes/calendario.gif" width="16" height="16" align="texttop"> Modificar Cita</td>
						</tr>

					<form action="actualizarCalendarioMensaje.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&cbo_materia=<?=$_GET['cbo_materia']?>" name="frm_calendario" method="post" >
					<?
					if($_GET['error']=="0x001")
					{
					?>
						<tr  class="trInformacion">
						  <td colspan="2"><img src="imagenes/error.gif" width="17" height="18" align="texttop"><span class="Estilo1"> Error: Debe escribir la descripci&oacute;n de la Cita</span></td>
					  </tr>
					<?
					}
					?>
						<tr  class="trInformacion">
							<td width="25%"><b>Fecha Cita :</b></td>
							<td width="75%"><b>
						    <? $calendario->CrearFrmCalendario();?>
							</b> </td>
						</tr>
						<tr class="trInformacion">
							<td width="25%"><div align="left"><b>T&iacute;tulo Cita :</b></div></td>
							<td width="75%"><b>
						    <input name="txt_mensajeCalendario" type="text" size="50" value="<?= $tituloMensaje?>">
							</b></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="2"><b>Descripci&oacute;n:</b></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="2"><? $editor->crearEditor();?></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="2"><p align="left"><input name="chk_cartelera" type="checkbox" id="chk_cartelera" value="checkbox" <?= $estadoCartelera?>>
							  Publicar Cita en Cartelera</p></td>
						</tr>
					<tr class="trInformacion">
					 <td colspan="2" valign="top">

                     <?
                     //INICIO NO MOSTRAR SELECCION GRUPOS PARA MOD MSJ CREADO EN UN PREVIO
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
            	       	$sql="SELECT mei_relcalvirgru.idprevio, mei_relcalvirgru.idactividad FROM mei_relcalvirgru WHERE mei_relcalvirgru.idcalendario =".$_GET['idCalendario'];
					else
            	       	$sql="SELECT mei_relcalgru.idprevio, mei_relcalgru.idactividad FROM mei_relcalgru WHERE mei_relcalgru.idcalendario =".$_GET['idCalendario'];
						$resultado=$baseDatos->ConsultarBD($sql);
              			list ($idPrevio, $idActividad) = mysql_fetch_row($resultado);
              			$a=3;


              		 if (empty($idPrevio) && empty($idActividad))
              		 {

                     ?>


					 <table width="100%" bgcolor="#DDDDDD" class="tablaPrincipal">
                            <tr>
                              <td colspan="4"><b>Destino:</b> </td>
                            </tr>
                            <tr>
                              <td><input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar()" <?= $destinoTodos?> id="radioT" ></td>
                              <td>Todos mis Grupos </td>
                              <td><input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()" <?= $destinoSeleccionados?> checked id="radioS"></td>
                              <td>Grupos Seleccionados </td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td><table width="100%" class="tablaPrincipal">
                                  <?

			$contChk=0;
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria
						WHERE mei_virmateria.idvirmateria
							IN (
								SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo
										WHERE mei_virgrupo.idvirgrupo
											IN (
												SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru
														WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
												)
								) GROUP BY (mei_virmateria.idvirmateria)";
			}
			else
			{
				$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia
						WHERE mei_materia.idmateria
							IN (
								SELECT mei_grupo.idmateria FROM mei_grupo
										WHERE mei_grupo.idgrupo
											IN (
												SELECT mei_relusugru.idgrupo FROM mei_relusugru
														WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
												)
								) GROUP BY (mei_materia.idmateria)";
			}

			$materias=$baseDatos->ConsultarBD($sql);

			while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
			{
		?>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td><b><?= $materiaNombre?></b></td>
                                  </tr>
                                  <?
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo
							WHERE mei_virgrupo.idvirgrupo
								IN (
									SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru
											WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
									) AND mei_virgrupo.idvirmateria =".$materiaCodigo;
			}
			else
			{
				$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo
							WHERE mei_grupo.idgrupo
								IN (
									SELECT mei_relusugru.idgrupo FROM mei_relusugru
											WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
									) AND mei_grupo.idmateria =".$materiaCodigo;
			}

				$grupos=$baseDatos->ConsultarBD($sql);

				while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
				{
					if(sizeof($listaGrupos) > 0)
					{
						if(in_array($grupoCodigo,$listaGrupos))
						{
							$checkedGrupo="checked";
						}
						else
						{
							$checkedGrupo=" ";
						}
				}
		?>
                                  <tr>
                                    <td width="26">&nbsp;</td>
                                    <td><input id="grupo" name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>" <?= $checkedGrupo?> <?= $gruposCheck?>>                                      <?= $grupoNombre?></td>
                                  </tr>


                                  <?
					$contChk++;
				}
			}

		?>
                              </table></td>
                            </tr>
                          </table>
                          </td>
					  </tr>

                        <?
                     }
                     else
                     {
                     	$idCalendario=$_GET['idCalendario'];
						if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
						{
							$sql="SELECT mei_virmateria.nombre FROM mei_virmateria
							WHERE mei_virmateria.idvirmateria
							IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo
								WHERE mei_virgrupo.idvirgrupo
							IN (SELECT mei_relcalvirgru.idvirgrupo FROM mei_relcalvirgru
								WHERE mei_relcalvirgru.idcalendario= ".$_GET['idCalendario']."
								)
								)" ;
						}
						else
						{
							$sql="SELECT mei_materia.nombre FROM mei_materia
							WHERE mei_materia.idmateria
							IN (SELECT mei_grupo.idmateria FROM mei_grupo
								WHERE mei_grupo.idgrupo
							IN (SELECT mei_relcalgru.idgrupo FROM mei_relcalgru
								WHERE mei_relcalgru.idcalendario= ".$_GET['idCalendario']."
								)
								)" ;
						}

						$materia=$baseDatos->ConsultarBD($sql);
						list($materiaNombre)=mysql_fetch_array($materia);

						if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
						{
							$sql="SELECT  mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
							IN (SELECT mei_relcalvirgru.idvirgrupo FROM mei_relcalvirgru WHERE mei_relcalvirgru.idcalendario=".$_GET['idCalendario'].")";
						}
						else
						{
							$sql="SELECT  mei_grupo.nombre FROM mei_grupo WHERE mei_grupo.idgrupo
							IN (SELECT mei_relcalgru.idgrupo FROM mei_relcalgru WHERE mei_relcalgru.idcalendario=".$_GET['idCalendario'].")";
						}
                        $grupo=$baseDatos->ConsultarBD($sql);
                       list($grupoNombre)=mysql_fetch_array($grupo);


						?>


                          <tr class="trInformacion">
									<td colspan="2"><p align="left">
									<input id="grupo" disabled checked
									name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>"
									<?= $checkedGrupo?> <?= $gruposCheck?>><b><? echo $materiaNombre ?> grupo <? echo $grupoNombre ?></b></td>
						  </tr>

                     	<?
                     }
                        //FINAL NO MOSTRAR SELECCION GRUPOS PARA MOD MSJ CREADO EN UN PREVIO
                        ?>



					<tr class="trInformacion">
						<td colspan="4"><div align="center">
						  <table width="269" border="0">
                            <tr>
                              <td width="25"><input type="hidden" name="hid_idCalendario" value="<?= $_GET['idCalendario']?>">
                                <input type="hidden" name="hid_contGrupo" value="<?= $contChk?>"></td>
                              <td width="89"><div align="right">
                                <input name="btn_actualizar" type="button" value="Modificar Cita" onClick="javascript:enviar()">
</div></td>
                              <td width="141"><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar()"></td>
                            </tr>
                          </table>
						</div></td>
					  </tr>
					  </form>

					</table>
				</td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
        </body>
	</html>
	<?
	}
	else
	{
		redireccionar("../calendario/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
	}

	}
else
	redireccionar('../login/');
?>
