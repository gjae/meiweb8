<?

	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');


if(comprobarSession())
	{

	$baseDatos= new BD();

			$p='1';

			if(!empty($_POST['hid_idCalendario']))
			{
				if($_POST['rad_grupo']=="todos")
				{
					$destino=0;
				}
				else if($_POST['rad_grupo']=="seleccionados")
				{
					$destino=1;
				}


				if(!comprobarEditor($_POST['edt_descripcionCalendario']))
				{
					redireccionar("modificarCalendarioMensaje.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&error=0x001&idCalendario=".$_POST['hid_idCalendario']);
				}

				if(empty($_POST['chk_cartelera']))
				{
					$cartelera=0;
				}
				else
				{
					$cartelera=1;
				}

				registrarBitacora(2,5,false);
				$_POST['txt_mensajeCalendario']=arreglarTilde($_POST['txt_mensajeCalendario']);

				$sql="UPDATE `mei_calendario`
						SET `mensaje` = '".base64_encode($_POST['txt_mensajeCalendario']."[$$$]".eliminarEspacios($_POST['edt_descripcionCalendario']))."',
							`fechamensaje` = '".$_POST['txt_fecha']."',
							`estado` = '0',
							`idusuario` = '".$_SESSION['idusuario']."',
							`fechacreacion` = '".date('Y-n-j')."' ,
							`cartelera` = '".$cartelera."' ,
							`destino` = '".$destino."'
						WHERE `idcalendario` =".$_POST['hid_idCalendario'];

				$consulta=$baseDatos->ConsultarBD($sql);


				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					$sql="SELECT mei_relcalvirgru.idprevio, mei_relcalvirgru.idactividad FROM mei_relcalvirgru WHERE mei_relcalvirgru.idcalendario=".$_POST['hid_idCalendario'];
				else
					$sql="SELECT mei_relcalgru.idprevio, mei_relcalgru.idactividad FROM mei_relcalgru WHERE mei_relcalgru.idcalendario=".$_POST['hid_idCalendario'];
				$resultado=$baseDatos->ConsultarBD($sql);
              	list ($idPrevio, $idActividad) = mysql_fetch_row($resultado);
              	if (!empty($idPrevio)) {
              		$sql="SELECT mei_evaprevio.fechaactivacion, mei_evaprevio.fechafinalizacion FROM mei_evaprevio
              			  WHERE mei_evaprevio.idprevio ='".$idPrevio."' ";
              		$consulta=$baseDatos->ConsultarBD($sql);
              		list($facti,$ffin)=mysql_fetch_array($consulta);
              		$hacti= substr($facti,8);	
              		$hfin= substr($ffin,8);
              		$arreglo=explode("-", $_POST['txt_fecha']);
              		if ($arreglo[1]<10) {
              			if (strlen($arreglo[1])<2){
              				$arreglo[1]='0'.$arreglo[1];
              			}
              			}
              		if ($arreglo[2]<10) {
              			if (strlen($arreglo[2])<2){
              				$arreglo[2]='0'.$arreglo[2];
              			}
              			}	
              		$fechaponer=$arreglo[0].$arreglo[1].$arreglo[2];
              		$factin=$fechaponer.$hacti;
              		$ffinn=$fechaponer.$hfin;

              		$sql="UPDATE `mei_evaprevio`
						  SET `fechaactivacion` = '".$factin."',
						  	  `fechafinalizacion` = '".$ffinn."'
					      	   WHERE `idprevio` = ".$idPrevio;	    

					$consulta=$baseDatos->ConsultarBD($sql);   

              	}

              	if (!empty($idActividad)) {
              		$sql="SELECT mei_actividad.fechaactivacion, mei_actividad.fechafinalizacion FROM mei_actividad
              			  WHERE mei_actividad.idactividad ='".$idActividad."' ";
              		$consulta=$baseDatos->ConsultarBD($sql);
              		list($facti,$ffin)=mysql_fetch_array($consulta);
              		$hacti= substr($facti,8);	
              		$hfin= substr($ffin,8);
              		$arreglo=explode("-", $_POST['txt_fecha']);
              		if ($arreglo[1]<10) {
              			if (strlen($arreglo[1])<2){
              				$arreglo[1]='0'.$arreglo[1];
              			}
              			}
              		if ($arreglo[2]<10) {
              			if (strlen($arreglo[2])<2){
              				$arreglo[2]='0'.$arreglo[2];
              			}
              			}	
              		$fechaponer=$arreglo[0].$arreglo[1].$arreglo[2];
              		$factin=$fechaponer.$hacti;
              		$ffinn=$fechaponer.$hfin;

              		$sql="UPDATE `mei_actividad`
						  SET `fechafinalizacion` = '".$ffinn."'
					      WHERE `idactividad` = ".$idActividad;	    

					$consulta=$baseDatos->ConsultarBD($sql);   

              	}

				if(empty($idPrevio) && empty($idActividad))
				{
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
						$sql="DELETE FROM `mei_relcalvirgru` WHERE `idcalendario` = ".$_POST['hid_idCalendario']." AND `idvirgrupo` IN
						(SELECT mei_relusuvirgru.idvirgrupo FROM `mei_relusuvirgru` WHERE mei_relusvirgru.idusuario=".$_SESSION['idusuario'].")";
					}
					else
					{
						$sql="DELETE FROM `mei_relcalgru` WHERE `idcalendario` = ".$_POST['hid_idCalendario']." AND `idgrupo` IN
						(SELECT mei_relusugru.idgrupo FROM `mei_relusugru` WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")";
					}
					$consulta=$baseDatos->ConsultarBD($sql);

					for($i=0;$i<$_POST['hid_contGrupo'];$i++)
					{
						if(!empty($_POST['chk_idgrupo'.$i]))
						{
							if(empty($valores))
								$valores="('".$_POST['hid_idCalendario']."', '".$_POST['chk_idgrupo'.$i]."')";
							else
								$valores.=" , ('".$_POST['hid_idCalendario']."', '".$_POST['chk_idgrupo'.$i]."')";
						}

					}
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
						$sql="INSERT INTO `mei_relcalvirgru` ( `idcalendario` , `idvirgrupo` ) VALUES ".$valores;
					else
						$sql="INSERT INTO `mei_relcalgru` ( `idcalendario` , `idgrupo` ) VALUES ".$valores;
					$baseDatos->ConsultarBD($sql);
                }
			}
			redireccionar("mostrarCalendarioMensaje.php?idmateria=".$_GET['cbo_materia']."&materia=".$_GET['materia']."&calendarioGeneral=1&cbo_materia=".$_GET['cbo_materia']."");

	}
else
	redireccionar('../login/');

?>
