<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/vistas.lib.php');
	include_once ('../calendario/FrmCalendario.class.php');
	include_once ('../menu1/Menu1.class.php');
	
	$baseDatos=new BD();
	if(comprobarSession())
	{
		$idmateria=$_GET["idmateria"];
		$materia=$_GET['materia'];

		if ($_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==5)
		{
			$codigoLanzador=$_GET['codigoLanzador'];
			$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.titulo,mei_lanzador.descripcion,
				mei_lanzador.tamano,mei_lanzador.fechacarga,mei_lanzador.lanzador,mei_lanzador.destino 
				FROM mei_lanzador WHERE mei_lanzador.idlanzador=".$_GET['codigoLanzador'];
			$resultado=$baseDatos->ConsultarBD($sql);
			list($codigoLanzador,$titulo,$descripcion,$tamano,$fechaCarga,$lanzador,$destino)=mysql_fetch_array($resultado);

			if($destino==0)
			{
				$destinoTodos="checked";
				$gruposCheck="checked";
			}
			else if($destino==1)
			{
				$destinoSeleccionados="checked";
				if ($_SESSION['idtipousuario']==5)
				{
					$sql="SELECT mei_rellanvirgru.idvirgrupo FROM mei_rellanvirgru WHERE mei_rellanvirgru.idlanzador =".$_GET['codigoLanzador']; 
				}
				else
				{
					$sql="SELECT mei_rellangru.idgrupo FROM mei_rellangru WHERE mei_rellangru.idlanzador =".$_GET['codigoLanzador']; 
				}
				$consulta=$baseDatos->ConsultarBD($sql);
				$cont=0;
				while(list($datoGrupo)=mysql_fetch_array($consulta))
				{
					$listaGrupos[$cont++]=$datoGrupo;
				}
			}
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
	<style type="text/css">
	<!--
		.Estilo1 {color: #FF0000}
	-->
	</style>
</head>
<body>
<script> 
	function chequear()
	{
		if(document.frm_modificarLanzador.chk_archivo.checked)
		{
			document.frm_modificarLanzador.fil_archivo.disabled=false;
			document.frm_modificarLanzador.txt_url.disabled=true;
			document.frm_modificarLanzador.chk_url.disabled=true;
		}
		else 
		{
			document.frm_modificarLanzador.fil_archivo.disabled=true;
			document.frm_modificarLanzador.chk_url.disabled=false;
		}
	}
	
	function chequear1()
	{
		if(document.frm_modificarLanzador.chk_url.checked)
		{
			document.frm_modificarLanzador.fil_archivo.disabled=true;
			document.frm_modificarLanzador.txt_url.disabled=false;
			document.frm_modificarLanzador.chk_archivo.disabled=true;
		}
		else 
		{
			document.frm_modificarLanzador.fil_archivo.disabled=true;
			document.frm_modificarLanzador.txt_url.disabled=true;
			document.frm_modificarLanzador.chk_archivo.disabled=false;
		}
	}

	function enviarCancelar()
	{
		location.replace("../lanzadores/index.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>");
	}

	function validar()
	{
		if(document.frm_modificarLanzador.txt_tituloLanzador.value!="" && document.frm_modificarLanzador.txt_descripcionLanzador.value!="")
			document.frm_modificarLanzador.submit();
		else
			alert ("Debe llenar completamente la información solicitada");
	}

	function chk_habilitar()
	{
		for(i=0;i<document.frm_modificarLanzador.elements.length;i++)
		{
			if(document.frm_modificarLanzador.elements[i].id=="grupo")
				document.frm_modificarLanzador.elements[i].checked=true;
		}
	}

	function chk_deshabilitar()
	{
		for(i=0;i<document.frm_modificarLanzador.elements.length;i++)
		{
			if(document.frm_modificarLanzador.elements[i].id=="grupo")
				document.frm_modificarLanzador.elements[i].checked=false;
		}
	}
 
	function chk_click ()
	{
		var radioTodos=document.getElementById('radioT');
		var radioSelec=document.getElementById('radioS');	
		radioTodos.checked=false;
		radioSelec.checked=true;	
	}
</script> 
<table class="tablaPrincipal">
	<tr valign="top">
		<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
		<td class="tablaEspacio">&nbsp;</td>
		<td>
                <table class="tablaMarquesina" >
                    <tr>
        <?	 				
                        if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
                            $sql= "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
                        else
                            $sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
                        $resul = $baseDatos->ConsultarBD($sql);
                        list($nom) = mysql_fetch_row($resul); 
        ?>	  
                        <td class="BloqueClaroB">
                            <a href="../scripts/" class="link">Inicio</a><a> -> </a>
                            <a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">
                                <?=ucwords(strtolower($nom))?></a>
                            <a href="../lanzadores/index.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"> -> Lanzadores </a>
                            <a> -> Modificar Lanzador </a> 
                        </td>
                    </tr>
                </table><br>
        	<table class="tablaGeneral">
			<form name="frm_modificarLanzador" method="post" action="guardarModificacion.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>&codigoLanzador=<?=$codigoLanzador?>" enctype="multipart/form-data">
				<tr>
					<td colspan="3" class="trTitulo"><img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar Lanzador </td>
				</tr>
<?
			if($_GET['error'] == "0x001")
			{
?>
				<tr class="trInformacion">
					<td colspan="3"><img src="imagenes/error.gif" width="17" height="18"><SPAN class=Estilo1>
						Error: No se pudo cargar el archivo, excede el tama&ntilde;o permitido por el servidor. 
						Consulte con el Administrador del sistema </SPAN></td>
				</tr>
<?
			}
?>
				<tr class="trInformacion">
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr class="trInformacion">
					<td colspan="2"><b>&nbsp;T&iacute;tulo Lanzador:</b></td>
					<td width="84%"><input name="txt_tituloLanzador" type="text" value="<?=$titulo?>" size="80" maxlength="80"></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="2"><b>&nbsp;Descripci&oacute;n lanzador:</b></td>
					<td><input name="txt_descripcionLanzador" type="text" value="<?=$descripcion?>" size="80" maxlength="80"></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="4"><b>
						<input name="chk_archivo" type="checkbox"  onClick="chequear();" value="1">&nbsp;Reemplazar Archivo</b>&nbsp;</td>
				</tr>
				<tr class="trInformacion">
					<td width="5%"><b>&nbsp;</b></td>
					<td colspan="3"><b>Nuevo Archivo:&nbsp;&nbsp;</b><input name="fil_archivo" type="file" disabled></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="4"><b>
						<input name="chk_url" type="checkbox"  onClick="chequear1();" value="0">&nbsp;Cambiar URL </b>&nbsp;</td>
					</tr>
					<tr class="trInformacion">
					<td width="5%"><b>&nbsp;</b></td>
					<td colspan="3"><b>Nueva URL:&nbsp;&nbsp;</b><input name="txt_url" type="text" disabled value="http://"></td>
					</tr>
                	<tr class="trInformacion">
						<td colspan="4" valign="top">
							<table class="tablaPrincipal" width="100%" bgcolor="#DDDDDD">
								<tr>
									<td colspan="5"><b>Destino:</b></td>
								</tr>
								<tr>
                                	<td height="40" width="3%">
                                    	<input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar()"<?=$destinoTodos?> id="radioT"></td>
									<td width="22%">Todos mis Grupos </td>
									<td width="3%">
                                    	<input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()" <?=$destinoSeleccionados?> id="radioS"></td>
									<td colspan="2" width="72%">Grupos Seleccionados</td>
								</tr>
<?
						$contChk=0;
						if ($_SESSION['idtipousuario']==5)
						{
							$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
								IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
								IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].")) 
								GROUP BY (mei_virmateria.idvirmateria)"; 
						}
						else
						{
							$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
								IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
								IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].")) 
								GROUP BY (mei_materia.idmateria)"; 
						}
								
						$materias=$baseDatos->ConsultarBD($sql);
						while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
						{
?>
								<tr>
									<td width="26" colspan="2">&nbsp;</td>
									<td colspan="2"><?= $materiaNombre?></td>
								</tr>
<?
							if ($_SESSION['idtipousuario']==5)
							{
								$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
									IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].") 
									AND mei_virgrupo.idvirmateria =".$materiaCodigo;  
							}
							else
							{
								$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
									IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].") 
									AND mei_grupo.idmateria =".$materiaCodigo;
							}
									
							$grupos=$baseDatos->ConsultarBD($sql);
							while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
							{
								if(sizeof($listaGrupos) > 0)
								{
									if(in_array($grupoCodigo,$listaGrupos)) 
										$checkedGrupo="checked";
									else
										$checkedGrupo=" ";
								}
?>
								<tr>
								  <td width="26" colspan="2">&nbsp;</td>
									<td width="33">
                                    	<input id="grupo" name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>" <?= $checkedGrupo?><?= $gruposCheck?>></td>
									<td width="280"><?= $grupoNombre?></td>
								</tr>
<?
								$contChk++;
							}
						}  
?>
								<input type="hidden" name="hid_contGrupo" value="<?=$contChk?>">
							</table>
						</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center"><input type="button" name="btn_modificarLanzador" value="Modificar" onClick="javascript:validar()">
							<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()">
							</div></td>
					</tr>
					</form>
				</table>
</td>
            <td class="tablaEspacio">&nbsp;</td>
			<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>            
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<? 
		}
		else
		{
			redireccionar('../login/');					
		}
	}
	else
	{
		redireccionar('../login/');					
	}
?>