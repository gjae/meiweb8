<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/vistas.lib.php');
	include_once ('../calendario/FrmCalendario.class.php');

	$baseDatos=new BD();
	if(comprobarSession())
	{
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{		
			if($_POST['rad_lanzador']==1)
			{
				if($_FILES['fil_archivo'])
				{
					if (!is_uploaded_file($_FILES['fil_archivo']['tmp_name']))
							redireccionar("../lanzadores/agregarLanzador.php?error=0x001&titulo=".$_POST['txt_nombreLanzador']."&lanzador=".$_FILES['fil_archivo']."&descripcion=".$_POST['txt_descripcionLanzador']."&idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']); 
					else
					{
						if($_POST['rad_grupo']=="todos")
							$destino=0;
						else if($_POST['rad_grupo']=="seleccionados")
							$destino=1;
						$titulo=$_POST['txt_nombreLanzador'];
						$descripcionLanzador=$_POST['txt_descripcionLanzador'];
						$lanzador = $_FILES['fil_archivo']['name']; 
						$tamanoLanzador = $_FILES['fil_archivo']['size']; 
						
						$sql="SELECT MAX(mei_lanzador.idlanzador) FROM mei_lanzador";
						$consulta=$baseDatos->ConsultarBD($sql);
						list($maxId)=mysql_fetch_array($consulta);
						
						if(empty($maxId))
							$maxId=1;
						else
							$maxId=$maxId+1;
							
						$nombreLanzador="id".$maxId.$lanzador;
						if (cargarArchivo("fil_archivo",corregirCaracteresURL($nombreLanzador),"../../datosMEIWEB/archivosLanzador/"))
						{
							registrarBitacora(10,31,false);
							$sql="INSERT INTO mei_lanzador (idlanzador,idusuario,titulo,descripcion,tamano,fechacarga,lanzador,ubicacion,destino) 
								VALUES('', '".$_SESSION['idusuario']."','$titulo','$descripcionLanzador','$tamanoLanzador','".date("Y-n-d")."','".corregirCaracteresURL($nombreLanzador)."','../../datosMEIWEB/archivosLanzador/".corregirCaracteresURL($nombreLanzador)."', ".$destino." )";
							$resultado=$baseDatos->ConsultarBD($sql);
							$idlanzador = $baseDatos->InsertIdBD();
							$sql="SELECT MAX(mei_lanzador.idlanzador) FROM mei_lanzador";
							$consulta=$baseDatos->ConsultarBD($sql);
							list($maxId)=mysql_fetch_array($consulta);
							
							if ($destino==0)
							{
								if ($_SESSION['idtipousuario']==5)
								{
									$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
										IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
										IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")) 
										GROUP BY (mei_virmateria.idvirmateria)"; 
								}
								else
								{
									$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
										IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
										IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")) 
										GROUP BY (mei_materia.idmateria)";
								}
								$materias=$baseDatos->ConsultarBD($sql);
								while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
								{
									if ($_SESSION['idtipousuario']==5)
									{
										$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
											IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].") 
											AND mei_virgrupo.idvirmateria =".$materiaCodigo;
									}
									else
									{
										$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
											IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].") 
											AND mei_grupo.idmateria =".$materiaCodigo;
									}
									$grupos=$baseDatos->ConsultarBD($sql);
									while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
									{
										if ($_SESSION['idtipousuario']==5)
										{
											$sql="INSERT INTO mei_rellanvirgru ( idlanzador,idvirgrupo) VALUES ('".$idlanzador."','".$grupoCodigo."')";
										}
										else
										{
											$sql="INSERT INTO mei_rellangru ( idlanzador,idgrupo) VALUES ('".$idlanzador."','".$grupoCodigo."')";
										}
										$baseDatos->ConsultarBD($sql);
									}
								}
							}
							else if($destino==1)
							{
								for($i=0;$i<$_POST['hid_contGrupo'];$i++) 
								{ 
									if(!empty($_POST['chk_idgrupo'.$i]))
									{ 
										if ($_SESSION['idtipousuario']==5)
										{
											$sql="INSERT INTO mei_rellanvirgru ( idlanzador,idvirgrupo) VALUES ('".$idlanzador."','".$_POST['chk_idgrupo'.$i]."')";
											$baseDatos->ConsultarBD($sql);
										}
										else
										{
											$sql="INSERT INTO mei_rellangru (idlanzador,idgrupo) VALUES ('".$idlanzador."','".$_POST['chk_idgrupo'.$i]."')";
											$baseDatos->ConsultarBD($sql);
										}
									}					
								}  
							}
						}
						else
						{
							redireccionar("../lanzadores/agregarLanzador.php?error=0x001&titulo=".$_POST['txt_nombreLanzador']."&lanzador=".$_FILES['fil_archivo']."&descripcion=".$_POST['txt_descripcionLanzador']."&idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']); 
						}
					}
				}
			}
			if($_POST['rad_lanzador']==2)
			{
				if($_POST['rad_grupo']=="todos")
					$destino=0;			
				else if($_POST['rad_grupo']=="seleccionados")
					$destino=1;
				$titulo=$_POST['txt_nombreLanzador'];
				$descripcionLanzador=$_POST['txt_descripcionLanzador'];
				$lanzador = $_POST['txt_url'];
				
				$sql="INSERT INTO mei_lanzador (idlanzador,idusuario,titulo,descripcion,tamano,fechacarga,lanzador,ubicacion,destino,tipo) 
					VALUES('', '".$_SESSION['idusuario']."','".$titulo."','".$descripcionLanzador."',0,'".date("Y-n-d")."','".$lanzador."','".$lanzador."','$destino','1') ";
				$resultado=$baseDatos->ConsultarBD($sql);
				$idlanzador = $baseDatos->InsertIdBD();
				$sql="SELECT MAX(mei_lanzador.idlanzador) FROM mei_lanzador";
				$consulta=$baseDatos->ConsultarBD($sql);
				list($maxId)=mysql_fetch_array($consulta);
					
				if ($destino==0)
				{
					if ($_SESSION['idtipousuario']==5)
					{
						$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
							IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
							IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")) 
							GROUP BY (mei_virmateria.idvirmateria)"; 
					}
					else
					{
						$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
							IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
							IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")) 
							GROUP BY (mei_materia.idmateria)";
					}
					$materias=$baseDatos->ConsultarBD($sql);
					while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
					{
						if ($_SESSION['idtipousuario']==5)
						{
							$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
								IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].") 
								AND mei_virgrupo.idvirmateria =".$materiaCodigo;
						}
						else
						{
							$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
								IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].") 
								AND mei_grupo.idmateria =".$materiaCodigo;
						}
						$grupos=$baseDatos->ConsultarBD($sql);
						while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
						{
							if ($_SESSION['idtipousuario']==5)
							{
								$sql="INSERT INTO mei_rellanvirgru ( idlanzador,idvirgrupo) VALUES ('".$idlanzador."','".$grupoCodigo."')";
							}
							else
							{
								$sql="INSERT INTO mei_rellangru ( idlanzador,idgrupo) VALUES ('".$idlanzador."','".$grupoCodigo."')";
							}
							$baseDatos->ConsultarBD($sql);
						}
					}
				}
				else if($destino==1)
				{
					for($i=0;$i<$_POST['hid_contGrupo'];$i++) 
					{ 
						if(!empty($_POST['chk_idgrupo'.$i]))
						{ 
							if ($_SESSION['idtipousuario']==5)
							{
								$sql="INSERT INTO mei_rellanvirgru ( idlanzador,idvirgrupo) VALUES ('".$idlanzador."','".$_POST['chk_idgrupo'.$i]."')";
								$baseDatos->ConsultarBD($sql);
							}
							else
							{
								$sql="INSERT INTO mei_rellangru (idlanzador,idgrupo) VALUES ('".$idlanzador."','".$_POST['chk_idgrupo'.$i]."')";
								$baseDatos->ConsultarBD($sql);
							}
						}					
					}  
				}
			}
			redireccionar('../lanzadores/index.php?idmateria='.$_GET['idmateria'].'&materia='.$_GET['materia']);
		}
		else
		{
			redireccionar('../login/');
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>