<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../menu1/Menu1.class.php');		
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/vistas.lib.php');
	include_once ('../calendario/FrmCalendario.class.php');

	$baseDatos=new BD();
	if(comprobarSession())
	{
		$idmateria=$_GET['idmateria'];		
		$materia=$_GET['materia'];
		if ($_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==5)
		{	
			$sql="SELECT mei_directorio.directorio,mei_directorio.ubicacion,mei_directorio.nombre, mei_directorio.destino FROM mei_directorio 
				WHERE mei_directorio.iddirectorio=".$_GET['idDirectorio'];
					
			$resultado=$baseDatos->ConsultarBD($sql);
			list($directorio,$ubicacion,$nombre,$destino)=mysql_fetch_array($resultado);
			if($destino==0)
			{
				$destinoTodos="checked";
				$gruposCheck="checked";
			}
			else if($destino==1)
			{
				$destinoSeleccionados="checked";
				if ($_SESSION['idtipousuario']==5)
					$sql="SELECT mei_reldirvirgru.idvirgrupo FROM mei_reldirvirgru WHERE mei_reldirvirgru.iddirectorio =".$_GET['idDirectorio']; 
				else
					$sql="SELECT mei_reldirgru.idgrupo FROM mei_reldirgru WHERE mei_reldirgru.iddirectorio =".$_GET['idDirectorio']; 
				$consulta=$baseDatos->ConsultarBD($sql);
				$cont=0;
				while(list($datoGrupo)=mysql_fetch_array($consulta))
				{
					$listaGrupos[$cont++]=$datoGrupo;
				}
			}
			
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		</head>
<body>
	<script> 
	function enviarCancelar(idDirectorio)
	{
		location.replace("../lanzadores/editarDirectorio.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>&idDirectorio="+idDirectorio);
	}
			
	function validar()
	{
		var patron=new RegExp("^([\_\s a-zA-Z0-9])+$");	
		estado=patron.test(document.frm_modificarDirectorio.txt_directorio.value);
		if(estado==false)
		{
			alert ("Debe asignarle un nombre válido al nuevo Directorio. Use solo caracteres alfanumúricos");
		}
		else
		{
			document.frm_modificarDirectorio.submit();
		}
	}
	function chk_habilitar()
	{
		for(i=0;i<document.frm_modificarDirectorio.elements.length;i++)
		{
			if(document.frm_modificarDirectorio.elements[i].id=="grupo")
				document.frm_modificarDirectorio.elements[i].checked=true;
		}
	}

	function chk_deshabilitar()
	{
		for(i=0;i<document.frm_modificarDirectorio.elements.length;i++)
		{
			if(document.frm_modificarDirectorio.elements[i].id=="grupo")
				document.frm_modificarDirectorio.elements[i].checked=false;
		}
	}
 
	function chk_click ()
	{
		var radioTodos=document.getElementById('radioT');
		var radioSelec=document.getElementById('radioS');	
		radioTodos.checked=false;
		radioSelec.checked=true;	
	}
	</script> 

	<table class="tablaPrincipal">
		<tr valign="top">
			<td class="tablaEspacio"></td>
			<td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']); ?>	</td>
			<td class="tablaEspacio"></td>
			<td>
                  <table class="tablaMarquesina" >
                    <tr>
            <?	 				
                    if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
                        $sql123 = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
                    else
                        $sql123 = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
                    $resul = $baseDatos->ConsultarBD($sql123);
                    list($nom) = mysql_fetch_row($resul); ?>	  
                      <td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nom))?></a><a> -> </a><a href="editarDirectorio.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idDirectorio=<?=$_GET['idDirectorio']?>" class="link">Directorios</a><a> -> </a>Modificar Directorio</td>
                    </tr>
                  </table><br>
				<table class="tablaGeneral">
				<form name="frm_modificarDirectorio" method="post" action="guardarModificacionDirectorio.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>&idDirectorio=<?=$_GET['idDirectorio']?>">
					<tr>
						<td colspan="2" class="trTitulo">
                        	<img src="imagenes/modificar.gif" width="16" height="16"> Modificar Directorio </td>
					</tr>
					<tr class="trInformacion">
						<td width="25%">Nombre Directorio: </td>
						<td width="75%"><input name="txt_directorio" type="text" value="<?=$nombre?>" size="80" maxlength="80"></td>
					</tr>
					<tr class="trInformacion">
						<td>&nbsp;</td>
						<td colspan="2">&nbsp;</td>
					</tr>
<tr class="trInformacion">
						<td colspan="4" valign="top">
							<table class="tablaPrincipal" width="100%" bgcolor="#DDDDDD">
								<tr>
									<td colspan="5"><b>Destino:</b></td>
								</tr>
								<tr>
                                	<td height="40" width="3%">
                                    	<input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar()"<?=$destinoTodos?> id="radioT"></td>
									<td width="22%">Todos mis Grupos </td>
									<td width="3%">
                                    	<input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()" <?=$destinoSeleccionados?> id="radioS"></td>
									<td colspan="2" width="72%">Grupos Seleccionados</td>
								</tr>
<?
						$contChk=0;
						if ($_SESSION['idtipousuario']==5)
						{
							$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
								IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
								IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].")) 
								GROUP BY (mei_virmateria.idvirmateria)"; 
						}
						else
						{
							$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
								IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
								IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].")) 
								GROUP BY (mei_materia.idmateria)"; 
						}
								
						$materias=$baseDatos->ConsultarBD($sql);
						while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
						{
?>
								<tr>
									<td width="26" colspan="2">&nbsp;</td>
									<td colspan="2"><?= $materiaNombre?></td>
								</tr>
<?
							if ($_SESSION['idtipousuario']==5)
							{
								$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
									IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].") 
									AND mei_virgrupo.idvirmateria =".$materiaCodigo;  
							}
							else
							{
								$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
									IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].") 
									AND mei_grupo.idmateria =".$materiaCodigo;
							}
									
							$grupos=$baseDatos->ConsultarBD($sql);
							while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
							{
								if(sizeof($listaGrupos) > 0)
								{
									if(in_array($grupoCodigo,$listaGrupos)) 
										$checkedGrupo="checked";
									else
										$checkedGrupo=" ";
								}
?>
								<tr>
								  <td width="26" colspan="2">&nbsp;</td>
									<td width="33">
                                    	<input id="grupo" name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>" <?= $checkedGrupo?><?= $gruposCheck?>></td>
									<td width="280"><?= $grupoNombre?></td>
								</tr>
<?
								$contChk++;
							}
						}  
?>
								<input type="hidden" name="hid_contGrupo" value="<?=$contChk?>">
							</table>
						</td>
					</tr>
                    <tr class="trInformacion">
						<td colspan="2" align="center">
                        	<input type="button" name="btn_modificarArchivo" value="Modificar Directorio" onClick="javascript:validar()">
							<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar('<?=$_GET['idDirectorio']?>')">
						</td>
					</tr>
				</form>
				</table>
			</td>
			<td class="tablaEspacio">&nbsp;</td>                
			<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']);?></td>
			<td class="tablaEspacioDerecho">&nbsp;</td>         
		</tr>
	</table>
</body>
</html>
<? 
		}
		else
		{
			redireccionar('../login/');					
		}
	}
	else
	{
		redireccionar('../login/');					
	}
?>
