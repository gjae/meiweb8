<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../calendario/FrmCalendario.class.php');

	$baseDatos=new BD();
	if(comprobarSession())
	{	
		$idmateria=$_GET["idmateria"];		
		$materia=$_GET['materia'];
		
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{
			if($_FILES['fil_archivo'])
			{
				if (!is_uploaded_file($_FILES['fil_archivo']['tmp_name']))
				{
					redireccionar("../lanzadores/index.php?idmateria=".$idmateria."&materia=".$materia."&error=0x001");
				}
				else
				{
					$titulo=$_POST['txt_nombreLanzador'];
					$descripcionArchivo=$_POST['txt_descripcionLanzador'];
					$lanzador = $_FILES['fil_archivo']['name']; 
					$tamanoArchivo = $_FILES['fil_archivo']['size']; 
					$sql="SELECT MAX(mei_archivos.idarchivo) FROM mei_archivos";
					$consulta=$baseDatos->ConsultarBD($sql);
					list($maxId)=mysql_fetch_array($consulta);
					
					if(empty($maxId))
						$maxId=1;
					else
						$maxId=$maxId+1;
						
					if (is_dir("../../datosMEIWEB/archivosLanzador/".$_POST['txt_directorio']))
					{
						redireccionar("../lanzadores/agregarLanzador.php?idmateria=".$idmateria."&materia=".$materia."&error=0x002&titulo=".$titulo."&descripcion=".$descripcionArchivo."&directorio=".$_POST['txt_directorio']);
					}
					else
					{
						if (mkdir("../../datosMEIWEB/archivosLanzador/".$_POST['txt_directorio']))
						{
							$nombreArchivo="id".$maxId.$lanzador;
							if (cargarArchivo("fil_archivo",corregirCaracteresURL($nombreArchivo),"../../datosMEIWEB/archivosLanzador/".$_POST['txt_directorio']."/"))
							{
								if($_POST['rad_grupo']=="todos")
									$destino=0;
								else if($_POST['rad_grupo']=="seleccionados")
									$destino=1;
								registrarBitacora(10,31,false);
								$sql="INSERT INTO mei_directorio (iddirectorio,idusuario,directorio,ubicacion,nombre,destino)
									VALUES ('','".$_SESSION['idusuario']."','".$_POST['txt_directorio']."','../../datosMEIWEB/archivosLanzador/','".$_POST['txt_directorio']."','".$destino."')";
								$resultado=$baseDatos->ConsultarBD($sql);
								$idDirectorio=$baseDatos->InsertIdBD();
									
								if ($destino==0)
								{
									if ($_SESSION['idtipousuario']==5)
									{
										$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
										IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
										IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")) 
										GROUP BY (mei_virmateria.idvirmateria)"; 
									}
									else if ($_SESSION['idtipousuario']==2)
									{
										$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
										IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
										IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")) 
										GROUP BY (mei_materia.idmateria)"; 
									}
									$materias=$baseDatos->ConsultarBD($sql);
									while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
									{
										if ($_SESSION['idtipousuario']==5)
										{
											$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
											IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].") 
											AND mei_virgrupo.idvirmateria =".$materiaCodigo;
										}
										else if ($_SESSION['idtipousuario']==2)
										{
											$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
											IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].") 
											AND mei_grupo.idmateria =".$materiaCodigo;
										}
										$grupos=$baseDatos->ConsultarBD($sql);
										while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
										{
											if ($_SESSION['idtipousuario']==5)
												$sql="INSERT INTO mei_reldirvirgru (iddirectorio, idvirgrupo) VALUES ('".$idDirectorio."','".$grupoCodigo."')";
											else if ($_SESSION['idtipousuario']==2)
												$sql="INSERT INTO mei_reldirgru (iddirectorio, idgrupo) VALUES ('".$idDirectorio."','".$grupoCodigo."')";
											$baseDatos->ConsultarBD($sql);
										}
									}
								}
								else if($destino==1)
								{
									for($i=0;$i<$_POST['hid_contGrupo'];$i++) 
									{ 
										if(!empty($_POST['chk_idgrupo'.$i]))
										{ 
											if ($_SESSION['idtipousuario']==5)
												$sql="INSERT INTO mei_reldirvirgru (iddirectorio,idvirgrupo) VALUES ('".$idDirectorio."','".$_POST['chk_idgrupo'.$i]."')";
											else
												$sql="INSERT INTO mei_reldirgru (iddirectorio,idgrupo) VALUES ('".$idDirectorio."','".$_POST['chk_idgrupo'.$i]."')";
											$baseDatos->ConsultarBD($sql);
										}					
									}  
								}
								
								$sql="INSERT INTO mei_archivos (idarchivo,iddirectorio,titulo,descripcion,tamano,fechacarga,archivo) 
											VALUES('','".$idDirectorio."','".$titulo."','".$descripcionArchivo."','".$tamanoArchivo."','".date("Y-n-d")."','".corregirCaracteresURL($nombreArchivo)."')";
								$resultado=$baseDatos->ConsultarBD($sql);
							}
							else
							{
								redireccionar("../lanzadores/index.php?idmateria=".$idmateria."&materia=".$materia."&error=0x001"); 
							}
						}
						else
						{
							redireccionar("../lanzadores/agregarLanzador.php?idmateria=".$idmateria."&materia=".$materia."&error=0x002&titulo=".$titulo."&descripcion=".$descripcionArchivo."&directorio=".$_POST['txt_directorio']);
						}
					}
				}
			}
			redireccionar('../lanzadores/editarDirectorio.php?idmateria='.$idmateria.'&materia='.$materia.'&idDirectorio='.$idDirectorio);
		}
		else
		{
		redireccionar('../login/');					
		}
	}
	else
	{
		redireccionar('../login/');					
	}
?>