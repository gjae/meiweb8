<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once('../calendario/FrmCalendario.class.php');
	include_once ('../menu1/Menu1.class.php');	

	if(comprobarSession())
	{
		$idmateria=$_GET["idmateria"];		
		$materia=$_GET['materia'];
	
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{
				$sql="SELECT mei_archivos.idarchivo,mei_archivos.titulo,mei_archivos.descripcion,
						mei_archivos.tamano,mei_archivos.fechacarga,mei_archivos.archivo 
						FROM mei_archivos WHERE mei_archivos.idarchivo=".$_GET['idArchivo'];
						
				$resultado=$baseDatos->ConsultarBD($sql);
				list($idArchivo,$titulo,$descripcion,$tamano,$fecha,$archivo)=mysql_fetch_array($resultado);
				
				$sql="SELECT mei_directorio.iddirectorio,mei_directorio.directorio FROM mei_directorio 
						WHERE mei_directorio.iddirectorio =(SELECT mei_archivos. iddirectorio
						FROM mei_archivos WHERE mei_archivos.idarchivo=".$_GET['idArchivo'].")";
				
				$consultaDirectorio=$baseDatos->ConsultarBD($sql);
				list($idDirectorioActual,$nombreDirectorio)=mysql_fetch_array($consultaDirectorio);
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
        </style>
		</head>
		<body>
			<script> 
				
			function chequear()
			{
				if(document.frm_modificarArchivo.chk_archivo.checked)
				{
					document.frm_modificarArchivo.fil_archivo.disabled=false;
				}
				else
				{
					document.frm_modificarArchivo.fil_archivo.disabled=true;
				}
			}
			
			function enviarCancelar(idDirectorio)
			{
				location.replace("../lanzadores/editarDirectorio.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>&idDirectorio="+idDirectorio);
			}
				
			function validar()
			{
			
				if(document.frm_modificarArchivo.txt_titulo.value!="" && document.frm_modificarArchivo.txt_descripcion.value!="")
				document.frm_modificarArchivo.submit();
				else
				alert ("Debe llenar completamente la información solicitada");
			}
		</script> 

		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
                  <table class="tablaMarquesina" >
                    <tr>
            <?	 				
                    if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
                        $sql123 = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
                    else
                        $sql123 = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
                    $resul = $baseDatos->ConsultarBD($sql123);
                    list($nom) = mysql_fetch_row($resul); ?>	  
                      <td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nom))?></a><a> -> </a><a href="editarDirectorio.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idDirectorio=<?=$_GET['idDirectorio']?>" class="link">Directorios</a><a> -> </a>Modificar Archivo</td>
                    </tr>
                  </table><br>
					<table class="tablaGeneral">
					<form action="guardarModificacionArchivo.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>&idArchivo=<?=$_GET['idArchivo']?>&idDirectorio=<?=$_GET['idDirectorio']?>" method="post" enctype="multipart/form-data" name="frm_modificarArchivo">
						<tr>
							<td colspan="2" class="trTitulo"><img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar Archivo </td>
						</tr>
						<?
						if($_GET['error'] == "0x001")
						{
						?>
						<tr class="trInformacion">
						  <td colspan="2"><img src="imagenes/error.gif" width="17" height="18"> <SPAN class=Estilo1>Error: No se pudo cargar el archivo, excede el tama&ntilde;o permitido por el servidor. Consulte con el Administrador del sistema </SPAN></td>
					  </tr>
					  <?
					  }
					  ?>
						<tr class="trInformacion">
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
					  </tr>
						<tr class="trInformacion">
							<td width="18%">&nbsp;<strong>T&iacute;tulo Archivo: </strong></td>
							<td><input name="txt_titulo" type="text" value="<?=$titulo?>" size="80" maxlength="80"></td>
						</tr>
						
						<tr class="trInformacion">
							<td>&nbsp;<strong>Descripci&oacute;n Archivo:</strong></td>
							<td><input name="txt_descripcion" type="text" value="<?=$descripcion?>" size="80" maxlength="80"></td>
						</tr>
						<tr class="trInformacion">
						  <td>&nbsp;<strong>Directorio Actual:</strong></td>
						  <td ><?= $nombreDirectorio?> </td>
						  <?
						if ($_SESSION['idtipousuario']==5)  
						{
							$sql="SELECT mei_directorio.iddirectorio, mei_directorio.directorio, mei_directorio.ubicacion 
								FROM mei_directorio WHERE mei_directorio.iddirectorio <> '".$idDirectorioActual."' AND mei_directorio.idusuario IN 
								(SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo IN
								(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."))";
						}
						else
						{
							$sql="SELECT mei_directorio.iddirectorio, mei_directorio.directorio, mei_directorio.ubicacion 
								FROM mei_directorio WHERE mei_directorio.iddirectorio <> '".$idDirectorioActual."' AND mei_directorio.idusuario IN 
								(SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idgrupo IN
								(SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']."))";
						}
	
								$directorios=$baseDatos->ConsultarBD($sql);
								
								
						 ?>
					  </tr>
						
						<tr class="trInformacion">
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
					  </tr>
					  <?
					  if(mysql_num_rows($directorios) > 0)
						{
						  
						  ?>
					    <tr class="trInformacion">
						  <td>&nbsp;<strong>Mover al Directorio:</strong> </td>
					      <td>
					        <select name="cbo_destino" id="cbo_destino">
								<option value="0"></option>
							<?
								while(list($idDirectorio,$directorio,$ubicacion)=mysql_fetch_array($directorios))
								{
							?>
								<option value="<?= $idDirectorioActual.";".$idDirectorio?>"><?= $directorio?></option>
							<?
								}
							?>
				         	</select></td>
				      </tr>
						  <?
						 		}
							?>				  
						
						  <tr class="trInformacion">
						    <td colspan="2"><strong>
						      <input name="chk_archivo" type="checkbox"  onClick="chequear();" value="1">
					        &nbsp;Reemplazar Archivo</strong>&nbsp;</td>
		              </tr>
					      <tr class="trInformacion">
					        <td><strong>&nbsp;Nuevo Archivo: </strong></td>
			                <td><input name="fil_archivo" type="file" disabled></td>
				      </tr>
						<tr class="trInformacion">
							<td colspan="2"><div align="center"><input type="button" name="btn_modificarArchivo" value="Modificar Archivo" onClick="javascript:validar()">
							  <input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar('<?=$_GET['idDirectorio']?>')">
							</div></td>
						</tr>
					</form>
					</table>
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
		</body>
		</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
?>
