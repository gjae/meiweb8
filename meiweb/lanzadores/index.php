<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once ('../menu1/Menu1.class.php');	
	
	
  	$baseDatos=new BD();
	$editor=new FCKeditor('edt_lanzadores' , '100%' , '100%' , 'barraBasica' , '' ) ;
	
	if(comprobarSession())
	{
		$idmateria=$_GET["idmateria"];		
		$materia=$_GET['materia'];
	
		$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=10";
		$resultado=$baseDatos->ConsultarBD($sql);
		list($estado)=mysql_fetch_array($resultado);
		if($estado==1)
		{
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_lanzador.idlanzador, mei_lanzador.idusuario, mei_lanzador.titulo, mei_lanzador.descripcion, mei_lanzador.tamano,
						mei_lanzador.lanzador, mei_lanzador.ubicacion FROM mei_lanzador
						WHERE mei_lanzador.idlanzador IN (SELECT mei_rellanvirgru.idlanzador FROM mei_rellanvirgru
						WHERE mei_rellanvirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo 
						WHERE mei_virgrupo.idvirmateria =".$idmateria." AND mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo
						FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].")))";				
			}
			else
			{
				$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.idusuario,mei_lanzador.titulo,mei_lanzador.descripcion,mei_lanzador.tamano,
						mei_lanzador.lanzador,mei_lanzador.ubicacion FROM mei_lanzador 
						WHERE mei_lanzador.idlanzador IN (SELECT mei_rellangru.idlanzador FROM mei_rellangru 
						WHERE mei_rellangru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo 
						WHERE mei_grupo.idmateria =".$idmateria." AND mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo 
						FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']." )))";				
			}
				$resultado=$baseDatos->ConsultarBD($sql); //lanzadores del usuario
				$resultado1=$baseDatos->ConsultarBD($sql);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
	<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
        </style>
</head>
<body>
	<script> 
		function eliminarLanzador(codigoLanzador,lanzador)
		{
			if(confirm("¿Está seguro de eliminar este Lanzador?"))
			{
				location.replace("eliminarLanzador.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>&codigoLanzador="+codigoLanzador+"&lanzador="+lanzador);					
			}
		}
			
		function eliminar()
		{
			if(confirm("¿Está seguro de eliminar los Lanzadores seleccionados?"))
			{
				document.frm_lanzador.action="eliminarLanzador.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>";
				document.frm_lanzador.submit();					
			}
		}
			
		function chekear()
		{
			var todo=document.getElementById('todo');
			if (todo.checked==true)
			{
				for(i=0;i<document.frm_lanzador.elements.length;i++)
				{
					if(document.frm_lanzador.elements[i].id=="codigoLanzador")
						document.frm_lanzador.elements[i].checked=true;
				}
			}
			else
			{
				for(i=0;i<document.frm_lanzador.elements.length;i++)
				{
					if(document.frm_lanzador.elements[i].id=="codigoLanzador")
						document.frm_lanzador.elements[i].checked=false;
				}
			}
		}
	</script> 
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio"></td>
			<td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']);?></td>
            <td class="tablaEspacio"></td>
			<td>
                <table class="tablaMarquesina" >
                    <tr>
        <?	 				
                        if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
                            $sql= "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
                        else
                            $sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
                        $resul = $baseDatos->ConsultarBD($sql);
                        list($nom) = mysql_fetch_row($resul); 
        ?>	  
                        <td class="BloqueClaroB">
                            <a href="../scripts/" class="link">Inicio</a><a> -> </a>
                            <a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">
                                <?=ucwords(strtolower($nom))?></a><a> -> Lanzadores </a>
                        </td>
                    </tr>
                </table><br>
            	<table class="tablaGeneral" >
            	<form name="frm_lanzador" method="post">
					<tr>
						<td colspan="8" class="trTitulo"><img src="imagenes/lanzadores.gif" width="16" height="16" align="texttop"> Ver Lanzadores</td>
					</tr>
<?
					if($_GET['error'] == '0x001')
					{
?>
					<tr class="trAviso">
						<td colspan="8"><img src="imagenes/error.gif" width="17" height="18" align="texttop"><span class="Estilo1">
							Error: No se pudo cargar el archivo, excede el tamaño permitido por el servidor. Consulte con el Administrador del sistema</span></td>
					</tr>
<?
					}
				list($codigoLanzador,$autor,$titulo,$descripcion,$tamano,$lanzador,$ubicacion)=mysql_fetch_array($resultado);
				if(!empty($codigoLanzador)) //si no esta vacio
				{
					registrarBitacora(10,30,false);
?>
					<tr class="trSubTitulo">
<?
					if($_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==5)
					{
?>
						<td width="5%" class="trSubTitulo" align="center">&nbsp;&nbsp;&nbsp;
							<input id="todo" type="checkbox" name="chk_todoLanzador" onClick="javascript:chekear()" ></td>
						<td width="10%" class="trSubTitulo" ><div align="center">T&iacute;tulo</div></td>
<?
					}
					else
					{
?>
						<td class="trSubTitulo" colspan="2"><div align="center"> T&iacute;tulo</div></td>
<?
					}
?>
						<td width="11%" class="trSubTitulo"><div align="center">Tamaño</div></td>					
						<td width="21%" class="trSubTitulo"><div align="center">Lanzador</div></td>		
						<td width="21%" class="trSubTitulo"><div align="center">Descripci&oacute;n</div></td>					
<?
					if($_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==5)
					{
?>
						<td width="13%" class="trSubTitulo"><div align="center">Editar</div></td>						
<?
					}
?>
					</tr>
<?
					if ($_SESSION['idtipousuario']==5  || $_SESSION['idtipousuario']==6)
					{
						$sql="SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']." 
								AND mei_relusuvirgru.idvirgrupo IN (SELECT mei_rellanvirgru.idvirgrupo FROM mei_rellanvirgru)";
					}
					else
					{
						$sql="SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']." 
								AND mei_relusugru.idgrupo IN (SELECT mei_rellangru.idgrupo FROM mei_rellangru)";						
					}
					$consulta=$baseDatos->ConsultarBD($sql);
					list($usuario)=mysql_fetch_array($consulta);
					if(!empty($usuario))
					{
						$cont=0;
						while (list($codigoLanzador,$autor,$titulo,$descripcion,$tamano,$lanzador,$ubicacion)=mysql_fetch_array($resultado1))
						{
							$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario='".$autor."'";
							$autor=$baseDatos->ConsultarBD($sql);
							list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
							if($cont%2==0)
								$color="trListaClaro";
							else
								$color="trListaOscuro";
							$tituloAlt="Archivo: ".$lanzador."\n";
							$tituloAlt.="Autor: ".$nombre1." ".$nombre2." ".$apellido1." ".$apellido2;
								
							if(strlen($lanzador) > 30)
								$lanzador=substr($lanzador,0,30)."...";
?>
					<tr class="<?=$color?>">
<?
							if($_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==5)
							{
?>
						<td class="<?=$color?>" width="7%"><div align="center"><?=$cont+1?>
                        	<input id="codigoLanzador" type="checkbox" name="<?= "chk_lanzador".$cont?>" value="<?=$codigoLanzador?>"></div></td>
<?
							}
							else
							{
?>
						<td class="<?=$color?>" width="7%" align="center"><img src="imagenes/transparente.gif"><?=$cont+1?></td>
<?
							}								 //td
?>
						<td width="15%" class="<?=$color?>" title="<?= $tituloAlt?>">&nbsp;<?=$titulo?></td>
						<td width="11%" class="<?=$color?>" align="center">&nbsp;<?=$tamano/1000?> KB</td>					
						<td width="25%" class="<?=$color?>" title="<?= $tituloAlt?>"><a href="<?=$ubicacion?>" class="link" target="_blank"><?=$lanzador?></a></td>	
						<td width="30%" class="<?=$color?>">&nbsp;<?=$descripcion?></td>
<?
							if($_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==5)
							{
?>
						<td  width="12" class="<?=$color?>" align="center">
							<table class="tablaPrincipal" align="center">
								<tr>
									<td align="center">
                                    	<a href="modificarLanzador.php?idmateria=<?=$_GET[idmateria]?>&materia=<?=$_GET[materia]?>&codigoLanzador=<?=$codigoLanzador?>" class="link">
                                        <img src="imagenes/modificar.gif" alt="Modificar Lanzador" width="16" height="16" border="0"></a></td>
									<td align="center">
                                    	<a href="javascript: eliminarLanzador('<?=$codigoLanzador?>','<?=$lanzador?>')" class="link">
                                        <img src="imagenes/eliminar.gif" alt="Eliminar Lanzador" width="16" height="16" border="0"></a></td>
								</tr>
							</table></td>
<?
							}
?>
					</tr>
<?
							$cont++;
						}//FIN WHILE
						if($_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==5)
						{
?>
							<tr class="trInformacion">
								<td colspan="8" class="trInformacion" align="center">
								  <input type="button" name="btn_eliminar" value="Eliminar Seleccionados" onClick="javascript:eliminar()">
								</td>
							</tr>
<?
						}
					}//FIN IF
				}
				else
				{
?>
							<tr class="trAviso">
								<td class="trAviso" colspan="8">Actualmente no Existen Lanzadores para  <?=$materia?></td>
							</tr>
<?
				}
?>
					<input name="hid_contLanzador" type="hidden" value="<?=$cont?>">
				</form>
				</table>
			</td>
            <td class="tablaEspacio"></td>
			<td class="tablaDerecho"><?=menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho"></td>            
  		</tr>
	</table>
</body>
</html>
<? 
		}
		else
		{
			redireccionar("../scripts/index.phh?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);					
		}
} 
else
{
	redireccionar('../login/');					
}
?>