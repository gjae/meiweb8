<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ("../editor/fckeditor.php") ;
	include_once ('../menu1/Menu1.class.php');	
	
	$baseDatos=new BD();
	$editor=new FCKeditor('edt_lanzadores' , '100%' , '100%' , 'barraBasica' , '' ) ;
	comprobarSession();
	$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=10";
	$resultado=$baseDatos->ConsultarBD($sql);
	list($estado)=mysql_fetch_array($resultado);
	if($estado==1)
	{
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
</head>
<body>
<script> 
	function validar()
	{
		if(document.frm_cargarLanzador.txt_nombreLanzador.value!="" && document.frm_cargarLanzador.fil_archivo.value!="")
			document.frm_cargarLanzador.submit();
		else
			alert ("Debe llenar completamente la información solicitada");
	}
		
	
	function eliminarLanzador(codigoLanzador,lanzador)
	{
		if(confirm("¿Está seguro de eliminar este Lanzador?"))
		{
			location.replace("eliminarLanzador.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>&codigoLanzador="+codigoLanzador+"&lanzador="+lanzador);					
		}
	}
</script> 
<table class="tablaPrincipal">
	<tr valign="top">
		<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']);?></td>
		<td class="tablaEspacio">&nbsp;</td>
		<td>
        <table class="tablaMarquesina" >
				<tr>
					<td>Editar Lanzadores</td>
				</tr>
			</table>
			<br>
<?
		if ($_SESSION['idtipousuario']==5)
		{
			$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.idusuario,mei_lanzador.titulo,mei_lanzador.descripcion,mei_lanzador.tamano,mei_lanzador.lanzador 
			FROM mei_lanzador WHERE mei_lanzador.idlanzador IN 
			(SELECT mei_rellanvirgru.idlanzador FROM mei_rellanvirgru WHERE mei_rellanvirgru.idvirgrupo IN 
			(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."))";
		}
		else
		{
			$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.idusuario,mei_lanzador.titulo,mei_lanzador.descripcion,mei_lanzador.tamano,mei_lanzador.lanzador 
			FROM mei_lanzador WHERE mei_lanzador.idlanzador IN 
			(SELECT mei_rellangru.idlanzador FROM mei_rellangru WHERE mei_rellangru.idgrupo IN 
			(SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']."))";
		}
		$resultado=$baseDatos->ConsultarBD($sql);
		list($codigoLanzador,$autor,$titulo,$descripcion,$tamano,$lanzador)=mysql_fetch_array($resultado);
		if(!empty($codigoLanzador))
		{
?>
			<table class="tablaGeneral" >
				<tr class="trTitulo" >
					<td colspan="5"><img src="imagenes/lanzadores.gif" width="16" height="16" align="texttop"> Editar Lanzadores</td>
				</tr>
				<tr class="trSubTitulo">
					<td width="20%" class="trSubTitulo"><div align="center">T&iacute;tulo</div></td>
					<td width="15%" class="trSubTitulo"><div align="center">Tamaño</div></td>
					<td width="25%" class="trSubTitulo"><div align="center">Descripci&oacute;n</div></td>
					<td width="20%" class="trSubTitulo"><div align="center">Lanzador</div></td>
					<td width="20%" class="trSubTitulo"><div align="center">Editar</div></td>
				</tr>
			  
<?
					if ($_SESSION['idtipousuario']==5)
					{
						$sql="SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']." 
							AND mei_relusuvirgru.idvirgrupo IN (SELECT mei_rellanvirgru.idvirgrupo FROM mei_rellanvirgru)";
					}
					else
					{
						$sql="SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']." 
							AND mei_relusugru.idgrupo IN (SELECT mei_rellangru.idgrupo FROM mei_rellangru)";
					}
						$resultado=$baseDatos->ConsultarBD($sql);
						list($usuario)=mysql_fetch_array($resultado);
						if(!empty($usuario))
						{
							$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.titulo,mei_lanzador.descripcion,mei_lanzador.tamano,mei_lanzador.fechacarga,mei_lanzador.lanzador 
								FROM mei_lanzador WHERE mei_lanzador.idusuario=".$_SESSION['idusuario'];
							$resultado=$baseDatos->ConsultarBD($sql);
							$cont=0;
							while (list($codigoLanzador,$titulo,$descripcion,$tamano,$fechaCarga,$lanzador)=mysql_fetch_array($resultado))
							{
							if($cont%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}

						?>
						<tr class="<?=$color?>">
							<td class="<?=$color?>"><?=$titulo?></td>
							<td class="<?=$color?>"><?=$tamano/1000?> KB </td>					
							<td class="<?=$color?>"><?=$descripcion?></td>					
							<td class="<?=$color?>"><a href="../../datosMEIWEB/archivosLanzador/<?=$lanzador?>" class="link" target="_blank"><?=$lanzador?></a></td>					
							<td class="<?=$color?>">
							<table class="tablaOpciones" align="left">
								<tr>
									<td width="25%"></td>
									<td width="25%"><div align="center"><a href="modificarLanzador.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>&codigoLanzador=<?=$codigoLanzador?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Lanzador" width="16" height="16" border="0"></a></div></td>
									<td width="25%"><div align="center"><a href="javascript: eliminarLanzador('<?= $codigoLanzador?>','<?= $lanzador?>')"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Lanzador" width="16" height="16" border="0"></a></div></td>
									<td width="25%"></td>
								</tr>
							</table>		
							</td>						
						</tr>
						<?
						$cont++;
							}//FIN WHILE
						}//FIN IF
					  ?>
					</table>
					<br>
				<?
				}
				else
				{
				?>
					<table class="tablaGeneral" >
						<tr class="trTitulo" >
						  <td><img src="imagenes/lanzadores.gif" width="16" height="16" align="texttop"> Editar Lanzadores</td>
					  </tr>
					  <tr class="trAviso">
					  	<td>Actualmente no existen Lanzadores</td>
					  </tr>
				  </table>
				<?
				}
				?>
					
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
		</body>
		</html>
<? 
	}
	else
	{
		redireccionar("../scripts/index.phh?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);					
	}
	
?>
