<?
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once('../calendario/FrmCalendario.class.php');


if(comprobarSession())
{
	
	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
	{
				if($_FILES['fil_archivo'])
				{
					if (!is_uploaded_file($_FILES['fil_archivo']['tmp_name']))
					{
						redireccionar("../lanzadores/editarLanzador.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&error=0x001&idDirectorio=".$_GET['idDirectorio']);
					}
					else
					{
						$titulo=$_POST['txt_titulo'];
						$descripcionArchivo=$_POST['txt_descripcion'];
					
						$archivo = $_FILES['fil_archivo']['name']; 
						$tamanoArchivo = $_FILES['fil_archivo']['size']; 
						
						$sql="SELECT MAX(mei_archivos.idarchivo) FROM mei_archivos";
						$consulta=$baseDatos->ConsultarBD($sql);
						list($maxId)=mysql_fetch_array($consulta);
						
						if(empty($maxId))
						{
							$maxId=1;
						}
						else
						{
							$maxId=$maxId+1;
						}
						$nombreArchivo="id".$maxId.$archivo;
						$sql="SELECT mei_directorio.directorio, mei_directorio.ubicacion 
							FROM mei_directorio WHERE mei_directorio.iddirectorio='".$_GET['idDirectorio']."'";
						$directorios=$baseDatos->ConsultarBD($sql);
						list($directorio,$ubicacion)=mysql_fetch_array($directorios);
							
						if (cargarArchivo("fil_archivo",corregirCaracteresURL($nombreArchivo),$ubicacion.$directorio."/"))
						{
						
							registrarBitacora(10,31,false);
						
							$sql="INSERT INTO mei_archivos (idarchivo,iddirectorio,titulo,descripcion,tamano,fechacarga,archivo) 
								VALUES('','".$_GET['idDirectorio']."','$titulo','$descripcionArchivo','$tamanoArchivo','".date("Y-n-d")."','".corregirCaracteresURL($nombreArchivo)."') ";
							$resultado=$baseDatos->ConsultarBD($sql);
							
						}
						else
						{
							redireccionar("../lanzadores/editarDirectorio.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&error=0x001&idDirectorio=".$_GET['idDirectorio']);
						}
					}
				}
				redireccionar("../lanzadores/editarDirectorio.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idDirectorio=".$_GET['idDirectorio']);
		}
		else
		{
				redireccionar('../login/');					
		}
}
else
{
		redireccionar('../login/');					
}
?>
