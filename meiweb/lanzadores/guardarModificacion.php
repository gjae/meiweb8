<?
	include_once ('../librerias/validar.lib.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php'); 
	
	$baseDatos=new BD();
	if(comprobarSession())
	{ 
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{
			if($_FILES['fil_archivo'] and is_uploaded_file($_FILES['fil_archivo']['tmp_name']))
			{
				$maxId=$_GET['codigoLanzador'];
				$nombreFichero="id".$maxId.$_FILES['fil_archivo']['name'];
				$tamanoLanzador=$_FILES['fil_archivo']['size'];
				$sql="SELECT mei_lanzador.ubicacion  FROM mei_lanzador 
						WHERE mei_lanzador.idlanzador=".$_GET['codigoLanzador'];
				$consultaArchivo=$baseDatos->ConsultarBD($sql);
				list($archivo)=mysql_fetch_array($consultaArchivo);
				if(file_exists($archivo))
					unlink($archivo);
										
				if (cargarArchivo("fil_archivo",corregirCaracteresURL($nombreFichero),"../../datosMEIWEB/archivosLanzador/"))
				{
					$sql="UPDATE mei_lanzador SET 
						mei_lanzador.ubicacion='../../datosMEIWEB/archivosLanzador/".corregirCaracteresURL($nombreFichero)."' ,
						mei_lanzador.lanzador='".corregirCaracteresURL($nombreFichero)."',
						mei_lanzador.tamano='".$tamanoLanzador."'
						WHERE  mei_lanzador.idlanzador=".$_GET['codigoLanzador'];
					$baseDatos->ConsultarBD($sql);
				}
				else
				{
					redireccionar("../lanzadores/modificarLanzador.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&error=0x001&codigoLanzador=".$_GET['codigoLanzador']);
				}				
			}
			else if (isset($_POST['txt_url']))
			{
				$maxId=$_GET['codigoLanzador'];
				$lanzador = $_POST['txt_url'];
				$sql="SELECT mei_lanzador.ubicacion  FROM mei_lanzador 
						WHERE mei_lanzador.idlanzador=".$_GET['codigoLanzador'];
				$consultaArchivo=$baseDatos->ConsultarBD($sql);
				list($archivo)=mysql_fetch_array($consultaArchivo);
				if(file_exists($archivo))
					unlink($archivo);
										
				$sql="UPDATE mei_lanzador SET 
					mei_lanzador.ubicacion='".$lanzador."' ,
					mei_lanzador.lanzador='".$lanzador."',
					mei_lanzador.tamano=''
					WHERE  mei_lanzador.idlanzador=".$_GET['codigoLanzador'];
				$baseDatos->ConsultarBD($sql);
			}
			if($_POST['rad_grupo']=="todos")
				$destino=0;			
			else if($_POST['rad_grupo']=="seleccionados")
				$destino=1;
			registrarBitacora(10,32,false);
			
			$sql="UPDATE mei_lanzador SET mei_lanzador.titulo='".$_POST['txt_tituloLanzador']."',
					mei_lanzador.descripcion='".$_POST['txt_descripcionLanzador']."' ,
					mei_lanzador.destino='".$destino."'
					WHERE mei_lanzador.idlanzador=".$_GET['codigoLanzador'];
			$resultado=$baseDatos->ConsultarBD($sql);
			
			if ($_SESSION['idtipousuario']==5)
				$sql="DELETE FROM mei_rellanvirgru WHERE mei_rellanvirgru.idlanzador = '".$_GET['codigoLanzador']."'";
			else
				$sql="DELETE FROM mei_rellangru WHERE mei_rellangru.idlanzador = '".$_GET['codigoLanzador']."'";
			$consulta=$baseDatos->ConsultarBD($sql);

			if ($destino==0)
			{
				if ($_SESSION['idtipousuario']==5)
				{
					$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
							IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
							IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")) 
							GROUP BY (mei_virmateria.idvirmateria)"; 
				}
				else
				{
					$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
						IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
						IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")) 
						GROUP BY (mei_materia.idmateria)";
				}
				$materias=$baseDatos->ConsultarBD($sql);
				while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
				{
					if ($_SESSION['idtipousuario']==5)
					{
						$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
							IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].") 
							AND mei_virgrupo.idvirmateria =".$materiaCodigo;
					}
					else
					{
						$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
							IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].") 
							AND mei_grupo.idmateria =".$materiaCodigo;
					}
					$grupos=$baseDatos->ConsultarBD($sql);
					while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
					{
						if ($_SESSION['idtipousuario']==5)
						{
							$sql="INSERT INTO mei_rellanvirgru ( idlanzador,idvirgrupo) VALUES ('".$_GET['codigoLanzador']."','".$grupoCodigo."')";
						}
						else
						{
							$sql="INSERT INTO mei_rellangru ( idlanzador,idgrupo) VALUES ('".$_GET['codigoLanzador']."','".$grupoCodigo."')";
						}
						$baseDatos->ConsultarBD($sql);
					}
				}
			}
			else if($destino==1)
			{
				for($i=0;$i<$_POST['hid_contGrupo'];$i++) 
				{ 
					if(!empty($_POST['chk_idgrupo'.$i]))
					{ 
						if ($_SESSION['idtipousuario']==5)
						{
							$sql="INSERT INTO mei_rellanvirgru (idlanzador,idvirgrupo) VALUES ('".$_GET['codigoLanzador']."','".$_POST['chk_idgrupo'.$i]."')";
							$baseDatos->ConsultarBD($sql);
						}
						else
						{
							$sql="INSERT INTO mei_rellangru (idlanzador,idgrupo) VALUES ('".$_GET['codigoLanzador']."','".$_POST['chk_idgrupo'.$i]."')";
							$baseDatos->ConsultarBD($sql);
						}
					}					
				}  
			}	
			redireccionar("../lanzadores/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);		
		}
	else
	{
		redireccionar('../login/');					
	}
			
	}
	else
		redireccionar('../login/');					
?>