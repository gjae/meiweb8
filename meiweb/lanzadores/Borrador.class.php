<?

	class Eliminador
	{
		
		var $m_listaFicheros;
		var $m_listaDirectorios;
		var $m_indiceFicheros;
		var $m_indiceDirectorios;
		
		function Eliminador()
		{
			$this->m_indiceFicheros=0;
			$this->m_indiceDirectorios=0;
		}
		
		
		function EliminarDirectorio($a_directorioBase)
		{
			if(file_exists($a_directorioBase))
			{
				$this->Listar($a_directorioBase);
		
				if($this->UnlinkFicheros() && $this->RmDirectorios())
				{
					if(!rmdir($a_directorioBase))
					{
						return false;
					}
					else
					{
						return true;
					}
				}
			}
			else
			{
				return false;
			}
		}
		
		
		function RmDirectorios()
		{
			if(sizeof($this->m_listaDirectorios))
			{
				foreach($this->m_listaDirectorios as $directorio)
				{
					if(!rmdir($directorio))
					{
						print "No se pudo eliminar el fichero: <b>".$directorio."</b>";
						return false;
					}
				}
			}
			return true;
		}
	
		
		function UnlinkFicheros()
		{
			if(sizeof($this->m_listaFicheros))
			{
				foreach($this->m_listaFicheros as $fichero)
				{
					if(!unlink($fichero))
					{
						print "No se pudo eliminar el fichero: <b>".$fichero."</b>";
						return false;
					}	
				}
			}
			return true;
		}
	
	
		function Listar($a_directorioBase)
		{			
			$pathBase=opendir($a_directorioBase);
			
			while ($fichero = readdir($pathBase)) 
			{				
				$nombreElemento=$a_directorioBase."/".$fichero;
					
				if(is_dir($nombreElemento) && $fichero != "." && $fichero != "..")
				{
					$this->listar($nombreElemento);
					$this->m_listaDirectorios[$this->m_indiceDirectorios++]=$nombreElemento;				
				}
				else if(is_file($nombreElemento))
				{					
					$this->m_listaFicheros[$this->m_indiceFicheros++]=$nombreElemento;
				}
			}
			closedir($pathBase);			
		}
	}
	// Fin Eliminador

?>