<?PHP 
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
		
	$baseDatos=new BD();
	$tipoProfesor=2;
	$cbo_materia = @$_POST['cbo_materia'];
	$cbo_archivo = @$_POST['cbo_archivo'];
	$cbo_profesor = @$_POST['cbo_profesor'];
	
	if(comprobarSession())
	{	
		if($_SESSION['banderaAdmnistrador']==1)
			{
?>
				<html>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<head>
				<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
				<script language="javascript">
		
				function enviarpaquete()
				{
								
				var materia= document.frm_contenidosscorm.cbo_materia.value.split('[&&]');
				var archivo= document.frm_contenidosscorm.cbo_archivo.value;
				var profesor= document.frm_contenidosscorm.cbo_profesor.value.split('[&&]');
				
				if (confirm ("Se va a restaurar el contenido de: \n \n Materia: "+ materia[1] +"  \n Profesor: "+ profesor[1] +"  \n Archivo: "+ archivo))
				 {
					document.frm_contenidosscorm.action="descomprimir.php";
					document.frm_contenidosscorm.submit();
				 }
					
				}
		</script>
		</head>
		<body>
		
		<table class="tablaPrincipal">
			<tr valign="top">
			<td height="520" class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><? menu(1); ?></td>
			<td class="tablaEspacio">&nbsp;</td>
				<td valign="top">
					<form method="post" name="frm_contenidosscorm"  id="frm_contenidosscorm" action= "<?PHP  echo $_SERVER['PHP_SELF']; ?>">
				   		<table class="tablaGeneral">		
							<tr class="trTitulo">
								<td colspan="6" class="trTitulo"><img src="../imagenes/scorm.gif" width="16" height="16"> Restaurar contenido							    </td>
							</tr>
						
							<tr class="trSubTitulo">
                        		<td colspan="6" class="trSubTitulo">Opciones para restaurar el contenido en una materia a partir del paquete SCORM</td>
                      		</tr>
						
							<tr class="trInformacion">
								<td colspan="2" align="center"><p><br>
								  <br>
						    	  <strong>Seleccione el paquete SCORM: </strong></p>	
								 </td>
						      		<td>
						   			 <?PHP   
                          	 		 if ($dir = @opendir("../../datosMEIWEB/archivosScorm"))
           								{
               								while (($file = readdir($dir)) !== false)
               								{
                   								$arrayarchivo = explode (".",$file);
				   								if (strtolower($arrayarchivo[sizeof($arrayarchivo)-1])=="zip")
				    							{
				    								$filelist[] = $file;
												}
               								}
             								closedir($dir);
           								}
		   		          			?>
				           		 	<br>
				           		 	<select name="cbo_archivo" id="cbo_archivo">
				       		        <?PHP 
           								asort($filelist);
           								while (list ($key, $nombrearchivo) = each ($filelist))
           								{
               						?>
							         <option value="<?PHP  echo $nombrearchivo; ?>" <?PHP  echo $cbo_archivo == $nombrearchivo? 'selected="selected"':false;?>>
									  <?PHP  echo $nombrearchivo; ?> 
									  </option>
							         <?PHP  
           								}
           								?>
        					     	 </select>
							    </td>
							</tr>
						   
						    <tr class="trInformacion">
								<td colspan="2" align="center"><strong><br>
							    <br> Nombre del Profesor:</strong></td>
				      		 	<td>
								<p> <br><br>
								<select name="cbo_profesor" id="cbo_profesor" onChange="javascript:document.getElementById('frm_contenidosscorm').submit()">
               		   		    <?PHP 
									$sql = "SELECT mei_usuario.idusuario, CONCAT( mei_usuario.primerapellido, ' ', mei_usuario.segundoapellido, ' ', mei_usuario.primernombre, ' ', mei_usuario.segundonombre ) AS nombreprofesor
											FROM mei_usuario
											WHERE  mei_usuario.idusuario
												IN (								
														SELECT mei_usuario.idusuario
														FROM mei_usuario
														WHERE mei_usuario.estado =1
														AND mei_usuario.idtipousuario =2
													) ORDER BY  mei_usuario.primerapellido"; 	
							
									$consulta = $baseDatos->ConsultarBD($sql);
							    ?> <option value="0"> </option>
									 <?PHP 
							 			while($asoc_profesor = mysql_fetch_assoc($consulta))
										{
										?><option value="<?PHP echo  $asoc_profesor['idusuario']; ?>[&&]<?PHP echo  $asoc_profesor['nombreprofesor']; ?>" <?PHP  echo ($asoc_profesor['idusuario']."[&&]".$asoc_profesor['nombreprofesor']) == $cbo_profesor ? "selected" : false;?>><?PHP echo  $asoc_profesor['nombreprofesor']; ?></option><?PHP 
										}							
										?>						
               			      </select>
							  <select name="cbo_materia" id="cbo_materia" onChange="javascript:document.getElementById('frm_contenidosscorm').submit()">
               		  		  <?PHP   
                          				
									list($idProfesor,$nombreProfesor) = explode('[&&]',$cbo_profesor);
									$sql2 = "SELECT DISTINCT mei_materia.idmateria, mei_materia.nombre
											FROM mei_materia, mei_grupo,mei_relusugru
											WHERE mei_materia.idmateria=mei_grupo.idmateria
											AND mei_relusugru.idgrupo = mei_grupo.idgrupo
											AND mei_relusugru.idusuario='".$idProfesor."'ORDER BY nombre";
								 								
											$consulta2 = $baseDatos->ConsultarBD($sql2);
							 
							 				while($asoc_materia = mysql_fetch_assoc($consulta2))
											{
							 				?><option value="<?PHP echo  $asoc_materia['idmateria']; ?>[&&]<?PHP echo $asoc_materia['nombre']; ?>" <?PHP  echo  ($asoc_materia['idmateria']."[&&]".$asoc_materia['nombre'])== $cbo_materia ? "selected" : false;?> ><?PHP echo  $asoc_materia['nombre']; ?></option><?PHP 
											}							
										?>	
           		       		</select>
			           		</p>
							</td>
						 
						  <tr>
						  	  	<td colspan="2" align="center">
								<p><br>
								  <input type="button" name="btn_restaurarcontenido" value="Restaurar" onClick="javascript: enviarpaquete()" />
                              	</p>
							  
							</td>
							<td>
								
								<h2 style="color: #DF013A">
										<ul><br />
											<h2>Advertencia <br /></h2>
											<li style="color: #333">Antes de restaurar  el Scorm se recomienda eliminar el contenido de la materia a restaurar.<br /></li>
											<li style="color: #333">Los examenes perderan las preguntas asignadas.<br /></li>
										</ul>
									</h2>
								
							</td>
							
						  </tr>
				</table>
		 </form>
	 </td>
  </tr>
</table>
 
 </body>
</html>
				
<?PHP  

	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
	
?>
