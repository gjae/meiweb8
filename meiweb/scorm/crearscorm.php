<?PHP 

include_once('../baseDatos/BD.class.php');
include_once ('../librerias/estandar.lib.php');
include('imageneshtml.php');
include_once('crearhtmlcontenidos.php');
include('eliminarTemporal.php');
include('eliminarhtml.php');// libreria para eliminar archivos html temporales
include('restaurarhtml.php');
include('resPregXML.php');

function borrardirectorio($path)
{
    $path = rtrim( strval( $path ), '/' ) ;
     $d = dir( $path );
     if( ! $d )
        return false;
 
    while ( false !== ($current = $d->read()) )
    {
        if( $current === '.' || $current === '..')
            continue;
 
        $file = $d->path . '/' . $current;
 
        if( is_dir($file) )
            removeDirectory($file);
 
        if( is_file($file) )
            unlink($file);
    }
 
    rmdir( $d->path );
    $d->close();
    return true;
}

if(comprobarSession())
{

$baseDatos=new BD();
$codigoMateria=$_GET["idmateria"];//capturo los parametros que se han pasado
$materia=$_GET['materia'];
$versionscorm=$_GET['Versionscorm'];

restaurarhtml($codigoMateria);
$nombrecarpeta=$codigoMateria.$materia;

echo $nombrecarpeta;

if(file_exists ($nombrecarpeta));
{
borrardirectorio($nombrecarpeta);
}

if(mkdir($codigoMateria.$materia, 0777));//creo un carpeta temporal en donde se van a almacenar los recursos antes de comprimir

preguntasXML($codigoMateria,$nombrecarpeta);
respuestasXML($codigoMateria,$nombrecarpeta);

if($versionscorm==0) // segun el tipo de version el manifiesto tiene unas modificaciones en los namespace
{
$xml= '<?PHP xml version="1.0" encoding="UTF-8"?>
<manifest xmlns="http://www.imsglobal.org/xsd/imscp_v1p1" xmlns:imsmd="http://ltsc.ieee.org/xsd/LOM" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_v1p3" xmlns:imsss="http://www.imsglobal.org/xsd/imsss" xmlns:adlseq="http://www.adlnet.org/xsd/adlseq_v1p3" xmlns:adlnav="http://www.adlnet.org/xsd/adlnav_v1p3" identifier="MANIFEST-6C81F826C79865A6966A7C0E83C96702" xsi:schemaLocation="http://www.imsglobal.org/xsd/imscp_v1p1 imscp_v1p1.xsd http://ltsc.ieee.org/xsd/LOM lom.xsd http://www.adlnet.org/xsd/adlcp_v1p3 adlcp_v1p3.xsd http://www.imsglobal.org/xsd/imsss imsss_v1p0.xsd http://www.adlnet.org/xsd/adlseq_v1p3 adlseq_v1p3.xsd http://www.adlnet.org/xsd/adlnav_v1p3 adlnav_v1p3.xsd">
  <metadata>
    <schema>ADL SCORM</schema>
    <schemaversion>2004 3rd Edition</schemaversion>
  </metadata>';
  
 }
 else
 {
 $xml= '<?PHP xml version="1.0" encoding="UTF-8"?>
<manifest identifier="MANIFEST-6C81F826C79865A6966A7C0E83C96702" xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2" xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2" xmlns:imsmd="http://www.imsglobal.org/xsd/imsmd_v1p2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd" >
 <metadata>
    <schema>ADL SCORM</schema>
    <schemaversion>1.2</schemaversion>
  </metadata>';
   }
//crearBupBd($codigoMateria);



		$nombre= "imsmanifest.xml"; 
		$archivo= fopen($nombrecarpeta."/".$nombre,"w");//creo el manifiesto en la carpeta temporal 
		fwrite($archivo,$xml);
		fclose($archivo);  
    $quer = "select idmateria,nombre from mei_materia where idmateria='".$codigoMateria."'"; //
	$list = $baseDatos->ConsultarBD($quer);
    
	$archivo=fopen($nombrecarpeta."/".$nombre,"a");

      while( $row=mysql_fetch_array($list))

	{ 
	fwrite($archivo,"\n");
      fwrite($archivo,'<organizations default="ORG-'.$row[idmateria].'"'.'>');
	  fwrite($archivo,"\n");
      fwrite($archivo,'<organization identifier="ORG-'.$row[idmateria].'"'.' structure="hierarchical">');
	  fwrite($archivo,"\n");
	  fwrite($archivo,'<title>'.$row[nombre].'</title>');
	  fwrite($archivo,"\n");
	  fwrite($archivo,'<item identifier="ITEM-0'.$row[idmateria].'0" isvisible="true" identifierref="RES-0'.$codigoMateria.'">');
	  fwrite($archivo,"\n");
	  fwrite($archivo,'<title>introduccion</title>');
	  fwrite($archivo,"</item>");

   }

     mysql_free_result($list);

    $quer = "select idtema,idtemapadre,titulo,contenido from mei_tema where idmateria='".$codigoMateria."' order by idtemapadre,orden"; //recupero todo  los datos de la consulta para el el codigo de la materia 
	$list=$baseDatos->ConsultarBD($quer);

	function recursiva($archivo,$idtemapadre,$cont)
	{
	include_once('../baseDatos/BD.class.php');
$baseDatos=new BD();
				$sources= "select * from mei_biblioteca where mei_biblioteca.idtema="."'".$idtemapadre."'order by mei_biblioteca.archivo";//miro si tiene recursos asociados 
		$src=$baseDatos->consultarBD($sources);
			while($rsrc=mysql_fetch_array($src))
				{
					fwrite($archivo,"\n");
					fwrite($archivo,'<item identifier="ITEM-0'.$rsrc[idarchivo].'" isvisible="true" identifierref="RES-'.$rsrc[idarchivo].'">');
					fwrite($archivo,"\n");
					fwrite($archivo,'<title>'.$rsrc[titulo].'</title>');
					fwrite($archivo,"\n");
					fwrite($archivo,"</item>");
					$cont++;
				}
				
				
				$quera = "select * from mei_tema where idtemapadre="."'".$idtemapadre."'"."order by orden"; //recupero los hijos del tema actual
				$lista=$baseDatos->ConsultarBD($quera);
				while( $rowa=mysql_fetch_array($lista))

				{

				fwrite($archivo,"\n");
				fwrite($archivo,'<item identifier="ITEM-'.$rowa[idtema].'" '.' isvisible="true">');
				fwrite($archivo,"\n");
				fwrite($archivo,'<title>'.$rowa[titulo].'</title>');
				  recursiva($archivo,$rowa[idtema],$db_conn);//llamo asi misma para hacerla recursiva
				fwrite($archivo,"\n");
				fwrite($archivo,"</item>");

				}
				


	}


	while( $row=mysql_fetch_array($list))

	{
	
		if($row[idtemapadre]==NULL || $row[idtemapadre]=='0' ) //verificando si el tema es tema principal
		{
		fwrite($archivo,"\n");
		fwrite($archivo,'<item identifier="ITEM-'.$row[idtema].'" '.' isvisible="true">');
		fwrite($archivo,"\n");
		fwrite($archivo,'<title>'.$row[titulo].'</title>');
		$sources= "select * from mei_biblioteca where mei_biblioteca.idtema="."'".$row[idtema]."'order by mei_biblioteca.archivo" ;//miro si tiene recursos asociados 
		$src=$baseDatos->consultarBD($sources);
			while($rsrc=mysql_fetch_array($src))
				{
					
					fwrite($archivo,"\n");
					fwrite($archivo,'<item identifier="ITEM-0'.$rsrc[idarchivo].'" isvisible="true" identifierref="RES-'.$rsrc[idarchivo].'">');
					fwrite($archivo,"\n");
					fwrite($archivo,'<title>'.$rsrc[titulo].'</title>');
					fwrite($archivo,"\n");
					fwrite($archivo,"</item>");
					
				}
		$quera = "select * from mei_tema where idtemapadre="."'".$row[idtema]."'"."order by orden"; //busco los hijos dependiendo del codigo del padre
				$lista=$baseDatos->ConsultarBD($quera);
				//echo print_r($lista);
				$cont++;
				while( $rowa=mysql_fetch_array($lista))

				{
				fwrite($archivo,"\n");
				fwrite($archivo,'<item identifier="ITEM-'.$rowa[idtema].'" '.' isvisible="true">');
				fwrite($archivo,"\n");
				fwrite($archivo,'<title>'.$rowa[titulo].'</title>');
				recursiva($archivo,$rowa[idtema],$cont);//llamo a la funcion recursiva que me recorre hijos y subhijos
				fwrite($archivo,"\n");
				fwrite($archivo,"</item>");

				}
				fwrite($archivo,"\n");
				fwrite($archivo,"</item>");
				fwrite($archivo,"\n");
				
		}
		
	}
	mysql_free_result($list);
	mysql_free_result($src);
	mysql_free_result($lista);
	
	
	fwrite($archivo,"</organization>");
	fwrite($archivo,"\n");
	fwrite($archivo,"</organizations>");
	fwrite($archivo,"\n");
	fwrite($archivo,"<resources>");
	fwrite($archivo,"\n");
	
	$query="select idtema,contenido from mei_tema where idmateria='".$codigoMateria."'";//imprimo las etiquetas de recursos 
	$lista=$baseDatos->ConsultarBD($query);
				
				while( $rowa=mysql_fetch_array($lista))

				{
					$quer="select idarchivo, idtipoarchivo, idtipoaplicacion ,archivo, descripcion from mei_biblioteca where idtema="."'".$rowa[idtema]."'";//imprimo las etiquetas de recursos 
					$list=$baseDatos->ConsultarBD($quer);
					while( $row=mysql_fetch_array($list))
					{
					
						if(eregi("^(html)[0-9]", $row[archivo]))//si el archivo comienza por html(cualquiernumero)
						{
							tomarvalores($row[archivo],$nombrecarpeta);
						
						}
						
								fwrite($archivo,"\n");
								fwrite($archivo,'<resource identifier="RES-'.$row[idarchivo].'"'.' adlcp:scormtype="asset" tipoarchivo="'.$row[idtipoarchivo].'" tipoaplicacion="'.$row[idtipoaplicacion].'" descri="'.$row[descripcion].'" type="webcontent" href="'.$row[archivo].'">');
								copy("../../datosMEIWEB/archivosBiblioteca/".$row[archivo],$nombrecarpeta."/".$row[archivo]);
								fwrite($archivo,"\n");
								fwrite($archivo,'<file href="'.$row[archivo].'"/>');
								fwrite($archivo,"\n");
								fwrite($archivo,"</resource>");
								fwrite($archivo,"\n");
							
					}
				}
	fwrite($archivo,'<resource identifier="RES-0'.$codigoMateria.'" adlcp:scormtype="asset" type="webcontent" href="html'.$codigoMateria.'Introduccion.html">');
	if ( file_exists('../../datosMEIWEB/archivosBiblioteca/html'.$codigoMateria.'introduccion.html') ) 
	{
	copy("../../datosMEIWEB/archivosBiblioteca/html".$codigoMateria."Introduccion.html",$nombrecarpeta."/html".$codigoMateria."Introduccion.html");
	}
	else
	{
	
	crearhtml("Introduccion",$materia,$codigoMateria);
	copy("../../datosMEIWEB/archivosBiblioteca/html".$codigoMateria."Introduccion.html",$nombrecarpeta."/html".$codigoMateria."Introduccion.html");
	}
	fwrite($archivo,"\n");
	fwrite($archivo,'<file href="html'.$codigoMateria.'Introduccion.html"/>');
	fwrite($archivo,"\n");
	fwrite($archivo,"</resource>");			
	
	fwrite($archivo,"\n");
	fwrite($archivo,"</resources>");
	fwrite($archivo,"\n");
	fwrite($archivo,"</manifest>");
   
		 fclose($archivo);
		 if($versionscorm==0)
		 {
		 $path = "archivosscorm2004/";
		}
		else {$path = "archivosscormv12/";}
include('scormzip.php');

empaquetarscorm($nombrecarpeta,$path);
eliminartemporal($nombrecarpeta);

eliminarhtml($codigoMateria);// eliminar archivoshtmltemporales


redireccionar('infoarchivo.php?nombrearchivo='.$nombrecarpeta.'&materia='.$materia.'&idmateria='.$codigoMateria);	
}
else redireccionar('../login/');
?>