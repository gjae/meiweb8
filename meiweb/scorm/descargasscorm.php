<?PHP 
		
		$nombre_archivo=$_GET["nombrearchivo"];
		
		$contenidoarchivo=file_get_contents($nombre_archivo);
		$fechamodificacion=date("Y-m-d",filemtime($nombre_archivo));
		$nombre_archivo= str_replace("../../datosMEIWEB/archivosScorm/","",$nombre_archivo);
		$nombre_archivo= str_replace(".zip","",$nombre_archivo);
		
		header('Content-Type: application/x-zip');
		header('Content-transfer-encoding: binary'); 
		header('Content-Disposition: attachment; filename="'.htmlspecialchars($nombre_archivo)." ".$fechamodificacion.'.zip"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		//header('Content-Length: '.$this->get_size());
		echo $contenidoarchivo;
?>