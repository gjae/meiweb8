<?
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once('mail.php');
	if(comprobarSession())
	{
		registrarBitacora(5,22,false);
		
		$destinopre = mysql_real_escape_string($_POST["txt_para"]);
		$asuntomod = $_POST["txt_asunto"];
		$contenidomod = $_POST["edt_correo"];
		$hid_idmateria = $_POST["hid_idmateria"];
		
		/*Agregar Campo*/
		$exists = false;
		$db = "mei_correo";
		$column = "idmateria";
		$column_attr = "int(11) null";
		$columns = "show columns from $db";
		$resultcolumn = $baseDatos->ConsultarBD($resultcolumn);
		while($c = mysql_fetch_assoc($resultcolumn)){
			if($c['Field'] == $column){
				$exists = true;
				break;
			}
		}        
		if(!$exists){
			$sql = "ALTER TABLE `$db` ADD `$column`  $column_attr";
		}					
		$baseDatos->ConsultarBD($sql);		
		/*Fin Agregar Campo*/
		
		$destino = explode(",",$destinopre);
		if(!empty($_POST['hid_codigoCorreo']))
		{
			$sql = "UPDATE mei_correo SET mei_correo.asunto='".base64_encode($asuntomod)."',contenido='".base64_encode($_POST['edt_correo'])."', idmateria = ".$hid_idmateria."
					WHERE mei_correo.idcorreo=".$_POST['hid_codigoCorreo'];
			$resultado=$baseDatos->ConsultarBD($sql);
		}
		else
		{
			$sql = "INSERT INTO mei_correo (idcorreo,remitente,asunto,fecha,hora,contenido,idmateria)
				VALUES('','".$_SESSION['idusuario']."','".base64_encode($asuntomod)."','".date('Y-n-j')."','".date("H:i:s")."','".base64_encode($_POST['edt_correo'])."','".$hid_idmateria."') ";
			$resultado=$baseDatos->ConsultarBD($sql);
		}

		$sql="SELECT MAX(mei_correo.idcorreo) FROM mei_correo";
		$consulta=$baseDatos->ConsultarBD($sql);
		list($maxId)=mysql_fetch_array($consulta);
//
		if($_POST['rad_grupo']=="general")
		{
			$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.idusuario <> ".$_SESSION['idusuario'];
			$consulta=$baseDatos->ConsultarBD($sql);
			while (list($codigoEnviar)=mysql_fetch_array($consulta))
			{
				$sql="INSERT INTO mei_relcorusu ( idusuario , idcorreo , estado ) VALUES ('".$codigoEnviar."','".$maxId."','0')";
				$resultado=$baseDatos->ConsultarBD($sql);
			}
			$grupoEnviado.="Todos los MeiWeb"."[***]";
		}
		else
		{
			for ($i=0; $i<sizeof($destino);$i++)
			{
				list ($codigos)= explode("@meiweb",$destino[$i]);
				if(is_numeric($codigos))
				{
					$sql="SELECT COUNT(mei_usuario.idusuario) FROM mei_usuario WHERE mei_usuario.idusuario=".$codigos;
					$resultado=$baseDatos->ConsultarBD($sql);
					list($existeCodigo)=mysql_fetch_array($resultado);
					if(!empty($existeCodigo))
					{
						$enviado.=$codigos."[***]";
						$codigosEnviar.=$codigos."[***]";
					}
					else
					{
						$noEnviado.=$destino[$i]."[***]";
					}
				}
				else
				{
					$noEnviado.=$destino[$i]."[***]";
				}
			}

			for($i=0;$i<$_POST['hid_contGrupo'];$i++)
			{
				if(!empty($_POST['chk_idgrupo'.$i]))
				{
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
						$sql="SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario <>".$_SESSION['idusuario']." 
							AND mei_relusuvirgru.idvirgrupo=".$_POST['chk_idgrupo'.$i];
					}
					else
					{
						$sql="SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idusuario <>".$_SESSION['idusuario']." 
							AND mei_relusugru.idgrupo=".$_POST['chk_idgrupo'.$i];
					}
					$consulta=$baseDatos->ConsultarBD($sql);
					while (list($codigoEnviar)=mysql_fetch_array($consulta))
					{
							$codigosEnviar.=$codigoEnviar."[***]";
					}
				//BORRAR REPETIDOS
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
						$sql="SELECT mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo=".$_POST['chk_idgrupo'.$i];
					else
						$sql="SELECT mei_grupo.nombre FROM mei_grupo WHERE mei_grupo.idgrupo=".$_POST['chk_idgrupo'.$i];
					$consulta=$baseDatos->ConsultarBD($sql);
					list($grupo)=mysql_fetch_array($consulta);
					$grupoEnviado.=$grupo."[***]";
				}
			}
			$codigos=eliminarRepetidos("[***]",$codigosEnviar);

			if(sizeof($codigos)>0)//inserta en la tabla relcorusu
			{
				foreach($codigos as $dato)
				{
					$sql = "INSERT INTO  mei_relcorusu (idusuario,idcorreo,estado) VALUES('".$dato."','".$maxId."','0') ";
						$baseDatos->ConsultarBD($sql);
				}
			}

//INICIO Envio a correos externos

				if(!empty($_POST['chk_externo']) || !empty($_POST['chk_externo1']))
				{
					if(sizeof($codigos)>0)//inserta en la tabla relcorusu
					{
						$var=0;
						foreach($codigos as $dato)
						{
							$sql = "SELECT mei_relcorarc.archivo,mei_relcorarc.localizacion FROM mei_relcorarc WHERE mei_relcorarc.idcorreo=".$maxId;
							$archivos=$baseDatos->ConsultarBD($sql);
							$sql = "SELECT mei_usuario.correo FROM mei_usuario WHERE mei_usuario.idusuario = ".$dato;
	                        $consulta=$baseDatos->ConsultarBD($sql);
							list($direccion)=mysql_fetch_array($consulta);
							$sql1="SELECT mei_usuario.primernombre, mei_usuario.segundonombre,
							mei_usuario.primerapellido, mei_usuario.segundoapellido 
							FROM mei_usuario WHERE mei_usuario.idusuario = ".$dato;
							$consulta1=$baseDatos->ConsultarBD($sql1);
							list($nombre1,$nombre2,$ape1,$ape2)=mysql_fetch_array($consulta1);
							
							$nombredestino=$nombre1." ".$nombre2." ".$ape1." ".$ape2;
							
							$contenido='';
														
							$cont=0;
				while (list($archivo,$localizacion) = mysql_fetch_row($archivos))
								{
									if ($cont==0) {
									$enlaces="/var/www/html/meiweb/datosMEIWEB/archivosCorreo/".$localizacion;$cont++;		
									}else {

								$enlaces.=" | "."/var/www/html/meiweb/datosMEIWEB/archivosCorreo/".$localizacion;}
								}
							/*	if($enlaces!=''){
									$contenido .= "< h3>Archivos Adjuntos</h3>".$enlaces."<br><br>";
							*/	$adjuntos=explode(" | ", $enlaces);
								
								$contenido.= $contenidomod;
								$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
								$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
								// Cabeceras adicionales
								$nombrecompleto=$_SESSION['nombreusuario']." ".$_SESSION['apellidousuario'];
								$cabeceras .= 'To: '.$direccion. "\r\n";
								//$cabeceras .= 'From: Meiweb'. $nombrecompleto . "\r\n";
								$cabeceras .= 'From: Meiweb'."\r\n";
								//$contenido.="\n\n\n\n\n NOTA: No responda a esta dirección de correo. El emisario ha usado los servicios la plataforma MEIWEB de la Escuela de Ingeniería de Sistemas e Informática UIS.";
				
				//		$correoExterno=mail ("$direccion",$asuntomod, "$contenido",$cabeceras);
						//$conte =strip_tags($contenido);
								if ($var==0) {
									$correo=$direccion;
									$nombrecomp=$nombredestino;
								}
								else{
									$correo.=" | ".$direccion;
									$nombrecomp.=" | ".$nombredestino;
								}
								$var++;
						}
						$direccion=explode(" | ", $correo);
						$nombredestino=explode(" | ", $nombrecomp);
						sendgmail($direccion,$asuntomod,$nombrecompleto,$contenido,$nombredestino,$adjuntos,$archivo);
					}
				}


							//FIN Envio a correos externos


				}


//
				/*$vectorEnviado=eliminarRepetidos("[***]",$enviado);

				if (sizeof($vectorEnviado)>0)
				{
					$codigosEnviar=implode("[***]",$vectorEnviado);
				}

				if(strlen($codigosEnviar)>0)
				{
					$codigosEnviar.="[***]";
				}*/
				if(!empty ($_GET['foro']))
				{
					redireccionar("../foro/verMensajeForo.php?foro=1&codigoMensaje=".$_GET['codigoMensaje']."&idmateria=".$_GET['idmateria']);
				}
				else
				{
					redireccionar("confirmarEnvio.php?enviado=".$enviado."&noEnviado=".$noEnviado."&grupoEnviado=".$grupoEnviado."&idmateria=".$_GET['idmateria']);
				}
	}
	else
		redireccionar('../login/');
?>