<?
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{
?>
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral">
						<tr class="trTitulo">
							<td><div align="center" class="trTitulo">
							  <div align="left">Confirmaci&oacute;n de Envio</div>
							</div></td>
						</tr>
<?
//

					$grupoEnviado= explode("[***]",$_GET["grupoEnviado"]);
					if(!empty($_GET["grupoEnviado"]))
					{
?>
					 	<tr class="trSubTitulo">
							<td class="trSubTitulo"><div align="center">Grupo Enviado </div></td>
					  	</tr>
<?
						for ($i=0;$i<sizeof($grupoEnviado)-1;$i++)
						{	
							if($i%2==0)
							{
								$color="trListaClaro";
							}
							else
							{
								$color="trListaOscuro";
							}
?>
							<tr class="<?=$color?>">
								<td class="<?=$color?>"><div align="center">Grupo <?=$grupoEnviado[$i]?></div></td>
					  		</tr>
<?
						}
?>
						</table>
<?
					}
//					
					$enviado= explode("[***]",$_GET["enviado"]);
					if(!empty($_GET["enviado"]))
					{
?>
                        <br>
                        <table class="tablaGeneral">
                            <tr class="trSubTitulo">
                                <td><div align="center">Correo Enviado </div></td>
                            </tr>
<?
						for ($i=0;$i<sizeof($enviado)-1;$i++)
						{	  
							if($i%2==0)
							{
								$color="trListaClaro";
							}
							else
							{
								$color="trListaOscuro";
							}
?>
					
							<tr class="<?=$color?>">
								<td class="<?=$color?>"><div align="center"><?=$enviado[$i].'@meiweb'?></div> <div align="center"></div></td>
					  		</tr>
<?
						}
?>
					</table>
<?
					}
					
  	  				$noEnviado= explode("[***]",$_GET["noEnviado"]);
					if(!empty($_GET["noEnviado"]))
					{
?>
                        <br>
                        <table class="tablaGeneral">
                            <tr class="trSubTitulo">
                                <!-- <td class="trSubTitulo"><div align="center">Correo no Enviado </div></td> ojo --> 
                            </tr>
<?
						$cont=0;
						for ($i=0;$i<sizeof($noEnviado)-1;$i++)
						{	
							if($i%2==0)
							{
								$color="trListaClaro";
							}
							else
							{
								$color="trListaOscuro";
							}
?>
							<tr class="<?=$color?>">
								<td class="<?=$color?>"><div align="center"><?=$noEnviado[$i]?>      </div></td>
						  	</tr>
<?
							$cont++;
						}
?>
					</table>
<?						
					}
?>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
	</table>
 <? 
	}
	else
	{
		redireccionar('../login/');					
	}
?>
</body>
</html>
