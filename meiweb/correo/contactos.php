<?
	include_once('../baseDatos/BD.class.php'); 
	include_once ('../librerias/estandar.lib.php');
	include_once("../editor/fckeditor.php") ;
	
	$baseDatos=new BD();
	if(comprobarSession())
	{
		mysql_query("SET NAMES 'utf8'");
	
?>
<html>
	<head>
		<title>Lista de Contactos</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<script language="javascript">
			function agregar(correo, codigo)
			{
				if (window.opener.document.frm_redactarCorreo.txt_para.value == "")
				{
					window.opener.document.frm_redactarCorreo.txt_para.value = correo 
				}
				else
				{
					window.opener.document.frm_redactarCorreo.txt_para.value += "," + correo 
				}
			}
				
			function agregarFavorito(codigo,idUsuario)
			{
				document.frm_contactos.action="agregarFavorito.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idUsuario="+idUsuario+"&codigo="+codigo+"&estado="+codigo;
				document.frm_contactos.submit();
			}
				
			function eliminarFavorito(codigo,idUsuario)
			{
				document.frm_contactos.action="eliminarFavorito.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idUsuario="+idUsuario+"&codigo="+codigo+"&estado="+codigo;
				document.frm_contactos.submit();
			}
				
			function buscarFavorito(idUsuario)
			{
				document.frm_contactos.action="contactos.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idUsuario="+idUsuario;
				document.frm_contactos.submit();
			}
		</script>
	</head>
	<body>
<?
		if(!empty($_GET['estado']))
		{
			$sql="INSERT INTO mei_favorito (idfavorito,idusuario,favorito) VALUES('','".$_GET['idUsuario']."','".$_GET['codigo']."') ";
			$resultado=$baseDatos->ConsultarBD($sql);
		}
?>
				<table class="tablaPrincipal" align="center" width="100%">
					<tr>
						<td valign="top">
							<form name="frm_contactos" method="post" target="_self">
								<table class="tablaContactos">
									<tr class="trTitulo">
										<td colspan="4" class="trTitulo">
                                        	<div align="center">Lista de Contactos<img src="imagenes/transparente.gif" width="16" height="16"></div></td>
									</tr>
									<tr class="trListaClaro">
										<td colspan="4">
                                        	<img src="imagenes/favorito.gif" width="15" height="13" alt="Favoritos" align="texttop"> Pertenece a Favoritos</td>
									</tr>
									<tr class="trListaClaro">
										<td colspan="4">
                                        	<img src="imagenes/agregar.gif" width="15" height="13" alt="Agregar a Favoritos" align="texttop"> Agregar a Favoritos</td>
									</tr>
									<tr class="trListaClaro">
										<td height="21" colspan="4">
                                        <img src="imagenes/eliminar.gif" width="15" height="13" alt="Eliminar de Favoritos" align="texttop"> Eliminar de Favoritos</td>
									</tr>
									<tr class="trSubTitulo">
										<td colspan="4" class="trSubTitulo">
                                            <input name="txt_buscar" type="text" id="txt_buscar" value="<?= $_POST['txt_buscar']?>" size="40">
                                            <input name="btn_buscar" type="button" id="btn_buscar" value="Buscar" onClick="javascript: buscarFavorito('<?= $_GET['idUsuario']?>')"></td>
									</tr>
<?
			// INICIO DE LA BUSQUEDA
			if(!empty($_POST['txt_buscar']))
			{
				$cadena=explode(' ',$_POST['txt_buscar']);
				foreach($cadena as $palabra)
				{
					if(empty($busqueda))
					{
						$busqueda="mei_usuario.idusuario LIKE '%".$palabra."%'";
						$busqueda.=" OR CONCAT(mei_usuario.primernombre,' ',mei_usuario.segundonombre,' ',mei_usuario.primerapellido,' ',mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
					}
					else
					{
						$busqueda.=" OR mei_usuario.idusuario LIKE '%".$palabra."%'";
						$busqueda.=" OR CONCAT(mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
					}
				}
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					mysql_query("SET NAMES 'utf8'");
					$sql="SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido, mei_usuario.segundoapellido
							FROM mei_usuario
								WHERE (mei_usuario.idtipousuario=5 OR mei_usuario.idtipousuario=6) AND (".$busqueda.")";
				}
				else
				{
					mysql_query("SET NAMES 'utf8'");
					$sql="SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido, mei_usuario.segundoapellido
							FROM mei_usuario
								WHERE (mei_usuario.idtipousuario=2 OR mei_usuario.idtipousuario=3) AND (".$busqueda.")";
				}
				$consultaBusqueda=$baseDatos->ConsultarBD($sql);
				$numRegistros=mysql_num_rows($consultaBusqueda);
				if($numRegistros)
				{
					$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registro encontrado.";
				}
				else
				{
					$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registros encontrados.";
				}
?>
									<tr class="trSubTitulo">
										<td colspan="4" class="trSubTitulo"><?= $cadenaResultado?></td>
									</tr>
<?
				$contBusqueda=0;
				while(list($idUsuario,$nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($consultaBusqueda))
				{
					if($contBusqueda++%2==0)
					{
						$color="trListaClaro";
					}
					else
					{
						$color="trListaOscuro";
					}
					$titulo="Agregar este contacto como destinatario de correo \n";
					$titulo.="Codigo: ".$idUsuario;
					$sql="SELECT DISTINCT mei_favorito.favorito
							FROM mei_favorito
								WHERE mei_favorito.favorito = '".$idUsuario."'
									AND mei_favorito.idusuario = '".$_GET['idUsuario']."'";
					
					$consultaFavorito=$baseDatos->ConsultarBD($sql);
					if(mysql_num_rows($consultaFavorito))
					{
?>
									<tr class="<?=$color?>">
										<td width="97%" class="<?=$color?>">
                                        	<img src='imagenes/favorito.gif' alt="Favoritos" border="0" align="texttop"><a href="javascript:agregar('<?=$idUsuario .'@meiweb';?>', '<?=$idUsuario?>')" class="link" title="<?= $titulo?>"> <?= $apellido1.' '.$apellido2.' '.$nombre1.' '.$nombre2; ?></a></td>
										<td width="3%" class="<?=$color?>"><a href="javascript:eliminarFavorito('<?=$idUsuario?>','<?= $_GET['idUsuario']?>')" class="link"><img src="imagenes/eliminar.gif" width="15" height="13" border="0" alt="Eliminar de Favoritos"></a></td>
									</tr>
<?
					}
					else
					{
?>
									<tr class="<?=$color?>">
										<td width="97%" class="<?=$color?>"><a href="javascript:agregar('<?=$idUsuario .'@meiweb';?>', '<?=$idUsuario?>')" class="link" title="<?= $titulo?>">&nbsp;<?= $apellido1.' '.$apellido2.' '.$nombre1.' '.$nombre2; ?></a></td>
										<td width="3%" class="<?=$color?>"><a href="javascript:agregarFavorito('<?=$idUsuario?>','<?= $_GET['idUsuario']?>')" class="link"><img src="imagenes/agregar.gif" width="15" height="13" border="0" alt="Agregar a Favoritos"></a></td>
									</tr>
<? 
					}
				}
			}// FIN DE LA BUSQUEDA
?>
									<tr>
										<td class="trInformacion" colspan="4">&nbsp;</td>
									</tr>
<?
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
					IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
					IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario ='".$_GET['idUsuario']."')) 
					GROUP BY (mei_virmateria.idvirmateria)"; 
			}
			else
			{
				$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
					IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
					IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario ='".$_GET['idUsuario']."')) 
					GROUP BY (mei_materia.idmateria)"; 
			}
			$materias=$baseDatos->ConsultarBD($sql);
			while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
			{
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
						IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario ='".$_GET['idUsuario']."') 
						AND mei_virgrupo.idvirmateria =".$materiaCodigo;
				}
				else
				{
					$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
						IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario ='".$_GET['idUsuario']."') AND mei_grupo.idmateria =".$materiaCodigo;
				}
				$grupos=$baseDatos->ConsultarBD($sql);
				while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
				{
?>
									<tr class="trSubTitulo">
										<td colspan="4" class="trSubTitulo">Grupo <?= $grupoNombre.' '.$materiaNombre?></td>
									</tr>
<?
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
						$sql ="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, mei_usuario.idusuario 
								FROM mei_usuario WHERE mei_usuario.idusuario IN(SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru 
									WHERE mei_relusuvirgru.idvirgrupo=".$grupoCodigo.") AND mei_usuario.idusuario <> '".$_GET['idUsuario']."'
										ORDER BY mei_usuario.primerapellido";
					}
					else
					{
						$sql ="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, mei_usuario.idusuario FROM mei_usuario 
								WHERE mei_usuario.idusuario IN(SELECT mei_relusugru.idusuario FROM mei_relusugru 
									WHERE mei_relusugru.idgrupo=".$grupoCodigo.") AND mei_usuario.idusuario <> '".$_GET['idUsuario']."'
										ORDER BY mei_usuario.primerapellido";
					}
			
					$resultado = $baseDatos->ConsultarBD($sql); 
					$cont=0;
					while (list($nombre1,$nombre2,$apellido1,$apellido2,$idusuario) = mysql_fetch_array($resultado))
					{
						if($cont%2==0)
						{
							$color="trListaClaro";
						}
						else
						{
							$color="trListaOscuro";
						}
						
						$sql="SELECT mei_favorito.favorito FROM mei_favorito WHERE mei_favorito.favorito=".$idusuario." AND mei_favorito.idusuario='".$_GET['idUsuario']."'";
						$favoritos = $baseDatos->ConsultarBD($sql); 
						list($favorito) = mysql_fetch_array($favoritos);
						$titulo="Agregar este contacto como destinatario de correo \n";
						$titulo.="Codigo: ".$idusuario;
						if(!empty($favorito))
						{
?>
									<tr class="<?=$color?>">
										<td width="97%" class="<?=$color?>"><img src='imagenes/favorito.gif' alt="Favoritos" border="0" align="texttop"><a href="javascript:agregar('<?=$idusuario .'@meiweb';?>', '<?=$idusuario?>')" class="link" title="<?= $titulo?>"> <?= $apellido1.' '.$apellido2.' '.$nombre1.' '.$nombre2; ?></a></td>
										<td width="3%" class="<?=$color?>"><a href="javascript:eliminarFavorito('<?=$idusuario?>','<?= $_GET['idUsuario']?>')" class="link"><img src="imagenes/eliminar.gif" width="15" height="13" border="0" alt="Eliminar de Favoritos"></a></td>
									</tr>
<?
						}
						else
						{
?>
									<tr class="<?=$color?>">
										<td width="97%" class="<?=$color?>"><a href="javascript:agregar('<?=$idusuario .'@meiweb';?>', '<?=$idusuario?>')" class="link" title="<?= $titulo?>">&nbsp;<?= $apellido1.' '.$apellido2.' '.$nombre1.' '.$nombre2; ?></a></td>
										<td width="3%" class="<?=$color?>"><a href="javascript:agregarFavorito('<?=$idusuario?>','<?= $_GET['idUsuario']?>')" class="link"><img src="imagenes/agregar.gif" width="15" height="13" border="0" alt="Agregar a Favoritos"></a></td>
									</tr>
<?
						}
						$cont++;
					}
?>
									<tr>
										<td class="trInformacion" colspan="4">&nbsp;</td>
									</tr>
<?					
				}
			}
?>
									<tr>
										<td class="trInformacion" colspan="4">&nbsp;</td>
									</tr>
									<tr class="trSubTitulo">
										<td colspan="4" class="trSubTitulo">Otros Grupos</td>
									</tr>
<?
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				$sql="SELECT DISTINCT( mei_relusuvirgru.idvirgrupo) FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario <> '".$_GET['idUsuario']."'";
			else
				$sql="SELECT DISTINCT( mei_relusugru.idgrupo) FROM mei_relusugru WHERE mei_relusugru.idusuario <> '".$_GET['idUsuario']."'";
			
			$resultado = $baseDatos->ConsultarBD($sql); 
			while(list($codigoGrupo)=mysql_fetch_array($resultado))
			{
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					$sql = "SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, 
							mei_usuario.idusuario FROM mei_usuario 
							WHERE (mei_usuario.idtipousuario=5 OR mei_usuario.idtipousuario=6) AND mei_usuario.idusuario NOT 
							IN(SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru 
							WHERE mei_relusuvirgru.idvirgrupo=".$codigoGrupo.") AND mei_usuario.idusuario <> '".$_GET['idUsuario']."' 
							ORDER BY mei_usuario.primerapellido";
				}
				else
				{
					$sql = "SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, 
					mei_usuario.idusuario FROM mei_usuario 
					WHERE (mei_usuario.idtipousuario=2 OR mei_usuario.idtipousuario=3) AND mei_usuario.idusuario NOT 
					IN(SELECT mei_relusugru.idusuario FROM mei_relusugru 
					WHERE mei_relusugru.idgrupo=".$codigoGrupo.") AND mei_usuario.idusuario <> '".$_GET['idUsuario']."' 
					ORDER BY mei_usuario.primerapellido";
				}
				$resultado = $baseDatos->ConsultarBD($sql); 
				$cont=0;
				while (list($nombre1,$nombre2,$apellido1,$apellido2,$idusuario) = mysql_fetch_array($resultado))
				{
					if($cont%2==0)
					{
						$color="trListaClaro";
					}
					else
					{
						$color="trListaOscuro";
					}
			
					$sql="SELECT mei_favorito.favorito FROM mei_favorito WHERE mei_favorito.favorito=".$idusuario." AND mei_favorito.idusuario=".$_GET['idUsuario'];
					$favoritos = $baseDatos->ConsultarBD($sql); 
					list($favorito) = mysql_fetch_array($favoritos);
					if(!empty($favorito))
					{
?>
									<tr class="<?=$color?>">
										<td width="97%" class="<?=$color?>"><img src='imagenes/favorito.gif' alt="Favoritos" border="0" align="texttop"><a href="javascript:agregar('<?=$idusuario .'@meiweb';?>')" class="link" title="Codigo: <?=$idusuario?>"> <?= $apellido1.' '.$apellido2.' '.$nombre1.' '.$nombre2; ?></a></td>
										<td width="3%" class="<?=$color?>"><a href="javascript:eliminarFavorito('<?=$idusuario?>','<?= $_GET['idUsuario']?>')" class="link"><img src="imagenes/eliminar.gif" width="15" height="13" border="0" alt="Eliminar de Favoritos"></a></td>
									</tr>
<?
					}
					else
					{
?>
									<tr class="<?=$color?>">
										<td width="97%" class="<?=$color?>"><a href="javascript:agregar('<?=$idusuario .'@meiweb';?>', '<?=$idusuario?>')" class="link" title="Codigo: <?=$idusuario?>"><?= $apellido1.' '.$apellido2.' '.$nombre1.' '.$nombre2; ?></a></td>
										<td width="3%" class="<?=$color?>"><a href="javascript:agregarFavorito('<?=$idusuario?>','<?= $_GET['idUsuario']?>')" class="link"><img src="imagenes/agregar.gif" width="15" height="13" border="0" alt="Agregar a Favoritos"></a></td>
									</tr>
<?
					}
					$cont++;
				}
			}
?>
									<tr class="trInformacion">
										<td colspan="4"><div align="center">
                                        	<input type ="button" name="Cerrar" value="cerrar" onClick="javascript:window.close();"></div></td>
									</tr>
								</table>
							</form>
						</td>
					</tr>
				</table>
			</body>
		</html>
<?
}
?>


