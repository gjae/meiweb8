<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');

  	$baseDatos=new BD();
	if(comprobarSession())
	{
		if ($_SESSION['idtipousuario']==6)
		{
			$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=7";
			$resultado=$baseDatos->ConsultarBD($sql);
			list($estado)=mysql_fetch_array($resultado);
			if($estado==1)
			{
				$sql="SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE 
						(mei_virgrupo.idvirmateria=".$_GET['idmateria']." AND mei_virgrupo.idvirgrupo IN 
						(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."))"; 
				$resultado=$baseDatos->ConsultarBD($sql);
				list($grupo)=mysql_fetch_array($resultado);
				
				$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.telefono,
							mei_usuario.correo,mei_usuario.perfil,mei_usuario.imagen,mei_usuario.sexo FROM mei_usuario WHERE
 							mei_usuario.idtipousuario=5 AND mei_usuario.idusuario IN (
							  SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$grupo.")";
				$resultado=$baseDatos->ConsultarBD($sql);
				list($nombre1,$nombre2,$apellido1,$apellido2,$telefono,$correo,$perfil,$imagen,$sexo)=mysql_fetch_array($resultado);
				
				if ($sexo==f)
					$sexo="El éxito consiste en obtener lo que se desea. La felicidad, en disfrutar lo que se obtiene.";
				else if ($sexo==m)
					$sexo="El secreto del éxito en la vida de un hombre está en prepararse para aprovechar la ocasión cuando se presente";
					
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<table class="tablaPrincipal">
			<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td>
    		  <table class="tablaGeneral" >
						<tr class="trTitulo">
						  <td colspan="6" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"><b>Perfil Tutor</b></td>
					  	</tr>
   						<tr class="trInformacion">
                        	<td rowspan="7" width="10%">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        	<td>&nbsp;</td>
                            <td rowspan="7" width="10%">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                       	  <td>&nbsp;</td> 
                            <td>&nbsp;</td>             
                        	<td rowspan="7" width="10%">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
   					 	<tr class="trInformacion">
                       	  <?
				$nombreImagen='../../datosMEIWEB/fotosUsuarios/'.$imagen;
				if ($imagen==-1)
				{
?>
                        	<td rowspan="6" align="center" valign="top"><br><img src="imagenes/avatar.jpg" width="200" height="250"></td>
<?
           		}
				else
				{
?>
                   		  <td rowspan="6" align="center" valign="top"><br>
               		      <img src="<?php print $nombreImagen?>" width="130" height="180"></td>
<?
           		}
?>
   						  <td align="center"><b><?=$nombre1?> <?=$nombre2?> <?=$apellido1 ?> <?=$apellido2?></b></td>
                          <td>&nbsp;</td>
				</tr>
   					 	<tr class="trInformacion">
   					 	  <td align="center"><p><br><?=$sexo?><br>
   					 	    <br><?=$correo?><br><?=$telefono?><br>
                           	  </p>
   					 	    <p><br>
			 	          </p></td>
                          <td>&nbsp;<br></td>
		 	  			</tr>
                       
   					 	<tr class="trSubTitulo">
   					 	  <td align="center"><b>Perfil</b></td>
                          <td>&nbsp;</td>
		 	  			</tr>
   						<tr class="trInformacion">
                        	<td align="center"><p><br><?=$perfil?></p><p></p></td>
                            <td>&nbsp;</td>
                        </tr> 
   					 	<tr class="trSubTitulo">
   					 	  <td align="center">&nbsp;</td>
                          <td>&nbsp;</td>
		 	  			</tr>
                                               
   					 	<tr class="trInformacion">
   					 	  <td>&nbsp;</td>
                          <td>&nbsp;</td>
				 	    </tr>
					</table>
				</td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><?=menu1($_SESSION['idtipousuario']);?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<? 
			}
			else
			{
				redireccionar('../scripts/');					
			}
		}
		else
		{
			redireccionar('../login/');					
		}
	}
	else
	{
		redireccionar('../login/');					
	}
?>