<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/validar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');  
	
  	$baseDatos=new BD();
	if(comprobarSession())
	{ 
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		</head>
		<body>
		<script> 
			function verBitacora(codigoUsuario)
			{
					location.replace("../bitacora/index.php?idUsuario="+codigoUsuario);					
			}
		
		function enviarOrdenar()
		{
			if (document.frm_buscarUsuario.cbo_tipoUsuario.value==0 || document.frm_buscarUsuario.cbo_tipoUsuario.value==1 || document.frm_buscarUsuario.cbo_tipoUsuario.value==2 || document.frm_buscarUsuario.cbo_tipoUsuario.value==3)
			{
				document.frm_buscarUsuario.action="listarUsuariosAdministrador.php";
				document.frm_buscarUsuario.submit();
			}
			 if (document.frm_buscarUsuario.cbo_tipoUsuario.value==4)
			{
				document.frm_buscarUsuario.action="listarGruposAdministrador.php";
				document.frm_buscarUsuario.submit();
			}
			 
			else
			{
				document.frm_buscarUsuario.submit();
			}

		}
		
		function chekear()
		{
			var todo=document.getElementById('todo');
			if (todo.checked==true)
				{
					for(i=0;i<document.frm_usuario.elements.length;i++)
					{
						if(document.frm_usuario.elements[i].id=="codigoUsuario")
						document.frm_usuario.elements[i].checked=true;
					}
				}
			else
				{
					for(i=0;i<document.frm_usuario.elements.length;i++)
					{
						if(document.frm_usuario.elements[i].id=="codigoUsuario")
						document.frm_usuario.elements[i].checked=false;
					}
				}
		}
		
			function eliminarUsuario(codigoUsuario)
			{
				if(confirm("¿Está seguro de eliminar este usuario del sistema?"))
				{
					location.replace("eliminarUsuario.php?codigoUsuario="+codigoUsuario);					
				}
			}
			function reiniciarUsuario(codigoUsuario)
			{
				if(confirm("¿Está seguro de reinicar este usuario?"))
				{
					location.replace("reiniciarUsuario.php?admin=1&codigoUsuario="+codigoUsuario);					
				}
			}
			function reiniciar(codigoGrupo)
			{
				if(confirm("¿Está seguro de reinicar a todos los Usuarios?"))
				{
					location.replace("reiniciarUsuario.php?admin=2&codigoGrupo="+codigoGrupo);					
				}
			}
			
		function enviar()
		{
			document.frm_verUsuario.submit();				
		}
		function eliminar()
		{
			if(confirm("¿Está seguro de eliminar los usuarios seleccionados?"))
			{
			var contador=0;
					for(i=0;i<document.frm_usuario.elements.length;i++)
					{
						if(document.frm_usuario.elements[i].id=="codigoUsuario")
						{
							if (document.frm_usuario.elements[i].checked==true)
							{
								contador++;							
							}
							
						}
					}
			if(contador>0)
				{
				document.frm_usuario.action="eliminarUsuario.php";
				document.frm_usuario.submit();
				}
			else
				alert('No hay ningún usuario seleccionado')
			}
		}
		function eliminarBuscados()
		{
			if(confirm("¿Está seguro de eliminar los usuarios seleccionados?"))
			{
			var contador=0;
					for(i=0;i<document.frm_buscarUsuario.elements.length;i++)
					{
							if (document.frm_buscarUsuario.elements[i].checked==true)
							{
								contador++;							
							}
					}
					
			if(contador>0)
				{
				document.frm_buscarUsuario.action="eliminarUsuario.php";
				document.frm_buscarUsuario.submit();
				}
			else
				alert('No ha seleccionado ningun Alumno')
			}
		}
		
		</script> 
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><?=menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td><form name="frm_buscarUsuario" method="post" action="<?=$PHP_SELF?>">
				
				<table class="tablaGeneral" >
					<tr class="trSubTitulo">
						  <td width="20%">Buscar Usuarios:</td>
			      <td width="50%"><input type="text" name="txt_buscarUsuario" size="50" value="<?=$_POST['txt_buscarUsuario']?>">
					        <input type="submit" name="sub_buscar" value="Buscar"></td>
							<td>Mostrar: </td>
							<td>
							<select name="cbo_tipoUsuario" onChange="javascript: enviarOrdenar()">
								<option value="0">Todos los Usuarios</option>
								<option value="1">Administradores</option>
								<option value="2">Alumnos</option>
								<option value="3" >Profesores</option>					
								<option value="4" selected>Ver por Materias</option>							
								<option value="5" selected>Ver por Profesor y Materias</option>							
		
							</select>
							</td>
					</tr>
				
				  </table><br>
				<?
// BUSQUEDA
				if(!empty($_POST['txt_buscarUsuario']))
				{
					$cadena=explode('+',$_POST['txt_buscarUsuario']);
					$contPalabras=0;
					foreach($cadena as $palabra)
					{
						if(empty($busqueda))
						{
							$busqueda="mei_usuario.idusuario LIKE '%".$palabra."%'";
							$busqueda.=" OR CONCAT(mei_usuario.primernombre,' ',mei_usuario.segundonombre,' ',mei_usuario.primerapellido,' ',mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
						}
						else
						{
							$busqueda.=" OR mei_usuario.idusuario LIKE '%".$palabra."%'";
							$busqueda.=" OR CONCAT(mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
						}
			
					}
					$sql="SELECT mei_usuario.idtipousuario,mei_usuario.idusuario,
					mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.estado,mei_usuario.administrador 
						FROM mei_usuario WHERE ".$busqueda;
					$resultado=$baseDatos->ConsultarBD($sql);
					$numRegistros=mysql_num_rows($resultado);
					if($numRegistros>0)
					{
						if($numRegistros == 1)
						{
							$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registro encontrado.";
						}
						else
						{
							$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registros encontrados.";
						}
					
					?>
					<table class="tablaGeneral" >
					  <tr class="trSubTitulo">
					  	<td colspan="6"><?= $cadenaResultado?></td>
					  </tr>
						<tr class="trSubTitulo">
						  <td width="15%" class="trSubTitulo" colspan="2"><div align="center">						    &nbsp;&nbsp;C&oacute;digo</div></td>
							<td width="65%" class="trSubTitulo"><div align="center">Nombre del Alumno</div></td>					
							<td width="10%" class="trSubTitulo"><div align="center">Editar</div></td>					
							<td width="10%" class="trSubTitulo" colspan="2"><div align="center">Reiniciar</div></td>					
						</tr>
					<?
						$cont=0;
						while(list($codigoTipo,$codigoUsuario,$nombre1,$nombre2,$apellido1,$apellido2,$estado,$administrador)=mysql_fetch_array($resultado))
						{
								if($cont%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}
						
					?>
								<tr class="trListaClaro">
								<td class="<?=$color?>">&nbsp;<?=$cont+1?></td>
									<td class="<?=$color?>">&nbsp;<?=$codigoUsuario?></td>
									<td class="<?=$color?>">&nbsp;<?= $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2 ?></td>					
									<td class="<?=$color?>" align="center">
										<table class="tablaPrincipal" >
											<tr>
												<td width="35%"><div align="center"><a href="modificarUsuario.php?codigoUsuario=<?=$codigoUsuario?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Usuario" width="16" height="16" border="0"></a></div></td>
												<td width="30%">&nbsp;</td>
												<?
												if($_SESSION['idusuario']!=$codigoUsuario)
												{
												?>
												<td width="35%"><div align="center"><a href="javascript: eliminarUsuario('<?= $codigoUsuario?>')"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Usuario" width="16" height="16" border="0"></a></div></td>
												<?
												}
												else
												{
												?>
												<td width="35%"></td>
												<?
												}
												?>
											</tr>
										</table>
									</td>
									<?
									if($estado==1)
									{
									?>
									<td colspan="2" class="<?=$color?>" align="center"><a href="javascript: reiniciarUsuario('<?= $codigoUsuario?>')"  class="link"><img src="imagenes/activo.gif" alt="Usuario Activo" width="16" height="16" border="0"></a></div></td>
									<?
									}
									else  if($estado==0)
									{
									?>
									<td colspan="2" class="<?=$color?>" align="center"><img src="imagenes/inactivo.gif" alt="Usuario Inactivo" width="16" height="16" border="0"></a></div></td>
									<?
									
									}
									?>
								</tr>
								
					<?
							$cont++;
						}
					?>
						</table>					
						
					<?
					}
					else
					{
					?>
						<b>	<?=$codigoUsuario?></b><br>
						<table class="tablaGeneral" >
						  <tr class="trAviso">
							<td colspan="6">No se encontraron coincidencias con <i><?=$_POST['txt_buscarUsuario']?></i> </td>
						  </tr>
				  </table>
					<?
					}
				}

//FIN BUSQUEDA				
				
				
				?>
					<input name="hid_contUsuario" type="hidden" value="<?=$cont?>">
				</form>
				
				<form name="frm_usuario" method="post">
			<?
				$sql="SELECT mei_usuario.idusuario FROM mei_usuario";
				$resultado=$baseDatos->ConsultarBD($sql);
				list($codigoUsuario)=mysql_fetch_array($resultado);
				
				if(!empty($codigoUsuario))
				{
				?>
					<table class="tablaGeneral" >
					  <tr class="trTitulo">
					  	<td colspan="5" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Ver Profesores </td>
					  </tr>
                  </table><br>
<?
				$sql="SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
								mei_usuario.primerapellido, mei_usuario.segundoapellido	FROM mei_usuario WHERE mei_usuario.idtipousuario=2 ORDER BY mei_usuario.primerapellido, mei_usuario.segundoapellido";
				$profesor=$baseDatos->ConsultarBD($sql);
				while (list($codigoProfesor,$primernombre,$segundonombre,$primerapellido,$segundoapellido)=mysql_fetch_array($profesor))
					{
?>
					
			  <table class="tablaGeneral" >                        
                        <tr>
                            <td class="trTitulo" align="center">Profesor:</td>
                            <td class="trTitulo" align="center"><?=$codigoProfesor?></td>
                            <td colspan="2" class="trTitulo">&nbsp;&nbsp;<?=$primernombre." ". $segundonombre.' '.$primerapellido.' '.$segundoapellido?></td>
                        </tr>
<?
					$sql="SELECT  mei_materia.idmateria,mei_materia.nombre, mei_materia.codigo FROM  mei_materia WHERE mei_materia.idmateria IN ( SELECT mei_grupo.idmateria FROM mei_grupo, mei_relusugru,mei_usuario WHERE mei_grupo.idgrupo=mei_relusugru.idgrupo AND mei_usuario.idtipousuario=2 AND mei_relusugru.idusuario=".$codigoProfesor.")";							
							$resultado=$baseDatos->ConsultarBD($sql);
							$numeroMaterias=mysql_num_rows($resultado);
				?>						
				<tr class="trSubTitulo">
					<td width="15%" class="trSubTitulo" colspan="2" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C&oacute;digo</td>
					<td colspan="2" class="trSubTitulo"><div align="center">Nombre de la Materia</div>
					  </div></td>					
				</tr>
							<?
							$cont=0;
							while (list($idMateria,$nombreMateria,$codigoMateria)=mysql_fetch_array($resultado))
							{	
									if($cont%2==0)
										$color="trListaClaro";
									else
										$color="trListaOscuro";
									?>
				<tr class="<?=$color?>">
					<td class="<?=$color?>" align="center" width="1%">&nbsp;<?=$cont+1?></td>
					<td class="<?=$color?>" align="center">&nbsp;<?= $codigoMateria?></td>
					<td class="<?=$color?>">&nbsp;<?= $nombreMateria?> </td>					
				</tr>
<?
								$cont++;
							}//FIN WHILE
?>					  
					</table><br>
<?							
						}
				}
				else
				{
				?>
					<table class="tablaGeneral" >
						<tr class="trTitulo" >
						  <td><img src="imagenes/usuario.gif" width="16" height="16">Ver Profesores</td>
					  </tr>
					  <tr class="trAviso">
					  	<td class="trAviso">Actualmente no existen usuarios registrados</td>
					  </tr>
				  </table>
				<?
				}
		
				?>
					<input name="hid_contUsuario" type="hidden" value="<?=$cont?>">
					
		</form>
			  </td>
              <td class="tablaEspacio">&nbsp;</td>
              <td class="tablaDerecho">&nbsp;</td>
              <td class="tablaEspacioDerecho">&nbsp;</td>                            
			</tr>
		</table>
<?
	if($_GET['error'] == 1)
	{
	
?>
	<script language="javascript">
		alert("No se puede eliminar al profesor porque tiene grupos registrados.");
	</script>
<?
	}
?>		
		</body>
		</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
	
?>