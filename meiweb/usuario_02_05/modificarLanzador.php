<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once('../calendario/FrmCalendario.class.php');

	if(comprobarSession())
	{
	if ($_SESSION['idtipousuario']==2)
	{
			$codigoLanzador=$_GET['codigoLanzador'];
			
			$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.titulo,mei_lanzador.descripcion,
				mei_lanzador.tamano,mei_lanzador.fechacarga,mei_lanzador.lanzador,mei_lanzador.destino, mei_lanzador.ubicacion,mei_lanzador.tipo 
					FROM mei_lanzador WHERE mei_lanzador.idlanzador=".$_GET['codigoLanzador'];
			$resultado=$baseDatos->ConsultarBD($sql);
			list($codigoLanzador,$titulo,$descripcion,$tamano,$fechaCarga,$lanzador,$destino,$ubicacion,$tipo)=mysql_fetch_array($resultado);

			if($destino==0)
			{
				$destinoTodos="checked";
				$gruposCheck="checked";
			}
			else if($destino==1)
			{
				$destinoSeleccionados="checked";
				$sql="SELECT mei_rellangru.idgrupo FROM mei_rellangru WHERE mei_rellangru.idlanzador =".$_GET['codigoLanzador'];
				$consulta=$baseDatos->ConsultarBD($sql);
				
				$cont=0;
				
				while(list($datoGrupo)=mysql_fetch_array($consulta))
				{
					$listaGrupos[$cont++]=$datoGrupo;
				}					
							
			} 

?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
        </style>
		</head>
		<body>
				<script> 
			
			function chequear()
			{
				if(document.frm_modificarLanzador.chk_archivo.checked)
				{
					document.frm_modificarLanzador.fil_archivo.disabled=false;
				}
				else
				{
					document.frm_modificarLanzador.fil_archivo.disabled=true;
				}
			}
			
			function enviarCancelar()
			{
				location.replace("../lanzadores/");
			}
				
			function validar()
			{
			
				if(document.frm_modificarLanzador.txt_tituloLanzador.value!="" && document.frm_modificarLanzador.txt_descripcionLanzador.value!="" )
				document.frm_modificarLanzador.submit();
				else
				alert ("Debe llenar completamente la información solicitada");
			}
		function chk_habilitar()
		{
			for(i=0;i<document.frm_cartelera.elements.length;i++)
			{
				if(document.frm_cartelera.elements[i].id=="grupo")
				document.frm_cartelera.elements[i].disabled=false;
			}	
		}
		
		function chk_deshabilitar()
		{
			for(i=0;i<document.frm_cartelera.elements.length;i++)
			{
				if(document.frm_cartelera.elements[i].id=="grupo")
				document.frm_cartelera.elements[i].checked=true;
			}
		}
		 
		function chk_click ()
		{
			var radioTodos=document.getElementById('radioT');
			var radioSelec=document.getElementById('radioS');	
			radioTodos.checked=false;
			radioSelec.checked=true;	
		}
		</script> 

		<table class="tablaPrincipal">
			<tr valign="top">
				<td><? menu($_SESSION['idtipousuario']); ?></td>
				<td>
					<table class="tablaGeneral">
					<form name="frm_modificarLanzador" method="post" action="guardarModificacion.php?codigoLanzador=<?=$codigoLanzador?>" enctype="multipart/form-data">
						<tr>
							<td colspan="2" class="trTitulo"><img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar Lanzador </td>
						</tr>
						<?
						if($_GET['error'] == "0x001")
						{
						?>
						<tr class="trInformacion">
						  <td colspan="2"><img src="imagenes/error.gif" width="17" height="18"> <SPAN class=Estilo1>Error: No se pudo cargar el archivo, excede el tama&ntilde;o permitido por el servidor. Consulte con el Administrador del sistema </SPAN></td>
					  </tr>
					  <?
					  }
					  ?>
						<tr class="trInformacion">
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
					  </tr>
						<tr class="trInformacion">
							<td width="25%"><strong>&nbsp;T&iacute;tulo Lanzador:</strong></td>
							<td width="75%"><input name="txt_tituloLanzador" type="text" value="<?=$titulo?>" size="80" maxlength="80"></td>
						</tr>
						
						<tr class="trInformacion">
							<td><strong>&nbsp;Descripci&oacute;n lanzador:</strong></td>
							<td><input name="txt_descripcionLanzador" type="text" value="<?=$descripcion?>" size="80" maxlength="80"></td>
						</tr>
                        <? 
						 if($tipo==1) {
						?>
                        <tr class="trInformacion">
							<td><strong>URL:</strong></td>
							<td><input name="txt_Lanzador" type="text" value="<?=$ubicacion?>" size="80" maxlength="80"></td>
						</tr>
                        <?
						}
                        else{
						?>
                        <tr class="trInformacion">
							<td>&nbsp;</td>
						</tr>
                        <?
                        }
						?>
                        
						<tr class="trInformacion">
						  <td colspan="3"><strong>
						    <input name="chk_archivo" type="checkbox"  onClick="chequear();" value="1">
&nbsp;Reemplazar Archivo</strong>&nbsp;</td>
					  </tr>
						<tr class="trInformacion">
						  <td><strong>&nbsp;Nuevo Archivo:</strong></td>
						  <td colspan="2"><input name="fil_archivo" type="file" disabled></td>
					  </tr>
						
						<tr class="trInformacion">
							<td colspan="3" valign="top">
								<table class="tablaPrincipal" width="100%" bgcolor="#DDDDDD">
									<tr>
										<td colspan="5"><b>Destino:</b></td>
									</tr>
									<tr>
									  <td height="40" width="3%"><input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar()" <?= $destinoTodos?>  id="radioT"></td>
										<td width="22%">Todos mis Grupos </td>
										<td width="3%"><input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()" <?= $destinoSeleccionados?> id="radioS"></td>
										<td colspan="2" width="72%">Grupos Seleccionados</td>
									</tr>
								<?
								$contChk=0;
								  
								$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia 
											WHERE mei_materia.idmateria
												IN (
													SELECT mei_grupo.idmateria FROM mei_grupo 
															WHERE mei_grupo.idgrupo
																IN (
																	SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																			WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
																	)
													) GROUP BY (mei_materia.idmateria)"; 
								
								$materias=$baseDatos->ConsultarBD($sql);
								
								while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
								{
								?>
									<tr>
										<td colspan="2">&nbsp;</td>
										<td colspan="2"><?= $materiaNombre?></td>
									</tr>
									<?
									$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo 
												WHERE mei_grupo.idgrupo 
													IN (
														SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
														) AND mei_grupo.idmateria =".$materiaCodigo;
									
									$grupos=$baseDatos->ConsultarBD($sql);
									
									while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
									{
											if(sizeof($listaGrupos) > 0)
											{
												if(in_array($grupoCodigo,$listaGrupos))
												{
													$checkedGrupo="checked";
												}
												else
												{
													$checkedGrupo=" ";
												}
											}
									
									?>
									<tr>
										<td width="26">&nbsp;</td>
										<td width="26">&nbsp;</td>
										<td width="33"><input id="grupo" name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>" <?= $checkedGrupo?> <?= $gruposCheck?>></td>
										<td width="280"><?= $grupoNombre?></td>
									</tr>
								<?
									$contChk++;
									}
								}  
								?>
                                  <input type="hidden" name="hid_contGrupo" value="<?=$contChk?>">
								
								</table>
							</td>
						</tr>
						<tr class="trInformacion">
							<td colspan="2"><div align="center"><input type="button" name="btn_modificarLanzador" value="Modificar" onClick="javascript:validar()">
							  <input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()">
							</div></td>
						</tr>
					</form>
					</table>
			  </td>
			</tr>
		</table>
		</body>
		</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
?>
