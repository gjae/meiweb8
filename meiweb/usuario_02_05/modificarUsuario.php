<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/validar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	
	if(comprobarSession)
	{
						
				$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
					FROM mei_usuario WHERE mei_usuario.idusuario=".$_GET['codigoUsuario'];
				$resultado=$baseDatos->ConsultarBD($sql);
				list($codigoUsuario,$nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($resultado);
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<script language="javascript"> 
					function enviarCancelar()
					{
						location.replace("listarUsuariosAdministrador.php");
					}
		
			function validar()
			{
				if(document.frm_modificarUsuario.txt_nombre1.value!="" && document.frm_modificarUsuario.txt_apellido1.value!="")
				document.frm_modificarUsuario.submit();
				else
				alert ("Debe llenar todos los campos");
			}
			
		</script> 
		
		<table width="563" class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table width="100%" class="tablaGeneral">
					<form name="frm_modificarUsuario" method="post" action="guardarModificacionUsuario.php?idUsuario=<?=$codigoUsuario?>">
						<tr class="trTitulo">
							<td colspan="2" class="trTitulo"><img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar Usuario</td>
						</tr>
				<?
					if($_GET['error'] == '0x001')
					{
				?>
				<?
					}
				?>
						
						<tr class="trInformacion">
						  <td>&nbsp;</td>
						  <td>&nbsp;</td>
					  </tr>
						<tr class="trInformacion">
							<td width="25%"><b>C&oacute;digo Usuario:</b></td>
							<td width="75%"><?=$codigoUsuario?></td><input name="hid_codigo" type="hidden" value="<?=$codigoUsuario?>">
						</tr>
						<tr class="trInformacion">
							<td><b>Primer Nombre:</b></td>
							<td><input name="txt_nombre1" type="text" value="<?=$nombre1?>" size="40"></td>
						</tr>
						<tr class="trInformacion">
							<td><b>Segundo Nombre:</b></td>
							<td><input name="txt_nombre2" type="text" value="<?=$nombre2?>" size="40"></td>
						</tr>
						<tr class="trInformacion">
							<td><b>Primer Apellido:</b></td>
							<td><input name="txt_apellido1" type="text" value="<?=$apellido1?>" size="40"></td>
						</tr>
						<tr class="trInformacion">
							<td><b>Segundo Apellido:</b></td>
							<td><input name="txt_apellido2" type="text" value="<?=$apellido2?>" size="40"></td>
						</tr>
						<?
						
					$sql="SELECT mei_usuario.idtipousuario,mei_usuario.administrador FROM mei_usuario WHERE mei_usuario.idusuario=".$codigoUsuario;
					$consulta=$baseDatos->ConsultarBD($sql);
					list($codigoTipo,$administrador)=mysql_fetch_array($consulta);
						
						if ($codigoTipo==2)
						{
							if(empty($administrador))
							{
								$activo="";
							}
							else
							{
								$activo="checked";
							}
						
						?>
						<tr class="trInformacion">
							<td colspan="2"><input type="checkbox" name="chk_administrador" value="1" <?=$activo?>>
							  <b>						    Activar este usuario como Administrador
                            </b> </td>
						</tr>
						<?
						}
						?>
						<tr class="trInformacion">
							<td colspan="2"><div align="center"><input type="button" name="btn_modificarUsuario" value="Modificar" onClick="javascript:validar()">
							  <input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()">
							</div></td>
						</tr>
						<input name="hid_codigoAnterior" type="hidden" value="<?=$codigoUsuario?>">
					</form>
				  </table>
			  </td>
            <td class="tablaEspacio">&nbsp;</td>
			<td class="tablaDerecho">&nbsp;</td>            
            <td class="tablaEspacioDerecho">&nbsp;</td>              
			</tr>
		</table>
		</body>
		</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
?>
