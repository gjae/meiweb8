<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/validar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../baseDatos/BD.class.php'); 
	include_once ('../librerias/vistas.lib.php');

	$baseDatos=new BD();
	if(comprobarSession)
	{
		$sql="SELECT mei_usuario.idusuario,mei_usuario.documento,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,
			mei_usuario.segundoapellido,mei_usuario.correo FROM mei_usuario WHERE mei_usuario.idusuario=".$_GET['codigoUsuario'];
		$resultado=$baseDatos->ConsultarBD($sql);
		list($codigoUsuario,$documento,$nombre1,$nombre2,$apellido1,$apellido2,$correo)=mysql_fetch_array($resultado);
		
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
</head>
	<style type="text/css">
		<!--
			.Estilo1 {color: #FF0000}
		-->
	</style>
<body>
	<script language="javascript"> 
		function enviarCancelar()
		{
			location.replace("VirListarUsuariosAdministrador.php");
		}
		
		function validar()
		{
			if(document.frm_modificarUsuario.txt_nombre1.value=="" || document.frm_modificarUsuario.txt_apellido1.value=="" || document.frm_modificarUsuario.txt_documento.value=="")
				alert ("Debe llenar todos los campos");
			else if (validarCorreo())
				document.frm_modificarUsuario.submit();
		}
		
		function validarCorreo()
		{
			if(document.frm_modificarUsuario.txt_correo.value=="")
			{
				alert("Debe ingregar una dirección de correo electronico");
				document.frm_modificarUsuario.txt_correo.focus();
				return false;
			}
			else if(document.frm_modificarUsuario.txt_correo.value.search(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig))
			{
				alert("El correo electronico no es válido, escribalo en forma correcta");
				document.frm_modificarUsuario.txt_correo.focus();
				return false;
			}
			else
			{
				return true;
			}
		}
		
	</script> 
		
	<table width="563" class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><? menu(1);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td><table class="tablaGeneral">
				<form name="frm_modificarUsuario" method="post" action="VirGuardarModificacionUsuario.php?idUsuario=<?=$codigoUsuario?>">
				<tr class="trTitulo">
					<td colspan="2" class="trTitulo"><img src="imagenes/modificar.gif" width="16" height="16" align="texttop">&nbsp;Modificar Usuario</td>
				</tr>
<?
		if($_GET['error'] == '0x001')
		{
?>
					<tr class="trAviso">
						<td colspan="3"><img src="imagenes/error.gif" width="17" height="18"><span class="Estilo1">Error: Ya existe un profesor registrado con ese documento </span></td>
					</tr>
<?
		}
?>
                
				<tr class="trInformacion">
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr class="trInformacion">
					<td><b>&nbsp;C&oacute;digo:</b></td>
					<td><?=$codigoUsuario?></td><input name="hid_codigo" type="hidden" value="<?=$codigoUsuario?>">
				</tr>
				<tr class="trInformacion">
					<td><strong>&nbsp;N° Documento : </strong></td>
					<td><input name="txt_documento" type="text" value="<?=$documento?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					<td><strong>&nbsp;Primer Nombre : </strong></td>
					<td><input name="txt_nombre1" type="text" value="<?=$nombre1?>" size="40"></td>
				</tr>                
				<tr class="trInformacion">
					<td><strong>&nbsp;Segundo Nombre : </strong></td>
					<td><input name="txt_nombre2" type="text" value="<?=$nombre2?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					<td><strong>&nbsp;Primer Apellido : </strong></td>
					<td><input name="txt_apellido1" type="text" value="<?=$apellido1?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					<td><strong>&nbsp;Segundo Apellido :</strong></td>
					<td><input name="txt_apellido2" type="text" value="<?=$apellido2?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					  <td><strong>&nbsp;Correo Electronico:</strong></td>
					<td><input name="txt_correo" type="text" value="<?=$correo?>" size="40"></td>
				</tr>
<?
		$sql="SELECT mei_usuario.idtipousuario,mei_usuario.administrador FROM mei_usuario WHERE mei_usuario.idusuario=".$codigoUsuario;
		$consulta=$baseDatos->ConsultarBD($sql);
		list($codigoTipo,$administrador)=mysql_fetch_array($consulta);
		if ($codigoTipo==5)
		{
			if(empty($administrador))
				$activo="";
			else
				$activo="checked";
?>
				<tr class="trInformacion">
					<td colspan="2">&nbsp;<input type="checkbox" name="chk_administrador" value="1" <?=$activo?> disabled><b>&nbsp;Activar este usuario como Administrador</b></td>
				</tr>
<?
		}
?>
				<tr class="trInformacion">
					<td colspan="2"><div align="center">
						<input name="btn_reset" type="reset" value="Restablecer">                   
						<input type="button" name="btn_modificarUsuario" value="Modificar" onClick="javascript:validar()">
						<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div>
					</td>
				</tr>
					<input name="hid_codigoAnterior" type="hidden" value="<?=$codigoUsuario?>">
				</form>
				</table>
	    </td>
            <td class="tablaEspacio">&nbsp;</td>
			<td class="tablaDerecho">&nbsp;</td>            
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
?>