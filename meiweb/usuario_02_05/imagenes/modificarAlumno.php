<?PHP
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');		
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
	if ($_SESSION['idtipousuario']==2)
	{
	
				$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, idtipousuario 
					FROM mei_usuario WHERE mei_usuario.idusuario=".$_GET['codigoUsuario'];
				$resultado=$baseDatos->ConsultarBD($sql);
				list($codigoUsuario,$nombre1,$nombre2,$apellido1,$apellido2, $idtipousu)=mysql_fetch_array($resultado);
			
			
			
		?>
			<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
				<script language="javascript">
				
					function validar()
					{
						if(comprobarDatos())
						{
							document.frm_modificarUsuario.submit();
						}
					}
					
					function enviarCancelar()
					{
						location.replace("listarProfesor.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>");
					}
					
					function comprobarDatos()
					{
						if(document.frm_modificarUsuario.txt_primerNombre.value == false || !isNaN(parseInt(document.frm_modificarUsuario.txt_primerNombre.value)))
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(!isNaN(parseInt(document.frm_modificarUsuario.txt_segundoNombre.value)))
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(document.frm_modificarUsuario.txt_primerApellido.value == false || !isNaN(parseInt(document.frm_modificarUsuario.txt_primerApellido.value)))
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(!isNaN(parseInt(document.frm_modificarUsuario.txt_segundoApellido.value)))
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else
						{
							return true;
						}
					}
								
				
				</script>
				
				<style type="text/css">
				<!--
				.Estilo1 {color: #FF0000}
				-->
				</style>
				
				</head>
				<body>
				
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><?PHP menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<form name="frm_modificarUsuario" method="post" action="guardarModificacionAlumno.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&idUsuario=<?PHP echo $codigoUsuario; ?>">
									<table class="tablaGeneral" >
							  <tr class="trTitulo">
								<td colspan="2" class="trTitulo"><img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar Alumno </b> 
								</td>
							  </tr>
							  <tr class="trSubTitulo">
									<td colspan="2" >&nbsp;</td>
								</tr>
					<tr class="trInformacion">
					  <td width="20"><b>C&oacute;digo:</b></td>
					  <td width="80%"><?PHP echo $codigoUsuario; ?><input name="hid_codigo" type="hidden" value="<?PHP echo $codigoUsuario; ?>"></td>
					  </tr>
					<tr class="trInformacion">
						<td><b>Primer Nombre : </b></td>
						<td><input name="txt_primerNombre" type="text" id="txt_primerNombre" value="<?PHP echo  $nombre1; ?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td><b>Segundo Nombre : </b></td>
						<td><input name="txt_segundoNombre" type="text" id="txt_segundoNombre" value="<?PHP echo  $nombre2; ?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td><b>Primer Apellido : </b></td>
						<td><input name="txt_primerApellido" type="text" id="txt_primerApellido" value="<?PHP echo  $apellido1; ?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td><b>Segundo Apellido :</b></td>
						<td><input name="txt_segundoApellido" type="text" id="txt_segundoApellido" value="<?PHP echo  $apellido2; ?>" size="40">
					    <input name="hid_idGrupo" type="hidden" id="hid_idGrupo" value="<?PHP echo  $_GET['idGrupo']; ?>"></td>
					</tr>
					<tr class="trInformacion">
						<td><b>Estudiante auxiliar :</b></td>
						<td>
							<input name="chk_tipousuario" type="checkbox" id="chk_tipousuario" value="<?PHP echo  $idtipousu; ?>" size="40" <?PHP if ($idtipousu == 7) { echo "checked=true"; } ?>>
					    </td>
					</tr>
					<tr class="trInformacion">
					  <td colspan="2">&nbsp;</td>
					  </tr>
					<tr class="trInformacion">
						<td colspan="2"><div align="center">	
						<input type="button" name="btn_modificarUsuario" value="Modificar" onClick="javascript:validar()">
						<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div></td>
				  </tr>
					<tr class="trInformacion">
					  <td colspan="2">&nbsp;</td>
					  </tr>
				</table>
				  </form>
			  </td>
            <td class="tablaEspacio">&nbsp;</td>
			<td class="tablaDerecho"><?PHP echo menu1($_SESSION['idtipousuario']);?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>              
		  </tr>
				</table>

				</body>
			</html>
		<?PHP 
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}

?>
