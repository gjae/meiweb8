<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');
	
	$baseDatos=new BD();
	if(comprobarSession())
	{ 
		if ($_SESSION['idtipousuario']==5)
		{
			if(empty($_POST['cbo_ordenar']))
				$_POST['cbo_ordenar']=3;
			if($_POST['cbo_ordenar']==2)
				$activoNombre="selected";
			else if($_POST['cbo_ordenar']==1)
				$activoCodigo="selected";
			else 
				$activoApellido="selected";
			if(!empty($_POST['idvirgrupo']))
			{
				$sql="SELECT mei_virgrupo.idvirmateria FROM  mei_virgrupo WHERE mei_virgrupo.idvirgrupo='".$_POST['idvirgrupo']."'";
				$consultaMateria=$baseDatos->ConsultarBD($sql);
				list($idvirmateria)=mysql_fetch_array($consultaMateria);
				$_POST['cbo_verUsuario']=$_POST['idvirgrupo'].",".$idvirmateria;			
			}
			$cont=0;
			$contChk=0;
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?= recuperarVariableSistema("sistematema")?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
</head>
<body>
<script> 
	function chekear(codigoGrupo)
	{
		var todo=document.getElementById('grupo'+codigoGrupo);
		if (todo.checked==true)
		{
			for(i=0;i<document.frm_usuario.elements.length;i++)
			{
				if(document.frm_usuario.elements[i].id=="codigoGrupo")
					document.frm_usuario.elements[i].checked=true;
			}
		}
		else
		{
			for(i=0;i<document.frm_usuario.elements.length;i++)
			{
				if(document.frm_usuario.elements[i].id=="codigoGrupo")
					document.frm_usuario.elements[i].checked=false;
			}
		}
	}
		
	function enviar()
	{
		document.frm_verUsuario.submit();				
	}
	
	function crearSubgrupo(idgrupo)
	{
		location.replace("../materias/VirCrearSubgrupo.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idGrupo="+idgrupo);
	}
	
	function ordenarLista()
	{
		document.frm_verUsuario.submit();				
	}
	
	var ventanaDocumento=false;
	function imprimirDocumento(idUsuario,idGrupo,orden)
	{		
		if (typeof ventanaDocumento.document == 'object')
		{
			ventanaDocumento.close()
		}
		ventanaDocumento = window.open('../usuario/imprimirListaPerfil.php?idUsuario='+idUsuario+'&idGrupo='+idGrupo,'ImprimirDocumento','width=700,height=650,left=10,top=10,scrollbars=YES,menubars=NO,status=NO,statusbar=NO,resizable=NO,location=NO');
	}
	
	function verBitacora(codigoUsuario)
	{
		location.replace("../bitacora/index.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idUsuario="+codigoUsuario);					
	}
	
	function verFoto(nombreImagen,imagen)
	{
		window.open('../scripts/verImagenUsuario.php?imagen='+imagen+'&nombreImagen='+nombreImagen,'Usuario','width=200,height=200,left=1010,top=250,scrollbars=NO,menubars=NO,status=NO,statusbar=NO,status=NO,resizable=NO,location=NO');
	}
	
</script>

	<form name="frm_verUsuario" method="post" action="<?= $PHP_SELF ?>">
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td><table class="tablaGeneral" >
					<tr class="trSubTitulo">
						<td>Buscar Alumno:</td>
						<td colspan="3"><input name="txt_buscarUsuario" type="text" value="<?= $_POST['txt_buscarUsuario']?>" size="50">
							<input type="submit" name="sub_buscar" value="Buscar"></td>
					</tr>
					<tr class="trSubTitulo">
						<td width="25%">Ver Alumnos de:</td>
						<td width="37%">
							<select name="cbo_verUsuario" onChange="javascript:enviar()">
								<option class="link" value="1">Todos los Cursos</option>
<?
			$sql="SELECT mei_virgrupo.idvirgrupo,mei_virgrupo.nombre,mei_virmateria.idvirmateria,mei_virmateria.nombre 
				FROM mei_virgrupo, mei_virmateria WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo 
				FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")
				AND mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria ";
			$resultado=$baseDatos->ConsultarBD($sql);
			while (list($codigoGrupo,$grupo,$codigoMateria,$materia)=mysql_fetch_array($resultado))
			{
				$codigoGrupoS=$codigoGrupo+10;
				unset($seleccion); 
				if($codigoGrupoS.','.$codigoMateria==$_POST['cbo_verUsuario'])
					$seleccion="selected";
?>
								<option class="link" value="<?= $codigoGrupoS.','.$codigoMateria?>" <?=$seleccion?>><?=$grupo.' '.$materia?>
<?
			}
?>
							</select></td>
						<td width="25%"><div align="center">Ordenar Lista Por:</div></td>
						<td width="13%"><select name="cbo_ordenar" onChange="javascript:ordenarLista()">
							<option value="1" <?=$activoCodigo?>>C&oacute;digo</option>
							<option value="2" <?=$activoNombre?>>Nombre</option>
							<option value="3" <?=$activoApellido?>>Apellido</option>
							</select></td>
					</tr>
				</table>
				<p>
<?
// BUSQUEDA
			if(!empty($_POST['txt_buscarUsuario']))
			{
				$cadena=explode(' ',$_POST['txt_buscarUsuario']);
				foreach($cadena as $palabra)
				{
					if(empty($busqueda))
					{
						$busqueda="mei_usuario.idusuario LIKE '%".$palabra."%'";
						$busqueda.=" OR CONCAT(mei_usuario.primernombre,' ',mei_usuario.segundonombre,' ',mei_usuario.primerapellido,' ',mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
					}
					else
					{
						$busqueda.=" OR mei_usuario.idusuario LIKE '%".$palabra."%'";
						$busqueda.=" OR CONCAT(mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
					}
				}
				if($_POST['cbo_ordenar']==1)
				{
					$sql="SELECT mei_virgrupo.nombre,mei_virgrupo.idvirgrupo,mei_usuario.idtipousuario,mei_usuario.idtipoalumno,mei_usuario.idusuario,
							mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,
							mei_usuario.correo,mei_usuario.telefono,mei_usuario.estado,mei_usuario.fechanacimiento,mei_usuario.imagen 
							FROM mei_virgrupo,mei_usuario,mei_relusuvirgru 
							WHERE mei_virgrupo.idvirgrupo=mei_relusuvirgru.idvirgrupo 
							AND  mei_relusuvirgru.idvirgrupo 
							IN (
									SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
									WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."
								)
								AND mei_relusuvirgru.idusuario=mei_usuario.idusuario 
								AND mei_usuario.idtipousuario=6 
								AND (".$busqueda.") ORDER BY(mei_usuario.idusuario)";
				}
				else if($_POST['cbo_ordenar']==3)
				{
					$sql="SELECT mei_virgrupo.nombre,mei_virgrupo.idvirgrupo,mei_usuario.idtipousuario,mei_usuario.idtipoalumno,mei_usuario.idusuario,
							mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,
							mei_usuario.correo,mei_usuario.telefono,mei_usuario.estado,mei_usuario.fechanacimiento,mei_usuario.imagen 
							FROM mei_virgrupo,mei_usuario,mei_relusuvirgru 
							WHERE mei_virgrupo.idvirgrupo=mei_relusuvirgru.idvirgrupo 
							AND  mei_relusuvirgru.idvirgrupo 
							IN (
									SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
									WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."
								)
								AND mei_relusuvirgru.idusuario=mei_usuario.idusuario 
								AND mei_usuario.idtipousuario=6 
								AND (".$busqueda.") ORDER BY(mei_usuario.primerapellido)";
				}
				else
				{
					$sql="SELECT mei_virgrupo.nombre,mei_virgrupo.idvirgrupo,mei_usuario.idtipousuario,mei_usuario.idtipoalumno,mei_usuario.idusuario,
							mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,
							mei_usuario.correo,mei_usuario.telefono,mei_usuario.estado,mei_usuario.fechanacimiento,mei_usuario.imagen 
							FROM mei_virgrupo,mei_usuario,mei_relusuvirgru 
							WHERE mei_virgrupo.idvirgrupo=mei_relusuvirgru.idvirgrupo 
							AND  mei_relusuvirgru.idvirgrupo 
							IN (
									SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
									WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."
								)
								AND mei_relusuvirgru.idusuario=mei_usuario.idusuario 
								AND mei_usuario.idtipousuario=6 
								AND (".$busqueda.") ORDER BY(mei_usuario.primernombre)";
				}
				$resultado=$baseDatos->ConsultarBD($sql);
				$numRegistros=mysql_num_rows($resultado);
				if($numRegistros>0)
				{
					$color="trListaClaro";
					if($numRegistros == 1)
						$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registro encontrado.";
					else
						$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registros encontrados.";
?>
				<br>
				<br>
				<table class="tablaGeneral" >
					<tr class="trSubTitulo">
						<td colspan="4"><?= $cadenaResultado?></td>
					</tr>
					<tr class="trSubTitulo">
						<td width="15%" class="trSubTitulo" colspan="2"align="center">C&oacute;digo</td>
						<td width="65%" class="trSubTitulo" align="center">Nombre del Alumno</td>
						<td width="10%" class="trSubTitulo" align="center">Bitacora</td>
					</tr>
<?
					while(list($grupo,$codigoGrupo,$codigoTipo,$codigoTipoAlumno,$codigoUsuario,$nombre1,$nombre2,$apellido1,$apellido2,$correo,$telefono,$estado,$fecha,$imagen)=mysql_fetch_array($resultado))
					{
						list($aA,$mA,$dA)=explode('-',date("Y-m-d"));
						list($a,$m,$d)=explode('-',$fecha);
						if($a=="0000" || $m=="00" || $d=="00")
						{
							$edad="---";
						}
						else
						{
							if ($mA<$m)
							{
								$edad=$aA-$a-1;
							}
							else if ($mA>$m)
							{
								$edad=$aA-$a;
							}
							else if($mA=$m)
							{
								if($dA<$d)
								{
									 $edad=$aA-$a-1;
								}
								else
								{
									$edad=$aA-$a;
								}
							}
							$edad.=" Años";
						}
						
						if($cont%2==0)
							$color="trListaClaro";
						else
							$color="trListaOscuro";
						$nombreImagen='../../datosMEIWEB/fotosUsuarios/'.$imagen;							
 ?>
					<tr class="<?=$color?>">
						<td class="<?=$color?>" width="5%"align="right"><?=++$cont?>
						<td class="<?=$color?>" align="center">&nbsp;<?=$codigoUsuario?></td>
						<td class="<?=$color?>" title="Ver Foto">&nbsp;                          	<a href="javascript:verFoto('<?= $nombreImagen?>','<?=$imagen?>')" class="link">&nbsp;<?= $apellido1.' '. $apellido2.' '.$nombre1.' '.$nombre2 ?></a></td>
<?
						if($estado==1)
						{
?>
						<td class="<?=$color?>" align="center"><a href="javascript: verBitacora('<?= $codigoUsuario?>')"  class="link"><img src="../bitacora/imagenes/bitacora.gif" alt="Ver Registro de Bitacora de este Alumno" width="16" height="16" border="0"></a></td>
<?
						}
						else  if($estado==0)
						{
?>
						<td class="<?=$color?>" align="center"><img src="../bitacora/imagenes/bitacoraInactiva.gif" alt="No existe Bitacora de este Alumno" width="16" height="16" border="0"></td>
<?
						}
?>
					</tr>
<?
					}
?>
				</table>
                <br>
<?
				}
				else
				{
?>
				<br>
				<table class="tablaGeneral" >
					<tr class="trAviso">
						<td>&nbsp;No se encontraron coincidencias con <i><?=$_POST['txt_buscarUsuario']?></i> </td>
					</tr>
				</table>
                <br>
<?
				}
			}
//FIN BUSQUEDA				
			if(empty($_POST['cbo_verUsuario']))
				$_POST['cbo_verUsuario']=1;
			$sql="SELECT mei_virmateria.idvirmateria
					FROM mei_virgrupo, mei_virmateria WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo 
					FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")
					AND mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria";
			$resultado=$baseDatos->ConsultarBD($sql);
			list($codigoMateria)=mysql_fetch_array($resultado);
			if(!empty($codigoMateria))
			{
?>
			  <table class="tablaGeneral">
					<tr class="trTitulo">
						<td colspan="5" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Ver Alumnos</td>
					</tr>
			  </table>
<?
				switch ($_POST['cbo_verUsuario'])
				{
					case 1: 
						$sql="SELECT DISTINCT(mei_virmateria.idvirmateria),mei_virmateria.nombre 
							FROM mei_virgrupo, mei_virmateria WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo 
							FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")
							AND mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria ";
						break;
						default:
						list($codigoGrupo,$codigoMateria)=explode(',',$_POST['cbo_verUsuario']);
							$sql="SELECT mei_virmateria.idvirmateria,mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$codigoMateria;
						break;
				}
				$materias=$baseDatos->ConsultarBD($sql);
				while (list($codigoMateria,$materia)=mysql_fetch_array($materias))
				{
?>
				<table class="tablaGeneral">
					<tr class="trSubTitulo">
						<td colspan="7" class="trSubTitulo"><?=$materia?></td>
					</tr>
<?
					switch ($_POST['cbo_verUsuario'])
					{
						case 1: 
							$sql="SELECT mei_virgrupo.idvirgrupo,mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].") AND mei_virgrupo.idvirmateria=".$codigoMateria;
						break;
						default:
						list($codigoGrupo,$codigoMateria)=explode(',',$_POST['cbo_verUsuario']);
							$sql="SELECT mei_virgrupo.idvirgrupo,mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo=".($codigoGrupo-10);
						break;
					}
					$grupos=$baseDatos->ConsultarBD($sql);
					while (list($codigoGrupo,$grupo)=mysql_fetch_array($grupos))
					{
						if($_POST['cbo_ordenar']==2)
						{
							$sql="SELECT mei_usuario.idusuario,mei_usuario.idtipoalumno,mei_usuario.primernombre,mei_usuario.segundonombre,
								mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.correo,
								mei_usuario.telefono,mei_usuario.estado,mei_usuario.fechanacimiento,mei_usuario.imagen 
								FROM mei_usuario WHERE mei_usuario.idtipousuario=6 AND mei_usuario.idusuario IN (SELECT mei_relusuvirgru.idusuario 
								FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$codigoGrupo.") ORDER BY mei_usuario.primernombre";
						}
						else if($_POST['cbo_ordenar']==1)
						{
							$sql="SELECT mei_usuario.idusuario,mei_usuario.idtipoalumno,mei_usuario.primernombre,mei_usuario.segundonombre,
								mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.correo,
								mei_usuario.telefono,mei_usuario.estado,mei_usuario.fechanacimiento,mei_usuario.imagen 
								FROM mei_usuario WHERE mei_usuario.idtipousuario=6 AND mei_usuario.idusuario IN (SELECT mei_relusuvirgru.idusuario 
								FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$codigoGrupo.") ORDER BY mei_usuario.idusuario";
						}
						else 
						{
							$sql="SELECT mei_usuario.idusuario,mei_usuario.idtipoalumno,mei_usuario.primerapellido,mei_usuario.segundoapellido,
								mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.correo,
								mei_usuario.telefono,mei_usuario.estado,mei_usuario.fechanacimiento,mei_usuario.imagen 
								FROM mei_usuario WHERE mei_usuario.idtipousuario=6 AND mei_usuario.idusuario IN (SELECT mei_relusuvirgru.idusuario 
								FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$codigoGrupo.") ORDER BY mei_usuario.primerapellido";
						}
						$resultado=$baseDatos->ConsultarBD($sql);
						$numeroAlumnos=mysql_num_rows($resultado);
?>
					<tr class="trSubTitulo">
<?
						if(!empty($numeroAlumnos))
						{
?>
						<td colspan="4"align="left">
<!--							<input id="grupo<?//= $codigoGrupo?>" type="checkbox" name="chk_grupo<?//= $codigoGrupo?>" onClick="javascript:chekear('</td>/?= $codigoGrupo?>')" title="Seleccionar todos los Alumnos de este Grupo"> !-->
&nbsp;Grupo&nbsp;<?=$grupo?></td>
						<td colspan="3"align="left"><div align="right"><a href="javascript: imprimirDocumento('<?=$_SESSION['idusuario']?>','<?=$codigoGrupo?>')" class="link"><img src="imagenes/imprimir.gif" border="0" alt="Imprimir Lista de Subgrupos"> Imprimir Lista</a></div></td>
<?
						}
						else
						{
?>
						<td colspan="7"align="left" class="trSubTitulo">
<!--							<input id="grupo<?//= $codigoGrupo?>" type="checkbox" name="chk_grupo<?//= $codigoGrupo?>" onClick="javascript:chekear('</td>/?= $codigoGrupo?>')" title="Seleccionar todos los Alumnos de este Grupo"> !-->
&nbsp;Grupo&nbsp;<?=$grupo?></td>
<?
						}
?>
					</tr>
<?
						if(empty($numeroAlumnos))
						{
?>
					<tr class="trAviso">
						<td colspan="7" class="trAviso">&nbsp;No se han agregado Alumnos al Grupo&nbsp;<?=$grupo?></td>
					</tr>
					<tr>
						<td class="trInformacion" colspan="7">&nbsp;</td>
					</tr>
<?
						}
						else 
						{
							$sql="SELECT mei_subgrupo.idsubgrupo FROM mei_subgrupo WHERE mei_subgrupo.idgrupo='".$codigoGrupo."'";
							$consultaSubgrupo=$baseDatos->ConsultarBD($sql);
							$numSubgrupos=mysql_num_rows($consultaSubgrupo);
							if($numSubgrupos > 0)
							{
								if($numSubgrupos == 1)
									$cadenaSubgrupos="&nbsp;&nbsp; Existe ".$numSubgrupos." Subgrupo de Clase";
								else
									$cadenaSubgrupos="&nbsp;&nbsp; Existe ".$numSubgrupos." Subgrupos de Clase";
?>
					<tr class="trAviso">
						<td colspan="5">&nbsp;</td>
						<td colspan="2"><div align="right"><img src="imagenes/usuario.gif" width="16" height="16"><b> <a href="../materias/listarSubgrupos.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idGrupo=<?=$codigoGrupo?>" class="link">Ver Subgrupos</a></b></div></td>
					</tr>
<?
							}
?>
					<tr class="trSubTitulo">
						<td colspan="2" align="right" class="trSubTitulo">C&oacute;digo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td class="trSubTitulo" align="center">Nombre</td>
						<td class="trSubTitulo" align="center">Edad</td>					                                                
						<td class="trSubTitulo" align="center">&nbsp;&nbsp;&nbsp;Correo&nbsp;&nbsp;&nbsp;</td>
						<td class="trSubTitulo" align="center">Tel&eacute;fono</td>
						<td class="trSubTitulo" align="center">Bitacora</td>
					</tr>
<? 
							$cont=0;
							$contChk=0;
							while (list($codigoUsuario,$codigoTipoAlumno,$apellido1,$apellido2,$nombre1,$nombre2,$correo,$telefono,$estado,$fecha,$imagen)=mysql_fetch_array($resultado))
							{
								list($aA,$mA,$dA)=explode('-',date("Y-m-d"));
								list($a,$m,$d)=explode('-',$fecha);
								if($a=="0000" || $m=="00" || $d=="00")
								{
									$edad="---";
								}
								else
								{
									if ($mA<$m)
									{
										$edad=$aA-$a-1;
									}
									else if ($mA>$m)
									{
										$edad=$aA-$a;
									}
									else if($mA=$m)
									{
										if($dA<$d)
										{
											 $edad=$aA-$a-1;
										}
										else
										{
											$edad=$aA-$a;
										}
									}
									$edad.=" Años";
								}
								
								if($cont%2==0)
									$color="trListaClaro";
								else
									$color="trListaOscuro";
								$nombreImagen='../../datosMEIWEB/fotosUsuarios/'.$imagen;

	?>
						<tr class="<?=$color?>">
							<td class="<//?=$color?>" align="center" width="5%"><?=$cont+1?>
<!--							<input id="codigoGrupo" type="checkbox" name="</tr>/?= "chk_usuario".$contChk++?>" value="<//?=$codigoGrupo?>"> !-->
							</td>
						  <td class="<?=$color?>">&nbsp;<?=$codigoUsuario?></td>
						  <td class="<?=$color?>" title="Ver Foto">
                          	<a href="javascript:verFoto('<?= $nombreImagen?>','<?=$imagen?>')" class="link">&nbsp;<?= $apellido1.' '. $apellido2.' '.$nombre1.' '.$nombre2 ?></a></td>
						  <td class="<?=$color?>" align="center"><?=$edad?></td>                          
						  <td align="center" class="<?=$color?>"><a href="../correo/redactarCorreo.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$materia?>&destinatario=<?=$codigoUsuario?>" class="link"><?=$correo?></a></td>
						  <td align="center" class="<?=$color?>"><?=$telefono?></td>
	<?
								if($estado==1)
								{
	?>
						  <td class="<?=$color?>" align="center"><a href="javascript: verBitacora('<?= $codigoUsuario?>')"  class="link"><img src="../bitacora/imagenes/bitacora.gif" alt="Ver Registro de Bitacora de este Alumno" width="16" height="16" border="0"></a></td>
	<?
								}
								else  if($estado==0)
								{
	?>
						  <td width="10%" align="center" class="<?=$color?>"><img src="../bitacora/imagenes/bitacoraInactiva.gif" alt="No existe Bitacora de este Alumno" width="16" height="16" border="0"></td>
	<?
								}
	?>
						</tr>
	<?
								$cont++;
							}//FIN WHILE
?>
                        <tr>
                            <td class="trInformacion" colspan="7" align="center">&nbsp;</td>
                        </tr>
	                    <tr>
                      		<td colspan="7" class="trInformacion" align="center">
                        <input type="button" name="btn_crearAlumno" value="Crear Subgrupo" onClick="javascript: crearSubgrupo('<?= $codigoGrupo?>')"></td>
                   	 </tr> 
<?
						}
					}//FIN WHILE
?>
				</table>
                <br>
<?
				}//FIN WHILE
?>
                 <br>
<?
			}
			else
			{
?>
                 <table class="tablaGeneral" >
                    <tr class="trTitulo" >
                      <td><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Ver Usuarios</td>
                    </tr>
                    <tr class="trAviso">
                      <td>&nbsp;Actualmente no existen alumnos registrados</td>
                    </tr>
              </table>
<?
			}
// FIN Usuario Alumno (PUEDE VER ADMON)			
?>
                  <input name="hid_contUsuario" type="hidden" value="<?=$contChk?>">
          </td>
          <td class="tablaEspacio">&nbsp;</td>
          <td class="tablaDerecho"><?=menu1($_SESSION['idtipousuario']);?></td>                  
          <td class="tablaEspacioDerecho">&nbsp;</td>
	  </tr>
    </table>
	</form>
</body>
</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
?>