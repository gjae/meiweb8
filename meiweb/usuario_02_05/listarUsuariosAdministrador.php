<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/validar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	
  	$baseDatos=new BD();
	if(comprobarSession())
	{ 
		mysql_query("SET NAMES 'utf8'");
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		if($_POST['cbo_tipoUsuario']==1)
		{
			$selectedAdmin="selected";
		}
		else if($_POST['cbo_tipoUsuario']==2)
		{
			$selectedAlumnos="selected";
		}
		else if($_POST['cbo_tipoUsuario']==3)
		{
			$selectedProfesores="selected";
		}
		else if($_POST['cbo_tipoUsuario']==4)
		{
			$selectedGrupos="selected";
		}
		
		else
		{
			$selectedTodos="selected";
		}
    	/*Agregar Campo*/
    	$exists = false;
    	$db = "mei_usuario";
    	$column = "visibilidad";
    	$column_attr = "int(11) null";
    	$columns = "show columns from $db";
    	$resultcolumn = $baseDatos->ConsultarBD($resultcolumn);
    	while($c = mysql_fetch_assoc($resultcolumn)){
    		if($c['Field'] == $column){
    			$exists = true;
    			break;
    		}
    	}        
    	if(!$exists){
    		$sql = "ALTER TABLE `$db` ADD `$column`  $column_attr";
    	}					
    	$baseDatos->ConsultarBD($sql);		
    	/*Fin Agregar Campo*/ 		
		
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		</head>
		<body>
		<script>
		
                                    function desactivarUsuario(codigo, estado, i, value){
                                    	if( codigo != "" ){
                                    	  $.ajax({
                                    					data:  ({ codigo : codigo,estado:estado  }),
                                    					url:   'desactivarUsuario.php',
                                    					type:  'post',
                                    					
                                    			});
          if (value=="imagenes/activo.gif") {
			//alert('desactiva');
			document.getElementById(i).value="imagenes/inactivo.gif";
			document.getElementById(i).src="imagenes/inactivo.gif";
			
		}
		else  {
			//alert('activa');
			document.getElementById(i).value="imagenes/activo.gif";
			document.getElementById(i).src="imagenes/activo.gif";
			
		}						
                                    	}                                        
                                    }
                                    
			function verBitacora(codigoUsuario)
			{
					location.replace("../bitacora/index.php?idUsuario="+codigoUsuario);					
			}
		
		function enviarOrdenar()
		{
			if (document.frm_buscarUsuario.cbo_tipoUsuario.value==0 || document.frm_buscarUsuario.cbo_tipoUsuario.value==1 || document.frm_buscarUsuario.cbo_tipoUsuario.value==2 || document.frm_buscarUsuario.cbo_tipoUsuario.value==3)
			{
				document.frm_buscarUsuario.action="listarUsuariosAdministrador.php";
				document.frm_buscarUsuario.submit();
			}
			if (document.frm_buscarUsuario.cbo_tipoUsuario.value==4)
			{
			document.frm_buscarUsuario.action="listarGruposAdministrador.php";
			document.frm_buscarUsuario.submit();

			}
			if (document.frm_buscarUsuario.cbo_tipoUsuario.value==5)
			{
					document.frm_buscarUsuario.action="listarProfesorMateriaAdministrador.php";
     				document.frm_buscarUsuario.submit();
			
			}
			else
			{
				document.frm_buscarUsuario.submit();
			}

		}
		
		function chekear()
		{
			var todo=document.getElementById('todo');
			if (todo.checked==true)
				{
					for(i=0;i<document.frm_usuario.elements.length;i++)
					{
						if(document.frm_usuario.elements[i].id=="codigoUsuario")
						document.frm_usuario.elements[i].checked=true;
					}
				}
			else
				{
					for(i=0;i<document.frm_usuario.elements.length;i++)
					{
						if(document.frm_usuario.elements[i].id=="codigoUsuario")
						document.frm_usuario.elements[i].checked=false;
					}
				}
		}
		
			function eliminarUsuario(codigoUsuario)
			{
				if(confirm("¿Está seguro de eliminar este usuario del sistema?"))
				{
					location.replace("eliminarUsuario.php?codigoUsuario="+codigoUsuario);					
				}
			}
			function reiniciarUsuario(codigoUsuario)
			{
				if(confirm("¿Está seguro de reinicar este usuario?"))
				{
					location.replace("reiniciarUsuario.php?admin=1&codigoUsuario="+codigoUsuario);					
				}
			}
		function enviar()
		{
			document.frm_verUsuario.submit();				
		}
		function eliminar()
		{
			if(confirm("¿Está seguro de eliminar los usuarios seleccionados?"))
			{
			var contador=0;
					for(i=0;i<document.frm_usuario.elements.length;i++)
					{
						if(document.frm_usuario.elements[i].id=="codigoUsuario")
						{
							if (document.frm_usuario.elements[i].checked==true)
							{
								contador++;							
							}
							
						}
					}
			if(contador>0)
				{
				document.frm_usuario.action="eliminarUsuario.php";
				document.frm_usuario.submit();
				}
			else
				alert('No hay ningún usuario seleccionado')
			}
		}
		function eliminarBuscados()
		{
			if(confirm("¿Está seguro de eliminar los usuarios seleccionados?"))
			{
			var contador=0;
					for(i=0;i<document.frm_buscarUsuario.elements.length;i++)
					{
							if (document.frm_buscarUsuario.elements[i].checked==true)
							{
								contador++;							
							}
					}
					
			if(contador>0)
				{
				document.frm_buscarUsuario.action="eliminarUsuario.php";
				document.frm_buscarUsuario.submit();
				}
			else
				alert('No ha seleccionado ningún Alumno')
			}
		}
		
		</script> 
		
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><?=menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
				<form name="frm_buscarUsuario" method="post" action="<?=$PHP_SELF?>">
				
				<table class="tablaGeneral" >
					<tr class="trSubTitulo">
						  <td width="20%">Buscar Usuarios:</td>
			      <td width="50%"><input type="text" name="txt_buscarUsuario" size="50" value="<?=$_POST['txt_buscarUsuario']?>">
					        <input type="submit" name="sub_buscar" value="Buscar"></td>
							<td>Mostrar: </td>
							<td>
							<select name="cbo_tipoUsuario" onChange="javascript: enviarOrdenar()">
								<option value="0" <?= $selectedTodos?>>Todos los Usuarios</option>
								<option value="1" <?= $selectedAdmin?>>Administradores</option>
								<option value="2" <?= $selectedAlumnos?>>Alumnos</option>
								<option value="3" <?= $selectedProfesores?>>Profesores</option>					
								<option value="4" <?= $selectedGrupos?>>Ver por Materias</option>	
								<option value="5" <?= $selectedMaterias?>>Ver por Profesor y Materias</option>							
						
										
							</select>
							</td>
					</tr>
				
				  </table>
				<?
// BUSQUEDA
				if(!empty($_POST['txt_buscarUsuario']))
				{
					$cadena=explode('+',$_POST['txt_buscarUsuario']);
					$contPalabras=0;
					foreach($cadena as $palabra)
					{
						if(empty($busqueda))
						{
							$busqueda="mei_usuario.idusuario LIKE '%".$palabra."%'";
							$busqueda.=" OR CONCAT(mei_usuario.primernombre,' ',mei_usuario.segundonombre,' ',mei_usuario.primerapellido,' ',mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
						}
						else
						{
							$busqueda.=" OR mei_usuario.idusuario LIKE '%".$palabra."%'";
							$busqueda.=" OR CONCAT(mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
						}
			
					}
					$sql="SELECT mei_usuario.idtipousuario,mei_usuario.idusuario,
					mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.estado,mei_usuario.visibilidad ,mei_usuario.administrador 
						FROM mei_usuario WHERE ".$busqueda;
					$resultado=$baseDatos->ConsultarBD($sql);
					$numRegistros=mysql_num_rows($resultado);
					if($numRegistros>0)
					{
						if($numRegistros == 1)
						{
							$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registro encontrado.";
						}
						else
						{
							$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registros encontrados.";
						}
					
					?>
						<br><table class="tablaGeneral" >
					  <tr class="trSubTitulo">
					  	<td colspan="6"><?= $cadenaResultado?></td>
					  </tr>
						<tr class="trSubTitulo">
						  <td width="15%" class="trSubTitulo" colspan="2"><div align="center">&nbsp;&nbsp;C&oacute;digo</div></td>
							<td width="55%" class="trSubTitulo"><div align="center">Nombre del Alumno</div></td>					
							<td width="10%" class="trSubTitulo"><div align="center">Editar</div></td>					
							<td width="10%" class="trSubTitulo" colspan="2"><div align="center">Reiniciar</div></td>
						</tr>
					<?
						$cont=0;
						while(list($codigoTipo,$codigoUsuario,$nombre1,$nombre2,$apellido1,$apellido2,$estado,$estadoact,$administrador)=mysql_fetch_array($resultado))
						{
								if($cont%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}
						
					?>
								<tr class="trListaClaro">
									<td class="<?=$color?>" align="right" width="5%"><?=$cont+1?><input id="codigoUsuario" type="checkbox" name="<?= "chk_usuario".$cont?>" value="<?=$codigoUsuario?>"></td>
									<td class="<?=$color?>">&nbsp;<?=$codigoUsuario?></td>
									<td class="<?=$color?>">&nbsp;<?= $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2 ?></td>					
									<td class="<?=$color?>" align="center">
										<table class="tablaPrincipal" >
											<tr>
												<td width="35%"><div align="center"><a href="modificarUsuario.php?codigoUsuario=<?=$codigoUsuario?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Usuario" width="16" height="16" border="0"></a></div></td>
												<td width="30%">&nbsp;</td>
												<?
												if($_SESSION['idusuario']!=$codigoUsuario)
												{
												?>
												<td width="35%"><div align="center"><a href="javascript: eliminarUsuario('<?= $codigoUsuario?>')"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Usuario" width="16" height="16" border="0"></a></div></td>
												<?
												}
												else
												{
												?>
												<td width="35%"></td>
												<?
												}
												?>
											</tr>
										</table>
									</td>
									<?
									if($estado==1)
									{
									?>
									<td colspan="2" class="<?=$color?>" align="center"><a href="javascript: reiniciarUsuario('<?= $codigoUsuario?>')"  class="link"><img src="imagenes/activo.gif" alt="Usuario Activo" width="16" height="16" border="0"></a></div></td>
									<?
									/*<td class="<?=$color?>" align="center"><a href="javascript: verBitacora('<?= $codigoUsuario?>')"  class="link"><img src="../bitacora/imagenes/bitacora.gif" alt="Ver Registro de Bitacora de este Alumno" width="16" height="16" border="0"></a></div></td>*/
									}
									else  if($estado==0)
									{
									?>
									<td colspan="2" class="<?=$color?>" align="center"><img src="imagenes/inactivo.gif" alt="Usuario Inactivo" width="16" height="16" border="0"></a></div></td>
									<?
									/*<td class="<?=$color?>" align="center"><img src="../bitacora/imagenes/bitacoraInactiva.gif"  width="16" height="16" border="0"></div></td>*/
									
									}
									?>
								</tr>
								
					<?
							$cont++;
						}
					?>
							  <tr class="trInformacion">
								<td colspan="6" class="trSubTitulo"><div align="center">
								<input type="button" name="btn_eliminarBuscar" value="Eliminar Seleccionados" onClick="javascript: eliminarBuscados()"></div></td>
							  </tr>
					
						</table>					
						
					<?
					}
					else
					{
					?>
						<b>
						<?=$codigoUsuario?>
						</b><br>
						<table class="tablaGeneral" >
						  <tr class="trAviso">
							<td colspan="6">No se encontraron coincidencias con <i><?=$_POST['txt_buscarUsuario']?></i> </td>
						  </tr>
				  </table>
					<?
					}
				}

//FIN BUSQUEDA				
				
				
				?>
					<input name="hid_contUsuario" type="hidden" value="<?=$cont?>">
				</form>
				
				<form name="frm_usuario" method="post">
			<?
			

				$sql="SELECT mei_usuario.idusuario FROM mei_usuario";
				$resultado=$baseDatos->ConsultarBD($sql);
				list($codigoUsuario)=mysql_fetch_array($resultado);
				
				if(!empty($codigoUsuario))
				{
				?>
					<table class="tablaGeneral" >
					  <tr class="trTitulo">
					  	<td colspan="6" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Ver Usuarios </td>
					  </tr>
				<?
							if($_POST['cbo_tipoUsuario']==1)
							{
								$sql="SELECT DISTINCT (mei_usuario.idusuario), mei_usuario.idtipousuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
								mei_usuario.primerapellido, mei_usuario.segundoapellido, mei_usuario.estado, mei_usuario.visibilidad,mei_usuario.administrador
								FROM mei_usuario WHERE mei_usuario.administrador=1 AND mei_usuario.idtipousuario=2 ORDER BY mei_usuario.primerapellido, mei_usuario.segundoapellido";
							}
							else if($_POST['cbo_tipoUsuario']==2)
							{
								$sql="SELECT DISTINCT (mei_usuario.idusuario), mei_usuario.idtipousuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
								mei_usuario.primerapellido, mei_usuario.segundoapellido, mei_usuario.estado, mei_usuario.visibilidad,mei_usuario.administrador
								FROM mei_usuario WHERE mei_usuario.idtipousuario=3 OR mei_usuario.idtipousuario=7 ORDER BY mei_usuario.primerapellido, mei_usuario.segundoapellido";
							}
							else if($_POST['cbo_tipoUsuario']==3)
							{
								$sql="SELECT DISTINCT (mei_usuario.idusuario), mei_usuario.idtipousuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
								mei_usuario.primerapellido, mei_usuario.segundoapellido, mei_usuario.estado, mei_usuario.visibilidad,mei_usuario.administrador
								FROM mei_usuario WHERE mei_usuario.idtipousuario=2 ORDER BY mei_usuario.primerapellido, mei_usuario.segundoapellido";
							}
							else 
							{
								$sql="SELECT DISTINCT (mei_usuario.idusuario), mei_usuario.idtipousuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
								mei_usuario.primerapellido, mei_usuario.segundoapellido, mei_usuario.estado, mei_usuario.visibilidad,mei_usuario.administrador
								FROM mei_usuario WHERE mei_usuario.idtipousuario=2 OR mei_usuario.idtipousuario=3 OR mei_usuario.idtipousuario=7 ORDER BY mei_usuario.primerapellido, mei_usuario.segundoapellido";
							}
							
							
								
								
																
								$resultado=$baseDatos->ConsultarBD($sql);
						?>
						<tr class="trSubTitulo">
						  <td width="15%" class="trSubTitulo" colspan="2"><div align="center">
						    <input id="todo" type="checkbox" name="chk_todoUsuario" onClick="javascript:chekear()">&nbsp;Código</div></td>
							<td width="55%" class="trSubTitulo"><div align="center">Nombre del Alumno</div></td>					
							<td width="10%" class="trSubTitulo"><div align="center">Editar</div></td>					
							<td width="10%" class="trSubTitulo"><div align="center">Reiniciar</div></td>
                            <td width="10%" class="trSubTitulo"><div align="center">Activar</div></td>					
						</tr>
						
							<?
							$cont=0;
							$j=0;
							while (list($codigoUsuario,$codigoTipo,$nombre1,$nombre2,$apellido1,$apellido2,$estado,$estadoact,$administrador)=mysql_fetch_array($resultado))
								{
								$r=2*$j;
								$a=(2*$j)+1;
								if($cont%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}
								
								if($codigoTipo==2 && $administrador==1)
								{
								
								?>
								<tr title="Usuario: Profesor Administrador" class="<?=$color?>">
								  <td class="<?=$color?>" align="right"><?=$cont+1?><input id="codigoUsuario" type="checkbox" name="<?= "chk_usuario".$cont?>" value="<?=$codigoUsuario?>"></td>
								  <td class="<?=$color?>">&nbsp;<b><?=$codigoUsuario?></b></td>
								<?
								}
								else if($codigoTipo==2 && $administrador==0)
								{
								?>
								<tr title="Usuario: Profesor" class="<?=$color?>">
									<td class="<?=$color?>" align="right"><?=$cont+1?><input id="codigoUsuario" type="checkbox" name="<?= "chk_usuario".$cont?>" value="<?=$codigoUsuario?>"></td>
									<td class="<?=$color?>">&nbsp;<?=$codigoUsuario?></td>
								<?
								}
								else if($codigoTipo==3 || $codigoTipo==7)
								{
								?>
								<tr title="Usuario: Alumno" class="<?=$color?>">
									<td class="<?=$color?>" align="right"><?=$cont+1?><input id="codigoUsuario" type="checkbox" name="<?= "chk_usuario".$cont?>" value="<?=$codigoUsuario?>"></td>
									<td class="<?=$color?>">&nbsp;<?=$codigoUsuario?></td>
								<?
								}
								
								?>
									
									<td class="<?=$color?>">&nbsp<?= $apellido1." ".$apellido2." ".$nombre1." ". $nombre2?></td>					
									<td class="<?=$color?>" align="center">
										<table class="tablaPrincipal" align="center">
											<tr>
												<td width="35%"><div align="center"><a href="modificarUsuario.php?codigoUsuario=<?=$codigoUsuario?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Usuario" width="16" height="16" border="0"></a></div></td>
												<td width="30%">&nbsp;</td>
												<?
												if($_SESSION['idusuario']!=$codigoUsuario)
												{
												?>
												<td width="35%"><div align="center"><a href="javascript: eliminarUsuario('<?= $codigoUsuario?>')"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Usuario" width="16" height="16" border="0"></a></div></td>
												<?
												}
												else
												{
												?>
												<td width="35%"></td>
												<?
												}
												?>
											</tr>
										</table>
									</td>
											<?
											if($estado==1)
											{
											?>
											<td colspan="1" class="<?=$color?>" align="center"><a href="javascript: reiniciarUsuario('<?= $codigoUsuario?>')"  class="link"><img src="imagenes/activo.gif" alt="Usuario Activo" width="16" height="16" border="0"></a></div></td>
											
											<?
											/*<td class="<?=$color?>" align="center"><a href="javascript: verBitacora('<?= $codigoUsuario?>')"  class="link"><img src="../bitacora/imagenes/bitacora.gif" alt="Ver Registro de Bitacora de este Alumno" width="16" height="16" border="0"></a></div></td>*/
											}
											else  if($estado==0)
											{
											?>
											<td colspan="1" class="<?=$color?>" align="center"><img src="imagenes/inactivo.gif" alt="Usuario Inactivo" width="16" height="16" border="0"></a></div></td>
											<?
											/*<td class="trListaClaro" align="center"><img src="../bitacora/imagenes/bitacoraInactiva.gif"  width="16" height="16" border="0"></div></td>*/
											}
											?>
                                   
                                     
                                     
                                     <td align="center"><?
		  		if ($estadoact == 1)
				{
					
			?>
		    <input type="image" id="<?=$a?>" value="imagenes/inactivo.gif" src="imagenes/inactivo.gif"  onclick="desactivarUsuario(<?= $codigoUsuario?>,0,this.id,this.value)" />
			<?
				}
				else
				{
			?>
			<input type="image" id="<?=$a?>" value="imagenes/activo.gif" src="imagenes/activo.gif"  onclick="desactivarUsuario(<?= $codigoUsuario?>,1,this.id,this.value)" />
			<?
				}
			?>&nbsp;
			</td>
                                    
                                    
                                    
                                    
                                    
									 
								</tr>
								<?
								$cont++;
								$j++;	
								}//FIN WHILE
					  ?>
							  <tr class="trInformacion">
								<td colspan="6" class="trSubTitulo"><div align="center">
								<input type="button" name="btn_eliminar" value="Eliminar Seleccionados" onClick="javascript: eliminar()"></div></td>
							  </tr>
					  
					<br>
				<?
				}
				else
				{
				?>
					<table class="tablaGeneral" >
						<tr class="trTitulo" >
						  <td><img src="imagenes/usuario.gif" width="16" height="16">Ver Usuarios</td>
					  </tr>
					  <tr class="trAviso">
					  	<td class="trAviso">Actualmente no existen usuarios registrados</td>
					  </tr>
				  </table>
				<?
				}
		
				?>
					</table>
					<input name="hid_contUsuario" type="hidden" value="<?=$cont?>">
					
		</form>
			  </td>
              <td class="tablaEspacio">&nbsp;</td>
              <td class="tablaDerecho">&nbsp;</td>
              <td class="tablaEspacioDerecho">&nbsp;</td>              
			</tr>
		</table>
<?
	if($_GET['error'] == 1)
	{
	
?>
	<script language="javascript">
		alert("No se puede eliminar al profesor porque tiene grupos registrados.");
	</script>
<?
	}
?>		
		</body>
		</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
	
?>