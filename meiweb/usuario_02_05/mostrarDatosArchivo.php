<?
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');	
	include_once ('../librerias/vistas.lib.php');
	include_once ('../usuario/Cargador.class.php');

	$baseDatos=new BD();
	$cargador= new Cargador();
	$cargador->SeparadorListaDatos($_GET['idSeparador']);
	
	if(comprobarSession())
	{
		if(!empty($_GET['idGrupo']))
		{
			$cargador->LeerListaDatos();		
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
</head>

<?
			$cargador->JavascriptCargador();				
?>								
<script language="javascript">
	function enviarCarga(idGrupo,separador)
	{
		if(comprobarCampos())
		{
			document.frm_carga.action="insertarAlumnoMasivo.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idGrupo="+idGrupo+"&idSeparador="+separador;
			document.frm_carga.submit();
		}					
	}
		
	function enviarCancelar(idGrupo)
	{
		location.replace("../usuario/cargaMasiva.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idGrupo="+idGrupo);
	}
</script>
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td><br><form name="frm_carga" method="post">
				<table width="100%" class="tablaGeneral">
					<tr class="trAviso">
						<td width="3%" valign="top"><img src="imagenes/existente.gif" width="16" height="15"></td>
						<td width="97%"><div align="justify">
							<p>Seleccione el nombre de cada columna dependiendo del valor mostrado en ellas. Debe seleccionar la columna correspondiente al valor <b> C&oacute;digo</b>, <b>Primer Nombre</b>, <b>Segundo Nombre</b>, <b>Primer Apellido</b>, <b>Segundo Apellido</b> y en caso de existir mas columnas debe marcarla como <b>No cargar</b> para excluir esta informaci&oacute;n de la carga de datos.<br><br></div>
						</td>
					</tr>
					<tr class="trAviso">
						<td>&nbsp;</td>
						<td><div align="center">
							<input name="btn_cargarAlumnos" type="button" id="btn_cargarAlumnos" onClick="javascript: enviarCarga('<?= $_GET['idGrupo']?>','<?= $_GET['idSeparador']?>')" value="Cargar Alumnos">
             				<input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar('<?= $_GET['idGrupo']?>')">
						</div></td>
					</tr>
    			</table>
				<br>
				<table class="tablaGeneral" >
					<tr class="trSubTitulo">
<?	
		for($i=0;$i<$cargador->m_centinela;$i++)			
		{
?>
						<td class="trSubTitulo"><select name="cbo_campo<?= $i?>">
					        <option value="1">Codigo</option>
					        <option value="2">Primer Nombre</option>
					        <option value="3">Segundo Nombre</option>
					        <option value="4">Primer Apellido</option>
					        <option value="5">Segundo Apellido</option>
					        <option value="0" selected>No cargar</option>
					      </select></td>
<?
		}
?>
						<input type="hidden" name="hid_numeroCampos" value="<?= $i?>">
    				</tr>
<?
		$cont=0;
		foreach($cargador->m_listaDatos as $dato)
		{
			if($cont%2==0)
				$color="trListaClaro";
			else
				$color="trListaOscuro";
?>
					<tr class="<?=$color?>">
<?
			$cargador->m_listaDatos=explode($cargador->m_separador,$dato);
			for($i=0;$i<sizeof($cargador->m_listaDatos);$i++)				
			{
?>
						<td class="<?=$color?>"><img src="imagenes/transparente.gif" width="10" height="16"><?= $cargador->m_listaDatos[$i]?></td>
<?		
			}	
?>
					</tr>
<?
			$cont++;
		}
?>
				</table>
				</form>
				</p>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>

</body>
</html>
<? 
		}
		else
		{
			redireccionar("../materias/verMateriaProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>
