<?
	class Cargador
	{
		var $m_nombreArchivoCarga;
		var $m_listaDatos;
		var $m_centinela;
		var $m_separador;
		var $m_nombreArchivo;
		var $m_nombreDirectorio;
		var $m_patronNum;
		var $m_tipoUsuario;
		var $m_valorEstado;
		var $m_listaSqlCorrectos;
		var $m_listaSqlRelUsuGru;
		var $m_listaSqlRelUsuMatGru;
		var $m_listaSqlIncorrectos;
		var $m_listaCodigosCorrectos;
		var $m_posCodigo;
		var $m_posPriNombre;
		var $m_posSegNombre;
		var $m_posPriApellido;
		var $m_posSegApellido;



		
		function Cargador()
		{
			$this->m_nombreArchivo="datosUsuarios.usr";			
			$this->m_nombreDirectorio="../../datosMEIWEB/archivosUsuarios/";
			$this->m_archivoIndiceSeparador="../configuracion/archivos/indiceSeparador.dat";
			$this->m_patronNum = "^[[:digit:]]+$";
			$this->m_tipoUsuario=3;
			$this->m_valorEstado=0;
			$this->m_nombreArchivoCarga=$this->m_nombreDirectorio.$this->m_nombreArchivo;
			
			$this->m_separador=";";	
			
		}
		
		function SeparadorListaDatos($a_idSeparador)
		{
			$archivoIndiceSeparador="../configuracion/archivos/indiceSeparador.dat";
			
			if(file_exists($archivoIndiceSeparador))
			{
				$listaIndiceSeparador=file($archivoIndiceSeparador);
				
				list($nombreSeparador,$this->m_separador)=explode("=",$listaIndiceSeparador[$a_idSeparador-1]);
				$this->m_separador=trim($this->m_separador);
				return $this->m_separador;
			}
			else
			{
				print "No exite el archivo indiceSeparador.dat";
			}
		}
				
		function InsertarSQL()
		{
			include_once("../baseDatos/BD.class.php");
			
			$baseDatos= new BD();
			
			$contLista=0;
						
			if(sizeof($this->m_listaCodigosCorrectos))
			{
				foreach($this->m_listaCodigosCorrectos as $codigoCorrecto)
				{
					$sql="SELECT mei_usuario.idusuario FROM mei_usuario 
							WHERE mei_usuario.idusuario=".$this->m_listaCodigosCorrectos[$contLista];
					
					$consultaIdUsuario=$baseDatos->ConsultarBD($sql);					
					$numIdUsuario=mysql_num_rows($consultaIdUsuario);
					
					if(empty($numIdUsuario))
					{
						$baseDatos->ConsultarBD($this->m_listaSqlCorrectos[$contLista]);
					}
					
					$sql="SELECT mei_relusugru.idusuario FROM mei_relusugru 
							WHERE mei_relusugru.idusuario=".$codigoCorrecto." 
								AND mei_relusugru.idgrupo=".$_GET['idGrupo'];
								
					$consultaIdGrupoUsuario=$baseDatos->ConsultarBD($sql);	
					$numIdGrupoUsuario=mysql_num_rows($consultaIdGrupoUsuario);
					
					if(empty($numIdGrupoUsuario))
					{
						$baseDatos->ConsultarBD($this->m_listaSqlRelUsuGru[$contLista]);
					}

					$sql="SELECT mei_relusumatgru.idusuario FROM mei_relusumatgru 
							WHERE mei_relusumatgru.idusuario=".$codigoCorrecto." 
								AND mei_relusumatgru.idgrupo=".$_GET['idGrupo']."
								AND mei_relusumatgru.idmateria=".$_GET['idmateria'];
								
					$consultaIdGrupoMateriaUsuario=$baseDatos->ConsultarBD($sql);	
					$numIdGrupoUsuarioM=mysql_num_rows($consultaIdGrupoMateriaUsuario);
					
					if(empty($numIdGrupoUsuarioM))
					{
						$baseDatos->ConsultarBD($this->m_listaSqlRelUsuMatGru[$contLista]);
					}
					
					$contLista++;
				}
			}
			
			if(sizeof($this->m_listaSqlIncorrectos))
			{
				foreach($this->m_listaSqlIncorrectos as $sqlIncorrecto)
				{
					$baseDatos->ConsultarBD($sqlIncorrecto);
				}
			}
		}		
		
		function ConstruirSQL($idmateria)
		{
			$archivoUsuarios=file($this->m_nombreArchivoCarga);
				
			$sqlUsuario="INSERT INTO mei_usuario ( idtipousuario,estado,clave";	
			$sqlRelUsuGru="INSERT INTO mei_relusugru ( idusuario,idgrupo) VALUES"; 
			$sqlRelUsuMatGru="INSERT INTO mei_relusumatgru ( idusuario,idgrupo,idmateria) VALUES"; 
			$sqlError="INSERT INTO mei_usuariotemp ( idusuariotemp , idtipousuario , idgrupo , datos ,separador ) 	VALUES ('', '".$this->m_tipoUsuario."', '".$_GET['idGrupo']."', '"; 
		
			$contDescartados=0;
			$contCorrectos=0;
			$contIncorrectos=0;
		
			for($i=0;$i<$_POST['hid_numeroCampos'];$i++)
			{
				$listaCampos[$i]=$_POST['cbo_campo'.$i];
			}
			
			for($i=0;$i<sizeof($listaCampos);$i++)			
			{				
				switch($listaCampos[$i])
				{
					case 1:
					{
						$sqlCampo.=",idusuario";
						$this->m_posCodigo=$i;
					}
					break;
					
					case 2:
					{
						$sqlCampo.=",primernombre";
						$this->m_posPriNombre=$i;
					}
					break;
					
					case 3:
					{
						$sqlCampo.=",segundonombre";
						$this->m_posSegNombre=$i;
					}
					break;
					
					case 4:
					{
						$sqlCampo.=",primerapellido";
						$this->m_posPriApellido=$i;
					}
					break;
					
					case 5:
					{
						$sqlCampo.=",segundoapellido";
						$this->m_posSegApellido=$i;
					}
					break;	
					case 0:
					{
						$camposDescartados[$contDescartados].=$i;
						$contDescartados++;
					}
					break;	
				}
			}
					
			if(sizeof($listaCampos)!= sizeof($camposDescartados))
			{/*
				$sql="INSERT INTO mei_relusumatgru ('idusuario','idmateria','idgrupo') VALUES
				('".$datosUsuario[$this->m_posCodigo]."','".$idmateria."','".$_GET['idGrupo']."')";
				$baseDatos->ConsultarBD($sql);
				*/
				$sqlInsert=$sqlUsuario.$sqlCampo.") VALUES (".$this->m_tipoUsuario.",".$this->m_valorEstado;
			
				foreach($archivoUsuarios as $dato)
				{
					$datosUsuario=explode($this->m_separador,$dato);
					
					$datosUsuario[$this->m_posPriNombre]=trim($datosUsuario[$this->m_posPriNombre]);
					$datosUsuario[$this->m_posSegNombre]=trim($datosUsuario[$this->m_posSegNombre]);				
					$datosUsuario[$this->m_posPriApellido]=trim($datosUsuario[$this->m_posPriApellido]);					
					$datosUsuario[$this->m_posSegApellido]=trim($datosUsuario[$this->m_posSegApellido]);					
										
					if(eregi($this->m_patronNum,trim($datosUsuario[$this->m_posCodigo])) && strlen($datosUsuario[$this->m_posPriNombre]) && strlen($datosUsuario[$this->m_posPriApellido]))
					{
						unset($sqlValues);
						
						if(empty($datosUsuario[$this->m_posSegNombre]))
						{
							$datosUsuario[$this->m_posSegNombre]="";
						}
						
						if(empty($datosUsuario[$this->m_posSegApellido]))
						{
							$datosUsuario[$this->m_posSegApellido]="";
						}
						
						








						for($i=0;$i<sizeof($datosUsuario);$i++)				
						{
							if(sizeof($camposDescartados) >0 )
							{
								if(!in_array($i,$camposDescartados))
								{
									$sqlValues.=",'".strtoupper($datosUsuario[$i])."'";
								}
							}
							else
							{
								$sqlValues.=",'".strtoupper($datosUsuario[$i])."'";
							}
						}
						
						$claveUsuario=trim($datosUsuario[$this->m_posCodigo]);
						
						$this->m_listaSqlCorrectos[$contCorrectos]=$sqlInsert.",'".md5(md5($claveUsuario))."'".$sqlValues.")";
						$this->m_listaCodigosCorrectos[$contCorrectos]=$datosUsuario[$this->m_posCodigo];
						$this->m_listaSqlRelUsuGru[$contCorrectos]=$sqlRelUsuGru."('".$datosUsuario[$this->m_posCodigo]."','".$_GET['idGrupo']."')";
						$this->m_listaSqlRelUsuMatGru[$contCorrectos]=$sqlRelUsuMatGru."('".$datosUsuario[$this->m_posCodigo]."','".$_GET['idGrupo']."','".$idmateria."')";
							
						$contCorrectos++;
					}	
					else
					{
						$this->m_listaSqlIncorrectos[$contIncorrectos]=$sqlError.$dato."', '".$this->m_separador."')";
						$contIncorrectos++;
					}
				}
				//	echo $datosUsuario[$this->m_posCodigo];
					
				}
			else
			{
				print "Error no se han seleccionado correctamente los campos para la insersion";
			}	
		}
		
		function CargarArchivoUsr($a_archivo)
		{
			if (move_uploaded_file($_FILES[$a_archivo]['tmp_name'],$this->m_nombreArchivoCarga))
			{ 
				return true;					
			}
			else
			{ 
				return false;
			} 			
		}
		
		function MaxNumCampos($a_listaDatos)
		{
			foreach($a_listaDatos as $dato)
			{
				$vectorCampos=explode($this->m_separador,$dato);	
				$listaLong[$cont++]=sizeof($vectorCampos);
			}
			
			rsort($listaLong);			
			return $listaLong[0];		
		}
		
		function LeerListaDatos()
		{
			if(file_exists($this->m_nombreArchivoCarga))
			{
				$this->m_listaDatos=file($this->m_nombreArchivoCarga);
				$this->m_centinela=$this->MaxNumCampos($this->m_listaDatos);
			}
			else
			{
				print "Error, el archivo de usuarios no existe ".$this->m_nombreArchivoCarga;
			}		
		}
		
		function JavascriptCargador()
		{
			for($i=0;$i<$this->m_centinela;$i++)	
			{
				if(empty($cadenaSuma))
				{
					$cadenaSuma.="parseInt(document.frm_carga.cbo_campo".$i.".value)";
					$cadenaCampos="document.frm_carga.cbo_campo".$i.".value";
				}
				else
				{
					$cadenaSuma.="+parseInt(document.frm_carga.cbo_campo".$i.".value)";
					$cadenaCampos.=",document.frm_carga.cbo_campo".$i.".value";
				}
			}
			
			print "<script>

					  function comprobarRepetidos(listaValores)
						{
							var cadena='54321';
							var lista='';
							
							listaValores.sort();
							listaValores.reverse();
							
							for(i=0;i<listaValores.length;i++)
							{
								if(listaValores[i]!=0)
								{
									lista+=listaValores[i];
								}
							}
							
							if(cadena==lista)
							{
								return true;
							}
							else
							{
								return false;
							}
						}
						
						";

			print "	function comprobarCampos()
						{
							var listaCampos= new Array(".$cadenaCampos.");
							var sumaCampos=".$cadenaSuma.";
							
							listaCampos.sort();
							
								if(sumaCampos == 15 && comprobarRepetidos(listaCampos))
								{	
									return true;
								}
								else if(sumaCampos == 0)
								{
									alert('No ha seleccionado ningun campo ');
									return false;
								}		
								else
								{
									alert('No ha seleccionado los campos correctamente');
									return false;
								}
							
						}
						
				</script>";
		
			
		}		
	
	
	}///

?>