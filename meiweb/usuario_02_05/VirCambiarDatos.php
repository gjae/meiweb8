<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ("../calendario/FrmSelectorEdad.class.php");
	include_once ("../editor/fckeditor.php") ;


	$baseDatos=new BD();
	$editorPerfil=new FCKeditor('edt_perfil' , '100%' , '200' , 'barraEstandar' , '' ) ;
	if(comprobarSession())
	{
		if (!empty($_GET['idmateria'])) {
			
			$flag=1;

		}
		else $flag=0;
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
		{
			$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=7";
			$resultado=$baseDatos->ConsultarBD($sql);
			list($estado)=mysql_fetch_array($resultado);
			if($estado==1)
			{
				$sql="SELECT mei_usuario.idusuario, mei_usuario.clave,mei_usuario.login,mei_usuario.correo,mei_usuario.telefono,
					mei_usuario.fechanacimiento,mei_usuario.perfil,mei_usuario.pregunta,mei_usuario.respuesta FROM mei_usuario WHERE mei_usuario.idusuario=".$_SESSION['idusuario'];
				$resultado=$baseDatos->ConsultarBD($sql);
				list($codigoUsuario,$clave,$login,$correo,$telefono,$fecha,$perfil,$pregunta,$respuesta)=mysql_fetch_array($resultado);
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema");?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<script src="jquery.js" type="text/javascript"></script>
		<script type="text/javascript">

			$(document).ready(function(){
				var correo="<?php echo $correo;?>";
				var login = "<?php echo $login;?>";
	
	$('#txt-NomUsu').blur(function() {
		var loginN = $(this).val();
		caja=$(this);
		if(loginN != "" && login!=loginN) {
			$.ajax({
				type : "GET",
				data : "Login=" + loginN,
				url : "../login/peticion.php",
				dataType : "json",
				success : function(data) {
					if(parseInt(data.Respuesta) > 0) {
						alert("El login " + data.Login + " ya existe \n por favor escoja otro");
						caja.css("background", "rgba(200, 50, 20, 1)");
						$("#btningresar").attr("disabled", "disabled");
					} else {
						caja.css("background", "white");
						$("#btningresar").attr("disabled", false);

					}
				}
			});
		} else{
			caja.css("background", "white");
		}

	});
	$('#CorreoN').blur(function() {
		var correoN = $(this).val();
		caja=$(this);
		if(correo != correoN && correoN!="") {
			$.ajax({
				type : "GET",
				data : "Correo=" + correoN,
				url : "../login/peticionCorreo.php",
				dataType : "json",
				success : function(data) {
					if(parseInt(data.Respuesta) > 0) {
						alert("El Correo " + data.Correo + " ya esta registrado \n por favor escoja otro");
						$("#btningresar").attr("disabled", "disabled");
						caja.css('background-color', 'rgba(200, 50, 20, 1)');
					} else {
						caja.css('background-color', 'white');
						$("#btningresar").attr("disabled", false);
					}
				}
			});

		}else{
			caja.css('background-color', 'white');
		}
	});
	
	});
		</script>
	</head>
	<body>
		<script>
						function enviarCancelar(flag)
			{
				if (flag==0) {
			location.replace("../scripts/");
		}
		else
			location.replace("../scripts/homeMateria.php?idmateria=<? echo $_GET["idmateria"]; ?>");
			}

			function enviarDatos()
			{
				if(document.frm_cambiarDatos.txt_pregunta.value==false){
					alert("Debe ingresar una pregunta secreta");
				}else if(document.frm_cambiarDatos.txt_respuesta.value==false){
					alert("Debe ingresar una respuesta secreta");
				}else{
			if(validarDatos())
					{
						var bandera=true;
						if(document.frm_cambiarDatos.chk_clave.checked==true)
								{
									if(confirm("Ha cambiado su contraseña. Debe salir del sistema e ingresar nuevamente. ¿Está seguro que desea cambiar su contraseña?"))
									{
									document.frm_cambiarDatos.submit();
									}else{
										$("#chk_clave").attr('checked',false);
										//document.frm_cambiarDatos.chk_clave.checked=false;
										bandera=false;
									}
								}
						if(document.frm_cambiarDatos.chk_NomUsu.checked==true)
								{
									if(confirm("Ha cambiado su  nombre de usuario. Debe salir del sistema e ingresar nuevamente. ¿Está seguro que desea cambiar su  nombre de usuario?"))
										{
										document.frm_cambiarDatos.submit();
										}else{
											$("#chk_NomUsu").attr('checked',false);
											//document.frm_cambiarDatos.chk_NomUsu.checked=false;
										bandera=false;
									}
								}
						if(document.frm_cambiarDatos.chk_imagen.checked==true)
								{
									if(confirm("¿Está seguro que desea cambiar su imagen?"))
										{
										document.frm_cambiarDatos.submit();
										}else{
										bandera=false;
									}
								}
								if(bandera){
								document.frm_cambiarDatos.submit();
									}
			}
			}
			}

			function validarDatos()
			{
			if(validarCorreo())
			{
			if(comprobarFecha(document.frm_cambiarDatos.cbo_agno.value,document.frm_cambiarDatos.cbo_mes.value,document.frm_cambiarDatos.cbo_dia.value) != true)
			{
				alert ("La fecha de Nacimiento introducida no es correcta");
			}
			else if(document.frm_cambiarDatos.txt_telefono.value == false)
				{
				alert ("Es necesario que llene completamente la información solicitada");
				}
				else if(document.frm_cambiarDatos.chk_clave.checked==true)
					{
						if(document.frm_cambiarDatos.txt_nuevaClave.value == false || document.frm_cambiarDatos.txt_clave.value== false)
							{
								alert("Es necesario que llene los campos de Clave y Confirmar Clave");
							}
							else if(document.frm_cambiarDatos.txt_nuevaClave.value != document.frm_cambiarDatos.txt_clave.value)
									{
									alert("La contraseña y su confirmación no son iguales");
									}
									else
									{
								return true;
			}
			}
			else if(document.frm_cambiarDatos.chk_NomUsu.checked==true)
			{
			if(document.frm_cambiarDatos.txt_nuevaNomUsu.value == false || document.frm_cambiarDatos.txt_NomUsu.value== false)
			{
			alert("Es necesario que llene los campos de nombre de usuario y Confirmar nombre de usuario");
			}
			else if(document.frm_cambiarDatos.txt_nuevaNomUsu.value != document.frm_cambiarDatos.txt_NomUsu.value)
			{
			alert("El nombre de usuario y su confirmación no son iguales");
			}
			else if(document.frm_cambiarDatos.txt_nuevaNomUsu.value.length < parseInt('<?php print recuperarVariableSistema('sistemalongitudlogin')?>'))
			{
			alert("La longitud del  nombre de usuario no es correcto, ingrese un nombre de usuario con una longitud mayor a <?php print recuperarVariableSistema('sistemalongitudlogin')?>");
				}
				else {
					return true;
				}
				}
				else
				if(document.frm_cambiarDatos.chk_imagen.checked == true) {
					if(document.frm_cambiarDatos.fil_archivo.value == false) {
						alert("Debe elegir una imagen para cargar");
					} else {
						return true;
					}
				} else {
					return true;
				}
				}
				}

				function validarCorreo() {
					if(document.frm_cambiarDatos.txt_correo.value == "") {
						alert("Debe llenar completamente la información solicitada");
						document.frm_cambiarDatos.txt_correo.focus();
						return false;
					} else if(document.frm_cambiarDatos.txt_correo.value.search(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig)) {
						alert("El correo electronico no es válido, escribalo en forma correcta");
						document.frm_cambiarDatos.txt_correo.focus();
						return false;
					} else {
						return true;
					}
				}

				function activarClave() {
					if(document.frm_cambiarDatos.chk_clave.checked == false) {
						document.frm_cambiarDatos.txt_clave.disabled = true;
						document.frm_cambiarDatos.txt_nuevaClave.disabled = true;
						document.frm_cambiarDatos.VerClave.disabled = true;
						
					} else {
						document.frm_cambiarDatos.txt_clave.disabled = false;
						document.frm_cambiarDatos.txt_nuevaClave.disabled = false;
						document.frm_cambiarDatos.VerClave.disabled = false;
					}
				}
				function verclave(){
					
					if(document.frm_cambiarDatos.VerClave.checked) {
						document.frm_cambiarDatos.txt_clave.type = "text";
						document.frm_cambiarDatos.txt_nuevaClave.type = "text";
					}else{
						document.frm_cambiarDatos.txt_clave.type = "password";
						document.frm_cambiarDatos.txt_nuevaClave.type = "password";
					}
				}
				function activarNomUsu() {
					if(document.frm_cambiarDatos.chk_NomUsu.checked == false) {
						document.frm_cambiarDatos.txt_NomUsu.disabled = true;
						document.frm_cambiarDatos.txt_nuevaNomUsu.disabled = true;
						document.frm_cambiarDatos.VerLogin.disabled = true;
					} else {
						document.frm_cambiarDatos.txt_NomUsu.disabled = false;
						document.frm_cambiarDatos.txt_nuevaNomUsu.disabled = false;
						document.frm_cambiarDatos.VerLogin.disabled = false;
					}
				}
				function verlogin(){
					if(document.frm_cambiarDatos.VerLogin.checked) {
						document.frm_cambiarDatos.txt_NomUsu.type = "text";
						document.frm_cambiarDatos.txt_nuevaNomUsu.type = "text";
					}else{
						document.frm_cambiarDatos.txt_NomUsu.type = "password";
						document.frm_cambiarDatos.txt_nuevaNomUsu.type = "password";
					}
				}
				function cambiarImagen() {
					if(document.frm_cambiarDatos.chk_imagen.checked == false) {
						document.frm_cambiarDatos.fil_archivo.disabled = true;
					} else {
						document.frm_cambiarDatos.fil_archivo.disabled = false;
					}
				}
				function verPre() {
					if(document.frm_cambiarDatos.VerPre.checked != false) {
						document.frm_cambiarDatos.txt_pregunta.type = "text";
						document.frm_cambiarDatos.txt_respuesta.type = "text";
						
					} else {
						document.frm_cambiarDatos.txt_respuesta.type =  "password";
						document.frm_cambiarDatos.txt_pregunta.type =  "password";
					}
				}
		</script>
		<?				$selectorEdad = new FrmSelectorEdad();?>

		<table class="tablaPrincipal">
			<tr valign="top">
				<td class="tablaEspacio">&nbsp;</td>
				<td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']);?></td>
				<td class="tablaEspacio">&nbsp;</td>
				<td>
				<table class="tablaGeneral" >
					<tr class="trTitulo" >
						<td colspan="5" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Modificar Mi Perfil</td>
					</tr>
					<form name="frm_cambiarDatos" method="post" action="VirGuardarCambiosDatos.php" enctype="multipart/form-data">
						<tr class="trSubTitulo">
							<td colspan="5"  class="trSubTitulo">&nbsp;Cambiar Datos Personales</td>
						</tr>
						<tr class="trInformacion">
							<td colspan="2"><b>&nbsp;Numero de Documento:<img src="imagenes/transparente.gif" width="16" height="16"></b></td>
							<td colspan="2"><?=$_SESSION['idusuario']
							?></td>
							<?
$sql="SELECT imagen FROM mei_usuario WHERE idusuario=".$_SESSION['idusuario'];
$resultado=$baseDatos->ConsultarBD($sql);
list($imagen)=mysql_fetch_array($resultado);
$nombreImagen='../../datosMEIWEB/fotosUsuarios/'.$imagen;
if ($imagen==-1)
{
							?>
							<td width="26%" rowspan="5" align="center" valign="middle"><img src="imagenes/avatar.jpg" width="160" height="160"></td>
							<?
							}
							else
							{
							?>
							<td width="18%" rowspan="5" align="center" valign="middle"><img src="<?php print $nombreImagen?>" width="160" height="160"></td>
							<?
							}
							?>
						</tr>
						<tr class="trInformacion">
							<td colspan="2"></td>
							<td colspan="2"></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="2"><b>&nbsp;Correo Electr&oacute;nico:</b></td>
							<td colspan="2">
							<input type="text" name="txt_correo" id="CorreoN" value="<?=$correo?>" title="Correo Interno">
							</td>
						</tr>
						<tr class="trInformacion">
							<td colspan="2"><b>&nbsp;Tel&eacute;fono:</b></td>
							<td colspan="2">
							<input name="txt_telefono" type="text" class="link" value="<?=$telefono?>">
							</td>
						</tr>
						<tr class="trInformacion">
							<td colspan="2"><b>&nbsp;Fecha Nacimiento: </b></td>
							<td colspan="2"><? $selectorEdad -> SelectorEdar();?></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4"><b>&nbsp;
							<input type="checkbox" name="chk_clave" value="checkbox" onClick="javascript: activarClave()" id='chk_clave'>
							Cambiar Contrase&ntilde;a </b></td>
							<td>
							<input type="checkbox" name="chk_imagen" onClick="javascript: cambiarImagen()" id="chk_imagen">
							<b>Cambiar Imagen</b></td>
						</tr>
						<tr class="trInformacion" width="25%">
							<td width="5%">&nbsp;</td>
							<td><b>Nueva Clave:</b></td>
							<td>
							<input name="txt_clave" type="password" disabled>
							</td>
							<td width="17%">
								<label>
									<input type="checkbox" id="VerClave" name="VerClave" disabled="true" onclick="verclave();"/>
									<b>Ver Clave</b>
									</label>
									</td>
							<td>
							<input name="fil_archivo" type="file" id="fil_archivo" disabled>
							</td>
						</tr>
						<tr class="trInformacion">
							<td>&nbsp;</td>
							<td><b>Confirmar Clave: </b></td>
							<td>
							<input name="txt_nuevaClave" type="password" disabled>
							</td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr class="trInformacion">
							<td colspan="5"><b>&nbsp;
							<input type="checkbox" name="chk_NomUsu" value="checkbox" onClick="javascript: activarNomUsu()" id="chk_NomUsu">
							Cambiar Login de Usuario</b></td>
						</tr>
						<tr class="trInformacion">
							<td>&nbsp;</td>
							<td width="20%"><b>Nuevo login:</b></td>
							<td width="17%">
							<input name="txt_NomUsu"  type="password" id="txt-NomUsu" disabled>
							</td>
							<td colspan="2">
							<label>
									<input type="checkbox" id="VerLogin" name="VerLogin" disabled="true" onclick="verlogin();"/>
									<b>Ver Login</b>
									</label>
						</td>
						</tr>
						<tr class="trInformacion">
							<td>&nbsp;</td>
							<td width="20%"><b>Confirmar Login: </b></td>
							<td width="17%">
							<input name="txt_nuevaNomUsu"  type="password" disabled id="txt_nuevaNomUsu">
							</td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr class="trInformacion"> 
							<td>&nbsp;</td>
							<td width="20%"></td>
							<td width="17%"></td>
							<td colspan="2">&nbsp;</td>
							</tr>
							<tr class="trInformacion">
							<td>&nbsp;</td>
							<td width="20%"><b>Pregunta Secreta:</b></td>
							<td width="17%">
							<input name="txt_pregunta"  type="password" id="txt_pregunta" value="<?=$pregunta?>" >
							</td>
							<td colspan="2">
								<label>
									<input type="checkbox" id="VerPre" name="VerPre"   onclick="verPre();"/>
									<b>Ver Pregunta y Respuesta</b>
									</label>
						</td>
						</tr>
						<tr class="trInformacion">
							<td>&nbsp;</td>
							<td width="20%"><b>Respuesta Secreta: </b></td>
							<td width="17%">
							<input name="txt_respuesta"  type="password"  id="txt_respuesta" value="<?=base64_decode($respuesta)?>">
							</td>
							<td colspan="2">&nbsp;</td>
						</tr>
						<?
if ($_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==5)
{
						?>
						<tr class="trInformacion">
							<td height="31" colspan="5">
							<center>
								<b>Mi Perfil</b>
							</center></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="5"><?
							if (!empty($perfil))
								$editorPerfil -> Value = $perfil;
							else
								$editorPerfil -> Value = 'Escriba información que desee compartir con sus estudiantes ';
							$editorPerfil -> crearEditor();
							?></td>
						</tr>
						<?
						}
						else
						{
						?>
						<tr class="trInformacion">
							<td colspan="5">&nbsp;</td>
						</tr>
						<?
						}
						?>
						<tr class="trInformacion">
							<td height="36" colspan="5">
							<div align="center">
								<input name="btn_ingresar" id="btningresar" type="button" value="Cambiar datos" onClick="javascript:enviarDatos()">
								<input type="reset" name="sub_resetear" value="Restablecer">
								<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript:enviarCancelar('<?echo $flag;?>')">
							</div></td>
						</tr>
					</form>
				</table></td>
				<td class="tablaEspacio">&nbsp;</td>
				<td class="tablaDerecho"><?=menu1($_SESSION['idtipousuario']);?></td>
				<td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<?
}
else
{
redireccionar('../scripts/');
}
}
else
{
redireccionar('../login/');
}
}
else
{
redireccionar('../login/');
}
?>