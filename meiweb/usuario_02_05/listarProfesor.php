<?PHP
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');
	
  	$baseDatos=new BD();
	if(comprobarSession())
	{ 
		if (!empty($_GET['grupo'])) {
			$_POST['cbo_verUsuario']=$_GET['grupo'];
		}
	if ($_SESSION['idtipousuario']==2)
	{
		
		if(empty($_POST['cbo_ordenar']))
		{
			$_POST['cbo_ordenar']=3;
		}
		
		if($_POST['cbo_ordenar']==2)
		{
			$activoNombre="selected";
			
		}
		else if($_POST['cbo_ordenar']==1)
		{

			$activoCodigo="selected";
		}
		else 
		{
			$activoApellido="selected";			
		}
		
		if(!empty($_POST['idGrupo']))
		{
			$sql="SELECT mei_grupo.idmateria FROM  mei_grupo 
					WHERE mei_grupo.idgrupo='".$_POST['idGrupo']."'";
										
			$consultaMateria=$baseDatos->ConsultarBD($sql);
			
			list($idMateria)=mysql_fetch_array($consultaMateria);
			
			$_POST['cbo_verUsuario']=$_POST['idGrupo'].",".$idMateria;			
		}
				
		
		$cont=0;
		$contChk=0;
		
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?PHP echo  recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		</head>
		<body>
		<style type="text/css">
		a.imagen {
			position: relative;
		}


		a.imagen img{

			position: absolute;
			bottom: -500%;
			right: 100%;
			display:none;
		}

  		a.imagen:hover img{
  				display: block;
  		}

  		  	    </style>
		<script> 
		function eliminar(idGrupo,materia,idmateria)
		{
			if(confirm("¿Está seguro de eliminar los Usuarios seleccionados?"))
			{
			var contador=0;
					for(i=0;i<document.frm_verUsuario.elements.length;i++)
					{
							if (document.frm_verUsuario.elements[i].checked==true)
							{
								contador++;							
							}
					}
					
			if(contador>0)
				{
				document.frm_verUsuario.action="eliminarAlumno.php?idmateria="+idmateria+"&materia="+materia+"&idGrupo="+idGrupo;
				document.frm_verUsuario.submit();
				}
			else
				alert('No ha seleccionado ningún Alumno ')
			}
		}
		
		function eliminarBuscados(idProfesor)
		{
			if(confirm("¿Está seguro de eliminar los Usuarios seleccionados"))
			{
			var contador=0;
					for(i=0;i<document.frm_verUsuario.elements.length;i++)
					{
							if (document.frm_verUsuario.elements[i].checked==true)
							{
								contador++;							
							}
					}
					
			if(contador>0)
				{
				document.frm_verUsuario.action="eliminarAlumno.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&idProfesor="+idProfesor;
				document.frm_verUsuario.submit();
				}
			else
				alert('No ha seleccionado ningún Alumno')
			}
		}
		
		
		
		
		
		function chekear(codigoGrupo)
		{
			var todo=document.getElementById('grupo'+codigoGrupo);
			if (todo.checked==true)
				{
					for(i=0;i<document.frm_verUsuario.elements.length;i++)
					{
						if(document.frm_verUsuario.elements[i].id==codigoGrupo)
						document.frm_verUsuario.elements[i].checked=true;
					}
				}
			else
				{
					for(i=0;i<document.frm_verUsuario.elements.length;i++)
					{
						if(document.frm_verUsuario.elements[i].id==codigoGrupo)
						document.frm_verUsuario.elements[i].checked=false;
					}
				}
		}
		
		function crearAlumno(idGrupo,idMateria)
		{
			//salert(idGrupo+idMateria);
			location.replace("../usuario/crearAlumno.php?idmateria="+idMateria+"&materia=<?PHP echo $_GET['materia']; ?>&idGrupo="+idGrupo+"&destino=1");
		}
			function crearAlumnoPrueba(idGrupo,idMateria)
						{
							location.replace("../usuario/crearAlumnoPrueba.php?idmateria="+idMateria+"&materia=<?PHP echo $_GET['materia']; ?>&idGrupo="+idGrupo);
						}			
		
			function eliminarUsuario(codigoUsuario)
			{
				
				if(confirm("¿Está seguro de eliminar este usuario?"))
				{
					location.replace("eliminarAlumno.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&codigoUsuario="+codigoUsuario);					
				}
			}
			function eliminarUsuarioGrupo(codigoUsuario,codigoGrupo,orden)
			{
				if(confirm("¿Está seguro de eliminar este usuario del grupo?"))
				{
					location.replace("eliminarAlumno.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&codigoUsuario="+codigoUsuario+"&codigoGrupo="+codigoGrupo+"&orden="+orden);					
				}
			}
			
			//-----------------------------------------------------------------------------------------------------------------------------------------
			function bloquearSesionUsuarioUno(codigoUsuario,idProfesor,codigoGrupo) 
			{
				if(confirm("¿Está seguro que quiere bloquear este usuario del examen?"))
				{
					location.replace("../login/examenDesactivadoUsuario.php?codigoUsuario="+codigoUsuario+"&idProfesor="+idProfesor+"&codigoGrupo="+codigoGrupo);
				}
			}
//-----------------------------------------------------------------------------------------------------------------------------------------
			function bloquearSesionUsuarioDos(codigoUsuario,codigoGrupo,orden) 
			{
				if(confirm("¿Está seguro que quiere bloquear este usuario del examen?"))
				{
					location.replace("../login/sesionDesactivadaUsuario.php?codigoUsuario="+codigoUsuario+"&codigoGrupo="+codigoGrupo+"&orden="+orden);
				}
			}
//-----------------------------------------------------------------------------------------------------------------------------------------
			function reiniciarUsuario(codigoUsuario)
			{
				if(confirm("¿Está seguro de reinicar este usuario?"))
				{
					location.replace("reiniciarUsuario.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&codigoUsuario="+codigoUsuario);					
				}
			}
			
		function enviar()
		{
			document.frm_verUsuario.submit();				
		}
		
		function crearSubgrupo(idGrupo)
		{
			location.replace("../materias/crearSubgrupo.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&idGrupo="+idGrupo);
		}
		function ordenarLista()
		{
			document.frm_verUsuario.submit();				
		}
		
		
		
		var ventanaDocumento=false;
				
		function imprimirDocumento(idUsuario,idGrupo,orden)
		{		
											
						if (typeof ventanaDocumento.document == 'object')
						 {
							ventanaDocumento.close()
						 }
						 
						ventanaDocumento = window.open('../materias/imprimirListaGrupo.php?idUsuario='+idUsuario+'&idGrupo='+idGrupo+'&orden='+orden,'ImprimirDocumento','width=700,height=650,left=10,top=10,scrollbars=YES,menubars=NO,status=NO,statusbar=NO,resizable=NO,location=NO');
						
		}
				
			function verBitacora(codigoUsuario,orden)
			{
					location.replace("../bitacora/index.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&idUsuario="+codigoUsuario+"&orden="+orden);					
			}
		function eliminarSesionUsuario(codigoUsuario,orden)
		{
			if(confirm("Está seguro que desea desbloquear la cuenta del estudiante seleccionado?"))
				{
					location.replace("../login/salirSistemaUsuario.php?idUsuario="+codigoUsuario+"&orden="+orden);	
				}
		}
		</script>
		<table class="tablaPrincipal">
			<tr valign="top">
				<td class="tablaEspacio">&nbsp;</td>
				<td class="tablaIzquierdo"><?PHP echo menu($_SESSION['idtipousuario']);?></td>
				<td class="tablaEspacio">&nbsp;</td>
				<td><form name="frm_verUsuario" method="post" action="<?PHP echo  $_SERVER['PHP_SELF']; ?>">
					<table class="tablaGeneral" >
						<tr class="trSubTitulo">
							<td>Buscar Alumno:</td>
							<td colspan="3"><input name="txt_buscarUsuario" type="text" value="<?PHP echo  @$_POST['txt_buscarUsuario']; ?>" size="50">
								<input type="submit" name="sub_buscar" value="Buscar"></td>
						</tr>
						<tr class="trSubTitulo">
							<td width="25%">Ver Alumnos de:</td>
							<td width="37%"><select name="cbo_verUsuario" onChange="javascript:enviar()">
								<option class="link" value="1">Todos los Usuarios por Materias</option>
<?PHP 							
									mysql_query("SET NAMES 'utf8'");

									$sql="SELECT mei_grupo.idgrupo,mei_grupo.nombre,mei_materia.idmateria,mei_materia.nombre 
										FROM mei_grupo, mei_materia WHERE mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo 
										FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")
										AND mei_grupo.idmateria=mei_materia.idmateria ";

								$resultado=$baseDatos->ConsultarBD($sql);
								while (list($codigoGrupo,$grupo,$codigoMateria,$materia)=mysql_fetch_array($resultado))
								{
								$codigoGrupoS=$codigoGrupo+10;
								unset($seleccion); 
								if (isset($_POST['cbo_verUsuario'])) { $codigoMateriaP = $_POST['cbo_verUsuario']; } else { $codigoMateriaP = Null; }
								

								?>
									<option class="link" value="<?PHP echo  $codigoGrupoS.','.$codigoMateria; ?>" 
						 <?PHP if($codigoGrupoS.','.$codigoMateria==$codigoMateriaP)
								{
									echo $seleccion="selected";
								}  ?>><?PHP echo $grupo.' '.$materia; ?></option>
<?PHP
								}
								?>
										</select> 
								</td>
								<td width="25%"><div align="center">Ordenar Lista Por:</div></td>
								<td width="13%"><select name="cbo_ordenar" onChange="javascript:ordenarLista()">
									<option value="1" <?PHP echo @$activoCodigo; ?>>C&oacute;digo</option>
									<option value="2" <?PHP echo @$activoNombre; ?>>Nombre</option>
									<option value="3" <?PHP echo @$activoApellido; ?>>Apellido</option>
								</select></td>
							</tr>
					</table>
<?PHP
				// BUSQUEDA
				
				if(!empty($_POST['txt_buscarUsuario']))
				{
				
					$cadena=explode('+',$_POST['txt_buscarUsuario']);
										
					foreach($cadena as $palabra)
					{
						if(empty($busqueda))
						{
							$busqueda="mei_usuario.idusuario LIKE '%".$palabra."%'";
							$busqueda.=" OR CONCAT(mei_usuario.primernombre,' ',mei_usuario.segundonombre,' ',mei_usuario.primerapellido,' ',mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
						}
						else
						{
							$busqueda.=" OR mei_usuario.idusuario LIKE '%".$palabra."%'";
							$busqueda.=" OR CONCAT(mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
						}
			
					}
					
					
					if($_POST['cbo_ordenar']==1)
					{
						$sql="SELECT mei_grupo.nombre,mei_grupo.idgrupo,mei_usuario.idtipousuario,mei_usuario.idtipoalumno,mei_usuario.idusuario,
								 mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.estado 
							FROM mei_grupo,mei_usuario,mei_relusugru 
								WHERE mei_grupo.idgrupo=mei_relusugru.idgrupo 
									AND  mei_relusugru.idgrupo 
										IN (
											SELECT mei_relusugru.idgrupo FROM mei_relusugru 
												WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']."
											)
										AND mei_relusugru.idusuario=mei_usuario.idusuario 
											AND mei_usuario.idtipousuario IN (3,7) 
												AND (".$busqueda.") ORDER BY(mei_usuario.idusuario)";
					
					}
					else if($_POST['cbo_ordenar']==3)
					{
						$sql="SELECT mei_grupo.nombre,mei_grupo.idgrupo,mei_usuario.idtipousuario,mei_usuario.idtipoalumno,mei_usuario.idusuario,
								 mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.estado 
							FROM mei_grupo,mei_usuario,mei_relusugru 
								WHERE mei_grupo.idgrupo=mei_relusugru.idgrupo 
									AND  mei_relusugru.idgrupo 
										IN (
											SELECT mei_relusugru.idgrupo FROM mei_relusugru 
												WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']."
											)
										AND mei_relusugru.idusuario=mei_usuario.idusuario 
											AND mei_usuario.idtipousuario IN (3,7)
												AND (".$busqueda.") ORDER BY(mei_usuario.primerapellido)";
					
					}
					else
					{
						
						$sql="SELECT mei_grupo.nombre,mei_grupo.idgrupo,mei_usuario.idtipousuario,mei_usuario.idtipoalumno,mei_usuario.idusuario,
								 mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.estado 
							FROM mei_grupo,mei_usuario,mei_relusugru 
								WHERE mei_grupo.idgrupo=mei_relusugru.idgrupo 
									AND  mei_relusugru.idgrupo 
										IN (
											SELECT mei_relusugru.idgrupo FROM mei_relusugru 
												WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']."
											)
										AND mei_relusugru.idusuario=mei_usuario.idusuario 
											AND mei_usuario.idtipousuario IN (3,7) 
												AND (".$busqueda.") ORDER BY(mei_usuario.primernombre)";
						
											
					}
					
					
											
					$resultado=$baseDatos->ConsultarBD($sql);
					
					$numRegistros=mysql_num_rows($resultado);
					
					if($numRegistros>0)
					{
						$color="trListaClaro";
						
						if($numRegistros == 1)
						{
							$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registro encontrado.";
						}
						else
						{
							$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registros encontrados.";
						}
						
					?>
				<br>
				<table class="tablaGeneral" >
					<tr class="trSubTitulo">
						<td colspan="5"><?PHP echo  $cadenaResultado; ?></td>
					</tr>
					<tr class="trSubTitulo">
						<td width="15%" class="trSubTitulo" colspan="2"><div align="center">C&oacute;digo</div></td>
						<td width="65%" class="trSubTitulo"><div align="center">Nombre del Alumno</div></td>
						<td width="10%" class="trSubTitulo"><div align="center">Editar</div></td>
						<td width="10%" class="trSubTitulo"><div align="center">Bitacora</div></td>
					</tr>
<?PHP
					 while(list($grupo,$codigoGrupo,$codigoTipo,$codigoTipoAlumno,$codigoUsuario,$nombre1,$nombre2,$apellido1,$apellido2,$estado)=mysql_fetch_array($resultado))
					{
					  			if($cont%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}
					?>
					<tr class="<?PHP echo $color; ?>">

<?PHP
$sql="SELECT imagen FROM mei_usuario WHERE idusuario=".$_GET['idusuario'];
$resultado=$baseDatos->ConsultarBD($sql);
list($imagen)=mysql_fetch_array($resultado);
$nombreImagen='../../datosMEIWEB/fotosUsuarios/'.$imagen;
if ($imagen==-1) {
	$nombreImagen='imagenes/avatar.jpg';
}
else $nombreImagen='../../datosMEIWEB/fotosUsuarios/'.$imagen;

								if($codigoTipoAlumno==1)
								{
								?>
						<td class="<?PHP echo $color; ?>" title="Alumno de Pregrado" width="5%"><div align="right"><?PHP echo ++$cont; ?>
							<input id="<?PHP echo $codigoGrupo; ?>" type="checkbox" name="<?PHP echo  "chk_usuario".$contChk++; ?>" value="<?PHP echo "$codigoUsuario"; ?>"></div></td>
						<td class="<?PHP echo $color; ?>" title="Alumno de Pregrado"><div align="left">&nbsp;   <a class="imagen"> <?PHP echo $codigoUsuario; ?>  <img src="<?=$nombreImagen?>" width="120" height="120"> </a>   </div></td>
                      <?PHP
								}
								else if($codigoTipoAlumno==2)
								{
								?>
						<td class="<?PHP echo $color; ?>" title="Alumno de Maestria" width="5%"><div align="right">
                          <a class="imagen"> <?PHP echo $codigoUsuario; ?>  <img src="<?=$nombreImagen?>" width="120" height="120"> </a> 
                        <input id="<?PHP echo $codigoGrupo; ?>" type="checkbox" name="<?PHP echo  "chk_usuario".$contChk++; ?>" value="<?PHP echo $codigoUsuario; ?>">
                      </div></td>
						<td class="<?PHP echo $color; ?>" title="Alumno de Maestria"><div align="left">&nbsp;<b>
                           <a class="imagen"> <?PHP echo $codigoUsuario; ?>  <img src="<?=$nombreImagen?>" width="120" height="120"> </a> 
                      </b></div></td>
                      <?PHP
								}
								else if($codigoTipoAlumno==3)
								{
								?>
						<td class="<?PHP echo $color; ?>" title="Alumno de Prueba" width="5%"><div align="right">
                        <?PHP echo ++$cont; ?>
                        <input id="<?PHP echo $codigoGrupo; ?>" type="checkbox" name="<?PHP echo  "chk_usuario".$contChk++; ?>" value="<?PHP echo $codigoUsuario; ?>">
                      </div></td>
						<td class="<?PHP echo $color; ?>" title="Alumno de Prueba"><div align="left">&nbsp;<b>
                        <?PHP echo $codigoUsuario; ?>
                      </b></div></td>
                      <?PHP
								}
								?>
						<td class="<?PHP echo $color; ?>">&nbsp;
                          <?PHP echo  $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2; ?></td>
						<td class="<?PHP echo $color; ?>" align="center">
							<table class="tablaPrincipal" align="centar">
								<tr>
									<td width="35%"><div align="center"><a href="modificarAlumno.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&codigoUsuario=<?PHP echo $codigoUsuario; ?>&grupo=<? echo "1"; ?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Alumno" width="16" height="16" border="0"></a></div></td>
									<td width="30%">&nbsp;</td>
									<td width="35%"><div align="center"><a href="javascript: eliminarUsuarioGrupo('<?PHP echo  $codigoUsuario; ?>','<?PHP echo  $codigoGrupo; ?>','1')"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Alumno" width="16" height="16" border="0"></a></div></td>
								</tr>
							</table>
						</td>
                      <?PHP
											if($estado==1)
											{
											?>
                      <td class="<?PHP echo $color; ?>" align="center"><a href="javascript: verBitacora('<?PHP echo  $codigoUsuario; ?>','1')"  class="link"><img src="../bitacora/imagenes/bitacora.gif" alt="Ver Registro de Bitacora de este Alumno" width="16" height="16" border="0"></a></td>
                      <?PHP
											}
											else  if($estado==0)
											{
											?>
                      <td class="<?PHP echo $color; ?>" align="center"><img src="../bitacora/imagenes/bitacoraInactiva.gif" alt="No existe Bitacora de este Alumno" width="16" height="16" border="0"></td>
                      <?PHP
											}
											?>
                    </tr>
                    <?PHP
					}
								?>
                    <tr class="trInformacion">
                      <td colspan="8"><div align="center">
                          <input type="button" name="btn_eliminarAlumnoBuscado" value="Eliminar Seleccionados" onClick="javascript: eliminarBuscados('<?PHP echo  $_SESSION['idusuario']; ?>')">
                          <input name="hid_contUsuarioBuscados" type="hidden" id="hid_contUsuarioBuscados" value="<?PHP echo  $contChk; ?>">
                      </div></td>
                    </tr>
                  </table>
                <?PHP
					}
					else
					{
					?>
					<br>
				<table class="tablaGeneral" >
                    <tr class="trAviso">
                      <td>No se encontraron coincidencias con <i>
                        <?PHP echo $_POST['txt_buscarUsuario']; ?>
                      </i> </td>
                    </tr>
                  </table>
                <?PHP
					}
				}

?>
                  <?PHP
//FIN BUSQUEDA				
				
				
				if(empty($_POST['cbo_verUsuario']))
				{
						$_POST['cbo_verUsuario']=1;
				}

				$sql="SELECT mei_materia.idmateria
								FROM mei_grupo, mei_materia WHERE mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo 
								FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")
								AND mei_grupo.idmateria=mei_materia.idmateria";
				$resultado=$baseDatos->ConsultarBD($sql);
				list($codigoMateria)=mysql_fetch_array($resultado);
				
				if(!empty($codigoMateria))
				{
				?>
                <br>
				<table class="tablaGeneral">
					<tr class="trTitulo">
						<td colspan="8" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Ver Alumnos</td>
					</tr>
				</table>
                <?PHP
					switch ($_POST['cbo_verUsuario'])
					{
						case 1: 
							$sql="SELECT DISTINCT(mei_materia.idmateria),mei_materia.nombre 
								FROM mei_grupo, mei_materia WHERE mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo 
								FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")
								AND mei_grupo.idmateria=mei_materia.idmateria ";
						break;
						default:
						list($codigoGrupo,$codigoMateria)=explode(',',$_POST['cbo_verUsuario']);
							$sql="SELECT mei_materia.idmateria,mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$codigoMateria;
						break;
					}
						$materias=$baseDatos->ConsultarBD($sql);

					while (list($codigoMateria,$materia)=mysql_fetch_array($materias))
						{
						?>
				<table class="tablaGeneral" >
						<tr class="trSubTitulo">
						<td colspan="9" class="trSubTitulo"><?PHP echo $materia; ?></td>
						</tr>
                    <?PHP
					switch ($_POST['cbo_verUsuario'])
					{
						case 1: 
							$sql="SELECT mei_grupo.idgrupo,mei_grupo.nombre FROM mei_grupo WHERE mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].") AND mei_grupo.idmateria=".$codigoMateria;
						break;
						default:
						list($codigoGrupo,$codigoMateria)=explode(',',$_POST['cbo_verUsuario']);
							$sql="SELECT mei_grupo.idgrupo,mei_grupo.nombre FROM mei_grupo WHERE mei_grupo.idgrupo=".($codigoGrupo-10);
						break;
					}
							$grupos=$baseDatos->ConsultarBD($sql);
							
							while (list($codigoGrupo,$grupo)=mysql_fetch_array($grupos))
							{
								if($_POST['cbo_ordenar']==2)
								{
								$sql="SELECT mei_usuario.idusuario,mei_usuario.idtipoalumno,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.estado 
										FROM mei_usuario WHERE mei_usuario.idtipousuario IN (3,7) AND mei_usuario.idusuario IN (SELECT mei_relusugru.idusuario 
											FROM mei_relusugru WHERE mei_relusugru.idgrupo=".$codigoGrupo.") ORDER BY mei_usuario.primernombre";
								}
								else if($_POST['cbo_ordenar']==1)
								{
									$sql="SELECT mei_usuario.idusuario,mei_usuario.idtipoalumno,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.estado 
										FROM mei_usuario WHERE mei_usuario.idtipousuario IN (3,7) AND mei_usuario.idusuario IN (SELECT mei_relusugru.idusuario 
											FROM mei_relusugru WHERE mei_relusugru.idgrupo=".$codigoGrupo.") ORDER BY mei_usuario.idusuario";
								}
								else 
								{
								
								$sql="SELECT mei_usuario.idusuario,mei_usuario.idtipoalumno,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.estado 
										FROM mei_usuario WHERE mei_usuario.idtipousuario IN (3,7) AND mei_usuario.idusuario IN (SELECT mei_relusugru.idusuario 
											FROM mei_relusugru WHERE mei_relusugru.idgrupo=".$codigoGrupo.") ORDER BY mei_usuario.primerapellido";
								
								}
									
							$resultado=$baseDatos->ConsultarBD($sql);
							$numeroAlumnos=mysql_num_rows($resultado);
								
								?>
						<tr class="trSubTitulo">
							<td colspan="5" class="trSubTitulo"><div align="left">
								<input id="grupo<?PHP echo $codigoGrupo; ?>" type="checkbox" name="chk_grupo<?PHP echo $codigoGrupo; ?>" onClick="javascript:chekear('<?PHP echo $codigoGrupo; ?>')" title="Seleccionar todos los Alumnos de este Grupo">Grupo<?PHP echo $grupo; ?></div></td>
                      <?PHP
									if(empty($numeroAlumnos))
									{
?>
							<td colspan="4" class="trSubTitulo"><div align="right">&nbsp;</div></td>
                      <?PHP
									
									}
									else
									{
									?>
							<td colspan="4" class="trSubTitulo"><div align="right"><a href="javascript: imprimirDocumento('<?PHP echo $_SESSION['idusuario']; ?>','<?PHP echo $codigoGrupo; ?>','<?PHP echo  $_POST['cbo_ordenar']; ?>')" class="link"><img src="imagenes/imprimir.gif" border="0" alt="Imprimir Lista de Subgrupos"> Imprimir Lista</a></div></td>
                      <?PHP
									}
									?>
					</tr>
                    <?PHP
				

				if(empty($numeroAlumnos))
				{
				?>
					<tr class="trAviso">
						<td colspan="9">No se han creado Alumnos en el Grupo
                        <?PHP echo $grupo; ?></td>
					</tr>
					<tr>
						<td class="trInformacion" colspan="9"><div align="center">
							<input type="button" id="<?$codigoMateria?>" name="btn_crearAlumno" value="Agregar Alumno" onClick="javascript: crearAlumno('<?PHP echo  $codigoGrupo; ?>','<?PHP echo $codigoMateria; ?>')">
						</div></td>
					</tr>
                    <?PHP
				}
				else
				{
				
				
				$sql="SELECT mei_subgrupo.idsubgrupo FROM mei_subgrupo WHERE mei_subgrupo.idgrupo='".$codigoGrupo."'";
				
				$consultaSubgrupo=$baseDatos->ConsultarBD($sql);
				
				$numSubgrupos=mysql_num_rows($consultaSubgrupo);
				
				if($numSubgrupos > 0)
				{
					if($numSubgrupos == 1)
					{
						$cadenaSubgrupos="&nbsp;&nbsp; Existe ".$numSubgrupos." Subgrupo de Clase";
					}
					else
					{
						$cadenaSubgrupos="&nbsp;&nbsp; Existe ".$numSubgrupos." Subgrupos de Clase";
					}
					
				?>
                    <tr class="trAviso">
                      <td colspan="3">&nbsp;</td>
                      <td colspan="6"><div align="right"><img src="imagenes/usuario.gif" width="16" height="16"><b> <a href="../materias/listarSubgrupos.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&idGrupo=<?PHP echo  $codigoGrupo; ?>" class="link">Ver Subgrupos</a></b></div></td>
                    </tr>
                    <?PHP
				}
				?>
                    <tr class="trSubTitulo">
                      <td width="15%" class="trSubTitulo" colspan="2"><div align="center">C&oacute;digo</div></td>
                      <td width="50%" class="trSubTitulo"><div align="center">Nombre del Alumno</div></td>
                      <td width="6%" class="trSubTitulo"><div align="center">Editar</div></td>
                      <td width="6%" class="trSubTitulo"><div align="center">Bitacora</div></td>
                      <td width="6%" class="trSubTitulo"><div align="center">Desbloquear</div></td>
                      <!--<td width="5%" class="trSubTitulo"><div align="center">BloquearInts</div></td>-->
		      		  <td width="5%" class="trSubTitulo"><div align="center">BloquearSess</div></td>
                      <td width="7%" class="trSubTitulo"><div align="center">Correo</div></td>
                    </tr>
                    <?PHP 
								$cont=0;
								$contChk=0;
								
								while (list($codigoUsuario,$codigoTipoAlumno,$nombre1,$nombre2,$apellido1,$apellido2,$estado)=mysql_fetch_array($resultado))
								{
								if($cont%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}
								
								?>
                    <tr class="<?PHP echo $color; ?>">
                      <td class="<?PHP echo $color; ?>" align="right" width="5%"><?PHP echo $cont+1; ?>
                          <input id="<?PHP echo $codigoGrupo; ?>" type="checkbox" name="<?PHP echo  "chk_usuario".$contChk++; ?>" value="<?PHP echo $codigoUsuario; ?>"></td>
                      <?PHP

                      $sql="SELECT imagen FROM mei_usuario WHERE idusuario=".$codigoUsuario;
$resultado1=$baseDatos->ConsultarBD($sql);
list($imagen)=mysql_fetch_array($resultado1);
if ($imagen==-1) {
	$nombreImagen='imagenes/avatar.jpg';
}
else $nombreImagen='../../datosMEIWEB/fotosUsuarios/'.$imagen;


								if($codigoTipoAlumno==1)
								{
								?>
                      <td class="<?PHP echo $color; ?>" title="Alumno de Pregrado">&nbsp;
                            
                          <a class="imagen"> <?PHP echo $codigoUsuario; ?>  <img src="<?=$nombreImagen?>" width="120" height="120"> </a>       
                          </td>
                      <?PHP
								}
								else if($codigoTipoAlumno==2)
								{
								?>
                      <td class="<?PHP echo $color; ?>" title="Alumno de Maestria">&nbsp;<b>
                          <a class="imagen"> <?PHP echo $codigoUsuario; ?>  <img src="<?=$nombreImagen?>" width="120" height="120"> </a> 
                      </b></td>
                      <?PHP
								}
								else if($codigoTipoAlumno==3)
								{
								?>
                      <td class="<?PHP echo $color; ?>" title="Alumno de prueba">&nbsp;<b>
                         <a class="imagen"> <?PHP echo $codigoUsuario; ?>  <img src="<?=$nombreImagen?>" width="120" height="120"> </a> 
                      </b></td>
					  <?PHP
                      }
					  ?>
								
                                
                      <td class="<?PHP echo $color; ?>">&nbsp;
                          <?PHP echo  $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2; ?></td>
                      <td class="<?PHP echo $color; ?>" align="center">
						<table class="tablaPrincipal" align="centar">
                          <tr>
                            <td width="35%"><div align="center"><a href="modificarAlumno.php?idmateria=<?PHP echo $codigoMateria; ?>&materia=<?PHP echo $_GET['materia']; ?>&codigoUsuario=<?PHP echo $codigoUsuario; ?>&grupo=<?PHP echo $_POST['cbo_verUsuario'];?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Alumno" width="16" height="16" border="0"></a></div></td>
                            <td width="30%">&nbsp;</td>
                            <td width="35%"><div align="center"><a href="javascript: eliminarUsuarioGrupo('<?PHP echo  $codigoUsuario; ?>','<?PHP echo  $codigoGrupo; ?>','<?PHP echo $_POST['cbo_verUsuario'];?>')"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Alumno" width="16" height="16" border="0"></a></div></td>
                          </tr>
                      </table></td>
                      <?PHP
											if($estado==1)
											{
											?>
                      <td class="<?PHP echo $color; ?>" align="center"><a href="javascript: verBitacora('<?PHP echo  $codigoUsuario; ?>','<?PHP echo $_POST['cbo_verUsuario'];?>')"  class="link"><img src="../bitacora/imagenes/bitacora.gif" alt="Ver Registro de Bitacora de este Alumno" width="16" height="16" border="0"></a></td>
                      <?PHP
											}
											else  if($estado==0)
											{
											?>
                      <td class="<?PHP echo $color; ?>" align="center"><img src="../bitacora/imagenes/bitacoraInactiva.gif" alt="No existe Bitacora de este Alumno" width="16" height="16" border="0"></td>
                      <?PHP
											}
											?>
						<td class="<?PHP echo $color; ?>"><div align="center"><a href="javascript: eliminarSesionUsuario('<?PHP echo  $codigoUsuario; ?>','<?PHP echo $_POST['cbo_verUsuario'];?>')"  class="link"><img src="imagenes/eliminar.gif" alt="Desbloquear" width="16" height="16" border="0"></a></div></td>											
                    	<!-- ***************************************************************************************************************************** -->

<!--<td class="<?PHP echo $color; ?>"><div align="center"><a href="#"><img src="imagenes/eliminar.gif" alt="Bloquear Alumno-Previo" width="16" height="16" border="0"></a></div></td>-->

<!-- ***************************************************************************************************************************** -->

<td class="<?PHP echo $color; ?>"><div align="center"><a href="javascript: bloquearSesionUsuarioDos('<?PHP echo  $codigoUsuario; ?>','<?PHP echo  $codigoGrupo; ?>','<?PHP echo $_POST['cbo_verUsuario'];?>')"  class="link"><img src="../evaluaciones/imagenes/closed.gif" alt="Bloquear Alumno-Sesion" width="16" height="16" border="0"></a></div></td>

<!-- ***************************************************************************************************************************** -->
						
                    	<td class="<?PHP echo $color; ?>"><div align="center"><a href="../correo/redactarCorreo.php?idmateria=<?=$codigoMateria?>&materia=<?=$materia?>&destinatario=<?=$codigoUsuario?>"  class="link"><img src="imagenes/correo.gif" alt="Enviar Correo" width="16" height="16" border="0"></a></div></td>
                    </tr>
                    <?PHP
								$cont++;
								}//FIN WHILE
								?>
                    <tr>
                      <td colspan="9" class="trInformacion"><div align="center">
                        <input type="button" id="<?=$codigoMateria?>" name="btn_crearAlumno" value="Agregar Alumno" onClick="javascript: crearAlumno('<?PHP echo  $codigoGrupo; ?>','<?PHP echo  $codigoMateria; ?>')">
                        <!--boton que cambiamos de ubicamos para poder cargar un alumno de pruba desde listar alumnos(lina, jaime 9 de junio de 2010-->
                        <input name="btn_agregarAlumno2" type="button" value="Agregar Alumno de Prueba" onClick="javascript: crearAlumnoPrueba('<?PHP echo  $codigoGrupo; ?>')">
                        <input type="button" name="btn_crearAlumno" value="Crear Subgrupo" onClick="javascript: crearSubgrupo('<?PHP echo  $codigoGrupo; ?>')">
                        <input type="button" name="btn_eliminarAlumno" value="Eliminar Seleccionados" onClick="javascript: eliminar('<?PHP echo  $codigoGrupo; ?>','<?PHP echo  $materia; ?>','<?PHP echo  $codigoMateria; ?>')">
                      </div></td>
                    </tr>
                    <tr>
                      <td colspan="9" class="trInformacion"><div align="center">
						Nota: El alumno auxiliar, necesita primero crear el estudiante y despues dar en editar y seleccionar como auxiliar.
                      </div></td>
                    </tr>					
                    <?PHP
							}
							}//FIN WHILE
						?>
                  </table>
				<br>
                  <?PHP
						}//FIN WHILE
					  ?>
				<br>
                  <?PHP
				}
				else
				{
				?>
				<table class="tablaGeneral" >
					<tr class="trTitulo" >
						<td><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Ver Usuarios</td>
					</tr>
					<tr class="trAviso">
						<td>Actualmente no existen alumnos registrados</td>
					</tr>
					</table>
                <?PHP
				}
// FIN Usuario Alumno (PUEDE VER ADMON)			
		
				?>
					<input name="hid_contUsuario" type="hidden" value="<?PHP echo $contChk; ?>">
				</form>
				</td>
				<td class="tablaEspacio">&nbsp;</td>
				<td class="tablaDerecho"><?PHP echo menu1($_SESSION['idtipousuario']); ?></td>
				<td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
		</body>
		</html>
<?PHP 
	}
	else
	{
		redireccionar('../login/');
	}

	}
	else
	{
		redireccionar('../login/');
	}
	
?>
