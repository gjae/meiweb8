<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		?>
			<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
				<script language="javascript">
				
					function enviarCrear()
					{
						if(comprobarDatos())
						{
							document.frm_crearProfesor.submit();
						}
					}
					
					function enviarCancelar()
					{
						location.replace("listarUsuariosAdministrador.php");
					}
					
					function borrarFormulario()
					{
						document.frm_crearProfesor.txt_codigoProfesor.value="";
						document.frm_crearProfesor.txt_primerNombre.value="";
						document.frm_crearProfesor.txt_segundoNombre.value="";
						document.frm_crearProfesor.txt_primerApellido.value="";
						document.frm_crearProfesor.txt_segundoApellido.value="";
					}
				
					function comprobarDatos()
					{
						if(document.frm_crearProfesor.txt_codigoProfesor.value == false || isNaN(parseInt(document.frm_crearProfesor.txt_codigoProfesor.value)))
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(document.frm_crearProfesor.txt_primerNombre.value == false || !isNaN(parseInt(document.frm_crearProfesor.txt_primerNombre.value)))
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(!isNaN(parseInt(document.frm_crearProfesor.txt_segundoNombre.value)))
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(document.frm_crearProfesor.txt_primerApellido.value == false || !isNaN(parseInt(document.frm_crearProfesor.txt_primerApellido.value)))
						{
							alert("Debe llenar completamente la información solicitadao");
						}
						else if(!isNaN(parseInt(document.frm_crearProfesor.txt_segundoApellido.value)))
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else
						{
							return true;
						}
					}
								
				
				</script>
				
				<style type="text/css">
				<!--
				.Estilo1 {color: #FF0000}
				-->
				</style>
				
				</head>
				<body>
				
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral">
					<form action="insertarProfesor.php" method="post" name="frm_crearProfesor">
						<tr>
							<td width="3%" class="trTitulo"><img src="imagenes/usuario.gif" width="17" height="18"></td>
							<td width="97%" colspan="2" class="trTitulo">Crear Profesor</td>
						</tr>
				<?
					if($_GET['error'] == '0x001')
					{
				?>
						<tr class="trAviso">
							<td width="3%"><img src="imagenes/error.gif" width="17" height="18"></td>
							<td colspan="2"><span class="Estilo1">Error: Ya existe un profesor registrado con ese c&oacute;digo </span></td>
						</tr>
				<?
					}
				?>
					<tr class="trInformacion">
					  <td colspan="2">&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr class="trInformacion">
						  <td colspan="2"><strong>C&oacute;digo:</strong></td>
						  <td width="75%"><input name="txt_codigoProfesor" type="text" id="txt_codigoProfesor" value="<?= $_GET['codigoProfesor']?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Primer Nombre : </strong></td>
						<td><input name="txt_primerNombre" type="text" id="txt_primerNombre" value="<?= $_GET['primerNombre']?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Segundo Nombre : </strong></td>
						<td><input name="txt_segundoNombre" type="text" id="txt_segundoNombre" value="<?= $_GET['segundoNombre']?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Primer Apellido : </strong></td>
						<td><input name="txt_primerApellido" type="text" id="txt_primerApellido" value="<?= $_GET['primerApellido']?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Segundo Apellido :</strong></td>
						<td><input name="txt_segundoApellido" type="text" id="txt_segundoApellido" value="<?= $_GET['segundoApellido']?>" size="40">
					    <input name="hid_idGrupo" type="hidden" id="hid_idGrupo" value="<?= $_GET['idGrupo']?>"></td>
					</tr>
					<tr class="trInformacion">
							<td colspan="3"><input type="checkbox" name="chk_administrador" value="1">
						    Activar este usuario como Administrador
							</td>
						</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center">
						  <input name="btn_reset" type="button" value="Restablecer" onClick="javascript: borrarFormulario()">
						  <input name="btn_crearProfesor" type="button" id="btn_crearProfesor" value="Crear Profesor" onClick="javascript: enviarCrear();">
					      <input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()">
						  </div></td>
				 </tr>
						  </form>
				</table>
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho">&nbsp;</td>            
                <td class="tablaEspacioDerecho">&nbsp;</td>              
					</tr>
				</table>

				</body>
			</html>
		<? 
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>
