<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');		
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
	if ($_SESSION['idtipousuario']==2)
	{
		if(!empty($_GET['idGrupo']))
		{
		
		$directorioBase="../conecciones";
	
		$pathBase=opendir($directorioBase);
		$cont=0;
		
		$archivoSeparador="../configuracion/archivos/indiceSeparador.dat";
		
		if(file_exists($archivoSeparador))
		{
			$indiceSeparador=file($archivoSeparador);
		}
	
		
		while ($directorio = readdir($pathBase)) 
		{
			if(!is_file($directorio) && $directorio!= "." && $directorio!= "..")
			{
				$listaIndexConeccion[$cont]=$directorio.";";
				$listaIndexConeccion[$cont].=file_get_contents($directorioBase."/".$directorio."/indiceConeccion.conf");
				$cont++;
			}
		}
		closedir($pathBase);
		
		$sql="SELECT mei_materia.codigo, mei_materia.nombre, mei_grupo.nombre
				FROM mei_materia, mei_grupo
					WHERE mei_grupo.idmateria = mei_materia.idmateria AND mei_grupo.idgrupo=".$_GET['idGrupo'];

		$consultaGrupo=$baseDatos->ConsultarBD($sql);
		
		list($idMateria,$nombreMateria,$codigoGrupo)=mysql_fetch_array($consultaGrupo);	
		

		
		?>
			<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
				<script language="javascript">
					
					function enviarCarga()
					{
						if(comprobarDatos())
						{
							if(document.frm_cargarAlumnos.cbo_coneccion.disabled == true)
							{
								document.frm_cargarAlumnos.action="cargarArchivo.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>";
							}
							else
							{
								document.frm_cargarAlumnos.action="../conecciones/"+document.frm_cargarAlumnos.cbo_coneccion.value+"/coneccion.php?idmateria=<?=$_GET['idmateria']?>";
							}
						
							document.frm_cargarAlumnos.submit();
						}
					}
					
					function comprobarDatos()
					{
						
						if(document.frm_cargarAlumnos.cbo_coneccion.disabled == true )
						{
							if(parseInt(document.frm_cargarAlumnos.cbo_separador.value) == 0)
							{
								alert("Debe seleccionar un separador");
							}
							else if(document.frm_cargarAlumnos.fil_archivo.value == false)
							{
								alert("Debe seleccionar un archivo");
							}
							else
							{
								return true;
							}			
						}
						else
						{
							if(parseInt(document.frm_cargarAlumnos.cbo_coneccion.value) == 0)
							{
								alert("Debe seleccionar una conexión")
							}							
							else
							{
								return true;
							}
						}
				
					}				
					
					function enviarCancelar(idGrupo)
					{
						location.replace("crearAlumno.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idGrupo="+idGrupo+"&destino=<?=$_GET['destino']?>");
					}				
				
					function activarCargaConeccion()
					{
						document.frm_cargarAlumnos.cbo_coneccion.disabled=false;
						document.frm_cargarAlumnos.cbo_separador.disabled=true;
						document.frm_cargarAlumnos.fil_archivo.disabled=true;
						
					}
					
					function desactivarCargaConeccion()
					{
						document.frm_cargarAlumnos.cbo_coneccion.disabled=true;
						document.frm_cargarAlumnos.cbo_separador.disabled=false;
						document.frm_cargarAlumnos.fil_archivo.disabled=false;
						
					}
					
					function restablecer()
					{
						document.frm_cargarAlumnos.cbo_coneccion.disabled=true;
						document.frm_cargarAlumnos.cbo_separador.disabled=false;
						document.frm_cargarAlumnos.fil_archivo.disabled=false;
						document.frm_cargarAlumnos.reset();
					}
								
				
				</script>				
				<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
                </style>
				</head>
				<body>
				
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
						  <form method="post" enctype="multipart/form-data" name="frm_cargarAlumnos" id="frm_cargarAlumnos">
				            <table class="tablaGeneral" >
						<tr class="trTitulo">
							<td colspan="5" class="SubTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Carga Masiva</td>
						</tr>
							
                              <tr class="trSubTitulo">
                                <td colspan="5">Carga masiva de Alumnos en <b>
                                  <?= $nombreMateria?>
                                  </b> Grupo <b>
                                  <?= $codigoGrupo?>
                                </b> </td>
                              </tr>
                              <?
								if($_GET['error'] == 0x001)
								{
							?>
                              <tr class="trAviso">
                                <td width="3%"><img src="imagenes/error.gif" width="17" height="18"></td>
                                <td width="97%" colspan="4"><span class="Estilo1">Error: No se pudo  cargar el archivo. Consulte con el Administrador del sistema </span></td>
                              </tr>
                              <?
							  }
							  ?>
                              <tr class="trSubTitulo">
                                <td width="7%" height="21" class="trSubTitulo" colspan="5"><input name="rad_tipoCarga" type="radio" value="0" onClick="javascript: desactivarCargaConeccion()" checked>
      Carga Masiva de Alumnos desde un Archivo Plano</td>
                              </tr>
                              <tr class="trInformacion">
                                <td>&nbsp;</td>
                                <td colspan="4"><b>Metodologia</b></td>
                              </tr>
                              <tr class="trInformacion">
                                <td>&nbsp;</td>
                                <td width="24%">Elija un Separador</td>
                                <td width="19%"><select name="cbo_separador">
                                    <option value="0"></option>
                                    <?
								  $contSeparador=1;
								  
								  if(sizeof($indiceSeparador))
								  {
								  	foreach($indiceSeparador as $separador )
									{
										list($nombreCaracter,$caracter)=explode("=",$separador);
									?>
                                    <option value="<?= $contSeparador?>">
                                    <?= $nombreCaracter?>
                                    </option>
                                    <?
								  	$contSeparador++;
									}
								  
								  }
						  
								  
								  ?>
                                </select></td>
                                <td width="29%">Seleccione el Archivo de Datos </td>
                                <td width="25%"><input name="fil_archivo" type="file" id="fil_archivo"></td>
                              </tr>
                              <tr class="trSubTitulo">
                                <td class="trSubTitulo" colspan="5"><input name="rad_tipoCarga" type="radio" value="1" onClick="javascript: activarCargaConeccion()">
      Carga Masiva de Alumnos desde una Conexi&oacute;n</td>
                              </tr>
                              <tr class="trInformacion">
                                <td>&nbsp;</td>
                                <td colspan="5"><b>Metodologia</b></td>
                              </tr>
                              <tr class="trInformacion">
                                <td>&nbsp;</td>
                                <td>Seleccione una Conexi&oacute;n </td>
                                <td><select name="cbo_coneccion" disabled>
                                    <option value="0"></option>
                                    <?
								  	if(sizeof($listaIndexConeccion))
									{
									
									foreach($listaIndexConeccion as $coneccion)
									{
										list($directorioConeccion,$nombreConeccion)=explode(";",$coneccion);	
								  ?>
                                    <option value="<?= $directorioConeccion?>">
                                    <?= $nombreConeccion?>
                                    </option>
                                    <?
								  	}
								  }
								  ?>
                                </select></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr class="trInformacion">
                                <td>&nbsp;</td>
                                <td colspan="4">&nbsp;</td>
                              </tr>
                              <tr class="trInformacion">
                                <td colspan="5"><div align="center">
                                    <input name="btn_reset" type="reset" id="btn_reset" value="Restablecer" onClick="javascript: restablecer()">
                                    <input name="btn_cargarAlumno" type="button" value="Cargar Alumnos" onClick="javascript: enviarCarga()">
                                    <input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar('<?= $_GET['idGrupo']?>')">
                                    <input name="hid_idGrupo" type="hidden" value="<?= $_GET['idGrupo']?>">
                                    <input name="hid_codigoGrupo" type="hidden" value="<?= $codigoGrupo?>">
                                    <input name="hid_idMateria" type="hidden" value="<?= $idMateria?>">
                                </div></td>
                              </tr>
                            </table>
						  </form>	
                        </td>
                        <td class="tablaEspacio">&nbsp;</td>
                        <td class="tablaDerecho"><?=menu1($_SESSION['idtipousuario']);?></td>
                        <td class="tablaEspacioDerecho">&nbsp;</td>                        
					</tr>
				</table>

            </body>
			</html>
		<? 
		}
		else
		{
			redireccionar("../materias/verMateriasProfesor.phpidmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
		}
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>
