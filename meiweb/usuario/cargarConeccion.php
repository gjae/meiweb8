<?
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ("../usuario/Cargador.class.php");

	if(comprobarSession())
	{	
		$cargador= new Cargador();
		$baseDatos=new BD();
		
		$tipoUsuario=3;
		$estadoUsuario=0;
					
		$nombreArchivo="../../datosMEIWEB/archivosUsuarios/datosUsuarios.usr";
		
		if(isset($_GET['idGrupo']))
		{
			if(file_exists($nombreArchivo))		
			{
				$datosUsuarios=file($nombreArchivo);
				
				if(sizeof($datosUsuarios))
				{
					registrarBitacora(1,35,false);
					
					foreach($datosUsuarios as $dato)
					{
						list($idUsuario,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=explode(";",$dato);
																		
						$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.idusuario=".$idUsuario;
						$consultaUsuario=$baseDatos->ConsultarBD($sql);						
						
						$numUsuario=mysql_num_rows($consultaUsuario);
						
						$listaCodigos[$indice++]=$idUsuario;
						
						if(empty($numUsuario))
						{
			
							$sql="INSERT INTO mei_usuario ( idusuario , idtipousuario , primernombre , segundonombre , primerapellido , segundoapellido , clave , estado ) 
									VALUES ('".$idUsuario."', '".$tipoUsuario."', '".strtoupper($primerNombre)."', '".strtoupper($segundoNombre)."', '".strtoupper($primerApellido)."', '".strtoupper($segundoApellido)."', '".md5(md5($idUsuario))."', '".$estadoUsuario."') ";
									
							$baseDatos->ConsultarBD($sql);
						}							
						
						$sql="SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idgrupo=".$_GET['idGrupo']." AND mei_relusugru.idusuario=".$idUsuario;
						$consultaGrupo=$baseDatos->ConsultarBD($sql);
						
						$numUsuario=mysql_num_rows($consultaGrupo);
						
						if(empty($numUsuario))
						{
							$sql="INSERT INTO mei_relusugru ( idusuario , idgrupo ) 
									VALUES ('".$idUsuario."', '".$_GET['idGrupo']."')";
									
							$baseDatos->ConsultarBD($sql);						
						}


						$sql="SELECT mei_relusumatgru.idusuario FROM mei_relusumatgru WHERE mei_relusumatgru.idgrupo=".$_GET['idGrupo']." AND mei_relusumatgru.idusuario=".$idUsuario." AND mei_relusumatgru.idmateria=".$_GET['idmateria'];
						$consultaGrupo=$baseDatos->ConsultarBD($sql);
						
						$numUsuario=mysql_num_rows($consultaGrupo);
						
						if(empty($numUsuario))
						{
							$sql="INSERT INTO mei_relusumatgru ( idusuario , idgrupo , idmateria) 
									VALUES ('".$idUsuario."', '".$_GET['idGrupo']."','".$_GET['idmateria']."')";
									
							$baseDatos->ConsultarBD($sql);						
						}




					}
				}
				
				$listaEnviado=implode("[+++]",$listaCodigos);
				$listaNoEliminar=implode(",",$listaCodigos);
				
				
				$sql="DELETE FROM mei_relusugru WHERE mei_relusugru.idgrupo =".$_GET['idGrupo']." 
						AND mei_relusugru.idusuario 
							IN (
								SELECT mei_usuario.idusuario FROM mei_usuario
									WHERE mei_usuario.idtipoalumno =1
										AND mei_usuario.idtipousuario =3
											AND mei_usuario.idusuario NOT 
												IN ( ".$listaNoEliminar." ) 
								)";
				$baseDatos->ConsultarBD($sql);	

				$sql="DELETE FROM mei_relusumatgru WHERE mei_relusumatgru.idgrupo =".$_GET['idGrupo']." AND mei_relusumatgru.idmateria=".$_GET['idmateria']." AND mei_relusumatgru.idusuario 
							IN (
								SELECT mei_usuario.idusuario FROM mei_usuario
									WHERE mei_usuario.idtipoalumno =1
										AND mei_usuario.idtipousuario =3
											AND mei_usuario.idusuario NOT 
												IN ( ".$listaNoEliminar." ) 
								)";
				$baseDatos->ConsultarBD($sql);

				redireccionar("../usuario/listarCargaMasiva.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idGrupo=".$_GET['idGrupo']."&listaCodigos=".$baseDatos->CodificarClaveBD($listaEnviado));					
				
			}
			else
			{
				print "No se pudo cargar la coneccion";
			}
		}
		else
		{
			redireccionar("../materias/varMateriaProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>
