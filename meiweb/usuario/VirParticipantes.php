<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	

	$baseDatos=new BD();
	if(comprobarSession())
	{
		mysql_query("SET NAMES 'utf8'");
		 
	if ($_SESSION['idtipousuario']==6)
	{
		if($_POST['cbo_ordenar']==2)
			$activoNombre="selected";
		else if($_POST['cbo_ordenar']==3)
			$activoApellido="selected";
		$cont=0;
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
</head>
<body>
	<script> 
		function enviar()
		{
			document.frm_verUsuario.submit();
		}
		
		function ordenarLista()
		{
			document.frm_verUsuario.submit();
		}

		var ventanaDocumento=false;
		function imprimirDocumento(idUsuario,idGrupo)
		{
			if (typeof ventanaDocumento.document == 'object')
			{
				ventanaDocumento.close()
			}
			ventanaDocumento = window.open('../usuario/imprimirListaPerfil.php?idUsuario='+idUsuario+'&idGrupo='+idGrupo,'ImprimirDocumento','width=700,height=650,left=10,top=10,scrollbars=YES,menubars=NO,status=NO,statusbar=NO,resizable=NO,location=NO');
		}
	</script> 
<table class="tablaPrincipal">
	<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']);?></td>
            <td class="tablaEspacio">&nbsp;</td>
		<td>
			<table class="tablaGeneral" >
			<form name="frm_verUsuario" method="post" action="<?= $PHP_SELF ?>">
				<tr class="trSubTitulo">
					<td>Buscar Participante:</td>
					<td colspan="3"><input name="txt_buscarUsuario" type="text" value="<?= $_POST['txt_buscarUsuario']?>" size="50">
						<input type="submit" name="sub_buscar" value="Buscar"></td>
	      </tr>            
				<tr class="trSubTitulo">
					<td width="25%">Ver Participantes de </td>
					<td width="37%">
						<select name="cbo_verUsuario" onChange="javascript:enviar()">
							<option class="link" value="1">Todos Mis Cursos</option>
							
<?
								$sql="SELECT mei_virgrupo.idvirgrupo,mei_virgrupo.nombre,mei_virmateria.idvirmateria,mei_virmateria.nombre 
									FROM mei_virgrupo, mei_virmateria WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo 
									FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")
									AND mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria ";
								$resultado=$baseDatos->ConsultarBD($sql);
								while (list($codigoGrupo,$grupo,$codigoMateria,$materia)=mysql_fetch_array($resultado))
								{
									$codigoGrupoS=$codigoGrupo+10;
									unset($seleccion); 
									if($codigoGrupoS.','.$codigoMateria==$_POST['cbo_verUsuario'])
									{
										$seleccion="selected";
									}
?>
							<option class="link" value="<?= $codigoGrupoS.','.$codigoMateria?>" <?=$seleccion?>><?=$grupo.'&nbsp;-&nbsp;'.$materia?>
<?
								}
?>
						</select>
					</td>
					<td width="25%"><div align="center">Ordenar Lista Por:</div></td>
					<td width="13%">
						<select name="cbo_ordenar" onChange="javascript:ordenarLista()">
							<option value="2" <?=$activoNombre?>>Nombre</option>
							<option value="3" <?=$activoApellido?>>Apellido</option>
						</select></td>
				</tr>
			</form>
			</table>
			<p>
<?
							if(!empty($_POST['txt_buscarUsuario']))
							{
								$cadena=explode(' ',$_POST['txt_buscarUsuario']);
								foreach($cadena as $palabra)
								{
									if(empty($busqueda))
									{
										$busqueda="mei_usuario.idusuario LIKE '%".$palabra."%'";
										$busqueda.=" OR CONCAT(mei_usuario.primernombre,' ',mei_usuario.segundonombre,' ',mei_usuario.primerapellido,' ',mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
									}
									else
									{
										$busqueda.=" OR mei_usuario.idusuario LIKE '%".$palabra."%'";
										$busqueda.=" OR CONCAT(mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido) LIKE '%".$palabra."%'";
									}
								}
								if($_POST['cbo_ordenar']==3)
								{
									$sql="SELECT mei_virgrupo.nombre,mei_virgrupo.idvirgrupo,mei_usuario.idtipousuario,mei_usuario.idtipoalumno,mei_usuario.idusuario,
										mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.estado,
										mei_usuario.correo,mei_usuario.telefono,mei_usuario.fechanacimiento,mei_usuario.alias 
											FROM mei_virgrupo,mei_usuario,mei_relusuvirgru 
											WHERE mei_virgrupo.idvirgrupo=mei_relusuvirgru.idvirgrupo 
											AND  mei_relusuvirgru.idvirgrupo 
											IN (
													SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
													WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."
												)
												AND mei_relusuvirgru.idusuario=mei_usuario.idusuario 
												AND mei_usuario.idtipousuario=6 
												AND (".$busqueda.") ORDER BY(mei_usuario.primerapellido)";
								}
								else if($_POST['cbo_ordenar']==2)
								{
									$sql="SELECT mei_virgrupo.nombre,mei_virgrupo.idvirgrupo,mei_usuario.idtipousuario,mei_usuario.idtipoalumno,mei_usuario.idusuario,
										mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.estado,
										mei_usuario.correo,mei_usuario.telefono,mei_usuario.fechanacimiento,mei_usuario.alias 
											FROM mei_virgrupo,mei_usuario,mei_relusuvirgru 
											WHERE mei_virgrupo.idvirgrupo=mei_relusuvirgru.idvirgrupo 
											AND  mei_relusuvirgru.idvirgrupo 
											IN (
													SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
													WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."
												)
												AND mei_relusuvirgru.idusuario=mei_usuario.idusuario 
												AND mei_usuario.idtipousuario=6 
												AND (".$busqueda.") ORDER BY(mei_usuario.primernombre)";
								} 			
								$resultado=$baseDatos->ConsultarBD($sql);
								$numRegistros=mysql_num_rows($resultado);
								if($numRegistros>0)
								{
									$color="trListaClaro";
									if($numRegistros == 1)
										$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registro encontrado.";
									else
										$cadenaResultado="Resultado de la busqueda, ".$numRegistros." registros encontrados.";
?>
				<br>
				<br>
			<table class="tablaGeneral" >
				<tr class="trSubTitulo">
					<td colspan="7"><?= $cadenaResultado?></td>
				</tr>
				<tr class="trSubTitulo">
					<td width="5%" class="trSubTitulo"></td>                
					<td width="40%" class="trSubTitulo"><div align="center">Participante</div></td>			
					<td width="10%" class="trSubTitulo"><div align="center">Edad</div></td>					
					<td width="30%" class="trSubTitulo"><div align="center">Correo</div></td>					
					<td width="10%" class="trSubTitulo"><div align="center">Tel&eacute;fono</div></td>					
				</tr>
<?
									while(list($grupo,$codigoGrupo,$codigoTipo,$codigoTipoAlumno,$codigoUsuario,$nombre1,$nombre2,$apellido1,$apellido2,$estado,$correo,$telefono,$fecha,$alias)=mysql_fetch_array($resultado))
									{
										list($aA,$mA,$dA)=explode('-',date("Y-m-d"));
										list($a,$m,$d)=explode('-',$fecha);
										if($a=="0000" || $m=="00" || $d=="00")
										{
											$edad="---";
										}
										else
										{
											if ($mA<$m)
											{
												$edad=$aA-$a-1;
											}
											else if ($mA>$m)
											{
												$edad=$aA-$a;
											}
											else if($mA=$m)
											{
												if($dA<$d)
												{
													 $edad=$aA-$a-1;
												}
												else
												{
													$edad=$aA-$a;
												}
											}
											$edad.=" Años";
										}
										
										if($cont%2==0)
											$color="trListaClaro";
										else
											$color="trListaOscuro";
 ?>
				<tr class="<?=$color?>">
					<td class="<?=$color?>" width="5%"align="right"><?=$cont+1?>
					<td class="<?=$color?>"> &nbsp;<?= $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2 ?></td>			
					<td class="<?=$color?>"><div align="center"><?=$edad?></div></td>
					<td class="<?=$color?>">&nbsp;<a href="redactarCorreo.php?idmateria=<?echo $_GET['idmateria']?>&usuario=<?echo $correo ?>&codigo=<?echo $codigoUsuario?>" class="link"><?=$correo?></a></div></td>
					<td class="<?=$color?>">&nbsp;<?=$telefono?></div></td>                    
				</tr>
<?
									$cont++;
									}
?>
				</table>
				<br>
<?
								}
								else
								{
?>
				<br>
				<table class="tablaGeneral" >
					<tr class="trAviso">
						<td>No se encontraron coincidencias con <i><?=$_POST['txt_buscarUsuario']?></i> </td>
					</tr>
				</table>
                <br>
<?
								}
							}
							if(empty($_POST['cbo_verUsuario']))
							{
								$_POST['cbo_verUsuario']=1;
							}

							$sql="SELECT mei_virmateria.idvirmateria
									FROM mei_virgrupo, mei_virmateria WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo 
									FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")
									AND mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria";
							$resultado=$baseDatos->ConsultarBD($sql);
							list($codigoMateria)=mysql_fetch_array($resultado);
							if(!empty($codigoMateria))
							{
?>
					<form name="frm_usuario" method="post">
					<table class="tablaGeneral">
						<tr class="trTitulo">
							<td colspan="6" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop">Perfil de Participantes</td>
						</tr>
					</table>
<?
								switch ($_POST['cbo_verUsuario'])
								{
									case 1: 
										$sql="SELECT DISTINCT(mei_virmateria.idvirmateria),mei_virmateria.nombre 
											FROM mei_virgrupo, mei_virmateria WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo 
											FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")
											AND mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria ";
									break;
									default:
									list($codigoGrupo,$codigoMateria)=explode(',',$_POST['cbo_verUsuario']);
										$sql="SELECT mei_virmateria.idvirmateria,mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$codigoMateria;
									break;
								}
								$materias=$baseDatos->ConsultarBD($sql);
								while (list($codigoMateria,$materia)=mysql_fetch_array($materias))
								{
?>
					<table class="tablaGeneral" >
						<tr class="trSubTitulo">
							<td colspan="6" class="trSubTitulo"><?=$materia?></td>
						</tr>
<?
									switch ($_POST['cbo_verUsuario'])
									{
										case 1: 
											$sql="SELECT mei_virgrupo.idvirgrupo,mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].") AND mei_virgrupo.idvirmateria=".$codigoMateria;
										break;
										default:
										list($codigoGrupo,$codigoMateria)=explode(',',$_POST['cbo_verUsuario']);
											$sql="SELECT mei_virgrupo.idvirgrupo,mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo=".($codigoGrupo-10);
										break;
									}
									$grupos=$baseDatos->ConsultarBD($sql);
									$contChk=0;
									while (list($codigoGrupo,$grupo)=mysql_fetch_array($grupos))
									{
										if($_POST['cbo_ordenar']==2)
										{
											$sql="SELECT mei_usuario.idusuario,mei_usuario.idtipoalumno,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.estado,mei_usuario.correo,mei_usuario.telefono,mei_usuario.fechanacimiento 
												FROM mei_usuario WHERE mei_usuario.idtipousuario=6 AND mei_usuario.idusuario IN (SELECT mei_relusuvirgru.idusuario 
												FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$codigoGrupo.") AND  (mei_usuario.idtipoalumno=1 OR mei_usuario.idtipoalumno=2)  ORDER BY mei_usuario.primernombre ";
										}
										else if($_POST['cbo_ordenar']==3)
										{
											$sql="SELECT mei_usuario.idusuario,mei_usuario.idtipoalumno,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.estado,mei_usuario.correo,mei_usuario.telefono,mei_usuario.fechanacimiento,mei_usuario.alias  
												FROM mei_usuario WHERE mei_usuario.idtipousuario=6 AND  mei_usuario.idusuario IN (SELECT mei_relusuvirgru.idusuario 
												FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$codigoGrupo.") AND  (mei_usuario.idtipoalumno=1 OR mei_usuario.idtipoalumno=2)  ORDER BY mei_usuario.primerapellido";
										}
										else
										{
											$sql="SELECT mei_usuario.idusuario,mei_usuario.idtipoalumno,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.estado,mei_usuario.correo,mei_usuario.telefono,mei_usuario.fechanacimiento,mei_usuario.alias  
												FROM mei_usuario WHERE mei_usuario.idtipousuario=6 AND  mei_usuario.idusuario IN (SELECT mei_relusuvirgru.idusuario 
												FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$codigoGrupo.") AND  (mei_usuario.idtipoalumno=1 OR mei_usuario.idtipoalumno=2)  ORDER BY mei_usuario.idusuario";
										}
										$resultado=$baseDatos->ConsultarBD($sql);
										$numeroAlumnos=mysql_num_rows($resultado);

?>
						<tr class="trSubTitulo">
							<td colspan="3" ><div align="left">Grupo <?=$grupo?></div></td>
<?
										if(!empty($numeroAlumnos))
										{
?>
							<td colspan="3"><?= $numeroAlumnos ?>&nbsp;&nbsp;Participantes</td>                                    								
<?
										}
										else
										{
?>
							<td colspan="3">&nbsp;</td>                                    
<?
										}
?>
						</tr>
<?
										if(empty($numeroAlumnos))
										{
?>
						<tr class="trAviso">
							<td colspan="6">No se han agregado participantes al Grupo <?=$grupo?></td>
						</tr>
						<tr>
							<td class="trInformacion" colspan="7">&nbsp;</td>
						</tr>
<?
										}
										else
										{
?>
						<tr class="trSubTitulo">
                            <td width="5%" class="trSubTitulo"></td>                
                            <td width="40%" class="trSubTitulo"><div align="center"> Participante</div></td>			
                            <td width="10%" class="trSubTitulo"><div align="center">Edad</div></td>					
                            <td width="30%" class="trSubTitulo"><div align="center">Correo</div></td>					
                            <td width="10%" class="trSubTitulo"><div align="center">Tel&eacute;fono</div></td>					
						</tr>
<? 
											$contador=0;
											while (list($codigoUsuario,$codigoTipoAlumno,$nombre1,$nombre2,$apellido1,$apellido2,$estado,$correo,$telefono,$fecha,$alias)=mysql_fetch_array($resultado))
											{
												list($aA,$mA,$dA)=explode('-',date("Y-m-d"));
												list($a,$m,$d)=explode('-',$fecha);
												if($a=="0000" || $m=="00" || $d=="00")
												{
													$edad="---";
												}
												else
												{
													if ($mA<$m)
													{
														$edad=$aA-$a-1;
													}
													else if ($mA>$m)
													{
														$edad=$aA-$a;
													}
													else if($mA=$m)
													{
														if($dA<$d)
														{
														 $edad=$aA-$a-1;
														}
														else
														{
														$edad=$aA-$a;
														}
													}
													$edad.=" Años";
												}
												if($contador%2==0)
													$color="trListaClaro";
												else
													$color="trListaOscuro";
?>
							<tr class="<?=$color?>">
                                <td class="<?=$color?>" width="5%"align="right"><?=$contador+1?>
                                <td class="<?=$color?>"> &nbsp;<?= $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2 ?></td>			
                                <td class="<?=$color?>"><div align="center"><?=$edad?></div></td>
                                <td class="<?=$color?>">&nbsp;<a href="redactarCorreo.php?idmateria=<?echo $_GET['idmateria']?>&usuario=<?echo $correo ?>&codigo=<?echo $codigoUsuario?>" class="link"><?=$correo?></a></div></td>
                                <td class="<?=$color?>">&nbsp;<?=$telefono?></div></td>                    
							</tr>
<?
												$contador++;
											}//FIN WHILE
										}
									}//FIN WHILE
?>
					</table>
					<br>
<?
								}//FIN WHILE
?>
					<br>
<?
							}
						else
						{
?>
					<table class="tablaGeneral" >
						<tr class="trTitulo" >
							<td><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Ver Participantes</td>
						</tr>
						<tr class="trAviso">
							<td>Actualmente no existen mas alumnos registrados</td>
						</tr>
					</table>
<?
						}
// FIN Usuario Alumno (PUEDE VER ADMON)			
?>
					<input name="hid_contUsuario" type="hidden" value="<?=$contChk?>">					
				</form>
			</td>
			<td class="tablaEspacio">&nbsp;</td>                       
			<td class="tablaDerecho"><?=menu1($_SESSION['idtipousuario']);?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>  
		</tr>
</table>
</body>
</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}

	}
	else
	{
		redireccionar('../login/');					
	}
	
?>