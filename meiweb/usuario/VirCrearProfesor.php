<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	if(comprobarSession())
	{
		if($_SESSION['banderaAdmnistrador']==1)
		{
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
	<script language="javascript">
		function enviarCrear()
		{
			if(comprobarDatos())
			{
				document.frm_crearProfesor.submit();
			}
		}
				
		function enviarCancelar()
		{
			location.replace("VirListarUsuariosAdministrador.php");
		}
				
		function borrarFormulario()
		{
			document.frm_crearProfesor.txt_documentoProfesor.value="";
			document.frm_crearProfesor.txt_primerNombre.value="";
			document.frm_crearProfesor.txt_segundoNombre.value="";
			document.frm_crearProfesor.txt_primerApellido.value="";
			document.frm_crearProfesor.txt_segundoApellido.value="";
		}
	
		function comprobarDatos()
		{
			if(document.frm_crearProfesor.txt_documentoProfesor.value == false || isNaN(parseInt(document.frm_crearProfesor.txt_documentoProfesor.value)))
			{
				alert("Debe ingresar el número de documento");
				document.frm_crearProfesor.txt_documentoProfesor.focus();				
			}
			else if(document.frm_crearProfesor.txt_documentoProfesor.value!= document.frm_crearProfesor.txt_reDocumentoProfesor.value)
			{
				alert("El numero de documento y su confirmacion no son iguales");
				document.frm_crearProfesor.txt_reDocumentoProfesor.focus();				
			}
			else if(document.frm_crearProfesor.txt_primerNombre.value == false || !isNaN(parseInt(document.frm_crearProfesor.txt_primerNombre.value)))
			{
				alert("Debe llenar completamente la información solicitada");
				document.frm_crearProfesor.txt_primerNombre.focus();
			}
			else if(!isNaN(parseInt(document.frm_crearProfesor.txt_segundoNombre.value)))
			{
				alert("Debe llenar completamente la información solicitada");
				document.frm_crearProfesor.txt_segundoNombre.focus();				
			}
			else if(document.frm_crearProfesor.txt_primerApellido.value == false || !isNaN(parseInt(document.frm_crearProfesor.txt_primerApellido.value)))
			{
				alert("Debe llenar completamente la información solicitadao");
				document.frm_crearProfesor.txt_primerApellido.focus();				
			}
			else if(!isNaN(parseInt(document.frm_crearProfesor.txt_segundoApellido.value)))
			{
				alert("Debe llenar completamente la información solicitada");
				document.frm_crearProfesor.txt_segundoApellido.focus();				
			}
			else if(validarCorreo())
			{
				return true;
			}
		}
		
		function validarCorreo()
		{
			if(document.frm_crearProfesor.txt_correo.value=="")
			{
				alert("Debe ingregar una dirección de correo electronico");
				document.frm_crearProfesor.txt_correo.focus();
				return false;
			}
			else if(document.frm_crearProfesor.txt_correo.value.search(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig))
			{
				alert("El correo electronico no es válido, escribalo en forma correcta");
				document.frm_crearProfesor.txt_correo.focus();
				return false;
			}
			else
			{
				return true;
			}
		}
		
	</script>
	<style type="text/css">
		<!--
			.Estilo1 {color: #FF0000}
		-->
	</style>
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
	        <td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><? menu(1);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td>
				<table class="tablaGeneral">
					<form action="VirInsertarProfesor.php" method="post" name="frm_crearProfesor">
					<tr>
						<td colspan="3" class="trTitulo"><img src="imagenes/usuario.gif" width="17" height="17">&nbsp;Crear Profesor Educaci&oacute;n Virtual</td>
					  </tr>
<?
		if($_GET['error'] == '0x001')
		{
?>
					<tr class="trAviso">
						<td width="3%"><img src="imagenes/error.gif" width="17" height="18"></td>
						<td colspan="2"><span class="Estilo1">Error: Ya existe un profesor registrado con ese documento </span></td>
					</tr>
<?
		}
?>
					<tr class="trInformacion">
						<td colspan="2">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr class="trInformacion">
					  <td colspan="2"><strong>&nbsp;Tipo Documento:</strong></td>
					  <td><label>
					    <select name="cbo_tipoDoc" id="cbo_tipoDoc">
					      <option value="CC">C&eacute;dula de Ciudadania</option>
					      <option value="CE">Tarjeta de Identidad</option>
					      <option value="P">C&eacute;dula de Extranjer&iacute;a</option>
					      <option value="RC">Registro Civil</option>
				        </select>
					  </label></td>
					  </tr>
					<tr class="trInformacion">
					  <td colspan="2"><strong>&nbsp;Número de Documento:</strong></td>
					  <td width="75%"><input name="txt_documentoProfesor" type="text" id="txt_documentoProfesor" value="<?= $_GET['documentoProfesor']?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Primer Nombre : </strong></td>
						<td><input name="txt_primerNombre" type="text" id="txt_primerNombre" value="<?= $_GET['primerNombre']?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Segundo Nombre : </strong></td>
						<td><input name="txt_segundoNombre" type="text" id="txt_segundoNombre" value="<?= $_GET['segundoNombre']?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Primer Apellido : </strong></td>
						<td><input name="txt_primerApellido" type="text" id="txt_primerApellido" value="<?= $_GET['primerApellido']?>" size="40"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Segundo Apellido :</strong></td>
						<td><input name="txt_segundoApellido" type="text" id="txt_segundoApellido" value="<?= $_GET['segundoApellido']?>" size="40">
					    <input name="hid_idGrupo" type="hidden" id="hid_idGrupo" value="<?= $_GET['idGrupo']?>"></td>
					</tr>
					<tr class="trInformacion">
					  <td colspan="2"><strong>&nbsp;Correo Electr&oacute;nico:</strong></td>
					  <td><input name="txt_correo" type="text" id="txt_correo" value="<?= $_GET['correo']?>" size="40"></td>
					  </tr>
					<tr class="trInformacion">
					  <td colspan="2"><strong>&nbsp;Confirmar Documento:</strong></td>
					  <td><input name="txt_reDocumentoProfesor" type="text" id="txt_reDocumentoProfesor" value="<?= $_GET['documentoProfesor']?>" size="40"></td>
					  </tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;<input type="checkbox" name="chk_administrador" value="1" disabled>Activar este usuario como Administrador</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center">
							<input name="btn_reset" type="button" value="Restablecer" onClick="javascript: borrarFormulario()">
							<input name="btn_crearProfesor" type="button" id="btn_crearProfesor" value="Crear Profesor" onClick="javascript: enviarCrear();">
							<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div>
						</td>
					</tr>
					</form>
				</table>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho">&nbsp;</td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<? 
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>