<?
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	if(comprobarSession())
	{
		if ($_SESSION['banderaAdmnistrador']==1)
		{
			if(!empty($_GET['idGrupo']))
			{
				$sql="SELECT mei_virmateria.nombre, mei_virgrupo.nombre FROM mei_virmateria, mei_virgrupo 
						WHERE mei_virgrupo.idvirmateria = mei_virmateria.idvirmateria AND mei_virgrupo.idvirgrupo=".$_GET['idGrupo'];
				$consultaGrupo=$baseDatos->ConsultarBD($sql);
				list($nombreMateria,$codigoGrupo)=mysql_fetch_array($consultaGrupo);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				

<script language="javascript">
	function enviarCrear(opcionCrear)
	{
		if(comprobarDatos())
		{
			document.frm_crearAlumno.action="VirInsertarAlumno.php?opcionCrear="+opcionCrear;
			document.frm_crearAlumno.submit();
		}
	}
					
	function enviarCancelar()
	{
		location.replace("../materias/VirVerMateriasAdministrador.php");
	}
									
	function cargarAlumnos(idGrupo) //carga masiva
	{
		document.frm_crearAlumno.btn_crearAlumno.disabled=true;
		location.replace("VirCargaMasiva.php?idGrupo="+idGrupo);
	}
				
	function comprobarDatos()
	{
		if(document.frm_crearAlumno.txt_documentoAlumno.value == false || isNaN(parseInt(document.frm_crearAlumno.txt_documentoAlumno.value)))
		{
			alert("Debe ingresar el numero de documento");
			document.frm_crearAlumno.txt_documentoAlumno.focus();
		}
		else if(document.frm_crearAlumno.txt_primerNombre.value == false || !isNaN(parseInt(document.frm_crearAlumno.txt_primerNombre.value)))
		{
			alert("Debe llenar completamente la información solicitada");
			document.frm_crearAlumno.txt_primerNombre.focus();			
		}
		else if(!isNaN(parseInt(document.frm_crearAlumno.txt_segundoNombre.value)))
		{
			alert("Debe llenar completamente la información solicitada");
			document.frm_crearAlumno.txt_segundoombre.focus();
		}
		else if(document.frm_crearAlumno.txt_primerApellido.value == false || !isNaN(parseInt(document.frm_crearAlumno.txt_primerApellido.value)))
		{
			alert("Debe llenar completamente la información solicitada");
			document.frm_crearAlumno.txt_primerApellido.focus();
		}
		else if(!isNaN(parseInt(document.frm_crearAlumno.txt_segundoApellido.value)))
		{
			alert("Debe llenar completamente la información solicitada");
			document.frm_crearAlumno.txt_segundoApellido.focus();
		}
		else if(document.frm_crearAlumno.txt_documentoAlumno.value!= document.frm_crearAlumno.txt_reDocumentoAlumno.value)
		{
			alert("El numero de documento y su confirmacion no son iguales");
			document.frm_crearAlumno.txt_reDocumentoAlumno.focus();				
		}
		else if (validarCorreo())
		{
			return true;
		}
	}
	
	function validarCorreo()
	{
		if(document.frm_crearAlumno.txt_correo.value=="")
		{
			alert("Debe ingregar una dirección de correo electronico");
			document.frm_crearAlumno.txt_correo.focus();
			return false;
		}
		else if(document.frm_crearAlumno.txt_correo.value.search(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig))
		{
			alert("El correo electronico no es válido, escribalo en forma correcta");
			document.frm_crearAlumno.txt_correo.focus();
			return false;
		}
		else
		{
			return true;
		}
	}
								
</script>
				
<style type="text/css">
	<!--
		.Estilo1 {color: #FF0000}
	-->
</style>
	
</head>
<body>
<table class="tablaPrincipal">
	<tr valign="top">
    	<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaIzquierdo"><?=menu(1);?></td>
        <td class="tablaEspacio">&nbsp;</td>
		<td><form method="post" name="frm_crearAlumno">
			<table class="tablaGeneral" >
				<tr class="trTitulo">
					<td colspan="3" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop">&nbsp;Crear Alumno </b>Educaci&oacute;n Virtual</td>
				</tr>
                <tr class="trInformacion">
					<td colspan="3">&nbsp;</td>
				</tr>
                <tr class="trSubTitulo">
					<td colspan="3" class="trSubTitulo">
						&nbsp;Inserte la informaci&oacute;n del nuevo Alumno en&nbsp;<?= $nombreMateria?>&nbsp;grupo&nbsp;<?= $codigoGrupo?></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="3" >&nbsp;</td>
				</tr>
<?
				$valorBtnCrear="Crear Alumno";
				$opcionCrear=0;
				if($_GET['error'] == "0x001" && !empty($_GET['documentoAlumno']))
				{
					$valorBtnCrear="Agregar Alumno";
					$opcionCrear=1;
					$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.correo FROM mei_usuario WHERE mei_usuario.documento='".$_GET['documentoAlumno']."'";
					$consultaAlumno=$baseDatos->ConsultarBD($sql);
						
					list($primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$correo)=mysql_fetch_array($consultaAlumno);
?>
				<tr class="trAviso">
					<td colspan="1" width="2%"><img src="imagenes/existente.gif" width="16" height="15"></td>
					<td colspan="2" >El alumno con documento N° <b><?= $_GET['documentoAlumno']?></b> ya se encuentra registrado en <b>MeiWeb</b>. Para agregar el alumno al grupo <b><?= $codigoGrupo?></b> de click en <b>Agregar Alumno</b>.</td>
				</tr>
<?
				}
				else if($_GET['error'] == "0x002") //el alumno ya esta registrado en el grupo 
				{
					$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.correo FROM mei_usuario WHERE mei_usuario.documento='".$_GET['documentoAlumno']."'";
					$consultaAlumno=$baseDatos->ConsultarBD($sql);
					list($primerNombre,$segundoNombre,$primerApellido,$segundoApellido,$correo)=mysql_fetch_array($consultaAlumno);
?>
				<tr class="trAviso">
					<td colspan="1" width="2%"><img src="imagenes/error.gif" width="17" height="18"></td>
					<td colspan="2" class="Estilo1">Error: No se ha podido crear el alumno con documento N° <b><?= $_GET['documentoAlumno']?></b>. Este alumno ya se encuentra registrado en el grupo <b><?= $_GET['nombreGrupo']?></b> de <b><?=$_GET['materia']?></b>.</td>
				</tr>
<?
				}
?>
				<tr class="trInformacion">
					<td colspan="2"><strong>&nbsp;Tipo Documento:</strong></td>
					<td><label><select name="cbo_tipoDoc" id="cbo_tipoDoc">
							<option value="CC">C&eacute;dula de Ciudadania</option>
							<option value="CE">Tarjeta de Identidad</option>
							<option value="P">C&eacute;dula de Extranjer&iacute;a</option>
							<option value="RC">Registro Civil</option>
						</select></label></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="2"><b>&nbsp;Numero de Documento:</b></td>
					<td width="77%"><input name="txt_documentoAlumno" type="text" id="txt_documentoAlumno" value="<?= $_GET['documentoAlumno']?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="2"><b>&nbsp;Primer Nombre : </b></td>
					<td><input name="txt_primerNombre" type="text" id="txt_primerNombre" value="<?= $primerNombre?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="2"><b>&nbsp;Segundo Nombre : </b></td>
					<td><input name="txt_segundoNombre" type="text" id="txt_segundoNombre" value="<?= $segundoNombre?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="2"><b>&nbsp;Primer Apellido : </b></td>
					<td><input name="txt_primerApellido" type="text" id="txt_primerApellido" value="<?= $primerApellido?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="2"><b>&nbsp;Segundo Apellido :</b></td>
					<td><input name="txt_segundoApellido" type="text" id="txt_segundoApellido" value="<?= $segundoApellido?>" size="40">
						<input name="hid_idGrupo" type="hidden" id="hid_idGrupo" value="<?= $_GET['idGrupo']?>"></td>
				</tr>
                <tr class="trInformacion">
					<td colspan="2"><b>&nbsp;Correo Electr&oacute;nico:</b></td>
				  <td><input name="txt_correo" type="text" id="txt_correo" value="<?= $correo?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="2"><strong>&nbsp;Confirmar Documento:</strong></td>
					<td><input name="txt_reDocumentoAlumno" type="text" id="txt_reDocumentoAlumno" value="<?= $_GET['documentoAlumno']?>" size="40"></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr class="trInformacion">
					<td colspan="3"><div align="center">	
						<input name="btn_cargaMasiva" type="button" id="btn_cargaMasiva" value="Carga Masiva" onClick="javascript: cargarAlumnos('<?= $_GET['idGrupo']?>')">
						<input name="btn_reset" type="reset" value="Restablecer">
						<input name="btn_crearAlumno" type="button" id="btn_crearAlumno" value="<?= $valorBtnCrear?>" onClick="javascript: enviarCrear('<?= $opcionCrear?>');">
						<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div>
					</td>
				</tr>
				<tr class="trInformacion">
					<td colspan="3">&nbsp;</td>
				</tr>
			</table>
			</form>
		</td>
        <td class="tablaEspacio">&nbsp;</td>
        <td class="tablaDerecho">&nbsp;</td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
	</tr>
</table>
</body>
</html>
<? 
			}
			else
			{
				redireccionar('../materias/verMateriasProfesor.php');
			}
		}
		else
		{
			redireccionar('../login/');
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>