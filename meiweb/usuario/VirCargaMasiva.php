<?
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	if(comprobarSession())
	{
		if ($_SESSION['banderaAdmnistrador']==1) 
		{
			if(!empty($_GET['idGrupo']))
			{
				$cont=0;
				$archivoSeparador="../configuracion/archivos/indiceSeparador.dat";
				if(file_exists($archivoSeparador))
				{
					$indiceSeparador=file($archivoSeparador);
				}
				$sql="SELECT mei_virmateria.idvirmateria, mei_virmateria.nombre, mei_virgrupo.nombre FROM mei_virmateria, mei_virgrupo
					WHERE mei_virgrupo.idvirmateria = mei_virmateria.idvirmateria AND mei_virgrupo.idvirgrupo=".$_GET['idGrupo'];
				$consultaGrupo=$baseDatos->ConsultarBD($sql);
				list($idMateria,$nombreMateria,$nombreGrupo)=mysql_fetch_array($consultaGrupo);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
	
<script language="javascript">
	function enviarCarga()
	{
	//	if(comprobarDatos())
			document.frm_cargarAlumnos.action="VirCargarPreinscritos.php";
			document.frm_cargarAlumnos.submit();
	}

	function comprobarDatos()
	{
		if(document.frm_cargarAlumnos.cbo_separador.disabled==false)
		{
			if(parseInt(document.frm_cargarAlumnos.cbo_separador.value) == 0)
			{
				alert("Debe seleccionar un separador");
			}
			else if(document.frm_cargarAlumnos.fil_archivo.value == false)
			{
				alert("Debe seleccionar un archivo");
			}
			else
			{
				document.frm_cargarAlumnos.action="cargarArchivo.php";
				return true;
			}
		}
		else if (document.frm_cargarAlumnos.cbo_separador.disabled==true)
		{
			document.frm_cargarAlumnos.action="VirCargarPreinscritos.php";
			return true;
		}
	}

	function enviarCancelar(idGrupo)
	{
		location.replace("VirCrearAlumno.php?idGrupo="+idGrupo);
	}

	function activarCargaConeccion()
	{
		document.frm_cargarAlumnos.cbo_separador.disabled=true;
		document.frm_cargarAlumnos.fil_archivo.disabled=true;
	}

	function desactivarCargaConeccion()
	{
		document.frm_cargarAlumnos.cbo_separador.disabled=false;
		document.frm_cargarAlumnos.fil_archivo.disabled=false;
	}
</script>
<style type="text/css">
	<!--
	.Estilo1 {color: #FF0000}
	-->
</style>
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
	        <td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><?=menu(1);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td><form method="post" enctype="multipart/form-data" name="frm_cargarAlumnos" id="frm_cargarAlumnos">            
				<table class="tablaGeneral" >
					<tr class="trTitulo">
						<td width="4%" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Carga Masiva</td>
					</tr>
					<tr class="trInformacion">
						<td class="trInformacion">&nbsp;</td>
					</tr>
					<tr class="trSubTitulo">
						<td class="trSubTitulo">&nbsp;Carga masiva de Alumnos al Grupo&nbsp;<b>
							<?= $nombreGrupo?></b>&nbsp;del Curso&nbsp;<b><?= $nombreMateria?></b></td>
					</tr>
<?
				if($_GET['error']==0x001)
				{
?>

					<tr class="trAviso">
						<td colspan="2">&nbsp;<img src="imagenes/error.gif" width="17" height="18"><span class="Estilo1">&nbsp;&nbsp;Error: No se pudo  cargar el archivo.</span></td>
					</tr>
<?
				}
?>
					<tr class="trInformacion">
						<td class="trInformacion">&nbsp;</td>
					</tr>
<!--					<tr class="trSubTitulo">
				    	<td class="trSubTitulo">&nbsp;
                      <input name="rad_tipoCarga" type="radio" value="1" onClick="javascript: desactivarCargaConeccion()" checked>
Cargar desde un Archivo Plano</td>
					</tr>
					<tr class="trInformacion">
						<td class="trInformacion">&nbsp;</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Elija un Separador&nbsp;
							<select name="cbo_separador">
                           		<option value="0"></option>
<?
				$contSeparador=1;
				if(sizeof($indiceSeparador))
				{
					foreach($indiceSeparador as $separador)
					{
						list($nombreCaracter,$caracter)=explode("=",$separador);
?>
                            <option value="<?= $contSeparador?>"><?= $nombreCaracter?></option>
<?
						$contSeparador++;
					}
				}
?>
							</select></td>
					</tr>                    
					<tr class="trInformacion">
						<td class="trInformacion">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seleccione el Archivo de Datos &nbsp;
                            <input name="fil_archivo" type="file">
						</td>
					</tr>
					<tr class="trInformacion">
						<td class="trInformacion">&nbsp;</td>
					</tr>
-->					<tr class="trSubTitulo">
						<td class="trSubTitulo">&nbsp;
                            <input name="rad_tipoCarga" type="radio" value="0" onClick="javascript: activarCargaConeccion()">
Cargar Preinscritos</td>
					</tr>
					<tr class="trInformacion">
					  <td class="trInformacion">&nbsp;</td>
				  	</tr>
<!--					<tr class="trInformacion">
						<td>&nbsp;</b></td>
					</tr>
-->					<tr class="trInformacion">
						<td class="trInformacion"><div align="center">
							<input name="btn_reset" type="reset" id="btn_reset" value="Restablecer" >
							<input name="btn_cargarAlumno" type="button" value="Cargar Alumnos" onClick="javascript: enviarCarga()">
							<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar('<?= $_GET['idGrupo']?>')">
							<input name="hid_idGrupo" type="hidden" value="<?= $_GET['idGrupo']?>">
							<input name="hid_codigoGrupo" type="hidden" value="<?= $nombreGrupo?>">
							<input name="hid_idMateria" type="hidden" value="<?= $idMateria?>"></div></td>
					</tr>
				</table>
  		  </form></td>
          <td class="tablaEspacio">&nbsp;</td>
          <td class="tablaDerecho">&nbsp;</td>
          <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<? 
			}
			else
			{
				redireccionar('../usuario/VirVerMateriasAdministrador.php');
			}
		}
		else
		{
			redireccionar('../login/');
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>