<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	
	$baseDatos=new BD();
	if(comprobarSession())
	{	
		$tipoAlumno=6;
		if(isset($_POST['txt_documentoAlumno']) && isset($_POST['txt_primerNombre']) && isset($_POST['txt_segundoNombre']) && isset($_POST['txt_primerApellido']) && isset($_POST['txt_segundoApellido']))
		{	
			if($_GET['opcionCrear']==1)
			{
				$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.documento='".$_POST['txt_documentoAlumno']."'";
				$consulta=$baseDatos->ConsultarBD($sql);
				list($codigo)=mysql_fetch_array($consulta);						
				registrarBitacora(1,34,$codigo);
				$sql="INSERT INTO mei_relusuvirgru (idusuario,idvirgrupo) VALUES ('".$codigo."', '".$_POST['hid_idGrupo']."') ";
				$baseDatos->ConsultarBD($sql);
				redireccionar("../materias/VirVerMateriasAdministrador.php");
			}
			else
			{
				$sql="SELECT count(mei_usuario.documento) FROM mei_usuario WHERE mei_usuario.documento='".$_POST['txt_documentoAlumno']."'";
				$consulta=$baseDatos->ConsultarBD($sql);
				list($numeroAlumno)=mysql_fetch_array($consulta);
				if(empty($numeroAlumno))
				{	
					$sql="SELECT MAX( mei_usuario.idusuario) FROM mei_usuario";
					$resultado=$baseDatos->ConsultarBD($sql);
					list($maxCodigo)=mysql_fetch_array($resultado);
					if ($maxCodigo<10000000)
						$codigoAlumno=10000000;
					else
						$codigoAlumno=$maxCodigo+1;				
				
					registrarBitacora(1,34,$codigo);
					$sql="INSERT INTO mei_usuario (tipodocumento,documento,idusuario,idtipousuario,primernombre,segundonombre,primerapellido,segundoapellido,clave,login,correo) 
							VALUES ('".$_POST['cbo_tipoDoc']."','".$_POST['txt_documentoAlumno']."','".$codigoAlumno."','".$tipoAlumno."','".strtoupper($_POST['txt_primerNombre'])."','".strtoupper($_POST['txt_segundoNombre'])."','".strtoupper($_POST['txt_primerApellido'])."','".strtoupper($_POST['txt_segundoApellido'])."','".md5(md5($_POST['txt_documentoAlumno']))."','".$_POST['txt_documentoAlumno']."','".$_POST['txt_correo']."')";
							
					$baseDatos->ConsultarBD($sql);
					if($_POST['hid_idGrupo'])
					{
						$destino="../materias/VirVerMateriasAdministrador.php";
						$sql="INSERT INTO mei_relusuvirgru (idusuario,idvirgrupo) 
							VALUES ('".$codigoAlumno."','".$_POST['hid_idGrupo']."') ";
						$baseDatos->ConsultarBD($sql);				
					}
					else
					{
						$destino="../materias/VirVerMateriasAdministrador.php";
					}
					redireccionar($destino);
				}
				else
				{
					$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.documento='".$_POST['txt_documentoAlumno']."'";
					$consulta=$baseDatos->ConsultarBD($sql);
					list($codigo)=mysql_fetch_array($consulta);											
					$sql="SELECT mei_virmateria.nombre,mei_virmateria.idvirmateria FROM mei_virmateria 
							WHERE mei_virmateria.idvirmateria=(SELECT mei_virgrupo.idvirmateria 
							FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo='".$_POST['hid_idGrupo']."')";
					$consultaMateria=$baseDatos->ConsultarBD($sql);
					list($materia,$codigoMateria)=mysql_fetch_array($consultaMateria);
					
					$sql="SELECT DISTINCT mei_relusuvirgru.idusuario,mei_virgrupo.idvirgrupo,mei_virgrupo.nombre FROM mei_relusuvirgru,mei_virgrupo 
							WHERE mei_relusuvirgru.idvirgrupo=mei_virgrupo.idvirgrupo AND mei_relusuvirgru.idusuario='".$codigo."' 
							AND mei_relusuvirgru.idvirgrupo IN(SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo 
							WHERE mei_virgrupo.idvirmateria='".$codigoMateria."')";
					$consultaGrupo=$baseDatos->ConsultarBD($sql);
					$numRelacion=mysql_num_rows($consultaGrupo);
					list($usuario,$codigoGrupo,$nombreGrupo)=mysql_fetch_array($consultaGrupo);
					if(empty($numRelacion))	
					{
						$error="0x001";
						redireccionar("../usuario/VirCrearAlumno.php?error=".$error."&documentoAlumno=".$_POST['txt_documentoAlumno']."&idGrupo=".$_POST['hid_idGrupo']."&materia=".$materia);
					}
					else
					{
						$error="0x002";
						redireccionar("../usuario/VirCrearAlumno.php?error=".$error."&documentoAlumno=".$_POST['txt_documentoAlumno']."&nombreGrupo=".$nombreGrupo."&idGrupo=".$_POST['hid_idGrupo']."&materia=".$materia);
					}
				}
			}//
		}
		else
		{
			redireccionar('../login/');
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>