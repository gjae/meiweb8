<? 
	include_once ('../librerias/validar.lib.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../baseDatos/BD.class.php');  
	
	$baseDatos=new BD();
	
	if(comprobarSession())
	{ 
	  	if (!empty($_POST['txt_telefono']) && !empty($_POST['txt_correo']) )
		{
			$correomod = mysql_real_escape_string($_POST['txt_correo']);
			$telefonomod = mysql_real_escape_string($_POST['txt_telefono']);
			registrarBitacora(7,29,false);			
			if (!empty($_POST['chk_clave']))
			{
				$clave=md5(md5($_POST['txt_nuevaClave']));
				$sql="UPDATE mei_usuario SET clave= '".$clave."' WHERE mei_usuario.idusuario=".$_SESSION['idusuario'];
				$resultado=$baseDatos->ConsultarBD($sql);
				$destino=1;
			}			
			if (!empty($_POST['chk_NomUsu']))
			{
				$nombremod = mysql_real_escape_string($_POST['txt_nuevaNomUsu']);	
				$sql="UPDATE mei_usuario SET login= '".$nombremod."' WHERE mei_usuario.idusuario=".$_SESSION['idusuario'];
				$resultado=$baseDatos->ConsultarBD($sql);
				$destino=1;
			}
			if (!empty($_POST['chk_imagen']))
			{
				$sql="SELECT imagen FROM mei_usuario WHERE idusuario=".$_SESSION['idusuario'];
				$resultado=$baseDatos->ConsultarBD($sql);
				list($imagen)=mysql_fetch_array($resultado);
				
				if (file_exists("../../datosMEIWEB/fotosUsuarios/".$imagen))
				{
					unlink("../../datosMEIWEB/fotosUsuarios/".$imagen);
				}	
				
				$Id= $_SESSION['idusuario'];
				$archivo= $_FILES['fil_archivo']['name']; 
				$nombreArchivo="id".$Id.$archivo;
				if (cargarArchivo("fil_archivo",corregirCaracteresURL($nombreArchivo),"../../datosMEIWEB/fotosUsuarios/"))
				{
					$sql="UPDATE mei_usuario SET imagen='".corregirCaracteresURL($nombreArchivo)."' WHERE mei_usuario.idusuario=".$_SESSION['idusuario'];
					$resultado=$baseDatos->ConsultarBD($sql);
				}
			}
			$fecha=$_POST['cbo_agno']."-".$_POST['cbo_mes']."-".$_POST['cbo_dia'];
			$sql="UPDATE mei_usuario SET correo='".$correomod."',telefono='".$telefonomod."',
				fechanacimiento='".$fecha."', perfil='".$_POST['edt_perfil']."',pregunta='".$_POST['txt_pregunta']."',respuesta='".base64_encode($_POST['txt_respuesta'])."' WHERE mei_usuario.idusuario=".$_SESSION['idusuario'];
			$resultado=$baseDatos->ConsultarBD($sql);
		
			if(empty($destino))
			{
				redireccionar ('../scripts/');
			}
			else
			{
				redireccionar ('../login/');
			}
		}
		else
		{ 
			redireccionar('../login/');					
		}
	}
	else
	{ 
		redireccionar('../login/');					
	}
?>