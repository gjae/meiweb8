<?
	
	include_once("../usuario/Cargador.class.php");
	include_once ('../librerias/estandar.lib.php');
	include_once("../baseDatos/BD.class.php");
	
	$baseDatos= new BD();
	
	if(comprobarSession())
	{	
		if(isset($_GET['idGrupo']))
		{
						
			$cargador= new Cargador();
			$cargador->SeparadorListaDatos($_GET['idSeparador']);
			$cargador->ConstruirSQL($_GET['idmateria']);
			$cargador->InsertarSQL();
		
			registrarBitacora(1,35,false);
			$listaCodigos=implode("[+++]",$cargador->m_listaCodigosCorrectos);
			
			$listaNoEliminar=implode(",",$cargador->m_listaCodigosCorrectos);
			
			$sql="DELETE FROM mei_relusugru WHERE mei_relusugru.idgrupo =".$_GET['idGrupo']." 
						AND mei_relusugru.idusuario 
							IN (
								SELECT mei_usuario.idusuario FROM mei_usuario
									WHERE mei_usuario.idtipoalumno =1
										AND mei_usuario.idtipousuario =3
											AND mei_usuario.idusuario NOT 
												IN ( ".$listaNoEliminar." ) 
								)";
			$baseDatos->ConsultarBD($sql);

			$sql1="DELETE FROM mei_relusumatgru WHERE mei_relusumatgru.idgrupo =".$_GET['idGrupo']."
						AND mei_relusumatgru.idmateria = ".$_GET['idmateria']." 
						AND mei_relusumatgru.idusuario 
							IN (
								SELECT mei_usuario.idusuario FROM mei_usuario
									WHERE mei_usuario.idtipoalumno =1
										AND mei_usuario.idtipousuario =3
											AND mei_usuario.idusuario NOT 
												IN ( ".$listaNoEliminar." ) 
								)";
			$baseDatos->ConsultarBD($sql1);
					
			redireccionar("../usuario/listarCargaMasiva.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&destino=1&idGrupo=".$_GET['idGrupo']."&listaCodigos=".$baseDatos->CodificarClaveBD($listaCodigos));	
			
		}
		else
		{
			redireccionar("../materias/verMateriasProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);		
		}
	}
	else
	{
		redireccionar("../login/");
	}
?>