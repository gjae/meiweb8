<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once('evaclass.php');
	
	if(comprobarSession())
	{	
		$evaluacion = $_SESSION["evaluacion"]; 		
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
function enviar()
{
	
	//alert(document.frm.action);
	document.frm.submit();
    window.close();
	
	
}
</script>
<title>MEIWEB</title>
</head>
<body onLoad="window.focus()">
<form name="frm" method="get"  target="verEvaluacion" action="verIntentoEvaluacion.php?idpregunta=<?=$_GET['idpregunta']?>&esp=<?=$_GET['esp']?>&idmateria=<?=$_GET["idmateria"]?>&cbo_intento=<?=$_GET['cbo_intento']?>&intentos=<?=$_GET['intentos']?>&idprevio=<?=$_GET['idprevio']?>&idusuario=<?=$_GET['idusuario']?>&idgrupo=<?=$_GET['idgrupo']?>" >
 <input type="hidden" name="idpregunta" value="<?=$_GET['idpregunta']?>" />
  <input type="hidden" name="esp" value="<?=$_GET['esp']?>" />
   <input type="hidden" name="idmateria" value="<?=$_GET["idmateria"]?>" />
    <input type="hidden" name="cbo_intento" value="<?=$_GET['cbo_intento']?>" />
     <input type="hidden" name="idprevio" value="<?=$_GET['idprevio']?>" />
      <input type="hidden" name="idusuario" value="<?=$_GET['idusuario']?>" />
       <input type="hidden" name="idgrupo" value="<?=$_GET['idgrupo']?>" />
		<input type="hidden" name="intentos" value="<?=$_GET['intentos']?>" />
  <table width="220" border="0" cellpadding="0" cellspacing="0">
    <tr class="trTitulo">
      <td colspan="2" align="left" valign="middle">Calificar Pregunta </td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Nota:</strong></td>
      <td align="left" valign="middle">
      	<select  name="nota_valor"  >
      		<?
		      		for ($i=0.0;$i<=5.0;$i=$i+0.1)
										{
											$x=$i;
											print "<option value='$x'>";
											print $x."</option>";
										}
		      	?>
		      	
      	<select />
        </td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td colspan="2" align="left" valign="middle"><label>
       </td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trSubTitulo" align="center" valign="middle">
      <td colspan="2"><input type="button" name="Submit" value="Calificar" onClick="javascript:enviar()"></td>
    </tr>
  </table>
  
</form>

</body>
</html>
<?
}
else redireccionar('../login/');
?>