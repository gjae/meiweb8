<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ("../editor/fckeditor.php") ;
	include_once ('../calendario/FrmCalendario.class.php');
	include_once ('evaclass.php');

	if(comprobarSession())
	{

		$baseDatos=new BD();
		list ($idmateria, $materia) = explode("*", $_POST["hid_materia"]);

		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{
			if (empty($_POST["cbo_valor"])) $valor = 0;
			else $valor = $_POST["cbo_valor"];
		//*************************ARREGLO DE LA FECHA***********************************
			list($agno_a, $mes_a, $dia_a) = explode("-",$_POST['txt_fecha_a']);
			if ($mes_a<10) $mes_a = "0".$mes_a;
			if ($dia_a<10) $dia_a = "0".$dia_a;

			list($agno_f, $mes_f, $dia_f) = explode("-",$_POST['txt_fecha_f']);
			if ($mes_f<10) $mes_f = "0".$mes_f;
			if ($dia_f<10) $dia_f = "0".$dia_f;
		//**********************************************************************************

			$fecha_a = $agno_a.$mes_a.$dia_a.$_POST["cbo_hora_a"].$_POST["cbo_minuto_a"];
			$fecha_f = $agno_f.$mes_f.$dia_f.$_POST["cbo_hora_f"].$_POST["cbo_minuto_f"];

		/*print $fecha_a."<p>";
		print $fecha_f."<p>";*/



		//**********************************************************************************

		/*if ( $_GET["modo"] == 'E' )
		{
			$sql = "UPDATE mei_evaprevio SET
					idevaluacion = '".$_POST['cbo_nota']."',
					titulo = '".$_POST['txt_titulo']."',
					fechaactivacion = '".$fecha_a."',
					fechafinalizacion = '".$fecha_f."',
					comentario = '".base64_encode($_POST['edt_comentario'])."',
					direccionIp1 = '".$_POST['direccionIp1']."',
					direccionIp2 = '".$_POST['direccionIp2']."',
					tiempo = '".round($_POST['txt_tiempo'])."',
					barajarpreg = '".$_POST['cbo_bpreguntas']."',
					barajarresp = '".$_POST['cbo_brespuestas']."',
					intentos = '".$_POST['cbo_intentos']."',
					valorIntento = '".round(($_POST["cbo_valorIntento"]/100),2)."',
					idmodCalificar = '".$_POST["cbo_calificar"]."',
					mostrarnota = '".$_POST["cbo_mnota"]."',
					estado = '".$_POST['cbo_estado']."',
					valor = '".$valor."',
					idtiposubgrupo = ".$_POST["cbo_subgrupo"]."
					WHERE mei_evaprevio.idprevio = '".$_POST['hid_previo']."'";
			$baseDatos->ConsultarBD($sql);
			$idprevio = $_POST['hid_previo'];
			redireccionar("llenarEvaluacion.php");
		}*/
		$incentivo=0;
		$curva=0;
		$curva=$_POST['incentivo_curva'];
		
		if($curva!="curva"){
			$incentivo=$_POST['incentivo_valor'];
			$curva=0;
		}else{
			$curva=1;
		}
		
			if ( $_GET["modo"] == 'E' )
			{ 
				if($_GET['esp']!='reposicion')
				{
					$sql = "UPDATE mei_evaprevio SET
					idevaluacion = '".$_POST['cbo_nota']."',
					titulo = '".$_POST['txt_titulo']."',
					fechaactivacion = '".$fecha_a."',
					fechafinalizacion = '".$fecha_f."',
					comentario = '".base64_encode($_POST['edt_comentario'])."',
					clave= '".$_POST['txt_clave']."',
					tiempo = '".round($_POST['txt_tiempo'])."',
					barajarpreg = '".$_POST['cbo_bpreguntas']."',
					barajarresp = '".$_POST['cbo_brespuestas']."',
					intentos = '".$_POST['cbo_intentos']."',
					valorIntento = '".round(($_POST["cbo_valorIntento"]/100),4)."',
					idmodCalificar = '".$_POST["cbo_calificar"]."',
					mostrarnota = '".$_POST["cbo_mnota"]."',
					estado = '".$_POST['cbo_estado']."',
					valor = '".$valor."',
					incentivo = '".$incentivo."',
					curva = '".$curva."',
					idtiposubgrupo = ".$_POST["cbo_subgrupo"]."
					WHERE mei_evaprevio.idprevio = '".$_POST['hid_previo']."'";

					$baseDatos->ConsultarBD($sql);

					$sql="SELECT mei_usuprevio.idusuario FROM mei_usuprevio WHERE mei_usuprevio.idprevio='".$_POST['hid_previo']."'";
					$variab = $baseDatos->ConsultarBD($sql);
					while (list($iduser)=mysql_fetch_array($variab)) 
					{
						$sql="UPDATE mei_usuprevio set notaextra='".$incentivo."', flag=0 WHERE mei_usuprevio.idprevio='".$_POST['hid_previo']."' and mei_usuprevio.idusuario='".$iduser."'";
						$baseDatos->ConsultarBD($sql);
					}
					//INICIO MODIFICACION MENSAJE CALENDARIO



					if(!comprobarEditor($_POST['edt_comentario']))
					{
						$descripcion=$_POST['txt_titulo'];
					}
					else
					{
	            	 	$descripcion=$_POST['edt_comentario'];
	            	}

					registrarBitacora(2,5,false);
					if ($_SESSION['idtipousuario']==5)
						{	
							$sql="SELECT mei_relcalvirgru.idcalendario FROM mei_relcalvirgru WHERE mei_relcalvirgru.idprevio='".$_POST['hid_previo']."' ";
						}
					else
						{	
							$sql="SELECT mei_relcalgru.idcalendario FROM mei_relcalgru WHERE mei_relcalgru.idprevio='".$_POST['hid_previo']."' ";
						}
					$resultado=$baseDatos->ConsultarBD($sql);
              	    list ($idCalendario) = mysql_fetch_row($resultado);

              	    $fecha_a = substr($fecha_a, 0, -4);
              	    $fecha_a = date($fecha_a);

              	    if (empty($idCalendario)) {
              	    	$sql="INSERT INTO mei_calendario ( idcalendario , idusuario ,mensaje , fechamensaje , estado ,  fechacreacion , cartelera, destino)	
					VALUES ('', '".$_SESSION['idusuario']."', '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."', '".$fecha_a."', '0',  '".date('Y-n-j')."' , '1', '1' )";
					
					$baseDatos->ConsultarBD($sql);
					$idCalendario=$baseDatos->InsertIdBD();
														
					//cerramos modificación calendario

					if ($_SESSION['idtipousuario']==5){
	                	$sql1="INSERT INTO mei_relcalvirgru ( idcalendario , idvirgrupo , idprevio) VALUES ( '$idCalendario' , '".$_POST['hid_grupo']."' , '$idprevio' )";
						//$sql="INSERT INTO mei_relcarvirgru ( idcartelera , idvirgrupo, idprevio ) VALUES ('".$idCartelera."','".$_POST['hid_grupo']."' , '$idprevio')";
					}
					else{
	                	//colocamos doble comilla a $idCalendario	y quitamos el idprevio
	                	$sql1="INSERT INTO mei_relcalgru ( idcalendario , idgrupo, idprevio , idactividad ) VALUES ( '$idCalendario' , '".$_POST['hid_grupo']."', '$idprevio','0' )";
						//$sql="INSERT INTO mei_relcargru ( idcartelera , idgrupo, idprevio ) VALUES ('".$idCartelera."','".$_POST['hid_grupo']."' , '$idprevio')";
					}
					$baseDatos->ConsultarBD($sql1);
              	    }

              	    else
              	    {
						$sql="UPDATE mei_calendario
						SET mensaje = '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."',
						fechamensaje = '".$_POST['txt_fecha_a']."',
						estado = '0',
						fechacreacion = '".date('Y-n-j')."' ,
						cartelera = '1' ,
						destino = '1'
						WHERE `idcalendario` ='$idCalendario' ";
					}
				

					//FIN MODIFICACION MENSAJE CALENDARIO


				}
			   else
				{
					$sql = "UPDATE mei_evaquiz SET
					idevaluacion = '".$_POST['cbo_nota']."',
					titulo = '".$_POST['txt_titulo']."',
					fechaactivacion = '".$fecha_a."',
					fechafinalizacion = '".$fecha_f."',
					comentario = '".base64_encode($_POST['edt_comentario'])."',
					clave= '".$_POST['txt_clave']."',
					tiempo = '".round($_POST['txt_tiempo'])."',
					barajarpreg = '".$_POST['cbo_bpreguntas']."',
					barajarresp = '".$_POST['cbo_brespuestas']."',
					intentos = '".$_POST['cbo_intentos']."',
					valorIntento = '".round(($_POST["cbo_valorIntento"]/100),4)."',
					idmodCalificar = '".$_POST["cbo_calificar"]."',
					mostrarnota = '".$_POST["cbo_mnota"]."',
					estado = '".$_POST['cbo_estado']."',
					valor = '".$valor."',
					incentivo = '".$incentivo."',
					curva = '".$curva."',
					idtiposubgrupo = ".$_POST["cbo_subgrupo"]."
					WHERE mei_evaquiz.idprevio = '".$_POST['hid_previo']."'";

					$baseDatos->ConsultarBD($sql);
				}

				$consulta=$baseDatos->ConsultarBD($sql);
				
				/* Actualizacion Cartelera
				$sqlidcartelera="SELECT mei_relcargru.idcartelera FROM mei_relcargru WHERE mei_relcargru.idprevio='".$_POST['hid_previo']."' ";
				$resultado_idcartelera = $baseDatos->ConsultarBD($sqlidcartelera);	
				list ($idcartelera) = mysql_fetch_row($resultado_idcartelera);
				
				$sqlcartelera="UPDATE mei_cartelera 
				SET mensaje = '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."',
				estado = '0',
				idusuario = '".$_SESSION['idusuario']."' ,
				fechaactivacion = '$fecha_a',
				fechacaducidad = '".$_POST['txt_fecha_f']."'
				WHERE `idcartelera` ='$idcartelera'";

				$consultacartelera=$baseDatos->ConsultarBD($sqlcartelera);	
				/* Fin Actualizacion Cartelera*/				

				 $sql1=" SELECT mei_evaprevio.estado FROM mei_evaprevio WHERE mei_evaprevio.idprevio ='".$_POST['hid_previo']."' ";
             	 $resultado=$baseDatos->ConsultarBD($sql1);
              	 list ($estadop) = mysql_fetch_row($resultado);
              	 if ($estadop==1)
              	 {
              	 	$sql=" SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio WHERE mei_evaprevio.idprevio ='".$_POST['hid_previo']."' ";
             	    $resultado=$baseDatos->ConsultarBD($sql);
              	    list ($ideval) = mysql_fetch_row($resultado);

					if ($_SESSION['idtipousuario']==5)
              	 		$sql=" SELECT mei_evavirgrupo.idvirgrupo FROM mei_evavirgrupo WHERE mei_evavirgrupo.idevaluacion =$ideval ";
					else
              	 		$sql=" SELECT mei_evagrupo.idgrupo FROM mei_evagrupo WHERE mei_evagrupo.idevaluacion =$ideval ";
             	    $resultado=$baseDatos->ConsultarBD($sql);
             	    list ($idgrup) = mysql_fetch_row($resultado);

					if ($_SESSION['idtipousuario']==5)
	             	    $sql=" SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo =$idgrup ";
					else
	             	    $sql=" SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idgrupo =$idgrup ";
             	    $resultado=$baseDatos->ConsultarBD($sql);

             	    $sql="SELECT mei_ipactivo.idusuario FROM mei_ipactivo";
             	    $consultaIp=$baseDatos->ConsultarBD($sql);
              	 	$cont=0;

             	 	$alumnos = array ();
     		 	 	while(list($idusu)=mysql_fetch_array($resultado))
             	 	{
             	 		$alumnos[$cont] = $idusu;
             	 		$cont++;
            	 	}


              	 	while(list($idusuario)=mysql_fetch_array($consultaIp))
    			 	{


                      foreach ($alumnos as $idusuIP)
                      {
      		                 if($idusuario == $idusuIP )
      		 				 {
        			  			$contador++;
        			  			//$sql="DELETE FROM mei_ipactivo WHERE mei_ipactivo.idusuario=".$idusuario;
        						//$baseDatos->ConsultarBD($sql);
      				       	 }

      				  }

    			 	}



              	 }

				$idprevio = $_POST['hid_previo'];

				if(($_GET['sologuardar'] == 1)&&(empty($_GET['parcial']))){
					redireccionar("../actividades/verActividad.php?idmateria=".$_POST["hid_materia"]."&materia=".$_GET["materia"]."&cbo_orden=".$_GET["ordengrupo"]);
				}
				elseif(($_GET['sologuardar'] == 1)&&($_GET['parcial'] == 1)){
					redireccionar("../evaluaciones/verEvaluacion.php?idmateria=".$_POST["hid_materia"]."&materia=".$_GET["materia"]."&cbo_orden=".$_GET["cbo_orden"]);
				}				
				else{
					redireccionar("llenarEvaluacion.php?esp=".$_GET['esp']."&idmateria=".$_POST["hid_materia"]."&idgrupo=".$_GET['idgrupo']."&materia=".$_GET['materia']."&totalpreguntas=".$_POST['totalpreguntas']);
				}

		    }

			elseif($_GET["modo"] == 'DEL')
			{
				
				
				if($_GET['esp']!='reposicion'){
						
					$sql1 = "SELECT idevaluacion FROM mei_evaprevio WHERE mei_evaprevio.idprevio ='".$_GET["idprevio"]."' ";
				$eva=$baseDatos->ConsultarBD($sql1);
				list($ideva)=mysql_fetch_array($eva);	
					
					$sql = "DELETE FROM mei_evaprevio WHERE mei_evaprevio.idprevio=".$_GET["idprevio"];
				}
				else{
					$sql = "DELETE FROM mei_evaquiz WHERE mei_evaquiz.idprevio=".$_GET["idprevio"];}
				
				$baseDatos->ConsultarBD($sql);
				
				$sql="SELECT tipoevaluacion FROM mei_evaluacion WHERE idevaluacion='".$ideva."'";
				$tipoeva=$baseDatos->ConsultarBD($sql);
				list($tipo)=mysql_fetch_array($tipoeva);
				
				if($tipo==1){
				
				$sql = "DELETE FROM mei_evaluacion WHERE mei_evaluacion.idevaluacion=".$ideva;
				$baseDatos->ConsultarBD($sql);
				}

				//INICIO BORRADO MENSAJE CALENDARIO
					if ($_SESSION['idtipousuario']==5)
	   					$sql = "SELECT mei_relcalvirgru.idcalendario FROM mei_relcalvirgru WHERE mei_relcalvirgru.idprevio ='".$_GET["idprevio"]."' ";
					else
	   					$sql = "SELECT mei_relcalgru.idcalendario FROM mei_relcalgru WHERE mei_relcalgru.idprevio ='".$_GET["idprevio"]."' ";
	                $resultado=$baseDatos->ConsultarBD($sql);
	                list ($idCalendario) = mysql_fetch_row($resultado);

	   				registrarBitacora(2,6,false);

					$sql="DELETE FROM mei_calendario WHERE mei_calendario.idcalendario = '$idCalendario' ";
					$consulta=$baseDatos->ConsultarBD($sql);
					
					/* Actualizacion Cartelera
					$sqlidcartelera="SELECT mei_relcargru.idcartelera FROM mei_relcargru WHERE mei_relcargru.idprevio='".$_GET['idprevio']."' ";
					$resultado_idcartelera = $baseDatos->ConsultarBD($sqlidcartelera);	
					list ($idcartelera) = mysql_fetch_row($resultado_idcartelera);
					
					$sqleliminar="DELETE FROM mei_cartelera WHERE mei_cartelera.idcartelera = '$idcartelera' ";
					$eliminarcartelera=$baseDatos->ConsultarBD($sqleliminar);	
					$sqleliminar_gru="DELETE FROM mei_relcargru WHERE mei_relcargru.idcartelera = '$idcartelera' ";
					$eliminarcartelera=$baseDatos->ConsultarBD($sqleliminar_gru);
					/* Fin Actualizacion Cartelera*/					


	            //FIN BORRADO MENSAJE CALENDARIO
				
				if($tipo==1){
				redireccionar("verEvaluacion.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']."&cbo_orden=".$_GET["cbo_orden"]);
				}else{
				redireccionar("../actividades/verActividad.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']."&cbo_orden=".$_GET["cbo_orden"]);
				}
			}
			elseif($_GET["modo"] == 'ACT')
			{
	  			$status=$_GET["estado"];

	            if ($status==1)
	            {

				 	 $sql=" SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio WHERE mei_evaprevio.idprevio ='".$_GET["idprevio"]."' ";
	             	 $resultado=$baseDatos->ConsultarBD($sql);
	              	 list ($ideval) = mysql_fetch_row($resultado);

					 if ($_SESSION['idtipousuario']==5)
	             	 	$sql=" SELECT mei_evavirgrupo.idvirgrupo FROM mei_evavirgrupo WHERE mei_evavirgrupo.idevaluacion =$ideval ";
					else
	             	 	$sql=" SELECT mei_evagrupo.idgrupo FROM mei_evagrupo WHERE mei_evagrupo.idevaluacion =$ideval ";
	             	 $resultado=$baseDatos->ConsultarBD($sql);
	             	 list ($idgrup) = mysql_fetch_row($resultado);

					 if ($_SESSION['idtipousuario']==5)
	             	 	$sql=" SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo =$idgrup ";
					else
	             	 	$sql=" SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idgrupo =$idgrup ";
	             	 $resultado=$baseDatos->ConsultarBD($sql);

	            	 $sql="SELECT mei_ipactivo.idusuario FROM mei_ipactivo";
	             	 $consultaIp=$baseDatos->ConsultarBD($sql);
	              	 $cont=0;

	             	 $alumnos = array ();
	     		 	 while(list($idusu)=mysql_fetch_array($resultado))
	             	 {
	             	 	$alumnos[$cont] = $idusu;
	             	 	$cont++;
	            	 }


	              	 while(list($idusuario)=mysql_fetch_array($consultaIp))
	    			 {


	                      foreach ($alumnos as $idusuIP)
	                      {
	      		                 if($idusuario == $idusuIP )
	      		 				 {
	        			  			$contador++;
	        			  			$sql="DELETE FROM mei_ipactivo WHERE mei_ipactivo.idusuario=".$idusuario;
	        						$baseDatos->ConsultarBD($sql);
	      				       	 }

	      				  }

	    			 }


	            }



	      			if($_GET['esp']!='reposicion')
						{$sql = "UPDATE mei_evaprevio SET
						mei_evaprevio.estado = '".$_GET["estado"]."'
						WHERE mei_evaprevio.idprevio = '".$_GET["idprevio"]."'";}
					else
						{$sql = "UPDATE mei_evaquiz SET
						mei_evaquiz.estado = '".$_GET["estado"]."'
						WHERE mei_evaquiz.idprevio = '".$_GET["idprevio"]."'";}

						$baseDatos->ConsultarBD($sql);
				//print $sql;
					if ($_GET["tipo"]=='Q')
						redireccionar("../actividades/verActividad.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']);
					else
						redireccionar("verEvaluacion.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']."&cbo_orden=".$_GET["cbo_orden"]);
			}
			elseif($_GET["modo"] == 'ACTVISI'){
				
				if($_GET['esp']!='reposicion')
					{$sql = "UPDATE mei_evaprevio SET
					mei_evaprevio.visibilidad = '".$_GET["visibilidad"]."'
					WHERE mei_evaprevio.idprevio = '".$_GET["idprevio"]."'";}
				else
					{$sql = "UPDATE mei_evaquiz SET
					mei_evaquiz.visibilidad = '".$_GET["visibilidad"]."'
					WHERE mei_evaquiz.idprevio = '".$_GET["idprevio"]."'";}

					$baseDatos->ConsultarBD($sql);
				if($_GET['evaluacion']==1){
					$sql=" SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio WHERE mei_evaprevio.idprevio =".$_GET["idprevio"];
					$resultado=$baseDatos->ConsultarBD($sql);
	             	list ($idevaluacionprev) = mysql_fetch_row($resultado);
					$sql = "UPDATE mei_evaluacion SET
					mei_evaluacion.visibilidad = '".$_GET["visibilidad"]."',
					mei_evaluacion.valor_eval = 0
					WHERE mei_evaluacion.idevaluacion = '".$idevaluacionprev."'";
					$baseDatos->ConsultarBD($sql);
				}
			//print $sql;
				if ($_GET["tipo"]=='Q')
					redireccionar("../actividades/verActividad.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']);
				else
					redireccionar("verEvaluacion.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']."&cbo_orden=".$_GET["cbo_orden"]);			
					
			}
			else
			{
			/*$sql = "INSERT INTO `mei_evaprevio` ( `idprevio` , `idevaluacion` , `idautor` , `titulo` , `fechacreacion` ,
						`fechaactivacion` , `fechafinalizacion` , `comentario` , `tiempo` , `barajarpreg` , `barajarresp` ,
						`intentos` , `valorIntento` , `idmodCalificar` , `mostrarnota` , `estado` , `clave`,`idtipoprevio` , `valor`, `idtiposubgrupo` )
						VALUES ('','".$_POST["cbo_nota"]."',
						'".$_SESSION["idusuario"]."', '".$_POST["txt_titulo"]."', '".date('YmdHi')."', '".$fecha_a."',
						'".$fecha_f."', '".base64_encode($_POST["edt_comentario"])."', '".round($_POST["txt_tiempo"])."',
						'".$_POST["cbo_bpreguntas"]."', '".$_POST["cbo_brespuestas"]."', '".$_POST["cbo_intentos"]."',
						'".round(($_POST["cbo_valorIntento"]/100),2)."', '".$_POST["cbo_calificar"]."', '".$_POST["cbo_mnota"]."',
						'".$_POST["cbo_estado"]."', '".base64_encode($_POST['txt_clave'])."', '".$_POST["hid_tipoprevio"]."', '".$valor."', ".$_POST["cbo_subgrupo"].")";*/

				$tiposubgruponulo=0;							
				if ("".$_POST["cbo_subgrupo"]=="NULL")
					{
						$tiposubgruponulo=1;
						$_POST["cbo_subgrupo"]=1;
						$q = "";
						$w = "";
						
					}else
					{
						$w = ",'".$_POST["cbo_subgrupo"]."'";
						$q = ",idtiposubgrupo";
					}								
				if($_GET['esp']!='reposicion')
			
				$sql = "INSERT INTO `mei_evaprevio` ( `idprevio` , `idevaluacion` , `idautor` , `titulo` , `fechacreacion` ,
						`fechaactivacion` , `fechafinalizacion` , `comentario` , `clave`,`tiempo` , `barajarpreg` , `barajarresp` ,
						`intentos` , `valorIntento` , `idmodCalificar` , `mostrarnota` , `estado` , `idtipoprevio` , `valor`".$q.", `incentivo`, `curva`  )
						VALUES ('','".$_POST["cbo_nota"]."',
						'".$_SESSION["idusuario"]."', '".$_POST["txt_titulo"]."', '".date('YmdHi')."', '".$fecha_a."',
						'".$fecha_f."', '".base64_encode($_POST["edt_comentario"])."','".$_POST["txt_clave"]."', '".round($_POST["txt_tiempo"])."',
						'".$_POST["cbo_bpreguntas"]."', '".$_POST["cbo_brespuestas"]."', '".$_POST["cbo_intentos"]."',
						'".round(($_POST["cbo_valorIntento"]/100),4)."', '".$_POST["cbo_calificar"]."', '".$_POST["cbo_mnota"]."',
						'".$_POST["cbo_estado"]."', '".$_POST["hid_tipoprevio"]."', '".$valor."'".$w.",'".$incentivo."','".$curva."')";
				else
			    $sql = "INSERT INTO `mei_evaquiz` ( `idprevio` , `idevaluacion` , `idautor` , `titulo` , `fechacreacion` ,
						`fechaactivacion` , `fechafinalizacion` , `comentario` , `clave` , `tiempo` , `barajarpreg` , `barajarresp` ,
						`intentos` , `valorIntento` , `idmodCalificar` , `mostrarnota` , `estado` , `idtipoprevio` , `valor`".$q.", `incentivo`, `curva`  )
						VALUES ('','".$_POST["cbo_nota"]."',
						'".$_SESSION["idusuario"]."', '".$_POST["txt_titulo"]."', '".date('YmdHi')."', '".$fecha_a."',
						'".$fecha_f."', '".base64_encode($_POST["edt_comentario"])."','".$_POST["txt_clave"]."', '".round($_POST["txt_tiempo"])."',
						'".$_POST["cbo_bpreguntas"]."', '".$_POST["cbo_brespuestas"]."', '".$_POST["cbo_intentos"]."',
						'".round(($_POST["cbo_valorIntento"]/100),4)."', '".$_POST["cbo_calificar"]."', '".$_POST["cbo_mnota"]."',
						'".$_POST["cbo_estado"]."', '".$_POST["hid_tipoprevio"]."', '".$valor."'".$w.",'".$incentivo."','".$curva."')";


				$baseDatos->ConsultarBD($sql);				
	            $idprevio = $baseDatos->InsertIdBD();	           
	            if ($tiposubgruponulo == 1)
	            {
					
					$sql = "UPDATE mei_evaprevio SET
					idtiposubgrupo = 'NULL'
					WHERE mei_evaprevio.idprevio = '".$idprevio."'";

					$baseDatos->ConsultarBD($sql);
					
	            }
				$evaluacion = new evaluacion($idprevio,$idmateria,$_POST["hid_tipoprevio"],'');
				$_SESSION["evaluacion"] = $evaluacion;
			

	        //INICIO Agregar en el Calendario y cartelera. (Creacion por primera ves de una evaluacion)

				if(!empty($_POST['chk_calendario']))
				{

					if(!comprobarEditor($_POST['edt_comentario']))
					{
						$descripcion=$_POST['txt_titulo'];
					}
					else
					{
	            	 	$descripcion=$_POST['edt_comentario'];
	            	}


					registrarBitacora(2,2,false);

 				   /* $exists = false;
					$db = "mei_relcargru";
					$column = "idprevio";
					$column_attr = "int(11)";
				    $columns = "show columns from $db";
					$resultcolumn = $baseDatos->ConsultarBD($resultcolumn);
				    while($c = mysql_fetch_assoc($resultcolumn)){
					    if($c['Field'] == $column){
						    $exists = true;
						    break;
					    }
				    }        
				    if(!$exists){
					    $sql = "ALTER TABLE `$db` ADD `$column`  $column_attr";
				    }					
					$baseDatos->ConsultarBD($sql);
					
					$caducidad = 1;
					$fechaactivacion= $_POST['txt_fecha_a'];
					$fechaCaducidad = $_POST['txt_fecha_f'];
					$destino = 1;
					
					$sql="INSERT INTO mei_cartelera ( idcartelera , mensaje , estado , idusuario , fechacreacion , fechaactivacion, caducidad , fechacaducidad , destino) 
						VALUES ('', '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."', '0', '".$_SESSION['idusuario']."', '".date('Y-n-j')."','".$fecha_a."' ,'".$caducidad."', '".$fechaCaducidad."' , ".$destino.")";
					
					$baseDatos->ConsultarBD($sql);
					$idCartelera=$baseDatos->InsertIdBD();
					*/
					 //Agregamos todo lo de mei_cartelera
					
					 				    $exists = false;
					$db = "mei_relcalgru";
					$column = "idactividad";
					$column_attr = "int(11)";
				    $columns = "show columns from $db";
					$resultcolumn = $baseDatos->ConsultarBD($resultcolumn);
				    while($c = mysql_fetch_assoc($resultcolumn)){
					    if($c['Field'] == $column){
						    $exists = true;
						    break;
					    }
				    }        
				    if(!$exists){
					    $sql = "ALTER TABLE `$db` ADD `$column`  $column_attr";
				    }					
					$baseDatos->ConsultarBD($sql);
					$fechaactivacion= $_POST['txt_fecha_a'];
					$fechaCaducidad = $_POST['txt_fecha_f'];
					$destino = 1;					
					
					$sql="INSERT INTO mei_calendario ( idcalendario , idusuario ,mensaje , fechamensaje , estado ,  fechacreacion , cartelera, destino)	
					VALUES ('', '".$_SESSION['idusuario']."', '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."', '".$fechaactivacion."', '0',  '".date('Y-n-j')."' , '1', ".$destino." )";
					
					$baseDatos->ConsultarBD($sql);
					$idCalendario=$baseDatos->InsertIdBD();
														
					//cerramos modificación calendario

					if ($_SESSION['idtipousuario']==5){
	                	$sql1="INSERT INTO mei_relcalvirgru ( idcalendario , idvirgrupo , idprevio) VALUES ( '$idCalendario' , '".$_POST['hid_grupo']."' , '$idprevio' )";
						//$sql="INSERT INTO mei_relcarvirgru ( idcartelera , idvirgrupo, idprevio ) VALUES ('".$idCartelera."','".$_POST['hid_grupo']."' , '$idprevio')";
					}
					else{
	                	//colocamos doble comilla a $idCalendario	y quitamos el idprevio
	                	$sql1="INSERT INTO mei_relcalgru ( idcalendario , idgrupo, idprevio , idactividad ) VALUES ( '$idCalendario' , '".$_POST['hid_grupo']."', '$idprevio','0' )";
						//$sql="INSERT INTO mei_relcargru ( idcartelera , idgrupo, idprevio ) VALUES ('".$idCartelera."','".$_POST['hid_grupo']."' , '$idprevio')";
					}

					//$baseDatos->ConsultarBD($sql);
					$baseDatos->ConsultarBD($sql1);

	 			}

			//FIN Agregar en el Calendario y cartelera. (Creacion por primera ves de una evaluacion)
	 			for($i=1;$i<=5;$i++)
					{//FOR 1
						
					if($_FILES['fil_archivo'.$i])
				{
			$archivo= $_FILES['fil_archivo'.$i]['name'];
			$nombreArchivo="(".$idprevio.")".$archivo;
			$nombreArchivo=corregirCaracteresURL($nombreArchivo);
			$archivo=corregirCaracteresURL($archivo);
			if (cargarArchivo('fil_archivo'.$i,corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosEvaluaciones/'))
			{
				

				$sql2 = "INSERT INTO mei_relevalarchi ( idarchivo , idevaluacion , archivo , localizacion ) VALUES ('','".$idprevio."','".$archivo."','".$nombreArchivo."') ";
				$baseDatos->ConsultarBD($sql2);
					

			}
			else
			{
				if(!empty($archivo))
				{
						?> <script> alert('Algunos de los archivos no se cargaron correctamente ')</script> 
						<?
					break;
					if(empty($noAdjuntados))
						$noAdjuntados.=$archivo;
					else
						$noAdjuntados.=' | '.$archivo;
				}
			}
		}
}

               if($_GET['automatica'] == "default"){
                    redireccionar("guardarPreguntaAleatoria.php?esp=".$_GET['esp']."&idgrupo=".$_GET['idgrupo']."&idmateria=".$_POST["hid_materia"]."&materia=".$_GET['materia']."&totalpreguntas=".$_POST['totalpreguntas']."&arrpreguntas=".$_POST['arrpreguntas']);
                }else{
                    redireccionar("llenarEvaluacion.php?esp=".$_GET['esp']."&idgrupo=".$_GET['idgrupo']."&idmateria=".$_POST["hid_materia"]."&materia=".$_GET['materia']."&totalpreguntas=".$_POST['totalpreguntas']);                
                }
		
			}
	}
	else redireccionar('../login/');
}
?>

