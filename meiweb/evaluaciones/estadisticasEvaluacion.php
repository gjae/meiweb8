<?
	include_once ('../menu/Menu.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{	
		$baseDatos= new BD();	
		$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraBasica' , '' );
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
</head>
<body>
	<table height="350" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
		    <td valign="top" class="tdGeneral">
            	<table class="tablaMarquesina">
					<tr class="trTitulo">
<?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado);
?>
              			<td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nombre))?></a><a> -> </a><a href="../evaluaciones/verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a><a> -> </a><a href="verDtlleEvaluacion.php?idprevio=<?=$_GET["idprevio"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET["materia"]?>" class="link">Ver Evaluaci&oacute;n</a> -> Ver Estadisticas </td>
	              </tr>
            </table><br>
<?
			
			//**********************CARATERISTICAS DE LA EVALUACION*************************
			$sql = "SELECT mei_evaprevio.titulo, mei_evaluacion.valor_eval, mei_evaprevio.intentos,mei_evaprevio.valorIntento 
					FROM mei_evaluacion, mei_evaprevio WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion 
					AND	mei_evaprevio.idprevio = '".$_GET["idprevio"]."'";
			//print $sql;		
			$resultado = $baseDatos->ConsultarBD($sql);
			
			list($titulo, $valor, $intentos, $valorI) = mysql_fetch_row($resultado);							
?>			
			<table class="tablaGeneral">
              <tr class="trSubTitulo">
                <td colspan="2" align="left" valign="middle">Caracteristicas de la Evaluaci&oacute;n </td>
              </tr>
              <tr class="trInformacion">
                <td width="30%" align="left" valign="middle"><strong>Titulo: </strong></td>
                <td width="70%" align="left" valign="middle"><?=$titulo?></td>
              </tr>
              <tr class="trInformacion">
                <td align="left" valign="middle"><strong>Valor: </strong></td>
                <td align="left" valign="middle"><?=($valor*100)."%"?></td>
              </tr>
              <tr class="trInformacion">
                <td align="left" valign="middle"><strong>N&uacute;mero M&aacute;ximo de Intentos: </strong></td>
                <td align="left" valign="middle"><?=$intentos?></td>
              </tr>
            </table>&nbsp;
			<table class="tablaGeneral">
				<tr class="trTitulo">
					<td>Resultados de los Intentos</td>
				</tr>
			</table>
		<?
			/*$sql = "SELECT COUNT(*) FROM mei_usuprevio WHERE mei_usuprevio.idprevio = '".$_GET["idprevio"]."'";
			$resultado = $baseDatos->ConsultarBD($sql);
			list($numResp) = mysql_fetch_row($resultado);									
		?>&nbsp;
			<table class="tablaGeneral">
              <tr class="trSubTitulo">
                <td colspan="2">Shit</td>
              </tr>
              <tr class="trInformacion">
                <td width="35%"><strong>N&uacute;mero de Respuestas: </strong></td>
                <td width="65%"><?=$numResp?></td>
              </tr>
              <tr class="trListaClaro">
                <td><strong>Nota Promedio: </strong></td>
                <td>&nbsp;</td>
              </tr>
              <tr class="trInformacion">
                <td><strong>Respuestas cuya nota es mayor a 3.0</strong></td>
                <td>&nbsp;</td>
              </tr>
              <tr class="trListaClaro">
                <td><strong>Respuestas cuya nota es menor a 3.0 </strong></td>
                <td>&nbsp;</td>
              </tr>
            </table>		
			<? */
			/*$sql = "SELECT mei_evaintento.valor, mei_evaintento.orden FROM mei_evaintento 
					WHERE mei_evaintento.idintento <= '".$intentos."' ORDER BY mei_evaintento.orden ASC" ;
			$resultado = $baseDatos->ConsultarBD($sql);*/

			//while ( list($valorIntento, $orden) = mysql_fetch_row($resultado) )
			for ($i=1;$i<=$intentos;$i++)
			{//WHILE 1
				$valorIntento = (1 - ($i-1)*$valorI);
		?>&nbsp;
			
			<table width="290" class="tablaGeneral">              
              <tr class="trSubTitulo">
                <td width="30%" align="left" valign="middle"><strong>Intento Número <?=$i?></strong></td>
                <td colspan="3" align="left" valign="middle">Valor del Intento: <?=($valorIntento*100)."%"?></td>
              </tr>
		<?
				//****************************NOTA TOTAL*************************************
				$sql = "SELECT COUNT(*), SUM(mei_usuprevio.nota), VARIANCE(mei_usuprevio.nota), STD(mei_usuprevio.nota)
				  		FROM mei_usuprevio
						WHERE mei_usuprevio.idprevio = '".$_GET["idprevio"]."' AND 
						mei_usuprevio.idintento = '".$i."' AND mei_usuprevio.nota>0";
				$resultado2 = $baseDatos->ConsultarBD($sql);
				//print $sql."<p>";
				list($numResp, $sumNota, $varianza, $devsSd) = mysql_fetch_row($resultado2);
				
				//****************************NOTA < 3.0*************************************
				$sql = "SELECT COUNT(*), SUM(mei_usuprevio.nota) FROM mei_usuprevio
						WHERE mei_usuprevio.idprevio = '".$_GET["idprevio"]."' AND 
						mei_usuprevio.idintento = '".$i."' AND mei_usuprevio.nota<3 AND mei_usuprevio.nota>0";
				$resultado3 = $baseDatos->ConsultarBD($sql);
				//print $sql;
				list($numResp_min3, $sumNota_min3) = mysql_fetch_row($resultado3);
				
				//****************************NOTA > 3.0*************************************
				$sql = "SELECT COUNT(*), SUM(mei_usuprevio.nota) FROM mei_usuprevio
						WHERE mei_usuprevio.idprevio = '".$_GET["idprevio"]."' AND 
						mei_usuprevio.idintento = '".$i."' AND mei_usuprevio.nota>=3";
				$resultado4 = $baseDatos->ConsultarBD($sql);
				//print $sql;
				list($numResp_max3, $sumNota_max3) = mysql_fetch_row($resultado4);
?>
              <tr class="trInformacion">
                <td align="left" valign="middle"><strong>Respuestas a este Intento</strong></td>
                <td colspan="3" align="left" valign="middle"><?=$numResp?></td>
              </tr>
              <tr class="trListaClaro">
                <td align="left" valign="middle"><strong>Nota Promedio: </strong></td>
                 <td width="20%" align="left" valign="middle"><? if ($numResp>0) print number_format(($sumNota/$numResp),1,'.','');else print 0;?></td>
                <td width="20%" align="left" valign="middle"><strong>Varianza: </strong><?=round($varianza,2)?></td>
                <td width="30%" align="left" valign="middle"><strong>Desviaci&oacute;n Estandar: </strong><?=round($devsSd,2)?></td>
              </tr>
              <tr class="trInformacion">
                <td align="left" valign="middle"><strong>Respuestas cuya nota es mayor o igual a  3.0 </strong></td>
                <td colspan="3" align="left" valign="middle"><?=$numResp_max3?></td>
              </tr>
              <tr class="trListaClaro">
                <td align="left" valign="middle"><strong>Respuestas cuya nota es menor a 3.0 </strong></td>
                <td colspan="3" align="left" valign="middle"><?=$numResp_min3?></td>
              </tr>
            </table>
		<?
			}//WHILE 2
		?>&nbsp;
		<table class="tablaMarquesina">
          <tr align="center" valign="middle">
            <td><a href="verDtlleEvaluacion.php?idprevio=<?=$_GET["idprevio"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_GET["cbo_orden"]?>" class="link">Volver</a></td>
          </tr>
        </table></td>
        <td class="tablaEspacio">&nbsp;</td>
        <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>