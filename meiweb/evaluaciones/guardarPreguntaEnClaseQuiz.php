<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once('evaclass.php');

if( (comprobarSession() ) && ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==5) )
{
	$evaluacion = $_SESSION["evaluacion"]; 	
	$baseDatos= new BD();		

	if ( $_GET["accion"] == "del" )
	{
		$evaluacion->eliminar_preg($_GET["idpregunta"]);
	}
	else
	{
		$sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta, mei_evatipopregunta.tipo, mei_tema.titulo 
				FROM mei_evapreguntas, mei_evatipopregunta, mei_tema
				WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
				mei_evapreguntas.idtema = mei_tema.idtema AND mei_evapreguntas.idpregunta=".$_GET["idpregunta"];			
	
		$resultado = $baseDatos->ConsultarBD($sql);		
		
		list($idpreg, $preg, $idtipo, $tipo, $tema, $valor) = mysql_fetch_row($resultado);
		$valorPreg = ($_POST["cbo_valor"]/100);
		$evaluacion->agregar_preg($idpreg, $idpreg, stripslashes(base64_decode($preg)), $idtipo, $tipo, $tema, $valorPreg);

		if ($_POST["chk_promedio"]==1)
		{
			$numPreg = count($evaluacion->preguntas);
			$valorPreg = round((1/$numPreg),3);
			
			while( list($id) = each($evaluacion->preguntas) )
			{
				$evaluacion->preguntas[$id]->valor = $valorPreg;		
			} 
		}			
	}
	
	
	redireccionar("llenarQuiz.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
	
}
else redireccionar('../login/');
?>