<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once('evaclass.php');
	
	if(comprobarSession())
	{	
		$evaluacion = $_SESSION["evaluacion"]; 	

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>

function validarDecimal()
{ 
	valor = document.frm.cbo_valor.value
	if ( (isNaN(valor)) ) 	  
	{              
      		return false   
	}	
	else
	{      
            	return true 
      	} 
}

function enviar()
{
	if (validarDecimal())
	{
		document.frm.submit();
		window.close();
	}
	else alert("El campo 'Valor' no puede estar vacío")
}

</script>
<title>MEIWEB</title>
</head>
<body onLoad="window.focus()">
<form name="frm" method="post" action="guardarValoresNE.php?idusuario=<?=$_GET["idusuario"]?>&idprevio=<?=$_GET["idprevio"]?>&esp=<?=$_GET["esp"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET['materia']?>&intentos=<?=$_GET['intentos']?>" target="notasEvaluacion">
 

  <table width="220" border="0" cellpadding="0" cellspacing="0">
    <tr class="trTitulo">
      <td colspan="2" align="left" valign="middle">Editar</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Valor:</strong></td>
      <td align="left" valign="middle"><label>
        <input name="cbo_valor" type="text" id="cbo_valor" size="10" maxlength="5">
      </label></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trSubTitulo" align="center" valign="middle">
      <td colspan="2"><input type="button" name="Submit" value="Cambiar" onClick="javascript:enviar()"></td>
    </tr>
  </table>
  
</form>

</body>
</html>
<?
}
else redireccionar('../login/');
?>

