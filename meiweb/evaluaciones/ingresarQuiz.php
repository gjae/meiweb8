<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
if(comprobarSession())
{	
	$baseDatos= new BD();	
	$editor=new FCKeditor('edt_comentario' , '100%' , '200' , 'barraBasica' , '' );
	$fecha_a=new FrmCalendario('frm_ingresarEvaluacion','txt_fecha_a','formulario','false','');	
	$fecha_f=new FrmCalendario('frm_ingresarEvaluacion','txt_fecha_f','formulario','false','');	
	
	//******************GRUPO*************************
	if (!empty($_POST["cbo_grupo"]))
		$opcion = $_POST["cbo_grupo"];
	else $opcion = $_POST["hid_grupo"];

	list($idgrupo, $grupo) = explode("-", $opcion);
	//************************************************
	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion, mei_evavirgrupo WHERE 
			mei_evavirgrupo.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.tipoevaluacion = 2 AND
			mei_evaluacion.promediar = 1 AND mei_evavirgrupo.idvirgrupo='".$idgrupo."'";
	}
	else
	{
		$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion, mei_evagrupo WHERE 
			mei_evagrupo.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.tipoevaluacion = 2 AND
			mei_evaluacion.promediar = 1 AND mei_evagrupo.idgrupo='".$idgrupo."'";
	}

	$resgrupo = $baseDatos->ConsultarBD($sql);
	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_evaprevio.idevaluacion, SUM(mei_evaprevio.valor) FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaprevio.idtipoprevio = 2 AND 
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."'
			GROUP BY mei_evaprevio.idevaluacion";
	}
	else
	{
		$sql = "SELECT mei_evaprevio.idevaluacion, SUM(mei_evaprevio.valor) FROM mei_evaprevio, mei_evaluacion, mei_evagrupo 
			WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaprevio.idtipoprevio = 2 AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'
			GROUP BY mei_evaprevio.idevaluacion";
	}
	//print $sql."<p>";			
	$resquiz = $baseDatos->ConsultarBD($sql);
	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_actividad.idevaluacion, SUM(mei_actividad.valor) FROM mei_actividad, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."'
			GROUP BY mei_actividad.idevaluacion";
	}
	else
	{
		$sql = "SELECT mei_actividad.idevaluacion, SUM(mei_actividad.valor) FROM mei_actividad, mei_evaluacion, mei_evagrupo 
			WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'
			GROUP BY mei_actividad.idevaluacion";
	}
	//print $sql."<p>";
	$resultado = $baseDatos->ConsultarBD($sql);
	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_foro.idevaluacion, SUM(mei_foro.valor) FROM mei_foro, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_foro.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."'
			GROUP BY mei_foro.idevaluacion";
	}
	else
	{
		$sql = "SELECT mei_foro.idevaluacion, SUM(mei_foro.valor) FROM mei_foro, mei_evaluacion, mei_evagrupo 
			WHERE mei_foro.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'
			GROUP BY mei_foro.idevaluacion";
	}
	//print $sql."<p>";
	
	$resforo = $baseDatos->ConsultarBD($sql);
	//******************AGREGAR CARTELERA*************************
	if(!(empty($_POST['txt_titulo']))){
	
	$caducidad=1;
	$fechaCaducidad = $_POST['txt_fecha_f'];
	$destino = 1;
	$sql2="INSERT INTO mei_cartelera ( idcartelera , mensaje , estado , idusuario , fechacreacion , fechaactivacion , caducidad , fechacaducidad , destino) 
		VALUES ('', '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($_POST['txt_titulo']))."', '0', '".$_SESSION['idusuario']."', '".date('Y-n-j')."', '".$fecha_a."' , '".$caducidad."', '".$fechaCaducidad."' , ".$destino.")";
					
	$baseDatos->ConsultarBD($sql2);		
	
	$idCartelera=$baseDatos->InsertIdBD();
	}
	//******************FINCARTELERA*************************
	$i=0;
	?>
	<script language="javascript">
		function val_porcentaje()
		{
			var notaValor = new Array(<?=mysql_num_rows($resultado)?>)
			idnota = document.frm_ingresarEvaluacion.cbo_nota.value
			valor = document.frm_ingresarEvaluacion.cbo_valor.value
			//alert(document.frm_ingresarTaller.hid_valor.value)
	<?
	while ( list($id, $avalor) = mysql_fetch_row($resultado) )
	{
		if ( round($avalor,2)>=1 )
			$inact[$i] = $id;
		else $vact[$id] = $avalor;
			
	?>
			notaValor[<?=$id?>] = <?=round($avalor,2)?>;
	<?		
		$i++;
	}
	?>
	
	<?
	while ( list($id, $avalor) = mysql_fetch_row($resforo) )
	{
		$vact[$id] += round($avalor,2);
		if ( round($vact[$id],2)>=1 )
			$inact[$i] = $id;
		//else $vact[$i] = $avalor;
	?>
			if (notaValor[<?=$id?>]>0)
				notaValor[<?=$id?>] = notaValor[<?=$id?>] + <?=round($avalor,2)?>;
			else notaValor[<?=$id?>] = <?=round($avalor,2)?>;
	<?		
		$i++;
	}
	?>	
	<?
	while ( list($id, $avalor) = mysql_fetch_row($resquiz) )
	{
		$vact[$id] += round($avalor,2);
		if ( round($vact[$id],2)>=1 )
			$inact[$i] = $id;
		//else $vact[$i] = $avalor;
	?>
			if (notaValor[<?=$id?>]>0)
				notaValor[<?=$id?>] = notaValor[<?=$id?>] + <?=round($avalor,2)?>;
			else notaValor[<?=$id?>] = <?=round($avalor,2)?>;
	<?		
		$i++;
	}
	?>	
			if (notaValor[idnota]>0)
				total = parseFloat(notaValor[idnota])+parseFloat(valor);
			else total = parseFloat(valor);
			//alert(total)
			//document.frm_ingresarTaller.hid_valor.value = total;
			if (total>1)
			{
				//alert('El porcentajes de las Actividades asiganadas a la Nota Seleccionada excede en 100%')
				return false
			}
			else return true
		}
		
		function comprobarNota(idnota)
		{
			var notaProm = new Array(<?=mysql_num_rows($resgrupo)?>)
			
			if (idnota==0) idnota = document.frm_ingresarEvaluacion.cbo_nota.value
			//alert(idnota)
		<?
			while( list($id) = mysql_fetch_row($resgrupo) )
			{
		?>
				notaProm[<?=$id?>] = 1
		<?
			}
		?>
			if (notaProm[idnota]==1)
			{
				document.frm_ingresarEvaluacion.cbo_valor.disabled = true
				//document.frm_ingresarTaller.cbo_valor.style.visibility = 'hidden'
				document.getElementById('div_etiqueta').style.visibility = 'visible'
			}
			else
			{
				document.frm_ingresarEvaluacion.cbo_valor.disabled = false
				//document.frm_ingresarTaller.cbo_valor.style.visibility = 'visible'
				document.getElementById('div_etiqueta').style.visibility = 'hidden'
			}
		}
		function cambiar(tec){
			for (var i=0; i < 20; i++) {
			  tec.value = tec.value.replace("-", "_");
			};
	
}		
</script>		
	<?	
		if ( count($inact)>0 )
			$idact_ex = implode(',', $inact);
		else $idact_ex = 0;
		/*print "lista: ".$idact_ex."<p>";
		print "count(vact): ".count($inact)."<p>";*/
		//************************************************
	?>	

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
	
function validarEntero(){ 
      //intento convertir a entero. 
     //si era un entero no le afecta, si no lo era lo intenta convertir 
     valor = parseInt(document.frm_ingresarEvaluacion.txt_tiempo.value) 
	 valor2 = parseInt(document.frm_ingresarEvaluacion.cbo_valorIntento.value) 
      //Compruebo si es un valor numérico 
      if ( ((isNaN(valor)) || (valor<1)) || ( (isNaN(valor2)) || (valor2<0) ) ) { 
            //entonces (no es numero) devuelvo el valor cadena vacia 
            return false  
      }else{ 
            //En caso contrario (Si era un número) devuelvo el valor 
            return true 
      } 
}
	
	function validar()
	{
		if ( document.frm_ingresarEvaluacion.txt_titulo.value!="" && document.frm_ingresarEvaluacion.txt_tiempo.value!="" )
		{	
			if ( validarEntero() )
			{
				if ( val_porcentaje() )
				{
					return true;
				}
				else alert('El Valor de las Actividades Asiganadas a la Nota Seleccionada Excede en 100%')
			}
			else alert("El campo 'Tiempo de la Evaluación' debe ser un número mayor o igual a 1. El campo 'Disminuir la Nota de cada intento' debe ser un número mayor a 0")
		}
		else 
			{
				alert('Debe llenar todos los campos')
				return false;
			}
	}
	
	function verHora()
	{

		var fecha=frm_ingresarEvaluacion.txt_fecha_a.value.split("-");
		if(fecha[1]<10){ fecha[1]='0'+fecha[1];}
		if(fecha[2]<10){ fecha[2]='0'+fecha[2];}
		fecha=fecha[0]+"-"+fecha[1]+"-"+fecha[2];
		
		var fechaC=frm_ingresarEvaluacion.txt_fecha_f.value.split("-");
		if(fechaC[1]<10){ fechaC[1]='0'+fechaC[1];}
		if(fechaC[2]<10){ fechaC[2]='0'+fechaC[2];}
		fechaC=fechaC[0]+"-"+fechaC[1]+"-"+fechaC[2];
		
		// se pasan las fechas a formato aaaa-mm-dd hh:mm:ss
		var hora_a = document.frm_ingresarEvaluacion.cbo_hora_a.value;
		var min_a= document.frm_ingresarEvaluacion.cbo_minuto_a.value;
		var hora_f = document.frm_ingresarEvaluacion.cbo_hora_f.value; 
		var min_f= document.frm_ingresarEvaluacion.cbo_minuto_f.value;
		
			fecha=fecha+" "+hora_a+":"+min_a+":00";
			fechaC=fechaC+" "+hora_f+":"+min_f+":00";
			
			if(fecha<fechaC){
				
					return true;
				}
			else{
				alert("la fecha de Activacion debe ser menor q la fecha de Finalizacion");
				return false;
			}
	}	

	function guardar()
	{
		if (validar())		{
			if (verHora())
			{
				//alert('nice')
				document.frm_ingresarEvaluacion.action="guardarEvaluacion.php?idgrupo=<?=$idgrupo?>&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>";
				document.frm_ingresarEvaluacion.submit();
			}
			else alert('La Hora de Finalizacion debe ser mayor que la Hora de Activación')			
		}
	}
	function cancelar(id, mat)
	{
		document.frm_ingresarEvaluacion.action="../actividades/verActividad.php?cbo_orden=<?=$_GET['cbo_orden']?>&idmateria="+id+"&materia="+mat;
		document.frm_ingresarEvaluacion.submit();
	}
    
    function generarautomatica(){
		if (validar())
		{
			if (verHora())
			{
				//alert('nice')
				if(document.getElementById('txt_clave').value=='')
				{
					if(confirm("¿Está seguro de dejar en blanco el campo contraseña?"))
					{
						document.frm_ingresarEvaluacion.action="guardarEvaluacion.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&automatica=default";
						document.frm_ingresarEvaluacion.submit();
					}
 				}

 				else
				{
					document.frm_ingresarEvaluacion.action="guardarEvaluacion.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&automatica=default";
					document.frm_ingresarEvaluacion.submit();
 				}
			}
			else alert('La Hora de Finalizacion debe ser mayor que la Hora de Activación')

		}        
    }
    function cargarsubtema(idtema){
    	if( idtema != "" ){
    	  $.ajax({
    					data:  ({ idtema : idtema }),
    					url:   'cargarsubtemas.php',
    					type:  'post',
    					beforeSend: function () {
    							$("#subtemacarga").html("Procesando, espere por favor...");
    					},
    					success:  function (response) {
    							$("#subtema").html(response);
    							//document.getElementById("subtematr").style.display = 'table-row';
    							document.getElementById("subtemacarga").style.display = 'none';
    							document.getElementById("cbo_gradoP").value='none';
    					}
    			});							
    	}	
    }
    function cantpreg(){
        var tema = document.getElementById("cbo_tema").value;
        var subtema = document.getElementById("cbo_sub_tema").value;
        var gradodif = document.getElementById("cbo_gradoP").value;   
        
        if( (tema != "")&&(gradodif != "") ){
    	  $.ajax({
    					data:  ({ tema : tema,subtema: subtema,gradodif: gradodif  }),
    					url:   'cantpreguntas.php',
    					type:  'post',
       					success:  function (response) {
       						    document.getElementById("numero").value = response;
    							var texto='Cantidad de preguntas max:'+response;
    							$("#cantpreg").html(texto);
    							if (response==0) {
    								document.getElementById("agregar").disabled = true;

    							}
    							else {
    								document.getElementById("agregar").disabled = false;
    							}
    							document.getElementById("cantpregbtn").display = "none";
    					}
    			});  
       } 
       else{ alert("Llenar todos los campos por favor"); }           
    }
    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " 1234567890";
        especiales = [8, 37, 39, 46];
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
         {   
    		alert("Digite solo numero");
    		return false;	 
    	 }
    }   
    function guardarpreguntas(){
        var tema = document.getElementById("cbo_tema").value;
        var subtema = document.getElementById("cbo_sub_tema").value;
        var gradodif = document.getElementById("cbo_gradoP").value;
        var confirmar = document.getElementById("cantipreguntas").value;
        var cantidadpreguntas = parseInt(document.getElementById("cantipreguntas").value);
        var totalpreguntas = parseInt(document.getElementById("totalpreguntas").value);
        var preguntas = document.getElementById("numero").value;
        var arrpreguntas = document.getElementById("arrpreguntas").value;
        
        if(subtema == ""){
            subtema = "general";
        }
       if(confirmar != "") {

       	if (preguntas>=cantidadpreguntas) {

           if(totalpreguntas == 0){
                document.getElementById("totalpreguntas").value = cantidadpreguntas+' Preguntas Agregadas';
            }
            else{
                totalpreguntas = totalpreguntas+cantidadpreguntas;
                document.getElementById("totalpreguntas"). value = totalpreguntas+' Preguntas Agregadas';
            }
            
            if(arrpreguntas == ""){
                arreglo = tema+","+subtema+","+gradodif+","+cantidadpreguntas;
                document.getElementById("arrpreguntas").value = arreglo;
            }
            else{
                arreglo = "/"+tema+","+subtema+","+gradodif+","+cantidadpreguntas;
                arrpreguntas = arrpreguntas+arreglo;
                document.getElementById("arrpreguntas").value = arrpreguntas;
            }
            alert("Ha agregado "+cantidadpreguntas+" preguntas");
            document.getElementById("cantipreguntas").value= "";
        }
        else{ alert("Por ingrese un número de preguntas aleatorias valido"); }
       }
       else{ alert("Por ingrese un número de preguntas aleatorias valido"); }
    }

    function ver(estado)
	{
	var nca = new Object();
	nca = document.getElementById("nombre")

		if (estado == 'visible')
		{
			//Propiedades capa absoluta
			caw = nca.offsetWidth
			//*************************

			//Propiedades capa relativa
			crpx = ancla.offsetLeft;
			crpy = ancla.offsetTop;
			crh = ancla.offsetHeight;
			crw = ancla.offsetWidth
			//*************************
			//alert(crw)
			with (nca.style)
			{
				left = crpx + crw;
				top = crpy //+ crh
				visibility = estado;
			}
		}else nca.style.visibility = estado;
	}    

</script>
</head>
<body onLoad="javascript:comprobarNota(0)">
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
	      	<td valign="top" class="tdGeneral">
            	<table class="tablaMarquesina">
					<tr>
<?	 				
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($mat) = mysql_fetch_row($resultado); 
		
		if($_SESSION['idtipousuario']==2){
?>	  
		                <td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($mat))?></a><a> -> </a><a href="verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a><a> -> Ingresar Quiz</a> </td>
						<?
		}
		if($_SESSION['idtipousuario']==5){
						?>
						<td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($mat))?></a><a> -> </a><a href="verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a><a> -> Ingresar Previo</a> </td>
            		   <?
		}
		?>         		
            		
            		</tr>
            </table>&nbsp;
<?
	//list($idgrupo, $grupo) = explode("-", $_POST["hid_grupo"]);
	list($idtipoprevio, $tipoprevio) = explode('-',$_POST["cbo_tipoprevio"]);	
		
	list ($idnota, $nota) = explode("-", $_POST["hid_nota"]);

	/*$sql = "SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evagrupo 
			WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'";
	//print $sql."<p>";
	$resultado = $baseDatos->ConsultarBD($sql);
	$i=0;
	while ( list($id) = mysql_fetch_row($resultado) )
	{
		$veva[$i] = $id;
		$i++;
	}
	if ( count($veva)>0 )
		$ideva_ex = implode(',', $veva);
	else $ideva_ex = 0;*/
	//print $ideva_ex."<p>";	
	
	/*$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre FROM mei_evaluacion, mei_evagrupo
			WHERE mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo='".$idgrupo."' AND 
			mei_evaluacion.tipoevaluacion='1' AND mei_evaluacion.idevaluacion NOT IN (".$ideva_ex.")";*/
	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval 
			FROM mei_evaluacion, mei_evavirgrupo WHERE mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion 
			AND mei_evavirgrupo.idvirgrupo=".$idgrupo." AND mei_evaluacion.tipoevaluacion=2
			AND mei_evaluacion.idevaluacion NOT IN (".$idact_ex.")";
	}
	else
	{
		$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval 
			FROM mei_evaluacion, mei_evagrupo WHERE mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion 
			AND mei_evagrupo.idgrupo=".$idgrupo." AND mei_evaluacion.tipoevaluacion=2
			AND mei_evaluacion.idevaluacion NOT IN (".$idact_ex.")";
	}
	//print $sql."<p>";
	$resultado = $baseDatos->ConsultarBD($sql);	
				
	if ( mysql_num_rows($resultado) > 0 )
	{//IF RESULTADO
		$sql = "SELECT * FROM mei_tiposubgrupo";
		$resultado2 = $baseDatos->ConsultarBD($sql);		
?>
<form enctype="multipart/form-data" name="frm_ingresarEvaluacion" method="post" action="">

<div id="nombre" align="center" style="background-color:#FFFFFF; BORDER-RIGHT: 2px ridge;
	BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge;
	WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute; width:450; height:auto;">

	<div align="center" style="height:10px" class="trTitulo">Adjuntar:</div>
	  <table class="tablaPrincipal" width="417" height="auto">
        <tr class="trInformacion">
          <td width="20%">Archivo 1: </td>
          <td width="80%">
          	<input name="fil_archivo1" type="file" size="40"></td>
        </tr>
         <tr  class="trListaOscuro">
          <td>Archivo 2: </td>
          <td><input name="fil_archivo2" type="file" size="40"></td>
        </tr>
        <tr class="trInformacion">
          <td>Archivo 3:</td>
          <td><input name="fil_archivo3" type="file" size="40"></td>
        </tr>
        <tr  class="trListaOscuro">
          <td>Archivo 4:</td>
          <td><input name="fil_archivo4" type="file" size="40"></td>
        </tr>
        <tr  class="trInformacion">
          <td>Archivo 5:</td>
          <td><input name="fil_archivo5" type="file" size="40"></td>
        </tr>
        <tr  height="10px">
        <td colspan="2" align="center" >	
	  <a href="javascript:ver('hidden')" class="link" ><h2>Cargar Archivos</h2></a>
    	</td>
        </tr>
      </table>
	
</div>

  <table width="100%" class="tablaGeneral">
    <tr class="trSubTitulo">
      <td width="20%" align="left" valign="middle"><b>Autor:</b></td>
      <td width="80%" colspan="3"><input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_POST["hid_materia"]?>">
								  <input name="hid_tipoprevio" type="hidden" id="hid_tipoprevio" value="<?=$idtipoprevio?>">
          <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Grupo:</b></td>
      <td><?=$grupo?><input name="hid_grupo" type="hidden" id="hid_grupo" value="<?=$idgrupo?>"></td>
      <td><strong>Subgrupo:</strong></td>
      <td><select name="cbo_subgrupo" id="cbo_subgrupo">
      	<?if($_SESSION['idtipousuario']==2){?>
        <option value="NULL">Quiz No Grupal</option>
        <?}?>
        <?if($_SESSION['idtipousuario']==5){?>
        <option value="NULL">Previo No Grupal</option>
        <?}?>
        <?
							while ( list($idtipo, $subgrupo) = mysql_fetch_row($resultado2) )
							{
								$pre="Quiz No Grupal";
								print "<option class='link' value='$idtipo' ";
								if ( $idtipo == $_POST["hid_subgrupo"] )
									print "selected>";
								else print ">";
								print "$subgrupo</option>";
							}
						?>
      </select></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Nota a la que pertenece:</b> </td>
      <td colspan="3"><select name="cbo_nota" onChange="javascript:comprobarNota(this.value)">
          <?
									while ( list($ideva, $nomeva, $val_eval) = mysql_fetch_row($resultado) )
									{//WHILE RESULTADO
										print "<option class='link' value='$ideva' ";
										if ( $ideva == $_POST["hid_nota"] )
											print "selected>";
										else print ">";
										print "$nomeva --- ".($val_eval*100)."%</option>";
									}//FIN WHILE RESULTADO
							  ?>
      </select></td>
    </tr>
    <tr class="trInformacion">
    	<?if($_SESSION['idtipousuario']==5){?>
      <td align="left" valign="middle"><strong>Valor del Previo:</strong></td>
      <?}?><?if($_SESSION['idtipousuario']==2){?>
      <td align="left" valign="middle"><strong>Valor del Quiz:</strong></td>
      <?}?>
      <td colspan="3">
        <div id="div_cbo_valor" style="float:left">
        <select name="cbo_valor" id="select">
          <?
							for ($i=0;$i<=100;$i=$i+1) 
						   {
								
								print "<option value='".($i/100)."'";								
								if ($i == ($_POST["hid_valor"]*100))
									print "selected>";
								else print ">";
								
								print "$i%</option>";
											   
						   }
						  ?>
        </select>
      </div>
		<div id="div_etiqueta" style="visibility:hidden;"><em>La Calificacion de esta actividad se promediada con las dem&aacute;s actividades asignadas a esta Nota</em></div>	  </td>
    </tr>
    <tr class="trInformacion" >
    	<?if($_SESSION['idtipousuario']==5){?>
      <td align="left" valign="middle"><b>Nombre del Previo:  </b></td>
      <?}?>
      <?if($_SESSION['idtipousuario']==2){?>
      <td align="left" valign="middle"><b>Nombre del Quiz:  </b></td>
      <?}?>
     <td colspan="3"><input name="txt_titulo" type="text" id="txt_titulo" size="80"  onChange="cambiar(this)"></td>
	  
    </tr>
    <tr class="trInformacion">
      <td width="20%" align="left" valign="middle"><b>Fecha Activaci&oacute;n:</b> </td>
      <td width="40%"><? 
						/*if(empty($_POST['hid_fecha']))
							$fecha_a->m_valor='';
						else
							$fecha_a->m_valor=$_POST['hid_fecha'];*/
							$fecha_a->CrearFrmCalendario(); 
						?></td>
      <td width="10%"><b>Hora Activaci&oacute;n:</b> </td>
      <td width="30%" ><select name="cbo_hora_a" id="cbo_hora_a">
          <?
          	$sqlho="SELECT horario FROM mei_grupo WHERE idgrupo='".$idgrupo."'";
			$horar=$baseDatos->ConsultarBD($sqlho);
			list($horario)=mysql_fetch_row($horar);
			$horas=explode(" ", $horario);
          	$horain=explode(":", $horas[2]);
			$horafi=explode(":", $horas[5]);
			$tiempofin=explode(",", $horas[6]);
			$tiempo="pm";
			if(strcmp($tiempo, $horas[3]) === 0){
				$horain[0]=$horain[0]+12;
			}
			if(strcmp($tiempo, $tiempofin[0]) === 0){
				$horafi[0]=$horafi[0]+12;
			}
			
			for ($i=0;$i<=23;$i++)
			{
				
				if($horain[0]==$i){
					if ($i<10) $i = "0".$i;
					print "<option selected value='$i'>$i</option>";
				}else{
					if ($i<10) $i = "0".$i;
				print "<option value='$i'>$i</option>";
				}
			}
		  ?>
        </select>
        :
  <select name="cbo_minuto_a" id="cbo_minuto_a">
    <?
		for ($i=0;$i<=55;$i=$i+5)
		{
			if ($i<10) $i = "0".$i;
			print "<option value='$i'>$i</option>";
		}
	?>
</select></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Fecha Finalizaci&oacute;n:</b> </td>
      <td><? 
								/*if(empty($_POST['hid_fecha']))
									$fecha_f->m_valor='';
								else
									$fecha_f->m_valor=$_POST['hid_fecha'];*/
									$fecha_f->CrearFrmCalendario(); 
								?></td>
      <td><b>Hora Finalizaci&oacute;n:</b> </td>
      <td><select name="cbo_hora_f" id="select2">
          <?
								for ($i=0;$i<=23;$i++)
			{
				
				if($horafi[0]==$i){
					if ($i<10) $i = "0".$i;
					print "<option selected value='$i'>$i</option>";
				}else{
					if ($i<10) $i = "0".$i;
				print "<option value='$i'>$i</option>";
				}
			}
							?>
        </select>
        :
  <select name="cbo_minuto_f" id="select3">
    <?
		for ($i=0;$i<=55;$i=$i+5)
		{
			if ($i<10) $i = "0".$i;
			print "<option value='$i'>$i</option>";
		}
	?>
</select></td>
    </tr>
    <tr class="trInformacion">
      <td height="24" align="left" valign="middle"><b>Estado:</b></td>
      <td colspan="3"><select name="cbo_estado" id="cbo_estado" class="link">
                            <option value="1">Activa</option>
                            <option value="0" selected>Inactiva</option>
                          </select></td>
    </tr>
    <tr class="trInformacion">
      <td height="24" align="left" valign="middle"><b>Comentarios:</b></td>
      <td colspan="3"><?								 
								 if (!empty($_POST["hid_editor"]) ) 
								 	$editor->Value = $_POST["hid_editor"];
								 $editor->crearEditor();
							?></td>
    </tr>
	<tr class="trInformacion">
	<td height="24" align="left" valign="middle"><b> Contraseña: </b> </td>
	<td colspan="3"><input name="txt_clave" type="text" id="txt_clave" size="30" value=""></td> 
    </tr>

    <!--Adjuntar-->
	
	<!--Adjuntar-->
	<tr class="trInformacion">
	<td height="10" align="left" valign="middle"></td>
	<td colspan="3"></td>
    </tr>
	<tr class="trInformacion">
                          <td colspan="4"><div id="ancla" style="position:relative; width:250px">
						  <a style="color: red; font-weight: bold; font-size: 14px;" href="javascript:ver('visible')" class="link"><img src="imagenes/adjuntar.gif" width="16" height="16" border="0">Adjuntar Archivos </a>
						  </div>						  </td>
						</tr>
						
	 <tr class="trInformacion">
          <td width="20%">Archivo 1: </td>
          <td colspan="3" width="80%">
          	<input name="fil_archivo1" type="file" size="40"></td>
        </tr>
         <tr  class="trListaOscuro">
          <td>Archivo 2: </td>
          <td colspan="3"><input name="fil_archivo2" type="file" size="40"></td>
        </tr>
        <tr class="trInformacion">
          <td>Archivo 3:</td>
          <td colspan="3"><input name="fil_archivo3" type="file" size="40"></td>
        </tr>
        <tr  class="trListaOscuro">
          <td>Archivo 4:</td>
          <td colspan="3"><input name="fil_archivo4" type="file" size="40"></td>
        </tr>
        <tr  class="trInformacion">
          <td>Archivo 5:</td>
          <td colspan="3"><input name="fil_archivo5" type="file" size="40"></td>
        </tr>
	<tr class="trInformacion">
	<td height="10" align="left" valign="middle"></td>
	<td colspan="3"></td>
    </tr>
	<!--Fin Adjuntar-->

    <tr  class="trInformacion">
		<td height="24" align="left" valign="middle"><b> Publicar en Calendario </b> </td>
        <td colspan="3"  height="24" align="left" valign="middle">
         <input name="chk_calendario" type="checkbox" id="chk_calendario" value="checkbox" checked>
		</td>
	</tr>

  </table>
  
  <table class="tablaGeneral">
    <tr class="trSubTitulo">
    <?if($_SESSION['idtipousuario']==5){?>
      <td colspan="4">Caracteristicas del Previo: </td>
      <?}?>
      <?if($_SESSION['idtipousuario']==2){?>
      <td colspan="4">Caracteristicas del Quiz: </td>
      <?}?>
    </tr>
    <tr class="trInformacion">
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>N&uacute;mero de Intentos:</b> </td>
      <td align="left" valign="middle"><select name="cbo_intentos" id="cbo_intentos">
	  <option value="1" selected>1</option>
        <?
			for ($i=1;$i<=10;$i++)
			{
				print "<option value='$i'>$i</option>";
			}	
		?>
        </select></td>
      <td align="left" valign="middle"><strong>Disminuir la Nota de cada intento en: </strong></td>
      <td align="left" valign="middle"><input name="cbo_valorIntento" type="text" id="bo_calificacion" size="10" maxlength="5" value="10">
        %</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Forma de Calificar: </strong></td>
      <td align="left" valign="middle"><select name="cbo_calificar" id="cbo_calificar">
	  <?
	  	$sql = "SELECT mei_evamodcalificar.idmodcalificar, mei_evamodcalificar.modcalificar FROM mei_evamodcalificar";
		$resultado = $baseDatos->ConsultarBD($sql);
		
		while ( list($idmod, $mod) = mysql_fetch_row($resultado) )
		{
			print "<option value = '$idmod'>$mod</option>'";
		}
		
	  ?>
      </select></td>
      <?if($_SESSION['idtipousuario']==5){?>
      <td align="left" valign="middle"><b>Tiempo del Previo: </b></td>
      <?}?>
      <?if($_SESSION['idtipousuario']==2){?>
      <td align="left" valign="middle"><b>Tiempo del Quiz: </b></td>
      <?}?>
      <td align="left" valign="middle"><input name="txt_tiempo" type="text" id="txt_tiempo" size="10" value="15">
	  
	  <!--
	  <select name="cbo_tiempo" id="cbo_tiempo">
        <? /*
			for ($i=15;$i<=120;$i=$i+15)
			{
				if ($i<10) $i = "0".$i;
				print "<option value='$i'>$i Minutos</option>";
			}*/
		?>
      </select>--> Minutos	  </td>
    </tr>
    
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Barajar Preguntas:</b></td>
      <td align="left" valign="middle"><select name="cbo_bpreguntas" id="cbo_bpreguntas">
        <option value="1" selected>Si</option>
        <option value="0">No</option>
      </select></td>
      <td align="left" valign="middle"><b>Barajar Respuestas:</b></td>
      <td align="left" valign="middle"><select name="cbo_brespuestas" id="cbo_brespuestas">
        <option value="1" selected>Si</option>
        <option value="0" >No</option>
      </select></td>
    </tr>
    
    <tr class="trInformacion">
      <td height="21"><strong>Mostrar Nota: </strong></td>
      <td><select name="cbo_mnota" id="cbo_mnota">
        <option value="1" selected>Si</option>
        <option value="0" >No</option>
      </select>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      <tr class="trInformacion">
                          <td align="left" valign="middle"><h2 style="color: #ec7000"><strong>Otorgar Incentivo </h2></strong></td>
                          <td><strong>Otorgar Curva Automatica</strong> 
                          	<input type="checkbox" value="curva" name="incentivo_curva"  onclick="incentivo1()" />
                          	</td>
                          <td><strong>Nota Adicional</strong> 
                          	<input type="checkbox"  name="incentivo_nota"  checked onclick="incentivo2()" value="nota"/>
                          	</td>
                          <td>
                          	<strong>Nota</strong> 
	                          	<select  name="incentivo_valor"  >
	                          		<?
	                          			$incentivo=0.0;
	                          		for ($i=0.0;$i<=5.0;$i=$i+0.1)
										{
											$x=$i;
											print "<option value='$x'";
											if ($incentivo==$x) print " selected>";
											else print ">";
											print $x."</option>";
										}?>
	                          		
	                          	</select> 
                          	</td>
                        </tr>	
    <tr class="trInformacion">
      <td height="21">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>

<br/>
  <table class="tablaGeneral">
    <tr class="trSubTitulo">
      <td colspan="4">Preguntas Aleatoria </td>
    </tr>
    <tr class="trInformacion">
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Tema :</strong></td>
      <td align="left" valign="middle">
        <select name="cbo_tema" id="cbo_tema" onchange="cargarsubtema(this.value); this.selectedindex = -1" >
            <option value=""></option>
            <?
    	  	$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
    				WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.tipo=1";
    		$resultado = $baseDatos->ConsultarBD($sql);
    		while ( list($codtema, $nomtema) = mysql_fetch_row($resultado) )		
    		{	
    			print "<option value='$codtema' ";
    			
    			if ("$codtema-$nomtema" == $_SESSION['tema'])
    					print "selected";
    			print  ">$nomtema</option>";			
    		}
    		mysql_free_result($resultado);
    	  ?>
        </select>
        <div id="subtemacarga"></div>      
      </td>
      <td align="left" valign="middle"><strong>Subtema :</strong></td>
      <td align="left" valign="middle">
        <div id="subtema"></div>     
      </td>            
    </tr> 
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Grado : </strong></td>
      <td align="left" valign="middle">
        <select name="cbo_gradoP" id="cbo_gradoP" onchange="cantpreg()">
            <option value=""></option>
          <?
    	  	$sql = "SELECT * FROM mei_evagradopregunta";
    		$resultado = $baseDatos->ConsultarBD($sql);
    		while ( list($codgrado, $nomgrado) = mysql_fetch_row($resultado) )		
    		{		
    			print "<option value='$codgrado-$nomgrado'";
    			if ( ( $codgrado == $_POST["hid_grado"] ) or ( $codgrado = $_POST["cbo_gradoP"] ) )
    				print "selected";
    				
    			print ">$nomgrado</option>";			
    		}
    		mysql_free_result($resultado);
    	  ?>
        </select>      
      </td>
      
      <td align="left" valign="middle"><strong>Cantidad : </strong></td>
      <td align="left" valign="middle">
        <input style="width: 50px;" type="text" id="cantipreguntas" name="cantipreguntas" onkeypress="return soloNumeros(event)" />
        <!-- <input type="button" id="cantpregbtn" name="cantpregbtn" value="M&aacute;ximo" /> -->
        <div id="cantpreg"></div>
        <input type="text" id="numero" style="display: none"></input>
        
      </td>
    </tr>
    <tr class="trInformacion">
    <td colspan="4" align="left" valign="middle" style="text-align: center;">
    <input type="text" style="background-color:transparent; color: red; font-weight: bold; text-align: center; border-width: 0;" readonly="readonly" value="0 Preguntas Agregadas" name="totalpreguntas" id="totalpreguntas" />
    </td>
    </tr> 
     <tr class="trInformacion">
    	<td colspan="4" height="10px"></td>
    </tr>  
    <tr class="trInformacion">
        <td colspan="4" align="left" valign="middle" style="text-align: center;">
            <strong>Nota: Si excede la cantidad m&aacute;xima de preguntas se seleccionar&aacute;n el m&aacute;ximo de estas.</strong>
        </td>
    </tr>     
    <tr class="trInformacion">
        <td colspan="4" align="left" valign="middle" style="text-align: center;"><input type="button" value="Agregar Preguntas" onclick="guardarpreguntas()" /></td>
        
        <input type="hidden" value="" name="arrpreguntas" id="arrpreguntas" />
    </tr>
             
  </table>
  <br />  
  
  <table class="tablaMarquesina">
    <tr align="center" valign="middle">
      <td><input type="button" name="btn_guardarEvaluacion" value="Generar Evaluaci&oacute;n Manual" onClick="javascript:guardar()"></td>
      <td><input type="button" name="btn_guardarEvaluacionAutomatica" id="btn_guardarEvaluacionAutomatica" value="Generar Evaluaci&oacute;n Automatica" onClick="javascript:generarautomatica()"></td>
      <td><input type="button" name="btn_cancelar" value="Cancelar" 
							  onClick="javascript:cancelar('<?=$_POST["hid_materia"]?>', '<?=$materia?>')"></td>
    </tr>
  </table>
</form>
<?
		}//FIN IF RESULTADO
		else
		{//else sin nota evaluacion
 ?>
&nbsp;
<table class="tablaGeneral" width="200">
  <tr class="trSubTitulo">
    <td colspan="2" align="center" valign="middle">No se puede Crear el Quiz </td>
  </tr>
  <tr class="trInformacion">
    <td colspan="2" align="left" valign="middle"><p><strong>Posibles Causas:</strong></p>
        <ul>
          <li>No existen Notas de Actividades definidas para el grupo <strong><?=$grupo?></strong></li>
          <br>
          <li>El valor de las actividades asignadas a las notas Tipo Actividad del Grupo <strong><?=$grupo?></strong> a llegado al 100% </li>
        </ul>
      Para crear Notas haga click <a href="../notas/index.php?idmateria=<?=$_POST["hid_materia"]?>&materia=<?=$_GET['materia']?>" class="link"><strong><em>Aqui</em></strong></a> </td>
  </tr>
  <tr class="trInformacion">
    <td colspan="2" align="center" valign="middle">&nbsp;</td>
  </tr>
</table>
<?
	}//fin else sin nota evaluacion
?>&nbsp;
  <table class="tablaMarquesina" width="200">
  	<?if($_SESSION['idtipousuario']==2){?>
	<tr align="center" valign="middle">
		<td><a href="verEvaluacion.php?idmateria=<?=$_POST["hid_materia"]?>&materia=<?=$_GET['materia']?>" class="link">Volver</a></td>
	</tr>
	<?}
	if($_SESSION['idtipousuario']==5){
	?>
	<tr align="center" valign="middle">
		<td><a href="ingresarEvaluacion.php?idmateria=<?=$_POST["hid_materia"]?>&materia=<?=$_GET['materia']?>" class="link">Volver</a></td>
	</tr>
	<?
	}
	?>
  </table>
          </td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>