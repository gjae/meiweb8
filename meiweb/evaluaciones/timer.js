// $Id: timer.js,v 1.2 2004/06/03 12:45:38 julmis Exp $
//
// QuizTimer
// Provides a counter that keeps track how much
// time user have left to check in started quiz.
//
function countdown_clock() {
    var timeout_id = null;
    quizTimerValue = quizTimerValue - 1;

    if(quizTimerValue <= 0) {
        clearTimeout(timeout_id);
        alert(timesup);
        document.forms[0].submit();
    }

    now = quizTimerValue;
    var hours = Math.floor( now / 3600 );
    parseInt(hours);
    now = now - (hours * 3600);
    var minutes = Math.floor(now / 60);
    parseInt(minutes);
    now = now - (minutes * 60);
    var seconds = now;
    parseInt(seconds);

    var t = "" + hours;
    t += ((minutes < 10) ? ":0" : ":") + minutes;
    t += ((seconds < 10) ? ":0" : ":") + seconds;
    window.status = t.toString();

    if(hours == 0 && minutes == 0 && seconds <= 15) {
        //go from fff0f0 to ffe0e0 to ffd0d0...ff2020, ff1010, ff0000 in 15 steps
        var hexascii = "0123456789ABCDEF";
        var col = 'ff' + hexascii.charAt(seconds) + '0' + hexascii.charAt(seconds) + 0;
        changecolor(col);
    }
    document.forms['frm_previo'].time.value = t.toString();
    timeout_id = setTimeout("countdown_clock()", 1000);
}

function countup_clock() {
    var timeout_id = null;
    quizTimeValue = quizTimeValue + 1;

   

    now = quizTimeValue;
    var hours = Math.floor( now / 3600 );
    parseInt(hours);
    now = now - (hours * 3600);
    var minutes = Math.floor(now / 60);
    parseInt(minutes);
    now = now - (minutes * 60);
    var seconds = now;
    parseInt(seconds);

    var t = "" + hours;
    t += ((minutes < 10) ? ":0" : ":") + minutes;
    t += ((seconds < 10) ? ":0" : ":") + seconds;
    window.status = t.toString();

    
    document.forms['frm_previo'].tim.value = t.toString();
    timeout_id = setTimeout("countup_clock()", 1000);
}

function clock(){
/*var time = new Date();
var hr = time.getHours();
var min = time.getMinutes();
var sec = time.getSeconds();
var ampm = " pm ";
if (hr < 12){
ampm = " am ";
}
if (hr > 12){
hr -= 12;
}
if (hr < 10){
hr = " " + hr;
}
if (min < 10){
min = "0" + min;
}
if (sec < 10){
sec = "0" + sec;

document.forms['frm_previo'].actim.value = hr + ":" + min + ":" + sec + ampm;
setTimeout("clock()", 1000);

}*/

 time=time +1;

   

    now = time;
    var hours = Math.floor( now / 3600 );
    parseInt(hours);
    now = now - (hours * 3600);
    var minutes = Math.floor(now / 60);
    parseInt(minutes);
    now = now - (minutes * 60);
    var seconds = now;
    parseInt(seconds);
    var ampm = " pm ";
if (hours < 12){
ampm = " am ";
}

    var t = "" + hours;
    t += ((minutes < 10) ? ":0" : ":") + minutes;
    t += ((seconds < 10) ? ":0" : ":") + seconds;
    window.status = t.toString();

    
    document.forms['frm_previo'].actim.value = t.toString() + ampm;
    setTimeout("clock()", 1000);



}

function actualtime() {
    var timeout_id = null;
    actualtim = actualtim + 1;

   

    now = actualtim;
    var hours = Math.floor( now / 3600 );
    parseInt(hours);
    now = now - (hours * 3600);
    var minutes = Math.floor(now / 60);
    parseInt(minutes);
    now = now - (minutes * 60);
    var seconds = now;
    parseInt(seconds);

    var t = "" + hours;
    t += ((minutes < 10) ? ":0" : ":") + minutes;
    t += ((seconds < 10) ? ":0" : ":") + seconds;
    window.status = t.toString();

    
    document.forms['frm_previo'].actim.value = t.toString();
    timeout_id = setTimeout("actualtime()", 1000);
}

function movecounter() {

    var pos;

    if (window.innerHeight) {
        pos = window.pageYOffset;
    } else if (document.documentElement && document.documentElement.scrollTop) {
        pos = document.documentElement.scrollTop;
    } else if (document.body) {
          pos = document.body.scrollTop;
    }

    if (pos < theTop) {
        pos = theTop;
    } else {
        pos += 50;
    }
    if (pos == old) {
        this.style.top = pos;
    }
    old = pos;
    temp = setTimeout('movecounter()',100);
}

function getObjectById (name) {

    if (document.getElementById) {
        this.obj = document.getElementById(name);
        this.style = document.getElementById(name).style;
    } else if (document.all) {
        this.obj = document.all[name];
        this.style = document.all[name].style;
    } else if (document.layers) {
        this.obj = document.layers[name];
        this.style = document.layers[name];
    }
}
