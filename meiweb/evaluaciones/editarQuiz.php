<?
	include_once ('../baseDatos/BD.class.php');  
    include_once ('../librerias/estandar.lib.php');
    include_once ('../librerias/vistas.lib.php');
    include_once ("../editor/fckeditor.php") ;
    include_once ('../calendario/FrmCalendario.class.php');
	include_once ('evaclass.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../menu1/Menu1.class.php');	
if(comprobarSession())
{
/*  Nombre  : vertaller.php
	Autor   : Claudia
	Fecha   : 06/03/2006
	Detalle :Ingresar Taller
	Versión :  
*/ $baseDatos=new BD();
    
	// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , [Barra de herramientas] ,
	$editor=new FCKeditor('edt_comentario' , '100%' , '200' , 'barraCorreo' , '' ) ;
	$fecha_a=new FrmCalendario('frm_ingresarTaller','txt_fecha_a','formulario','false','');	
	$fecha_f=new FrmCalendario('frm_ingresarTaller','txt_fecha_f','formulario','false','');	

//************************************************************************
if ( !empty($_GET["idmateria"]) )
{
	$idmateria = $_GET["idmateria"];
	$materia = $_GET["materia"];
}
elseif ( !empty($_POST["hid_materia"]) ) 
	list ($idmateria, $materia) = explode("*", $_POST["hid_materia"]);
//***********************************************************************
	if($_GET['esp']!='reposicion')
	{
		if ($_SESSION['idtipousuario']==5) 
		{
			$sql = "SELECT mei_virgrupo.nombre, mei_evavirgrupo.idvirgrupo, mei_evaprevio.idevaluacion, mei_evaprevio.idautor, 
				mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, mei_evaprevio.fechafinalizacion, mei_evaprevio.comentario,
				mei_evaprevio.tiempo, mei_evaprevio.barajarpreg, mei_evaprevio.barajarresp, mei_evaprevio.valor, mei_evaprevio.incentivo, mei_evaprevio.curva,
				mei_evaprevio.estado, mei_evaprevio.intentos, mei_evaprevio.valorIntento, mei_evaprevio.idmodCalificar,
				mei_evaprevio.mostrarnota, mei_evaprevio.idtipoprevio, mei_tipoprevio.tipoprevio, mei_evaprevio.idtiposubgrupo, mei_evaprevio.clave 
				FROM mei_evaprevio, mei_evaluacion,mei_evavirgrupo, mei_virgrupo, mei_tipoprevio WHERE mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND 
				mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evavirgrupo.idevaluacion = mei_evaluacion.idevaluacion 
				AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo AND mei_evaprevio.idprevio = ".$_GET["idprevio"];
		}
		else
		{
			$sql = "SELECT mei_grupo.nombre, mei_evagrupo.idgrupo, mei_evaprevio.idevaluacion, mei_evaprevio.idautor, 
				mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, mei_evaprevio.fechafinalizacion, mei_evaprevio.comentario,
				mei_evaprevio.tiempo, mei_evaprevio.barajarpreg, mei_evaprevio.barajarresp, mei_evaprevio.valor,mei_evaprevio.incentivo, mei_evaprevio.curva,
				mei_evaprevio.estado, mei_evaprevio.intentos, mei_evaprevio.valorIntento, mei_evaprevio.idmodCalificar,
				mei_evaprevio.mostrarnota, mei_evaprevio.idtipoprevio, mei_tipoprevio.tipoprevio, mei_evaprevio.idtiposubgrupo, mei_evaprevio.clave 
				FROM mei_evaprevio, mei_evaluacion,mei_evagrupo, mei_grupo, mei_tipoprevio WHERE mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND 
				mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evagrupo.idevaluacion = mei_evaluacion.idevaluacion 
				AND mei_evagrupo.idgrupo = mei_grupo.idgrupo AND mei_evaprevio.idprevio = ".$_GET["idprevio"];
		}
	}
	else
	{
		if ($_SESSION['idtipousuario']==5) 
		{
			$sql = "SELECT mei_virgrupo.nombre, mei_evavirgrupo.idvirgrupo, mei_evaquiz.idevaluacion, mei_evaquiz.idautor, 
			mei_evaquiz.titulo, mei_evaquiz.fechaactivacion, mei_evaquiz.fechafinalizacion, mei_evaquiz.comentario,
			mei_evaquiz.tiempo, mei_evaquiz.barajarpreg, mei_evaquiz.barajarresp, mei_evaprevio.valor,mei_evaprevio.incentivo,mei_evaprevio.curva,
			mei_evaquiz.estado, mei_evaquiz.intentos, mei_evaquiz.valorIntento, mei_evaquiz.idmodCalificar,
			mei_evaquiz.mostrarnota, mei_evaquiz.idtipoprevio, mei_tipoprevio.tipoprevio, mei_evaquiz.idtiposubgrupo, mei_evaprevio.clave FROM mei_evaquiz, 
			mei_evavirgrupo, mei_virgrupo, mei_tipoprevio WHERE mei_evaquiz.idtipoprevio = mei_tipoprevio.idtipoprevio AND 
			mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo AND mei_evaquiz.idprevio = ".$_GET["idprevio"];
		}
		else
		{
			$sql = "SELECT mei_grupo.nombre, mei_evagrupo.idgrupo, mei_evaquiz.idevaluacion, mei_evaquiz.idautor, 
			mei_evaquiz.titulo, mei_evaquiz.fechaactivacion, mei_evaquiz.fechafinalizacion, mei_evaquiz.comentario,
			mei_evaquiz.tiempo, mei_evaquiz.barajarpreg, mei_evaquiz.barajarresp, mei_evaprevio.valor,mei_evaprevio.incentivo,mei_evaprevio.curva,
			mei_evaquiz.estado, mei_evaquiz.intentos, mei_evaquiz.valorIntento, mei_evaquiz.idmodCalificar,
			mei_evaquiz.mostrarnota, mei_evaquiz.idtipoprevio, mei_tipoprevio.tipoprevio, mei_evaquiz.idtiposubgrupo, mei_evaprevio.clave FROM mei_evaquiz, 
			mei_evagrupo, mei_grupo, mei_tipoprevio WHERE mei_evaquiz.idtipoprevio = mei_tipoprevio.idtipoprevio AND 
			mei_evagrupo.idgrupo = mei_grupo.idgrupo AND mei_evaquiz.idprevio = ".$_GET["idprevio"];
		}
	}
	
	$resultado = $baseDatos->ConsultarBD($sql);
	//print $sql;
	
	list ($grupo,$idgrupo,$idnota,$idautor,$titulo,$a_fecha,$f_fecha,$comentario,$tiempo,$bpreg,$bresp,$porcentaje_valor,$incentivo,$curva,$estado,$intentos,$valorIntento,$mcalificar,$mnota,$idtipoprevio,$tipoprevio,$idtiposubgrupo, $clave) = mysql_fetch_row($resultado);

	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion, mei_evavirgrupo WHERE 
			mei_evavirgrupo.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.tipoevaluacion = 2 AND
			mei_evaluacion.promediar = 1 AND mei_evavirgrupo.idvirgrupo='".$idgrupo."'";
	}
	else
	{
		$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion, mei_evagrupo WHERE 
			mei_evagrupo.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.tipoevaluacion = 2 AND
			mei_evaluacion.promediar = 1 AND mei_evagrupo.idgrupo='".$idgrupo."'";
	}

	$resgrupo = $baseDatos->ConsultarBD($sql);

	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_evaprevio.idevaluacion, SUM(mei_evaprevio.valor) FROM mei_evaprevio, mei_evaluacion, mei_evagrupo 
			WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaprevio.idtipoprevio = 2 AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."' AND
			mei_evaprevio.idprevio <> '".$_GET["idprevio"]."' GROUP BY mei_evaprevio.idevaluacion";
	}
	else
	{
		$sql = "SELECT mei_evaprevio.idevaluacion, SUM(mei_evaprevio.valor) FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaprevio.idtipoprevio = 2 AND 
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."' AND
			mei_evaprevio.idprevio <> '".$_GET["idprevio"]."' GROUP BY mei_evaprevio.idevaluacion";
	}
	//print $sql."<p>";			
	$resquiz = $baseDatos->ConsultarBD($sql);
	
	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_actividad.idevaluacion, SUM(mei_actividad.valor) FROM mei_actividad, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."'
			GROUP BY mei_actividad.idevaluacion";
	}
	else
	{
		$sql = "SELECT mei_actividad.idevaluacion, SUM(mei_actividad.valor) FROM mei_actividad, mei_evaluacion, mei_evagrupo 
			WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'
			GROUP BY mei_actividad.idevaluacion";
	}
	//print $sql."<p>";
	$resultado = $baseDatos->ConsultarBD($sql);

	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_foro.idevaluacion, SUM(mei_foro.valor) FROM mei_foro, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_foro.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."'
			GROUP BY mei_foro.idevaluacion";
	}
	else
	{
		$sql = "SELECT mei_foro.idevaluacion, SUM(mei_foro.valor) FROM mei_foro, mei_evaluacion, mei_evagrupo 
			WHERE mei_foro.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'
			GROUP BY mei_foro.idevaluacion";
	}
	//print $sql."<p>";
	
	$resforo = $baseDatos->ConsultarBD($sql);
	
	$i=0;
	?>
	<script language="javascript">
		function val_porcentaje()
		{
			var notaValor = new Array(<?=mysql_num_rows($resultado)?>)
			idnota = document.frm_ingresarTaller.cbo_nota.value
			valor = document.frm_ingresarTaller.cbo_valor.value
			//alert(document.frm_ingresarTaller.hid_valor.value)
	<?
	while ( list($id, $avalor) = mysql_fetch_row($resultado) )
	{
		if ( round($avalor,2)>=1 )
			$inact[$i] = $id;
		else $vact[$id] = $avalor;
			
	?>
			notaValor[<?=$id?>] = <?=round($avalor,2)?>;
	<?		
		$i++;
	}
	?>
	
	<?
	while ( list($id, $avalor) = mysql_fetch_row($resforo) )
	{
		$vact[$id] += round($avalor,2);
		if ( round($vact[$id],2)>=1 )
			$inact[$i] = $id;
		//else $vact[$i] = $avalor;
	?>
			if (notaValor[<?=$id?>]>0)
				notaValor[<?=$id?>] = notaValor[<?=$id?>] + <?=round($avalor,2)?>;
			else notaValor[<?=$id?>] = <?=round($avalor,2)?>;
	<?		
		$i++;
	}
	?>	
	<?
	while ( list($id, $avalor) = mysql_fetch_row($resquiz) )
	{
		$vact[$id] += round($avalor,2);
		if ( round($vact[$id],2)>=1 )
			$inact[$i] = $id;
		//else $vact[$i] = $avalor;
	?>
			if (notaValor[<?=$id?>]>0)
				notaValor[<?=$id?>] = notaValor[<?=$id?>] + <?=round($avalor,2)?>;
			else notaValor[<?=$id?>] = <?=round($avalor,2)?>;
	<?		
		$i++;
	}
	?>	
			if (notaValor[idnota]>0)
				total = parseFloat(notaValor[idnota])+parseFloat(valor);
			else total = parseFloat(valor);
			//alert(total)
			//document.frm_ingresarTaller.hid_valor.value = total;
			if (total>1)
			{
				//alert('El porcentajes de las Actividades asiganadas a la Nota Seleccionada excede en 100%')
				return false
			}
			else return true
		}
		
		function comprobarNota(idnota)
		{
			var notaProm = new Array(<?=mysql_num_rows($resgrupo)?>)
			
			if (idnota==0) idnota = document.frm_ingresarTaller.cbo_nota.value
			//alert(idnota)
		<?
			while( list($id) = mysql_fetch_row($resgrupo) )
			{
		?>
				notaProm[<?=$id?>] = 1
		<?
			}
		?>
			if (notaProm[idnota]==1)
			{
				document.frm_ingresarTaller.cbo_valor.disabled = true
				//document.frm_ingresarTaller.cbo_valor.style.visibility = 'hidden'
				document.getElementById('div_etiqueta').style.visibility = 'visible'
			}
			else
			{
				document.frm_ingresarTaller.cbo_valor.disabled = false
				//document.frm_ingresarTaller.cbo_valor.style.visibility = 'visible'
				document.getElementById('div_etiqueta').style.visibility = 'hidden'
			}
		}	
		
		function cambiar(tec){
			for (var i=0; i < 20; i++) {
			  tec.value = tec.value.replace("-", "_");
			};
	
}	
			
</script>		
	<?	
		if ( count($inact)>0 )
			$idact_ex = implode(',', $inact);
		else $idact_ex = 0;
		/*print "lista: ".$idact_ex."<p>";
		print "count(vact): ".count($inact)."<p>";*/
		//************************************************
	?>	

<head>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Documento sin t&iacute;tulo</title>
<script>
function incentivo1 () {
 if(!document.frm_ingresarTaller.incentivo_curva.checked){
 	document.frm_ingresarTaller.incentivo_nota.checked=true;
 	document.frm_ingresarTaller.incentivo_valor.disabled=false;
 }else{
 	document.frm_ingresarTaller.incentivo_nota.checked=false;
 	document.frm_ingresarTaller.incentivo_valor.disabled=true;
 }
}
function incentivo2 () {
 if(document.frm_ingresarTaller.incentivo_nota.checked){
 	document.frm_ingresarTaller.incentivo_curva.checked=false;
 	document.frm_ingresarTaller.incentivo_valor.disabled=false;
 }else{
 	document.frm_ingresarTaller.incentivo_curva.checked=true;
 	document.frm_ingresarTaller.incentivo_valor.disabled=true;
 }
}
function validarEntero(){ 
      //intento convertir a entero. 
     //si era un entero no le afecta, si no lo era lo intenta convertir 
     valor = parseInt(document.frm_ingresarTaller.txt_tiempo.value) 
     valor2 = parseInt(document.frm_ingresarTaller.cbo_valorIntento.value) 
      //Compruebo si es un valor numérico 
      if ( ((isNaN(valor)) || (valor<1)) || ( (isNaN(valor2)) || (valor2<0) ) ) { 
            //entonces (no es numero) devuelvo el valor cadena vacia 
            return false  
      }else{ 
            //En caso contrario (Si era un número) devuelvo el valor 
            return true 
      } 
}


	function validar()
	{
		if ( document.frm_ingresarTaller.txt_titulo.value!="" && document.frm_ingresarTaller.txt_tiempo.value!="" )
		{	
			if ( validarEntero() )
				return true;
			else alert("El campo 'Tiempo de la Evaluación' debe ser un número mayor o igual a 1. El campo 'Disminuir la Nota de cada intento' debe ser un número mayor a 0")
		}
		else 
			{
				alert('Debe llenar todos los campos')
				return false;
			}
	}
	
	function verHora()
	{

		var fecha=frm_ingresarTaller.txt_fecha_a.value.split("-");
		if(fecha[1]<10){ fecha[1]='0'+fecha[1];}
		if(fecha[2]<10){ fecha[2]='0'+fecha[2];}
		fecha=fecha[0]+"-"+fecha[1]+"-"+fecha[2];
		
		var fechaC=frm_ingresarTaller.txt_fecha_f.value.split("-");
		if(fechaC[1]<10){ fechaC[1]='0'+fechaC[1];}
		if(fechaC[2]<10){ fechaC[2]='0'+fechaC[2];}
		fechaC=fechaC[0]+"-"+fechaC[1]+"-"+fechaC[2];
		
		// se pasan las fechas a formato aaaa-mm-dd hh:mm:ss
		var hora_a = document.frm_ingresarTaller.cbo_hora_a.value;
		var min_a= document.frm_ingresarTaller.cbo_minuto_a.value;
		var hora_f = document.frm_ingresarTaller.cbo_hora_f.value; 
		var min_f= document.frm_ingresarTaller.cbo_minuto_f.value;
		
			fecha=fecha+" "+hora_a+":"+min_a+":00";
			fechaC=fechaC+" "+hora_f+":"+min_f+":00";
			
			if(fecha<fechaC){
				
					return true;
				}
			else{
				alert("la fecha de Activacion debe ser menor q la fecha de Finalizacion");
				return false;
			}
	}
	
	/*function verHora()
	{
	
		var fecha=frm_ingresarTaller.txt_fecha_a.value.split("-");
		var fechaC=frm_ingresarTaller.txt_fecha_f.value.split("-");
		var hora_a = document.frm_ingresarTaller.cbo_hora_a.value + document.frm_ingresarTaller.cbo_minuto_a.value
		var hora_f = document.frm_ingresarTaller.cbo_hora_f.value + document.frm_ingresarTaller.cbo_minuto_f.value
		
		if(fecha[2] < fechaC[2])
		//if(fecha[0] <= fechaC[0])
		{
			return true
		}
		else
		{
			if(fecha[1] < fechaC[1])
			{
				return true
			}
			else
			{
				//if(fecha[2] < fechaC[2])
				if(fecha[0] < fechaC[0])
				{
					return true
				}
				else
				{
					if (hora_f > hora_a)
					{
						return true
					}
					else return false
				}						
			}							
		}		
	}	
*/
	function guardartaller()
	{ 
		if (validar())
		{
				//alert('asfsadfasdf')
			if (verHora())
			{
				document.frm_ingresarTaller.submit();
				//alert('good')
			}
			else alert('La Hora de Finalizacion debe ser mayor que la Hora de Activación')
		}
	}		
	function guardartallerquiz()
	{ 
		if (validar())
		{
				//alert('asfsadfasdf')
			if (verHora())
			{
				document.frm_ingresarTaller.action="guardarEvaluacion.php?modo=E&esp=<?=$_GET['esp']?>&idgrupo=<?=$_GET['idgrupo']?>&materia=<?=$_GET['materia']?>&idmateria=<?=$_GET['idmateria']?>&sologuardar=1&ordengrupo=<?=$idgrupo."-".$grupo?>";
				document.frm_ingresarTaller.submit();
				//alert('good')
			}
			else alert('La Hora de Finalizacion debe ser mayor que la Hora de Activación')
		}
	}	
	function cancelar(id, mat)
			{
				document.frm_ingresarTaller.action="verDtlleEvaluacion.php?idprevio="+id+"&idmateria="+mat+"&materia=<?=$_GET['materia']?>&cbo_orden=<?=$idgrupo."-".$grupo?>";
				document.frm_ingresarTaller.submit();
			}	
	function guardar()
	{
			document.frm_confirmar.action="guardarEvaluacion.php?modo=E&esp=<?=$_GET['esp']?>&materia=<?=$_GET['materia']?>&idmateria=<?=$_GET['idmateria']?>";
			document.frm_confirmar.submit();
	}
	function editar(idmateria,idactividad)
	{
		document.frm_confirmar.action="editarActividad.php?idmateria="+idmateria+"&idactividad="+idactividad+"&materia=<?=$_GET['materia']?>";
		document.frm_confirmar.submit();
	}

	function adjuntar()
		{
			//alert("adjuntar");
				document.frm_ingresarTaller.action="adjuntarArchivosPre.php?idprevio=<?=$_GET['idprevio']?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET['idgrupo']?>&esp=''materia=<?=$_GET["materia"]?>";
				document.frm_ingresarTaller.submit();
		}
	
	function enviar()
	{
		var contBandera=0;	
		for(i=0;i<document.frm_2.elements.length;i++)
		{
			if(document.frm_2.elements[i].checked==true && document.frm_2.elements[i].id=="notas")
			contBandera++;
		}
		
		if(contBandera == 0)
		{
			alert('Debe seleccionar por lo menos una nota');
			return false
		}
		else
		{
			return true
		}
	}
</script>
<script type="text/javascript" src="jquery.js" ></script>
<script type="text/javascript">

	
	$(document).ready(function(){
	
	$('.eliminaArchivo').click(function(){
		if(confirm("Esta seguro que desea borrar este archivo adjunto para este previo?")){
		var este=$(this);
		var Id=$(this).attr('id');
			$.ajax({
                           type:"GET",
                           data:"idArchiv="+Id,
                           url:"peticionborrararchivo.php",
                           dataType: "json",
                           success: function(data){
                            if(data.Respuesta="true"){
                            	este.parent("div").remove(); 
                            //	$("#"+Id+"ruta").remove();
                            }else{
                            	alert("El archivo no fue removido \n vuelve a intentar");
                            }
                           }
                       });
                      }
	  	});

	});
</script>
</head>

<body onLoad="javascript:comprobarNota(0)">
<table class="tablaPrincipal">
	<tr valign="top">		
        <td class="tablaEspacio">&nbsp;</td>
        <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacio">&nbsp;</td>
		<td class="tdGeneral" valign="top">
			<table class="tablaMarquesina" >
				<tr>
<?	 				
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($mat) = mysql_fetch_row($resultado); 
?>	  
              		<td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($mat))?></a><a> -> </a><a href="../evaluaciones/verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a><a> -> Editar Evaluaci&oacute;n </a></td>
				</tr>
			</table><br>
<? /*
				if($_GET['estado']==4)
				{//if estado 4
			  	list($idgrupo, $grupo) = explode("-", $_POST["hid_grupo"]);
					
				list ($idnota, $nota) = explode("-", $_POST["cbo_nota"]);
				
				list ($idsubgrupo, $subgrupo) = explode("-", $_POST["cbo_subgrupo"]);
				?>
				<? 
			} //Fin if estado 4
			else//if ($_GET['estado']==3)
			{ //If estado 3*/
			  	//list($idgrupo, $grupo) = explode("-", $_POST["hid_grupo"]);
				//*******************************************************************************
				$a_fecha = addGuiones($a_fecha);
				$f_fecha = addGuiones($f_fecha);
				list($aa,$mm,$dd,$hora_a,$min_a) = explode('-', $a_fecha);
				$a_fecha = $aa."-".$mm."-".$dd;
				list($aa,$mm,$dd,$hora_f,$min_f) = explode('-', $f_fecha);
				$f_fecha = $aa."-".$mm."-".$dd;
				/*print "a_: ".$a_fecha."<p>";
				print "f_: ".$f_fecha."<p>";*/
				
				//************************************************
				/*$sql = "SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evagrupo 
						WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND 
						mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."' AND
						mei_evaprevio.idprevio <> '".$_GET["idprevio"]."'";
				//print $sql."<p>";
				$resultado = $baseDatos->ConsultarBD($sql);
				$i=0;
				while ( list($id) = mysql_fetch_row($resultado) )
				{
					$veva[$i] = $id;
					$i++;
				}
				if ( count($veva)>0 )
					$ideva_ex = implode(',', $veva);
				else $ideva_ex = 0;*/
				//print $ideva_ex."<p>";
				$sql='';
				if ($_SESSION['idtipousuario']==5)
				{
					$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval 
					 		FROM mei_evaluacion, mei_evavirgrupo
							WHERE mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo=".$_GET['idgrupo']." AND 
							mei_evaluacion.tipoevaluacion=2 AND mei_evaluacion.idevaluacion NOT IN (".$idact_ex.")";
				}
				else
				{
					$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval 
					 		FROM mei_evaluacion, mei_evagrupo
							WHERE mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo=".$_GET['idgrupo']." AND 
							mei_evaluacion.tipoevaluacion=2 AND mei_evaluacion.idevaluacion NOT IN (".$idact_ex.")";
				}
			
				$resultado = $baseDatos->ConsultarBD($sql);	
							
				if ( mysql_num_rows($resultado) > 0 )
				{//IF RESULTADO	
					$sql = "SELECT * FROM mei_tiposubgrupo";
					$resultado2 = $baseDatos->ConsultarBD($sql);		
					echo($_GET['esp']);
?>
					<form name="frm_ingresarTaller" method="post" action="guardarEvaluacion.php?modo=E&esp=<?=$_GET['esp']?>&idgrupo=<?=$_GET['idgrupo']?>&materia=<?=$_GET['materia']?>&idmateria=<?=$_GET['idmateria']?>">
					  <table class="tablaGeneral" >
                        <tr class="trTitulo">
                          <?if($_SESSION['idtipousuario']==5){
                          	$tipoprevio="Previo";
                          }?>
                          <td colspan="4" align="left" valign="middle">Editar <?= $tipoprevio ?>
                            <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_GET["idmateria"]?>">
                          	<input name="hid_previo" type="hidden" id="hid_previo" value="<?=$_GET["idprevio"]?>">
                          	<input name="hid_tipoprevio" type="hidden" id="hid_tipoprevio" value="<?=$idtipoprevio?>"></td>
                        </tr>
						<tr class="trSubTitulo">
                          <td width="25%"><b>Autor:</b></td>
                          <td width="80%" colspan="3"><?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?>						  </td>
                        </tr>
                        <tr class="trInformacion">
                            <td width="25%"><b>Grupo:</b></td>
                            <td><?=$grupo?>
                            <input name="hid_grupo" type="hidden" id="hid_grupo" value="<?=$idgrupo."-".$grupo?>"></td>
                            <td><strong>Subgrupo:</strong></td>
                            <td><select name="cbo_subgrupo" id="cbo_subgrupo">
                              <?if($_SESSION['idtipousuario']==5){?>
      
                              <option value="NULL">Previo No Grupal</option>
      <?}?>
        <?if($_SESSION['idtipousuario']==2){?>
      
                              <option value="NULL">Quiz No Grupal</option>
      <?}?>
                              <?
							while ( list($idtipo, $subgrupo) = mysql_fetch_row($resultado2) )
							{
								print "<option class='link' value='$idtipo' ";
								if ( $idtipo == $idtiposubgrupo )
									print "selected>";
								else print ">";
								print "$subgrupo</option>";
							}
						?>
                            </select></td>
                       </tr>
                        <tr class="trInformacion">
                            <td><b>Nota a la que pertenece:</b> </td>
                            <td colspan="3"><select name="cbo_nota"  onChange="javascript:comprobarNota(this.value)">
                              <?
									while ( list($ideva, $nomeva, $val_eval) = mysql_fetch_row($resultado) )
									{//WHILE RESULTADO
										print "<option value='$ideva' ";
										if ( $ideva == $idnota )
											print "selected>";
										else print ">";
										print "$nomeva --- ".($val_eval*100)."%</option>";
									}//FIN WHILE RESULTADO
							  ?>
                            </select></td>
                          </tr>
                        <tr class="trInformacion">
                        	  <?if($_SESSION['idtipousuario']==5){?>
      
                              <td><strong>Valor del Previo:</strong></td>
      <?}?>
       <?if($_SESSION['idtipousuario']==2){?>
      
                              <td><strong>Valor del Quiz:</strong></td>
      <?}?>
                            
                            <td colspan="3">
							<div id="div_cbo_valor" style="float:left">
								<select name="cbo_valor" id="select">
								  <?
													for ($i=0;$i<=100;$i=$i+1) 
												   {
														
														print "<option value='".($i/100)."'";								
														/*if ($i == ($_POST["hid_valor"]*100))*/
														if ($i == ($porcentaje_valor*100))
															print "selected>";
														else print ">";
														
														print "$i%</option>";
																	   
												   }
												  ?>
								</select>
							</div>
							<div id="div_etiqueta" style="visibility:hidden;"><em>La Calificacion de esta actividad se promediada con las dem&aacute;s actividades asignadas a esta Nota</em></div>	</td>
                          </tr>
						<tr class="trInformacion">
							 <?if($_SESSION['idtipousuario']==5){?>
      
                              <td><b>Nombre del Previo: </b> </td>
      <?}?>
      <?if($_SESSION['idtipousuario']==2){?>
      
                              <td><b>Nombre del Quiz: </b> </td>
      <?}?>
                          
						  <td colspan="3"><input name="txt_titulo" type="text" id="txt_titulo" size="100" onChange="cambiar(this)"
						  value="<?=$titulo //if (!empty($_POST["hid_titulo"]) ) print $_POST["hid_titulo"];?>"></td>
                        </tr>
                        <tr class="trInformacion">
                          <td width="25%"><b>Fecha Activaci&oacute;n:</b></td>
                          <td width="25%"><? 
								/*if(empty($_POST['hid_fecha']))
									$calendario->m_valor='';
								else*/					
									
									$fecha_a->m_valor=$a_fecha;
									$fecha_a->CrearFrmCalendario(); 
								?></td>
                          <td width="10%"><b>Hora Activaci&oacute;n </b></td>
                          <td width="40%"><select name="cbo_hora_a" id="cbo_hora_a">
                            <?
								for ($i=0;$i<=23;$i++)
								{
									if ($i<10) $i = "0".$i;
									print "<option value='$i' ";
									if ($i == $hora_a) print "selected>";
									else print ">";
										
									print "$i</option>";
								}
							?>
                          </select>
							:
						<select name="cbo_minuto_a" id="cbo_minuto_a">
  <?
								for ($i=0;$i<=55;$i=$i+5)
								{
									if ($i<10) $i = "0".$i;	
									print "<option value='$i' ";
									if ($i == $min_a) print "selected>";
									else print ">";									
									print "$i</option>";
								}
							?>
						</select></td>
                        </tr>
                        <tr class="trInformacion">
                          <td><b>Fecha Finalizaci&oacute;n: </b></td>
                          <td><? 
								/*if(empty($_POST['hid_fecha']))
									$calendario->m_valor='';
								else*/
									$fecha_f->m_valor=$f_fecha;
									$fecha_f->CrearFrmCalendario(); 
								?></td>
                          <td><b>Hora Finalizaci&oacute;n </b></td>
                          <td><select name="cbo_hora_f" id="select2">
                            <?
								for ($i=0;$i<=23;$i++)
								{
									if ($i<10) $i = "0".$i;
									print "<option value='$i' ";
									if ($i == $hora_f) print "selected>";
									else print ">";
										
									print "$i</option>";
								}
							?>
                            </select>
						:
						<select name="cbo_minuto_f" id="select3">
						  <?
								for ($i=0;$i<=55;$i=$i+5)
								{
									if ($i<10) $i = "0".$i;
									print "<option value='$i' ";
									if ($i == $min_f) print "selected>";
									else print ">";										
									print "$i</option>";
								}
							?>
					</select></td>
                        </tr>
                        <tr class="trInformacion">
                          <td><b>Estado:</b></td>
                          <td colspan="3"><select name="cbo_estado" id="cbo_estado" class="link">
						  <?
						  	$v_estados[1]= "Activo";
							$v_estados[0]= "Desactivo";
							
							for ($i=(count($v_estados)-1);$i>=0;$i--)
							{
								print "<option value='$i'";
								if ($i==$estado) print " selected>";
								else print ">";
								print $v_estados[$i]."</option>";
							}						  	
						  ?>
                          </select></td>
                        </tr>

                        <tr class="trInformacion"><td colspan="1">	
                       <td width="70%" align="left" ><?// lista los archivos adjuntos a un previo
                       	$sql = "SELECT mei_relevalarchi.idarchivo,mei_relevalarchi.archivo,mei_relevalarchi.localizacion FROM mei_relevalarchi WHERE mei_relevalarchi.idevaluacion=".$_GET["idprevio"];
						$archivos=$baseDatos->ConsultarBD($sql);
						while (list($idarchprev,$archivo,$localizacion) = mysql_fetch_row($archivos))
						{
								echo "<div><a class='eliminaArchivo' id='".$idarchprev."' href='#' title='Eliminar Archivo'> <img src='imagenes/eliminar.gif'/></a> .:. <a align='left' id='".$idarchprev."ruta' href='../../datosMEIWEB/archivosEvaluaciones/".$localizacion."' target='_blank'><b>".$archivo."</b></a></div>";
						}
						?></td>			 
						  <td height="25" width="5%">
						  </td>
						   <td height="25" width="25%">
							<a href="javascript:adjuntar();" class="link"><img src="imagenes/adjuntar.gif" align="texttop">
						  		<b> Adjuntar Archivos</b>
						  	</a>
						  </td>
					   </tr>


                       	<tr class="trInformacion">
						  <td colspan="4"><?								 
								 //if (!empty($_POST["hid_editor"]) ) 
								 	$editor->Value = stripslashes(base64_decode($comentario));
								 $editor->crearEditor();
							?>						  </td>
					    </tr>
						<tr class="trInformacion">
                            <td height="24" align="left" valign="middle"><b> Contraseña: </b> </td>
                            <td colspan="3"><input name="txt_clave" type="text" id="txt_clave" size="30" value="<?=$clave?>"></td> 
                        </tr>
                     </table>
					 <table class="tablaGeneral">
                        <tr class="trSubTitulo">
                        	<?if($_SESSION['idtipousuario']==5){?>
      
                              <td colspan="4">Caracteristicas del Previo: </td>
      <?}?>
      	<?if($_SESSION['idtipousuario']==2){?>
      
                              <td colspan="4">Caracteristicas del Quiz: </td>
      <?}?>
                          
                        </tr>
                        <tr  class="trInformacion">
                          <td width="25%">&nbsp;</td>
                          <td width="25%">&nbsp;</td>
                          <td width="25%">&nbsp;</td>
                          <td width="25%">&nbsp;</td>
                        </tr>
                        <tr  class="trInformacion">
                          <td align="left" valign="middle"><b>N&uacute;mero de Intentos:</b></td>
                          <td align="left" valign="middle"><select name="cbo_intentos" id="cbo_intentos">
                            <?
									for ($i=1;$i<=10;$i++)
									{
										print "<option value='$i'";
										if ($i == $intentos) print " selected>";
										else print ">";
										print "$i</option>";
									}	
								?>
                          </select></td>
                          <td align="left" valign="middle"><strong>Disminuir la Nota de cada intento en:</strong></td>
                          <td align="left" valign="middle"><input name="cbo_valorIntento" type="text" id="bo_calificacion" size="10" maxlength="5" value="<?=($valorIntento*100)?>">
                          %</td>
                        </tr>
                        <tr class="trInformacion">
                          <td align="left" valign="middle"><strong>Forma de Calificar: </strong></td>
                          <td align="left" valign="middle"><select name="cbo_calificar" id="cbo_calificar" class="link">
                            <?
							$sql = "SELECT mei_evamodcalificar.idmodcalificar, mei_evamodcalificar.modcalificar 
									FROM mei_evamodcalificar";
							$resultado = $baseDatos->ConsultarBD($sql);
							
							while ( list($idmod, $mod) = mysql_fetch_row($resultado) )
							{
								print "<option value='$idmod'";
								if ($idmod==$mcalificar) print " selected>";
								else print ">";
								print $mod."</option>";
							}						  	
							

						  ?>
                          </select>                          </td>
                          	<?if($_SESSION['idtipousuario']==5){?>
      
                        <td align="left" valign="middle"><b>Tiempo del Previo:</b></td>
      <?}?>
      	<?if($_SESSION['idtipousuario']==2){?>
      
                        <td align="left" valign="middle"><b>Tiempo del Quiz:</b></td>
      <?}?>
                          
                          <td align="left" valign="middle"><input name="txt_tiempo" type="text" id="txt_tiempo" size="10" value="<?=$tiempo?>">
						  <!--
						  <select name="cbo_tiempo" id="cbo_tiempo">
                            <?
							/*				for ($i=15;$i<=120;$i=$i+15)
											{
												if ($i<10) $i = "0".$i;
												print "<option value='$i' ";
												if ($i==$tiempo) print "selected>";
												else print ">";
												print "$i Minutos</option>";
											}*/
											?>
                          </select>
						  --> Minutos						  </td>
                        </tr>
                        <tr class="trInformacion">
                          <td align="left" valign="middle"><b>Barajar Preguntas:</b></td>
                          <td align="left" valign="middle"><select name="cbo_bpreguntas" id="cbo_bpreguntas">
                            <? 
						  	$b[0] = "No";
							$b[1] = "Si";
							
							for ($i=(count($b)-1);$i>=0;$i--)
							{
								print "<option value='$i'";
								if ($i==$bpreg) print " selected>";
								else print ">";
								print $b[$i]."</option>";
							}?>
                          </select>                          </td>
                          <td align="left" valign="middle"><b>Barajar Respuestas:</b></td>
                          <td align="left" valign="middle"><select name="cbo_brespuestas" id="cbo_brespuestas">
                            <? 						
							for ($i=(count($b)-1);$i>=0;$i--)
							{
								print "<option value='$i'";
								if ($i==$bresp) print " selected>";
								else print ">";
								print $b[$i]."</option>";
							}?>
                          </select>                          </td>
                        </tr>
                        <tr class="trInformacion">
                          <td align="left" valign="middle"><strong>Mostrar Nota: </strong></td>
                          <td align="left" valign="middle"><select name="cbo_mnota" id="cbo_mnota">
                            <? 
						  	$b[0] = "No";
							$b[1] = "Si";
							
							for ($i=(count($b)-1);$i>=0;$i--)
							{
								print "<option value='$i'";
								if ($i==$mnota) print " selected>";
								else print ">";
								print $b[$i]."</option>";
							}?>
                          </select>
                          </td>
                          <td align="left" valign="middle">&nbsp;</td>
                          <td align="left" valign="middle">&nbsp;</td>
                        </tr>
                        <tr class="trInformacion"><?php echo $_GET['esp']?>
                          <td align="left" valign="middle" ><strong><h2 style="color: #ec7000">Otorgar Incentivo </h2></strong></td>
                          <td><strong>Otorgar Curva Automatica</strong> 
                          	<input type="checkbox" value="curva" name="incentivo_curva"  
                          	 <?
                          	if ($curva==1)
							 print " checked";
                          	
                          	?> onclick="incentivo1()" />
                          	</td>
                          <td><strong>Nota Adicional</strong> 
                          	<input type="checkbox"  name="incentivo_nota" <?if ($curva==0) print " checked";?> onclick="incentivo2()" value="nota"/>
                          	</td>
                          <td>
                          	<strong>Nota</strong> 
	                          	<select  name="incentivo_valor" <?if ($curva==1) print " disabled='true'";?> >
	                          		<?
	                          		if($incentivo==0){
	                          			$incentivo=0.0;
	                          		} 
	                          		for ($i=0.0;$i<=5.0;$i=$i+0.1)
										{
											$x=$i;
											print "<option value='$x'";
											if ( number_format($incentivo,1,'.','')==number_format($x,1,'.',''))
											 {print " selected='true' >";}
											else {print ">";}
											print $x."</option>";
										}?>
	                          		
	                          	</select> 
                          	</td>
                        </tr>						
                        <tr class="trInformacion">
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
      				</table>
					<table class="tablaMarquesina">
					  <tr align="center">
						<td width="33%" valign="middle">
						<input type="button" name="btn_guardarTaller" value="Editar" onClick="javascript:guardartaller()">						    
					    </td>
						<td width="33%" valign="middle">
							<input type="button" name="btn_guardarTallerContinuar" value="Guardar" onClick="javascript:guardartallerquiz()">						    
					    </td>							
						<td width="33%" valign="middle">
							<input type="button" name="btn_cancelar" value="Cancelar" 
							  onClick="javascript:cancelar('<?=$_GET["idprevio"]?>', '<?=$_GET["idmateria"].'&idgrupo='.$_GET["idgrupo"]?>')"></td>
						  
					  </tr>
				  </table>

			      </form>
				  <?
					}//FIN IF RESULTADO
					else
					{
				  ?>
  				  <table class="tablaMarquesina" width="200">
				  	<tr>
						<td colspan="2" align="center" valign="middle">No existen Notas para Actividades definidas para el grupo <strong><?=$grupo?></strong>. Para crear Notas haga click <a href="../notas/index.php?idmateria=<?=$_POST["hid_materia"]?>&materia=<?=$_GET['materia']?>" class="link">aqui</a> </td>
					</tr>
                  </table>&nbsp;
   				  <table class="tablaGeneral" width="200">
                    <tr align="center" valign="middle">
                    	<td><a href="../actividades/ingresarActividad.php?idmateria=<?=$_POST["hid_materia"]?>&materia=<?=$_GET['materia']?>" class="link">Volver</a></td>
                    </tr>
                  </table>
				  <?
					}
//				}
				  ?>
				  &nbsp;
		</td>
        <td class="tablaEspacio">&nbsp;</td>
        <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
        </tr>
        </table>
				</body>

<? 
  }
?>
</html>

