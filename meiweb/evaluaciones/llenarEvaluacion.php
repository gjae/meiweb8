<?
	include_once ('../menu/Menu.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once('evaclass.php');
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{
//_______________________________________________________________________________________________________________________________
		if (!empty($_GET['cbo_ordenar'])) {
			$_POST['cbo_ordenar']=$_GET['cbo_ordenar'];
		}
		if(empty($_POST['cbo_ordenar']))
		{
			$_POST['cbo_ordenar']=1;
		}
		if($_POST['cbo_ordenar']==2)
		{
			$tipopregunta="selected";
		}
		else 
		{
			$numero="selected";		
		}
//_______________________________________________________________________________________________________________________________
	$baseDatos= new BD();
	$var = '0';
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraBasica' , '' );
	$evaluacion = $_SESSION["evaluacion"];
	//$temaid=$_SESSION['idtema'];
	?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
window.name = "llenarEvaluacion";
var ventanaPreguntas=false;
function verPregunta(idpregunta)
{
	   var izquierda = (screen.availWidth - 700) / 2;
	   var arriba = (screen.availHeight - 300) / 2;


	if (typeof ventanaPreguntas.document == 'object')
	 {
		ventanaPreguntas.close()
	 }

	ventanaPreguntas = window.open('visualizarPregunta.php?idpregunta='+idpregunta,'visualizarPregunta','width=700,height=300,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');
}
var ventanaPrevio=false;
function vizPrevio()
{
	   var izquierda = (screen.availWidth - 700) / 2;
	   var arriba = (screen.availHeight - 550) / 2;


	if (typeof ventanaPrevio.document == 'object')
	 {
		ventanaPrevio.close()
	 }

	ventanaPrevio = window.open('visualizarPrevio.php','visualizarPrevio','width=700,height=550,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');
}
function enviar()
{
	document.frm_preguntas.submit();
}
function volver(id, mat,materia)
{
	document.frm_guardarPreguntas.action = "editarEvaluacion.php?idprevio="+id+"&idmateria="+mat+"&esp=<?=$_GET['esp']?>"+"&materia="+materia;
	document.frm_guardarPreguntas.submit()
}
function volverQ(id, mat,materia)
{
	document.frm_guardarPreguntas.action = "editarQuiz.php?idprevio="+id+"&idmateria="+mat+"&esp=<?=$_GET['esp']?>"+"&materia="+materia;
	document.frm_guardarPreguntas.submit()
}

function alerta(variable)
{
	if(confirm('El Porcentaje Acumulado de las Preguntas es '+variable*100+'%. Debe completar el 100% para poder continuar. \n\nPara corregir de clic en editar a una de las preguntas agregadas y luego hacer clic en agregar. De esta manera se re-calcularán los porcentajes. '))
		{
			document.frm_guardarPreguntas.action = "guardarPreguntasEvaluacion.php?esp=<?php echo($_GET['esp']);?>&materia=<?=$materia?>";
		document.frm_guardarPreguntas.submit();
		}

}

function guardar_cambios()
{
    if(document.frm_guardarPreguntas.hid_porcentaje.value>1){
		alert('El Porcentaje Acumulado de las Preguntas es '+document.frm_guardarPreguntas.hid_porcentaje.value*100+'%. Debe completar el 100% para poder continuar. \n\nPara corregir de clic en editar a una de las preguntas agregadas y luego hacer clic en agregar. De esta manera se re-calcularán los porcentajes. ');
	}
    if (document.frm_guardarPreguntas.hid_porcentaje.value<1)
	 {
	 	alerta(document.frm_guardarPreguntas.hid_porcentaje.value);
		    

	}
	if (document.frm_guardarPreguntas.hid_porcentaje.value==1)
	{
		document.frm_guardarPreguntas.action = "guardarPreguntasEvaluacion.php?esp=<?php echo($_GET['esp']);?>&materia=<?=$materia?>";
		document.frm_guardarPreguntas.submit();
	}
}

function agregar(idpreg)
{
   var izquierda = (screen.availWidth - 200) / 2;
   var arriba = (screen.availHeight - 180) / 2;

   ventana = window.open("agregarPreguntaEvaluacion.php?materia=<?=$_GET["materia"]?>&idmateria=<?=$_GET["idmateria"]?>&idpregunta="+idpreg+"&esp=<?=$_GET['esp']?>&cbo_ordenar=<?=$_POST['cbo_ordenar']?>","ventana","width=240,height=180,left="+izquierda+",top="+arriba+",scrollbars=NO,menubars=NO,status=NO,statusbar=NO,status=NO,resizable=NO,location=NO")
}

function ordenarLista()
{
	document.frm_preguntasm.submit();				
}

</script>
</head>
<body>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
		    <td valign="top" class="tdGeneral">
            	<table class="tablaGeneral" width="200" border="0">
             		<tr class="trSubTitulo">
<?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria='"./*$_GET["idmateria"]*/$evaluacion->idmateria."'";
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria='"./*$_GET["idmateria"]*/$evaluacion->idmateria."'";
		$resultado = $baseDatos->ConsultarBD($sql);
		list($materia) = mysql_fetch_row($resultado);
?>


              <td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$materia?>" class="link"><?=ucwords(strtolower($materia))?></a><a> -> </a><a href="../evaluaciones/verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$materia?>" class="link">Evaluaciones</a><a> -> </a><a href="verDtlleEvaluacion.php?idprevio=<?=$evaluacion->idprevio?>&idgrupo=<?=$_GET['idgrupo']?>&idmateria=<?=$evaluacion->idmateria?>&esp=<?=$_GET['esp']?>&materia=<?=$materia?>" class="link">Ver Evaluaci&oacute;n</a> -> Ingresar Evaluaci&oacute;n </td>
              </tr>
            </table><br>&nbsp;
            

			    <table class="tablaMarquesina">
                  <tr align="center" valign="middle">
                    <td width="50%" align="center" valign="middle">
                    	<input type="button" name="Submit" value="Guardar Cambios" onClick="javascript:guardar_cambios()"></td>
                    <td width="50%" align="center" valign="middle">
					  <input type="button" name="btnArriba" value="Volver"
					  onClick="
					  	<?
							if ($evaluacion->idtipoprevio==1)
							{
						?>
					  		volver('<?=$evaluacion->idprevio?>&idgrupo=<?=$_GET['idgrupo']?>', '<?=$evaluacion->idmateria?>','<?=$_GET["materia"]?>')
						<?
							}
							elseif ($evaluacion->idtipoprevio==2)
							{
						?>
					  		volverQ('<?=$evaluacion->idprevio?>&idgrupo=<?=$_GET['idgrupo']?>', '<?=$evaluacion->idmateria?>','<?=$_GET["materia"]?>')
						<?
							}
						?>
							">
                    </td>
                  </tr>
                </table>
            
			  <table class="tablaGeneral">
                <tr class="trSubTitulo">
                  <td class="trSubTitulo" width="50%" align="left" valign="middle">Preguntas de la Evaluaci&oacute;n </td>
                  <td class="trSubTitulo" width="50%" align="left" valign="middle">Banco de Preguntas </td>
                </tr>
                <tr class="trListaClaro">
                  <td class="trListaClaro" align="center" valign="top">
				  <!-- CELDA DEL PREVIO -->
			  <?
				/*$sql = "SELECT mei_evadtlleprevio.idpregunta, mei_evatipopregunta.tipo, mei_tema.titulo,
						mei_evadtlleprevio.valor_pregunta FROM mei_evadtlleprevio, mei_evapreguntas, mei_evatipopregunta, mei_tema
						WHERE mei_evadtlleprevio.idpregunta = mei_evapreguntas.idpregunta AND
						mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
						mei_evapreguntas.idtema = mei_tema.idtema AND mei_evadtlleprevio.idprevio=".$_GET["idprevio"];
				$resultado = $baseDatos->ConsultarBD($sql);
				print $sql;*/
			 if ( count($evaluacion->preguntas)>0 )
 			 //if ( mysql_num_rows($resultado)>0 )
			  {//IF HAY PREGUNTAS

			  ?>&nbsp;
              <table border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
                <tr class="trSubTitulo">
                  <td width="30%" height="26" align="center" valign="middle">Pregunta</td>
                  <td width="8%" align="center" valign="middle">Tipo</td>
                  <td width="7%" align="center" valign="middle"><span title="Grado de Dificultad">GD</span></td>
                  <td width="15%" align="center" valign="middle">Tema</td>
                  <td width="10%" align="center" valign="middle">Valor</td>
                   <td width="10%" align="center" valign="middle">Ver</td>
                  <td width="10%" align="center" valign="middle">Editar</td>
                  <td width="10%" align="center" valign="middle">Eliminar</td>
                </tr>
			<?
				$porcentaje = 0;
				$i = 0;
				$numeracion=1;
				while( list($cont) = each($evaluacion->preguntas) )

				//while ( list($idpreg, $tipo, $tema, $valor) = mysql_fetch_row($resultado) )
				{//WHILE IMPRIMIR PREGUNTAS
					if ( ($i%2)==0 ) $clase = "trListaOscuro";
					else $clase = "trInformacion";
					$porcentaje += $evaluacion->preguntas[$cont]->valor;
					//$porcentaje += $valor
			?>
                <tr class="<?=$clase?>">

			    <td align="center" valign="middle"><a> <? print $cont ?>.
				  <?
				  /*$idpreg*/
				  	if ( $evaluacion->preguntas[$cont]->idtipo == 4 )
					{
						list($afirmacion) = explode("[$$$]",$evaluacion->preguntas[$cont]->pregunta);
												print substr(strip_tags($afirmacion),0,20)."...";
					}
					else

					print substr(strip_tags($evaluacion->preguntas[$cont]->pregunta),0,30)."...";
				  //$evaluacion->preguntas[$cont]->pregunta
				  ?> </a></td>
                  <td align="center" valign="middle"><?/*$tipo*/
                        $tipoini = $evaluacion->preguntas[$cont]->idtipo;
                        if($tipoini == 1){ $inicial = "UR";$titleini ="Unica Respuesta";}
                        elseif($tipoini == 2){ $inicial = "MR";$titleini ="Multiple Respuesta";}
                        elseif($tipoini == 3){ $inicial = "FV";$titleini ="Falso o Verdadero";}
                        elseif($tipoini == 4){ $inicial = "AR";$titleini ="Analisis de Relacion";}
                        elseif($tipoini == 5){ $inicial = "PA";$titleini ="Pregunta Abierta";}
                        elseif($tipoini == 6){ $inicial = "RC";$titleini ="Relacion de Columnas";}  
                    ?>
                    <span title="<?=$titleini?>"><?=$inicial?></span>
                  </td>
                  <td align="center" valign="middle"><?/*$tipo*/
                        $idpreg = $evaluacion->preguntas[$cont]->idpregunta;
                		$sqlgrado = "SELECT mei_evapreguntas.idgrado FROM mei_evapreguntas WHERE mei_evapreguntas.idpregunta=".$idpreg;

                		$resultadoidgrado = $baseDatos->ConsultarBD($sqlgrado);
                		list($idgrado) = mysql_fetch_row($resultadoidgrado); 
                        
                        if($idgrado == 1){$inicialg = "F";$titleinig ="Facil";}  
                        elseif($idgrado == 2){$inicialg = "M";$titleinig ="Medio";}
                        elseif($idgrado == 3){$inicialg = "D";$titleinig ="Dificil";}                      
                    ?>
                    <span title="<?=$titleinig?>"><?=$inicialg?></span>
                  </td>                  
                  <td align="center" valign="middle"><?=/*$tema*/$evaluacion->preguntas[$cont]->tema?></td>
                  <td align="center" valign="middle"><?=/*($valor*100)."%"*/round(($evaluacion->preguntas[$cont]->valor*100),2)."%"?></td>
                   <td height="20" align="center" valign="middle"><a href="javascript:verPregunta('<?=$cont?>')"><img src="imagenes/lupa.gif" width="16" height="16" border="0"></a></td>
                  <td align="center" valign="middle"><a href="javascript:agregar('<?=$evaluacion->preguntas[$cont]->idpregunta?>')"><img src="../preguntasFrecuentes/imagenes/modificar.gif" width="16" height="16" border="0"></a></td>
                  <td align="center" valign="middle"><a href="guardarPreguntaEnClase.php?idmateria=<?=$_GET["idmateria"]?>&accion=del&idpregunta=<?=$evaluacion->preguntas[$cont]->idpregunta?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET['materia']?>"><img src="../preguntasFrecuentes/imagenes/eliminar.gif" width="16" height="16" border="0"></a></td>
                </tr>
			<?
					$numeracion++;
					$i++;
				}//FIN WHILE IMPRIMIR PREGUNTAS
			?>

                <tr class="trInformacion">
                  <td colspan="8" align="center" valign="middle">&nbsp;</td>
                </tr>
                <tr class="trSubTitulo">
                  <td colspan="8" align="center" valign="middle">Procentaje Acumulado de las Preguntas: <?=(round($porcentaje,2)*100)."%"?></td>
                </tr>
                <tr class="trSubTitulo">
                  <td colspan="8" align="center" valign="middle">Total de Preguntas Agregadas: <?=$i?></td>
                </tr>
                <tr>
                	<td colspan="8" align="center" valign="middle">&nbsp;</td>
                </tr>
                <tr>
                	 <td colspan="8" align="center" style="color: red;font-weight: bold" valign="middle">Nota: Recuerde que el procentaje acumulado de las preuntas de la evaluación debe sumar el 100%, de lo contrario no podra guardar los cambios.  </td>
                </tr>
              </table>
			  <?

			  }//IF HAY PREGUNTAS
			  else
			  {//ELSE NO HAY PREGUNTAS
			  ?>&nbsp;
			  <table border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
                <tr  align="center" valign="middle">
                  <td class="trListaOscuro">Actualmente esta Evaluaci&oacute;n no tiene  preguntas asignadas.</td>
                </tr>
                <tr>
                	<td colspan="7" align="center" valign="middle">&nbsp;</td>
                </tr>
                <tr>
                	 <td colspan="7" align="center" style="color: red;font-weight: bold" valign="middle">Nota: Para poder guardar los cambios la evaluación debe tener preguntas asignadas. </td>
                </tr>
              </table>
			  <?
			  }//ELSE NO HAY PREGUNTAS
			  ?>
				  <!--FIN CELDA DEL PREVIO-->				  </td>
				  <!-- CELDA BANCO PREGUNTAS-->
                  <td class="trListaClaro" align="center" valign="top">&nbsp;
<!-- ______________________________________________________________________________________________________________________________ -->

					<table border="0"  class="tablaPrincipal">
                <form name="frm_buscar" method="post" action="llenarEvaluacion.php?esp=<?=$_GET['esp']?>&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idpregunta=<?=$_GET['idpregunta']?>&tipopregunta=<?=$_GET['tipopregunta']?>">	
						<tr class="trSubTitulo">
							<td>Buscar pregunta:</td>
							<td colspan="4"><input name="txt_buscarPregunta" type="text" value="<?PHP echo  @$_POST['txt_buscarUsuario']; ?>" size="20">
							<input type="submit" name="sub_buscar" value="Buscar"></td>
						</tr>
<?php
//_____________________________________________________________________________________________________________________________
	if(!empty($_POST['txt_buscarPregunta']))
	{
		$codigopregunta = $_POST['txt_buscarPregunta'];
		$sql1="SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta,
							mei_evatipopregunta.tipo, mei_evapreguntas.autoevaluacion

							FROM mei_evapreguntas, mei_evatipopregunta, mei_tema WHERE
							mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
							mei_evapreguntas.idtema = mei_tema.idtema AND mei_tema.idmateria = '".$evaluacion->idmateria."' AND mei_evapreguntas.idpregunta='".$codigopregunta."'";
		$resultadouno = $baseDatos->ConsultarBD($sql1);			
		$numRegistros = mysql_num_rows($resultadouno);
		if($numRegistros>0)
		{
?>
			<tr class="trSubTitulo">
                        <td width="60%" align="center" valign="middle">Pregunta</td>
                        <td width="30%" align="center" valign="middle">Tipo</td>
                        <td width="30%" align="center" valign="middle"><span title="Pregunta de Autoevaluación">AU</span></td>
                        <td width="10%" align="center" valign="middle">Ver</td>
						  <td width="10%" align="center" valign="middle">Editar</td>
                      </tr>
<?php
	  	list($idpregunta, $pregunta, $idtipo, $tipo, $autoevaluacion, $idtema) = mysql_fetch_row($resultadouno);
//*******************************************************
?>
                      <tr class="trInformacion">
                        <td height="20" align="center" valign="middle"><a href="javascript:agregar('<?=$idpregunta?>')" class="link"><?=$idpregunta?>.<?
						if ( $idtipo == 4 )
						{
							list($afirmacion) = explode("[$$$]",stripslashes(base64_decode($pregunta)));
							print substr(strip_tags($afirmacion),0,20)."...";
						}
						else print substr(strip_tags(stripslashes(base64_decode($pregunta))),0,30)."...";?></a></td>
                        <td height="20" align="center" valign="middle"><?=$tipo?></td>

                        <td height="20" align="center" valign="middle"><?php
                       
            	if(!empty($autoevaluacion))
				{

						?><img src="../autoevaluaciones/imagenes/iconoautoevaluacion.png" width="16" height="16" title="Pregunta de Autoevaluación"><?php
				}
				else
				{

				?>&nbsp;<?php
				}
				?></td>
                      <td height="20" align="center" valign="middle"><a href="javascript:verPregunta('<?=$idpregunta?>')"><img src="imagenes/lupa.gif" width="16" height="16" border="0"></a></td> <td align="center" valign="middle"><a href="editarPregunta.php?idpregunta=<?=$idpregunta?>&idmateria=<?=$_GET["idmateria"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET['materia']?>" class="link"><img src="imagenes/modificar.gif" alt="modificar pregunta" width="16" height="16" border="0"></a></td>

                      </tr>
<?php
//*******************************************************
		}
		else
		{
?>
			
               		<tr class="trAviso">
               		<td colspan="3">No se encontraron coincidencias con 
			<i>
                       	<?PHP echo $codigopregunta; ?>
               		</i> 
			</td>
               		</tr>
               		
<?php
		}
	}
?>

		</form>		
					</table>

<!-- ______________________________________________________________________________________________________________________________ -->
				  <table border="0"  class="tablaPrincipal">

                    <form name="frm_preguntas" method="post" action="llenarEvaluacion.php?esp=<?=$_GET['esp']?>&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>">

                      <tr>


                         <td class="trSubTitulo">Ordenar por tema:</td>
                        <td class="trSubTitulo">

					  <select name="cbo_tema" id="cbo_tema"  onChange="enviar()">

                            <option value="*">Todos...</option>
                            <?
						$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema
								WHERE mei_tema.idmateria = '".$evaluacion->idmateria."' AND mei_tema.tipo = 1";
						$resultado = $baseDatos->ConsultarBD($sql);


						if(isset($_POST["cbo_tema"]))
						$_SESSION['idtema']=$_POST["cbo_tema"];
						else
						$_POST["cbo_tema"]=$_SESSION['idtema'];

						while ( list($idtema, $tema) = mysql_fetch_row($resultado) )
						{
							print "<option value='$idtema' ";

							if ( ($idtema == $_POST["cbo_tema"]) or ($temaid==$idtema)) print "selected>";
							else print ">";
							print "$tema</option>";
						}
					?>



                      </select>
 					  </td>

                      </tr>
                    </form>

                  </table>

				  <?php

				  if ( empty($_POST["cbo_tema"])) $_POST["cbo_tema"]='*';
//______________________________________________________________________________________________________________________________________
	if($_POST["cbo_ordenar"] == 1 )
	{
		if ( $_POST["cbo_tema"]!='*' )
				  {
						
						$subsql = "SELECT mei_tema.idtema FROM mei_tema WHERE mei_tema.idtema = '".$_POST["cbo_tema"]."' OR mei_tema.idtemapadre = '".$_POST["cbo_tema"]."' ";
						$resultado_sub_consulta = $baseDatos->ConsultarBD($subsql);
						while ( list($idtemasubconsulta) = mysql_fetch_row($resultado_sub_consulta) ){
							$subidtema[] = $idtemasubconsulta;
						}
						$contarsubidtema = count($subidtema);
						$contadorid = 0;
						foreach( $subidtema as $inditema ){
							$contadorid++;
							if( $contadorid < $contarsubidtema ){
								$sqlidtema .= "mei_evapreguntas.idtema = ".$inditema." OR ";
							}else{
							 $sqlidtema .= "mei_evapreguntas.idtema = ".$inditema;
							}
						}
					
					  $sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,
							mei_evatipopregunta.tipo, mei_evapreguntas.autoevaluacion, mei_evapreguntas.idtema
							FROM mei_evapreguntas,
							mei_evatipopregunta, mei_evagradopregunta
							WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
							AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND (".$sqlidtema.") ORDER BY mei_evapreguntas.idpregunta ASC";

				 }

				  else
				  {
$var = 1;

				  	$sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta,
							mei_evatipopregunta.tipo, mei_evapreguntas.autoevaluacion

							FROM mei_evapreguntas, mei_evatipopregunta, mei_tema WHERE
							mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
							mei_evapreguntas.idtema = mei_tema.idtema AND mei_tema.idmateria = '".$evaluacion->idmateria."' ORDER BY mei_evapreguntas.idpregunta ASC" ;
				  }
	}
	else if($_POST["cbo_ordenar"] == 2 )
	{
	if ( $_POST["cbo_tema"]!='*' )
				  {
						
						$subsql = "SELECT mei_tema.idtema FROM mei_tema WHERE mei_tema.idtema = '".$_POST["cbo_tema"]."' OR mei_tema.idtemapadre = '".$_POST["cbo_tema"]."' ";
						$resultado_sub_consulta = $baseDatos->ConsultarBD($subsql);
						while ( list($idtemasubconsulta) = mysql_fetch_row($resultado_sub_consulta) ){
							$subidtema[] = $idtemasubconsulta;
						}
						$contarsubidtema = count($subidtema);
						$contadorid = 0;
						foreach( $subidtema as $inditema ){
							$contadorid++;
							if( $contadorid < $contarsubidtema ){
								$sqlidtema .= "mei_evapreguntas.idtema = ".$inditema." OR ";
							}else{
							 $sqlidtema .= "mei_evapreguntas.idtema = ".$inditema;
							}
						}
					
					  $sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,
							mei_evatipopregunta.tipo, mei_evapreguntas.autoevaluacion, mei_evapreguntas.idtema
							FROM mei_evapreguntas,
							mei_evatipopregunta, mei_evagradopregunta
							WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
							AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND (".$sqlidtema.") ORDER BY mei_evatipopregunta.tipo DESC";

				 }

				  else
				  {
					$var = 1;

					$sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,
							mei_evatipopregunta.tipo, mei_evapreguntas.autoevaluacion, mei_evapreguntas.idtema
							FROM mei_evapreguntas,
							mei_evatipopregunta, mei_evagradopregunta
							WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
							AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND (".$sqlidtema.") ORDER BY mei_evatipopregunta.tipo DESC";
				  }					
					
	}
	else
	{
		/*{?}*/
	} 	
//______________________________________________________________________________________________________________________________________
				  

				  $resultado = $baseDatos->ConsultarBD($sql);
//____________________________________________________________________________________________________________________________
/*
$sql1 = "SELECT mei_evapreguntas.idpregunta, mei_evatipopregunta.tipo FROM mei_evapreguntas, mei_evatipopregunta, mei_tema WHERE
							mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
							mei_evapreguntas.idtema = mei_tema.idtema AND mei_tema.idmateria = '".$evaluacion->idmateria."' ORDER BY mei_evapreguntas.idpregunta ASC" ;
				  }

				  $resultadouno = $baseDatos->ConsultarBD($sql1);
*/
				//list($idpregunta, $preguntaa, $idtipoo, $tipopregunta, $autoevaluacionn, $idtemaa) = mysql_fetch_array($resultado);
//____________________________________________________________________________________________________________________________
				  if ( mysql_num_rows($resultado)>0 )
				  {// IF HAY PREGUNTAS
				  ?>&nbsp;
 <table border="0" class="tablaPrincipal">
                    <form name="frm_preguntasm" method="post" action="llenarEvaluacion.php?esp=<?=$_GET['esp']?>&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idpregunta=<?=$_GET['idpregunta']?>&tipopregunta=<?=$_GET['tipopregunta']?>">

				   	
<!-- ------------------------------------------------------------------------------------------------------------- -->
<?php
if($var == 0)
{
?>
					<tr>
						<td class="trSubTitulo">Ordenar lista por:</td>
						<td colspan="4" class="trSubTitulo">
						<select name="cbo_ordenar" id="cbo_ordenar" onChange="javascript:ordenarLista()">
				<!--		<option value="1" <?PHP echo @$variable; ?>></option>		-->
						<option value="1" <?PHP echo @$numero; ?>>Número</option>
						<option value="2" <?PHP echo @$tipopregunta; ?>>Tipo</option>
						</select>
						</td>
					</tr>
<?php
}
?>					
<!-- ------------------------------------------------------------------------------------------------------------- -->
                      <tr class="trSubTitulo">
                        <td width="60%" align="center" valign="middle">Pregunta</td>
                        <td width="30%" align="center" valign="middle">Tipo</td>
                        <td width="30%" align="center" valign="middle"><span title="Pregunta de Autoevaluación">AU</span></td>
                        <td width="10%" align="center" valign="middle">Ver</td>
						  <td width="10%" align="center" valign="middle">Editar</td>
                      </tr>

					  <?
						$i = 0;
					  	while ( list($idpregunta, $pregunta, $idtipo, $tipo, $autoevaluacion, $idtema) = mysql_fetch_row($resultado) )
						{

							if ( ($i%2)==0 ) $clase = "trListaOscuro";
							else $clase = "trInformacion";
					  ?>


                      <tr class="<?=$clase?>">
                        <td height="20" align="center" valign="middle"><a href="javascript:agregar('<?=$idpregunta?>')" class="link"><?=$idpregunta?>.<?
						if ( $idtipo == 4 )
						{
							list($afirmacion) = explode("[$$$]",stripslashes(base64_decode($pregunta)));
							print substr(strip_tags($afirmacion),0,20)."...";
						}
						else print substr(strip_tags(stripslashes(base64_decode($pregunta))),0,30)."...";?></a></td>
                        <td height="20" align="center" valign="middle"><?=$tipo?></td>

                        <td height="20" align="center" valign="middle"><?php
                       
            	if(!empty($autoevaluacion))
				{

						?><img src="../autoevaluaciones/imagenes/iconoautoevaluacion.png" width="16" height="16" title="Pregunta de Autoevaluación"><?php
				}
				else
				{

				?>&nbsp;<?php
				}
				?></td>
                      <td height="20" align="center" valign="middle"><a href="javascript:verPregunta('<?=$idpregunta?>')"><img src="imagenes/lupa.gif" width="16" height="16" border="0"></a></td> <td align="center" valign="middle"><a href="editarPregunta.php?idpregunta=<?=$idpregunta?>&idmateria=<?=$_GET["idmateria"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET['materia']?>" class="link"><img src="imagenes/modificar.gif" alt="modificar pregunta" width="16" height="16" border="0"></a></td>
                      </tr>
					  <?
					  		$i++;					  	
						}
					  ?>
                    </table>

</form>

					<?
					}//FIN IF HAY PREGUNTAS
					else
					{
					?>&nbsp;
					<table border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
                      <tr align="center" valign="middle">
                        <td class="trSubTitulo">No se han definido preguntas para este tema </td>
                      </tr>
                    </table>
					<?
					}
					?>
				    <!-- FIN CELDA BANCO PREGUNTAS -->				  &nbsp;</td>
                </tr>
                <tr class="trInformacion">
                  <td colspan="2" align="center" valign="top" class="trInformacion"><a href="javascript:vizPrevio()" class="link"><strong>Visualizar Evaluación</strong></a></td>
                </tr>
              </table>

			  <form name="frm_guardarPreguntas" method="post" action="guardarPreguntasEvaluacion.php?materia=<?=$_GET['materia']?>&esp=<?php echo($_GET['esp']);?>" onSubmit="guardarPreguntasEvaluacion.php?materia=<?=$_GET['materia']?>&esp=<?php echo($_GET['esp']);?>">

			    <table class="tablaMarquesina">
                  <tr align="center" valign="middle">
                    <td width="50%" align="center" valign="middle"><input type="button" name="Submit" value="Guardar Cambios" onClick="javascript:guardar_cambios()"></td>
                    <td width="50%" align="center" valign="middle">
					  <input type="button" name="Submit2" value="Volver"
					  onClick="
					  	<?
							if ($evaluacion->idtipoprevio==1)
							{
						?>
					  		volver('<?=$evaluacion->idprevio?>&idgrupo=<?=$_GET['idgrupo']?>', '<?=$evaluacion->idmateria?>','<?=$_GET["materia"]?>')
						<?
							}
							elseif ($evaluacion->idtipoprevio==2)
							{
						?>
					  		volverQ('<?=$evaluacion->idprevio?>&idgrupo=<?=$_GET['idgrupo']?>', '<?=$evaluacion->idmateria?>','<?=$_GET["materia"]?>')
						<?
							}
						?>
							">
                    </td>
                  </tr>
                </table>
                <input name="hid_porcentaje" type="hidden" id="hid_porcentaje" value="<?=round($porcentaje,2)?>">
			  </form>
              <p>&nbsp;</p></td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
} // fin IF comprobar sesion
else redireccionar('../login/');
?>

