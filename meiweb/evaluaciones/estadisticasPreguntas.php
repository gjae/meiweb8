<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	
	if(comprobarSession())
	{	
	$baseDatos= new BD();	
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraBasica' , '' );
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">

</head>
<body>
	<table width="277" height="260" class="tablaPrincipal">
		<tr>
			<td width="34" valign="top" class="tdMenu"><? menu($_SESSION['idtipousuario']); ?></td>
		    <td width="231" valign="top" class="tdGeneral"><table class="tablaMarquesina" width="200" border="0">
              <tr>
                <td>Evaluaciones > Estadisticas Preguntas </td>
              </tr>
            </table>&nbsp;
			
			<?
				//************NUMERO DE VECES PREGUNTA EN EVALUACIONES*********************
				$sql = "SELECT mei_evadtlleprevio.idprevio FROM mei_evadtlleprevio 
						WHERE mei_evadtlleprevio.idpregunta = '".$_GET["idpregunta"]."'";
				
				//print $sql."<p>";
				
				$resultado = $baseDatos->ConsultarBD($sql);
				
				$numPregEnEva = mysql_num_rows($resultado);
				
				//************NUMERO DE RESPUESTAS A LAS EVALUACIONES*********************
				
				$sql = "SELECT COUNT(*) FROM mei_usuprevio, mei_evaprevio, mei_evadtlleprevio 
						WHERE mei_usuprevio.idprevio = mei_evaprevio.idprevio AND
						mei_evaprevio.idprevio = mei_evadtlleprevio.idprevio AND
						mei_evadtlleprevio.idpregunta = '".$_GET["idpregunta"]."'";
				//print $sql;
				
				$resultado = $baseDatos->ConsultarBD($sql);
				list($numRespEva) = mysql_fetch_row($resultado);

				
				//************NUMERO DE VECES QUE LA HAN RESPONDIDO*********************
				
				$sql = "SELECT COUNT(*) FROM mei_usupreviodtlle WHERE
						mei_usupreviodtlle.idpregunta = '".$_GET["idpregunta"]."'";
				
				list($numResp) = mysql_fetch_row($baseDatos->ConsultarBD($sql));
				//print $sql."<p>";
				
				//************NUMERO DE VECES QUE LA HAN RESPONDIDO CORRECTAMENTE*******************
				
				$sql = "SELECT mei_evarespuestas.idrespuesta FROM mei_evarespuestas WHERE
						mei_evarespuestas.idpregunta = '".$_GET["idpregunta"]."' AND mei_evarespuestas.valor > 0";
				//print $sql."<p>";
				
				$resultado = $baseDatos->ConsultarBD($sql);
				
				$i = 0;
				while ( list($id) = mysql_fetch_row($resultado) )
				{
					$respOk_bd[$i] = $id;
					$i++;
				}
				
				$respOk = implode(",", $respOk_bd);
				
				//print "respuestas OK: ".$respOk."<p>";				
				
				if($_GET["tipo_preg"] != 2)
				{	//TIPO DE PREGUNTA 1,3,4			
					$sql = "SELECT COUNT(*) FROM mei_usupreviodtlle WHERE 
							mei_usupreviodtlle.idrespuesta IN ('".$respOk."') AND 
							mei_usupreviodtlle.idpregunta = '".$_GET["idpregunta"]."'";
					print $sql."<p>";
					
					$resultado = $baseDatos->ConsultarBD($sql);
					
					list($numrespOk) = mysql_fetch_row($resultado);
				}//FIN TIPO DE PREGUNTA 1,3,4
				else
				{//TIPO DE PREGUNTA 2
					for($i=1;$i<=count($respOk_bd);$i++)
					{
						$m[$i] = 0;
					}
					
					
					$sql = "SELECT mei_usupreviodtlle.idrespuesta FROM mei_usupreviodtlle WHERE 
							mei_usupreviodtlle.idpregunta = '".$_GET["idpregunta"]."'";
					print $sql."<p>";
					
					$resultado = $baseDatos->ConsultarBD($sql);
					

					while (list($respOk_usu) = mysql_fetch_row($resultado) )
					{//WHILE 1
						print "----------------------------------------<p>";
						print "REspuesta BD: ".$respOk."<p>";
						print "REspuesta usuario: ".$respOk_usu."<p>";
						
						$caca = explode(",",$respOk_usu);
						$cont = 0;
						while ( list(,$val) = each($respOk_bd) )
						{
							print "<blockquote>";
							print "val: ".$val."<p>";
							print "</blockquote>";
							if ( in_array($val,$caca) )	$cont++;
						}
						reset($respOk_bd);
						print "acerto en: ".$cont."<p>"; 
						$m[$cont]++;
					}//FIN WHILE 1
					
					while( list($key, $val) = each($m) )
					{
						print "LLave: ".$key."------ Valor: ".$val."<p>";
					}
					
				}//FIN TIPO DE PREGUNTA 2
			?>
			
			
		      <table class="tablaGeneral">
                <tr>
                  <td>N&uacute;mero de Evaluaciones en las que aparece: </td>
                  <td><?=$numPregEnEva?></td>
                </tr>
                <tr>
                  <td>N&uacute;mero de Respuestas a las Evaluaciones en las que aparece la pregunta: </td>
                  <td><?=$numRespEva?></td>
                </tr>
                <tr>
                  <td>N&uacute;mero de veces que la han contestado: </td>
                  <td><?=$numResp?></td>
                </tr>
				<?
				if ( $_GET["tipo_preg"] != 2 )
				{//TIPO DE PREGUNTA 1,3,4
				?>
                <tr>
                  <td>N&uacute;mero de veces que la han contestado correctamente: </td>
                  <td><?=$numrespOk?></td>
                </tr>
                <tr>
                  <td>N&uacute;mero de veces que la han contestado incorrectamente: </td>
                  <td><?=($numResp - $numrespOk)?></td>
                </tr>
				<?
				}//FIN TIPO DE PREGUNTA 1,3,4
				?>

              </table>&nbsp;
			  <table class="tablaGeneral">
			  	<tr align="center" valign="middle">
					<td><a href="editarPregunta.php?idpregunta=<?=$_GET["idpregunta"]?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET['materia']?>" class="link">Volver</a></td>
				</tr>
			  </table>
		  </td>
		</tr>
	</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>