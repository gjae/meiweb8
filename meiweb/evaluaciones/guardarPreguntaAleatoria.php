<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once('evaclass.php');
    
if( (comprobarSession() ) && ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==5) )
{
    $evaluacion = $_SESSION["evaluacion"]; 
	$baseDatos= new BD();

    $totalpreguntas = $_GET['totalpreguntas'];
    $arreglopreguntas = $_GET['arrpreguntas'];    
    
    $preguntas = explode("/",$arreglopreguntas);
    
    foreach($preguntas as $pregunta){
        $arrpreguntas = explode(",",$pregunta);
        $tema = $arrpreguntas[0];
        $subtema = $arrpreguntas[1];
        $gradodif = $arrpreguntas[2];
        $arrgradodif = explode("-",$gradodif);
        $idgrado = $arrgradodif[0];            
     
        $cantidadpreguntas = $arrpreguntas[3];
        
        /*echo "Tema: ".$tema."<br/>";
        echo "Subtema: ".$subtema."<br/>";
        echo "gradodif: ".$idgrado."<br/>";
        echo "cantidadpreguntas: ".$cantidadpreguntas."<br/>";*/
    
        if($subtema != "general"){
        	$sql = "SELECT mei_evapreguntas.idpregunta 
        			FROM mei_evapreguntas
                    WHERE mei_evapreguntas.idtema =".$subtema." AND mei_evapreguntas.idgrado =".$idgrado."
        			ORDER BY RAND() LIMIT ".$cantidadpreguntas;            
        }   
        else{ 
        	$sql = "SELECT mei_evapreguntas.idpregunta 
        			FROM mei_evapreguntas
                    WHERE mei_evapreguntas.idtema =".$tema." AND mei_evapreguntas.idgrado =".$idgrado."
        			ORDER BY RAND() LIMIT ".$cantidadpreguntas;
        }			
    
    	$resultadorand = $baseDatos->ConsultarBD($sql);		
    	  
        while ( list($idpregunta) = mysql_fetch_row($resultadorand) ){
        	$sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta, mei_evatipopregunta.tipo, mei_tema.titulo 
        			FROM mei_evapreguntas, mei_evatipopregunta, mei_tema
        			WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
        			mei_evapreguntas.idtema = mei_tema.idtema AND mei_evapreguntas.idpregunta=".$idpregunta;			
        
        	$resultado = $baseDatos->ConsultarBD($sql);		
        	list($idpreg, $preg, $idtipo, $tipo, $tema, $valor) = mysql_fetch_row($resultado);
        	$evaluacion->agregar_preg($idpreg, $idpreg, stripslashes(base64_decode($preg)), $idtipo, $tipo, $tema, $valorPreg);
        
           	$valorPreg = (1/$totalpreguntas);
        	
        	while( list($id) = each($evaluacion->preguntas) )
        	{
        		$evaluacion->preguntas[$id]->valor = $valorPreg;		
        	}
        }        
    }	
	redireccionar("llenarEvaluacion.php?esp=".$_GET['esp']."&idpregunta=".$id."&idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']);
	
        
	
}
else redireccionar('../login/');
?>