<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once('evaclass.php');

if( (comprobarSession() ) && ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==5))
{
	$evaluacion = $_SESSION["evaluacion"]; 	
	$baseDatos= new BD();	

	$sql = "DELETE FROM mei_evadtllequiz WHERE mei_evadtllequiz.idprevio='".$evaluacion->idprevio."'";

	$baseDatos->ConsultarBD($sql);
	
	//print $sql."<p>";
	
	//print "Numero de preguntas: ".count($evaluacion->preguntas)."<p>";

	if ( count($evaluacion->preguntas)>0 )
	{
		while( list($cont) = each($evaluacion->preguntas) )
		{	
			$sql = "INSERT INTO `mei_evadtllequiz` ( `idpregunta` , `idprevio` , `valor_pregunta` ) 
					VALUES ('".$evaluacion->preguntas[$cont]->idpregunta."', 
					'".$evaluacion->idprevio."', '".$evaluacion->preguntas[$cont]->valor."')";
					
			$baseDatos->ConsultarBD($sql);
			//print $sql."<p>";
		}
	}
	
	$idmateria = $evaluacion->idmateria;
	$idtipoprevio = $evaluacion->idtipoprevio;
	unset($evaluacion);
	if ($idtipoprevio==1)
		redireccionar("verEvaluacion.php?idmateria=".$idmateria."&materia=".$_GET['materia']);
	elseif ($idtipoprevio==2) redireccionar("../actividades/verActividad.php?idmateria=".$idmateria."&materia=".$_GET['materia']);
	
}
else redireccionar('../login/');
?>