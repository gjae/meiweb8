<?
	include_once ('../menu/Menu.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
if(comprobarSession())
{	
	$baseDatos= new BD();	
	$editor=new FCKeditor('edt_comentario' , '100%' , '200' , 'barraBasica' , '' );
	$fecha_a=new FrmCalendario('frm_ingresarEvaluacion','txt_fecha_a','formulario','false','');	
	$fecha_f=new FrmCalendario('frm_ingresarEvaluacion','txt_fecha_f','formulario','false','');	
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
	
function validarEntero(){ 
      //intento convertir a entero. 
     //si era un entero no le afecta, si no lo era lo intenta convertir 
     valor = parseInt(document.frm_ingresarEvaluacion.txt_tiempo.value) 
	 valor2 = parseInt(document.frm_ingresarEvaluacion.cbo_valorIntento.value) 
      //Compruebo si es un valor numérico 
      if ( ((isNaN(valor)) || (valor<1)) || ( (isNaN(valor2)) || (valor2<0) ) ) { 
            //entonces (no es numero) devuelvo el valor cadena vacia 
            return false  
      }else{ 
            //En caso contrario (Si era un número) devuelvo el valor 
            return true 
      } 
}
function habilitar()

{

    if (document.frm_ingresarEvaluacion.cbo_estado == "1")

    {

    document.frm_ingresarEvaluacion.direccionIp1.disabled = true;

    }

    else

    {

    document.frm_ingresarEvaluacion.direccionIp1.disabled  = false;

    }

}


	
	function validar()
	{
		if ( document.frm_ingresarEvaluacion.txt_titulo.value!="" && document.frm_ingresarEvaluacion.txt_tiempo.value!="" && document.frm_ingresarEvaluacion.cbo_valorIntento.value!="")
		{	
			if ( validarEntero() )
				return true;
			else alert("El campo 'Tiempo de la Evaluación' debe ser un número mayor o igual a 1. El campo 'Disminuir la Nota de cada intento' debe ser un número mayor a 0")
		}
		else 
			{
				alert('Debe llenar todos los campos')
				return false;
			}
	}

	function verHora()
	{
	
		var fecha=frm_ingresarEvaluacion.txt_fecha_a.value.split("-");
		var fechaC=frm_ingresarEvaluacion.txt_fecha_f.value.split("-");
		var hora_a = document.frm_ingresarEvaluacion.cbo_hora_a.value + document.frm_ingresarEvaluacion.cbo_minuto_a.value
		var hora_f = document.frm_ingresarEvaluacion.cbo_hora_f.value + document.frm_ingresarEvaluacion.cbo_minuto_f.value
		
		if(fecha[2] < fechaC[2])
		//if(fecha[0] <= fechaC[0])
		{
			return true
		}
		else
		{
			if(fecha[1] < fechaC[1])
			{
				return true
			}
			else
			{
				//if(fecha[2] < fechaC[2])
				if(fecha[0] < fechaC[0])
				{
					return true
				}
				else
				{
					if (hora_f > hora_a)
					{
						return true
					}
					else return false
				}						
			}							
		}
	}

	function guardar()
	{
		if (validar())
		{
			if (verHora())
			{
				//alert('nice')
				document.frm_ingresarEvaluacion.action="guardarQuizReposicion.php?idmateria=<?=$_POST['hid_materia']?>&materia=<?=$_GET["materia"]?>";
				document.frm_ingresarEvaluacion.submit();
			}
			else alert('La Hora de Finalizacion debe ser mayor que la Hora de Activación')

		}
	}
	function cancelar(id, mat)
	{
		document.frm_ingresarEvaluacion.action="ingresarEvaluacion.php?idmateria="+id+"&materia="+mat;
		document.frm_ingresarEvaluacion.submit();
	}
	
	function enviar()
	{
		var sel = frm_1.cbo_tipoprevio.value.split("-")
		//alert(document.frm_1.cbo_tipoprevio.value);
		if (sel[0]==1)
			document.frm_1.submit()
		else
		{
			document.frm_1.action ="ingresarQuiz.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>";
			document.frm_1.submit()
		}
	}
	
</script></head>
<body>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
	      	<td valign="top" class="tdGeneral">
            	<table class="tablaMarquesina">
	              <tr>
<?	 				
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($mat) = mysql_fetch_row($resultado); 
?>	  
                <td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($mat))?></a><a> -> </a><a href="verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a><a> -> Ingresar Previo</a> </td>
              </tr>
            </table>&nbsp;
<?
	if ($_GET['estado']==3)
	{ //If estado 3
		//list($idgrupo, $grupo) = explode("-", $_POST["hid_grupo"]);
		list($idtipoprevio, $tipoprevio) = explode('-',$_POST["cbo_tipoprevio"]);	
			
		list ($idnota, $nota) = explode("-", $_POST["hid_nota"]);
		//******************GRUPO*************************
		if (!empty($_POST["cbo_grupo"]))
			$opcion = $_POST["cbo_grupo"];
		else $opcion = $_POST["hid_grupo"];
		
		list($idgrupo, $grupo) = explode("-", $opcion);
		//************************************************
		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo 
				WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND 
				mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."'";
		}
		else
		{
			$sql = "SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evagrupo 
				WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND 
				mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'";
		}
		//print $sql."<p>";
		$resultado = $baseDatos->ConsultarBD($sql);
		$i=0;
		while ( list($id) = mysql_fetch_row($resultado) )
		{
			$veva[$i] = $id;
			$i++;
		}
		if ( count($veva)>0 )
			$ideva_ex = implode(',', $veva);
		else $ideva_ex = 0;
		//print $ideva_ex."<p>";
		
		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval FROM mei_evaluacion, mei_evavirgrupo
				WHERE mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo='".$idgrupo."' AND 
				mei_evaluacion.tipoevaluacion='1' AND mei_evaluacion.idevaluacion NOT IN (".$ideva_ex.")";
		}
		else
		{
			$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval FROM mei_evaluacion, mei_evagrupo
				WHERE mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo='".$idgrupo."' AND 
				mei_evaluacion.tipoevaluacion='1' AND mei_evaluacion.idevaluacion NOT IN (".$ideva_ex.")";
		}
		//print $sql."<p>";
		$resultado = $baseDatos->ConsultarBD($sql);	
					
		if ( mysql_num_rows($resultado) > 0 )
		{//IF RESULTADO
			$sql = "SELECT * FROM mei_tiposubgrupo";
			$resultado2 = $baseDatos->ConsultarBD($sql);		
?>
<form name="frm_ingresarEvaluacion" method="post" action="">
  <table width="100%" class="tablaGeneral">
    <tr class="trSubTitulo">
      <td width="20%" align="left" valign="middle"><b>Autor:</b></td>
      <td width="80%" colspan="3"><input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_POST["hid_materia"]?>">
        <input name="hid_tipoprevio" type="hidden" id="hid_tipoprevio" value="<?=$idtipoprevio?>">
          <input name="hid_grupo" type="hidden" id="hid_grupo" value="<?=$idgrupo?>">
          <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Grupo:</b></td>
      <td><?=$grupo?></td>
      <td><strong>Subgrupo:</strong></td>
      <td><select name="cbo_subgrupo" id="cbo_subgrupo">
        <option value="NULL">Previo No Grupal</option>
<?
			while ( list($idtipo, $subgrupo) = mysql_fetch_row($resultado2) )
			{
								print "<option class='link' value='$idtipo' ";
								if ( $idtipo == $_POST["hid_subgrupo"] )
									print "selected>";
								else print ">";
								print "$subgrupo</option>";
			}
?>
      </select></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b> Nota a la que pertenece:</b> </td>
      <td colspan="3"><select name="cbo_nota" onChange="javascript:document.frm_2.submit()">
          <?
									while ( list($ideva, $nomeva, $val_eval) = mysql_fetch_row($resultado) )
									{//WHILE RESULTADO
										print "<option class='link' value='$ideva' ";
										if ( $ideva == $_POST["hid_nota"] )
											print "selected>";
										else print ">";
										print "$nomeva --- ".($val_eval*100)."%</option>";
									}//FIN WHILE RESULTADO
							  ?>
      </select></td>
    </tr>
    <tr class="trInformacion" >
      <td align="left" valign="middle"><b>Nombre del Previo:  </b></td>
      <td colspan="3"><input name="txt_titulo" type="text" id="txt_titulo" size="80"></td>
    </tr>
    <tr class="trInformacion">
      <td width="20%" align="left" valign="middle"><b>Fecha Activaci&oacute;n:</b> </td>
      <td width="40%"><? 
						/*if(empty($_POST['hid_fecha']))
							$fecha_a->m_valor='';
						else
							$fecha_a->m_valor=$_POST['hid_fecha'];*/
							$fecha_a->CrearFrmCalendario(); 
						?></td>
      <td width="10%"><b>Hora Activaci&oacute;n:</b> </td>
      <td width="30%" ><select name="cbo_hora_a" id="cbo_hora_a">
          <?
			for ($i=0;$i<=23;$i++)
			{
				if ($i<10) $i = "0".$i;
				print "<option value='$i'>$i</option>";
			}
		  ?>
        </select>
        :
  <select name="cbo_minuto_a" id="cbo_minuto_a">
    <?
		for ($i=0;$i<=55;$i=$i+5)
		{
			if ($i<10) $i = "0".$i;
			print "<option value='$i'>$i</option>";
		}
	?>
</select></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Fecha Finalizaci&oacute;n:</b> </td>
      <td><? 
								/*if(empty($_POST['hid_fecha']))
									$fecha_f->m_valor='';
								else
									$fecha_f->m_valor=$_POST['hid_fecha'];*/
									$fecha_f->CrearFrmCalendario(); 
								?></td>
      <td><b>Hora Finalizaci&oacute;n:</b> </td>
      <td><select name="cbo_hora_f" id="select2">
          <?
								for ($i=0;$i<=23;$i++)
								{
									if ($i<10) $i = "0".$i;
									print "<option value='$i'>$i</option>";
								}
							?>
        </select>
        :
  <select name="cbo_minuto_f" id="select3">
    <?
		for ($i=0;$i<=55;$i=$i+5)
		{
			if ($i<10) $i = "0".$i;
			print "<option value='$i'>$i</option>";
		}
	?>
</select></td>
    </tr>
    <tr class="trInformacion">
      <td height="24" align="left" valign="middle"><b>Estado:</b></td>
      <td colspan="3"><select name="cbo_estado" id="cbo_estado" class="link"  >
                            <option value="1">Activa</option>
                            <option value="0" selected>Inactiva</option>
                          </select></td></tr>
   <?php /*?> <tr class="trInformacion">
      <td height="24" align="left" valign="middle"><b>Clave:</b></td>
       <td ><input name="txt_clave" type="text" id="txt_clave" size="30" value=""></td>
    </tr><?php */?>
	<?php /*?> cambiarlo de sitio para que no se ingrese en cada evaluacion<tr><td height="24" align="left" valign="middle" ><b> Rango Direccion Ip</b></td>
		    <td>
				  <input name="direccionIp1" id="direccionIp1" type="text" value="direccionIp1" > </td>
				  <td><input name="direccionIp2" id="direccionIp2" type="text" value="direccionIp2">
				      
					   </td> 	  
    </tr><?php */?>
    <tr class="trInformacion">
      <td height="24" align="left" valign="middle"><b>Comentarios:</b></td>
      <td colspan="3"><?								 
								 if (!empty($_POST["hid_editor"]) ) 
								 	$editor->Value = $_POST["hid_editor"];
								 $editor->crearEditor();
							?></td>
    </tr>
	<tr class="trInformacion">
	<td height="24" align="left" valign="middle"><b> Contraseña: </b> </td>
	<td colspan="3"><input name="txt_clave" type="text" id="txt_clave" size="30" value=""></td> 
    </tr>
  </table>
  
  <table class="tablaGeneral">
    <tr class="trSubTitulo">
      <td colspan="4">Caracteristicas del Previo: </td>
    </tr>
    <tr class="trInformacion">
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>N&uacute;mero de Intentos:</b> </td>
      <td align="left" valign="middle"><select name="cbo_intentos" id="cbo_intentos" >
        <?
		  
			for ($i=1;$i<=10;$i++)
			{
				print "<option value='$i'>$i</option>";
			}	
		?>
        </select></td>
      <td align="left" valign="middle"><strong>Disminuir la Nota de cada intento en: </strong></td>
      <td align="left" valign="middle"><input name="cbo_valorIntento" type="text" id="bo_calificacion" size="10" maxlength="5" value="10">
        %</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Forma de Calificar: </strong></td>
      <td align="left" valign="middle"><select name="cbo_calificar" id="cbo_calificar" >
	  <?
	  	$sql = "SELECT mei_evamodcalificar.idmodcalificar, mei_evamodcalificar.modcalificar FROM mei_evamodcalificar";
		$resultado = $baseDatos->ConsultarBD($sql);
		
		while ( list($idmod, $mod) = mysql_fetch_row($resultado) )
		{
			print "<option value = '$idmod'>$mod</option>'";
		}
		
	  ?>
      </select></td>
      <td align="left" valign="middle"><b>Tiempo del Previo: </b></td>
      <td align="left" valign="middle"><input name="txt_tiempo" type="text" id="txt_tiempo" size="10" value="60">
	  <!--
	  <select name="cbo_tiempo" id="cbo_tiempo">
        <? /*
			for ($i=15;$i<=120;$i=$i+15)
			{
				if ($i<10) $i = "0".$i;
				print "<option value='$i'>$i Minutos</option>";
			}*/
		?>
      </select>--> Minutos	  </td>
    </tr>
    
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Barajar Preguntas:</b></td>
      <td align="left" valign="middle"><select name="cbo_bpreguntas" id="cbo_bpreguntas">
        <option value="1" selected>Si</option>
        <option value="0" >No</option>
      </select></td>
      <td align="left" valign="middle"><b>Barajar Respuestas:</b></td>
      <td align="left" valign="middle"><select name="cbo_brespuestas" id="cbo_brespuestas">
        <option value="1" selected>Si</option>
        <option value="0" >No</option>
      </select></td>
    </tr>
    
    <tr class="trInformacion">
      <td height="21"><strong>Mostrar Nota: </strong></td>
      <td><select name="cbo_mnota" id="cbo_mnota">
        <option value="1" selected>Si</option>
        <option value="0" >No</option>
      </select>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td height="21">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  
  <table class="tablaMarquesina">
    <tr align="center" valign="middle">
      <td><input type="button" name="btn_guardarEvaluacion" value="Continuar" onClick="javascript:guardar()"></td>
      <td><input type="button" name="btn_cancelar" value="Cancelar" 
							  onClick="javascript:cancelar('<?=$_POST["hid_materia"]?>', '<?=$materia?>')"></td>
    </tr>
  </table>
</form>
<?
		}//FIN IF RESULTADO
		else
		{//else sin nota evaluacion
 ?>
&nbsp;
<table class="tablaGeneral">
  <tr class="trSubTitulo">
    <td colspan="2" align="center" valign="middle">No se puede Crear el Previo</td>
  </tr>
  <tr class="trInformacion">
    <td colspan="2" align="left" valign="middle"><p><strong>Posibles Causas:</strong></p>
        <ul>
          <li>No existen Notas de Evaluaciones definidas para el grupo <strong>
            <?=$grupo?>
          </strong></li>
          <br>
          <li>Todas las Notas de Tipo Previo ya tiene asiganadas Evaluaciones.</li>
        </ul>
      Para crear Notas haga click <a href="../notas/index.php?idmateria=<?=$_POST["hid_materia"]?>&materia=<?=$_GET['materia']?>" class="link"><strong><em>Aqui</em></strong></a> </td>
  </tr>
  <tr class="trInformacion">
    <td colspan="2" align="center" valign="middle">&nbsp;</td>
  </tr>
</table>
<?
		}//fin else sin nota evaluacion
?>&nbsp;
  <table class="tablaMarquesina" width="200">
	<tr align="center" valign="middle">
		<td><a href="ingresarEvaluacion.php?idmateria=<?=$_POST["hid_materia"]?>&materia=<?=$_GET["materia"]?>" class="link">Volver</a></td>
	</tr>
  </table>
<?
	}//If estado 3
	else
	{//INICIO
		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_virgrupo.idvirgrupo, mei_virgrupo.nombre FROM `mei_virgrupo`, mei_relusuvirgru 
				WHERE mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo AND 
				mei_relusuvirgru.idusuario=".$_SESSION["idusuario"]." AND 
				mei_virgrupo.idvirmateria=".$_GET["idmateria"];
		}
		else
		{
			$sql = "SELECT mei_grupo.idgrupo, mei_grupo.nombre FROM `mei_grupo`, mei_relusugru 
				WHERE mei_grupo.idgrupo = mei_relusugru.idgrupo AND 
				mei_relusugru.idusuario=".$_SESSION["idusuario"]." AND 
				mei_grupo.idmateria=".$_GET["idmateria"];
		}
				
		$resultado = $baseDatos->ConsultarBD($sql);				  
		
		if ( mysql_num_rows($resultado) > 0 )
		{//IF SI EXISTEN GRUPOS
			?>

	<form name="frm_1" method="post" action="?estado=3&materia=<?=$_GET["materia"]?>">
				  <table class="tablaGeneral" border="0">
                    <tr class="trTitulo">
                      <td colspan="2" align="left" valign="middle">Ingresar Evaluaci&oacute;n
                        <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_GET["idmateria"]?>"></td>
                    </tr>
                    
                    <tr class="trSubTitulo">
                      <td width="20%"><b>Autor:</b></td>
                      <td>&nbsp;<?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?></td>
                    </tr>
                    
                    <tr class="trInformacion">
                      <td><b>Grupo:</b></td>
                      <td align="left" valign="middle">
					 <select class="link" name="cbo_grupo" id="cbo_grupo" onChange="javascrip:document.frm_1.submit()">
			              
						   <?php /*?><select name="cbo_grupo"><?php */?>
					  			<?		
									while ( list($idgrupo, $grupo) = mysql_fetch_row($resultado) )
									{
										print "<option value='$idgrupo-$grupo'>$grupo</option>";
									}									
								?>
			            </select>
                        <label></label></td>
                    </tr>
                    <tr class="trInformacion">
                      <td><strong>Tipo de Evaluaci&oacute;n: </strong></td>
                      <?if($_SESSION['idtipousuario']==5){?>	
                      <td align="left" valign="middle"><input type="hidden" name="cbo_tipoprevio" id="cbo_tipoprevio" value="2">
Previo de Reposición</td>
<?}else{?>
	<td align="left" valign="middle"><input type="hidden" name="cbo_tipoprevio" id="cbo_tipoprevio" value="2">
Quiz de Reposición</td>
<?}?>
                    </tr>
                    <tr class="trInformacion">
                     <?php /*?> <td><strong>Evaluaci&oacute;n Aleatoria </strong></td>
                      <td align="left" valign="middle"><select name="tipoEvaluacion" ><option value="si">Si</option>
					                                                                  <option value="no">No </option> </select>
					                                    </td><?php */?>
                    </tr>
                    <tr class="trSubTitulo">
                      <td colspan="2"><div align="center">
                        <input type="button" name="Submit" value="Continuar" onClick="javascript: enviar();">
                      </div></td>
                    </tr>
                  </table>
  <?
		}//FIN IF SI EXISTEN GRUPOS
		else 
		{
			echo "No existen grupos definidos en esta materia";
 ?>
	<table class="tabalGeneral">
  		<tr>
			<td class="trInformacion"> No existen grupos definidos en esta materia </td>
		</tr>
	</table>
<?
		}
?>
				  <table class="tablaMarquesina" width="200">
                    <tr align="center" valign="middle">
                      <td><a href="verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Volver</a></td>
                    </tr>
                  </table>
		    </form>
<?
	}//FIN INICIO

?>
		  </td>
        <td class="tablaEspacio">&nbsp;</td>
        <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>