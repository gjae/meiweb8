<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ("../editor/fckeditor.php") ;
	include_once ('../calendario/FrmCalendario.class.php');
	include_once ('evaclass.php');

	if(comprobarSession())
	{
		$baseDatos=new BD();
		list ($idmateria, $materia) = explode("*", $_POST["hid_materia"]);

		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{
			if (empty($_POST["cbo_valor"])) $valor = 0;
			else $valor = $_POST["cbo_valor"];
		//*************************ARREGLO DE LA FECHA***********************************
			list($agno_a, $mes_a, $dia_a) = explode("-",$_POST['txt_fecha_a']);
			if ($mes_a<10) $mes_a = "0".$mes_a;
			if ($dia_a<10) $dia_a = "0".$dia_a;

			list($agno_f, $mes_f, $dia_f) = explode("-",$_POST['txt_fecha_f']);
			if ($mes_f<10) $mes_f = "0".$mes_f;
			if ($dia_f<10) $dia_f = "0".$dia_f;
		//**********************************************************************************

			$fecha_a = $agno_a.$mes_a.$dia_a.$_POST["cbo_hora_a"].$_POST["cbo_minuto_a"];
			$fecha_f = $agno_f.$mes_f.$dia_f.$_POST["cbo_hora_f"].$_POST["cbo_minuto_f"];

		/*print $fecha_a."<p>";
		print $fecha_f."<p>";*/



		//**********************************************************************************

		/*if ( $_GET["modo"] == 'E' )
		{
			$sql = "UPDATE mei_evaprevio SET
					idevaluacion = '".$_POST['cbo_nota']."',
					titulo = '".$_POST['txt_titulo']."',
					fechaactivacion = '".$fecha_a."',
					fechafinalizacion = '".$fecha_f."',
					comentario = '".base64_encode($_POST['edt_comentario'])."',
					direccionIp1 = '".$_POST['direccionIp1']."',
					direccionIp2 = '".$_POST['direccionIp2']."',
					tiempo = '".round($_POST['txt_tiempo'])."',
					barajarpreg = '".$_POST['cbo_bpreguntas']."',
					barajarresp = '".$_POST['cbo_brespuestas']."',
					intentos = '".$_POST['cbo_intentos']."',
					valorIntento = '".round(($_POST["cbo_valorIntento"]/100),2)."',
					idmodCalificar = '".$_POST["cbo_calificar"]."',
					mostrarnota = '".$_POST["cbo_mnota"]."',
					estado = '".$_POST['cbo_estado']."',
					valor = '".$valor."',
					idtiposubgrupo = ".$_POST["cbo_subgrupo"]."
					WHERE mei_evaprevio.idprevio = '".$_POST['hid_previo']."'";
			$baseDatos->ConsultarBD($sql);
			$idprevio = $_POST['hid_previo'];
			redireccionar("llenarEvaluacion.php");
		}*/
		$incentivo=0;
		$curva=0;
		$curva=$_POST['incentivo_curva'];
		
		if($curva!="curva"){
			$incentivo=$_POST['incentivo_valor'];
			$curva=0;
		}else{
			$curva=1;
		}
		
			if ( $_GET["modo"] == 'E' )
			{ 
				if($_GET['esp']!='reposicion')
				{
					$sql = "UPDATE mei_evaprevio SET
					idevaluacion = '".$_POST['cbo_nota']."',
					titulo = '".$_POST['txt_titulo']."',
					fechaactivacion = '".$fecha_a."',
					fechafinalizacion = '".$fecha_f."',
					comentario = '".base64_encode($_POST['edt_comentario'])."',
					clave= '".$_POST['txt_clave']."',
					tiempo = '".round($_POST['txt_tiempo'])."',
					barajarpreg = '".$_POST['cbo_bpreguntas']."',
					barajarresp = '".$_POST['cbo_brespuestas']."',
					intentos = '".$_POST['cbo_intentos']."',
					valorIntento = '".round(($_POST["cbo_valorIntento"]/100),4)."',
					idmodCalificar = '".$_POST["cbo_calificar"]."',
					mostrarnota = '".$_POST["cbo_mnota"]."',
					estado = '".$_POST['cbo_estado']."',
					valor = '".$valor."',
					incentivo = '".$incentivo."',
					curva = '".$curva."',
					idtiposubgrupo = ".$_POST["cbo_subgrupo"]."
					WHERE mei_evaprevio.idprevio = '".$_POST['hid_previo']."'";

					$baseDatos->ConsultarBD($sql);

					//INICIO MODIFICACION MENSAJE CALENDARIO



					if(!comprobarEditor($_POST['edt_comentario']))
					{
						$descripcion=$_POST['txt_titulo'];
					}
					else
					{
	            	 	$descripcion=$_POST['edt_comentario'];
	            	}

					registrarBitacora(2,5,false);
					if ($_SESSION['idtipousuario']==5)
						$sql="SELECT mei_relcalvirgru.idcalendario FROM mei_relcalvirgru WHERE mei_relcalvirgru.idprevio='".$_POST['hid_previo']."' ";
					else
						$sql="SELECT mei_relcalgru.idcalendario FROM mei_relcalgru WHERE mei_relcalgru.idprevio='".$_POST['hid_previo']."' ";
					$resultado=$baseDatos->ConsultarBD($sql);
              	    list ($idCalendario) = mysql_fetch_row($resultado);


					$sql="UPDATE mei_calendario
					SET mensaje = '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."',
					fechamensaje = '".$_POST['txt_fecha_a']."',
					estado = '0',
					fechacreacion = '".date('Y-n-j')."' ,
					cartelera = '1' ,
					destino = '1'
					WHERE `idcalendario` ='$idCalendario' ";

					$consulta=$baseDatos->ConsultarBD($sql);
					
					$sql="UPDATE mei_calendario_evaquiz
					SET mensaje = '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."',
					fechamensaje = '".$_POST['txt_fecha_a']."',
					estado = '0',
					fechacreacion = '".date('Y-n-j')."' ,
					cartelera = '1' ,
					destino = '1'
					WHERE `idcalendario` ='$idCalendario' ";

					$consulta=$baseDatos->ConsultarBD($sql);					

					//FIN MODIFICACION MENSAJE CALENDARIO


				}
			   else
				{
					$sql = "UPDATE mei_evaquiz SET
					idevaluacion = '".$_POST['cbo_nota']."',
					titulo = '".$_POST['txt_titulo']."',
					fechaactivacion = '".$fecha_a."',
					fechafinalizacion = '".$fecha_f."',
					comentario = '".base64_encode($_POST['edt_comentario'])."',
					clave= '".$_POST['txt_clave']."',
					tiempo = '".round($_POST['txt_tiempo'])."',
					barajarpreg = '".$_POST['cbo_bpreguntas']."',
					barajarresp = '".$_POST['cbo_brespuestas']."',
					intentos = '".$_POST['cbo_intentos']."',
					valorIntento = '".round(($_POST["cbo_valorIntento"]/100),4)."',
					idmodCalificar = '".$_POST["cbo_calificar"]."',
					mostrarnota = '".$_POST["cbo_mnota"]."',
					estado = '".$_POST['cbo_estado']."',
					valor = '".$valor."',
					incentivo = '".$incentivo."',
					curva = '".$curva."',
					idtiposubgrupo = ".$_POST["cbo_subgrupo"]."
					WHERE mei_evaquiz.idprevio = '".$_POST['hid_previo']."'";

					$baseDatos->ConsultarBD($sql);
				}


				 $sql1=" SELECT mei_evaprevio.estado FROM mei_evaprevio WHERE mei_evaprevio.idprevio ='".$_POST['hid_previo']."' ";
             	 $resultado=$baseDatos->ConsultarBD($sql1);
              	 list ($estadop) = mysql_fetch_row($resultado);
              	 if ($estadop==1)
              	 {
              	 	$sql=" SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio WHERE mei_evaprevio.idprevio ='".$_POST['hid_previo']."' ";
             	    $resultado=$baseDatos->ConsultarBD($sql);
              	    list ($ideval) = mysql_fetch_row($resultado);

					if ($_SESSION['idtipousuario']==5)
              	 		$sql=" SELECT mei_evavirgrupo.idvirgrupo FROM mei_evavirgrupo WHERE mei_evavirgrupo.idevaluacion =$ideval ";
					else
              	 		$sql=" SELECT mei_evagrupo.idgrupo FROM mei_evagrupo WHERE mei_evagrupo.idevaluacion =$ideval ";
             	    $resultado=$baseDatos->ConsultarBD($sql);
             	    list ($idgrup) = mysql_fetch_row($resultado);

					if ($_SESSION['idtipousuario']==5)
	             	    $sql=" SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo =$idgrup ";
					else
	             	    $sql=" SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idgrupo =$idgrup ";
             	    $resultado=$baseDatos->ConsultarBD($sql);

             	    $sql="SELECT mei_ipactivo.idusuario FROM mei_ipactivo";
             	    $consultaIp=$baseDatos->ConsultarBD($sql);
              	 	$cont=0;

             	 	$alumnos = array ();
     		 	 	while(list($idusu)=mysql_fetch_array($resultado))
             	 	{
             	 		$alumnos[$cont] = $idusu;
             	 		$cont++;
            	 	}


              	 	while(list($idusuario)=mysql_fetch_array($consultaIp))
    			 	{


                      foreach ($alumnos as $idusuIP)
                      {
      		                 if($idusuario == $idusuIP )
      		 				 {
        			  			$contador++;
        			  			$sql="DELETE FROM mei_ipactivo WHERE mei_ipactivo.idusuario=".$idusuario;
        						$baseDatos->ConsultarBD($sql);
      				       	 }

      				  }

    			 	}



              	 }

				$idprevio = $_POST['hid_previo'];
				redireccionar("llenarEvaluacion.php?esp=".$_GET['esp']."&idmateria=".$_POST["hid_materia"]."&idgrupo=".$_GET['idgrupo']."&materia=".$_GET['materia']);

		    }

			elseif($_GET["modo"] == 'DEL')
			{
				if($_GET['esp']!='reposicion')
					$sql = "DELETE FROM mei_evaprevio WHERE mei_evaprevio.idprevio=".$_GET["idprevio"];
				else
					$sql = "DELETE FROM mei_evaquiz WHERE mei_evaquiz.idprevio=".$_GET["idprevio"];
				$baseDatos->ConsultarBD($sql);

				//INICIO BORRADO MENSAJE CALENDARIO
					if ($_SESSION['idtipousuario']==5)
	   					$sql = "SELECT mei_relcalvirgru.idcalendario FROM mei_relcalvirgru WHERE mei_relcalvirgru.idprevio ='".$_GET["idprevio"]."' ";
					else
	   					$sql = "SELECT mei_relcalgru.idcalendario FROM mei_relcalgru WHERE mei_relcalgru.idprevio ='".$_GET["idprevio"]."' ";
	                $resultado=$baseDatos->ConsultarBD($sql);
	                list ($idCalendario) = mysql_fetch_row($resultado);

	   				registrarBitacora(2,6,false);

					$sql="DELETE FROM mei_calendario WHERE mei_calendario.idcalendario = '$idCalendario' ";
					$consulta=$baseDatos->ConsultarBD($sql);
					
					$sql="DELETE FROM mei_calendario_evaquiz WHERE mei_calendario_evaquiz.idcalendario = '$idCalendario' ";
					$consulta=$baseDatos->ConsultarBD($sql);					


	            //FIN BORRADO MENSAJE CALENDARIO

				redireccionar("verEvaluacion.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']);
			}
			elseif($_GET["modo"] == 'ACT')
			{
	  			$status=$_GET["estado"];

	            if ($status==1)
	            {

				 	 $sql=" SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio WHERE mei_evaprevio.idprevio ='".$_GET["idprevio"]."' ";
	             	 $resultado=$baseDatos->ConsultarBD($sql);
	              	 list ($ideval) = mysql_fetch_row($resultado);

					 if ($_SESSION['idtipousuario']==5)
	             	 	$sql=" SELECT mei_evavirgrupo.idvirgrupo FROM mei_evavirgrupo WHERE mei_evavirgrupo.idevaluacion =$ideval ";
					else
	             	 	$sql=" SELECT mei_evagrupo.idgrupo FROM mei_evagrupo WHERE mei_evagrupo.idevaluacion =$ideval ";
	             	 $resultado=$baseDatos->ConsultarBD($sql);
	             	 list ($idgrup) = mysql_fetch_row($resultado);

					 if ($_SESSION['idtipousuario']==5)
	             	 	$sql=" SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo =$idgrup ";
					else
	             	 	$sql=" SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idgrupo =$idgrup ";
	             	 $resultado=$baseDatos->ConsultarBD($sql);

	            	 $sql="SELECT mei_ipactivo.idusuario FROM mei_ipactivo";
	             	 $consultaIp=$baseDatos->ConsultarBD($sql);
	              	 $cont=0;

	             	 $alumnos = array ();
	     		 	 while(list($idusu)=mysql_fetch_array($resultado))
	             	 {
	             	 	$alumnos[$cont] = $idusu;
	             	 	$cont++;
	            	 }


	              	 while(list($idusuario)=mysql_fetch_array($consultaIp))
	    			 {


	                      foreach ($alumnos as $idusuIP)
	                      {
	      		                 if($idusuario == $idusuIP )
	      		 				 {
	        			  			$contador++;
	        			  			$sql="DELETE FROM mei_ipactivo WHERE mei_ipactivo.idusuario=".$idusuario;
	        						$baseDatos->ConsultarBD($sql);
	      				       	 }

	      				  }

	    			 }


	            }



	      			if($_GET['esp']!='reposicion')
						{$sql = "UPDATE mei_evaprevio SET
						mei_evaprevio.estado = '".$_GET["estado"]."'
						WHERE mei_evaprevio.idprevio = '".$_GET["idprevio"]."'";}
					else
						{$sql = "UPDATE mei_evaquiz SET
						mei_evaquiz.estado = '".$_GET["estado"]."'
						WHERE mei_evaquiz.idprevio = '".$_GET["idprevio"]."'";}

						$baseDatos->ConsultarBD($sql);
				//print $sql;
					if ($_GET["tipo"]=='Q')
						redireccionar("../actividades/verActividad.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']);
					else
						redireccionar("verEvaluacion.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET['materia']);
			}
			else
			{
			/*$sql = "INSERT INTO `mei_evaprevio` ( `idprevio` , `idevaluacion` , `idautor` , `titulo` , `fechacreacion` ,
						`fechaactivacion` , `fechafinalizacion` , `comentario` , `tiempo` , `barajarpreg` , `barajarresp` ,
						`intentos` , `valorIntento` , `idmodCalificar` , `mostrarnota` , `estado` , `clave`,`idtipoprevio` , `valor`, `idtiposubgrupo` )
						VALUES ('','".$_POST["cbo_nota"]."',
						'".$_SESSION["idusuario"]."', '".$_POST["txt_titulo"]."', '".date('YmdHi')."', '".$fecha_a."',
						'".$fecha_f."', '".base64_encode($_POST["edt_comentario"])."', '".round($_POST["txt_tiempo"])."',
						'".$_POST["cbo_bpreguntas"]."', '".$_POST["cbo_brespuestas"]."', '".$_POST["cbo_intentos"]."',
						'".round(($_POST["cbo_valorIntento"]/100),2)."', '".$_POST["cbo_calificar"]."', '".$_POST["cbo_mnota"]."',
						'".$_POST["cbo_estado"]."', '".base64_encode($_POST['txt_clave'])."', '".$_POST["hid_tipoprevio"]."', '".$valor."', ".$_POST["cbo_subgrupo"].")";*/

				$tiposubgruponulo=0;							
				if ("".$_POST["cbo_subgrupo"]=="NULL")
					{
						$tiposubgruponulo=1;
						$_POST["cbo_subgrupo"]=1;
						$q = "";
						$w = "";
						
					}else
					{
						$w = ",'".$_POST["cbo_subgrupo"]."'";
						$q = ",idtiposubgrupo";
					}								
				if($_GET['esp']!='reposicion')
			
				$sql = "INSERT INTO `mei_evaprevio` ( `idprevio` , `idevaluacion` , `idautor` , `titulo` , `fechacreacion` ,
						`fechaactivacion` , `fechafinalizacion` , `comentario` , `clave`,`tiempo` , `barajarpreg` , `barajarresp` ,
						`intentos` , `valorIntento` , `idmodCalificar` , `mostrarnota` , `estado` , `idtipoprevio` , `valor`".$q.", `incentivo`, `curva`  )
						VALUES ('','".$_POST["cbo_nota"]."',
						'".$_SESSION["idusuario"]."', '".$_POST["txt_titulo"]."', '".date('YmdHi')."', '".$fecha_a."',
						'".$fecha_f."', '".base64_encode($_POST["edt_comentario"])."','".$_POST["txt_clave"]."', '".round($_POST["txt_tiempo"])."',
						'".$_POST["cbo_bpreguntas"]."', '".$_POST["cbo_brespuestas"]."', '".$_POST["cbo_intentos"]."',
						'".round(($_POST["cbo_valorIntento"]/100),4)."', '".$_POST["cbo_calificar"]."', '".$_POST["cbo_mnota"]."',
						'".$_POST["cbo_estado"]."', '".$_POST["hid_tipoprevio"]."', '".$valor."'".$w.",'".$incentivo."','".$curva."')";
				else
			    $sql = "INSERT INTO `mei_evaquiz` ( `idprevio` , `idevaluacion` , `idautor` , `titulo` , `fechacreacion` ,
						`fechaactivacion` , `fechafinalizacion` , `comentario` , `clave` , `tiempo` , `barajarpreg` , `barajarresp` ,
						`intentos` , `valorIntento` , `idmodCalificar` , `mostrarnota` , `estado` , `idtipoprevio` , `valor`".$q.", `incentivo`, `curva`  )
						VALUES ('','".$_POST["cbo_nota"]."',
						'".$_SESSION["idusuario"]."', '".$_POST["txt_titulo"]."', '".date('YmdHi')."', '".$fecha_a."',
						'".$fecha_f."', '".base64_encode($_POST["edt_comentario"])."','".$_POST["txt_clave"]."', '".round($_POST["txt_tiempo"])."',
						'".$_POST["cbo_bpreguntas"]."', '".$_POST["cbo_brespuestas"]."', '".$_POST["cbo_intentos"]."',
						'".round(($_POST["cbo_valorIntento"]/100),4)."', '".$_POST["cbo_calificar"]."', '".$_POST["cbo_mnota"]."',
						'".$_POST["cbo_estado"]."', '".$_POST["hid_tipoprevio"]."', '".$valor."'".$w.",'".$incentivo."','".$curva."')";


				$baseDatos->ConsultarBD($sql);				
	            $idprevio = $baseDatos->InsertIdBD();	           
	            if ($tiposubgruponulo == 1)
	            {
					
					$sql = "UPDATE mei_evaprevio SET
					idtiposubgrupo = 'NULL'
					WHERE mei_evaprevio.idprevio = '".$idprevio."'";

					$baseDatos->ConsultarBD($sql);
					
	            }
				$evaluacion = new evaluacion($idprevio,$idmateria,$_POST["hid_tipoprevio"],'');
				$_SESSION["evaluacion"] = $evaluacion;
			

	        //INICIO Agregar en el Calendario y cartelera. (Creacion por primera ves de una evaluacion)

				if(!empty($_POST['txt_titulo']))
				{

					if(!comprobarEditor($_POST['edt_comentario']))
					{
						$descripcion=$_POST['txt_titulo'];
					}
					else
					{
	            	 	$descripcion=$_POST['edt_comentario'];
	            	}


					registrarBitacora(2,2,false);

					/*$sql="INSERT INTO mei_calendario ( idcalendario , mensaje , fechamensaje , estado , idusuario , fechacreacion , cartelera, destino)
						 VALUES ('', '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."', '".$_POST['txt_fecha_a']."', '0',
						 '".$_SESSION['idusuario']."', '".date('Y-n-j')."' , '1' , '1' )";
					$baseDatos->ConsultarBD($sql);

					$idCalendario=$baseDatos->InsertIdBD();*/
					$tb_mei_calendario_evaquiz = "SHOW TABLES LIKE 'mei_calendario_evaquiz' ";
					$resultaconulta = $baseDatos->ConsultarBD($tb_mei_calendario_evaquiz);
					if(mysql_fetch_row($resultaconulta)==false){
						$crearTabla_mei_calendario_evaquiz = "
							CREATE TABLE IF NOT EXISTS `mei_calendario_evaquiz` (
						  `idcalendario` int(11) NOT NULL AUTO_INCREMENT,
						  `idusuario` int(11) NOT NULL DEFAULT '0',
						  `mensaje` longtext COLLATE utf8_spanish_ci NOT NULL,
						  `fechamensaje` date NOT NULL DEFAULT '0000-00-00',
						  `estado` int(11) NOT NULL DEFAULT '0',
						  `fechacreacion` date NOT NULL DEFAULT '0000-00-00',
						  `cartelera` int(11) NOT NULL DEFAULT '0',
						  `destino` int(11) DEFAULT '0',
						  PRIMARY KEY (`idcalendario`),
						  KEY `idusuario` (`idusuario`)
						) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=10";
						
						$baseDatos->ConsultarBD($crearTabla_mei_calendario_evaquiz);
						
						$altertable_meicalendario_eva = "ALTER TABLE `mei_calendario_evaquiz` ADD CONSTRAINT `mei_calendario_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;";
						$baseDatos->ConsultarBD($altertable_meicalendario_eva);
						
						$sql="INSERT INTO mei_calendario_evaquiz ( idcalendario , mensaje , fechamensaje , estado , idusuario , fechacreacion , cartelera, destino)
							 VALUES ('', '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."', '".$_POST['txt_fecha_a']."', '0',
							 '".$_SESSION['idusuario']."', '".date('Y-n-j')."' , '1' , '1' )";
						$baseDatos->ConsultarBD($sql);

						$idCalendario=$baseDatos->InsertIdBD();							
					}else{
						/*$sql="INSERT INTO mei_calendario_evaquiz ( idcalendario , mensaje , fechamensaje , estado , idusuario , fechacreacion , cartelera, destino)
							 VALUES ('', '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."', '".$_POST['txt_fecha_a']."', '0',
							 '".$_SESSION['idusuario']."', '".date('Y-n-j')."' , '1' , '1' )";*/
							 
					$caducidad = 1;
					$fechaCaducidad = $_POST['txt_fecha_a'];
					$destino = 1;
					$sql="INSERT INTO mei_cartelera ( idcartelera , mensaje , estado , idusuario , fechacreacion , caducidad , fechacaducidad , destino) 
						VALUES ('', '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."', '0', '".$_SESSION['idusuario']."', '".date('Y-n-j')."', '".$caducidad."', '".$fechaCaducidad."' , ".$destino.")";
					echo $sql;
						$baseDatos->ConsultarBD($sql);

						$idCartelera=$baseDatos->InsertIdBD();					
					}

					if ($_SESSION['idtipousuario']==5){
	                	//$sql="INSERT INTO mei_relcalvirgru ( idcalendario , idvirgrupo , idprevio) VALUES ( '$idCalendario' , '".$_POST['hid_grupo']."' , '$idprevio' )";
						$sql="INSERT INTO mei_relcarvirgru ( idcartelera , idvirgrupo ) VALUES ('".$idCartelera."','".$_POST['hid_grupo']."')";
					}
					else{
	                	//$sql="INSERT INTO mei_relcalgru ( idcalendario , idgrupo , idprevio) VALUES ( '$idCalendario' , '".$_POST['hid_grupo']."' , '$idprevio' )";
						$sql="INSERT INTO mei_relcargru ( idcartelera , idgrupo ) VALUES ('".$idCartelera."','".$_POST['hid_grupo']."')";
					}

					$baseDatos->ConsultarBD($sql);

	 			}

			//FIN Agregar en el Calendario y cartelera. (Creacion por primera ves de una evaluacion)


			//redireccionar("llenarEvaluacion.php?esp=".$_GET['esp']."&idgrupo=".$_GET['idgrupo']."&idmateria=".$_POST["hid_materia"]."&materia=".$_GET['materia']);
			}
	}
	else redireccionar('../login/');
}
?>

