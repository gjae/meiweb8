<?
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../evaluaciones/previoClass.php');
	
if(comprobarSession())
{	
	$baseDatos= new BD();		
	$previo = $_SESSION["previo"];

	if ( empty($previo->idusuario) )
	{//YA ESTA CREADO EL OBJETO
		//****************************************************************	
		
		$sql = "SELECT mei_evaquiz.titulo, mei_evaquiz.comentario, mei_evaquiz.tiempo, 
				mei_evaquiz.barajarpreg, mei_evaquiz.barajarresp, mei_evaquiz.valorIntento,
				mei_evaquiz.idmodCalificar, mei_evaquiz.mostrarnota FROM mei_evaquiz 
				WHERE mei_evaquiz.idprevio = '".$_GET["idprevio"]."'";
	
		list( $titulo, $comentario, $tiempo, $bpreg, $bresp, $valorIntento, $mCalificar, $mnota ) = mysql_fetch_row($baseDatos->ConsultarBD($sql));
		
		//***********************INTENTO DEL ALUMNO**********************
		$sql = "SELECT mei_usuquiz.fecharespuesta ,	mei_usuquiz.nota  FROM mei_usuquiz WHERE mei_usuquiz.idquiz = '".$_GET["idprevio"]."' 
				AND mei_usuquiz.idusuario = '".$_SESSION['idusuario']."'";
		//print $sql."<p>"	;
		$resulusuprev = $baseDatos->ConsultarBD($sql);
		$usuIntento  = mysql_num_rows($resulusuprev)+1;
		//****************************************************************
		$tiempo *= 60;
		$valorIntento = (1 - ($usuIntento-1)*$valorIntento);
		//**********************************LLENADO DEL OBJETO*******************************************
		$previo = new previo($_SESSION['idusuario'],$_GET["idsubgrupo"],$_GET["idprevio"],$_GET["idmateria"],$titulo,stripslashes(base64_decode($comentario)),$usuIntento, $valorIntento, $mnota, $mCalificar);
		$previo->agregar_tiempo(($tiempo-$_POST["hid_tiempo"]), time());

		switch ($bpreg)
		{
			case 1://BARAJAR PREGUNTAS
				
				$sql = "SELECT mei_evadtllequiz.idpregunta FROM mei_evadtllequiz
						WHERE mei_evadtllequiz.idprevio = '".$_GET["idprevio"]."'";
				
				$resulpreg = $baseDatos->ConsultarBD($sql);
				
				$i = 0;
				while ( list($codpreg) = mysql_fetch_row($resulpreg) )
				{
					$idpregunta[$i] = $codpreg;
					$i++;
				}
				
				srand((double) microtime()* 1000000);				
				$idbpreg = array_rand($idpregunta, count($idpregunta));
				$numpreg = 1;
				while ( list(,$id) = each($idbpreg) )
				{//WHILE 1
					$sql = "SELECT mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,
							mei_evatipopregunta.tipo, mei_evadtllequiz.valor_pregunta 
							FROM mei_evadtllequiz, mei_evapreguntas, mei_evatipopregunta 
							WHERE mei_evadtllequiz.idprevio ='".$_GET["idprevio"]."'
							AND mei_evadtllequiz.idpregunta = mei_evapreguntas.idpregunta 
							AND mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
							AND mei_evapreguntas.idpregunta = '".$idpregunta[$id]."'";
					//print $sql."<p>";
					$resultado = $baseDatos->ConsultarBD($sql);
					
					list($titpreg, $tipo, $txt_tipo, $valor_preg) = mysql_fetch_row($resultado);
					$previo->agregar_preg($numpreg, $idpregunta[$id], stripslashes(base64_decode($titpreg)), $tipo, $txt_tipo, $valor_preg);//Agregar preguntas al objeto
					//and ( ($tipo == 1) or ($tipo == 2) or ($tipo == 4) )
					if ( ($bresp == 1)  )//BARAJAR RESPUESTAS PARA PREGUNTAS TIPO 1 Y 2
					{//BARAJAR RESPUESTAS
						$sql = "SELECT mei_evarespuestas.idrespuesta FROM mei_evarespuestas 
								WHERE mei_evarespuestas.idpregunta=".$idpregunta[$id];
						/*print "<blockquote>";
						print $sql."<p>";
						print "</blockquote>";*/
						$respuestas = $baseDatos->ConsultarBD($sql);
						
						$i = 0;
						while ( list($codresp) = mysql_fetch_row($respuestas) )
						{
							$idrespuesta[$i] = $codresp;
							$i++;
						}
						
						srand((double) microtime()* 1000000);				
						$idbresp = array_rand($idrespuesta, count($idrespuesta));
						$numresp = 1;
						while ( list(,$id) = each($idbresp) )
						{//WHILE 3
							//print "<blockquote>";
							$sql = "SELECT mei_evarespuestas.respuesta, mei_evarespuestas.valor 
									FROM mei_evarespuestas 
									WHERE mei_evarespuestas.idrespuesta=".$idrespuesta[$id];
							//print $sql."<p>";
							//print "</blockquote>";
							$respuestas = $baseDatos->ConsultarBD($sql);
							
							list($resp, $valor) = mysql_fetch_row($respuestas);
							$previo->agrega_respbd($numpreg, $numresp, $idrespuesta[$id], stripslashes(base64_decode($resp)), $valor);////Agregar respuestas a la pregunta
							$numresp++;
						}//FINWHILE 3
					}//FIN BARAJAR RESPUESTAS
					elseif( ($bresp == 0) )
					{// NO BARAJAR RESPUESTAS
					
						$sql = "SELECT mei_evarespuestas.idrespuesta, mei_evarespuestas.respuesta, 
								mei_evarespuestas.valor FROM mei_evarespuestas 
								WHERE mei_evarespuestas.idpregunta=".$idpregunta[$id];
						//print $sql."<p>";
						$respuestas = $baseDatos->ConsultarBD($sql);
					
						$numresp = 1;
						while ( list($idresp, $resp, $valor) = mysql_fetch_row($respuestas) )
						{
							$previo->agrega_respbd($numpreg, $numresp, $idresp, stripslashes(base64_decode($resp)), $valor);////Agregar respuestas a la pregunta
							$numresp++;
						}	
						$numpreg++;								
					}//FIN NO BARAJAR RESPUESTAS						
					$numpreg++;
					unset($idrespuesta);		
				}//FIN WHILE 1								
			break;//FIN BARAJAR PREGUNTAS
			
			case 0://NO BARAJAR PREGUNTAS
			
			$sql = "SELECT mei_evadtllequiz.idpregunta, mei_evapreguntas.pregunta, 
					mei_evapreguntas.tipo_pregunta,	mei_evatipopregunta.tipo, 
					mei_evadtllequiz.valor_pregunta FROM mei_evadtllequiz, mei_evapreguntas, mei_evatipopregunta  
					WHERE mei_evadtllequiz.idprevio ='".$_GET["idprevio"]."'
					AND mei_evadtllequiz.idpregunta = mei_evapreguntas.idpregunta
					AND mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta";
			//print $sql."<p>";
			$resultado = $baseDatos->ConsultarBD($sql);
			$numpreg = 1;
			while ( list($codpreg, $titpreg, $tipo, $txt_tipo, $valor_preg) = mysql_fetch_row($resultado) )
			{//WHILE 4
				$previo->agregar_preg($numpreg, $codpreg, stripslashes(base64_decode($titpreg)), $tipo, $txt_tipo, $valor_preg);//Agregar preguntas al objeto
				
				if ( ($bresp == 1) and ( ($tipo == 1) or ($tipo == 2) ) )//BARAJAR RESPUESTAS PARA PREGUNTAS TIPO 1 Y 2
				{//BARAJAR RESPUESTAS
					$sql = "SELECT mei_evarespuestas.idrespuesta FROM mei_evarespuestas 
							WHERE mei_evarespuestas.idpregunta=".$codpreg;
					
					//print $sql."<p>";
					
					$respuestas = $baseDatos->ConsultarBD($sql);
					
					$i = 0;
					while ( list($codresp) = mysql_fetch_row($respuestas) )
					{
						$idrespuesta[$i] = $codresp;
						$i++;
					}
					
					srand((double) microtime()* 1000000);				
					$idbresp = array_rand($idrespuesta, count($idrespuesta));
					/*print "No. idrespuesta: ".count($idrespuesta)."<p>";
					print "No. idbresp: ".count($idbresp)."<p>";*/
					$numresp = 1;
					while ( list(,$id) = each($idbresp) )
					{//WHILE 3
						$sql = "SELECT mei_evarespuestas.respuesta, Mei_evarespuestas.valor 
								FROM mei_evarespuestas 
								WHERE mei_evarespuestas.idrespuesta=".$idrespuesta[$id];
						$respuestas = $baseDatos->ConsultarBD($sql);
						
						list($resp, $valor) = mysql_fetch_row($respuestas);
						$previo->agrega_respbd($numpreg, $numresp, $idrespuesta[$id], stripslashes(base64_decode($resp)), $valor);////Agregar respuestas a la pregunta
						$numresp++;
					}//FINWHILE 3
				}//FIN BARAJAR RESPUESTAS
				elseif( $bresp == 0 )
				{// NO BARAJAR RESPUESTAS
				
					$sql = "SELECT mei_evarespuestas.idrespuesta, mei_evarespuestas.respuesta, 
							mei_evarespuestas.valor FROM mei_evarespuestas 
							WHERE mei_evarespuestas.idpregunta=".$codpreg;
					//print $sql."<p>";
					$respuestas = $baseDatos->ConsultarBD($sql);
				
					$numresp = 1;
					while ( list($idresp, $resp, $valor) = mysql_fetch_row($respuestas) )
					{
						$previo->agrega_respbd($numpreg, $numresp, $idresp, stripslashes(base64_decode($resp)), $valor);////Agregar respuestas a la pregunta
						$numresp++;
					}	
				}//FIN NO BARAJAR RESPUESTAS						
				$numpreg++;	
				unset($idrespuesta);				
			}//FIN WHILE 4			
			break;//FIN NO BARAJAR PREGUNTAS
		}			

		if (!empty($_GET["idsubgrupo"]))
		{
			$sql = "SELECT mei_relususub.idusuario FROM mei_relususub WHERE mei_relususub.idsubgrupo = '".$_GET["idsubgrupo"]."'";
			$ressubgrupo = $baseDatos->ConsultarBD($sql);
			$sql = "INSERT INTO `mei_usuquiz` ( `idquiz` , `idusuario` , `fecharespuesta` , `nota`) VALUES "; 
			while ( list($idusu) = mysql_fetch_row($ressubgrupo) )
			{
				$sql .= "('".$_GET["idprevio"]."', '".$idusu."', '".time()."', '0'),";
				$baseDatos->ConsultarBD($sql);				
			}
			$sql = rtrim($sql,",");
		}
		else
		{
			$sql = "INSERT INTO `mei_usuquiz` ( `idquiz` , `idusuario` , `fecharespuesta` , `nota` ) VALUES ('".$_GET["idprevio"]."', '".$_SESSION['idusuario']."',					'".time()."', '0')";
		}
		
		//print $sql;
		$baseDatos->ConsultarBD($sql);		
		$_SESSION["previo"] = $previo;
		redireccionar('responderEvaluacion.php');
		
	}//FIN YA ESTA CREADO EL OBJETO
	else redireccionar("../evaluaciones/responderEvaluacion.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
	//*****************************************************************	
}
else redireccionar('../login/');
?>