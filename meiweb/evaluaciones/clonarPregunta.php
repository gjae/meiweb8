<?
	include_once ('../menu/Menu.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{	
		$baseDatos= new BD();	
		$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraEstandar' , '' );
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="scripts.js"></script>
</head>
<script>

function validar(codtipo,materia)
{
	var contador_txt=0;
	var contador_int=0;	
	
	if ( (codtipo!=3) && (codtipo!=4)&& (codtipo!=5) && (codtipo!=6))
	{
	
		for(i=0;i<document.frm3.elements.length;i++)
		{			
			if (document.frm3.elements[i].id=='txt_respuesta')
			{
				if (document.frm3.elements[i].value!="")
				contador_txt++;							
			}
			if (document.frm3.elements[i].id=='int_respuesta')
			{
				if ( (document.frm3.elements[i].value>0) )
				contador_int++;							
			}		
		}
	
		if( (contador_txt>3) && (contador_int>0) )
		{
			document.frm3.submit();
		}
		else if(contador_txt<=4) alert('Debe llenar todos  los campos')
		else if(contador_int<=0) alert('Debe Seleccionar al menos una respuesta correcta')
	}
	else document.frm3.submit();
		
}




function cancelar(idmateria,materia)
{
	document.frm3.action = "ingresarPregunta.php?idmateria="+idmateria+"&materia="+materia;
	document.frm3.submit()
}
function cancelar2(idmateria,materia)
{
	document.frm2.action = "verPregunta.php?idmateria="+idmateria+"&materia="+materia;
	document.frm2.submit()
}
function cargarsubtema(idtema){
	if( idtema != "" ){
	  $.ajax({
					data:  ({ idtema : idtema }),
					url:   'cargarsubtemas.php',
					type:  'post',
					beforeSend: function () {
							$("#subtemacarga").html("Procesando, espere por favor...");
					},
					success:  function (response) {
							$("#subtema").html(response);
							document.getElementById("subtematr").style.display = 'table-row';
							document.getElementById("subtemacarga").style.display = 'none';
					}
			});							
	}	
}
function cargarsubtema2(idtema){
	if( idtema != "" ){
	  $.ajax({
					data:  ({ idtema : idtema }),
					url:   'cargarsubtemas.php',
					type:  'post',
					success:  function (response) {
							$("#subtema2").html(response);
							document.getElementById("subtemapost").style.display = 'none';
					}
			});							
	}	
}
</script>
<?php
	function buscartemapadre($codtema){
		global $baseDatos;
		$sqlbuscarpadre = "SELECT mei_tema.idtema, mei_tema.titulo, mei_tema.tipo, mei_tema.idtemapadre FROM mei_tema 
		WHERE mei_tema.idtema='".$codtema."' ";	
		$resultadopadre = $baseDatos->ConsultarBD($sqlbuscarpadre);
		list($codtema2, $nomtema2, $tipotema2, $idtemapadre) = mysql_fetch_row($resultadopadre);
		return $codtema2;
	}
	$estadoclonar = 3;
	$sql = "SELECT mei_usuario.primernombre, mei_usuario.primerapellido, mei_evapreguntas.pregunta ,mei_evapreguntas.idgrado, 
			mei_evapreguntas.idtema, mei_evapreguntas.tipo_pregunta,	mei_evatipopregunta.tipo, 
			mei_evapreguntas.autoevaluacion FROM mei_evapreguntas, mei_evatipopregunta, mei_usuario 
			WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
			mei_evapreguntas.idusuario = mei_usuario.idusuario AND 
			mei_evapreguntas.idpregunta = '".$_GET["idpregunta"]."'";
	//print $sql."<p>";	
	$resultado = $baseDatos->ConsultarBD($sql);
	list($nomAutor, $apeAutor, $pregunta, $idgrado, $idtema, $tipo_pregunta, $txt_tipo, $autoeva) = mysql_fetch_row($resultado);
	$codtipo = $tipo_pregunta;
	
	$sql = "SELECT mei_evarespuestas.idrespuesta, mei_evarespuestas.respuesta, mei_evarespuestas.valor 
			FROM mei_evarespuestas WHERE mei_evarespuestas.idpregunta = '".$_GET["idpregunta"]."'";
	//print $sql."<p>";	
	$resresultado = $baseDatos->ConsultarBD($sql);
?>
<body>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
		    <td valign="top" class="tdGeneral">
				<table class="tablaMarquesina">
					<tr>
<?	 				
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($mat) = mysql_fetch_row($resultado); 
?>	  
						<td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($mat))?></a><a> -> </a><a href="verPregunta.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Banco de Preguntas</a><a> -> Clonar Preguntas </a></td>
					</tr>
				</table><br>
<?
if ( $estadoclonar=='3' ) 
{//IF ESTADO 3 - Se escoje el tipo de pregunta
?>

<form name="frm3" method="post" action="guardarPregunta.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>">
  <table class="tablaGeneral" align="center">
    <tr class="trTitulo">
      <td height="20" colspan="2" align="left" valign="middle">Clonar Pregunta </td>
      </tr>
    <tr class="trInformacion">
      <td width="20%" height="20" align="left" valign="middle"><strong>Autor : </strong></td>
      <td width="80%" height="20" align="left" valign="middle"><strong>
          <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?>
          <input name="hid_materia" type="hidden" id="hid_materia"  value="<?=$_GET["idmateria"]?>">
          <input name="hid_idclonpregunta" type="hidden" id="hid_idclonpregunta"  value="<?=$_GET["idpregunta"]?>">
      </strong></td>
    </tr>
    
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Tema :</strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_tema" id="select2">
       <?
			/*Consultar Hijos*/
			$codtemapadre = "";
			$sqlconsultahijos = "SELECT mei_tema.idtema, mei_tema.titulo, mei_tema.idtemapadre FROM mei_tema 
					WHERE mei_tema.idtema='".$idtema."' ";
			$resultado_conhijos = $baseDatos->ConsultarBD($sqlconsultahijos);					
			list($codtema, $nomtema, $idtemapadre) = mysql_fetch_row($resultado_conhijos);
			if( $idtemapadre != "" ){
				$codtemapadre = buscartemapadre($idtemapadre);
				$idtema = $codtemapadre;
			}
			/*Fin Consultar Hijos*/

		  $sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
					WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.tipo=1";
			$resultado = $baseDatos->ConsultarBD($sql);
			while ( list($codtema, $nomtema) = mysql_fetch_row($resultado) )		
			{		
				print "<option value='$codtema-$nomtema' onclick='cargarsubtema2($codtema)'";
				if ( $codtema == $idtema )
					print "selected";
				
				print ">$nomtema</option>";			
			}
			mysql_free_result($resultado);
	  ?>
            </select></td>
    </tr>
	<?php if(isset($_POST["cbo_sub_tema"])): ?>
    <tr class="trInformacion" id="subtema">
      <td height="20" align="left" valign="middle"><strong>Subtema :</strong></td>
      <td height="20" align="left" valign="middle"><div id="subtemapost"><select name="cbo_subtema" id="select2subtema">
       <?
			$idsubtema = $_GET['idtemapregunta'];				  
			  if( $codtemapadre == "" ){
				$codtemapadre = $idtema;	
				$idsubtema = "";
			  }
			
		  $sqlsubtema = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
					WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.idtemapadre='".$codtemapadre."' ";
			$resultadosubtema = $baseDatos->ConsultarBD($sqlsubtema);
				if ( $idsubtema == "" ){
					print "<option value='' selected>General</option>";	
				}
			while ( list($codsubtema, $nomsubtema) = mysql_fetch_row($resultadosubtema) )		
			{	
				print "<option value='$codsubtema-$nomsubtema'";
				if ( $codsubtema == $idsubtema )
					print "selected";
				
				print ">$nomsubtema</option>";			
			}
			mysql_free_result($resultado);
	  ?>
            </select></div><div id="subtema2"></div></td>
    </tr>	
	<?php endif; ?>
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Grado : </strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_gradoP" id="select3">
      <?
		$sql = "SELECT * FROM mei_evagradopregunta";
		$resultado = $baseDatos->ConsultarBD($sql);
		while ( list($codgrado, $nomgrado) = mysql_fetch_row($resultado) )		
		{		
			print "<option value='$codgrado-$nomgrado'";
			if ( $codgrado == $idgrado )
				print "selected";
				
			print ">$nomgrado</option>";			
		}
		mysql_free_result($resultado);
	  ?>
            </select></td>
    </tr>
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Tipo : </strong></td>
      <td height="20" align="left" valign="middle"><?=$txt_tipo?>
        <input name="hid_tipopreg" type="hidden" id="hid_tipopreg" value="<?=$tipo_pregunta?>"></tr>
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Autoevaluacion? : </strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_autoeva" id="cbo_autoeva">
			<? 
			$b[0] = "No";
			$b[1] = "Si";
			
			for ($i=(count($b)-1);$i>=0;$i--)
			{
				print "<option value='$i'";
				if ($i==$autoeva) print " selected>";
				else print ">";
				print $b[$i]."</option>";
			}?>
            </select>
		</td>
    </tr>
  </table>
  
  
  <table class="tablaGeneral" >
    
    <tr>
      <td class="trTitulo" align="center" valign="middle">
        <? 
	  if ( $codtipo == 4 ){
		print "Afirmación";
		list ($pregunta, $razon ) = explode("[$$$]", stripslashes(base64_decode($pregunta)));
	  }
	  else {
		print "Pregunta";
		$pregunta = stripslashes(base64_decode($pregunta));	  
	  }
	  
	  ?>      </td>
    </tr>
    
	<tr class="trTitulo" >
	  <td align="center" valign="middle"><? 
		$editor=new FCKeditor('edt_pregunta' , '100%' , '200' , 'barraEstandar' , '' ) ;
		$editor->Value=$pregunta;
		$editor->crearEditor();		
	  ?></td>
	</tr>
  </table>
    <?
  	if ( $codtipo == 4 )
	{
  ?>
  &nbsp;
	  <table class="tablaGeneral" width="440" align="center">
		
		<tr align="center" valign="middle" class="trTitulo">
		  <td colspan="2">Raz&oacute;n</td>
		  </tr>
		
		<tr>
		  <td class="trIntermedio"colspan="2" align="center" valign="middle"><? 
			$editor=new FCKeditor('edt_razon' , '100%' , '200' , 'barraEstandar' , '' ) ;
			$editor->Value=$razon;
			$editor->crearEditor();
		  ?></td>
		  </tr>
	  </table>
  <?
  	}
  ?>
	  <?php
		if ($tipo_pregunta!=6):
	?>	
	  <table class="tablaGeneral" width="440" border="1" align="center">        
		<tr align="left" valign="middle" class="trTitulo">
		  <td width="30%" align="center" valign="middle">Repuesta(s)</td>
		  <td width="70%" height="15" align="center" valign="middle">Texto</td>
		  </tr>
	<?php else: ?>
	  <table class="tablaGeneral" width="440" border="1" align="center">        
		<tr align="left" valign="middle" class="trTitulo">
			  <td width="45%" align="center" valign="middle">Columna</td>
			  <td width="10%" align="center" valign="middle">Respuesta</td>
			  <td width="45%" height="15" align="center" valign="middle">Relaci&oacute;n</td>
		 </tr>
	<?php endif; ?>
	<?
		$i=0;		
		while( list($idrespuesta, $respuesta, $valor) = mysql_fetch_row($resresultado) )
		{//WHILE $resresultado
			if ( ($i%2)==0 ) $tr = "trListaOscuro";
			else $tr = "trInformacion";
			if ($tipo_pregunta==1)
			{// tipo_pregunta 1
	?>
		<tr class="<?=$tr?>">
		  <td  height="60" align="center" valign="middle">
			<input name="rad_respuesta[]" id="int_respuesta" type="radio" value="<?=$i;?>" <? if ($valor == 1)print "checked"?>>
			</td>
		  <td  height="60" align="center" valign="middle">
			<textarea name="txt_respuesta[]" cols="50" rows="3" id="txt_respuesta"><?=stripslashes(base64_decode($respuesta))?></textarea>      </td>
		</tr>
	<?
			}//fin tipo_pregunta 1
			elseif ($tipo_pregunta==2)
			{// tipo_pregunta 2
	?>
		<tr class="<?=$tr?>">
		  <td  height="60" align="center" valign="middle">
		  <select name="respuesta[]" id="int_respuesta">	  
		<?
			for ($k=100;$k>=-100;$k-=1)
			{				
				print "<option value='".($k/100)."'";
				if ( ($k/100)==$valor ) print " selected>";
				else print ">";
				
				print "$k%</option>";
			}
		?>	  
		  </select>
		  </td>
		  <td width="70%"  height="60" align="center" valign="middle">
			<textarea name="txt_respuesta[]" cols="50" rows="3" id="txt_respuesta"><?=stripslashes(base64_decode($respuesta))?></textarea>      </td>
		</tr>
	<?
			}//FIN tipo_pregunta 2
			elseif ($tipo_pregunta==3)
			{
	?>
		<tr class="<?=$tr?>">
		  <td  height="60" align="center" valign="middle">
			<input name="rad_respuesta[]" id="int_respuesta" type="radio" value="<?=$i;?>" <? if ($valor == 1)print "checked"?>>
			</td>
		  <td  height="60" align="center" valign="middle"><?=stripslashes(base64_decode($respuesta))?>
		    <input name="txt_respuesta[]" type="hidden" id="txt_respuesta[]" value="<?=stripslashes(base64_decode($respuesta))?>"></td>
		</tr>
	<?
			}
			elseif ($tipo_pregunta==4)
			{
	?>
		<tr class="<?=$tr?>">
		  <td  height="60" align="center" valign="middle">
			<input name="rad_respuesta[]" id="int_respuesta" type="radio" value="<?=$i;?>" <? if ($valor == 1)print "checked"?>>
			</td>
		  <td  height="60" align="left" valign="middle"><?=stripslashes(base64_decode($respuesta))?>
		    <input name="txt_respuesta[]" type="hidden" id="txt_respuesta[]" value="<?=stripslashes(base64_decode($respuesta))?>"></td>
		</tr>
	<?
			}
			elseif ($tipo_pregunta==6)
			{
			$sqlrelacioncolumna = "SELECT mei_evapreguntasrelacioncolumna.columna, mei_evapreguntasrelacioncolumna.relacion
						FROM mei_evapreguntasrelacioncolumna WHERE mei_evapreguntasrelacioncolumna.idrespuesta = ".$idrespuesta;
				//print $sql."<p>";	
				$resresultadorelacion = $baseDatos->ConsultarBD($sqlrelacioncolumna);				
				list($columna, $relacion) = mysql_fetch_row($resresultadorelacion);
	?>
    <tr class="<?=$tr?>">
      <td  height="60" align="center" valign="middle">
		<textarea style="width:370px;" name="preguntacolumna[]"><?=stripslashes(base64_decode($columna))?></textarea>
	  </td>
      <td  height="60" align="center" valign="middle">
		<input name="txt_respuesta[]" type="text" maxlength="1" size="2" onkeypress="return soloNumeros(event)" value="<?=stripslashes(base64_decode($respuesta))?>">
	  </td>	  
      <td height="60" align="center" valign="middle">
        <textarea style="width:350px;" name="preguntarelacion[]" ><?=stripslashes(base64_decode($relacion))?></textarea>
		<label>(<?php echo $j = $i+1; ?>)</label>
	  </td>
    </tr>	
	<?
			}

			$i++;
	?>
			<input name="hid_respuesta[]" type="hidden" id="hid_respuesta" value="<?=$idrespuesta?>">
	<?
		}//FIN WHILE $resresultado
	?>
	  </table>&nbsp;
  <table class="tablaGeneral" border="0" align="center">    
    <tr class="trSubTitulo">
      <!--<td width="50%" align="center" valign="middle"><input type="submit" name="Submit" value="Continuar"></td>  -->
      <td width="50%" align="center" valign="middle"><input type="button" name="Submit" value="Guardar" onClick="javascript:validar('<?=$codtipo?>','<?=$_GET["materia"]?>')"></td>
      <td width="50%" align="center" valign="middle"><input type="button" name="Submit2" value="Cancelar" onClick="javascript:cancelar('<?=$_GET["idmateria"]?>','<?=$_GET["materia"]?>')"></td>
    </tr>
  </table>
</form>      
<? 
}//FIN IF ESTADO 3			
else//if ( $_GET["estado"]=='2' ) 
{//IF ESTADO 2
?>
      <form action="<? print $PHP_SELF?>?estado=3&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" method="post" name="frm2" id="frm2">
        <table class="tablaGeneral" border="0" align="center">
    
    <tr class="trTitulo">
      <td height="20" colspan="2" align="left" valign="middle">Clonar Pregunta </td>
      </tr>
    <tr class="trInformacion">
      <td width="20%" height="20" align="left" valign="middle"><strong>Autor : </strong></td>
      <td width="80%" height="20" align="left" valign="middle"><strong>
        <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?>
        <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_GET["idmateria"]?>">
      </strong></td>
    </tr>
    
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Tema :</strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_tema" id="cbo_tema">
          <?
	  	$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
				WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.tipo=1";
		$resultado = $baseDatos->ConsultarBD($sql);
		while ( list($codtema, $nomtema) = mysql_fetch_row($resultado) )		
		{	
			print "<option value='$codtema' onclick='cargarsubtema($codtema)' ";
			
			if ("$codtema-$nomtema" == $_SESSION['tema'])
					print "selected";
			print  ">$nomtema</option>";			
		}
		mysql_free_result($resultado);
	  ?>
      </select><div id="subtemacarga"></div></td>
    </tr>
    <tr class="trInformacion" id="subtematr" style="display:none;">
      <td height="20" align="left" valign="middle"><strong>Subtema :</strong></td>
      <td height="20" align="left" valign="middle"><div id="subtema"></div></td>
    </tr>	
    
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Tipo : </strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_tipoP" onChange="javascript:document.frm2.submit()">
          <option value="0">Seleccione un Tipo</option>
          <?
	  	$sql = "SELECT * FROM mei_evatipopregunta";
		$resultado = $baseDatos->ConsultarBD($sql);
		while ( list($tipopreg, $nomtipo) = mysql_fetch_row($resultado) )		
		{		
			print "<option value='$tipopreg-$nomtipo'>$nomtipo</option>";			
		}
		mysql_free_result($resultado);
	  ?>
        </select></td>
      </tr>
  </table>
        
        <table class="tablaGeneral">
          <tr class="trSubTitulo" align="center" valign="middle">
            <td><input type="submit" name="Submit3" value="Cancelar" onClick="javascript:cancelar2('<?=$_GET["idmateria"]?>','<?=$_GET["materia"]?>')"></td>
          </tr>
        </table>
      </form>
<?
}//FIN IF ESTADO 2
?>			
			
		  </td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>