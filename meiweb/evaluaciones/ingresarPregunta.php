<?
	include_once ('../menu/Menu.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{	
		
		$baseDatos= new BD();
		$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraEstandar' , '' );
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="scripts.js"></script>
</head>
<script>
function soloNumeros(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " 12345";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
     {   
		alert("Digite numero del 1 al 5");
		return false;	 
	 }
}

function validar(codtipo,materia)
{
	var contador_txt=0;
	var contador_int=0;	
	
	if ( (codtipo!=3) && (codtipo!=4)&& (codtipo!=5)&& (codtipo!=6) )
	{  
	
		for(i=0;i<document.frm3.elements.length;i++)
		{			
			if (document.frm3.elements[i].id=='txt_respuesta')
			{
				if (document.frm3.elements[i].value!="")
				contador_txt++;							
			}
			if (document.frm3.elements[i].id=='int_respuesta')
			{
				if ( (document.frm3.elements[i].value>0) )
				contador_int++;							
			}		
		}
	
		if( (contador_txt>3) && (contador_int>0) )
		{
			document.frm3.submit();
		}
		else if(contador_txt<=4) alert('Debe llenar todos  los campos')
		else if(contador_int<=0) alert('Debe Seleccionar al menos una respuesta correcta')
	}
	else document.frm3.submit();
		
}




function cancelar(idmateria,materia)
{
	var temaa = document.getElementById("select2").value;
	var temas = temaa.split("-");
	var tema = temas[0];
	document.frm3.action = "ingresarPregunta.php?idmateria="+idmateria+"&materia="+materia+"&tema="+tema;
	document.frm3.submit()
}
function cancelar2(idmateria,materia)
{
	document.frm2.action = "verPregunta.php?idmateria="+idmateria+"&materia="+materia;
	document.frm2.submit()
}
function cancelar3(idmateria,materia)
{
	document.frm2.action = "verEvaluacion.php?idmateria="+idmateria+"&materia="+materia;
	document.frm2.submit()
}
function cargarsubtema(idtema){
	if( idtema != "" ){
	  $.ajax({
					data:  ({ idtema : idtema }),
					url:   'cargarsubtemas.php',
					type:  'post',
					beforeSend: function () {
							$("#subtemacarga").html("Procesando, espere por favor...");
					},
					success:  function (response) {
							$("#subtema").html(response);
							document.getElementById("subtematr").style.display = 'table-row';
							document.getElementById("subtemacarga").style.display = 'none';
					}
			});							
	}	
}
function cargarsubtema2(idtema){
	if( idtema != "" ){
	  $.ajax({
					data:  ({ idtema : idtema }),
					url:   'cargarsubtemas.php',
					type:  'post',
					success:  function (response) {
							$("#subtema2").html(response);
							document.getElementById("subtemapost").style.display = 'none';
					}
			});							
	}	
}
</script>
<body>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
		    <td valign="top" class="tdGeneral">
				<table class="tablaMarquesina">
					<tr>
<?	 				
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($mat) = mysql_fetch_row($resultado); 
?>	  
						<?if($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7){?>
						<td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($mat))?></a><a> -> </a><a href="verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a><a> -> Ingresar Preguntas </a></td>
					<?}else{?>
						<td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($mat))?></a><a> -> </a><a href="verPregunta.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Banco de Preguntas</a><a> -> Ingresar Preguntas </a></td>
						
						<?}?>
					</tr>
				</table><br>
<?
if ( $_GET["estado"]=='3' ) 
{//IF ESTADO 3 - Se escoje el tipo de pregunta

?>

<form name="frm3" method="post" action="guardarPregunta.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>">
  <table class="tablaGeneral" align="center">
    <tr class="trTitulo">
      <td height="20" colspan="2" align="left" valign="middle">Ingresar Pregunta </td>
      </tr>
    <tr class="trInformacion">
      <td width="20%" height="20" align="left" valign="middle"><strong>Autor : </strong></td>
      <td width="80%" height="20" align="left" valign="middle"><strong>
          <? mysql_query("SET NAMES 'utf8'"); 
             echo $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario'];?>
          <input name="hid_materia" type="hidden" id="hid_materia"  value="<?=$_POST["hid_materia"]?>">
      </strong></td>
    </tr>
    
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Temas : </strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_tema" id="select2" onchange="cargarsubtema2(this.value)">
       <?

	  	//if ( $_POST[ )
		$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
				WHERE mei_tema.idmateria='".$_POST["hid_materia"]."' AND mei_tema.tipo=1";
		$resultado = $baseDatos->ConsultarBD($sql);
			
			

		while ( list($codtema, $nomtema) = mysql_fetch_row($resultado) )		
		{		
			print "<option value='$codtema-$nomtema'";
			if ( ($codtema == $_POST["hid_tema"]) or ( $codtema == $_POST["cbo_tema"] ) or ('$codtema-$nomtema' == $_SESSION['tema']) )
				print "selected";
			
			print ">$nomtema</option>";			
		}
		mysql_free_result($resultado);
	  ?>
            </select></td>
    </tr>
	<?php  if(isset($_POST["cbo_sub_tema"])): ?>
    <tr class="trInformacion" id="subtema">
      <td height="20" align="left" valign="middle"><strong>Subtema :</strong></td>
      <td height="20" align="left" valign="middle"><div id="subtemapost"><select name="cbo_subtema" id="select2subtema">
       <?
	  	//if ( $_POST[ )
		$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
				WHERE mei_tema.idtema='".$_POST["cbo_sub_tema"]."' ";
		$resultado = $baseDatos->ConsultarBD($sql);
		while ( list($codtema, $nomtema) = mysql_fetch_row($resultado) )		
		{		
			print "<option value='$codtema-$nomtema'";
			if ( ($codtema == $_POST["hid_tema"]) or ( $codtema == $_POST["cbo_tema"] ) or ('$codtema-$nomtema' == $_SESSION['tema']) )
				print "selected";
			
			print ">$nomtema</option>";			
		}
		mysql_free_result($resultado);
	  ?>
            </select></div><div id="subtema2"></div></td>
    </tr>	
	<?php endif; ?>
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Grado : </strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_gradoP" id="select3">
      <?
	  	$sql = "SELECT * FROM mei_evagradopregunta";
		$resultado = $baseDatos->ConsultarBD($sql);
		while ( list($codgrado, $nomgrado) = mysql_fetch_row($resultado) )		
		{		
			print "<option value='$codgrado-$nomgrado'";
			if ( ( $codgrado == $_POST["hid_grado"] ) or ( $codgrado = $_POST["cbo_gradoP"] ) )
				print "selected";
				
			print ">$nomgrado</option>";			
		}
		mysql_free_result($resultado);
	  ?>
            </select></td>
    </tr>
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Tipo : </strong></td>
      <td height="20" align="left" valign="middle"><?  
		  list($codtipo,$nomtipo)=explode("-", $_POST["cbo_tipoP"]);
		  print $nomtipo;
	  	?>
        <input name="hid_tipopreg" type="hidden" id="hid_tipopreg" value="<?=$codtipo."-".$nomtipo?>"></tr>
    <?if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5){?>     
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Autoevaluacion? : </strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_autoeva" id="cbo_autoeva">
        <option value="SI">Si</option>
        <option value="NO" selected>NO</option>
                  </select></td>
    </tr>
    <?}?>
  </table>
  
  
  <table class="tablaGeneral" >
    
    <tr>
      <td class="trTitulo" align="center" valign="middle">
        <? 
	  if ( $codtipo == 4 ) print "Afirmación";
	  					  else print "Pregunta";?>      </td>
    </tr>
    
    <tr>
      <td class="trInformacion" align="center" valign="middle"><? 
		$editor=new FCKeditor('edt_pregunta' , '100%' , '200' , 'barraEstandar' , '' ) ;
		if ( !empty($_POST['hid_pregunta']) )
			$editor->Value=$_POST['hid_pregunta'];
		$editor->crearEditor();		
	  ?></td>
    </tr>
  </table>
    <?
  	if ( $codtipo == 4 )
	{
  ?>
  &nbsp;
  <table class="tablaGeneral" align="center">
    
    <tr align="center" valign="middle" class="trTitulo">
      <td colspan="2">Raz&oacute;n</td>
      </tr>
    
    <tr class="trInformacion">
      <td colspan="2" align="center" valign="middle"><? 
		$editor=new FCKeditor('edt_razon' , '100%' , '200' , 'barraEstudiante' , '' ) ;
		if ( !empty($_POST['hid_pregunta2']) )
			$editor->Value=$_POST['hid_pregunta2'];
		$editor->crearEditor();
	  ?></td>
      </tr>
  </table>
  <?
  	}
  ?>
    <? 
	if ( ($codtipo==1))//or ($codtipo==3) )	
	{//IF CODDTIPO
  ?>
  &nbsp;
  <table class="tablaGeneral" align="center">        
    <tr align="left" valign="middle" class="trTitulo">
      <td width="30%" align="center" valign="middle">Repuesta Correcta </td>
      <td width="70%" height="15" align="center" valign="middle">Texto</td>
      </tr>
	    <? 
		for ( $i=0;$i<4;$i++ )
		{//FOR 1
			if ( ($i%2)==0 ) $tr = "trListaOscuro";
			else $tr = "trInformacion";
		?>
    <tr class="<?=$tr?>">
      <td  height="60" align="center" valign="middle">
        <input name="rad_respuesta[]" type="radio" id="int_respuesta" value="<?=$i;?>" checked>
	  </td>
      <td  height="60" align="center" valign="middle">
        <textarea name="txt_respuesta[]" cols="50" rows="3" id="txt_respuesta"></textarea>      </td>
    </tr>
	<?
		}//FIN FOR 1
	?>
  </table>
 <?
	}
	elseif ($codtipo==2)
	{//MULTIPLE RESPUESTA
?>
  &nbsp;
  <table class="tablaGeneral" border="1" align="center">        
    <tr align="left" valign="middle" class="trTitulo">
      <td width="30%" align="center" valign="middle">Valor Repuesta</td>
      <td width="70%" height="15" align="center" valign="middle">Texto</td>
      </tr>
		<?
		for ( $i=0;$i<4;$i++ )
		{//FOR 1
			if ( ($i%2)==0 ) $tr = "trListaOscuro";
			else $tr = "trInformacion";
		?>
    <tr class="<?=$tr?>">
      <td  height="60" align="center" valign="middle">
	  <select name="respuesta[]" id="int_respuesta">	  
		<?
			for ($k=100;$k>=-100;$k-=1)
			{				
				print "<option value='".($k/100)."'";
				if ( ($k/100)==0 ) print " selected>";
				else print ">";				
				print "$k%</option>";
			}
		?>	  
	  </select>
	  </td>
      <td width="70%"  height="60" align="center" valign="middle">
        <textarea name="txt_respuesta[]" cols="50" rows="3" id="txt_respuesta"></textarea>      </td>
    </tr>
	<?
		}//FIN FOR 1
	?>
  </table>
<?
	}//FIN MULTIPLE RESPUESTA
	elseif ($codtipo==3)
	{
  ?>
  &nbsp;
  <table class="tablaGeneral" border="0" align="center">
    <tr align="center" class="trTitulo">
      <td colspan="2" valign="middle">Repuesta</td>
      </tr>    
    
    <tr class="trListaClaro">
      <td width="50%" align="right" valign="middle">
        <input name="txt_respuesta[]" type="hidden" id="txt_respuesta[]" value="Verdadero">
        Verdadero
        <input name="rad_respuesta[]" type="radio" id="int_respuesta" value="0" checked></td>
      <td width="50%" align="left" valign="middle"><input name="rad_respuesta[]" type="radio" value="1" id="int_respuesta">
        Falso
        <input name="txt_respuesta[]" type="hidden" id="txt_respuesta[]" value="Falso"></td>
    </tr>
  </table>
<?
	}elseif ($codtipo==4)
	{
?>&nbsp;
  <table class="tablaGeneral" border="0" align="center">
    
    <tr class="trTitulo">
      <td colspan="2" align="center" valign="middle">Respuestas</td>
      </tr>
    
    <tr class="trListaClaro">
      <td width="20%" align="right" valign="middle"><input name="rad_respuesta[]" type="radio" id="int_respuesta" value="0" checked></td>
      <td width="95%" align="left" valign="middle">La afirmaci&oacute;n y la raz&oacute;n son verdaderas y la raz&oacute;n es una expliaci&oacute;n correcta de la afirmaci&oacute;n.
        <input name="txt_respuesta[]" type="hidden" id="txt_respuesta[]" value="La afirmaci&oacute;n y la raz&oacute;n son verdaderas y la raz&oacute;n es una expliaci&oacute;n correcta de la afirmaci&oacute;n."></td>
    </tr>
    <tr  class="trListaOscuro">
      <td width="20%" align="right" valign="middle"><input name="rad_respuesta[]" type="radio" value="1" id="int_respuesta"></td>
      <td width="95%" align="left" valign="middle"> La afirmaci&oacute;n y la raz&oacute;n son verdaderas, pero la raz&oacute;n no es una expliaci&oacute;n correcta de la afirmaci&oacute;n.
        <input name="txt_respuesta[]" type="hidden" id="txt_respuesta[]" value="La afirmaci&oacute;n y la raz&oacute;n son verdaderas, pero la raz&oacute;n no es una expliaci&oacute;n correcta de la afirmaci&oacute;n."></td>
    </tr>
    <tr class="trListaClaro">
      <td width="20%" align="right" valign="middle"><input name="rad_respuesta[]" type="radio" value="2" id="int_respuesta"></td>
      <td width="95%" align="left" valign="middle">La afirmaci&oacute;n es verdadera, pero la raz&oacute;n es una proposici&oacute;n falsa.
        <input name="txt_respuesta[]" type="hidden" id="txt_respuesta[]" value="La afirmaci&oacute;n es verdadera, pero la raz&oacute;n es una proposici&oacute;n falsa."></td>
    </tr>
    <tr class="trListaOscuro">
      <td width="20%" align="right" valign="middle"><input name="rad_respuesta[]" type="radio" value="3" id="int_respuesta"></td>
      <td width="95%" align="left" valign="middle">La afirmaci&oacute;n es falsa, pero la raz&oacute;n es una proposic&oacute;n verdadera.
        <input name="txt_respuesta[]" type="hidden" id="txt_respuesta[]" value="La afirmaci&oacute;n es falsa, pero la raz&oacute;n es una proposic&oacute;n verdadera."></td>
    </tr>
    <tr class="trListaClaro">
      <td width="20%" align="right" valign="middle"><input name="rad_respuesta[]" type="radio" value="4" id="int_respuesta"></td>
      <td width="95%" align="left" valign="middle">Tanto la afirmaci&oacute;n como la raz&oacute;n son proposiciones falsas.
        <input name="txt_respuesta[]" type="hidden" id="txt_respuesta[]" value="Tanto la afirmaci&oacute;n como la raz&oacute;n son proposiciones falsas."></td>
    </tr>
  </table>
  <? 
  }elseif ($codtipo==6){
  ?>
  &nbsp;
  <table class="tablaGeneral" border="1" align="center">        
    <tr align="left" valign="middle" class="trTitulo">
      <td width="45%" align="center" valign="middle">Pregunta</td>
	  <td width="10%" align="center" valign="middle">Relaci&oacute;n</td>
      <td width="45%" height="15" align="center" valign="middle">Respuesta</td>
      </tr>
		<?
		for ( $i=0;$i<5;$i++ )
		{//FOR 1
			if ( ($i%2)==0 ) $tr = "trListaOscuro";
			else $tr = "trInformacion";
		?>
    <tr class="<?=$tr?>">
      <td  height="60" align="center" valign="middle">
		<textarea style="width:370px;" name="preguntacolumna[]" id="preguntacolumna_<?php echo $i; ?>" ></textarea>
	  </td>
      <td  height="60" align="center" valign="middle">
		<input name="txt_respuesta[]" type="text" id="respuesta_<?php echo $i; ?>" maxlength="1" size="2" onkeypress="return soloNumeros(event)" value="">
	  </td>	  
      <td height="60" align="center" valign="middle">
        <textarea style="width:350px;" name="preguntarelacion[]" id="preguntarelacion_<?php echo $i; ?>" ></textarea>
		<label>(<?php echo $i+1; ?>)</label>
	  </td>
    </tr>
	<?
		}//FIN FOR 1
	?>
  </table>
  <?php
  }//FIN IF CODTIPO
  ?>&nbsp;  
  <table class="tablaGeneral" border="0" align="center">    
    <tr class="trSubTitulo">
      <!--<td width="50%" align="center" valign="middle"><input type="submit" name="Submit" value="Continuar"></td>  -->
      <td width="50%" align="center" valign="middle"><input type="button" name="Submit" value="Guardar" onClick="javascript:validar('<?=$codtipo?>','<?=$_GET["materia"]?>')"></td>
      <td width="50%" align="center" valign="middle"><input type="button" name="Submit2" value="Cancelar" onClick="javascript:cancelar('<?=$_POST["hid_materia"]?>','<?=$_GET["materia"]?>')"></td>
    </tr>
  </table>
</form>      
<? 
}//FIN IF ESTADO 3			
else//if ( $_GET["estado"]=='2' ) 
{//IF ESTADO 2
	
?>
      <form action="<? print $_SERVER['PHP_SELF']?>?estado=3&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" method="post" name="frm2" id="frm2">
        <table class="tablaGeneral" border="0" align="center">
    
    <tr class="trTitulo">
      <td height="20" colspan="2" align="left" valign="middle">Ingresar Pregunta </td>
      </tr>
    <tr class="trInformacion">
      <td width="20%" height="20" align="left" valign="middle"><strong>Autor : </strong></td>
      <td width="80%" height="20" align="left" valign="middle"><strong>
        <? mysql_query("SET NAMES 'utf8'"); 
             echo $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario'];?>
        <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_GET["idmateria"]?>">
      </strong></td>
    </tr>
    
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Temas :</strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_tema" id="cbo_tema"  onchange="cargarsubtema(this.value)"  >
          <?
	  	$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
				WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.tipo=1";
		$resultado = $baseDatos->ConsultarBD($sql);
		print "<option selected > </option>";
		while ( list($codtema, $nomtema) = mysql_fetch_row($resultado) )		
		{	
			print "<option value='$codtema'";
			
		 	if ("$codtema" == $_GET["tema"])
			print "selected";		 
			print  ">$nomtema</option>";			
		}
		mysql_free_result($resultado);
		if (!empty($_GET["tema"]) && $_GET["tema"]!= "*" ) {
			?><script> cargarsubtema("<?echo $_GET["tema"];?>"); </script>  <?
		}
	  ?>
      </select><div id="subtemacarga"></div></td>
    </tr>
    <tr class="trInformacion" id="subtematr" style="display:none;">
      <td height="20" align="left" valign="middle"><strong>Subtema :</strong></td>
      <td height="20" align="left" valign="middle"><div id="subtema"></div></td>
    </tr>	
    
    <tr class="trInformacion">
      <td height="20" align="left" valign="middle"><strong>Tipo : </strong></td>
      <td height="20" align="left" valign="middle"><select name="cbo_tipoP" onChange="javascript:document.frm2.submit()">
          <option value="0">Seleccione un Tipo</option>
          <?
	  	if ($_SESSION['idtipousuario']==2 ||$_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==5) {
	  		$sql = "SELECT * FROM mei_evatipopregunta";
     		$resultado = $baseDatos->ConsultarBD($sql);
	  	}
	  	else{
	    	$sql = "SELECT * FROM mei_evatipopregunta WHERE mei_evatipopregunta.tipo_pregunta != 6";
		    $resultado = $baseDatos->ConsultarBD($sql);
		}


		while ( list($tipopreg, $nomtipo) = mysql_fetch_row($resultado) )		
		{		
			print "<option value='$tipopreg-$nomtipo'>$nomtipo</option>";			
		}
		mysql_free_result($resultado);
	  ?>
        </select></td>
      </tr>
  </table>
        
        <table class="tablaGeneral">
          <tr class="trSubTitulo" align="center" valign="middle">
          	<?if($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7){?>
            <td><input type="submit" name="Submit3" value="Cancelar" onClick="javascript:cancelar3('<?=$_GET["idmateria"]?>','<?=$_GET["materia"]?>')"></td>
          	<?}else{?>
          		<td><input type="submit" name="Submit3" value="Cancelar" onClick="javascript:cancelar2('<?=$_GET["idmateria"]?>','<?=$_GET["materia"]?>')"></td>
          	
          		<?}?>
          </tr>
        </table>
      </form>
<?
}//FIN IF ESTADO 2
?>			
			
		  </td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>