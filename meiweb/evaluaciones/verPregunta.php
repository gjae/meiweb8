<?PHP 
	include_once ('../menu1/Menu1.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	
if(comprobarSession())
{	
	if (!empty($_GET["tema"]) && $_GET["tema"] != '*') {
		$var=explode("-", $_GET["tema"]);
		$_POST["cbo_tema"]=$var[0];
	}
	else $_GET["tema"]=null;
	$baseDatos= new BD();	
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraBasica' , '' );
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="jquery.js"></script>
<script>
var ventanaPreguntas=false;
function verPregunta(idpregunta)
{	
	   var izquierda = (screen.availWidth - 700) / 2; 
	   var arriba = (screen.availHeight - 300) / 2; 
	if (typeof ventanaPreguntas.document == 'object')
	 {
		ventanaPreguntas.close()
	 }
	 
	ventanaPreguntas = window.open('visualizarPregunta.php?idpregunta='+idpregunta,'visualizarPregunta','width=700,height=300,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');				
}
function enviar()
{
	/*val = document.frm_temas.cbo_tema.value;
	if ( val != '*' )*/
		document.frm_temas.submit();		
}

var ventanaPreguntas=false;
function exportarPreguntas(idmateria,materia)
{	
	   var izquierda = (screen.availWidth - 700) / 2; 
	   var arriba = (screen.availHeight - 600) / 2; 
	
									
	if (typeof ventanaPreguntas.document == 'object')
	 {
		ventanaPreguntas.close();
	 }
	 
	//ventanaPreguntas = window.open('exportarPreguntas.php?idmateria='+idmateria+'&idtema='+idtema,'exportarPreguntas','width=700,height=600,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');	
		ventanaPreguntas = window.open('copiaSeguridad.php?idmateria='+idmateria+'&materia='+materia,'exportarPreguntas','width=700,height=600,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=YES,titlebar=YES,menubar=YES,status=YES,statusbar=YES,resizable=YES,location=YES');
}

var ventanaPreguntas=false; 
function mostrarPreguntas(idmateria,idtema,nomtema)
{	
	   var izquierda = (screen.availWidth - 700) / 2; 
	   var arriba = (screen.availHeight - 600) / 2; 
	
									
	if (typeof ventanaPreguntas.document == 'object')
	 {
		ventanaPreguntas.close();
	 }
	 
	ventanaPreguntas = window.open('mostrarPreguntas.php?idmateria='+idmateria+'&idtema='+idtema+'&titulo='+nomtema,'exportarPreguntas','width=700,height=600,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');	
					
}
function eliminar(idpregunta,tema)
{
	formu=document.getElementById(tema);
	formu.action="guardarPregunta.php?accion=DEL&idpregunta="+idpregunta+"&idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&tema=<? echo $_POST['cbo_tema']; ?>";
	//document.frm_ingresarTaller.action="verDtlleEvaluacion.php?idprevio="+id+"&idmateria="+mat+"&materia=</?=$_GET['materia']; ?>";
	formu.submit();
}
function ordenar(tab)
{
var tabla=document.getElementById(tab); 
for(var i=2;i<tabla.rows.length-1;i++) 
{ 
tabla.rows[i].cells[0].innerHTML=i-1; 
}
}
</script>
</head>
<body>
<?PHP 
/* BUSCAR PADRE TEMA */
function buscartemapadre($codtema){
	global $baseDatos;
	$sqlbuscarpadre = "SELECT mei_tema.idtema, mei_tema.titulo, mei_tema.tipo, mei_tema.idtemapadre FROM mei_tema 
	WHERE mei_tema.idtema='".$codtema."' ";	
	$resultadopadre = $baseDatos->ConsultarBD($sqlbuscarpadre);
	list($codtema2, $nomtema2, $tipotema2, $idtemapadre) = mysql_fetch_row($resultadopadre);
	return $nomtema2;
}
/* FIN BUSCAR PADRE TEMA */
function preguntas($sql,$IdTema)
{//function preguntas
	global $baseDatos;
	$resultado = $baseDatos->ConsultarBD($sql);
	
	if(mysql_num_rows($resultado))
	{//HAY PREGUNTAS
	$idtab=rand(0,9999);
	?>
	  <table class="tablaGeneral" id="<?PHP  echo($idtab); ?>">
		<form name="frm1" id="<?php echo $IdTema;?>" method="post" action="guardarPregunta.php">
        <tr class="trSubTitulo">
		  
          <td width="3%" align="center" valign="middle">N&uacute;mero</td>
          <td width="4%" align="center" valign="middle">&nbsp;</td>
          <td width="5%" align="center" valign="middle">&nbsp;</td>
          <td width="30%" align="center" valign="middle">Pregunta</td>
          <td width="20%" align="center" valign="middle">Tipo</td>
          
		  <td width="11%" align="center" valign="middle">Visualizar</td>
		  <td width="11%" align="center" valign="middle">Editar</td>
		  <td width="11%" align="center" valign="middle">Clonar</td>
		  <td width="11%" align="center" valign="middle">Eliminar</td>
        </tr>
        <tr class="trListaClaro">
          <td colspan="11" align="center" valign="middle">&nbsp;</td>
        </tr>
        <?PHP 	$numPreg= 1243;
			$i = 0;
			while ( list($idpregunta, $pregunta, $tipo, $txt_tipo, $grado, $autoevaluacion) = mysql_fetch_row($resultado))
			{
				if ( ($i%2)==0 ) $clase = "trListaOscuro";
				else $clase = "trInformacion";
	?>
        <tr class="<?PHP echo $clase?>">
		<td valign="middle" align="right"><?PHP echo $numPreg;?></td>
       	<td align="center" valign="middle">- <?PHP echo $idpregunta;?></td>
		<td align="center" valign="middle">
        	<?PHP 
            	if(!empty($autoevaluacion))
				{
				?><img src="../autoevaluaciones/imagenes/iconoautoevaluacion.png" width="16" height="16" title="Pregunta de Autoevaluación" align="right"><?PHP 
				}
				else
				{
				?>&nbsp;<?PHP 	
				}
				?>
          </td>
          <td align="center" valign="middle"><a href="editarPregunta.php?idpregunta=<?PHP echo $idpregunta?>&idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>&tema=<? echo $_POST['cbo_tema']; ?>" class="link">
            <?PHP 
		if ( $tipo == 4 )
		{
			list($afirmacion) = explode("[$$$]",stripslashes(base64_decode($pregunta)));
			print substr(strip_tags($afirmacion),0,20)."...";
		}
		else print substr(strip_tags(stripslashes(base64_decode($pregunta))),0,30)."...";
	  ?>
          </a> </td>
          <td align="center" valign="middle"><?PHP echo $txt_tipo?></td>
          
            <td align="center" valign="middle"><a href="javascript:verPregunta('<?PHP echo $idpregunta;?>')"><img src="../evaluaciones/imagenes/lupa.gif" width="16" height="16" border="0"></a></td>
			<td align="center" valign="middle"><a href="editarPregunta.php?idtemapregunta=<?PHP echo $IdTema;?>&idpregunta=<?PHP echo $idpregunta;?>&idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>&tema=<? echo $_POST['cbo_tema']; ?>" class="link"><img src="imagenes/modificar.gif" alt="modificar pregunta" width="16" height="16" border="0"></a></td>
			<td align="center" valign="middle"><a href="clonarPregunta.php?idtemapregunta=<?PHP echo $IdTema;?>&idpregunta=<?PHP echo $idpregunta;?>&idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link"><img src="imagenes/duplicar.gif" alt="modificar pregunta" width="16" height="16" border="0"></a></td>			
			<td align="center" valign="middle"><a href="javascript:eliminar('<?PHP echo $idpregunta;?>','<?php echo $IdTema;?>')"><img src="imagenes/eliminar.gif" alt="Eliminar Pregunta" width="16" height="16" border="0"></a></td>
		</tr>
        <?PHP 
				$i++;
			}				
	?>
        <tr  class="trListaClaro">
          <td colspan="11">&nbsp;</td>
        </tr>
        </form>
      </table>
<script>ordenar("<?PHP  echo($idtab);?>")</script>
	  <?PHP 
		}//FIN HAY PREGUNTAS
		else
		{
	?>
<table class="tablaGeneral">
	<tr class="trSubTitulo">
	<td align="center" valign="middle">&nbsp;</td>
	</tr>
	<tr class="trSubTitulo">
	<td align="center" valign="middle">No Existen Preguntas de este Tema</td>
	</tr>
	<tr class="trSubTitulo">
	<td align="center" valign="middle">&nbsp;</td>
	</tr>
</table>
	<?PHP 
		}
}//fin function preguntas	

function preguntasclonadas($sql,$IdTema)
{//function preguntas
	global $baseDatos;
	$resultado = $baseDatos->ConsultarBD($sql);
	
	if(mysql_num_rows($resultado))
	{//HAY PREGUNTAS
	$idtab=rand(0,9999);
	?>
	  <table class="tablaGeneral" id="<?PHP  echo($idtab); ?>">
		<form name="frm1" id="<?php echo $IdTema;?>" method="post" action="guardarPregunta.php">
        <tr class="trSubTitulo">
		  
          <td width="3%" align="center" valign="middle">N&uacute;mero</td>
          <td width="4%" align="center" valign="middle">&nbsp;</td>
          <td width="5%" align="center" valign="middle">&nbsp;</td>
		  <td align="center" valign="middle">Padre</td>
          <td width="30%" align="center" valign="middle">Pregunta</td>
          <td width="20%" align="center" valign="middle">Tipo</td>
          
		  <td width="11%" align="center" valign="middle">Visualizar</td>
		  <td width="11%" align="center" valign="middle">Editar</td>
		  <td width="11%" align="center" valign="middle">Clonar</td>
		  <td width="11%" align="center" valign="middle">Eliminar</td>
        </tr>
        <tr class="trListaClaro">
          <td colspan="11" align="center" valign="middle">&nbsp;</td>
        </tr>
        <?PHP 	$numPreg= 1243;
			$i = 0;
			while ( list($idpregunta, $pregunta, $tipo, $txt_tipo, $grado, $autoevaluacion, $clonid) = mysql_fetch_row($resultado))
			{
				if ( ($i%2)==0 ) $clase = "trListaOscuro";
				else $clase = "trInformacion";
	?>
        <tr class="<?PHP echo $clase?>">
		<td valign="middle" align="right"><?PHP echo $numPreg;?></td>
       	<td align="center" valign="middle">- <?PHP echo $idpregunta;?></td>
		<td align="center" valign="middle">
        	<?PHP 
            	if(!empty($autoevaluacion))
				{
				?><img src="../autoevaluaciones/imagenes/iconoautoevaluacion.png" width="16" height="16" title="Pregunta de Autoevaluación" align="right"><?PHP 
				}
				else
				{
				?>&nbsp;<?PHP 	
				}
				?>
          </td>
		  <td align="center" valign="middle"><?php echo $clonid; ?></td>
          <td align="center" valign="middle"><a href="editarPregunta.php?idpregunta=<?PHP echo $idpregunta?>&idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>&tema=<? echo $_POST['cbo_tema']; ?>" class="link">
            <?PHP 
		if ( $tipo == 4 )
		{
			list($afirmacion) = explode("[$$$]",stripslashes(base64_decode($pregunta)));
			print substr(strip_tags($afirmacion),0,20)."...";
		}
		else print substr(strip_tags(stripslashes(base64_decode($pregunta))),0,30)."...";
	  ?>
          </a> </td>
          <td align="center" valign="middle"><?PHP echo $txt_tipo?></td>
          
            <td align="center" valign="middle"><a href="javascript:verPregunta('<?PHP echo $idpregunta;?>')"><img src="../evaluaciones/imagenes/lupa.gif" width="16" height="16" border="0"></a></td>
			<td align="center" valign="middle"><a href="editarPregunta.php?idtemapregunta=<?PHP echo $IdTema;?>&idpregunta=<?PHP echo $idpregunta;?>&idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>&tema=<? echo $_POST['cbo_tema']; ?>" class="link"><img src="imagenes/modificar.gif" alt="modificar pregunta" width="16" height="16" border="0"></a></td>
			<td align="center" valign="middle"><a href="clonarPregunta.php?idtemapregunta=<?PHP echo $IdTema;?>&idpregunta=<?PHP echo $idpregunta;?>&idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link"><img src="imagenes/duplicar.gif" alt="modificar pregunta" width="16" height="16" border="0"></a></td>			
			<td align="center" valign="middle"><a href="javascript:eliminar('<?PHP echo $idpregunta;?>','<?php echo $IdTema;?>')"><img src="imagenes/eliminar.gif" alt="Eliminar Pregunta" width="16" height="16" border="0"></a></td>
		</tr>
        <?PHP 
				$i++;
			}				
	?>
        <tr  class="trListaClaro">
          <td colspan="11">&nbsp;</td>
        </tr>
        </form>
      </table>
<script>ordenar("<?PHP  echo($idtab);?>")</script>
	  <?PHP 
		}//FIN HAY PREGUNTAS
		else
		{
	?>
<table class="tablaGeneral">
	<tr class="trSubTitulo">
	<td align="center" valign="middle">&nbsp;</td>
	</tr>
	<tr class="trSubTitulo">
	<td align="center" valign="middle">No Existen Preguntas de este Tema</td>
	</tr>
	<tr class="trSubTitulo">
	<td align="center" valign="middle">&nbsp;</td>
	</tr>
</table>
	<?PHP 
		}
}//fin function preguntas	

?>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><?PHP  menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td valign="top" class="tdGeneral">
            	<table class="tablaMarquesina" border="0">
              	<tr>
<?PHP 
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado);
?>
              
					<td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link"><?PHP echo ucwords(strtolower($nombre))?></a><a> -> Banco de Preguntas</a></td>
              </tr>
            </table>&nbsp;
<?PHP 
			$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
					WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.tipo=1";
			$restema = $baseDatos->ConsultarBD($sql);
			
			if ( mysql_num_rows($restema) > 0 )
			{//EXISTEN TEMAS			
?>
		      	<table class="tablaGeneral">
				<form name="frm_temas" method="post" action="verPregunta.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>">
                <tr class="trTitulo">
                  <td width="15%" align="left" valign="middle">Ordenar por tema:</td>
                  <td width="37%" align="left" valign="middle"><select name="cbo_tema" id="cbo_tema" onChange="javascript:enviar()">
				  		<option value="*" class="link">Todos...</option>						
<?PHP 
						$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
								WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.tipo=1";
						$resultado = $baseDatos->ConsultarBD($sql);
						
						if (empty($_POST["cbo_tema"])) $_POST["cbo_tema"] = '*';
						
						while ( list($codtema, $nomtema) = mysql_fetch_row($restema) )		
						{		
							print "<option value='$codtema'";
							if ( $codtema == $_POST["cbo_tema"] ) print " selected>";
							else print ">";
							print "$nomtema</option>";			
						}
						//mysql_free_result($restema);
?>
                  </select></td>
                  <td width="32%" align="right" valign="middle"><a href="javascript:exportarPreguntas('<?PHP echo $_GET["idmateria"]; ?>','<?PHP echo $_GET["materia"]; ?>')" class="link"><img src="../contenido/imagenes/guardarTemp.gif" width="21" height="20" border="0"> Exportar Preguntas  /</a>
                  	<a href="cargarCopia.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">  Cargar Preguntas </a>
                  </td>
                </tr>
               </form>
				</table>
				&nbsp;
					<table class="tablaMarquesina">
					<tr align="center" valign="middle">
						<td width="32%"><a href="ingresarPregunta.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>&tema=<?echo $_POST["cbo_tema"];?> " class="link">Crear Pregunta</a></td>
					<!--	<td width="41%"><a href="editarPregunta.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Editar Pregunta</a></td> -->
				        <?
					$sql="SELECT idpregunta FROM mei_evapreguntasvalidar WHERE idmateria='".$_GET["idmateria"]."' AND 
					idprofesor='".$_SESSION['idusuario']."'";
					$resul=$baseDatos->ConsultarBD($sql);
					$contador=mysql_num_rows($resul);
					if($contador>0){
					?>
					<td width="27%"><a style="color: red;" href="validarPreguntas.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Validar Preguntas Autoevaluación</a></td>
				    <?}else{
				    	?>
				    <td width="27%"><a href="validarPreguntas.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Validar Preguntas Autoevaluación</a></td>	
				    	<?
				    }?>
				         <td width="27%"><a href="../scripts/homeMateria.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Volver</a></td>
				  </tr>
			</table>	
		      
			  <?PHP 
			  	if ($_POST["cbo_tema"]!='*')
				{
					$sqltitulotema = "SELECT mei_tema.titulo FROM mei_tema 
							WHERE mei_tema.idtema='".$_POST["cbo_tema"]."' ";
					$resultadotitulotema = $baseDatos->ConsultarBD($sqltitulotema);	
					list($nomtema) = mysql_fetch_row($resultadotitulotema)
				?>
					&nbsp;				
					<div>
					  <table class="tablaGeneral">
					    <tr class="trTitulo">
							<td><div  align="center">Tema: <?PHP echo $nomtema;?></div></td>
					    </tr>
		              </table>
		              <a href="javascript:exportarPreguntas('<?PHP echo $_GET["idmateria"]; ?>','<?PHP echo $_POST["cbo_tema"]; ?>')" class="link"></a></div>				
				<?php
				
					$sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,							
								mei_evatipopregunta.tipo, mei_evagradopregunta.grado, mei_evapreguntas.autoevaluacion FROM mei_evapreguntas, 
								mei_evatipopregunta, mei_evagradopregunta 
							WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
							AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND
							mei_evapreguntas.idtema = '".$_POST["cbo_tema"]."' AND
							(mei_evapreguntas.clonid is null OR mei_evapreguntas.clonid = 0) ORDER BY mei_evapreguntas.tipo_pregunta DESC";
					preguntas($sql,"".$_POST["cbo_tema"]."");
					$codtema = $_POST["cbo_tema"];
						/*Buscar Hijos*/
						$sql_buscar_hijos = "SELECT mei_tema.idtema, mei_tema.titulo, mei_tema.tipo, mei_tema.idtemapadre FROM mei_tema 
							WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.idtemapadre='".$codtema."' ";	
						$resultadobuscarhijos = $baseDatos->ConsultarBD($sql_buscar_hijos);							
						while ( list($codtemahijo, $nomtemahijo, $tipotemahijo, $idtemapadrehijo ) = mysql_fetch_row($resultadobuscarhijos) ){
							if( $tipotemahijo == 2 ){
								$nomtemapadre = buscartemapadre($idtemapadrehijo);
							}
?>
					&nbsp;				
					<div>
					  <table class="tablaGeneral">
					    <tr class="trTitulo">
							<td><div  align="center">Tema: <?PHP echo $nomtemapadre;?> Subtema: <?PHP echo $nomtemahijo;?></div></td>
					    </tr>
		              </table>
		              <a href="javascript:exportarPreguntas('<?PHP echo $_GET["idmateria"]; ?>','<?PHP echo $_POST["cbo_tema"]; ?>')" class="link"></a></div>
<?php			
						$sqlhijotema = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,							
								mei_evatipopregunta.tipo, mei_evagradopregunta.grado, mei_evapreguntas.autoevaluacion FROM mei_evapreguntas, 
								mei_evatipopregunta, mei_evagradopregunta 
								WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
								AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND
								mei_evapreguntas.idtema = '".$codtemahijo."' AND
								(mei_evapreguntas.clonid is null OR mei_evapreguntas.clonid = 0) ORDER BY mei_evapreguntas.tipo_pregunta DESC";
						preguntas($sqlhijotema,$codtemahijo);				
						}
						/*FinBuscar Hijos*/		
?>
					<div>
					  <table class="tablaGeneral">
					    <tr class="trTitulo">
							<td><div  align="center">Preguntas Clonadas</div></td>
					    </tr>
		              </table>
		              <a href="javascript:exportarPreguntas('<?PHP echo $_GET["idmateria"]; ?>','<?PHP echo $_POST["cbo_tema"]; ?>')" class="link"></a></div>
<?php		
						/*Clonadas*/
						$sqlclonadas = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,							
								mei_evatipopregunta.tipo, mei_evagradopregunta.grado, mei_evapreguntas.autoevaluacion, mei_evapreguntas.clonid FROM mei_evapreguntas, 
								mei_evatipopregunta, mei_evagradopregunta 
								WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
								AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND
								mei_evapreguntas.idtema = '".$codtema."' AND
								(mei_evapreguntas.clonid != 0) ORDER BY mei_evapreguntas.tipo_pregunta DESC";
						preguntasclonadas($sqlclonadas,$codtema);						
						/*Fin Clonadas*/				
				}
				else
				{
					$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
							WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.tipo=1";

					$resultado = $baseDatos->ConsultarBD($sql);
					while ( list($codtema, $nomtema ) = mysql_fetch_row($resultado) )		
					{
?>
					&nbsp;				
					<div>
					  <table class="tablaGeneral">
					    <tr class="trTitulo">
							<td><div  align="center">Tema: <?PHP echo $nomtema;?></div></td>
					    </tr>
		              </table>
		              <a href="javascript:exportarPreguntas('<?PHP echo $_GET["idmateria"]; ?>','<?PHP echo $_POST["cbo_tema"]; ?>')" class="link"></a></div>
<?PHP 
						$sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,							
								mei_evatipopregunta.tipo, mei_evagradopregunta.grado, mei_evapreguntas.autoevaluacion FROM mei_evapreguntas, 
								mei_evatipopregunta, mei_evagradopregunta 
								WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
								AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND
								mei_evapreguntas.idtema = '".$codtema."' AND
								(mei_evapreguntas.clonid is null OR mei_evapreguntas.clonid = 0) ORDER BY mei_evapreguntas.tipo_pregunta DESC";
						preguntas($sql,$codtema);	
						
						/*Buscar Hijos*/
						$sql_buscar_hijos = "SELECT mei_tema.idtema, mei_tema.titulo, mei_tema.tipo, mei_tema.idtemapadre FROM mei_tema 
							WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.idtemapadre='".$codtema."' ";	
						$resultadobuscarhijos = $baseDatos->ConsultarBD($sql_buscar_hijos);							
						while ( list($codtemahijo, $nomtemahijo, $tipotemahijo, $idtemapadrehijo ) = mysql_fetch_row($resultadobuscarhijos) ){
							if( $tipotemahijo == 2 ){
								$nomtemapadre = buscartemapadre($idtemapadrehijo);
							}
?>
					&nbsp;				
					<div>
					  <table class="tablaGeneral">
					    <tr class="trTitulo">
							<td><div  align="center">Tema: <?PHP echo $nomtemapadre;?> Subtema: <?PHP echo $nomtemahijo;?></div></td>
					    </tr>
		              </table>
		              <a href="javascript:exportarPreguntas('<?PHP echo $_GET["idmateria"]; ?>','<?PHP echo $_POST["cbo_tema"]; ?>')" class="link"></a></div>
<?php			
						$sqlhijotema = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,							
								mei_evatipopregunta.tipo, mei_evagradopregunta.grado, mei_evapreguntas.autoevaluacion FROM mei_evapreguntas, 
								mei_evatipopregunta, mei_evagradopregunta 
								WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
								AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND
								mei_evapreguntas.idtema = '".$codtemahijo."' AND
								(mei_evapreguntas.clonid is null OR mei_evapreguntas.clonid = 0) ORDER BY mei_evapreguntas.tipo_pregunta DESC";
						
						preguntas($sqlhijotema,$codtemahijo);				
						}
						/*FinBuscar Hijos*/
?>
					&nbsp;				
					<div>
					  <table class="tablaGeneral">
					    <tr class="trTitulo">
							<td><div  align="center">Preguntas Clonadas</div></td>
					    </tr>
		              </table>
		              <a href="javascript:exportarPreguntas('<?PHP echo $_GET["idmateria"]; ?>','<?PHP echo $_POST["cbo_tema"]; ?>')" class="link"></a></div>
<?php	
						/*Clonadas*/
						$sqlclonadas = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,							
								mei_evatipopregunta.tipo, mei_evagradopregunta.grado, mei_evapreguntas.autoevaluacion, mei_evapreguntas.clonid FROM mei_evapreguntas, 
								mei_evatipopregunta, mei_evagradopregunta 
								WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
								AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND
								mei_evapreguntas.idtema = '".$codtema."' AND
								mei_evapreguntas.clonid != 0 ORDER BY mei_evapreguntas.tipo_pregunta DESC";
						preguntasclonadas($sqlclonadas,$codtema);						
						/*Fin Clonadas*/
					}
				}
?>
				&nbsp;
				<table class="tablaMarquesina">
					<tr align="center" valign="middle">
						<td width="32%"><a href="ingresarPregunta.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>&tema=<?echo $_POST["cbo_tema"];?> " class="link">Crear Pregunta</a></td>
					<!--	<td width="41%"><a href="editarPregunta.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Editar Pregunta</a></td> -->
					<?
					
					if($contador>0){
					?>
					<td width="27%"><a style="color: red;" href="validarPreguntas.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Validar Preguntas Autoevaluación</a></td>
				    <?}else{
				    	?>
				    <td width="27%"><a href="validarPreguntas.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Validar Preguntas Autoevaluación</a></td>	
				    	<?
				    }?>
				    <td width="27%"><a href="../scripts/homeMateria.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Volver</a></td>
				  </tr>
			</table>				
<?PHP 			
			}  
			else
			{//NO EXISTEN TEMAS
?>&nbsp;
			<table class="tablaGeneral">
                <tr class="trInformacion">
                  <td>&nbsp;</td>
                </tr>
                <tr align="center" valign="middle" class="trInformacion">
                    <td><strong>No se han Definido temas para esta Materia </strong></td>
                </tr>
                <tr class="trInformacion">
                  <td>&nbsp;</td>
                </tr>
			</table>
<?PHP 
				}
?>&nbsp;		  
			<table class="tablaGeneral">
				<tr class="trSubTitulo" align="center" valign="middle">
					<td><a href="../scripts/homeMateria.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Volver</a></td>
			  </tr>
			</table>
		  </td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><?PHP  menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
</table>
</body>
</html>
<?PHP 
}
else redireccionar('../login/');
?>