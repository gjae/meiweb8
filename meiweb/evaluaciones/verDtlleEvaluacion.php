﻿<?
   include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
   include_once ('../librerias/estandar.lib.php');
   include_once ('../librerias/vistas.lib.php');
	include_once('evaclass.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../menu1/Menu1.class.php');	
/*  Nombre  : vertaller.php
	Autor   : Claudia
	Fecha   : 09/11/2005
	Detalle : Vista de Talleres
	Version :  
*/ 

if(@comprobarSession())
{
	    $autor='1991938';//cambiar esto por autor u otro campo
	if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
	{
		$sql = "SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, 
				mei_evaprevio.titulo, mei_virgrupo.nombre, mei_evaluacion.nombre, mei_evaprevio.fechaactivacion, 
				mei_evaprevio.fechafinalizacion, mei_evaprevio.tiempo, mei_evaprevio.barajarpreg, mei_evaprevio.barajarresp, 
				mei_evaprevio.comentario, mei_evaprevio.intentos, mei_evaprevio.valorIntento,
				mei_evamodcalificar.modcalificar, mei_evaprevio.mostrarnota, mei_evaprevio.estado, mei_evaprevio.idtipoprevio,
				mei_tipoprevio.tipoprevio, mei_evaprevio.idtiposubgrupo FROM mei_evaprevio, mei_usuario, mei_evamodcalificar, mei_tipoprevio,
				mei_evaluacion, mei_evavirgrupo, mei_virgrupo	WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND 
				mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo AND 
				mei_evaprevio.idautor = mei_usuario.idusuario AND mei_evaprevio.idmodCalificar = mei_evamodcalificar.idmodcalificar
				AND mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaprevio.idprevio=".$_GET["idprevio"]." UNION SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, 
				mei_evaquiz.titulo, mei_virgrupo.nombre, mei_evaluacion.nombre, mei_evaquiz.fechaactivacion, 
				mei_evaquiz.fechafinalizacion, mei_evaquiz.tiempo, mei_evaquiz.barajarpreg, mei_evaquiz.barajarresp, 
				mei_evaquiz.comentario, mei_evaquiz.intentos, mei_evaquiz.valorIntento,
				mei_evamodcalificar.modcalificar, mei_evaquiz.mostrarnota, mei_evaquiz.estado, mei_evaquiz.idtipoprevio,
				mei_tipoprevio.tipoprevio, mei_evaquiz.idtiposubgrupo FROM mei_evaquiz, mei_usuario, mei_evamodcalificar, mei_tipoprevio,
				mei_evaluacion, mei_evavirgrupo, mei_virgrupo	WHERE mei_evaquiz.idevaluacion = mei_evaluacion.idevaluacion AND 
				mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo AND 
				mei_evaquiz.idautor = mei_usuario.idusuario AND mei_evaquiz.idmodCalificar = mei_evamodcalificar.idmodcalificar
				AND mei_evaquiz.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaquiz.idprevio=".$_GET["idprevio"];
	}
	else
	{
		$sql = "SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, 
				mei_evaprevio.titulo, mei_grupo.nombre, mei_evaluacion.nombre, mei_evaprevio.fechaactivacion, 
				mei_evaprevio.fechafinalizacion, mei_evaprevio.tiempo, mei_evaprevio.barajarpreg, mei_evaprevio.barajarresp, 
				mei_evaprevio.comentario, mei_evaprevio.intentos, mei_evaprevio.valorIntento,
				mei_evamodcalificar.modcalificar, mei_evaprevio.mostrarnota, mei_evaprevio.estado, mei_evaprevio.idtipoprevio,
				mei_tipoprevio.tipoprevio, mei_evaprevio.idtiposubgrupo FROM mei_evaprevio, mei_usuario, mei_evamodcalificar, mei_tipoprevio,
				mei_evaluacion, mei_evagrupo, mei_grupo	WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND 
				mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = mei_grupo.idgrupo AND 
				mei_evaprevio.idautor = mei_usuario.idusuario AND mei_evaprevio.idmodCalificar = mei_evamodcalificar.idmodcalificar
				AND mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaprevio.idprevio=".$_GET["idprevio"]." UNION SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, 
				mei_evaquiz.titulo, mei_grupo.nombre, mei_evaluacion.nombre, mei_evaquiz.fechaactivacion, 
				mei_evaquiz.fechafinalizacion, mei_evaquiz.tiempo, mei_evaquiz.barajarpreg, mei_evaquiz.barajarresp, 
				mei_evaquiz.comentario, mei_evaquiz.intentos, mei_evaquiz.valorIntento,
				mei_evamodcalificar.modcalificar, mei_evaquiz.mostrarnota, mei_evaquiz.estado, mei_evaquiz.idtipoprevio,
				mei_tipoprevio.tipoprevio, mei_evaquiz.idtiposubgrupo FROM mei_evaquiz, mei_usuario, mei_evamodcalificar, mei_tipoprevio,
				mei_evaluacion, mei_evagrupo, mei_grupo	WHERE mei_evaquiz.idevaluacion = mei_evaluacion.idevaluacion AND 
				mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = mei_grupo.idgrupo AND 
				mei_evaquiz.idautor = mei_usuario.idusuario AND mei_evaquiz.idmodCalificar = mei_evamodcalificar.idmodcalificar
				AND mei_evaquiz.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaquiz.idprevio=".$_GET["idprevio"];
	}
		$resultado=$baseDatos->ConsultarBD($sql);	
		//print $sql;	
		list($prinomAutor,$segnomAutor,$priapeAutor,$segapeAutor, $titulo, $nomgrupo, $nomEva, $fecha_a, $fecha_f, $tiempo, $bpreg, $bresp, $comentario, $intentos, $valorIntento, $mcalificar, $mnota, $estado, $idtipoprevio, $tipoprevio, $idtiposubgrupo) = mysql_fetch_row($resultado);

		if ($_GET['descongelar']==1) {
			$sql = "UPDATE mei_usupreviodtlle SET
			        congelar=0
			        WHERE mei_usupreviodtlle.idprevio='".$_GET['idprevio']."'";
			$baseDatos->ConsultarBD($sql);
		}
?>	
<head>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MEIWEB</title>
<script type="text/JavaScript">
<!--
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
var ventanaPrevio=false;

function vizPrevio()
{	
	   var izquierda = (screen.availWidth - 700) / 2; 
	   var arriba = (screen.availHeight - 550) / 2; 
	
									
	if (typeof ventanaPrevio.document == 'object')
	 {
		ventanaPrevio.close()
	 }
	 
	ventanaPrevio = window.open('visualizarPrevio.php','visualizarPrevio','width=700,height=550,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');			
  
}
function vizPrevio1()
{	
	   var izquierda = (screen.availWidth - 700) / 2; 
	   var arriba = (screen.availHeight - 550) / 2; 
	
									
	if (typeof ventanaPrevio.document == 'object')
	 {
		ventanaPrevio.close()
	 }
	 
	/*ventanaPrevio = window.open('visualizarPrevio.php','visualizarPrevio','width=700,height=550,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');	*/			
  ventanaPrevio = window.open('visualizacionPreviosinRespuesta.php','Visualizacion','width=700,height=550,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');
}

function vizPrevio2()
{	
	   var izquierda = (screen.availWidth - 700) / 2; 
	   var arriba = (screen.availHeight - 550) / 2; 
	
									
	if (typeof ventanaPrevio.document == 'object')
	 {
		ventanaPrevio.close()
	 }
	 
	/*ventanaPrevio = window.open('visualizarPrevio.php','visualizarPrevio','width=700,height=550,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');	*/			
  ventanaPrevio = window.open('visualizacionPreviosinRespuestaImprimir.php','Visualizacion','width=700,height=550,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');
}
function vizPrevio3()
{	
	   var izquierda = (screen.availWidth - 700) / 2; 
	   var arriba = (screen.availHeight - 550) / 2; 
	
									
	if (typeof ventanaPrevio.document == 'object')
	 {
		ventanaPrevio.close()
	 }
	 
	ventanaPrevio = window.open('visualizarPrevioImprimir.php','visualizarPrevio','width=700,height=550,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');			
  
}

</script>
</head>

<body>
<table class="tablaPrincipal">
  <tr valign="top">
  	<td class="tablaEspacio">&nbsp;</td>
	<td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
	<td class="tablaEspacio">&nbsp;</td>
    <td class="tdGeneral" valign="top">
    	<table class="tablaMarquesina" >
        	<tr>
<?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado);
?>
          <td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nombre))?></a><a> -> </a><a href="../evaluaciones/verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a> -> Ver Evaluaci&oacute;n </td>
        </tr>
     </table>
        &nbsp;
<?
	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
	{ //if idtipousuario = 2
			if (!empty($_GET["idgrupo"]))	{
		$orde=$_GET["idgrupo"];
			$orden=ltrim($orde)."-".$nomgrupo;
		}
		else $orden=$_GET["cbo_orden"];
?>
        <table class="tablaGeneral" width="100%">
          <tr class="trSubTitulo">
            <td width="25%" align="left" valign="middle"><strong>Autor:</strong></td>
            <td width="80%" colspan="3"><b><?=$prinomAutor." ".$segnomAutor." ".$priapeAutor." ".$segapeAutor?></b></td>
          </tr>
          <tr class="trInformacion">
            <td width="20%" align="left" valign="middle"><strong>Grupo:</strong></td>
            <td width="40%"><strong><?=$nomgrupo?></strong></td>
            <td width="10%"><b>Subgrupo:</b></td>
            <td width="30%"><? if (empty($idtiposubgrupo)) print "Evaluacion No Grupal";
								else
								{ 
									$sql = "SELECT mei_tiposubgrupo.tiposubgrupo FROM mei_tiposubgrupo
											WHERE mei_tiposubgrupo.idtiposubgrupo=".$idtiposubgrupo;
									$rsql = $baseDatos->ConsultarBD($sql);
									list ($subgrupo) = mysql_fetch_row($rsql);
									print $subgrupo;
								}
							?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Nota a la que pertenece: </strong></td>
            <td colspan="3"><strong>
            <?=$nomEva?>
            </strong></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Tipo de Evaluaci&oacute;n: </strong></td>
            <td colspan="3"><strong>
            <?if($_SESSION['idtipousuario']==2){?>
      		 <?=$tipoprevio?>
			  <?}?>	
			  <?if($_SESSION['idtipousuario']==5){
			  	$tipoprevio="Previo"?>
      		 <?=$tipoprevio?>
			  <?}?>	 
            </strong></td>
          </tr>
          <tr class="trInformacion">
            <td width="25%" align="left" valign="middle"><strong>Titulo de la Evaluaci&oacute;n:</strong></td>
            <td colspan="3"><strong><? print $titulo."<br>";?></strong></td>
          </tr>
          
          
          <tr class="trInformacion">
            <td width="25%" align="left" valign="middle"><strong>Fecha Activaci&oacute;n: </strong></td>
            <td colspan="3"><?=mostrarFechaTexto($fecha_a,1)?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Fecha Finalizaci&oacute;n </strong></td>
            <td colspan="3"><?=mostrarFechaTexto($fecha_f,1)?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Tiempo de Duraci&oacute;n: </strong></td>
            <td colspan="3"><?=$tiempo." Minutos"?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Barajar Preguntas </strong></td>
            <td colspan="3"><? if ($bpreg==1) print "Si";
					elseif($bpreg==0) print "No";?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Barajar Respuestas: </strong></td>
            <td colspan="3"><? if ($bresp==1) print "Si";
					elseif($bresp==0) print "No";?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>N&uacute;mero de Intentos: </strong></td>
            <td colspan="3"><?=$intentos?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Disminuir la Nota de cada intento en:</strong></td>
            <td colspan="3"><?=($valorIntento*100)."%"?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Mostrar Nota: </strong></td>
            <td colspan="3"><? if ($mnota==1) print "Si";
					elseif($mnota==0) print "No";?></td>
          </tr>
          
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Forma de Calificaci&oacute;n: </strong></td>
            <td colspan="3"><?=$mcalificar?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Estado:</strong></td>
            <td colspan="3"><? if ($estado == 1) print "Activo";
							   elseif($estado == 0) print "Desactivo";
							?></td>
          </tr>

          <?
		  	$sql = "SELECT mei_relevalarchi.idarchivo, mei_relevalarchi.archivo,
					mei_relevalarchi.localizacion FROM mei_relevalarchi
					WHERE mei_relevalarchi.idevaluacion = '".$_GET["idprevio"]."'";
			$resArchivo = $baseDatos->ConsultarBD($sql);
			if ( mysql_num_rows($resArchivo) > 0 )
			{
		  ?>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Archivo(s) Adjunto(s): </strong></td>
			<?
				$cont=0;
				while ( list($idaa, $archivo, $localizacion) = mysql_fetch_row($resArchivo))
				{
					if($cont==0)
						$adjunto="<a href='descargarArchivoE.php?archivo=".$localizacion."&idprevio=".$_GET["idprevio"]."' class='link' target='_black'>".$archivo."</a>";
					else
						$adjunto.=" | "."<a href='descargarArchivoE.php?archivo=".$localizacion."&idprevio=".$_GET["idprevio"]."' class='link' target='_black'>".$archivo."</a>";
					$cont++;
				}
			?>
			<td colspan="3"><?=$adjunto?></td>
          </tr>
		  <?
		  	}
		  ?>


		  <?
		  	if ( !empty($comentario) )
			{
		  ?>
          <tr class="trListaClaro">
            <td width="25%" align="left" valign="top"><strong>Comentario:</strong></td>
            <td colspan="3"><? print stripslashes(base64_decode($comentario))."<br>";?></td>
          </tr>
		  <?
		  	}
		  ?>
	  	</table>
<?
	
		//********************************************************
		
		/*if ( isset($evaluacion) ) print "El objeto ya existe";
		else print "El objeto no existe";*/
		unset($evalucion);
		
		$evaluacion = new evaluacion($_GET["idprevio"],$_GET["idmateria"],$idtipoprevio, $tipoprevio);		

        
        if($_POST["gradodifOrd"] == 1){
    		if($_GET['esp']!='reposicion')
    		{
    			$sql = "SELECT mei_evapreguntas.idpregunta,mei_evapreguntas.idgrado, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta, mei_evatipopregunta.tipo, mei_tema.titulo,
    				mei_evadtlleprevio.valor_pregunta FROM mei_evadtlleprevio, mei_evapreguntas, mei_evatipopregunta, mei_tema
    				WHERE mei_evadtlleprevio.idpregunta = mei_evapreguntas.idpregunta AND 
    				mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
    				mei_evapreguntas.idtema = mei_tema.idtema AND mei_evadtlleprevio.idprevio=".$_GET["idprevio"]." 
                    ORDER BY mei_evapreguntas.idgrado ASC";
    		}
    		else
    		{
    			$sql = "SELECT mei_evapreguntas.idpregunta,mei_evapreguntas.idgrado, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta, mei_evatipopregunta.tipo, mei_tema.titulo,
    				mei_evadtllequiz.valor_pregunta FROM mei_evadtllequiz, mei_evapreguntas, mei_evatipopregunta, mei_tema
    				WHERE mei_evadtllequiz.idpregunta = mei_evapreguntas.idpregunta AND 
    				mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
    				mei_evapreguntas.idtema = mei_tema.idtema AND mei_evadtllequiz.idprevio=".$_GET["idprevio"]." 
                    ORDER BY mei_evapreguntas.idgrado ASC";
    		}             
        }        
        else{
    		if($_GET['esp']!='reposicion')
    		{
    			$sql = "SELECT mei_evapreguntas.idpregunta,mei_evapreguntas.idgrado, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta, mei_evatipopregunta.tipo, mei_tema.titulo,
    				mei_evadtlleprevio.valor_pregunta FROM mei_evadtlleprevio, mei_evapreguntas, mei_evatipopregunta, mei_tema
    				WHERE mei_evadtlleprevio.idpregunta = mei_evapreguntas.idpregunta AND 
    				mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
    				mei_evapreguntas.idtema = mei_tema.idtema AND mei_evadtlleprevio.idprevio=".$_GET["idprevio"];
    		}
    		else
    		{
    			$sql = "SELECT mei_evapreguntas.idpregunta,mei_evapreguntas.idgrado, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta, mei_evatipopregunta.tipo, mei_tema.titulo,
    				mei_evadtllequiz.valor_pregunta FROM mei_evadtllequiz, mei_evapreguntas, mei_evatipopregunta, mei_tema
    				WHERE mei_evadtllequiz.idpregunta = mei_evapreguntas.idpregunta AND 
    				mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
    				mei_evapreguntas.idtema = mei_tema.idtema AND mei_evadtllequiz.idprevio=".$_GET["idprevio"];
    		}            
        }
		//print $sql."<p>";
		$resultado = $baseDatos->ConsultarBD($sql);
		
		if ( mysql_num_rows($resultado)>0 )
		{//IF HAY PREGUNTAS
		//$cont = 0;
	?>	  &nbsp;

<!-- _______________!!!______________ - cambios nuevo codigo sabado 06 agosto 2016 - ____________________!!!___________________ -->

<script language="javascript">
	window.name = "sumarTiempo";

function adicionar(idmateria, idgrupo, idprevio)
	{
	   var izquierda = (screen.availWidth - 200) / 2; 
	   var arriba = (screen.availHeight - 180) / 2; 

		calificacion = window.open("adicionarTiempoEvaluacion.php?idmateria="+idmateria+"&idgrupo="+idgrupo+"&idprevio="+idprevio+"","Sumar","width=250,height=250,left="+izquierda+",top="+arriba+",scrollbars=NO,menubars=NO,status=NO,statusbar=NO,status=NO,resizable=NO,location=NO")
	}

function descongelar(idprevio)
	{

		if (confirm('Está seguro de descongelar?')) {
			document.postGradoDif.action="verDtlleEvaluacion.php?idprevio="+idprevio+"&idmateria=<?= $_GET["idmateria"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&materia=<?= $_GET['materia'] ?>&cbo_orden=<?$_GET["cbo_orden"]?>&descongelar=1";
        document.postGradoDif.submit();
		}

		
		
	}

</script>

<!-- !!!______________ - cambios nuevo codigo sabado 06 agosto 2016 - !!!___________________ -->

<!-- ________________________________________________________________ -->
    <script>
        function ordenargrado(){
            document.getElementById("gradodifOrd").value = 1;
            document.getElementById("postGradoDif").submit();
        }
    </script>
    <form id="postGradoDif" name="postGradoDif" method="POST" action="verDtlleEvaluacion.php?idprevio=<?=$_GET['idprevio']?>&idmateria=<?=$_GET['idmateria']?>&idgrupo=<?=$_GET['idgrupo']?>&materia=<?=$_GET['materia']?>">
        <input type="hidden" name="gradodifOrd" id="gradodifOrd" value="0" />
    </form>
		 <table class="tablaGeneral">
           <tr class="trTitulo" align="left">
             <td colspan="5" valign="middle">Preguntas Asignadas a la Evaluaci&ograve;n </td>
           </tr>
           
           <tr class="trSubTitulo">
             <td width="159" align="center" valign="middle">Pregunta</td>
             <td width="55" align="center" valign="middle"><a href="#" onclick="ordenargrado()"><span title="Grado de Dificultad">GD</span></a></td>
             <td width="55" align="center" valign="middle">Tipo</td>
             <td width="45" align="center" valign="middle">Tema</td>
             <td width="34" align="center" valign="middle">Valor</td>
           </tr>
	<?
		$i = 0;
		$cont = 1;
		while ( list($idpreg, $idgrado, $preg, $tipo_preg, $tipo, $tema, $valor) = mysql_fetch_row($resultado) )
		{//WHILE 1 
		
			$evaluacion->agregar_preg($idpreg,$idpreg, stripslashes(base64_decode($preg)), $tipo_preg, $tipo, $tema, $valor);				
			//$cont++;
			if ( ($i%2)==0 ) $clase = "trListaOscuro";
			else $clase = "trListaClaro";
	?>	   
           <tr class="<?=$clase?>">
             <td class="<?=$clase?>" align="center" valign="middle"><?=$cont?>.
			 
			 <?	
				if ( $tipo_preg == 4 )
				{
					list($afirmacion) = explode("[$$$]",stripslashes(base64_decode($preg)));
					print substr(strip_tags($afirmacion),0,20)."...";
				}
					else print substr(strip_tags(stripslashes(base64_decode($preg))),0,30)."...";?></td>
              <td class="<?=$clase?>" align="center" valign="middle"><?php
              if($idgrado == 1){ $inigrado = "F"; $nombregrado = "Facil"; }
              elseif($idgrado == 2){ $inigrado = "M"; $nombregrado = "Medio"; }
              elseif($idgrado == 3){ $inigrado = "D"; $nombregrado = "Dificil"; }
              ?><span title="<?php echo $nombregrado; ?>"><?=$inigrado?></span></td>
             <td class="<?=$clase?>" align="center" valign="middle"><?=$tipo?></td>
             <td class="<?=$clase?>" align="center" valign="middle"><?=$tema?></td>
             <td class="<?=$clase?>" align="center" valign="middle"><?=round(($valor*100),2)."%"?></td>
           </tr>
	<?
			$cont++;
			$i++;
			
		}//FIN WHILE 1
		
		//*********************************************************/
	?>
          <tr class="trInformacion">
             <td colspan="5" height="20" align="center" valign="middle">
             			</td>
		   </tr>
           <tr class="trInformacion">
             <td colspan="" align="center" valign="middle">
             	<a href="javascript:vizPrevio3()" class="link">
             		<strong>Visualizar <?=$tipoprevio?> Para imprimir con respuestas &nbsp; </strong>
             		</a>
             		</td>
              <td colspan="2" align="center" valign="middle">
             	
             			</td>
              <td colspan="2" align="center" valign="middle">
             	
             		<a href="javascript:vizPrevio2()" class="link">
             			<strong>Visualizar <?=$tipoprevio?> para imprimir Sin respuestas</strong>
             			</a>
             			</td>			
		   </tr>
		      <tr class="trInformacion">
             <td colspan="5" height="20" align="center" valign="middle">
             			</td>
		   </tr>
		      <tr class="trInformacion">
             <td colspan="5" align="center" valign="middle">
             	<a href="javascript:vizPrevio()" class="link">
             		<strong>Visualizar <?=$tipoprevio?> &nbsp; </strong>
             		</a>
             		<a href="javascript:vizPrevio1()" class="link">
             			<strong><?=$tipoprevio?> Sin respuestas</strong>
             			</a>
             			</td>
             			
		   </tr>
		  <!-- ________________________________ cambios nuevo codigo domingo 01 mayo 2016 __________________________________________ -->
<?php
$id_int = 2;
?>
            <tr class="tablaMarquesina" width="100%" border="0">
           <td align="center" colspan="5">
           	 <a>Este componente, se muestra la lista de Direcciones IP de los usuarios conectados: <img src="../evaluaciones/imagenes/ip.gif" width="16" height="16" align="texttop"><a href="ipGrupo.php?idGrupo=<?PHP echo $_GET['idgrupo']; ?>&idPrevio=<?PHP echo $_GET['idprevio']; ?>&idInt=<?PHP echo $id_int ?>"> Ver Tabla</a>.
		</td>
           	</tr>
           	 <tr class="tablaMarquesina" width="100%" border="0" height="20">
           <td align="center" colspan="5">
           	</td>
          </tr>
<!-- _______________!!!______________ - cambios nuevo codigo sabado 06 agosto 2016 - ____________________!!!___________________ -->

		<?
		$sqlcongeva = "SELECT congelar FROM mei_usupreviodtlle WHERE idprevio='".$_GET['idprevio']."' AND congelar=1";
		$resconge = $baseDatos->ConsultarBD($sqlcongeva);
		list($conge) = mysql_fetch_array($resconge);
		if(!empty($conge)){
			?>
		<tr class="tablaMarquesina" width="100%" border="0">
		<td align="center" colspan="3">
			<a href="javascript:adicionar('<?=$_GET["idmateria"]?>','<?=$_GET["idgrupo"]?>','<?=$_GET["idprevio"]?>')" class="link">
			<img src="imagenes/time.png" alt="tiempo" width="16" height="16" border="0">
			</a> 
		</td>
		<td align="center" colspan="2">
		<a href="javascript:descongelar('<?=$_GET["idprevio"]?>')" class="link" style="color: red;">Descongelar </a>
		</td>
		</tr>
		<?} 
		  else { 
		  		?>
		<tr class="tablaMarquesina" width="100%" border="0">
		<td align="center" colspan="5">
			<a href="javascript:adicionar('<?=$_GET["idmateria"]?>','<?=$_GET["idgrupo"]?>','<?=$_GET["idprevio"]?>')" class="link">
			<img src="imagenes/time.png" alt="tiempo" width="16" height="16" border="0">
			</a> 
		</td>
		</tr>
		<?}?>
	    
	    					

<tr class="tablaMarquesina" width="100%" border="0" height="20">
           <td align="center" colspan="5">
           	</td>
          </tr>

<!-- _______________!!!______________ - cambios nuevo codigo sabado 06 agosto 2016 - ____________________!!!___________________ -->



<!-- ________________________________ cambios nuevo codigo domingo 01 mayo 2016 __________________________________________ -->
         </table>
      <?
		}//FIN IF HAY PREGUNTAS
		else
		{//NO HAY PREGUNTAS
	?>&nbsp;
	<table class="tablaMarquesina" border="1">
      <tr align="center" valign="middle">
        <td>Actualmente esta Evaluaci&oacute;n no tiene  preguntas asignadas.</td>
      </tr>
    </table>
	<?	
		}//FIN NO HAY PREGUNTAS
	?>
&nbsp;		  
		  <table class="tablaMarquesina" width="100%" border="0">
          <tr class="trSubTitulo">
            <td width="20%" align="center" valign="middle"><img src="imagenes/resultados.gif" alt="eliminar" width="16" height="16" border="0"> <a href="../evaluaciones/notasEvaluacion.php?idprevio=<?=$_GET["idprevio"]?>&intentos=<?=$intentos?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$orden?>" class="link">Ver Resultados</a> </td>            
			<td width="20%" align="center" valign="middle"><a href="estadisticasEvaluacion.php?idprevio=<?=$_GET["idprevio"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$orden?>" class="link"><img src="imagenes/cartelera.gif" width="16" height="16" border="0">Estadisticas</a></td>
			<td width="20%" align="center" valign="middle"><img src="imagenes/icono_publicar.png" width="16" height="16"> <a href="../cartelera/agregarCarteleraRespuesta.php?id_previo=<?php print  $_GET["idprevio"]?>&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>" title="Publicar respuestas" class="link">Publicar</a></td>
            <?
            
				if ($idtipoprevio == 1)
				{
			?>
			<td width="20%" align="center" valign="middle"><img src="imagenes/modificar.gif" alt="eliminar" width="16" height="16" border="0"> <a href="editarEvaluacion.php?idprevio=<?=$_GET["idprevio"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$orden?>" class="link"> Editar </a> </td>
            <?
				}
				else
				{
			?>
			<td width="20%" align="center" valign="middle"><img src="imagenes/modificar.gif" alt="eliminar" width="16" height="16" border="0"> <a href="editarQuiz.php?idprevio=<?=$_GET["idprevio"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$orden?>" class="link"> Editar </a> </td>
			<?
				}
			?>
			<td width="20%" align="center" valign="middle"><a onClick="return confirm('Eliminar esta Evaluacion. Al Eliminar esta Evaluacion tambien se eliminaron todas las respuestas a esta')" href="guardarEvaluacion.php?modo=DEL&idprevio=<?=$_GET["idprevio"]?>&idmateria=<?=$_GET["idmateria"]?>&esp=<?=$_GET['esp']?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET['materia']?>&cbo_orden=<?=$orden?>" class="link"><img src="imagenes/eliminar.gif" alt="eliminar" width="16" height="16" border="0">Eliminar</a></td>
		  </table>
	<?
			$_SESSION["evaluacion"] = $evaluacion;
	}//FIN if idtipousuario = 2
	else
	{//OTRO USUARIO
?>
	<table class="tablaGeneral" width="100%">
      <tr class="trSubTitulo">
        <td width="20%" align="left" valign="middle"><strong>Autor:</strong></td>
        <td width="80%" colspan="3"><b>
          <?=$prinomAutor." ".$segnomAutor." ".$priapeAutor." ".$segapeAutor?>
        </b></td>
      </tr>
      <tr class="trInformacion">
        <td width="20%" align="left" valign="middle"><strong>Grupo:</strong></td>
        <td width="40%"><?=$nomgrupo?></td>
        <td width="10%"><b>Subgrupo:</b></td>
        <td width="30%"><? if (empty($idtiposubgrupo)) print "Evaluacion No Grupal";
								else
								{ 
									$sql = "SELECT mei_tiposubgrupo.tiposubgrupo FROM mei_tiposubgrupo
											WHERE mei_tiposubgrupo.idtiposubgrupo=".$idtiposubgrupo;
									$rsql = $baseDatos->ConsultarBD($sql);
									list ($subgrupo) = mysql_fetch_row($rsql);
									print $subgrupo;
								}
							?></td>
      </tr>
      
      <tr class="trInformacion">
        <td width="20%" align="left" valign="middle"><strong>Titulo de la Evaluaci&oacute;n:</strong></td>
        <td colspan="3"><? print $titulo."<br>";?></td>
      </tr>
      <tr class="trInformacion">
        <td width="20%" align="left" valign="middle"><strong>Fecha Activaci&oacute;n: </strong></td>
        <td colspan="3"><?=mostrarFechaTexto($fecha_a,1)?></td>
      </tr>
      <tr class="trInformacion">
        <td align="left" valign="middle"><strong>Fecha Finalizacion </strong></td>
        <td colspan="3"><?=mostrarFechaTexto($fecha_f,1)?></td>
      </tr>
      <tr class="trInformacion">
        <td align="left" valign="middle"><strong>Tiempo de Duraci&oacute;n: </strong></td>
        <td colspan="3"><?=$tiempo." Minutos"?></td>
      </tr>
      
      <tr class="trInformacion">
        <td align="left" valign="middle"><strong>N&uacute;mero de Intentos: </strong></td>
        <td colspan="3"><?=$intentos?></td>
      </tr>
	  <?
		if ( !empty($comentario) )
		{
	  ?>      
      <tr align="left" valign="middle" class="trInformacion">
        <td><strong>Disminuir la Nota de cada intento en:</strong></td>
        <td colspan="3"><?=($valorIntento*100)."%"?></td>
      </tr>
      <tr align="left" valign="middle" class="trDescripcion">
        <td width="25%"><strong>Comentario:</strong></td>
        <td colspan="3"><?=stripslashes(base64_decode($comentario))."<br>";?></td>
      </tr>
	  <?
		}
	  ?>
    </table>

<?
	}//FIN OTRO USUARIO
	?>        
&nbsp;		
		<table class="tablaMarquesina" width="100%" border="0">
  <tr valign="middle" class="trSubTitulo">
    <?
			if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
			{ //if idtipousuario = 3  )*/
	?>
		  <td width="34%" align="center"><a href="../evaluaciones/confirmarContestarEvaluacion.php?idprevio=<?=$_GET["idprevio"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagenes/ver.gif" alt="Actividad" width="16" height="16" border="0"> Responder/Ver Resultados </a></td>
 	      <td width="33%" align="center"><a href="estadisticasEvaluacion.php?idprevio=<?=$_GET["idprevio"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagenes/cartelera.gif" width="16" height="16" border="0">Estadisticas</a></td>
	<?

			}//fin if tipousuario = 3
			if (($idtipoprevio == 1) or ($_GET['esp']=='reposicion'))
			{
	?>	
			<td width="33%" align="center"><img src="imagenes/volver.gif" alt="eliminar" width="16" height="16" border="0"> <a href="verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$orden?>" class="link">Volver</a></td>
	<?
			}
			else
			{
				if($_SESSION['idtipousuario']==6){
	?>
			
			<td width="33%" align="center"><img src="imagenes/volver.gif" alt="eliminar" width="16" height="16" border="0"> <a href="../actividades/verVirActividad.php?idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$orden?>" class="link">Volver</a></td>
	<?
				}else{
					?>
					<td width="33%" align="center"><img src="imagenes/volver.gif" alt="eliminar" width="16" height="16" border="0"> <a href="../actividades/verActividad.php?idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET['esp']?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$orden?>" class="link">Volver</a></td>
	
					<?
				}
			}
	?>	
	
  </tr>
</table>


  </td>
	<td class="tablaEspacio">&nbsp;</td>
	<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
	<td class="tablaEspacioDerecho">&nbsp;</td>
</tr>
					</table>
</body>
<? 
  }
?>
</html>
