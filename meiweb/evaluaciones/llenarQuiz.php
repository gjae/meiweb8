<?
	include_once ('../menu1/Menu1.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once('evaclass.php');
	include_once ('../menu/Menu.class.php');	
	if(comprobarSession())
	{	
	$baseDatos= new BD();	
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraBasica' , '' );
	$evaluacion = $_SESSION["evaluacion"]; 
	?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
window.name = "llenarEvaluacion";
var ventanaPreguntas=false;
function verPregunta(idpregunta)
{	
	   var izquierda = (screen.availWidth - 700) / 2; 
	   var arriba = (screen.availHeight - 300) / 2; 
	
									
	if (typeof ventanaPreguntas.document == 'object')
	 {
		ventanaPreguntas.close()
	 }
	 
	ventanaPreguntas = window.open('visualizarPregunta.php?idpregunta='+idpregunta,'visualizarPregunta','width=700,height=300,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');				
}
var ventanaPrevio=false;
function vizPrevio()
{	
	   var izquierda = (screen.availWidth - 700) / 2; 
	   var arriba = (screen.availHeight - 550) / 2; 
	
									
	if (typeof ventanaPrevio.document == 'object')
	 {
		ventanaPrevio.close()
	 }
	 
	ventanaPrevio = window.open('visualizarPrevio.php','visualizarPrevio','width=700,height=550,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');				
}
function enviar()
{
	document.frm_preguntas.submit();
}
function volver(id, mat)
{
	document.frm_guardarPreguntas.action = "editarEvaluacion.php?idprevio="+id+"&idmateria="+mat+"materia=<?=$_GET['materia']?>";
	document.frm_guardarPreguntas.submit()
}
function volverQ(id, mat)
{
	document.frm_guardarPreguntas.action = "editarQuiz.php?idprevio="+id+"&idmateria="+mat+"materia=<?=$_GET['materia']?>";
	document.frm_guardarPreguntas.submit()
}

function guardar_cambios()
{   
    if (document.frm_guardarPreguntas.hid_porcentaje.value<=1)
	 {
		if (confirm('El Procentaje Acumulado de las Preguntas es '+(document.frm_guardarPreguntas.hid_porcentaje.value)*100+'%. Desea Continuar?'))
		    {			
			  document.frm_guardarPreguntas.action = "guardarPreguntasQuiz.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>";
			  document.frm_guardarPreguntas.submit()
		     }
	} 
					  
		
	
	else
	{
		alert('El Procentaje Acomulado de las Preguntas Supera el 100%')
	}
}

function agregar(idpreg)
{
   var izquierda = (screen.availWidth - 200) / 2; 
   var arriba = (screen.availHeight - 180) / 2; 
   
   ventana = window.open("agregarPreguntaQuiz.php?idpregunta="+idpreg+"","ventana","width=240,height=180,left="+izquierda+",top="+arriba+",scrollbars=NO,menubars=NO,status=NO,statusbar=NO,status=NO,resizable=NO,location=NO")
}

</script>
</head>
<body>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
		    <td valign="top" class="tdGeneral">
              <table class="tablaMarquesina" >
                <tr>
<?	 				
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($mat) = mysql_fetch_row($resultado); 
?>	  
                      <td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($mat))?></a><a> -> </a><a href="../evaluaciones/verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a><a> -> Ingresar Evaluaci&oacute;n </a></td>
                </tr>
              </table><br>
      
			  <table class="tablaGeneral">
                <tr class="trSubTitulo">
                  <td class="trSubTitulo" width="50%" align="left" valign="middle">Preguntas de la Evaluaci&oacute;n </td>
                  <td class="trSubTitulo" width="50%" align="left" valign="middle">Banco de Preguntas </td>
                </tr>
                <tr class="trListaClaro">
                  <td class="trListaClaro" align="center" valign="top">
				  <!-- CELDA DEL PREVIO -->
<?
				/*$sql = "SELECT mei_evadtlleprevio.idpregunta, mei_evatipopregunta.tipo, mei_tema.titulo,
						mei_evadtlleprevio.valor_pregunta FROM mei_evadtlleprevio, mei_evapreguntas, mei_evatipopregunta, mei_tema
						WHERE mei_evadtlleprevio.idpregunta = mei_evapreguntas.idpregunta AND 
						mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
						mei_evapreguntas.idtema = mei_tema.idtema AND mei_evadtlleprevio.idprevio=".$_GET["idprevio"];
				$resultado = $baseDatos->ConsultarBD($sql);*/
			 if ( count($evaluacion->preguntas)>0 )
 			 //if ( mysql_num_rows($resultado)>0 )
			  {//IF HAY PREGUNTAS
			  ?>&nbsp;	  
              <table border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
                <tr class="trSubTitulo">
                  <td width="30%" height="26" align="center" valign="middle">Pregunta</td>
                  <td width="15%" align="center" valign="middle">Tipo</td>
                  <td width="15%" align="center" valign="middle">Tema</td>
                  <td width="10%" align="center" valign="middle">Valor</td>
                  <td width="10%" align="center" valign="middle">Editar</td>
                  <td width="10%" align="center" valign="middle">Eliminar</td>
                </tr>
			<?
				$porcentaje = 0;
				$i = 0;
				while( list($cont) = each($evaluacion->preguntas) )
				//while ( list($idpreg, $tipo, $tema, $valor) = mysql_fetch_row($resultado) )
				{//WHILE IMPRIMIR PREGUNTAS
					if ( ($i%2)==0 ) $clase = "trListaOscuro";
					else $clase = "trInformacion";
					$porcentaje += $evaluacion->preguntas[$cont]->valor;
					//$porcentaje += $valor
			?>	
                <tr class="<?=$clase?>">
                  <td align="center" valign="middle">
				  <?
				  /*$idpreg*/
				  	if ( $evaluacion->preguntas[$cont]->idtipo == 4 )
					{
						list($afirmacion) = explode("[$$$]",$evaluacion->preguntas[$cont]->pregunta);
						print substr(strip_tags($afirmacion),0,20)."...";
					}
					else print substr(strip_tags($evaluacion->preguntas[$cont]->pregunta),0,30)."...";
				  //$evaluacion->preguntas[$cont]->pregunta
				  ?></td>
                  <td align="center" valign="middle"><?=/*$tipo*/$evaluacion->preguntas[$cont]->tipo?></td>
                  <td align="center" valign="middle"><?=/*$tema*/$evaluacion->preguntas[$cont]->tema?></td>
                  <td align="center" valign="middle"><?=/*($valor*100)."%"*/($evaluacion->preguntas[$cont]->valor*100)."%"?></td>
                  <td align="center" valign="middle"><a href="javascript:agregar('<?=$evaluacion->preguntas[$cont]->idpregunta?>')"><img src="../preguntasFrecuentes/imagenes/modificar.gif" width="16" height="16" border="0"></a></td>
                  <td align="center" valign="middle"><a href="guardarPreguntaEnClase.php?accion=del&idpregunta=<?=$evaluacion->preguntas[$cont]->idpregunta?>&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>"><img src="../preguntasFrecuentes/imagenes/eliminar.gif" width="16" height="16" border="0"></a></td>
                </tr>
			<?
					$i++;
				}//FIN WHILE IMPRIMIR PREGUNTAS
			?>
                <tr class="trInformacion">
                  <td colspan="6" align="center" valign="middle">&nbsp;</td>
                </tr>
                <tr class="trSubTitulo">
                  <td colspan="6" align="center" valign="middle">Procentaje Acumulado de las Preguntas: <?=(round($porcentaje,2)*100)."%"?></td>
                </tr>
              </table>
			  <?
			  }//IF HAY PREGUNTAS
			  else
			  {//ELSE NO HAY PREGUNTAS
			  ?>&nbsp;
			  <table border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
                <tr  align="center" valign="middle">
                  <td class="trListaOscuro">Actualmente esta Evaluaci&oacute;n no tiene  preguntas asignadas.</td>
                </tr>
              </table>	  
			  <?
			  }//ELSE NO HAY PREGUNTAS
			  ?>
				  <!--FIN CELDA DEL PREVIO-->				  </td>
				  <!-- CELDA BANCO PREGUNTAS-->
                  <td class="trListaClaro" align="center" valign="top">&nbsp;
				  <table border="0"  class="tablaPrincipal">
                    <form name="frm_preguntas" method="post" action="llenarQuiz.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>">
                      <tr>
					   
					  
                         <td class="trSubTitulo">Ordenar por tema:</td>
                        <td class="trSubTitulo"><select name="cbo_tema" id="cbo_tema"  onChange="enviar()">
						
                            <option value="*">Todos...</option>
                            <?
						$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema
								WHERE mei_tema.idmateria = '"./*$_GET["idmateria"]*/$evaluacion->idmateria."' AND mei_tema.tipo = 1";
						$resultado = $baseDatos->ConsultarBD($sql);
						while ( list($idtema, $tema) = mysql_fetch_row($resultado) )
						{
							print "<option value='$idtema' ";
							if ( $idtema == $_POST["cbo_tema"] ) print "selected>";
							else print ">";
							print "$tema</option>";
						}
					?>
					
				
					
                      </select>    </td>  
					
					                
                      </tr>
                    </form>
                  </table>
				  <?
				  if ( empty($_POST["cbo_tema"])) $_POST["cbo_tema"]='*';
				  
				  if ( $_POST["cbo_tema"]!='*' )
				  {
				  	$sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta, 
							mei_evatipopregunta.tipo FROM mei_evapreguntas, mei_evatipopregunta WHERE 
							mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND
							mei_evapreguntas.idtema='".$_POST["cbo_tema"]."'";
				  }
				  else
				  {
				  	$sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evatipopregunta.tipo_pregunta, 
							mei_evatipopregunta.tipo FROM mei_evapreguntas, mei_evatipopregunta, mei_tema WHERE 
							mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta AND 
							mei_evapreguntas.idtema = mei_tema.idtema AND mei_tema.idmateria = '".$evaluacion->idmateria."'";
				  }
				  
				  $resultado = $baseDatos->ConsultarBD($sql);
				  
				  if ( mysql_num_rows($resultado)>0 )
				  {// IF HAY PREGUNTAS
				  ?>&nbsp;
				    <table border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
                      <tr class="trSubTitulo">
                        <td width="60%" align="center" valign="middle">Pregunta</td>
                        <td width="30%" align="center" valign="middle">Tipo</td>
                        <td width="10%" align="center" valign="middle">Ver</td>
                      </tr>
					  <?
						$i = 0;
					  	while ( list($idpregunta, $pregunta, $idtipo, $tipo) = mysql_fetch_row($resultado) )
						{
							if ( ($i%2)==0 ) $clase = "trListaOscuro";
							else $clase = "trInformacion";
					  ?>					  
                      <tr class="<?=$clase?>">
                        <td height="20" align="center" valign="middle"><a href="javascript:agregar('<?=$idpregunta?>')" class="link"><?
						if ( $idtipo == 4 )
						{
							list($afirmacion) = explode("[$$$]",stripslashes(base64_decode($pregunta)));
							print substr(strip_tags($afirmacion),0,20)."...";
						}
						else print substr(strip_tags(stripslashes(base64_decode($pregunta))),0,30)."...";?></a></td>						
                        <td height="20" align="center" valign="middle"><?=$tipo?></td>
                        <td height="20" align="center" valign="middle"><a href="javascript:verPregunta('<?=$idpregunta?>')"><img src="imagenes/lupa.gif" width="16" height="16" border="0"></a></td>
                      </tr>
					  <?
					  		$i++;					  	}
					  ?>
                    </table>
					<?
					}//FIN IF HAY PREGUNTAS
					else
					{
					?>&nbsp;
					<table border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
                      <tr align="center" valign="middle">
                        <td class="trSubTitulo">No se han definido preguntas para este tema </td>
                      </tr>
                    </table>
					<?
					}
					?>
				    <!-- FIN CELDA BANCO PREGUNTAS -->				  &nbsp;</td>
                </tr>
                <tr class="trInformacion">
                  <td colspan="2" align="center" valign="top" class="trInformacion"><a href="javascript:vizPrevio()" class="link"><strong>Visualizar Evaluación</strong></a></td>
                </tr>
              </table>
			  <form name="frm_guardarPreguntas" method="post" action="guardarPreguntasQuiz.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>">
			    <table class="tablaMarquesina">
                  <tr align="center" valign="middle">
                    <td width="50%" align="center" valign="middle">
					  <input type="button" name="Submit" value="Guardar Cambios" onClick="javascript:guardar_cambios()">
					</td>
                    <td width="50%" align="center" valign="middle">
					  <input type="button" name="Submit2" value="Volver" 
					  onClick="
					  	<?
							if ($evaluacion->idtipoprevio==1)
							{
						?>
					  		volver('<?=$evaluacion->idprevio?>&idgrupo=<?=$_GET['idgrupo']?>', '<?=$evaluacion->idmateria?>')
						<?
							}
							elseif ($evaluacion->idtipoprevio==2)
							{
						?>
					  		volverQ('<?=$evaluacion->idprevio?>&idgrupo=<?=$_GET['idgrupo']?>', '<?=$evaluacion->idmateria?>')						
						<?
							}
						?>
							">
                    </td>
                  </tr>
                </table>
                <input name="hid_porcentaje" type="hidden" id="hid_porcentaje" value="<?=round($porcentaje,2)?>">
			  </form>
              <p>&nbsp;</p></td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>