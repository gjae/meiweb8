<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once('evaclass.php');
	
	if(comprobarSession())
	{	
		$evaluacion = $_SESSION["evaluacion"]; 		
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
function validarEntero(){ 
      //intento convertir a entero. 
     //si era un entero no le afecta, si no lo era lo intenta convertir 
     valor = parseInt(document.frm.cbo_valor.value) 

      //Compruebo si es un valor numérico       
	if (document.frm.chk_promedio.checked!=true)
	{
	
	  if ( (isNaN(valor)) || (valor<=0) ) { 
            //entonces (no es numero) devuelvo el valor cadena vacia 
            return false  
      }else{ 
            //En caso contrario (Si era un número) devuelvo el valor 
            return true 
      } 
	}else return true
	
}

function enviar()
{
	if (validarEntero())
	{
		document.frm.submit();
		window.close();
	}
	else alert("El campo 'Valor' debe ser un Número mayor a Cero")
	
}
function activar()
{
	if (document.frm.chk_promedio.checked==true)
		document.frm.cbo_valor.disabled = true
	else document.frm.cbo_valor.disabled = false
}

</script>
<title>MEIWEB</title>
</head>
<body onLoad="window.focus()">
<form name="frm" method="post" action="guardarPreguntaEnClase.php?idpregunta=<?=$_GET["idpregunta"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET["esp"]?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET['materia']?>&cbo_ordenar=<?=$_GET['cbo_ordenar']?>" target="llenarEvaluacion">
 

  <table width="220" border="0" cellpadding="0" cellspacing="0">
    <tr class="trTitulo">
      <td colspan="2" align="left" valign="middle">Agregar Pregunta </td>
    </tr><!--
    <tr class="trInformacion">
      <td width="30%" align="left" valign="middle"><strong>Pregunta:</strong></td>
      <td width="70%" align="left" valign="middle"><?=$_GET["idpregunta"]?></td>
    </tr>-->
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Valor:</strong></td>
      <td align="left" valign="middle"><label>
        <input disabled name="cbo_valor" type="text" id="cbo_valor" size="10" maxlength="5">
      %</label></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td colspan="2" align="left" valign="middle"><label>
        <input checked name="chk_promedio" type="checkbox" id="chk_promedio" onClick="javascript:activar()" value="1">
        Promediar Valor de la Pregunta.
      </label></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trSubTitulo" align="center" valign="middle">
      <td colspan="2"><input type="button" name="Submit" value="Agregar" onClick="javascript:enviar()"></td>
    </tr>
  </table>
  
</form>

</body>
</html>
<?
}
else redireccionar('../login/');
?>