<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	
	if(comprobarSession())
	{	
		$baseDatos= new BD();	
		$idprevio = $_GET["idprevio"];
		$idmateria = $_GET["idmateria"];
		$idgrupo = $_GET["idgrupo"];
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
function enviar()
{
	document.frm.submit();
	window.close();
}//
</script>
<title>Adicionar Tiempo</title></head>
<body onLoad="window.focus()">
<form name="frm" method="post" action="guardarTiempoAdicionalEvaluacion.php?idmateria=<?=$idmateria?>&idgrupo=<?=$idgrupo?>&idprevio=<?=$idprevio?>" target="sumarTiempo">
  <table width="180px" border="0" cellpadding="0" cellspacing="0">
    <tr class="trTitulo">
      <td colspan="2" align="center" valign="middle">Adicionar Tiempo</td>
    </tr>
    <tr class="trInformacion">
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td width="20%" align="center" valign="middle">En Minutos: </td>
      <td width="80%" align="center" valign="middle">
        <select name="cbo_tiempo" id="cbo_tiempo">
		<?
			for ($i=1;$i<=60;$i++) 
		   {				
				
				print "<option value='$i'";								
				if ($i == $_GET["tiempo"])
					print " selected>";
				else print ">";				
				print "$i</option>";
				

		   }
	  ?>
        </select>
      </td>
    </tr>
    
    <tr class="trInformacion">
      <td colspan="2">&nbsp;</td>
    </tr>
     <tr class="trInformacion">
					  	<td colspan="2"><div align="center">
                       <a><h5 align="justify"></h5></a>
</div></td>
					  </tr>
    <tr class="trInformacion">
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr class="trSubTitulo">
      <td colspan="2" align="center" valign="middle">
        <input type="button" name="Submit" value="Aceptar" onClick="javascript:enviar()">
      </td>
    </tr>
  </table>
</form>
</body>
</html>
<?
}
else redireccionar('../login/');
?>

