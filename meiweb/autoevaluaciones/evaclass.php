<?

//CLASE PREGUNTA
class preguntaA{//se refiere a una pregunta o una respuesta
	var $codpreg;
	var $idtipo;
	var $txt_preg;
	var $tipo;
	
	function llenar_preg($cod, $txt, $idtipo, $tipo){
		$this->codpreg = $cod;
		$this->idtipo = $idtipo;
		$this->txt_preg = $txt;
		$this->tipo = $tipo;
	}	
}
//FIN CLASS pregunta

//CLASE RESPUESTA
class respuestaA{
	var $codresp;
	var $txt_resp;
	var $valor;
	
	function llenar_resp($cod, $txt, $valor){
		$this->codresp = $cod;
		$this->txt_resp = $txt;
		$this->valor = $valor;
	}
}
//FIN CLASS respuesta

//CLASE PREVIO
class previoA{
	var $idusuario;
	var $pregunta;
	var $resp_bd;
	var $resp_usu;
	
	function previo($id){
		$this->idusuario = $id;		
	}

	function agregar_preg($num, $codpreg, $txtpreg, $idtipo, $tipo){
		$this->pregunta[$num] = new preguntaA;
		$this->pregunta[$num]->llenar_preg($codpreg, $txtpreg, $idtipo, $tipo);			
	}	
	
	function agrega_respbd($num, $i, $codresp, $txtresp, $valor){
		$this->resp_bd[$num][$i] = new respuestaA;
		$this->resp_bd[$num][$i]->llenar_resp($codresp, $txtresp, $valor);
	}
	
	function agrega_respusu($num, $i, $codresp, $txtresp){
		$this->resp_usu[$num][$i] = new respuesta;
		$this->resp_usu[$num][$i]->llenar_resp($codresp, $txtrespm);
	}
}
//FIN CLASS previo

?>