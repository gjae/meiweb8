<?
   include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
   include_once ('../librerias/estandar.lib.php');
   include_once ('../librerias/vistas.lib.php');
 
/*  Nombre  : vertaller.php
	Autor   : Claudia
	Fecha   : 09/11/2005
	Detalle : Autoevaluaciones
	Versión :  
*/ 

if(comprobarSession())
	{//if comprobar sesion
?>	
<head>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MEIWEB</title>
<link href="../temas/tema1/estilo1024x768.css" rel="stylesheet" type="text/css">
</head>
<body>
<?
if ($_GET["estado"] == 2)
{
?>
<form name="form1" method="post" action="">
  <p>&nbsp;</p>
  <table class="tablaGeneral" width="100%" border="0">
  <tr class="trIntermedio">
    <td width="55%"><table class="tablaGeneral" width="100%" border="0">
      <tr class="trIntermedio" align="center">
        <td colspan="2"><strong>UNIVERSIDAD INDUSTRIAL DE SANTANDER </strong></td>
      </tr>
      <tr class="trClaro">
        <td colspan="2" align="center"><p>AUTOEVALUACION No. &quot;algo&quot;</p></td>
      </tr>
      <tr class="trIntermedio">
        <td width="45%">NOMBRE DEL ALUMNO: </td>
        <td width="55%"><?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?></td>
      </tr>
      <tr class="trClaro">
        <td>C&Oacute;DIGO:</td>
        <td><?= $_SESSION['idusuario']?></td>
      </tr>
      <tr class="trIntermedio">
        <td>MATERIA:</td>
        <td><?
	  	list($codmateria, $nommateria) = explode("-",$_POST["cbo_materia"]);
		print $nommateria;
	  ?>
          <input name="hid_materia" type="hidden" id="hid_materia" value="<? print "$codmateria-$nommateria"?>"></td>
      </tr>
      <tr class="trClaro">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <?
	$sql = "SELECT mei_evapreguntas.pregunta, mei_evapreguntas.idpregunta
		    FROM mei_evapreguntas WHERE mei_evapreguntas.autoevaluacion = 1";
	$resul = $baseDatos->ConsultarBD($sql);
	
	$i=0;
	while (list ($pregunta,$idpregunta) = mysql_fetch_row ($resul))
	{ // Inicio While 1
	$i++;
	?>
      <tr class="trClaro">
        <td colspan="2"><? print $i.". ".$pregunta?></td>
      </tr>
      <?
	$sql = "SELECT distinct(mei_evarespuestas.respuesta), mei_evarespuestas.idpregunta
		    FROM mei_evapreguntas, mei_evarespuestas WHERE mei_evapreguntas.autoevaluacion = 1 AND mei_evarespuestas.idpregunta = '$idpregunta'";
	$res = $baseDatos->ConsultarBD($sql);
	
		while (list($respuesta) = mysql_fetch_row ($res))
		{//while respuesta
		?>
      <tr class="trClaro">
        <td colspan="2"><? 
				 print $respuesta?></td>
      </tr>
      <?
		}//fin while respuesta
	}//Fin While 1
	?>
    </table></td>
  </tr>
  </table>
</form>
<? 
}
else
{
?>
<form name="frm2" method="post" action="<?= $PHP_SELF ?>?estado=2">
  <table class="tablaGeneral" width="100%" border="0">
    <tr class="trIntermedio" align="center">
      <td colspan="2"><strong>UNIVERSIDAD INDUSTRIAL DE SANTANDER </strong></td>
    </tr>
    <tr class="trClaro">
      <td colspan="2" align="center"><p>AUTOEVALUACION No. &quot;algo&quot;</p>      </td>
    </tr>
    <tr class="trIntermedio">
      <td>NOMBRE DEL ALUMNO: </td>
      <td><?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?></td>
    </tr>
    <tr class="trClaro">
      <td>C&Oacute;DIGO:</td>
      <td><?= $_SESSION['idusuario']?></td>
    </tr>
    <tr class="trIntermedio">
      <td>MATERIA:</td>
      <td>
	 <select class="link" name="cbo_materia" onChange="javascript:document.frm2.submit()">
          <option value="Materia">--Seleccione Materia--</option>
          <?
	  		$sql = "SELECT mei_materia.idmateria, mei_materia.nombre FROM mei_materia, mei_grupo,
					mei_relacionug WHERE mei_materia.idmateria = mei_grupo.idmateria AND
					mei_grupo.idgrupo = mei_relacionug.idgrupo 	AND 
					mei_relacionug.idusuario =".$_SESSION['idusuario'];
				
			$resultado = $baseDatos->ConsultarBD($sql);
			
			while ( list($idmateria, $nommateria) = mysql_fetch_row($resultado) )
			{
				print "<option value='$idmateria-$nommateria'>$nommateria</option>\n";	
			}
			mysql_free_result($resultado);
		?>
        </select></td>
    </tr>
    <tr class="trClaro">
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<?
} //Fin else
?>
</body>
<?
} //Fin comprobar sesion
?>
</html>
