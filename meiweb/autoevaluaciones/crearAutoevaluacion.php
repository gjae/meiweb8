<?
   	include_once('../evaluaciones/evaclass.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	 include_once ('../menu1/Menu1.class.php');
	
	 
	/*  Nombre  : crearevaluacion.php
		Autor   : Claudia
		Fecha   : 24/11/2005
		Detalle : Creación de preguntas.
		Versión :  
	*/ 
	
	// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , [Barra de herramientas] , [Texto inicial] ) ;
	$editor=new FCKeditor('edt_evaluaciones' , '100%' , '100%' , 'barraBasica' , '' ) ;
	
	if(comprobarSession())
	{
	$baseDatos= new BD();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
</head>
<script>
	function validarEntero(){ 
		  //intento convertir a entero. 
		 //si era un entero no le afecta, si no lo era lo intenta convertir 
		 valor = parseInt(document.frm_crearEva.txt_npreguntas.value) 
	
		  //Compruebo si es un valor numérico 
		  if (isNaN(valor)) { 
				//entonces (no es numero) devuelvo el valor cadena vacia 
				return false  
		  }else{ 
				//En caso contrario (Si era un número) devuelvo el valor 
				return true 
		  } 
	}

		function enviar()
		{
			var contBandera=0;	
			for(i=0;i<document.frm_crearEva.elements.length;i++)
			{
				if(document.frm_crearEva.elements[i].checked==true && document.frm_crearEva.elements[i].id=="temas")
				contBandera++;
			}
			
			if(contBandera == 0)
			{
				alert('Debe seleccionar por lo menos un tema');
				return false
			}
			else
			{
				if ( validarEntero() )
				{
					return true
				}				
				else
				{
					alert("El campo 'Número de Preguntas' debe ser un número")
					return false
				}
			}
		}
		function cancelar(idmateria,materia)
		{
			document.frm_crearEva.action = "../evaluaciones/verEvaluacion.php?idmateria="+idmateria+"&materia="+materia;
			document.frm_crearEva.submit();
		}

</script>
<body>

<table height="1198" class="tablaPrincipal">

			<tr valign="top">
            <td width="6" class="tablaEspacio">&nbsp;</td>
            <td width="6" class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td width="6" class="tablaEspacio">&nbsp;</td>
            <td>
            
					<table class="tablaMarquesina" >
						<tr>
                        <?
                           if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==3)
		{
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		}
		else
		{
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		}
                           		$resultado = $baseDatos->ConsultarBD($sql);
		list($materia) = mysql_fetch_row($resultado); ?>
        
			  <td width="0"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($materia))?></a><a> -> </a><a href="../evaluaciones/verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a><a> -> Crear Autoevaluaci&oacute;n</a></td>
			</tr>
		  </table>&nbsp;
<form action="responderAutoevaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" method="post" name="frm_crearEva" id="frm3">
		  <?
		//if ( $_GET["estado"]==2 )
		if ( !empty($_GET["idmateria"]) )
		{//if estado = 3
			$sql = "SELECT DISTINCT(mei_tema.idtema), mei_tema.titulo FROM mei_tema,mei_evapreguntas WHERE 
					mei_tema.idmateria=".$_GET["idmateria"]." AND mei_tema.tipo=1 AND mei_tema.idtema=mei_evapreguntas.idtema AND mei_evapreguntas.autoevaluacion=1 AND mei_evapreguntas.tipo_pregunta!=6";
			$resultado = $baseDatos->ConsultarBD($sql);         
			
			if (mysql_num_rows($resultado))
			{//HAY TEMAS
		?>
			<table class="tablaGeneral" width="440" border="0" align="center">
			  <!-- TEMAS A EVALUAR -->
			  <tr class="trTitulo">
			    <td height="25" colspan="2" 
				align="left" valign="middle">Autoevaluaci&oacute;n
		        <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_GET["idmateria"]?>"></td>
		      <tr class="trInformacion">
			    <td width="20%" height="25" align="left" valign="middle" class="trInformacion"><strong>Tiempo:</strong></td>
	            <td class="trInformacion" width="80%" 
				align="left" valign="top">
				<select name="cbo_tiempo" id="cbo_tiempo">				
				<?
					for($i=5;$i<=40;$i+=5)
					{
						print "<option value='$i'>$i Minutos</option>";
					}
				?>
                </select></td>


                 <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_GET["idmateria"]?>"></td>
		      <tr class="trInformacion">
			    <td width="20%" height="25" align="left" valign="middle" class="trInformacion"><strong>N&uacute;mero de Preguntas: </strong></td>
	            <td class="trInformacion" width="80%" 
				align="left" valign="top">
				<select name="txt_npreguntas" id="txt_npreguntas">				
				<?
					for($i=5;$i<=20;$i+=1)
					{
						print "<option value='$i'>$i Preguntas</option>";
					}
				?>
                </select></td>



	          <tr  class="trInformacion">
			<td width="20%" height="25" rowspan="<? print mysql_num_rows($resultado)?>" 
				align="left" valign="middle" class="trInformacion"><strong>Tema(s) a Evaluar : 
	          <input id="temas" name="hid_materia" type="hidden" value="<?=$_GET["idmateria"] //"$codmateria-$nommateria";?>">
              <input type="hidden" name="hid_tiempo" id="hid_tiempo">
			</strong></td>				
		    <td class="trInformacion" align="left" valign="middle">
				<table class="tablaPrincipal" width="100%"  border="0">
			<?		
				$cont=0;
				while ( list($codtema, $nomtema) = mysql_fetch_row($resultado) )		
				{
					if ( ($cont%2)==0 )  $color='trListaOscuro';
					else $color='trInformacion';
			?>
				  <tr class="<?=$color?>">				  
					<td width="3%"><input id='temas' type='checkbox' name='chk_tema[]' value='<?=$codtema-$nomtema?>'></td>
					<td width="97%"><?=$nomtema?></td>
				  </tr>
			<?
					$cont++;
				}
				mysql_free_result($resultado);

			?>  
            </table>			</td>
		    <!-- FIN TEMAS A EVALUAR -->
		  </table>
			
			  <table class="tablaGeneral" width="200" border="0" align="center">
				<tr class="trSubTitulo" align="center" valign="middle">
				  <td><input type="submit" name="Submit" value="Responder" onClick="return enviar()"></td>
				  <td><input type="submit" name="Submit2" value="Cancelar" onClick="javascript:cancelar('<?=$_GET["idmateria"]?>','<?=$_GET["materia"]?>')"></td>
				</tr>
		  </table>
		<?
			}//FIN HAY TEMAS
			else
			{
		?>
		<table class="tablaGeneral">
          <tr class="trInformacion">
            <td align="center" valign="middle">&nbsp;</td>
          </tr>
          <tr class="trInformacion">
            <td align="center" valign="middle">No hay Temas Definidos </td>
          </tr>
          <tr class="trInformacion">
            <td align="center" valign="middle">&nbsp;</td>
          </tr>
        </table>&nbsp;
		<table class="tablaGeneral" width="200" border="0" align="center">
          <tr class="trSubTitulo" align="center" valign="middle">
            <td><input type="submit" name="Submit22" value="Cancelar" onClick="javascript:cancelar('<?=$_GET["idmateria"]?>','<?=$_GET["materia"]?>')"></td>
          </tr>
        </table>		<?
			}
		}
		?>	
	  </form>
		</td>
           <td width="6" class="tablaEspacio">&nbsp;</td>
		<td width="6" class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td width="10" class="tablaEspacioDerecho">&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?
}
	else
	{
		redireccionar('../login/');					
	}
?>			