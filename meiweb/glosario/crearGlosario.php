<?
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');

	// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , [Barra de herramientas] , [Texto inicial] ) ;
	$editorP=new FCKeditor('edt_palabra' , '100%' , '100%' , 'barraBasica' , '' ) ;
	$editorR=new FCKeditor('edt_significado' , '100%' , '100%' , 'barraBasica' , '' ) ;
	if(comprobarSession())
	{
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{

?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<script>

			function functionMateria()
			{
				document.frm_crearGlosario.submit();
			}
			function validar(idmateria,materia)
			{
				if(document.frm_crearGlos.txt_palabra.value!="")
				{
				document.frm_crearGlos.action="guardarPalabra.php?idmateria="+idmateria+"&materia="+materia;
				document.frm_crearGlos.submit();
				}
				else
				alert ("Debe llenar todos los campos");
			}

			function cancelar(idmateria,materia)
			{
				document.frm_crearGlos.action="index.php?idmateria="+idmateria+"&materia="+materia;
				document.frm_crearGlos.submit();
			}

			function editar(idmateria)
			{
				document.frm_crearGlosario.action="crearGlosario.php?idmateria="+idmateria;
				document.frm_crearGlosario.submit();
			}
			function confirmar(idmateria)
			{
				if(document.frm_crearGlosario.hid_palabra.value!="" && document.frm_crearGlosario.hid_significado.value!="")
				{
					document.frm_crearGlosario.action="guardarPalabra.php?idmateria="+idmateria;
					document.frm_crearGlosario.submit();
				}
				else
				{
					alert ("debe escribir un mensaje");
				}
			}
		</script>

		<table height="100%" class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
			 	 <td valign="top">
					<table class="tablaGeneral" >
						<tr class="trSubTitulo">
<?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql= "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado); ?>
							<td><a href="../scripts/" class="link">Inicio</a><a> : </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nombre))?></a><a> : </a><a href="index.php?idmateria=<?= $_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link">Ver Glosario </a> : Crear Palabra</td>
						</tr>
					</table><br>
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td><img src="imagenes/agregarIntroduccion.gif" width="22" height="20" border="0">Crear Palabra</td>
						</tr>
					</table>

					<form name="frm_crearGlos" method="post" action="<?= $PHP_SELF ?>?estado=1&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>">
					<table class="tablaGeneral" >

						<tr class="trInformacion">
							<td width="20%"><strong>Autor:
							  <input name="hid_idmateria" type="hidden" id="hid_idmateria" value="<?= $_GET['idmateria']?>">
							</strong></td>
							<td width="80%"><strong>
						    <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?>
							</strong></td>
						</tr>

						<tr class="trInformacion">
							<td><strong>Palabra:</strong></td>
						    <td><input name="txt_palabra" type="text" size="50"></td>
						</tr>


						<tr class="trInformacion">
							<td colspan="2"><strong>Significado:</strong></td>
						</tr>
						<tr class="trListaClaro">
							<td colspan="2">
							<?
								if(empty($_POST['hid_significado']))
									$editorR->Value='';
								else
									$editorR->Value=stripslashes(base64_decode($_POST['hid_significado']));
									$editorR->crearEditor();
							?>							</td>
						</tr>
					</table>
				    <table width="104" class="tablaGeneral">
                      <tr align="center" valign="middle" class="trSubTitulo">
                        <td width="50%"><input type="button" name="btn_crearGlosario" value="Aceptar" onClick="javascript:validar('<?= $_GET['idmateria']?>','<?=$_GET["materia"]?>')"></td>
                        <td width="50%"><input type="button" name="Submit" value="Cancelar" onClick="javascript:cancelar('<?=$_GET['idmateria']?>','<?=$_GET["materia"]?>')"></td>
                      </tr>
                    </table>
					</form>
			    <?
				//}
				?>
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
		</body>
		</html>
<?
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
?>

