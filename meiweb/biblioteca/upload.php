<?php
include_once ('../librerias/estandar.lib.php');
include_once('../baseDatos/BD.class.php');  
include_once ('../librerias/vistas.lib.php');
include_once("../editor/fckeditor.php") ;
include_once('../calendario/FrmCalendario.class.php');
include_once ('../menu1/Menu1.class.php');
	
$baseDatos=new BD();

if(comprobarSession())
{//IF COMPROBARSESSION
	
	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==7 || $_SESSION['idtipousuario']==5)
	{//IF IDTIPOUSUARIO
    
        // A list of permitted file extensions
        $allowed = array('png', 'jpg', 'gif','zip','pdf','doc','docx','avi','ppt','pptx','flv','mp3','xls','xlsx','txt','mp4','wav','mpeg','rar');
        
		$materia=$_POST ['hid_materia'];
		$tema=$_POST['tema'];
		list($codigoTema,$tema)=explode('-',$_POST['cbo_tema']);			
		list($codigoAplicacion,$nombreAplicacion)=explode('-',$_POST['cbo_aplicacion']);
		$tema2=$_POST['tema'];
		$materia2=$_POST['hidmateria'];			
			
		
		$descripcionArchivo=base64_encode($_POST['edt_biblioteca']);  
        
		$tipoArchivo = $_FILES['upl']['type']; 
		$tamanoArchivo = $_FILES['upl']['size'];               
        
        if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){
        
        	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
        
        	if(!in_array(strtolower($extension), $allowed)){
        		echo '{"status":"Error"}';
        		exit;
        	}
            
        	$sql="SELECT MAX(mei_biblioteca.idarchivo) FROM mei_biblioteca";
        	$consulta=$baseDatos->ConsultarBD($sql);
        	list($maxId)=mysql_fetch_array($consulta);
        			
        	if( empty($maxId) )
        		$maxId = 1;
        	else $maxId++;
                        
            $archivo = $_FILES['upl']['name']; 
    		$nombreArchivo=$archivo;
    		$nombreArchivo=corregirCaracteresURL($nombreArchivo);      
            $titulo=$nombreArchivo;      
        	if(move_uploaded_file($_FILES['upl']['tmp_name'], '../../datosMEIWEB/archivosBiblioteca/'.$nombreArchivo)){
    			$sql1=" SELECT mei_tipoarchivo.idtipoarchivo FROM mei_tipoarchivo 
    					WHERE mei_tipoarchivo.tipoarchivo='$tipoArchivo'";
    					
    			$codigotipoarchivo=$baseDatos->ConsultarBD($sql1);
    			list($codigotipoarchivo)=mysql_fetch_array($codigotipoarchivo);
    			
    			if (empty($codigotipoarchivo)) $codigotipoarchivo=8;
    				
    			$sql2=" INSERT INTO mei_biblioteca (idarchivo,idtipoarchivo,idtipoaplicacion,
    					idtema,idusuario,titulo,archivo,tamano,fechacarga,descripcion) 
    					VALUES ('$maxId', '$codigotipoarchivo','$codigoAplicacion', '$codigoTema', 
    					'".$_SESSION['idusuario']."','$titulo','$nombreArchivo','$tamanoArchivo',
    					'".date("Y-n-d")."','$descripcionArchivo') ";
    					
    			$resultado=$baseDatos->ConsultarBD($sql2); 
                       		
                echo '{"status":"Exito"}';
        		exit;
        	}
        }
        
        echo '{"status":"Error"}';
        exit;
    }//FIN IF IDTIPOUSUARIO
    	else redireccionar('../login/');        
}//IF COMPROBARSESSION
else redireccionar('../login/');