<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/validar.lib.php');	
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');

	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
</head>

<body>
<table align="center" class="tablaEncabezado">
 	<tr valign="top">
 	  <td width="237" >&nbsp;</td>
 	  <td width="365" >&nbsp;</td>
 	  <td width="150" >&nbsp;</td>
 	  <td width="194" >&nbsp;</td>
 	  <td width="10" >&nbsp;
	  </td>
  	</tr>
 	<tr valign="top">
 	  <td >&nbsp;</td>
 	  <td >&nbsp;</td>
 	  <td >&nbsp;</td>
 	  <td >&nbsp;</td>
 	  <td >&nbsp;</td>
  </tr>
 	<tr valign="top">
 	  <td >&nbsp;</td>
 	  <td >&nbsp;</td>
 	  <td rowspan="2" >
		  <table align="right" class="tablaOpciones">
				<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
				</tr>
				<tr>
								<td><a href="../scripts/indexAdministrador.php" target="_parent" class="letraEncabezado" title="Ir al Inicio"><b>Inicio</b></a></td>
								<td><a href="../ayuda/" target="self" class="letraEncabezado" title="Ir a Ayuda"><b>Ayuda</b></a></td>
								<td><a href="../login/salirSistema.php" target="_parent" class="letraEncabezado" title="Salir de MeiWeb"><b>Salir</b></a></td>
        		</tr>
      </table></td>
 	  <td ><div align="right" class="letraEncabezado"><b>ADMINISTRADOR</b></div></td>
 	  <td >&nbsp;</td>
  </tr>
 	<tr valign="top">
 	  <td >&nbsp;</td>
 	  <td height="20" >
					  <?
						if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
						{
						?>
					    <div align="center"><a href="../scripts/" class="letraEncabezado" title="Cambio de sesion, ingresar a MeiWeb como Profesor"><b>Ingresar como Profesor</b></a>					
						        <?
						}
						?>
	      
	                    </div></td>
 	  <td ><div align="right" class="letraFecha"><?=mostrarFecha(date('Y-m-j'),date('w'))?></div>

      </div></td>
 	  <td >&nbsp;</td>
  </tr>
 	<tr valign="top">
 	  <td >&nbsp;</td>
 	  <td >&nbsp;</td>
 	  <td >&nbsp;</td>
 	  <td >&nbsp;</td>
 	  <td >&nbsp;</td>
  </tr>
	
</table>

</body>
</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
	
?>
