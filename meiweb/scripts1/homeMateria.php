<?PHP
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');
	
	if(comprobarSession())
	{
		$idmateria=$_GET["idmateria"];
		$_SESSION['sesIdMateria'] = $_GET["idmateria"];
		$materia=$_GET['materia'];
		
		?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo" valign="top"><?PHP menu($_SESSION['idtipousuario']); ?></td>
        	<td class="tablaEspacio">&nbsp;</td> 
  			<td>  
                  <table class="tablaMarquesina">
                    <tr>
            <?PHP	 				
                    if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
                        $sql123 = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
                    else
                        $sql123 = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
                    $resul = $baseDatos->ConsultarBD($sql123);
                    list($nom) = mysql_fetch_row($resul); ?>	  
                      <td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link"><?PHP echo ucwords(strtolower($nom)); ?></a></td>
                    </tr>
                  </table><br>
<?PHP
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
		{
			$sql = "SELECT mei_virmateria.nombre,mei_virmateria.codigo,mei_virmateria.formato, mei_virmateria.numeromod, mei_virmateria.fechainicio,
				mei_virmateria.fechafin, mei_virmateria.requisitos, mei_virmateria.introduccion, mei_virmateria.ofertar FROM mei_virmateria 
				WHERE mei_virmateria.idvirmateria = '".$idmateria."'";
			$resultado = $baseDatos->ConsultarBD($sql);
			list($nombre,$codigo,$formato,$modulos,$fecha_ini,$fecha_fin,$requisitos,$intro,$ofertar)=mysql_fetch_array($resultado);		
		}
		else
		{
			$sql = "SELECT mei_materia.nombre, mei_materia.codigo, mei_materia.creditos, mei_materia.fechacreacion,
					mei_materia.introduccion FROM mei_materia WHERE mei_materia.idmateria = '".$idmateria."'";
			$resultado = $baseDatos->ConsultarBD($sql);
			list($nombre, $codigo, $creditos, $fecha_c, $intro) = mysql_fetch_row($resultado);	
		}
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
		{
?>
				<table class="tablaGeneral">
					<tr class="trTitulo">
        				<td colspan="5">Descripci&oacute;n del Curso </td>
					</tr>
					<tr class="trInformacion">
                    	<td colspan="5">&nbsp;</td>
					</tr>
                  <tr class="trInformacion">
                    <td width="20%"><b>Nombre del Curso:</b></td>
                    <td width="30%"><?PHP echo $nombre; ?></td>
                    <td width="20%"><b>C&oacute;digo:</b></td>
                    <td width="30%"><?PHP echo $codigo; ?></td>
                  </tr>
                  <tr class="trInformacion">
                    <td><b>Formato:</b></td>
                    <td><?PHP echo $formato; ?></td>
                    <td><b>N&deg; de modulos:</b></td>
                    <td><?PHP echo $modulos; ?></td>
                  </tr>
                  <tr class="trInformacion">
                    <td><b>Fecha Inicio:</b>&nbsp;</td>
                    <td><?PHP echo $fecha_ini; ?></td>
                    <td><b>Fecha Finalizaci&oacute;n:</b>&nbsp;</td>
                    <td><?PHP echo $fecha_fin; ?></td>
                  </tr>
					<tr class="trInformacion">
                    	<td colspan="5">&nbsp;</td>
					</tr>                  
                  <tr class="trInformacion">
                    <td align="center" colspan="5"><b>Requisitos</b></td>
                  </tr>
                  <tr class="trDescripcion">
                    <td colspan="5" align="center"><?PHP echo  $requisitos; ?></td>
                  </tr>
					<tr class="trInformacion">
                    	<td colspan="5">&nbsp;</td>
					</tr>                  
                  <tr class="trInformacion">
                    <td align="center" colspan="5"><b>Introducci&oacute;n</b></td>
                  </tr>
                  <tr class="trDescripcion">
                    <td colspan="5" align="center"><?PHP echo  $intro; ?></td>
                  </tr>                  
              </table>
<?PHP		}
		else
		{
?>
				<table class="tablaGeneral">
                  <tr class="trSubTitulo">
                    <td colspan="2">Descripci&oacute;n de la Materia </td>
                  </tr>
                  <tr class="trInformacion">
                    <td height="10" colspan="2"></td>
                  </tr>
                  <tr class="trInformacion">
                    <td width="20%" height="25"><strong>Nombre de la Materia: </strong></td>
                    <td width="80%" height="25"><?PHP echo $nombre; ?></td>
                  </tr>
                  <tr class="trInformacion">
                    <td height="25"><strong>C&oacute;digo</strong></td>
                    <td height="25"><?PHP echo $codigo; ?></td>
                  </tr>
                  <tr class="trInformacion">
                    <td height="25"><strong>Cr&eacute;ditos:</strong></td>
                    <td height="25"><?PHP echo $creditos; ?></td>
                  </tr>
                  <tr class="trInformacion">
                    <td height="25"><strong>Fecha Creaci&oacute;n:</strong></td>
                    <td height="25"><?PHP echo mostrarFecha($fecha_c,FALSE); ?></td>
                  </tr>
				  <?PHP
				  	if ( !empty($intro) )
					{
				  ?>
                  <tr class="trDescripcion">
                    <td height="25%" colspan="2" align="center" valign="middle"><strong>Introducci&oacute;n</strong></td>
                  </tr>
                  <tr class="trDescripcion">
                    <td colspan="2"><?PHP echo  stripslashes(base64_decode($intro)); ?></td>
                  </tr>
				  <?PHP
				  	}
				  ?>
                </table>
				<?PHP
		}
				if ($_SESSION['idtipousuario']==2) {
				?>	
					 <table class="tablaGeneral">
                      <tr class="trSubTitulo">
                        <td width="867" class="trSubTitulo"><img src="../imagenes/usuarios.gif" width="16" height="16">Direcciones IPs </td>
                      </tr>
                      <tr class="trListaClaro">
                        <td>En este componente , se muestra la lista de Direcciones IP de los usuarios conectados. <a href="listarIpsProfes.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET["materia"]; ?>">Ver Tabla</a>. </td>
                      </tr>
                    </table>
				<?PHP } ?>
			  <p>&nbsp;</p></td>
			<td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho" valign="top"><?PHP echo menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>             
			</tr>
		</table>
		</body>
		</html>
<?PHP 
	}
	else
		redireccionar('../login/');					
?>