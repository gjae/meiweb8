<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/validar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once('../cartelera/Cartelera.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../calendario/Calendario.class.php');	
	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		$bandeja=new Calendario();
		?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
		</head>
		<body>
		<table class="tablaPrincipal">
			<tr valign="top">
				<td class="tablaEspacio">&nbsp;</td>            
				<td class="tablaIzquierdo"><? menu(1)?></td>
				<td class="tablaEspacio">&nbsp;</td>                            
			  <td>
					<table class="tablaGeneral">
						<tr class="trTitulo"> 
							<td class="trTitulo"><img src="../imagenes/configuracion.gif">Configuraci&oacute;n MEIWEB</td>
						</tr>
						<tr class="trInformacion">
						  <td>La Configuraci&oacute;n de la Plataforma MEIWEB Versi&oacute;n 3.0,  esta conformada por los siguientes componentes:</td>
					  </tr>
						<tr class="trInformacion">
						  <td>&nbsp;</td>
					  </tr>
					  
						<tr class="trSubTitulo">
						  <td class="trSubTitulo"><img src="../imagenes/modulos.gif" width="16" height="16">Configuraci&oacute;n M&oacute;dulos</td>
					  </tr>
						<tr class="trListaClaro">
						  <td>En este componente el Administrador tiene la opci&oacute;n de activar y/o desactivar m&oacute;dulos del sistema y a su vez cambiar los par&aacute;metros de configuraci&oacute;n de la plataforma.</td>
					  </tr>
					</table><br>
					<table class="tablaGeneral">
					  
						<tr class="trSubTitulo">
						  <td class="trSubTitulo"><img src="../imagenes/usuarios.gif" width="16" height="16">Configuraci&oacute;n Usuarios </td>
					  </tr>
						<tr class="trListaClaro">
						  <td>En este componente , el Administrador tiene la opci&oacute;n de crear profesores y de editar los diferentes usuarios de la plataforma. </td>
					  </tr>
					</table><br>
					<table class="tablaGeneral">
						<tr class="trSubTitulo">
						  <td class="trSubTitulo"><img src="../imagenes/materias.gif" width="18" height="18">Configuraci&oacute;n Materias</td>
					  </tr>
						<tr class="trListaClaro">
						  <td>En este componente el Administrador tiene la opci&oacute;n de crear, modificar y eliminar materias y grupos en el sistema.</td>
					  </tr>
					</table><br>
					<table class="tablaGeneral">
					  
						<tr class="trSubTitulo">
						  <td class="trSubTitulo"><img src="../imagenes/seguridad.gif" width="16" height="16">Copias de Seguridad</td>
					  </tr>
						<tr class="trListaClaro"> 
							<td>&nbsp;En este componente el administrador tiene la opci&oacute;n de generar copias de seguridad manuales, adem&aacute;s de cambiar la periodicidad de las copias autom&aacute;ticas.
						    </td>
						</tr>
						
					</table>
				    <br>    
				    <table class="tablaGeneral">
                      <tr class="trSubTitulo">
                        <td width="867" class="trSubTitulo"><img src="../imagenes/usuarios.gif" width="16" height="16">Direcciones IPs </td>
                      </tr>
                      <tr class="trListaClaro">
                        <td>En este componente , se muestra la lista de Direcciones IP de los usuarios conectados. <a href="listarIps.php">Ver Tabla</a>. </td>
                      </tr>
                    </table>
			    <p>&nbsp;</p>
			    <p>&nbsp;</p>
				</td>
                <td class="tablaDerecho">&nbsp; </td>
				<td class="tablaEspacio">&nbsp;</td>               
				<td class="tablaEspacioDerecho">&nbsp;</td>                                                
			</tr>
		</table>
		</body>
		</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
	
?>
