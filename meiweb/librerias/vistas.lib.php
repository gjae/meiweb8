<?PHP   
/*
	Nombre  : vistas.lib.php
	Autor   : GrupoMEI
	Fecha   : 10/10/2005
	Detalle : Funciones estandar
	Versión : 2.0

	Lista de funciones :

		verForo()
		verCorreo()

	*/
	include_once ('../baseDatos/BD.class.php');

	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');

	//verForo()
	function verForo($codigo) //trae el idusuario
	{
		
			mysql_query("SET NAMES 'utf8'");
			
		$baseDatos= new BD();
		$sql1="SELECT mei_foro.idforo, mei_foro.fechaactivacion, mei_foro.fechacaducidad,mei_foro.horaactivacion, mei_foro.horacaducidad FROM mei_foro";
		$resultado1=$baseDatos->ConsultarBD($sql1);

		while (list($codigoForo,$fechaActivacion, $fechaCaducidad,$horaActivacion,$horaCaducidad)=mysql_fetch_array($resultado1))
		{
			list($a,$m,$d)=explode('-',date("Y-m-d"));
				$fechaActual= $a.$m.$d;
			list($a,$m,$d)=explode('-',$fechaActivacion);
				$fechaForo= $a.$m.$d;
			list($a,$m,$d)=explode('-',$fechaCaducidad);
				$fechaCadu= $a.$m.$d;
			list($h,$m,$s)=explode(':',date("H:i:s"));
				$horaActual= $h.$m.$s;
			list($h,$m,$s)=explode(':',$horaActivacion);
				$horaForo= $h.$m.$s;
			list($h,$m,$s)=explode(':',$horaCaducidad);
				$horaCadu= $h.$m.$s;

			if($fechaForo>$fechaActual)
				$estado='0';
			else if($fechaForo==$fechaActual)
			{
				if($horaForo>$horaActual)
					$estado='0';
				else
					$estado='1';
			}
			else if($fechaActual>$fechaForo)
				$estado='1';
				
			if($fechaActual>$fechaCadu)
				$estado='0';
			else if($fechaActual==$fechaCadu)
			{
				if($horaActual>$horaCadu)
					$estado='0';
				else
					$estado='1';
			}

			$sql2="UPDATE mei_foro SET mei_foro.estado='$estado' WHERE mei_foro.idforo=".$codigoForo;
			$resultado2=$baseDatos->ConsultarBD($sql2);
		}//fin de actualizacion

		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT idvirgrupo FROM mei_relusuvirgru WHERE idusuario='".$_SESSION['idusuario']."'";
		else
			$sql = "SELECT idgrupo FROM mei_relusugru WHERE idusuario='".$_SESSION['idusuario']."'";
			
		$grupo=$baseDatos->ConsultarBD($sql);
		unset($listaForos);
		$contForos=0;
		while (list($idgrupo)= mysql_fetch_array($grupo))
		{
			if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
				{
					$sql1="SELECT mei_foro.idforo,mei_foro.idmateria,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,
						mei_foro.fechacreacion,mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.calificable
						FROM mei_foro WHERE mei_foro.estado=1 AND mei_foro.idusuario='".$_SESSION['idusuario']."' AND mei_foro.idgrupo='".$idgrupo."' ORDER BY mei_foro.idforo DESC";
				}
				else if($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
				{
					$sql1="SELECT mei_foro.idforo,mei_foro.idmateria,mei_foro.mensaje,mei_foro.estado,mei_foro.idusuario,
						mei_foro.fechacreacion,mei_foro.fechaactivacion,mei_foro.fechacaducidad,mei_foro.calificable
						FROM mei_foro WHERE mei_foro.estado=1 AND mei_foro.idgrupo='".$idgrupo."' ORDER BY mei_foro.idforo DESC";
				
				}
				$consulta1=$baseDatos->ConsultarBD($sql1);
				//$consulta2=$baseDatos->ConsultarBD($sql2);
				$numeroForos=mysql_num_rows($consulta1);/*+mysql_num_rows($consulta2)*/
				while (list($codigoMensaje,$idmateria,$mensaje,$estado,$codigoAutor,$fecha,$facheAct,$fechaCad,$calificable) = mysql_fetch_array($consulta1))
				{
					$listaForos[$contForos]=$codigoMensaje."[$$$]".$idmateria."[$$$]".$mensaje."[$$$]".$estado."[$$$]".$codigoAutor."[$$$]".$fecha."[$$$]".$fechaAct."[$$$]".$fechaCad."[$$$]".$calificable;
					$contForos++;
				}
		}

		?>
				<table class="tablaCentral">
					<tr class="trTitulo">
		<?PHP   
				if (isset($listaForos) && sizeof($listaForos)==0)
				{
		?>
						<td><img src="../imagenes/foro.gif" alt="Foro">Foro</td>
		<?PHP   
				}
				else
				{
		?>
						<td><img src="../imagenes/foro.gif" alt="Foro"><a href="../foro/index.php?idmateria=<?PHP   echo  $idmateria; ?>" class="link"> Foro </a> </td>
		<?PHP   
				}
		?>
						<td><div align="right">Hay <?PHP   echo  @sizeof($listaForos); ?> Foros  Activos</div></td>
					</tr>
		<?PHP   
		
				if(isset($listaForos) && sizeof($listaForos)>0)
				{
					$contMensajes=0;
		?>
					<tr class="trSubTitulo">
		<?PHP   
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
		?>
						<td width="250" class="trSubTitulo"><div align="center">Curso</div></td>
		<?PHP   
					}
					else if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==7)
					{
		?>    
						<td width="250" class="trSubTitulo"><div align="center">Materia</div></td>
		<?PHP   
					}
		?>    
						<td width="400" class="trSubTitulo"><div align="center">Foro</div></td>
					</tr>
					<?PHP   
				$contMensajes=0;
		
					foreach ($listaForos as $foro)
					{  
						if (recuperarVariableSistema("sistemanumforo")>$contMensajes)
						{
		
						list($codigoMensaje,$idmateria,$mensaje,$estado,$codigoAutor,$fecha,$facheAct,$fechaCad,$calificable) = explode("[$$$]",$foro);
						$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
								FROM mei_usuario WHERE mei_usuario.idusuario='".$codigoAutor."'";
						$autor=$baseDatos->ConsultarBD($sql);
						list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
		
						if($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
							$sql="SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria='".$idmateria."'";
						else
							$sql="SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria='".$idmateria."'";
					
						$materias=$baseDatos->ConsultarBD($sql);
						list($materia)=mysql_fetch_array($materias);
		
						$alt="Ver los comentarios de este Foro \n";
						$alt.="Autor: ".$nombre1." ". $nombre2." ".$apellido1." ".$apellido2."\n";
						$alt.="Creado el ".$fecha;
		
						if($contMensajes%2==0)
						{
								$color="trListaClaro";
						}
						else
						{
							$color="trListaOscuro";
						}
		?>
											<tr class="<?PHP   echo $color; ?>">
							<td width="250" class="<?PHP   echo $color; ?>"><div align="left"><a href="../foro/index.php?idmateria=<?PHP   echo  $idmateria; ?>" class="link" title="Ver todos los Foros de esta Materia">
							<?PHP   echo  $materia; ?>
							</a></div></td>
							<td width="400" class="<?PHP   echo $color; ?>"><div align="left"><a href="../foro/verMensajeForo.php?idmateria=<?PHP   echo  $idmateria; ?>&codigoMensaje=<?PHP   echo  $codigoMensaje; ?>" class="link" title="<?PHP   echo $alt; ?>">
							<?PHP   echo  substr(strip_tags(stripslashes(base64_decode($mensaje))),0,60); ?>
							</a></div></td>
						</tr>
						<?PHP   
						$contMensajes++;
						}
					}
					?>
				<?PHP   
				}
				else
				{
			?>
					<tr class="trAviso">
						<td class="trAviso" colspan="2">Actualmente no existen Foros Activos</td>
					</tr>
			<?PHP   
		}
	?>
		</table>
	<?PHP   
	}

	//verCorreo()

	function verCorreo($codigo)
	{
		if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==7){
			mysql_query("SET NAMES 'utf8'");
			}
		$baseDatos= new BD();
		$sql  ="SELECT count(*) FROM  mei_relcorusu WHERE  mei_relcorusu.estado=0 AND  mei_relcorusu.idusuario =".$_SESSION['idusuario']."";
		$resultado=$baseDatos->ConsultarBD($sql);
		list($cantidad)=mysql_fetch_array($resultado);

		$sql  ="SELECT  mei_relcorusu.idcorreo FROM  mei_relcorusu WHERE  mei_relcorusu.estado=0 AND  mei_relcorusu.idusuario=".$_SESSION['idusuario']." 
				ORDER BY mei_relcorusu.idcorreo DESC";
		$correos = $baseDatos->ConsultarBD($sql);
	?>
		<br><table class="tablaCentral">
			<tr class="trTitulo">
				<td><img src="../imagenes/correo.gif" alt="Correo"><a href="../correo/index.php?idmateria=<?PHP   echo  $idmateria; ?>" class="link">Correo</a></td></td>
				<td colspan="5"><div align="right">Hay <?PHP   echo  $cantidad ; ?> Correos Nuevos </div></td>
			</tr>
<?PHP   
		if(!empty($cantidad))
		{
			$contCorreos=0;
			?>
      		<tr class="trSubTitulo">
				<td width="200" class="trSubTitulo"><div align="center">Remitente</div></td>
				<td width="200" class="trSubTitulo"><div align="center">Asunto</div></td>
				<td width="150" class="trSubTitulo"><div align="center">Fecha - Hora</div></td>
				<td width="150" class="trSubTitulo"><div align="center">Materia</div></td>
				<td width="50" class="trSubTitulo"><div align="center">Grupo</div></td>
				<td width="50" class="trSubTitulo"><div align="center">SubGrupo</div></td>
          	</tr>
			<?PHP   
                        echo '   <style type="text/css">
		a.imagen {
			position: relative;
		}


		a.imagen img{

			position: absolute;
			bottom: -50;
			left: -100;
			display:none;
		}

  		a.imagen:hover img{
  				display: block;
  		}

  		  	    </style>

                         <script language="javascript">
		function verFoto(nombreImagen,imagen)
		{
			window.open("verImagenUsuario.php?imagen="+imagen+"&nombreImagen="+nombreImagen+"","Usuario","width=200,height=200,left=1010,top=250,scrollbars=NO,menubars=NO,status=NO,statusbar=NO,status=NO,resizable=NO,location=NO");
		}    
    </script>';
						$contCorreo=0;
						while ((list($codigoCorreo) = mysql_fetch_array($correos)) )
						{
							$sql  ="SELECT mei_correo.idcorreo,mei_correo.remitente, mei_correo.asunto,mei_correo.fecha,mei_correo.hora,mei_correo.idmateria FROM mei_correo 
								WHERE mei_correo.idcorreo=".$codigoCorreo." ORDER BY mei_correo.fecha desc";
							$resultado = $baseDatos->ConsultarBD($sql);

							while ((list($codigoCorreo,$remitente,$asunto,$fecha,$hora,$hid_materia)=mysql_fetch_array($resultado))&& ($contCorreo <recuperarVariableSistema('sistemanumcorreo')) )
							{
								$sql= "SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido,mei_usuario.imagen, mei_usuario.idtipousuario 
										FROM mei_usuario WHERE mei_usuario.idusuario=". $remitente;
								$resultado = $baseDatos->ConsultarBD($sql);
								list($nombre1,$nombre2,$apellido1,$apellido2,$imagen,$idtipo)=mysql_fetch_row($resultado);
                                $nombreImagen='../../datosMEIWEB/fotosUsuarios/'.$imagen;
								if ($imagen==-1) {
													$nombreImagen='imagenes/avatar.jpg';
												}
								else $nombreImagen='../../datosMEIWEB/fotosUsuarios/'.$imagen;

								if($contCorreo%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}
								?>
								 <?PHP

								 if( $hid_materia != NULL ){
								 	
								 	if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6){
								 		$sqlmateria= "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$hid_materia." ";
										$resultadomateria = $baseDatos->ConsultarBD($sqlmateria);
										list($materia)= mysql_fetch_row($resultadomateria);
										
								 	}else{
								 		
								 	
										$sqlmateria= "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$hid_materia." ";
										$resultadomateria = $baseDatos->ConsultarBD($sqlmateria);
										list($materia)= mysql_fetch_row($resultadomateria);
									}
								}

									$sql  ="SELECT count(*) FROM  mei_relusumatgru WHERE mei_relusumatgru.idusuario=".$_SESSION['idusuario']."";
									$resultado=$baseDatos->ConsultarBD($sql);
									list($cantidad)=mysql_fetch_array($resultado);

									if(!empty($cantidad)){
									
									if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6){	
										$sql1= "SELECT mei_virgrupo.nombre, mei_virmateria.nombre FROM mei_virgrupo,mei_virmateria, mei_relusuvirgru WHERE  mei_relusuvirgru.idusuario=". $remitente." AND mei_relusuvirgru.idvirgrupo=mei_virgrupo.idvirgrupo AND mei_virgrupo.idvirmateria=".$hid_materia." ";
								
									$igrupo = $baseDatos->ConsultarBD($sql1);
									list($grupo) = mysql_fetch_array($igrupo);
								
									
									
									}else{
										$sql1="SELECT mei_relusumatgru.idgrupo from mei_relusumatgru WHERE mei_relusumatgru.idusuario=".$_SESSION['idusuario']." AND mei_relusumatgru.idmateria=".$hid_materia." ";
									$igrupo = $baseDatos->ConsultarBD($sql1);
									list($idgrupos) = mysql_fetch_array($igrupo);
										
									$sqlgru="SELECT mei_grupo.nombre from mei_grupo WHERE mei_grupo.idgrupo=".$idgrupos."";
									$gru = $baseDatos->ConsultarBD($sqlgru);
									list($grupo)= mysql_fetch_array($gru);
									}
									}	
									else 
										{
									
									
								if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6){
										$sql1= "SELECT mei_virgrupo.nombre, mei_virmateria.nombre FROM mei_virgrupo,mei_virmateria, mei_relusuvirgru WHERE  mei_relusuvirgru.idusuario=". $remitente." AND mei_relusuvirgru.idvirgrupo=mei_virgrupo.idvirgrupo AND mei_virgrupo.idvirmateria=".$hid_materia." ";
								
									$igrupo = $baseDatos->ConsultarBD($sql1);
									list($grupo) = mysql_fetch_array($igrupo);
									}else{
										$sql1="SELECT mei_relusumatgru.idgrupo from mei_relusumatgru WHERE mei_relusumatgru.idusuario=".$remitente." AND mei_relusumatgru.idmateria=".$hid_materia." ";
									$igrupo = $baseDatos->ConsultarBD($sql1);
									list($idgrupos) = mysql_fetch_array($igrupo);
									$sqlgru="SELECT mei_grupo.nombre from mei_grupo WHERE mei_grupo.idgrupo=".$idgrupos."";
									$gru = $baseDatos->ConsultarBD($sqlgru);
									list($grupo)= mysql_fetch_array($gru);
									};
	
											}							
								 ?>								
								<tr class="<?PHP   echo $color; ?>">
									<td class="<?PHP   echo $color; ?>">

										  <a class="imagen"><?if($idtipo!=6){?><img src="<?=$nombreImagen?>" width="100" height="100"><?}?>
										  <i><b><?PHP echo $remitente;?></b></i>
                                         </a> 
                                       <a href="../correo/verCorreo.php?codigoCorreo=<?PHP   echo $codigoCorreo; ?>&idmateria=<?PHP   echo  $idmateria; ?>&materia=<?PHP   echo  $materia; ?>&grupo=<?PHP   echo  $grupo; ?>" class="link" title="Leer este Correo">
										<?PHP   echo  "- ".$nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2 ; ?></a>

										</td>
									<td class="<?PHP   echo $color; ?>"><a href="../correo/verCorreo.php?codigoCorreo=<?PHP   echo $codigoCorreo; ?>&idmateria=<?PHP   echo  $idmateria; ?>&materia=<?PHP   echo  $materia; ?>&grupo=<?PHP   echo  $grupo; ?>" class="link" title="Leer este Correo">
										<?PHP   echo  base64_decode($asunto); ?></a></td>
                                    <td class="<?PHP   echo $color; ?>"><?PHP   echo $fecha."  ".$hora; ?></td>
                                    

                                      <td class="<?PHP   echo $color; ?>"><?PHP   echo $materia; ?></td>
                                      <td class="<?PHP   echo $color; ?>"><?PHP   echo $grupo; ?></td>
                                      <?PHP   
                                     $sql="SELECT mei_subgrupo.nombre from mei_subgrupo where mei_subgrupo.idsubgrupo in 
                                     (SELECT idsubgrupo from mei_relususub where mei_relususub.idusuario ='".$remitente."')";
													$autor3=$baseDatos->ConsultarBD($sql);
													list($subgrupo) = mysql_fetch_array($autor3);
                                     ?>
                                       <td class="<?PHP   echo $color; ?>"><?PHP   echo $subgrupo; ?></td>
								</tr>
							<?PHP   
								$contCorreo++;

							}
						}
		}
		else
		{
	?>
			<tr class="trAviso">
				<td colspan="3" class="trAviso">Actualmente no tiene Correos nuevos</td>
			</tr>
	<?PHP   
		}
	?>
		</table>
	<?PHP   
	}
	?>