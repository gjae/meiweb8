<?PHP
  /*
  Nombre  : estandar.lib.php
  Autor   : GrupoMEI
  Fecha   : 10/10/2005
  Detalle : Funciones estandar
  Version : 2.0

  Lista de funciones :
  eliminarRepetidos();
  descargarArchivo()
  mostrarError()
  mostrarSaludo()
  recuperarVariableSistema()
  mostrarFecha()
  redireccionar()
  borrarIP()
  registrarIp();
  comprobarSession()
  crearClavesession()
  crearSession()
  destruirSession()
  eliminarEspacios()
  cargarArchivo()
  registrarBitacora()


  */

  include_once('../baseDatos/BD.class.php');
  include_once('../librerias/estandar.lib.php');

  function arreglarCadena($texto)
  { $baseDatos=new BD();
    mysql_query("SET NAMES 'utf8'");
    
      $texto=str_replace("á","A",$texto);
      $texto=str_replace("Á","A",$texto);
      $texto=str_replace("é","E",$texto);
      $texto=str_replace("É","E",$texto);
      $texto=str_replace("í","I",$texto);
      $texto=str_replace("Í","I",$texto);  
      $texto=str_replace("ó","O",$texto);
      $texto=str_replace("Ó","O",$texto);   
      $texto=str_replace("ú","U",$texto);
      $texto=str_replace("Ú","U",$texto); 
      $texto=str_replace("ñ","Ñ",$texto);
      $texto=str_replace("Ñ","Ñ",$texto);
    
      return $texto;    
  }

  function arreglarTilde($texto)
  { $baseDatos=new BD();
    mysql_query("SET NAMES 'utf8'");
    
      $texto=str_replace("á","&aacute;",$texto);
      $texto=str_replace("Á","&Aacute;",$texto);
      $texto=str_replace("é","&eacute;",$texto);
      $texto=str_replace("É","&Eacute;",$texto);
      $texto=str_replace("í","&iacute;",$texto);
      $texto=str_replace("Í","&Iacute;",$texto);  
      $texto=str_replace("ó","&oacute;",$texto);
      $texto=str_replace("Ó","&Oacute;",$texto);   
      $texto=str_replace("ú","&uacute;",$texto);
      $texto=str_replace("Ú","&Uacute;",$texto);
      $texto=str_replace("ñ","&ntilde;",$texto);
      $texto=str_replace("Ñ","&Ntilde;",$texto); 
    
      return $texto;    
  }

  function corregirCaracteresURL($cadena)
  {
    if(strlen($cadena))
    {
      $listaReemplazo=array("�;a","�;e","�;i","�;o","�;u",
      "�;a","�;e","�;i","�;o","�;u",
      "�;a","�;e","�;i","�;o","�;u",
      "�;a","�;o","�;n",
      "�;A","�;E","�;I","�;O","�;U",
      "�;A","�;E","�;I","�;O","�;U",
      "�;A","�;E","�;I","�;O","�;U",
      "�;A","�;O","�;N"," ;_");

      foreach($listaReemplazo as $reemplazo)
      {
        list($error,$correcto)=explode(";",$reemplazo);
        $cadena=str_replace($error,$correcto,$cadena);
      }
      $cadena=eregi_replace ("\.php$",".txt", $cadena);
      $cadena=eregi_replace ("[^[:alnum:]\.]","_", $cadena);
      $cadena=eregi_replace ("[_]+","_", $cadena);

      return $cadena;
    }
    else
    {
      return $cadena;
    }
  }

  function limpiarCadenaBusqueda($cadena)
  {
    $listaReemplazo=array("�[---]A","�[---]E","�[---]I","�[---]O","�[---]U",
    "�[---]A","�[---]E","�[---]I","�[---]O","�[---]U",
    "�[---]A","�[---]E","�[---]I","�[---]O","�[---]U",
    "�[---]A","�[---]O","�[---]N",
    "&AACUTE;[---]A","&EACUTE;[---]E","&IACUTE;[---]I","&OACUTE;[---]O","&UACUTE;[---]U",
    "&AGRAVE;[---]A","&EGRAVE;[---]E","&IGRAVE;[---]I","&OGRAVE;[---]O","&UGRAVE;[---]U",
    "&ACIRC;[---]A","&ECIRC;[---]E","&ICIRC;[---]I","&OCIRC;[---]O","&UCIRC;[---]U",
    "&ATILDE;[---]A","&OTILDE;[---]O","&NTILDE;[---]N",
    "&NBSP;[---] ");

    $cadena=strtoupper($cadena);
    $cadena=trim($cadena);

    foreach($listaReemplazo as $caracterR)
    {
      list($Cerror,$Ccorrecto)=explode("[---]",$caracterR);

      $cadena=str_replace($Cerror,$Ccorrecto,$cadena);
    }
    $cadena=ereg_replace("[^[:alnum:]|^[:space:]]","",$cadena);

    $caracterSalto="
    ";
    $cadena=str_replace($caracterSalto," ",$cadena);

    return $cadena;
  }


  function comprobarEditor($cadena)
  {
    $cadenaLimpia=str_replace("<p>&nbsp;</p>","",$cadena);
    $cadenaLimpia=trim($cadenaLimpia);

    if(!empty($cadenaLimpia))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  function eliminarRepetidos($strSeparador,$strElementos)
  {
    $contElementos=0;
    $lista=explode($strSeparador,$strElementos);

    if(sizeof($lista))
    {
      sort($lista);

      for($i=0;$i<sizeof($lista);$i++)
      {
        if($lista[$i] != false)
        {
          $vector[$contElementos++]=$lista[$i];

          for($k=$i+1;$k<sizeof($lista);$k++)
          {
            if($lista[$i] == $lista[$k])
            {
              $lista[$k]=false;
            }
          }
        }
      }

      return $vector;
    }
    else
    {
      return false;
    }
  }


  //descargarArchivo($nombreArchivo)
  function descargarArchivo($nombreArchivo,$nombreDescarga)
  {
    $nombreBase = basename($nombreArchivo);
    header( "Content-Type: application/octet-stream");
    header( "Content-Length: ".filesize($nombreArchivo));
    header( "Content-Disposition: attachment; filename=".$nombreDescarga);
    readfile($nombreArchivo);
  }

  //mostrarError(), Retorna el mensaje de error

  function mostrarError($error)
  {
    $baseDatos= new BD();

    $sql="SELECT mei_errorsistema.mensajeerror FROM mei_errorsistema WHERE mei_errorsistema.codigoerror='$error'";
    list($mensaje)=mysql_fetch_array($baseDatos->ConsultarBD($sql));


    return $mensaje;
  }

  //mostrarSaludo(),

  function mostrarSaludo($tipo,$sexo)
  {
    if ($tipo==1 && $sexo=='f')

      return 'Administradora';

    else if ($tipo==1 && $sexo=='m')

        return 'Administrador';

      if ($tipo==2 && $sexo=='f')

      return 'Profesora';

    else if ($tipo==2 && $sexo=='m')

        return 'Profesor';

      if ($tipo==3 && $sexo=='f')

      return 'Alumna';

    else if ($tipo==3 && $sexo=='m')

        return 'Alumno';
  }

  //recuperarVariableSistema(), Retorna el valor de variables del sistema

  function recuperarVariableSistema($variable)
  {
    $baseDatos= new BD();

    $sql="SELECT mei_configuracion.valor FROM mei_configuracion WHERE mei_configuracion.variable='$variable'";
    $consulta=$baseDatos->ConsultarBD($sql);


    if(list($valor)=mysql_fetch_row($consulta))
    {
      return $valor;
    }
    else
    {
      print "No existe la variable ".$valor;
      exit();
    }
  }

  //mostrarFecha(), Retorna la fecha del servidor

  function mostrarFecha($fecha,$diaSemana)
  {
    list($agno,$mes,$diaMes)=explode('-',$fecha);

    switch ($diaSemana)
    {
      case 0:
        $textDiaSemana="Domingo";
        break;
      case 1:
        $textDiaSemana="Lunes";
        break;
      case 2:
        $textDiaSemana="Martes";
        break;
      case 3:
        $textDiaSemana="Miercoles";
        break;
      case 4:
        $textDiaSemana="Jueves";
        break;
      case 5:
        $textDiaSemana="Viernes";
        break;
      case 6:
        $textDiaSemana="Sabado";
        break;

    }
    switch ($mes)
    {
      case 1:
        $textMes="Enero";
        break;
      case 2:
        $textMes="Febrero";
        break;
      case 3:
        $textMes="Marzo";
        break;
      case 4:
        $textMes="Abril";
        break;
      case 5:
        $textMes="Mayo";
        break;
      case 6:
        $textMes="Junio";
        break;
      case 7:
        $textMes="Julio";
        break;
      case 8:
        $textMes="Agosto";
        break;
      case 9:
        $textMes="Septiembre";
        break;
      case 10:
        $textMes="Octubre";
        break;
      case 11:
        $textMes="Noviembre";
        break;
      case 12:
        $textMes="Diciembre";
        break;
    }
    if(!empty($diaSemana))
      return $textDiaSemana.", ".$diaMes." de ".$textMes." de ".$agno;
    else
      return (int)$diaMes." de ".$textMes." de ".$agno;
  }

  //redireccionar(), Redirecciona a una pagina

  function redireccionar($pagina)
  {
    print "<script language='javascript'> location.replace('$pagina')</script> </script>";
  }

  //borrarIp(), Borra las Ip registradas en la BD cuando han caducado
  function borrarIp()
  {
     registrarIp($_SERVER['REMOTE_ADDR']);
	$baseDatos= new BD();
    $sql="SELECT mei_ipactivo.idipactivo,mei_ipactivo.idusuario,mei_ipactivo.hora,mei_ipactivo.fecha  FROM mei_ipactivo";
    $consulta=$baseDatos->ConsultarBD($sql);

    while(list($ipActivo,$idusuario,$horaBD,$fechaBD)=mysql_fetch_array($consulta))
    {
      list($hora,$minuto,$segundo)=explode(":",$horaBD);
      list($ano,$mes,$dia)=explode("-",$fechaBD);

      $tiempoSegundoBD=(($ano*31556926)+($mes*2678400)+($dia*86400)+($hora*3600)+($minuto*60)+$segundo);

      list($hora,$minuto,$segundo)=explode(":",date("H:i:s"));
      list($ano,$mes,$dia)=explode("-",date("Y-n-d"));

      $tiempoSegundoSistema=(($ano*31556926)+($mes*2678400)+($dia*86400)+($hora*3600)+($minuto*60)+$segundo);

      if(($tiempoSegundoSistema-$tiempoSegundoBD)>recuperarVariableSistema('sistematiempoactivo'))
      {
        $sql="DELETE FROM mei_ipactivo WHERE mei_ipactivo.idipactivo=".$ipActivo;
        $baseDatos->ConsultarBD($sql);
      }
    }

  }

  //RegistrarIp(); Registra la Ip del equipo en La BD
  function registrarIp($ip)
  {
    $baseDatos= new BD();

	//Borra en todas las ocaciones cuando el tipo de usuario es docente
    $sql= "SELECT mei_usuario.idtipousuario FROM mei_usuario WHERE mei_usuario.idusuario ='".$_SESSION['idusuario']."' ";
  	$consulta=$baseDatos->ConsultarBD($sql);
    list($idTipoUsuario)=mysql_fetch_array($consulta);
    if ($idTipoUsuario==2 || $idTipoUsuario==5)
     {
     	 $sql="DELETE FROM mei_ipactivo WHERE mei_ipactivo.idusuario='".$_SESSION['idusuario']."' ";
     	 $baseDatos->ConsultarBD($sql);
     }

    //Borra en todas las ocaciones cuando el tipo de usuario es alumno y no esta en examen
   /* $sql= "SELECT mei_ipactivo.estado_eva FROM mei_ipactivo WHERE mei_ipactivo.idusuario ='".$_SESSION['idusuario']."' ";
  	$consulta=$baseDatos->ConsultarBD($sql);
    list($estadoEvaluacion)=mysql_fetch_array($consulta);
    if (($idTipoUsuario==3 || $idTipoUsuario==6) && $estadoEvaluacion==0)
     {
     	 $sql="DELETE FROM mei_ipactivo WHERE mei_ipactivo.idusuario='".$_SESSION['idusuario']."' ";
     	 $baseDatos->ConsultarBD($sql);
     }*/

  
       
    $sql="SELECT mei_ipactivo.* FROM mei_ipactivo WHERE mei_ipactivo.idusuario='".$_SESSION['idusuario']."'";
    $consulta=$baseDatos->ConsultarBD($sql);

    $assoc_ip = mysql_fetch_assoc($consulta);

    if(mysql_num_rows($consulta) == 0)
    {
      $sql="INSERT INTO mei_ipactivo ( idipactivo,idusuario,ipactivo,hora,fecha)
      VALUES ('', '".$_SESSION['idusuario']."', '".$_SERVER['REMOTE_ADDR']."', '".date("H:i:s")."' , '".date("Y-n-d")."')";
	  //echo $sql;
      $consulta=$baseDatos->ConsultarBD($sql);
    }
    else if($assoc_ip['ipactivo'] == $_SERVER['REMOTE_ADDR'])
    {
      $sql="UPDATE mei_ipactivo SET hora='".date("H:i:s")."' fecha='".date("Y-n-d")."' WHERE idusuario='".$idIpActivo."'";
      $consulta=$baseDatos->ConsultarBD($sql);
    }
      else
      {
        if ($idTipoUsuario==3)
     {
        $fechahoy = strtotime(date("Y-m-d"));
        $fechabd= strtotime($assoc_ip['fecha']);
        if ($fechahoy>$fechabd)
      {
         $sql="UPDATE mei_ipactivo SET ipactivo='".$_SERVER['REMOTE_ADDR']."' hora='".date("H:i:s")."' fecha='".date("Y-n-d")."' WHERE idusuario='".$idIpActivo."'";
      $consulta=$baseDatos->ConsultarBD($sql);
      }
      elseif ($fechahoy==$fechabd)
       {
        $horaact = strtotime(date("H:i:s"));
        $horadb= strtotime($assoc_ip['hora']);
        $dif = date("H:i:s",strtotime("00:00:00")+$horaact-$horadb);
        $horadif = strtotime($dif);
        $var = recuperarVariableSistema("sistematiempoactivo")/3600;
        $intervalo = strtotime(date($var.":00:00"));
        if($horadif>$intervalo) {
            $sql="UPDATE mei_ipactivo SET ipactivo='".$_SERVER['REMOTE_ADDR']."' hora='".date("H:i:s")."' fecha='".date("Y-n-d")."' WHERE idusuario='".$idIpActivo."'";
            $consulta=$baseDatos->ConsultarBD($sql);
        }
        else{
      
        redireccionar('../login/index.php?entrar=1&error_ip=1');
          }
      }
    }
    }
  }
  //comprobarSession(); //Comprueba si una session esta activa

  function comprobarSession()
  {
     
     session_start();
    $baseDatos= new BD();

    if($_SESSION['banderaAdmnistrador']==1)
    {
      //
      if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
      {
        $sql="SELECT mei_usuario.clavesession FROM mei_usuario WHERE mei_usuario.idusuario='".$_SESSION['idusuario']."'";
        $consulta=$baseDatos->ConsultarBD($sql);
        list($claveSessionBD)=mysql_fetch_array($consulta);

        if(md5($_SESSION['clavesession'])==$claveSessionBD)
        {
          registrarIp($_SERVER['REMOTE_ADDR']);
          return true;
        }
        else
        {
          // Clave de session invalida
          destruirSession();
          return false;
        }

      }
      else
      {
        if(file_exists("../configuracion/archivos/admin.conf"))
        {
          $cadenaAdmin=file_get_contents("../configuracion/archivos/admin.conf");
          if($_SESSION['admnistrador']==md5(sha1($cadenaAdmin)))
          {
            $_SESSION['idtipousuario']=1;
            return true;
          }
          else
          {
            return false;
          }

        }
        else
        {
          return false;
        }
      }
      //
    }
    else
    {
      if(isset($_SESSION['idusuario']))
      {

        $sql="SELECT mei_usuario.clavesession FROM mei_usuario WHERE mei_usuario.idusuario='".$_SESSION['idusuario']."'";
        $consulta=$baseDatos->ConsultarBD($sql);
        list($claveSessionBD)=mysql_fetch_array($consulta);

        if(md5($_SESSION['clavesession'])==$claveSessionBD)
        {
          //Session iniciada
          registrarIp($_SERVER['REMOTE_ADDR']);
          return true;
        }
        else
        {
          // Clave de session invalida
          destruirSession();
          return false;
        }
      }
      else
      {
        destruirSession();
        return false;
      }
    }//FIN IF PROFESOR
  }

  //crearClavesession(), Crea y retorna una variable de session

  function crearClavesession($idUsuario)
  {
    registrarIp($_SERVER['REMOTE_ADDR']);
    return md5(crypt($idUsuario));

  }

  //crearSession(), Crea la session

  function crearSession($login)
  {
    $baseDatos= new BD();
    $baseDatos->CentinelaCopiasBD();

    borrarIp();
	$claveSession = crearClavesession($idusuario);
    $sql="UPDATE mei_usuario SET mei_usuario.clavesession='".md5($claveSession)."' WHERE mei_usuario.login='".$login."'";
    $consulta=$baseDatos->ConsultarBD($sql);

    $sql="SELECT  mei_usuario.idusuario, mei_usuario.administrador,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, mei_usuario.sexo, mei_usuario.idtipousuario  FROM mei_usuario WHERE mei_usuario.login='".$login."'";
    $consulta=$baseDatos->ConsultarBD($sql);
    list($idusuario,$administrador,$nombre1,$nombre2,$apellido1,$apellido2,$sexoBD,$idTipoUsuarioBD,)=mysql_fetch_array($consulta);


    destruirSession();
    //Al destruirse la session anterior es necesario registrar nuevamente IdUsuario
    session_start();
    if($administrador==1)
    {
      $_SESSION['banderaAdmnistrador'];
      $_SESSION['banderaAdmnistrador']=1;
    }
    $_SESSION['clavesession'];
    $_SESSION['nombreusuario'];
    $_SESSION['apellidousuario'];
    $_SESSION['sexo'];
    $_SESSION['idtipousuario'];

    $_SESSION['idusuario']=$idusuario;
    $_SESSION['clavesession']=$claveSession;
    $_SESSION['nombreusuario']=$nombre1." ".$nombre2;
    $_SESSION['apellidousuario']=$apellido1." ".$apellido2;
    $_SESSION['sexo']=$sexoBD;
    $_SESSION['idtipousuario']=$idTipoUsuarioBD;

  }

  //destruirSession(), Destruye una session
  function destruirSession()
  {
    session_start();
    session_unset();
    session_destroy()
    or die ("No se ha podido destruir la session");
  }

  //	function eliminarEspacios($cadena)

  function eliminarEspacios($cadena)
  {
    $descripcion='';

    $listaDescripcion=explode('</p>',$cadena);

    foreach($listaDescripcion as $valor)
    {
      if(!empty($valor))
      {
        if(empty($descripcion))
          $descripcion=trim($valor);
        else
          $descripcion.=trim($valor);
      }
    }
    return $descripcion;
  }


  //cargarArchivo(), Sube archivos al servidor, entra el nombre de variable File y la ubicaci�n del archivo en el servidor
  function cargarArchivo($archivo,$nuevoNombre,$nombreDirectorio,$tamano)
  {
    $sistemaTamanoArchivo=recuperarVariableSistema('sistematamanoArchivo');

    //		$nombreArchivo = $_FILES[$archivo]['name'];

    $tipoArchivo = $_FILES[$archivo]['type'];
    //$tamano = $_FILES[$archivo]['size'];
	//echo $tamano;
    if ($tamanoArchivo > $sistemaTamanoArchivo)
    {
      print "1";	
      return false;
      
    }
    else
    {
      if (move_uploaded_file($_FILES[$archivo]['tmp_name'], $nombreDirectorio.$nuevoNombre))
      {
        return true;
        //print "El archivo ha sido cargado correctamente.";
      }
      else
      {
        return false;
         //print "2";
      }
    }
  }

  //registrarBitacora(), Registra en la bitacora
  function registrarBitacora($idModulo,$idTipoAccion,$descripcion)
  {
    $baseDatos= new BD();
   
    $sql="INSERT INTO  mei_bitacora(idbitacora,idusuario,idmodulo,idtipoaccion,fecha,hora,descripcion)
    VALUES('', '".$_SESSION['idusuario']."','".$idModulo."','".$idTipoAccion."','".date("Y-n-d")."','".date("H:i:s")."','".$descripcion."') ";
    $resultado=$baseDatos->ConsultarBD($sql);
  }

  //*************************************AGREGADAS POR CLAUDIA Y LEO*********************************
  function obtener_tamamo($tamano)
  {
    if ($tamano < 2) return "$size byte";
    $unidad = array(' Bytes', ' Kb', ' Mb', ' Gb', ' Tb');
    for ($i = 0; $tamano > 1024; $i++)
    { $tamano /= 1024; }

    return round($tamano, 2).$unidad[$i];
  }

  function descargar_archivo($ruta)
  {
    @header ("Content-Disposition: attachment; filename=".basename($ruta)."\n\n");
    @header ("Content-Type: application/octet-stream");
    @header ("Content-Length: ".filesize($ruta));
    readfile($ruta);
  }

  function mostrarFechaTexto($a_fecha, $tipo)
  {
    //list($agno,$mes,$dia,$hora,$min)=explode('-',$a_fecha);
    //list($hora,$min) = explode('-',$hora);
    if ($tipo == 1)
    {
      $agno = substr($a_fecha,0,4);
      $mes = substr($a_fecha,4,2);
      $dia = substr($a_fecha,6,2);
      $hora = substr($a_fecha,8,2);
      $min = substr($a_fecha,10,2);
      $a_diaSemana = date("w", mktime($hora,$min,0,$mes,$dia,$agno) );
      $hora_a = date("g:i a", mktime($hora,$min,0,$mes,$dia,$agno) );
    }
    elseif ($tipo == 0)
    {
      $agno = date("Y", $a_fecha );
      $mes = date("m", $a_fecha );
      $dia = date("d", $a_fecha );
      $a_diaSemana = date("w", $a_fecha );
      $hora_a = date("g:i a", $a_fecha );
    }
    /*
    print "<p>agno: ".$agno;
    print "<p>mes: ".$mes;
    print "<p>dia: ".$dia;
    print "<p>hora: ".$hora;
    print "<p>min: ".$min;
    */

    switch ($a_diaSemana)
    {
      case 0:
        $textA_diaSemana="Domingo";
        break;
      case 1:
        $textA_diaSemana="Lunes";
        break;
      case 2:
        $textA_diaSemana="Martes";
        break;
      case 3:
        $textA_diaSemana="Miercoles";
        break;
      case 4:
        $textA_diaSemana="Jueves";
        break;
      case 5:
        $textA_diaSemana="Viernes";
        break;
      case 6:
        $textA_diaSemana="Sabado";
        break;
    }

    switch ($mes)
    {
      case 1:
        $textA_mes="Enero";
        break;
      case 2:
        $textA_mes="Febrero";
        break;
      case 3:
        $textA_mes="Marzo";
        break;
      case 4:
        $textA_mes="Abril";
        break;
      case 5:
        $textA_mes="Mayo";
        break;
      case 6:
        $textA_mes="Junio";
        break;
      case 7:
        $textA_mes="Julio";
        break;
      case 8:
        $textA_mes="Agosto";
        break;
      case 9:
        $textA_mes="Septiembre";
        break;
      case 10:
        $textA_mes="Octubre";
        break;
      case 11:
        $textA_mes="Noviembre";
        break;
      case 12:
        $textA_mes="Diciembre";
        break;
    }

    return $textA_diaSemana." ".$dia." de ".$textA_mes." de ".$agno.", ".$hora_a;
  }

  function addGuiones($fecha)
  {
    $agno = substr($fecha,0,4);
    $mes = round(substr($fecha,4,2));
    $dia = round(substr($fecha,6,2));
    $hora = round(substr($fecha,8,2));
    $min = round(substr($fecha,10,2));

    return $agno."-".$mes."-".$dia."-".$hora."-".$min;
  }

  function tiempoEmpleado($duracion)
  {
    if ( $duracion < 60 )
      $tiempo = $duracion." Segundo(s)";
    else
    {
      $tiempo = ($duracion/60);
      $min = floor($tiempo);
      $seg = ($tiempo - $min)*60;
      $tiempo = $min." Minuto(s) ".round($seg)." Segundo(s)";
    }
    return $tiempo;
  }
?>
