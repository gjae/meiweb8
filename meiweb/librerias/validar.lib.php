<?PHP
	/*
	Nombre  : validar.lib.php
	Autor   : GrupoMEI
	Fecha   : 10/10/2005
	Detalle : Funciones de validacion
	Versión : 2.0
	
	Lista de funciones :
	
		validarUsuario()
		validarCorreoUsu()
		validarUsuarioIdusuario()
		validarClave()
		validarBloqueo()
		validarAlias()
		validarGrupo()
		desbloquearUsuario()
		validarBloqueoPrevio()
		
	*/
	
	include_once('../baseDatos/BD.class.php');
	
	// validarUsuariio(), Comprueba la existencia de un usuario
	
	
	
	function validarUsuario($idUsuario)
	{
		$baseDatos= new BD();
		$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.login='$idUsuario'";
		$consulta=$baseDatos->ConsultarBD($sql);
		list($idUsuario)=mysql_fetch_row($consulta);
		
		if (!empty($idUsuario))
			return true;
		else
			return false;
	}

	function validarCorreoUsu($correo)
	{
		$baseDatos= new BD();
		$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.correo='$correo'";
		$consulta=$baseDatos->ConsultarBD($sql);
		list($idUsuario)=mysql_fetch_row($consulta);
		
		if (!empty($idUsuario))
			return true;
		else
			return false;
	} 


	function validarUsuarioIdusuario($idUsuario)
	{
		$baseDatos= new BD();
		$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.idusuario='$idUsuario'";
		$consulta=$baseDatos->ConsultarBD($sql);
		list($idUsuario)=mysql_fetch_row($consulta);
		
		if (!empty($idUsuario))
			return true;
		else
			return false;
	}
	
	// validarGrupo(), Verifica si el usuario existe en un grupo.
	
	function validarGrupo($idUsuario)
	{
		$baseDatos= new BD();
				
		$sql="SELECT COUNT(mei_relusugru.idusuario) FROM mei_relusugru WHERE mei_relusugru.idusuario='".$idUsuario."'";
		$consulta=$baseDatos->ConsultarBD($sql);
		
		list($numGrupos)=mysql_fetch_array($consulta);
		
		if(!empty($numGrupos))
		{
			return true;
		}
		else
		{
			return false;
		}		
	}

	// validarClave(), Verifica si la clave es correcta
	
	function validarClave($idUsuario,$clave)
	{	
		$baseDatos= new BD();
		$clave=md5(md5($clave));
		$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.login='$idUsuario' AND clave='$clave'";
		$consulta=$baseDatos->ConsultarBD($sql);
		list($codigo)=mysql_fetch_row($consulta);
		
		if (!empty($codigo))	
		{			
			$sql="UPDATE mei_usuario SET mei_usuario.errorclave=0 WHERE mei_usuario.login='$idUsuario'";
			$consulta=$baseDatos->ConsultarBD($sql);
			return true;
		}
		else
		{	
			$sql="SELECT mei_usuario.errorclave FROM mei_usuario WHERE mei_usuario.login='$idUsuario'";
			$consulta=$baseDatos->ConsultarBD($sql);
			list($errorClave)=mysql_fetch_row($consulta);
				
			$errorClave=$errorClave+1;
				
			$sql="UPDATE mei_usuario SET mei_usuario.errorclave=$errorClave WHERE mei_usuario.login='$idUsuario'";
			$consulta=$baseDatos->ConsultarBD($sql);
				
			return false;
		}
	}

	// validarBloqueo(), Verifica si un usuario esta bloqueado
	
	function validarBloqueo($idUsuario)
	{	
		$baseDatos= new BD();
		$sql="SELECT mei_usuario.errorclave FROM mei_usuario WHERE mei_usuario.login='$idUsuario'";
		$consulta=$baseDatos->ConsultarBD($sql);
		list($errorClave)=mysql_fetch_row($consulta);
		
		if ($errorClave>=recuperarVariableSistema("sistemanumerror"))
		{
			$sql="UPDATE mei_usuario SET mei_usuario.estado=1
			WHERE mei_usuario.login='$idUsuario'";
			$consulta=$baseDatos->ConsultarBD($sql);
			return true;
		}
		else
		{
			return false;
		}
	}

	function desbloquearUsuario($idusuario)
	{
		
		$baseDatos= new BD();
		$sql="SELECT login, clave FROM mei_blss WHERE idusuario='".$idusuario."'";
		$consulta=$baseDatos->ConsultarBD($sql);
		list($login,$clave)=mysql_fetch_row($consulta);
		
		$sql1="UPDATE mei_usuario set mei_usuario.login='".$login."', mei_usuario.clave='".$clave."' WHERE mei_usuario.idusuario='".$idusuario."'";
		$consulta1=$baseDatos->ConsultarBD($sql1);

		$sql2= "DELETE FROM mei_blss WHERE mei_blss.idusuario='".$idusuario."'";
		$consulta2=$baseDatos->ConsultarBD($sql2);

	}

	function validarBloqueoPrevio($login)
	{	
		$baseDatos= new BD();
		$sql="SELECT mei_blss.idusuario, mei_blss.fecha, mei_blss.hora FROM mei_blss WHERE mei_blss.login='".$login."'";
		$consulta=$baseDatos->ConsultarBD($sql);
		list($idusuario,$fecha,$hora)=mysql_fetch_row($consulta);
		
		$fechahoy = strtotime(date("Y-m-d"));
		$fechabd= strtotime($fecha);

		if (empty($idusuario)) {
			return true;
		}
		else
		{
			if ($fechahoy>$fechabd)
			{
				desbloquearUsuario($idusuario);
				return true;
			}
			else
			{
				$horaact = strtotime(date("H:i:s"));
				$horadb= strtotime($hora);
				$dif = date("H:i:s",strtotime("00:00:00")+$horaact-$horadb);
				$horadif = strtotime($dif);
				$intervalo = strtotime(date("01:00:00"));
				if($horadif>$intervalo) {
					desbloquearUsuario($idusuario);
					return true;
				}
				else 
				{
					return false;
				}
			}
		}
	}

	// validarAlias(), Verifica si el alias es correcto
	
	function validarAlias($idUsuario,$alias)
	{	
		$baseDatos= new BD();
		$clave=md5(md5($clave));
		$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.login='$idUsuario' AND alias='$alias'";
		$consulta=$baseDatos->ConsultarBD($sql);
		list($codigo)=mysql_fetch_row($consulta);
		
		if (isset($codigo))	
		{			
			return true;
		}
		else
		{	
			return false;
		}
	}
	
	// VirValidarGrupo(), Verifica si el usuario existe en un grupo.
	
	function VirValidarGrupo($idUsuario)
	{
		$baseDatos= new BD();
				
		$sql="SELECT COUNT(mei_relusuvirgru.idusuario) FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario='".$idUsuario."'";
		$consulta=$baseDatos->ConsultarBD($sql);
		
		list($numGrupos)=mysql_fetch_array($consulta);
		
		if(isset($numGrupos))
		{
			return true;
		}
		else
		{
			return false;
		}		
	}	
?>