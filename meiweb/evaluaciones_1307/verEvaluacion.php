<?
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');	
/*  Nombre  : vertaller.php
	Autor   : Claudia
	Fecha   : 09/11/2005
	Detalle : Vista de Talleres
	Versión :
	usu = 1991175
	pass =123
*/
$baseDatos=new BD();
if(comprobarSession())
	{//if comprobar sesion
if (!empty($_GET["cbo_orden"])) {
			
			$_POST["cbo_orden"]=$_GET["cbo_orden"];
		}
?>
<head>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<link href="../evaluaciones/css/lightbox.css" type="text/css" media="screen"  rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MEIWEB</title>
</head>


	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js?load=effects" type="text/javascript"></script>
	<script src="js/lightbox.js" type="text/javascript"></script>
<script language="javascript">

	function activar(estado, idprevio)
	{
		location.replace('guardarEvaluacion.php?materia=<?=$_GET['materia']?>&idmateria=<?=$_GET["idmateria"]?>&modo=ACT&estado='+estado+'&idprevio='+idprevio)
	}
	function activarvisibilidad(estado, idprevio,visibilidad)
	{
			location.replace('guardarEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idprevio='+idprevio+'&modo=ACTVISI&estado='+estado+'&visibilidad='+visibilidad+'&evaluacion=1');
	}
	function cambiarestado(idprevio,i,value) {

		//alert (idprevio+i+value);
		$.ajax({
    					data:  ({ idprevio : idprevio }),
    					url:   'cambiarestadoEvaluaciones.php',
    					type:  'post',
    					
    					
    			});	
		if (value=="imagenes/activo.gif") {
			//alert('desactiva');
			document.getElementById(i).value="imagenes/inactivo.gif";
			document.getElementById(i).src="imagenes/inactivo.gif";
			
		}
		else  {
			//alert('activa');
			document.getElementById(i).value="imagenes/activo.gif";
			document.getElementById(i).src="imagenes/activo.gif";
			
		}
		
		

	}
	function cambiarvisibilidad(idprevio,i,value) {

		//alert (idprevio+i+value);
		$.ajax({
    					data:  ({ idprevio : idprevio }),
    					url:   'cambiarvisibilidadEvaluaciones.php',
    					type:  'post',
    					
    					
    			});	
		if (value=="imagenes/activo.gif") {
			//alert('desactiva');
			document.getElementById(i).value="imagenes/inactivo.gif";
			document.getElementById(i).src="imagenes/inactivo.gif";
			
		}
		else  {
			//alert('activa');
			document.getElementById(i).value="imagenes/activo.gif";
			document.getElementById(i).src="imagenes/activo.gif";
			
		}
		
		

	}
</script>
<body>

<table class="tablaPrincipal">
	<tr valign="top">
        <td class="tablaEspacio">&nbsp;</td>
        <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacio">&nbsp;</td>
        <td valign="top">
		 <table class="tablaMarquesina">
		  <tr>
<?

		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado);
		
		/*Agregar Campo*/
		$exists2 = false;
		$db2 = "mei_evaquiz";
		$column2 = "visibilidad";
		$column_attr2 = "int(11) null";
		$columns = "show columns from $db2";
		$resultcolumn2 = $baseDatos->ConsultarBD($resultcolumn2);
		while($c = mysql_fetch_assoc($resultcolumn2)){
			if($c['Field'] == $column2){
				$exists2 = true;
				break;
			}
		}        
		if(!$exists2){
			$sql = "ALTER TABLE `$db2` ADD `$column2`  $column_attr2";
		}					
		$baseDatos->ConsultarBD($sql);		
		/*Fin Agregar Campo*/
		
		/*Agregar Campo*/
		$exists2 = false;
		$db2 = "mei_evaluacion";
		$column2 = "visibilidad";
		$column_attr2 = "int(11) null";
		$columns = "show columns from $db2";
		$resultcolumn2 = $baseDatos->ConsultarBD($resultcolumn2);
		while($c = mysql_fetch_assoc($resultcolumn2)){
			if($c['Field'] == $column2){
				$exists2 = true;
				break;
			}
		}        
		if(!$exists2){
			$sql = "ALTER TABLE `$db2` ADD `$column2`  $column_attr2";
		}					
		$baseDatos->ConsultarBD($sql);		
		/*Fin Agregar Campo*/			
?>
		    <td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nombre))?></a><a> -> Evaluaciones</a></td>
		  </tr>
		 </table>&nbsp;
		
<?       $idbot = 0;
/*echo "<script>window.alert('".$_SESSION['idtipousuario']."')</script>";*/
if ($_SESSION['idtipousuario']==1 || $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
{//SELECCION DEL GRUPO
	//print "Ahora: ".date("YmdGi")."<p>";
	$m = date("YmdGi", mktime(17,0,0,6,25,2006) );
	//print "FEcha previo".$m

?>
<form name="frm1" method="post" action="verEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET['materia']?>">
	  <table class="tablaGeneral" border="0">
        <tr class="trTitulo">
          <td width="25%">Ordenar por el Grupo  : </td>
          <td width="75%"><select class="link" name="cbo_orden" id="cbo_orden" onChange="javascrip:document.frm1.submit()">
			              <option value="*">--Todos--</option>
<?		
		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_virgrupo.idvirgrupo, mei_virgrupo.nombre FROM `mei_virgrupo`, mei_relusuvirgru
				WHERE mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo AND
				mei_relusuvirgru.idusuario=".$_SESSION["idusuario"]." AND
				mei_virgrupo.idvirmateria=".$_GET["idmateria"];
		}
		else
		{
			$sql = "SELECT mei_grupo.idgrupo, mei_grupo.nombre FROM `mei_grupo`, mei_relusugru
				WHERE mei_grupo.idgrupo = mei_relusugru.idgrupo AND
				mei_relusugru.idusuario=".$_SESSION["idusuario"]." AND
				mei_grupo.idmateria=".$_GET["idmateria"];
		}
		$resultado = $baseDatos->ConsultarBD($sql);

		while ( list ($idgrupo, $grupo) = mysql_fetch_row($resultado) )
		{
			print "<option value = '$idgrupo-$grupo'";
				if ( $_POST["cbo_orden"] == ($idgrupo."-".$grupo))
					print " selected>";
				else print ">";
					print "$grupo</option>";
		}

?>
            </select>          </td>
        </tr>
      </table>
     </form>
<?
}//FIN SELECCION DEL GRUPO
function verEvaluacion($sql, $idgrupo, $idbot)
{//FUNCION verActividad
	global $baseDatos;
	$resevaluacion = $baseDatos->ConsultarBD($sql);

	if ( mysql_num_rows($resevaluacion)>0 )
	{//if resevaluacion

		$sql = "SELECT DISTINCT(mei_evadtlleprevio.idprevio) FROM mei_evadtlleprevio UNION SELECT DISTINCT(mei_evadtllequiz.idprevio) FROM mei_evadtllequiz";
		//print $sql."<p>";
		$evadtlle = $baseDatos->ConsultarBD($sql);
		$i = 0;
		while ( list($id) = mysql_fetch_row($evadtlle) )
		{
			$idlista[$i] = $id;
			$i++;
		}

		if( count($idlista) > 0 )
			$previoLista = implode(',', $idlista);
		else $previoLista = 0;

		//print $previoLista."<p>";


		if ($_SESSION['idtipousuario']==1 || $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{//USUARIO ADMINISTRADOR
			$i = 0;
			if ($_SESSION['idtipousuario']==5)
			{
				$sql = "SELECT mei_evaprevio.idprevio, mei_evaprevio.visibilidad,mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, mei_evaprevio.fechafinalizacion,
				mei_evaprevio.estado, '-' FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo WHERE mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND
				mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evaprevio.idtipoprevio = 1 AND mei_evavirgrupo.idvirgrupo='".$idgrupo."'
				UNION  SELECT mei_evaquiz.idprevio, mei_evaquiz.visibilidad,mei_evaquiz.titulo, mei_evaquiz.fechaactivacion, mei_evaquiz.fechafinalizacion,
				mei_evaquiz.estado, 'reposicion' FROM mei_evaquiz, mei_evaluacion, mei_evavirgrupo WHERE mei_evaquiz.idevaluacion = mei_evaluacion.idevaluacion AND
				mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evaquiz.idtipoprevio = 2 AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."'";
			}
			else
			{
				$sql = "SELECT mei_evaprevio.idprevio, mei_evaprevio.visibilidad,mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, mei_evaprevio.fechafinalizacion,
				mei_evaprevio.estado, '-' FROM mei_evaprevio, mei_evaluacion, mei_evagrupo WHERE mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND
				mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evaprevio.idtipoprevio = 1 AND mei_evagrupo.idgrupo = '".$idgrupo."'
				UNION  SELECT mei_evaquiz.idprevio, mei_evaquiz.visibilidad,mei_evaquiz.titulo, mei_evaquiz.fechaactivacion, mei_evaquiz.fechafinalizacion,
				mei_evaquiz.estado, 'reposicion' FROM mei_evaquiz, mei_evaluacion, mei_evagrupo WHERE mei_evaquiz.idevaluacion = mei_evaluacion.idevaluacion AND
				mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evaquiz.idtipoprevio = 2 AND mei_evagrupo.idgrupo = '".$idgrupo."' ORDER BY fechaactivacion ASC";
			}
		}//USUARIO ADMINISTRADOR
		else //mei_evaprevio.idprevio IN (".$previoLista.") AND
		{
			if ($_SESSION['idtipousuario']==6)
			{
				$sql = "SELECT mei_evaprevio.idprevio, mei_evaprevio.visibilidad, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, mei_evaprevio.fechafinalizacion,'-' FROM
				mei_evaprevio, mei_evaluacion, mei_evavirgrupo WHERE mei_evaprevio.idprevio IN (".$previoLista.") AND
				mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND
				mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_evaprevio.fechaactivacion<'".date('YmdHi')."' AND mei_evaprevio.estado=1 AND
				mei_evaprevio.fechafinalizacion>'".date('YmdHi')."' AND mei_evaprevio.idtipoprevio = 1
				UNION SELECT mei_evaquiz.idprevio, mei_evaquiz.visibilidad,mei_evaquiz.titulo, mei_evaquiz.fechaactivacion, mei_evaquiz.fechafinalizacion, 'reposicion' FROM
				mei_evaquiz, mei_evaluacion, mei_evavirgrupo WHERE mei_evaquiz.idprevio IN (".$previoLista.") AND
				mei_evaquiz.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND
				mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_evaquiz.fechaactivacion<'".date('YmdHi')."' AND mei_evaquiz.estado=1 AND
				mei_evaquiz.fechafinalizacion>'".date('YmdHi')."' AND mei_evaquiz.idtipoprevio = 2";
			}
			else
			{
				$sql = "SELECT mei_evaprevio.idprevio ,mei_evaprevio.visibilidad, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, mei_evaprevio.fechafinalizacion,'-' FROM
				mei_evaprevio, mei_evaluacion, mei_evagrupo WHERE mei_evaprevio.idprevio IN (".$previoLista.") AND
				mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND
				mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_evaprevio.fechaactivacion<'".date('YmdHi')."' AND mei_evaprevio.estado=1 AND
				mei_evaprevio.fechafinalizacion>'".date('YmdHi')."' AND mei_evaprevio.idtipoprevio = 1
				UNION SELECT mei_evaquiz.idprevio, mei_evaquiz.visibilidad,mei_evaquiz.titulo, mei_evaquiz.fechaactivacion, mei_evaquiz.fechafinalizacion, 'reposicion' FROM
				mei_evaquiz, mei_evaluacion, mei_evagrupo WHERE mei_evaquiz.idprevio IN (".$previoLista.") AND
				mei_evaquiz.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND
				mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_evaquiz.fechaactivacion<'".date('YmdHi')."' AND mei_evaquiz.estado=1 AND
				mei_evaquiz.fechafinalizacion>'".date('YmdHi')."' AND mei_evaquiz.idtipoprevio = 2";
			}
		}
			$resultado=$baseDatos->ConsultarBD($sql);
			/*print $sql."<p>";*/
			if(mysql_num_rows($resultado)> 0)
			{//if existen evaluaciones
	?>
	&nbsp;
	  <table class="tablaGeneral" width="100%" border="0">
		<tr class="trSubTitulo" align="center" valign="middle">
		  <td colspan="2" width="15%">Titulo</td>
		  <?
		  	if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
			{
		  ?>
		  <td width="23%">Fecha Activaci&oacute;n Evaluaci&oacute;n </td>
		  <?
		  	}
		  ?>
		  <td width="22%">Fecha Finalizaci&oacute;n Evaluaci&oacute;n </td>
		 <?php /*?> <td width="10%">Editar</td><?php mirar para incluirlo */?>
		  <?
		  	if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
			{
		  ?>
		  <td width="8%">Estado</td>
		  <td width="8%">Ver IP'S</td>
		  <?
		  	}
		  ?>
		  <td width="8%">Visible</td>
		</tr>
		<tr class="trListaClaro" align="center" valign="middle">
		  <td colspan="7">&nbsp;</td>
	    </tr>
	<?
				
				while ( list($idprevio, $visibilidad, $titulo, $fecha_a, $fecha_f, $estado, $esp) = mysql_fetch_row($resultado) )
				{//while 1
					if ( ($i%2)==0 ) $clase = "trListaOscuro";
					else $clase = "trInformacion";
					$j=2*$idbot;
					$k=(2*$idbot)+1;
					
					
					$sqlcongeva = "SELECT congelar FROM mei_usupreviodtlle WHERE idprevio='$idprevio' AND congelar=1";
					$resconge = $baseDatos->ConsultarBD($sqlcongeva);
					list($conge) = mysql_fetch_array($resconge);
	?>


		<tr class="<?=$clase?>" align="center" valign="middle">

		  <td width="20%">
		  	<a style="<?if(!empty($conge)){?>color:red;<?}?>" <?if(!empty($conge)){?>title="Previo Congelado"<?}?> href="verDtlleEvaluacion.php?idprevio=<?=$idprevio?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=
		  	<?=$idgrupo?>&esp=<?php if(isset($esp)) echo $esp; else echo $estado;?>&materia=<?=$_GET["materia"]?>" class="link">
		    <?=$titulo?>
		  	</a>
		  </td>

		  <td width="5%">
		  	<a href="verDtlleEvaluacion.php?idprevio=<?=$idprevio?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=
		  	<?=$idgrupo?>&esp=<?php if(isset($esp)) echo $esp; else echo $estado;?>&materia=<?=$_GET["materia"]?>" class="link">
		  	<?
		  		if (@!in_array($idprevio,$idlista))
				{
		  	?>
            		<img src="imagenes/error.gif" alt="Este Previo No tiene Asignadas Preguntas" width="17" height="18" border="0">
            <?
		  		}
		  	?>
		  	</a>
		  </td>

		  <?
		  	if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
			{
		  ?>
		  <td><?=mostrarFechaTexto($fecha_a, 1)?></td>
		  <?
		  	}
		  ?>
		  <td><?=mostrarFechaTexto($fecha_f,1)?></td>


		  <?

		  	if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
			{

		  ?>

		  <td><?
		  		if ($estado == 0)
				{
					
			?>
		    <input type="image" id="<?=$j?>" value="imagenes/inactivo.gif" src="imagenes/inactivo.gif"  onclick="cambiarestado(<?=$idprevio?>,this.id,this.value)" />
			<?
				}
				else
				{
			?>
			<input type="image" id="<?=$j?>" value="imagenes/activo.gif" src="imagenes/activo.gif"  onclick="cambiarestado(<?=$idprevio?>,this.id,this.value)" />
			<?
				}
			?>&nbsp;
			</td>

			<!-- ________________________________ cambios nuevo codigo domingo 01 mayo 2016 __________________________________________ -->
<?php
$id_int = 1;
?>			
			<td>
   			<a href="ipGrupo.php?idGrupo=<? print $idgrupo?>&idPrevio=<? print $idprevio?>&idInt=<? print $id_int?>" rel="lightbox_text">
			<img src="imagenes/ip.gif"  width="16" height="16" border="0">
			</a>
			</td>
<!-- ________________________________ cambios nuevo codigo domingo 01 mayo 2016 __________________________________________ -->
			<td>		  	
				<?
					if ($visibilidad == 0)
					{
				?>
				<input type="image" id="<?=$k?>" value="imagenes/activo.gif" src="imagenes/activo.gif"  onclick="cambiarvisibilidad(<?=$idprevio?>,this.id,this.value)" />
				<?
					}
					else
					{
				?>
				<input type="image" id="<?=$k?>" value="imagenes/inactivo.gif" src="imagenes/inactivo.gif"  onclick="cambiarvisibilidad(<?=$idprevio?>,this.id,this.value)" />				
				<?
					}
				?>&nbsp;			  
			</td>		
		  <?
		  	}
		  ?>
		</tr>
	<?
					$idbot++;
					
				}//fin while 1
	?>
		<tr class="trListaClaro" align="center" valign="middle">
		  <td colspan="7">&nbsp;</td>
	    </tr>
	  </table>

	<?	    return $idbot;
			}//fin if existen evaluaciones
			else
			{
	?>&nbsp;
<?if($_SESSION['idtipousuario']==2){?>
<table class="tablaGeneral" width="200" border="0">
				<tr class="tablaMarquesina">
					<td align="center"><? print "No existe ninguna Evaluación definida para este Grupo";?></td>
				</tr>
	  </table>
	<?}
			}
	}//FIN $resevaluacion
	else
	{//ELSE $resevaluacion
	?>&nbsp;<?if($_SESSION['idtipousuario']==2){?>
			<table class="tablaGeneral" width="200" border="0">
				<tr class="tablaMarquesina">
					<td align="center"><? print "No existe ninguna Evaluación definida para este Grupo";?></td>
				</tr>
			</table>
<?}?>
	<?
		}//FIN ELSE $resactividad*/
}//FIN FUNCION verActividad

if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
{//USUARIO ADMINISTRADOR

	if ( empty($_POST["cbo_orden"]) ) $_POST["cbo_orden"] = "*";

	if ($_POST["cbo_orden"] == "*")
	{
		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_virgrupo.idvirgrupo, mei_virgrupo.nombre FROM `mei_virgrupo`, mei_relusuvirgru
				WHERE mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo AND
				mei_relusuvirgru.idusuario=".$_SESSION["idusuario"]." AND
				mei_virgrupo.idvirmateria=".$_GET["idmateria"];
		}
		else
		{
			$sql = "SELECT mei_grupo.idgrupo, mei_grupo.nombre FROM `mei_grupo`, mei_relusugru
				WHERE mei_grupo.idgrupo = mei_relusugru.idgrupo AND
				mei_relusugru.idusuario=".$_SESSION["idusuario"]." AND
				mei_grupo.idmateria=".$_GET["idmateria"];
		}

		$resultado = $baseDatos->ConsultarBD($sql);
		
		while ( list($idgrupo, $grupo) = mysql_fetch_row($resultado) )
		{
			if ($_SESSION['idtipousuario']==5)
			{
				$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo WHERE mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion=mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo=".$idgrupo." UNION SELECT mei_evaquiz.idevaluacion FROM mei_evaquiz, mei_evaluacion, mei_evavirgrupo WHERE mei_evaquiz.idevaluacion=mei_evaquiz.idevaluacion  AND mei_evaquiz.idevaluacion=mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo=".$idgrupo;
			}
			else
			{
				$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evagrupo WHERE mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion=					                                                                                                                 mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo=".$idgrupo." UNION SELECT mei_evaquiz.idevaluacion FROM mei_evaquiz, mei_evaluacion, mei_evagrupo WHERE mei_evaquiz.idevaluacion=mei_evaquiz.idevaluacion  AND mei_evaquiz.idevaluacion=mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo=".$idgrupo;
			}
	?>&nbsp;
		<table class="tablaMarquesina" width="100%" border="0">
			<tr class="trTitulo">
			  <td width="10%" class="trOscuro">Grupo:</td>
			  <td width="90%" class="trOscuro"><?=$grupo?></td>
			</tr>
		</table><?

		   $idbot=verEvaluacion($sql, $idgrupo, $idbot);
		}
	}
	else
	{
		list ($idgrupo, $grupo) = explode("-", $_POST["cbo_orden"]);
		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo WHERE mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion=mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo=".$idgrupo." UNION SELECT mei_evaquiz.idevaluacion FROM mei_evaquiz, mei_evaluacion, mei_evavirgrupo WHERE mei_evaquiz.idevaluacion=mei_evaquiz.idevaluacion AND mei_evaquiz.idevaluacion=mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo=".$idgrupo;
		}
		else
		{
			$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evagrupo WHERE mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion=mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo=".$idgrupo." UNION SELECT mei_evaquiz.idevaluacion FROM mei_evaquiz, mei_evaluacion, mei_evagrupo WHERE mei_evaquiz.idevaluacion=mei_evaquiz.idevaluacion AND mei_evaquiz.idevaluacion=mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo=".$idgrupo;
		}
		//print "<p>$sql<p>";
		verEvaluacion($sql, $idgrupo);
	}
/*print "<script>window.alert('".$_POST["cbo_orden"]."')</script>";*/
?>
&nbsp;
<table class="tablaMarquesina" width="100%" border="0">
  <tr>
    <td width="33%" align="center" valign="middle">
<a href="ingresarEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_POST["cbo_orden"]?>" class="link"><img src="imagenes/ver.gif" alt="Taller" width="16" height="16" border="0"> Crear Evaluaci&oacute;n </a></td>
	 <?if($_SESSION['idtipousuario']==5){?>	
	 <td width="33%" align="center" valign="middle">
	<a href="ingresarQuizReposicion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagenes/ver.gif" alt="Taller" width="16" height="16" border="0">Crear Previo Reposici&oacute;n</a></td>
     <?}else{?>
     	<td width="33%" align="center" valign="middle">
	<a href="ingresarQuizReposicion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagenes/ver.gif" alt="Taller" width="16" height="16" border="0">Crear Quiz Reposici&oacute;n</a></td>
     	<?}?>
     <!--	<td width="33%" align="center" valign="middle"><a href="evaluacionAleatoria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagenes/ver.gif" alt="Taller" width="16" height="16" border="0">Crear Evaluac&oacute;n Aleatoria</a> </td>-->
    <td width="34%" align="center" valign="middle"><a href="../autoevaluaciones/crearAutoevaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagenes/usuarios.gif" width="16" height="16" border="0">Crear Autoevaluaciones</a></td>
  	<?if($_SESSION['idtipousuario']==6 ||$_SESSION['idtipousuario']==3){?>
  		<td width="34%" align="center" valign="middle"><a href="../autoevaluaciones/crearAutoevaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagenes/usuarios.gif" width="16" height="16" border="0">Crear Preguntas</a></td>
  	<?}?>
  </tr>
</table>
<?
}//FIN USUARIO ADMINISTRADOR
?>
<?
	if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
	{ //if idtipousuario = 2
		if ($_SESSION['idtipousuario']==6)
		{
			$sql = "SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo, mei_relusuvirgru WHERE mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo
				AND mei_relusuvirgru.idusuario = ".$_SESSION["idusuario"]." AND mei_virgrupo.idvirmateria=".$_GET["idmateria"];
		}
		else
		{
			$sql = "SELECT mei_grupo.idgrupo FROM mei_grupo, mei_relusugru WHERE mei_grupo.idgrupo = mei_relusugru.idgrupo
				AND mei_relusugru.idusuario = ".$_SESSION["idusuario"]." AND mei_grupo.idmateria=".$_GET["idmateria"];
		}
		$resultado = $baseDatos->ConsultarBD($sql);
		list ($idgrupo) = mysql_fetch_row($resultado);
		if ($_SESSION['idtipousuario']==6)
		{
			$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo WHERE mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion=mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo=".$idgrupo." UNION SELECT mei_evaquiz.idevaluacion FROM mei_evaquiz, mei_evaluacion, mei_evavirgrupo WHERE mei_evaquiz.idevaluacion=mei_evaquiz.idevaluacion AND mei_evaquiz.idevaluacion=mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo=".$idgrupo;
		}
		else
		{
			$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evagrupo WHERE mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion=mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo=".$idgrupo." UNION SELECT mei_evaquiz.idevaluacion FROM mei_evaquiz, mei_evaluacion, mei_evagrupo WHERE mei_evaquiz.idevaluacion=mei_evaquiz.idevaluacion AND mei_evaquiz.idevaluacion=mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo=".$idgrupo;
		}
		//print "<p>$sql<p>";
		verEvaluacion($sql, $idgrupo);
?>
&nbsp;
<table class="tablaMarquesina" width="100%" border="0">
  <tr>
    <td align="center" valign="middle"><a href="../autoevaluaciones/crearAutoevaluacion.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>" class="link"><img src="imagenes/usuarios.gif" width="16" height="16" border="0"> Crear Autoevaluaciones</a></td>
    <?if($_SESSION['idtipousuario']==6 ||$_SESSION['idtipousuario']==3){?>
  		<td width="34%" align="center" valign="middle"><a href="ingresarPregunta.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>&tema=<?echo $_POST["cbo_tema"];?> " class="link"><img src="imagenes/usuarios.gif" width="16" height="16" border="0">Crear Preguntas</a></td>
  	<?}?>
    </tr>
</table>
<?
	}//fin tipousuario = 2
?>
&nbsp;
<table class="tablaGeneral">
  <tr class="trSubTitulo" align="center" valign="middle">
    <td><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link">Volver</a></td>
  </tr>
</table>
</td>
	<td class="tablaEspacio">&nbsp;</td>
	<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
	<td class="tablaEspacioDerecho">&nbsp;</td>
</tr>
     </table>
</body>
</html>
<?
}//fin if comprobar sesion
?>