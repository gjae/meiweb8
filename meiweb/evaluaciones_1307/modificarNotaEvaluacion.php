<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	
	if(comprobarSession())
	{	
	$baseDatos= new BD();	
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraBasica' , '' );
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
function validarEntero(){ 
      //intento convertir a entero. 
     //si era un entero no le afecta, si no lo era lo intenta convertir 
     valor = parseInt(document.frm.cbo_calificacion.value) 

      //Compruebo si es un valor numérico       
	  if ( (isNaN(valor)) || (valor<0) ) { 
			//entonces (no es numero) devuelvo el valor cadena vacia 
			return false  
	  }else{ 
			//En caso contrario (Si era un número) devuelvo el valor 
			return true 
	  } 
	
}
function enviar()
{
	if (validarEntero())
	{
		document.frm.submit();
		window.close();
	}else alert("El campo 'Calificacion' debe ser un Número mayor a Cero")

}//
</script>
</head>
<body>
<form name="frm" method="post" action="eliminarIntento.php?accion=<?=$_GET["mod"]?>&idusuario=<?=$_GET["idusuario"]?>&idprevio=<?=$_GET["idprevio"]?>&idmateria=<?=$_GET["idmateria"]?>&intentos=<?=$_GET["intentos"]?>&cbo_intento=<?=$_GET["cbo_intento"]?>&idgrupo=<?=$_GET["idgrupo"]?>&esp=<?=$_GET["esp"]?>&materia=<?=$_GET['materia']?>" target="notasEvaluacion">
<table width="230" border="0" cellpadding="0" cellspacing="0">
  <tr class="trTitulo">
    <td colspan="2" align="center" valign="middle">Modificar Nota  </td>
  </tr>
  <tr class="trInformacion">
    <td colspan="2"><? //print "Nota: ".$_GET["nota"]?>
      &nbsp;</td>
  </tr>
  <tr class="trInformacion">
    <td width="20%">Calificacion: </td>
    <td width="80%"><input name="cbo_calificacion" type="text" id="bo_calificacion" size="10" maxlength="5"></td>
  </tr>
  <tr class="trInformacion">
    <td colspan="2">&nbsp;</td>
  </tr>
  <?
  	if($_GET['esp']!='reposicion')
	  	$sql = "SELECT mei_evaprevio.idtiposubgrupo FROM mei_evaprevio WHERE 
			mei_evaprevio.idprevio = '".$_GET["idprevio"]."'";
	else
	  	$sql = "SELECT mei_evaquiz.idtiposubgrupo FROM mei_evaquiz WHERE 
			mei_evaquiz.idprevio = '".$_GET["idprevio"]."'";
	
	$resultado = $baseDatos->ConsultarBD($sql);
	list($idtiposubgrupo) = mysql_fetch_row($resultado);
  	if (!empty($idtiposubgrupo))
	{
?>  
  <tr class="trInformacion">
    <td colspan="2"><label>
      <input name="chk_idsubgrupo" type="checkbox" id="chk_idsubgrupo" value="<?=$idtiposubgrupo?>">
    Asignar Nota a Todo el Subgrupo </label></td>
  </tr>
  <tr class="trInformacion">
    <td colspan="2">&nbsp;</td>
  </tr>
   <tr class="trInformacion">
					  	<td colspan="2"><div align="center">
                          <a>NOTA: Señor Docente al realizar este cambio en la notas, usted debe actualizar manualmente el hash para el control de seguridad e integridad de las notas.</a>
</div></td>
					  </tr>
  <?
  	}
  ?>
  <tr class="trSubTitulo">
    <td colspan="2" align="center" valign="middle"><label>
      <input type="button" name="Submit" value="Aceptar" onClick="javascript:enviar()">
    </label></td>
  </tr>
</table>
</form>
</body>
</html>
<?
}
else redireccionar('../login/');
?>