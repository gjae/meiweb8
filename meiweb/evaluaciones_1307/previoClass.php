<?

//CLASE PARA GUARDAR EL GRADO, TIPO Y NUMERO DE PREGUNTA QUE CONTENDRA UNA EVALUACION DE TIPO PREVIO
/*
class evaluacion{
	var $pregunta;
	
	function agregar($gr, $tp, $ct){
		$this->pregunta[$gr][$tp] = $ct;
	}
	
	function eliminar($gr, $tp, $ct)
	{
		if ( $this->pregunta[$gr][$tp] > $ct ){		
			$this->pregunta[$gr][$tp] -= $ct;
			return true;
		}
		else
		{
			 unset($this->pregunta[$gr][$tp]);
			 return false;
		}
	}
}
//FIN CLASS evaluacion
*/

//CLASE PREGUNTA
class preguntas{//se refiere a una pregunta 
	var $codpreg;
	var $txt_preg;
	var $tipo;
	var $txt_tipo;
	var $valor_preg;
	
	function llenar_preg($cod, $txt, $tipo, $txt_tipo, $valor){
		$this->codpreg = $cod;
		$this->txt_preg = $txt;
		$this->tipo = $tipo;
		$this->txt_tipo = $txt_tipo;
		$this->valor_preg = $valor;
	}	
}
//FIN CLASS pregunta

//CLASE RESPUESTA
class respuesta{
	var $codresp;
	var $txt_resp;
	var $valor_resp;
	
	function llenar_resp($cod, $txt, $valor){
		$this->codresp = $cod;
		$this->txt_resp = $txt;
		$this->valor_resp = $valor;
	}
}
//FIN CLASS respuesta

//CLASE PREVIO
class previo{
	var $idusuario;
	var $idsubgrupo;
	var $idprevio;
	var $idmateria;
	var $titulo;
	var $comentario;
	var $idintento;
	var $valorIntento;
	/*var $mPrevio;*/
	var $mNota;
	var $mCalificar;
	var $tiempoTotal;
	var $tiempoInicio;
	var $pregunta;
	var $resp_bd;
	var $resp_usu;
	
	function previo($id,$ids, $idprev, $idmat, $tit, $comen, $idintento, $valor/*, $mPrevio*/, $mNota, $mCalificar){
		$this->idusuario = $id;
		$this->idsubgrupo = $ids;
		$this->idprevio = $idprev;
		$this->idmateria = $idmat;
		$this->titulo = $tit;
		$this->comentario = $comen;
		$this->idintento = $idintento;
		$this->valorIntento = $valor;
		/*$this->mPrevio = $mPrevio;*/
		$this->mNota = $mNota;
		$this->mCalificar = $mCalificar;
	}

	function agregar_tiempo($tt, $ti)
	{
		$this->tiempoTotal = $tt;
		$this->tiempoInicio = $ti;
	}
	
	function agregar_preg($num, $codpreg, $txtpreg, $tipo, $txt_tipo, $valor){
		$this->pregunta[$num] = new preguntas;
		$this->pregunta[$num]->llenar_preg($codpreg, $txtpreg, $tipo, $txt_tipo, $valor);			
	}	
	
	function agrega_respbd($num, $i, $codresp, $txtresp, $valor){
		$this->resp_bd[$num][$i] = new respuesta;
		$this->resp_bd[$num][$i]->llenar_resp($codresp, $txtresp, $valor);
	}
	
	function agrega_respusu($num, $i, $codresp, $txtresp){
		$this->resp_usu[$num][$i] = new respuesta;
		$this->resp_usu[$num][$i]->llenar_resp($codresp, $txtrespm);
	}
	
	function destruir()
	{
		unset($this->idusuario);
		unset($this->idprevio);
		unset($this->idmateria);
		unset($this->idintento);
		unset($this->valorIntento);
		unset($this->tiempoTotal);
		unset($this->tiempoInicio);
		unset($this->pregunta);		
		unset($this->resp_bd);
	}
}
//FIN CLASS previo

?>