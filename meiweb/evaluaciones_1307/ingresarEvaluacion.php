<?PHP
	include_once ('../menu/Menu.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
if(@comprobarSession())
{
	$baseDatos= new BD();
	$editor=new FCKeditor('edt_comentario' , '100%' , '200' , 'barraBasica' , '' );
	$fecha_a=new FrmCalendario('frm_ingresarEvaluacion','txt_fecha_a','formulario','false',''); 
	$fecha_f=new FrmCalendario('frm_ingresarEvaluacion','txt_fecha_f','formulario','false','');
	
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
function cambiar(tec){
			for (var i=0; i < 20; i++) {
			  tec.value = tec.value.replace("-", "_");
			};
	
}	
function incentivo1 () {
 if(!document.frm_ingresarEvaluacion.incentivo_curva.checked){
 	document.frm_ingresarEvaluacion.incentivo_nota.checked=true;
 	document.frm_ingresarEvaluacion.incentivo_valor.disabled=false;
 }else{
 	document.frm_ingresarEvaluacion.incentivo_nota.checked=false;
 	document.frm_ingresarEvaluacion.incentivo_valor.disabled=true;
 }
}
function incentivo2 () {
 if(document.frm_ingresarEvaluacion.incentivo_nota.checked){
 	document.frm_ingresarEvaluacion.incentivo_curva.checked=false;
 	document.frm_ingresarEvaluacion.incentivo_valor.disabled=false;
 }else{
 	document.frm_ingresarEvaluacion.incentivo_curva.checked=true;
 	document.frm_ingresarEvaluacion.incentivo_valor.disabled=true;
 }
}
function validarEntero(){
      //intento convertir a entero.
     //si era un entero no le afecta, si no lo era lo intenta convertir
     valor = parseInt(document.frm_ingresarEvaluacion.txt_tiempo.value)
	 valor2 = parseInt(document.frm_ingresarEvaluacion.cbo_valorIntento.value)
      //Compruebo si es un valor numerico
      if ( ((isNaN(valor)) || (valor<1)) || ( (isNaN(valor2)) || (valor2<0) ) ) {
            //entonces (no es numero) devuelvo el valor cadena vacia
            return false
      }else{
            //En caso contrario (Si era un numero) devuelvo el valor
            return true
      }
}
function habilitar()

{

    if (document.frm_ingresarEvaluacion.cbo_estado == "1")

    {

    document.frm_ingresarEvaluacion.direccionIp1.disabled = true;

    }

    else

    {

    document.frm_ingresarEvaluacion.direccionIp1.disabled  = false;

    }

}



	function validar()
	{
		if ( document.frm_ingresarEvaluacion.txt_titulo.value!="" && document.frm_ingresarEvaluacion.txt_tiempo.value!="" && document.frm_ingresarEvaluacion.cbo_valorIntento.value!="")
		{
			if ( validarEntero() )
				return true;
			else alert("El campo 'Tiempo de la Evaluación' debe ser un número mayor o igual a 1. El campo 'Disminuir la Nota de cada intento' debe ser un número mayor a 0")
		}
		else
			{
				alert('Debe llenar todos los campos')
				return false;
			}
	}

	function verHora()
	{

		var fecha=frm_ingresarEvaluacion.txt_fecha_a.value.split("-");
		if(fecha[1]<10){ fecha[1]='0'+fecha[1];}
		if(fecha[2]<10){ fecha[2]='0'+fecha[2];}
		fecha=fecha[0]+"-"+fecha[1]+"-"+fecha[2];
		
		var fechaC=frm_ingresarEvaluacion.txt_fecha_f.value.split("-");
		if(fechaC[1]<10){ fechaC[1]='0'+fechaC[1];}
		if(fechaC[2]<10){ fechaC[2]='0'+fechaC[2];}
		fechaC=fechaC[0]+"-"+fechaC[1]+"-"+fechaC[2];
		
		// se pasan las fechas a formato aaaa-mm-dd hh:mm:ss
		var hora_a = document.frm_ingresarEvaluacion.cbo_hora_a.value;
		var min_a= document.frm_ingresarEvaluacion.cbo_minuto_a.value;
		var hora_f = document.frm_ingresarEvaluacion.cbo_hora_f.value; 
		var min_f= document.frm_ingresarEvaluacion.cbo_minuto_f.value;
		
			fecha=fecha+" "+hora_a+":"+min_a+":00";
			fechaC=fechaC+" "+hora_f+":"+min_f+":00";
			
			if(fecha<fechaC){
				
					return true;
				}
			else{
				alert("la fecha de Activacion debe ser menor q la fecha de Finalizacion");
				return false;
			}
	}


	 function guardar()
	{
		if (validar())
		{
			if (verHora())
			{
				//alert('nice')
				if(document.getElementById('txt_clave').value=='')
				{
					if(confirm("¿Está seguro de dejar en blanco el campo contraseña?"))
					{
						document.frm_ingresarEvaluacion.action="guardarEvaluacion.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>";
						document.frm_ingresarEvaluacion.submit();
					}
 				}

 				else
				{
					document.frm_ingresarEvaluacion.action="guardarEvaluacion.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>";
					document.frm_ingresarEvaluacion.submit();
 				}
			}
			else alert('La Hora de Finalizacion debe ser mayor que la Hora de Activación')

		}
	}
	
	function ver(estado)
	{
	var nca = new Object();
	nca = document.getElementById("nombre")

		if (estado == 'visible')
		{
			//Propiedades capa absoluta
			caw = nca.offsetWidth
			//*************************

			//Propiedades capa relativa
			crpx = ancla.offsetLeft;
			crpy = ancla.offsetTop;
			crh = ancla.offsetHeight;
			crw = ancla.offsetWidth
			//*************************
			//alert(crw)
			with (nca.style)
			{
				left = crpx + crw;
				top = crpy //+ crh
				visibility = estado;
			}
		}else nca.style.visibility = estado;
	}
	
    function generarautomatica(){
		if (validar())
		{
			if (verHora())
			{
				//alert('nice')
				if(document.getElementById('txt_clave').value=='')
				{
					if(confirm("¿Está seguro de dejar en blanco el campo contraseña?"))
					{
						document.frm_ingresarEvaluacion.action="guardarEvaluacion.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&automatica=default";
						document.frm_ingresarEvaluacion.submit();
					}
 				}

 				else
				{
					document.frm_ingresarEvaluacion.action="guardarEvaluacion.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET['materia']; ?>&automatica=default";
					document.frm_ingresarEvaluacion.submit();
 				}
			}
			else alert('La Hora de Finalizacion debe ser mayor que la Hora de Activación')

		}        
    }

	function cancelar(id, mat)
	{
		document.frm_ingresarEvaluacion.action="ingresarEvaluacion.php?idmateria="+id+"&materia="+mat;
		document.frm_ingresarEvaluacion.submit();
	}

	function enviar()
	{
		var sel = frm_1.cbo_tipoprevio.value.split("-")
		if (sel[0]==1)
			document.frm_1.submit()
		else
		{
			document.frm_1.action ="ingresarQuiz.php?materia=<?PHP echo $_GET["materia"]; ?>&idmateria=<?PHP echo $_GET["idmateria"]; ?>&cbo_orden=<? echo $_GET["cbo_orden"];?>"
			document.frm_1.submit()
		}
	}
    function cargarsubtema(idtema){
    	if( idtema != "" ){
    	  $.ajax({
    					data:  ({ idtema : idtema }),
    					url:   'cargarsubtemas.php',
    					type:  'post',
    					beforeSend: function () {
    							$("#subtemacarga").html("Procesando, espere por favor...");
    					},
    					success:  function (response) {
    							$("#subtema").html(response);
    							//document.getElementById("subtematr").style.display = 'table-row';
    							document.getElementById("subtemacarga").style.display = 'none';
    					}
    			});							
    	}	
    }
    function cantpreg(){
        var tema = document.getElementById("cbo_tema").value;
        var subtema = document.getElementById("cbo_sub_tema").value;
        var gradodif = document.getElementById("cbo_gradoP").value;   
        
        if( (tema != "")&&(gradodif != "") ){
    	  $.ajax({
    					data:  ({ tema : tema,subtema: subtema,gradodif: gradodif  }),
    					url:   'cantpreguntas.php',
    					type:  'post',
       					success:  function (response) {
    							$("#cantpreg").html(response);
    							document.getElementById("cantpregbtn").display = "none";
    					}
    			}); 
       } 
       else{ alert("Llenar todos los campos por favor"); }           
    }
    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " 1234567890";
        especiales = [8, 37, 39, 46];
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
         {   
    		alert("Digite solo numero");
    		return false;	 
    	 }
    }   
    function guardarpreguntas(){
        var tema = document.getElementById("cbo_tema").value;
        var subtema = document.getElementById("cbo_sub_tema").value;
        var gradodif = document.getElementById("cbo_gradoP").value;
        var cantidadpreguntas = parseInt(document.getElementById("cantipreguntas").value);
        var totalpreguntas = parseInt(document.getElementById("totalpreguntas").value);
        var arrpreguntas = document.getElementById("arrpreguntas").value;
        
        if(subtema == ""){
            subtema = "general";
        }
       if( (gradodif != "")&&(cantidadpreguntas != "")&&(tema != "")&&(cantidadpreguntas != "") ){
           if(totalpreguntas == 0){
                document.getElementById("totalpreguntas").value = cantidadpreguntas+' Preguntas Agregadas';
            }
            else{
                totalpreguntas = totalpreguntas+cantidadpreguntas;
                document.getElementById("totalpreguntas"). value = totalpreguntas+' Preguntas Agregadas';
            }
            
            if(arrpreguntas == ""){
                arreglo = tema+","+subtema+","+gradodif+","+cantidadpreguntas;
                document.getElementById("arrpreguntas").value = arreglo;
            }
            else{
                arreglo = "/"+tema+","+subtema+","+gradodif+","+cantidadpreguntas;
                arrpreguntas = arrpreguntas+arreglo;
                document.getElementById("arrpreguntas").value = arrpreguntas;
            }
            alert("Ha agregado "+cantidadpreguntas+" preguntas");
       }
       else{ alert("Por favor llenar todos los campos de preguntas aleatorias"); }
    }
    
    
         
</script>
</head>
<body>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><?PHP  menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
	      <td valign="top" class="tdGeneral">
          	<table class="tablaMarquesina">
              <tr>
<?PHP 	 				
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($mat) = mysql_fetch_row($resultado); 
?>	  
          		<td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link"><?PHP echo ucwords(strtolower($mat))?></a><a> -> </a><a href="verEvaluacion.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link">Evaluaciones</a><a> -> Ingresar Previo</a></td>
              </tr>
            </table>&nbsp;
<?PHP 
	if ($_GET['estado']==3)
	{ //If estado 3
		//list($idgrupo, $grupo) = explode("-", $_POST["hid_grupo"]);
		list($idtipoprevio, $tipoprevio) = explode('-',$_POST["cbo_tipoprevio"]);

		list ($idnota, $nota) = explode("-", $_POST["hid_nota"]);
		//******************GRUPO*************************
		if (!empty($_POST["cbo_grupo"]))
			$opcion = $_POST["cbo_grupo"];
		else $opcion = $_POST["hid_grupo"];

		list($idgrupo, $grupo) = explode("-", $opcion);
		//************************************************
		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo
				WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND
				mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."'";
		}
		else
		{
			$sql = "SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evagrupo
				WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND
				mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'";
		}
		//print $sql."<p>";
		$resultado = $baseDatos->ConsultarBD($sql);
		$i=0;
		while ( list($id) = mysql_fetch_row($resultado) )
		{
			$veva[$i] = $id;
			$i++;
		}
		if ( count($veva)>0 )
			$ideva_ex = implode(',', $veva);
		else $ideva_ex = 0;
		//print $ideva_ex."<p>";

		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval FROM mei_evaluacion, mei_evavirgrupo
				WHERE mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo='".$idgrupo."' AND
				mei_evaluacion.tipoevaluacion='1' AND mei_evaluacion.idevaluacion NOT IN (".$ideva_ex.")";
		}
		else
		{
			$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval FROM mei_evaluacion, mei_evagrupo
				WHERE mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo='".$idgrupo."' AND
				mei_evaluacion.tipoevaluacion='1' AND mei_evaluacion.idevaluacion NOT IN (".$ideva_ex.")";
		}
		//print $sql."<p>";
		$resultado = $baseDatos->ConsultarBD($sql);
		
		//_______________________________________________________________________
		
		if(mysql_num_rows($resultado)==0){
			
			
			$sql = "SELECT max(mei_evaluacion.idevaluacion) FROM mei_evaluacion";
					$resultado = $baseDatos->ConsultarBD($sql);
					list($ideva) = mysql_fetch_row($resultado);
			
			if ( $ideva == 0 )
				$ideva = 1;
			else $ideva++;		
			
			$sql="INSERT INTO mei_evaluacion (`idevaluacion`, `tipoevaluacion`, `idautor`, `nombre`, `valor_eval`, `promediar`)
					VALUES ('".$ideva."',1, '".$_SESSION["idusuario"]."','PrevioQuiz',0,0)";
					$baseDatos->ConsultarBD($sql);
			$sql="INSERT INTO mei_evagrupo (`idgrupo`, `idevaluacion`)
					VALUES ('".$idgrupo."','".$ideva."')";
					$baseDatos->ConsultarBD($sql);
			
			if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo
				WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND
				mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."'";
		}
		else
		{
			$sql = "SELECT mei_evaprevio.idevaluacion FROM mei_evaprevio, mei_evaluacion, mei_evagrupo
				WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND
				mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'";
		}
		//print $sql."<p>";
		$resultado = $baseDatos->ConsultarBD($sql);
		$i=0;
		while ( list($id) = mysql_fetch_row($resultado) )
		{
			$veva[$i] = $id;
			$i++;
		}
		if ( count($veva)>0 )
			$ideva_ex = implode(',', $veva);
		else $ideva_ex = 0;
		//print $ideva_ex."<p>";

		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval FROM mei_evaluacion, mei_evavirgrupo
				WHERE mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo='".$idgrupo."' AND
				mei_evaluacion.tipoevaluacion='1' AND mei_evaluacion.idevaluacion NOT IN (".$ideva_ex.")";
		}
		else
		{
			$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval FROM mei_evaluacion, mei_evagrupo
				WHERE mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo='".$idgrupo."' AND
				mei_evaluacion.tipoevaluacion='1' AND mei_evaluacion.idevaluacion NOT IN (".$ideva_ex.")";
		}
		//print $sql."<p>";
		$resultado = $baseDatos->ConsultarBD($sql);
		}
		
		//__________________________________________________________________________________________

		if ( mysql_num_rows($resultado) > 0 ) 
		{//IF RESULTADO
			$sql = "SELECT * FROM mei_tiposubgrupo";
			$resultado2 = $baseDatos->ConsultarBD($sql);
?>
<form enctype="multipart/form-data" name="frm_ingresarEvaluacion" method="post" action="">
	
	<!--<div id="nombre" align="center" style="background-color:#FFFFFF; BORDER-RIGHT: 2px ridge;
	BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge;
	WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute; width:450; height:auto;">

	<div align="center" style="height:10px" class="trTitulo">Adjuntar:</div>
	  <table class="tablaPrincipal" width="417" height="auto">
        <tr class="trInformacion">
          <td width="20%">Archivo 1: </td>
          <td width="80%">
          	<input name="fil_archivo1" type="file" size="40"></td>
        </tr>
         <tr  class="trListaOscuro">
          <td>Archivo 2: </td>
          <td><input name="fil_archivo2" type="file" size="40"></td>
        </tr>
        <tr class="trInformacion">
          <td>Archivo 3:</td>
          <td><input name="fil_archivo3" type="file" size="40"></td>
        </tr>
        <tr  class="trListaOscuro">
          <td>Archivo 4:</td>
          <td><input name="fil_archivo4" type="file" size="40"></td>
        </tr>
        <tr  class="trInformacion">
          <td>Archivo 5:</td>
          <td><input name="fil_archivo5" type="file" size="40"></td>
        </tr>
        <tr  height="10px">
        <td colspan="2" align="center" >	
	  <a href="javascript:ver('hidden')" class="link" ><h2>Cargar Archivos</h2></a>
    	</td>
        </tr>
      </table>
	
</div>
<!---->
	
  <table width="100%" class="tablaGeneral">
    <tr class="trSubTitulo">
      <td width="20%" align="left" valign="middle"><b>Autor:</b></td>
      <td width="80%" colspan="3"><input name="hid_materia" type="hidden" id="hid_materia" value="<?PHP echo $_POST["hid_materia"]; ?>">
        <input name="hid_tipoprevio" type="hidden" id="hid_tipoprevio" value="<?PHP echo $idtipoprevio?>">
          <input name="hid_grupo" type="hidden" id="hid_grupo" value="<?PHP echo $idgrupo?>">
          <?PHP echo  $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']; ?></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Grupo:</b></td>
      <td><?PHP echo $grupo?></td>
      <td><strong>Subgrupo:</strong></td>
      <td><select name="cbo_subgrupo" id="cbo_subgrupo">
        <option value="NULL">Previo No Grupal</option>
        <?PHP 
							while ( list($idtipo, $subgrupo) = mysql_fetch_row($resultado2) )
							{
								print "<option class='link' value='$idtipo' ";
								if ( $idtipo == $_POST["hid_subgrupo"] )
									print "selected>";
								else print ">";
								print "$subgrupo</option>";
							}
						?>
      </select></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Nota a la que pertenece:</b> </td>
      <td colspan="3"><select name="cbo_nota" onChange="javascript:document.frm_2.submit()">
          <?PHP 
									while ( list($ideva, $nomeva, $val_eval) = mysql_fetch_row($resultado) )
									{//WHILE RESULTADO
										print "<option class='link' value='$ideva' ";
										if ( $ideva == $_POST["hid_nota"] )
											print "selected>";
										else print ">";
										print "$nomeva --- ".($val_eval*100)."%</option>";
									}//FIN WHILE RESULTADO
							  ?>
      </select></td>
    </tr>
    <tr class="trInformacion" >
      <td align="left" valign="middle"><b>Nombre del Previo:  </b></td>
      <td colspan="3"><input name="txt_titulo" type="text" id="txt_titulo" size="80" onChange="cambiar(this)"></td>
    </tr>
    <tr class="trInformacion">
      <td width="20%" align="left" valign="middle"><b>Fecha Activaci&oacute;n:</b> </td>
      <td width="40%"><?PHP 
						/*if(empty($_POST['hid_fecha']))
							$fecha_a->m_valor='';
						else
							$fecha_a->m_valor=$_POST['hid_fecha'];*/
							$fecha_a->CrearFrmCalendario();
						?></td>
      <td width="10%"><b>Hora Activaci&oacute;n:</b> </td>
      <td width="30%" ><select name="cbo_hora_a" id="cbo_hora_a">
         <?
          	$sqlho="SELECT horario FROM mei_grupo WHERE idgrupo='".$idgrupo."'";
			$horar=$baseDatos->ConsultarBD($sqlho);
			list($horario)=mysql_fetch_row($horar);
			$horas=explode(" ", $horario);
          	$horain=explode(":", $horas[2]);
			$horafi=explode(":", $horas[5]);
			$tiempofin=explode(",", $horas[6]);
			$tiempo="pm";
			if(strcmp($tiempo, $horas[3]) === 0){
				$horain[0]=$horain[0]+12;
			}
			if(strcmp($tiempo, $tiempofin[0]) === 0){
				$horafi[0]=$horafi[0]+12;
			}
			
			for ($i=0;$i<=23;$i++)
			{
				
				if($horain[0]==$i){
					if ($i<10) $i = "0".$i;
					print "<option selected value='$i'>$i</option>";
				}else{
					if ($i<10) $i = "0".$i;
				print "<option value='$i'>$i</option>";
				}
			}
		  ?>
        </select>
        :
  <select name="cbo_minuto_a" id="cbo_minuto_a">
    <?PHP 
		for ($i=0;$i<=55;$i=$i+5)
		{
			if ($i<10) $i = "0".$i;
			print "<option value='$i'>$i</option>";
		}
	?>
</select></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Fecha Finalizaci&oacute;n:</b> </td>
      <td><?PHP 
								/*if(empty($_POST['hid_fecha']))
									$fecha_f->m_valor='';
								else
									$fecha_f->m_valor=$_POST['hid_fecha'];*/
									$fecha_f->CrearFrmCalendario();
								?></td>
      <td><b>Hora Finalizaci&oacute;n:</b> </td>
      <td><select name="cbo_hora_f" id="select2">
          <?
								for ($i=0;$i<=23;$i++)
			{
				
				if($horafi[0]==$i){
					if ($i<10) $i = "0".$i;
					print "<option selected value='$i'>$i</option>";
				}else{
					if ($i<10) $i = "0".$i;
				print "<option value='$i'>$i</option>";
				}
			}
							?>
        </select>
        :
  <select name="cbo_minuto_f" id="select3">
    <?PHP 
		for ($i=0;$i<=55;$i=$i+5)
		{
			if ($i<10) $i = "0".$i;
			print "<option value='$i'>$i</option>";
		}
	?>
</select></td>
    </tr>

    <tr class="trInformacion">
      <td height="24" align="left" valign="middle"><b>Estado:</b></td>
      <td colspan="3"><select name="cbo_estado" id="cbo_estado" class="link"  >
                            <option value="1">Activa</option>
                            <option value="0" selected>Inactiva</option>
                          </select></td></tr>
   <?PHP  /*?> <tr class="trInformacion">
      <td height="24" align="left" valign="middle"><b>Clave:</b></td>
       <td ><input name="txt_clave" type="text" id="txt_clave" size="30" value=""></td>
    </tr><?PHP  */?>
	<?PHP  /*?> cambiarlo de sitio para que no se ingrese en cada evaluacion<tr><td height="24" align="left" valign="middle" ><b> Rango Direccion Ip</b></td>
		    <td>
				  <input name="direccionIp1" id="direccionIp1" type="text" value="direccionIp1" > </td>
				  <td><input name="direccionIp2" id="direccionIp2" type="text" value="direccionIp2">

					   </td>
    </tr><?PHP  */?>
    <tr class="trInformacion">
      <td height="24" align="left" valign="middle"><b>Comentarios:</b></td>
      <td colspan="3"><?PHP 
								 if (!empty($_POST["hid_editor"]) )
								 	$editor->Value = $_POST["hid_editor"];
								 $editor->crearEditor();
							?></td>
    </tr>
	 <tr class="trInformacion">
	<td height="24" align="left" valign="middle"><b> Contraseña: </b> </td>
	<td colspan="3"><input name="txt_clave" type="text" id="txt_clave" size="30" value=""></td>
    </tr>
	
	<!--Adjuntar-->
	<tr class="trInformacion">
	<td height="10" align="left" valign="middle"></td>
	<td colspan="3"></td>
    </tr>
	<tr class="trInformacion">
                          <td colspan="4"><div id="ancla" style="position:relative; width:250px">
						  <a style="color: red; font-weight: bold; font-size: 14px;" href="javascript:ver('visible')" class="link"><img src="imagenes/adjuntar.gif" width="16" height="16" border="0">Adjuntar Archivos </a>
						  </div>						  </td>
						</tr>
						
	 <tr class="trInformacion">
          <td width="20%">Archivo 1: </td>
          <td colspan="3" width="80%">
          	<input name="fil_archivo1" type="file" size="40"></td>
        </tr>
         <tr  class="trListaOscuro">
          <td>Archivo 2: </td>
          <td colspan="3"><input name="fil_archivo2" type="file" size="40"></td>
        </tr>
        <tr class="trInformacion">
          <td>Archivo 3:</td>
          <td colspan="3"><input name="fil_archivo3" type="file" size="40"></td>
        </tr>
        <tr  class="trListaOscuro">
          <td>Archivo 4:</td>
          <td colspan="3"><input name="fil_archivo4" type="file" size="40"></td>
        </tr>
        <tr  class="trInformacion">
          <td>Archivo 5:</td>
          <td colspan="3"><input name="fil_archivo5" type="file" size="40"></td>
        </tr>
	<tr class="trInformacion">
	<td height="10" align="left" valign="middle"></td>
	<td colspan="3"></td>
    </tr>
	<!--Fin Adjuntar-->
	
	
	
    <tr  class="trInformacion">
		<td height="24" align="left" valign="middle"><b> Publicar en Calendario </b> </td>
        <td colspan="3"  height="24" align="left" valign="middle">
         <input name="chk_calendario" type="checkbox" id="chk_calendario" value="checkbox" checked>
		</td>
	</tr>
    
   
  </table>&nbsp;

  <table class="tablaGeneral">
    <tr class="trSubTitulo">
      <td colspan="4">Caracteristicas del Previo: </td>
    </tr>
    <tr class="trInformacion">
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><b>N&uacute;mero de Intentos:</b> </td>
      <td align="left" valign="middle"><select name="cbo_intentos" id="cbo_intentos" >
        <?PHP 

			for ($i=1;$i<=10;$i++)
			{
				print "<option value='$i'>$i</option>";
			}
		?>
        </select></td>
      <td align="left" valign="middle"><strong>Disminuir la Nota de cada intento en: </strong></td>
      <td align="left" valign="middle"><input name="cbo_valorIntento" type="text" id="bo_calificacion" size="10" maxlength="5" value="10">
        %</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Forma de Calificar: </strong></td>
      <td align="left" valign="middle"><select name="cbo_calificar" id="cbo_calificar" >
	  <?PHP 
	  	$sql = "SELECT mei_evamodcalificar.idmodcalificar, mei_evamodcalificar.modcalificar FROM mei_evamodcalificar";
		$resultado = $baseDatos->ConsultarBD($sql);

		while ( list($idmod, $mod) = mysql_fetch_row($resultado) )
		{
			if ($idmod==3) {
				print "<option selected value = '$idmod'>$mod</option>'";
			} else {
				print "<option value = '$idmod'>$mod</option>'";
			}
		}

	  ?>
      </select></td>
      <td align="left" valign="middle"><b>Tiempo del Previo: </b></td>
      <td align="left" valign="middle"><input name="txt_tiempo" type="text" id="txt_tiempo" size="10" value="60">
	  <!--
	  <select name="cbo_tiempo" id="cbo_tiempo">
        <?PHP  /*
			for ($i=15;$i<=120;$i=$i+15)
			{
				if ($i<10) $i = "0".$i;
				print "<option value='$i'>$i Minutos</option>";
			}*/
		?>
      </select>--> Minutos	  </td>
    </tr>

    <tr class="trInformacion">
      <td align="left" valign="middle"><b>Barajar Preguntas:</b></td>
      <td align="left" valign="middle"><select name="cbo_bpreguntas" id="cbo_bpreguntas">
        <option value="1" selected>Si</option>
        <option value="0" >No</option>
      </select></td>
      <td align="left" valign="middle"><b>Barajar Respuestas:</b></td>
      <td align="left" valign="middle"><select name="cbo_brespuestas" id="cbo_brespuestas">
        <option value="1" selected>Si</option>
        <option value="0" >No</option>
      </select></td>
    </tr>

    <tr class="trInformacion">
      <td height="21"><strong>Mostrar Nota: </strong></td>
      <td><select name="cbo_mnota" id="cbo_mnota">
        <option value="1" selected>Si</option>
        <option value="0" >No</option>
      </select>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      <tr class="trInformacion">
                          <td align="left" valign="middle"><h2 style="color: #ec7000"><strong>Otorgar Incentivo </h2></strong></td>
                          <td><strong>Otorgar Curva Automatica</strong> 
                          	<input type="checkbox" value="curva" name="incentivo_curva"  onclick="incentivo1()" />
                          	</td>
                          <td><strong>Nota Adicional</strong> 
                          	<input type="checkbox"  name="incentivo_nota"  checked onclick="incentivo2()" value="nota"/>
                          	</td>
                          <td>
                          	<strong>Nota</strong> 
	                          	<select  name="incentivo_valor"  >
	                          		<?
	                          			$incentivo=0.0;
	                          		for ($i=0.0;$i<=5.0;$i=$i+0.1)
										{
											$x=$i;
											print "<option value='$x'";
											if ($incentivo==$x) print " selected>";
											else print ">";
											print $x."</option>";
										}?>
	                          		
	                          	</select> 
                          	</td>
                        </tr>
    <tr class="trInformacion">
      <td height="21">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
<br/>
  <table class="tablaGeneral">
    <tr class="trSubTitulo">
      <td colspan="4">Preguntas Aleatoria </td>
    </tr>
    <tr class="trInformacion">
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Tema :</strong></td>
      <td align="left" valign="middle">
        <select name="cbo_tema" id="cbo_tema" onchange="cargarsubtema(this.value); this.selectedindex = -1" >
            <option value=""></option>
            <?
    	  	$sql = "SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema 
    				WHERE mei_tema.idmateria='".$_GET["idmateria"]."' AND mei_tema.tipo=1";
    		$resultado = $baseDatos->ConsultarBD($sql);
    		while ( list($codtema, $nomtema) = mysql_fetch_row($resultado) )		
    		{	
    			print "<option value='$codtema'";
    			
    			if ("$codtema-$nomtema" == $_SESSION['tema'])
    					print "selected";
    			print  ">$nomtema</option>";			
    		}
    		mysql_free_result($resultado);
    	  ?>
        </select>
        <div id="subtemacarga"></div>      
      </td>
      <td align="left" valign="middle"><strong>Subtema :</strong></td>
      <td align="left" valign="middle">
        <div id="subtema"></div>     
      </td>            
    </tr> 
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Grado : </strong></td>
      <td align="left" valign="middle">
        <select name="cbo_gradoP" id="cbo_gradoP">
            <option value=""></option>
            <option value="0-Aleatorio">Aleatorio</option>
          <?
    	  	$sql = "SELECT * FROM mei_evagradopregunta";
    		$resultado = $baseDatos->ConsultarBD($sql);
    		while ( list($codgrado, $nomgrado) = mysql_fetch_row($resultado) )		
    		{		
    			print "<option value='$codgrado-$nomgrado'";
    			if ( ( $codgrado == $_POST["hid_grado"] ) or ( $codgrado = $_POST["cbo_gradoP"] ) )
    				print "selected";
    				
    			print ">$nomgrado</option>";			
    		}
    		mysql_free_result($resultado);
    	  ?>
        </select>      
      </td>
      <td align="left" valign="middle"><strong>Cantidad : </strong></td>
      <td align="left" valign="middle">
        <input style="width: 50px;" type="text" id="cantipreguntas" name="cantipreguntas" onkeypress="return soloNumeros(event)" />
        <input type="button" id="cantpregbtn" name="cantpregbtn" value="M&aacute;ximo" onclick="cantpreg()" />
        <div id="cantpreg"></div>
      </td>
    </tr>
   
    <tr class="trInformacion">
    <td colspan="4" align="left" valign="middle" style="text-align: center;">
    <input type="text" style="background-color:transparent; color: red; font-weight: bold; text-align: center; border-width: 0;" readonly="readonly" value="0 Preguntas Agregadas" name="totalpreguntas" id="totalpreguntas" />
    </td>
    </tr> 
     <tr class="trInformacion">
    	<td colspan="4" height="10px"></td>
    </tr>  
    <tr class="trInformacion">
        <td colspan="4" align="left" valign="middle" style="text-align: center;">
            <strong>Nota: Si excede la cantidad m&aacute;xima de preguntas se seleccionar&aacute;n el m&aacute;ximo de estas.</strong>
        </td>
    </tr>     
    <tr class="trInformacion">
        <td colspan="4" align="left" valign="middle" style="text-align: center;"><input type="button" value="Agregar Preguntas" onclick="guardarpreguntas()" /></td>
        
        <input type="hidden" value="" name="arrpreguntas" id="arrpreguntas" />
    </tr>  
           
  </table>
  <br />
  <table class="tablaMarquesina">
    <tr align="center" valign="middle">
      <td><input type="button" name="btn_guardarEvaluacion" title="Escoger las preguntas manualmente" value="Generar Evaluaci&oacute;n Manual" onClick="javascript:guardar()"></td>
      <td><input type="button" name="btn_guardarEvaluacionAutomatica" title="Escoger las preguntas por aleatorio" id="btn_guardarEvaluacionAutomatica" value="Generar Evaluaci&oacute;n Automatica" onClick="javascript:generarautomatica()"></td>
      <td><input type="button" name="btn_cancelar" value="Cancelar" onClick="javascript:cancelar('<?PHP echo $_POST["hid_materia"]; ?>', '<?PHP echo $materia?>')"></td>
    </tr>
  </table>
</form>
<?PHP 
		}//FIN IF RESULTADO
		else
		{//else sin nota evaluacion
 ?>
&nbsp;
<table class="tablaGeneral" width="200">
  <tr class="trSubTitulo">
    <td colspan="2" align="center" valign="middle">No se puede Crear el Previo</td>
  </tr>
  <tr class="trInformacion">
    <td colspan="2" align="left" valign="middle"><p><strong>Posibles Causas:</strong></p>
        <ul>
          <li>No existen Notas de Evaluaciones definidas para el grupo <strong>
            <?PHP echo $grupo?>
          </strong></li>
          <br>
          <li>Todas las Notas de Tipo Previo ya tiene asiganadas Evaluaciones.</li>
        </ul>
      Para crear Notas haga click <a href="../notas/index.php?idmateria=<?PHP echo $_POST["hid_materia"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link"><strong><em>Aqui</em></strong></a> </td>
  </tr>
  <tr class="trInformacion">
    <td colspan="2" align="center" valign="middle">&nbsp;</td>
  </tr>
</table>
<?PHP 
		}//fin else sin nota evaluacion
?>&nbsp;
  <table class="tablaMarquesina" width="200">
	<tr align="center" valign="middle">
		<td><a href="ingresarEvaluacion.php?idmateria=<?PHP echo $_POST["hid_materia"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>&cbo_orden=<?=$idgrupo.'-'.$grupo ?>" class="link">Volver</a></td>
	</tr>
  </table>
<?PHP 
	}//If estado 3
	else
	{//INICIO
		if ($_SESSION['idtipousuario']==5)
		{
			$sql = "SELECT mei_virgrupo.idvirgrupo, mei_virgrupo.nombre FROM `mei_virgrupo`, mei_relusuvirgru
				WHERE mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo AND
				mei_relusuvirgru.idusuario=".$_SESSION["idusuario"]." AND
				mei_virgrupo.idvirmateria=".$_GET["idmateria"];
		}
		else
		{
			$sql = "SELECT mei_grupo.idgrupo, mei_grupo.nombre FROM `mei_grupo`, mei_relusugru
				WHERE mei_grupo.idgrupo = mei_relusugru.idgrupo AND
				mei_relusugru.idusuario=".$_SESSION["idusuario"]." AND
				mei_grupo.idmateria=".$_GET["idmateria"];
		}

		$resultado = $baseDatos->ConsultarBD($sql);

		if ( mysql_num_rows($resultado) > 0 )
		{//IF SI EXISTEN GRUPOS
			?>

			<form name="frm_1" method="post" action="?estado=3&materia=<?PHP echo $_GET["materia"]; ?>&idmateria=<?PHP echo $_GET["idmateria"]; ?>">
				  <table class="tablaGeneral" width="200" border="0">
                    <tr class="trTitulo">
                      <td colspan="2" align="left" valign="middle">Ingresar Evaluaci&oacute;n
                        <input name="hid_materia" type="hidden" id="hid_materia" value="<?PHP echo $_GET["idmateria"]; ?>"></td>
                    </tr>

                    <tr class="trSubTitulo">
                      <td width="20%"><b>Autor:</b></td>
                      <td>&nbsp;<?PHP echo  $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']; ?></td>
                    </tr>

                    <tr class="trInformacion">
                      <td><b>Grupo:</b></td>
                      <td align="left" valign="middle">
					 <select class="link" name="cbo_grupo" id="cbo_grupo">

						   <?PHP  /*?><select name="cbo_grupo"><?PHP  */?><?
						   	if ($_GET['cbo_orden']!='*') {
		$_POST['cbo_grupo']=$_GET['cbo_orden'];
	}?>
					  			<?PHP 
									while ( list($idgrupo, $grupo) = mysql_fetch_row($resultado) )
									{
										print "<option value = '$idgrupo-$grupo'";
											if ( $_POST["cbo_grupo"] == ($idgrupo."-".$grupo)){
												print " selected>";
											}
											else {
												print ">";
									    	}
											print "$grupo</option>";
									}
								?>
			            </select>
                        <label></label></td>
                    </tr>
                    <tr class="trInformacion">
                      <td><strong>Tipo de Evaluaci&oacute;n: </strong></td>
                      <td align="left" valign="middle"><select name="cbo_tipoprevio" id="cbo_tipoprevio">
                        <?PHP 
					  	$sql = "SELECT mei_tipoprevio.idtipoprevio, mei_tipoprevio.tipoprevio FROM mei_tipoprevio";
						$resultado = $baseDatos->ConsultarBD($sql);
						if($_SESSION['idtipousuario']==5){
						while ( list($idtipoprevio, $tipoprevio) = mysql_fetch_row($resultado) )
						{
							if($tipoprevio!="Previo"){
								if($tipoprevio="Quiz"){
									$tipoprevio="Previo";
							print "<option value='$idtipoprevio-$tipoprevio'>$tipoprevio</option>";
									}
							}
						}
						}
						else {
							while ( list($idtipoprevio, $tipoprevio) = mysql_fetch_row($resultado) )
						{
							print "<option value='$idtipoprevio-$tipoprevio'>$tipoprevio</option>";
						}	
						}
					  ?>
                      </select></td>
                    </tr>
                    <tr class="trInformacion">
                     <?PHP  /*?> <td><strong>Evaluaci&oacute;n Aleatoria </strong></td>
                      <td align="left" valign="middle"><select name="tipoEvaluacion" ><option value="si">Si</option>
					                                                                  <option value="no">No </option> </select>
					                                    </td><?PHP  */?>
                    </tr>
                    <tr class="trSubTitulo">
                      <td colspan="2"><div align="center">
                        <input type="button" name="Submit" value="Continuar" onClick="javascript:enviar()">
                      </div></td>
                    </tr>
                  </table>
  <?PHP 
		}//FIN IF SI EXISTEN GRUPOS
		else 
		{
			//echo "No existen grupos definidos en esta materia";
		
  ?>
	<table class="tabalGeneral">
  		<tr>
			<td class="trInformacion"> No existen grupos definidos en esta materia </td>
		</tr>
	</table>
<?PHP 
		}
?>
				  <table class="tablaMarquesina" width="200">
                    <tr align="center" valign="middle">
                    <td><a href="verEvaluacion.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>&cbo_orden=<?=$_GET["cbo_orden"]?>" class="link">Volver</a></td>
                    </tr>
                  </table>
		    </form>
<?PHP 
	}//FIN INICIO

?>
		  </td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><?PHP  menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
</table>
</body>
</html>
<?PHP 
}
else redireccionar('../login/');
?>