<?
	include_once('evaclass.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');

if(comprobarSession())
{
	$evaluacion = $_SESSION["evaluacion"]; 
	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, 
			mei_evaprevio.titulo, mei_virgrupo.nombre, mei_evaluacion.nombre, mei_evaprevio.fechaactivacion, 
			mei_evaprevio.fechafinalizacion, mei_evaprevio.tiempo, mei_evaprevio.barajarpreg, mei_evaprevio.barajarresp, 
			mei_evaprevio.comentario, mei_evaprevio.intentos, mei_evaprevio.valorIntento,
			mei_evamodcalificar.modcalificar, mei_evaprevio.mostrarnota, mei_evaprevio.estado, mei_evaprevio.idtipoprevio,
			mei_tipoprevio.tipoprevio, mei_evaprevio.idtiposubgrupo FROM mei_evaprevio, mei_usuario, mei_evamodcalificar, mei_tipoprevio,
			mei_evaluacion, mei_evavirgrupo, mei_virgrupo	WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo AND 
			mei_evaprevio.idautor = mei_usuario.idusuario AND mei_evaprevio.idmodCalificar = mei_evamodcalificar.idmodcalificar
			AND mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaprevio.idprevio=".$evaluacion->idprevio;
	}
	else
	{
		$sql = "SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, 
			mei_evaprevio.titulo, mei_grupo.nombre, mei_evaluacion.nombre, mei_evaprevio.fechaactivacion, 
			mei_evaprevio.fechafinalizacion, mei_evaprevio.tiempo, mei_evaprevio.barajarpreg, mei_evaprevio.barajarresp, 
			mei_evaprevio.comentario, mei_evaprevio.intentos, mei_evaprevio.valorIntento,
			mei_evamodcalificar.modcalificar, mei_evaprevio.mostrarnota, mei_evaprevio.estado, mei_evaprevio.idtipoprevio,
			mei_tipoprevio.tipoprevio, mei_evaprevio.idtiposubgrupo FROM mei_evaprevio, mei_usuario, mei_evamodcalificar, mei_tipoprevio,
			mei_evaluacion, mei_evagrupo, mei_grupo	WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = mei_grupo.idgrupo AND 
			mei_evaprevio.idautor = mei_usuario.idusuario AND mei_evaprevio.idmodCalificar = mei_evamodcalificar.idmodcalificar
			AND mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaprevio.idprevio=".$evaluacion->idprevio;
	}
	$resultado=$baseDatos->ConsultarBD($sql);	
	//print $sql;	
	list($prinomAutor,$segnomAutor,$priapeAutor,$segapeAutor, $titulo, $nomgrupo, $nomEva, $fecha_a, $fecha_f, $tiempo, $bpreg, $bresp, $comentario, $intentos, $valorIntento, $mcalificar, $mnota, $estado, $idtipoprevio, $tipoprevio, $idtiposubgrupo) = mysql_fetch_row($resultado);
	
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<title>Documento sin t&iacute;tulo</title>
</head>

<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  
  <tr align="center" class="trTitulo">
    <td colspan="4" valign="middle">Informaci&oacute;n de la Evaluaci&oacute;n </td>
  </tr>
  <tr class="trInformacion">
    <td width="30%" align="left" valign="middle"><strong>Grupo:</strong></td>
    <td width="20%"><strong>
      <?=$nomgrupo?>
    </strong></td>
    <td width="20%"><b>Subgrupo:</b></td>
    <td width="30%"><? if (empty($idtiposubgrupo)) print "Evaluaci&oacute;n No Grupal";
								else
								{ 
									$sql = "SELECT mei_tiposubgrupo.tiposubgrupo FROM mei_tiposubgrupo
											WHERE mei_tiposubgrupo.idtiposubgrupo=".$idtiposubgrupo;
									$rsql = $baseDatos->ConsultarBD($sql);
									list ($subgrupo) = mysql_fetch_row($rsql);
									print $subgrupo;
								}
							?></td>
  </tr>
  <tr class="trInformacion">
    <td align="left" valign="middle"><strong>Tipo de Evaluaci&oacute;n: </strong></td>
    <td colspan="3"><strong>
      <?=$tipoprevio?>
    </strong></td>
  </tr>
  <tr class="trInformacion">
    <td width="30%" align="left" valign="middle"><strong>Titulo de la Evaluaci&oacute;n:</strong></td>
    <td colspan="3"><strong><? print $titulo."<br>";?></strong></td>
  </tr>
  <tr class="trInformacion">
    <td width="30%" align="left" valign="middle"><strong>Fecha Activaci&oacute;n: </strong></td>
    <td colspan="3"><?=mostrarFechaTexto($fecha_a,1)?></td>
  </tr>
  
</table>

<?
if ( count($evaluacion->preguntas)>0 )
{//IF HAY PREGUNTAS
?>
			<table width="100%">
			  <tr align="center" valign="middle" class="trTitulo">
				<td>Preguntas de la Evaluaci&oacute;n</td>
			  </tr>
			</table>&nbsp;

<?
	while( list($idpreg) = each($evaluacion->preguntas) )
	{//WHILE IMPRIMIR PREGUNTAS
		
		$sql = "SELECT mei_evapreguntas.idpregunta, mei_evapreguntas.pregunta, mei_evapreguntas.tipo_pregunta,							
				mei_evatipopregunta.tipo, mei_evagradopregunta.grado, mei_tema.titulo FROM mei_evapreguntas, 
				mei_evatipopregunta, mei_evagradopregunta, mei_tema 
				WHERE mei_evapreguntas.tipo_pregunta = mei_evatipopregunta.tipo_pregunta
				AND mei_evapreguntas.idgrado = mei_evagradopregunta.idgrado AND mei_evapreguntas.idtema = mei_tema.idtema AND
				mei_evapreguntas.idpregunta = '".$idpreg."' ORDER BY mei_evapreguntas.tipo_pregunta";
				
		$respreguntas = $baseDatos->ConsultarBD($sql);
		if (mysql_num_rows($respreguntas)>0)
		{//IF 1
			while ( list($idpregunta, $pregunta, $tipo, $txt_tipo, $grado, $nomtema) = mysql_fetch_row($respreguntas))
			{//WHILE 1
?>				<!--<br>
				<div align="center" class="trSubTitulo" style=" width:70%; float:left"><?=stripslashes(base64_decode($pregunta))?></div>
				<div align="right" class="trSubTitulo" style=" width:30%; float:right"><?=$txt_tipo?></div>
				<br>
			<div align="left" class="trSubTitulo">Tema: <?=$nomtema?></div>-->
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr class="trTitulo">
					  <td width="50%" align="left" valign="middle">Valor de la Pregunta: <?=($evaluacion->preguntas[$idpreg]->valor*100)."%"?></td>
					  <td width="50%" align="right" valign="middle"><?=$txt_tipo?></td>
				  </tr>
					<tr class="trSubtitulo">
						<td colspan="2" align="left" valign="middle" class="trSubtitulo">
						<?
							if ( $tipo != 4 ) print stripslashes(base64_decode($pregunta));
							else
							{
								list( $afirmacion, $razon ) = explode("[$$$]",stripslashes(base64_decode($pregunta)));
								print $afirmacion."<P>PORQUE<P>".$razon;
							} 
						?>						</td>
					</tr>
</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">				
<?				
				if($tipo!=6){
				$sql = "SELECT mei_evarespuestas.respuesta, mei_evarespuestas.valor FROM mei_evarespuestas 
						WHERE mei_evarespuestas.idpregunta = '".$idpregunta."'"; 
				$resrespuestas = $baseDatos->ConsultarBD($sql);
				
				while( list($respuesta, $valor) =  mysql_fetch_row($resrespuestas))
				{
?>
						<tr class="trListaClaro">
							<td class="trListaClaro" width="90%" align="left" valign="middle"><?=stripslashes(base64_decode($respuesta))?></td>
							
						</tr>
					<!--</div>-->
<?			
				}
				}
				else{
					$sql = "SELECT mei_evapreguntasrelacioncolumna.idrespuesta, 
					mei_evapreguntasrelacioncolumna.columna, mei_evapreguntasrelacioncolumna.relacion, 
					mei_evapreguntasrelacioncolumna.valor FROM mei_evapreguntasrelacioncolumna 
						WHERE mei_evapreguntasrelacioncolumna.idpregunta = '".$idpregunta."'"; 
				$resrespuestas = $baseDatos->ConsultarBD($sql);
				
				while( list($idrespuesta, $columna, $relacion, $valor) =  mysql_fetch_row($resrespuestas))
				{
					$sql = "SELECT mei_evarespuestas.respuesta FROM mei_evarespuestas 
						WHERE mei_evarespuestas.idrespuesta = '".$idrespuesta."'"; 
				$respu = $baseDatos->ConsultarBD($sql);
				list($respuesta) = mysql_fetch_array($respu);
?>
						<tr class="trListaClaro">
							<td class="trListaClaro" width="40%" align="left" valign="middle"><?=stripslashes(base64_decode($columna))?></td>
							<td class="trListaClaro" width="10%" align="left" valign="middle"><?=stripslashes(base64_decode($respuesta))?></td>
							<td class="trListaClaro" width="40%" align="left" valign="middle"><?=stripslashes(base64_decode($relacion))?></td>
							
						</tr>
					<!--</div>--><?
					}
				}
?>
			</table>
	<br>
<?

			}//FIN WHILE 1
		}//FIN IF 1
	}//FIN WHILE IMPRIMIR PREGUNTAS
}//FIN IF HAY PREGUNTAS
?>
<table width="100%">
  <tr align="center" valign="middle" class="trSubTitulo">
  	<td><input type="button" name="cerrar" value="Cerrar" onclick="window.close();"></td>
    <td><input type="button" name="imprimir" value="Imprimir" onclick="window.print();"></td>
  </tr>
  
  
  
</table>
</body>
</html>
<?
}
?>