<?
include_once ('../baseDatos/BD.class.php');
include_once ('../librerias/estandar.lib.php');
include_once ('../librerias/vistas.lib.php');
include_once ('../menu/Menu.class.php');
include_once ('../menu1/Menu1.class.php');

$baseDatos = new BD();
if (comprobarSession()) {
	
    mysql_query("SET NAMES 'utf8'");
	
    if ($_GET['esp'] != 'reposicion')
        $sql = "SELECT mei_evaprevio.idtiposubgrupo FROM mei_evaprevio WHERE mei_evaprevio.idprevio = '" . $_GET["idprevio"] . "'";
    else
        $sql = "SELECT mei_evaquiz.idtiposubgrupo FROM mei_evaquiz WHERE mei_evaquiz.idprevio = '" . $_GET["idprevio"] . "'";

    $ressubgrupo = $baseDatos->ConsultarBD($sql);

    list($idtiposubgrupo) = mysql_fetch_row($ressubgrupo);

    if (empty($_POST["cbo_ordenar"]))
        $_POST["cbo_ordenar"] = 3;
    switch ($_POST["cbo_ordenar"]) {
        case 1:
            $orden = "mei_usuario.idusuario";
            $activoCodigo = "selected";
            break;
        case 2:
            $orden = "mei_usuario.primernombre";
            $activoNombre = "selected";
            break;
        default:
            $orden = "mei_usuario.primerapellido";
            $activoApellido = "selected";
    }
    ?>

    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="../temas/<?= recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
            <script>
                window.name = "notasEvaluacion";
                function modificar(mod, idusuario, idprevio, idmateria, nota, intentos, cbo_intento)
                {
                    var izquierda = (screen.availWidth - 200) / 2; 
                    var arriba = (screen.availHeight - 180) / 2; 

                    calificacion = window.open("modificarNotaEvaluacion.php?mod="+mod+"&idusuario="+idusuario+"&idprevio="+idprevio+"+&idmateria="+idmateria+"&intentos="+intentos+"&cbo_intento="+cbo_intento+"&nota="+nota+"&esp=<?= $_GET["esp"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>","Calificar","width = 250, height = 170, left = "+izquierda+", top = "+arriba+", scrollbars = NO, menubars = NO, status = NO, statusbar = NO, status = NO, resizable = NO, location = NO")
                }

                function chekear()
                {
                    var todo=document.getElementById('chk_grupo');
                    if (todo.checked==true)
                    {
                        for(i=0;i<document.frm_lista.elements.length;i++)
                        {
                            if(document.frm_lista.elements[i].id=='chk_lista')
                                document.frm_lista.elements[i].checked=true;
                        }
                    }
                    else
                    {
                        for(i=0;i<document.frm_lista.elements.length;i++)
                        {
                            if(document.frm_lista.elements[i].id=='chk_lista')
                                document.frm_lista.elements[i].checked=false;
                        }
                    }
                }

                function eliminar(intento)
                {
                    if(confirm("Se Eliminaron los Intentos de los Alumnos Seleccionados"))
                    {
                        var contador=0;
                        for(i=0;i<document.frm_lista.elements.length;i++)
                        {
                            if (document.frm_lista.elements[i].checked==true)
                            {
                                contador++;
                            }
                        }
                        if(contador>0)
                        {
                            document.frm_lista.action="eliminarIntento.php?accion=DEL&idprevio=<?= $_GET["idprevio"] ?>&idmateria=<?= $_GET["idmateria"] ?>&intentos=<?= $_GET["intentos"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&cbo_intento="+intento+"&esp=<?= $_GET["esp"] ?>&materia=<?= $_GET['materia'] ?>";
                            document.frm_lista.submit();
                        }
                        else
                            alert('No ha seleccionado ningún Alumno')
                    }
                }
                function recalificar(intento)
                {
                    if(confirm("Se Re-Calificarán los Intentos de los Alumnos Seleccionados"))
                    {
                        var contador=0;
                        for(i=0;i<document.frm_lista.elements.length;i++)
                        {
                            if (document.frm_lista.elements[i].checked==true)
                            {
                                contador++;
                            }
                        }
                        if(contador>0)
                        {
                            //alert("recalificarEvaluacion.php?idprevio=<?= $_GET["idprevio"] ?>&idmateria=<?= $_GET["idmateria"] ?>&intentos=<?= $_GET["intentos"] ?>&cbo_intento="+intento+"&esp=<?= $_GET["esp"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&materia=<?= $_GET['materia'] ?>");
                            document.frm_lista.action="recalificarEvaluacion.php?idprevio=<?= $_GET["idprevio"] ?>&idmateria=<?= $_GET["idmateria"] ?>&intentos=<?= $_GET["intentos"] ?>&cbo_intento="+intento+"&esp=<?= $_GET["esp"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&materia=<?= $_GET['materia'] ?>";
                            document.frm_lista.submit();
                        }
                        else
                            alert('No ha seleccionado ningún Alumno')
                    }
                }
                function descongelareva(codigoestudiante,idevaluacion){
                    var r = confirm("Desea que él estudiante con el codigo: "+codigoestudiante+" pueda continuar con el previo?");
                    if (r == true) {
                    	if( codigoestudiante != "" ){
                    	  $.ajax({
                    					data:  ({ codigoestudiante : codigoestudiante,idevaluacion:idevaluacion }),
                    					url:   'descongelarestudiante.php',
                    					type:  'post',
                    					success:  function (response) {
                    							alert("El estudiante continuará con el previo");
                    					}
                    			});							
                    	} 
                    }                     
                }
                
//--------------------------------------------------------------------------------------------------------------
 function descongelarevamuchos(idevaluacion, idgrupo)
		{
			
                    var r = confirm("Desea que los estudiantes puedan continuar con el previo?");
                    if (r == true) {
                    	if( idevaluacion != "" ){
                    	  $.ajax({
                    					data:  ({ idevaluacion : idevaluacion, idgrupo : idgrupo }),
                    					url:   'descongelarestudiantevarios.php',
                    					type:  'post',
                    					success:  function (response) {
                    							alert("los estudiantes continuaran con el previo");
                    					}
                    			});							
                    	} 
                    }                     
                }

		function congelareva(codigoestudiante,idevaluacion)
		{
			
                    var r = confirm("Desea congelar la evaluacion para el estudiante con codigo: "+codigoestudiante+" ?");
                    if (r == true) {
                    	if( codigoestudiante != "" ){
                    	  $.ajax({
                    					data:  ({ codigoestudiante : codigoestudiante,idevaluacion:idevaluacion }),
                    					url:   'congelarestudiante.php',
                    					type:  'post',

                    					success:  function (response) {
                    							alert("la evaluacion quedó congelada");
                    					}
                    			});							
                    	} 
                    }                     
                }
		function congelarevamuchos(idevaluacion, idgrupo)
		{
			
                    var r = confirm("Desea congelar la evaluacion para todos los estudiantes?");
                    if (r == true) {
                    	if( idevaluacion != "" ){
                    	  $.ajax({
                    					data:  ({ idevaluacion:idevaluacion, idgrupo : idgrupo }),
                    					url:   'congelarestudiantevarios.php',
                    					type:  'post',
                    					success:  function (response) {
                    							alert("las evaluaciones quedaron congeladas");
                    					}
                    			});							
                    	} 
                    }                     
                }      
//_____________________________________________________________________________________________________________________________     
//--------------------------------------------------- nuevo codigo ---------------------------------------------------------------------
		function editar(idusu,idprevio)
		{
   			var izquierda = (screen.availWidth - 200) / 2;
  	 		var arriba = (screen.availHeight - 180) / 2;

   			ventana = window.open("notaExtraIndividual.php?materia=<?=$_GET["materia"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&idusuario="+idusu+"&idprevio="+idprevio+"&esp=<?=$_GET['esp']?>&intentos=<?=$_GET['intentos']?>","ventana","width=240,height=180,left="+izquierda+",top="+arriba+",scrollbars=NO,menubars=NO,status=NO,statusbar=NO,status=NO,resizable=NO,location=NO")
		}
//--------------------------------------------------- nuevo codigo ---------------------------------------------------------------------
            </script>
        </head>
        <body>
            <table height="260" class="tablaPrincipal">
                <tr valign="top">
                    <td class="tablaEspacio">&nbsp;</td>
                    <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                    <td class="tablaEspacio">&nbsp;</td>
                    <td valign="top" class="tdGeneral">
                        <table class="tablaMarquesina" >
                            <tr  class="trSubTitulo">
                                <?
                                if ($_SESSION['idtipousuario'] == 5)
                                    $sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=" . $_GET['idmateria'];
                                else
                                    $sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=" . $_GET['idmateria'];
                                $resultado = $baseDatos->ConsultarBD($sql);
                                list($nombre) = mysql_fetch_row($resultado);
                                ?>
                                <td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?= $_GET['idmateria'] ?>&materia=<?= $_GET["materia"] ?>" class="link"><?= ucwords(strtolower($nombre)) ?></a><a> -> </a><a href="../evaluaciones/verEvaluacion.php?idmateria=<?= $_GET["idmateria"] ?>&materia=<?= $_GET["materia"] ?>" class="link">Evaluaciones</a><a> -> </a><a href="verDtlleEvaluacion.php?idprevio=<?= $_GET["idprevio"] ?>&idmateria=<?= $_GET["idmateria"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&materia=<?= $_GET["materia"] ?>" class="link">Ver Evaluaci&oacute;n</a> -> Ver Resultados </td>
                            </tr>
                        </table>&nbsp;

                        <form name="frm_intentos" method="post" action="notasEvaluacion.php?intentos=<?= $_GET["intentos"] ?>&idprevio=<?= $_GET["idprevio"] ?>&idmateria=<?= $_GET["idmateria"] ?>&idgrupo=<?= $_GET['idgrupo'] ?>&esp=<?= $_GET['esp'] ?>&materia=<?= $_GET["materia"] ?>">
                            <?
                            if ($_GET['esp'] != 'reposicion') {
                                if ($_SESSION['idtipousuario'] == 5) {
                                    $sql = "SELECT mei_tipoprevio.tipoprevio, mei_evaprevio.titulo, mei_virgrupo.nombre FROM mei_evaluacion, mei_evaprevio, mei_evavirgrupo, mei_virgrupo, mei_tipoprevio WHERE mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo AND mei_evaprevio.idprevio = '" . $_GET["idprevio"] . "'";
                                } else {
                                    $sql = "SELECT mei_tipoprevio.tipoprevio, mei_evaprevio.titulo, mei_grupo.nombre FROM mei_evaluacion, mei_evaprevio, mei_evagrupo, mei_grupo, mei_tipoprevio WHERE mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = mei_grupo.idgrupo AND mei_evaprevio.idprevio = '" . $_GET["idprevio"] . "'";
                                }
                            } else {
                                $sql = "SELECT 'Quiz de Reposicion', mei_evaquiz.titulo, '' FROM mei_evaquiz WHERE mei_evaquiz.idprevio = '" . $_GET["idprevio"] . "'";
                            }
                            $resprevio = $baseDatos->ConsultarBD($sql);
                            list($tipoprevio, $titulo, $grupo) = mysql_fetch_row($resprevio);
//print $sql;
///echo "<br> <br> consulta los datos";
                            ?>				
                            <table class="tablaGeneral">                  
                                <tr class="trTitulo">
                                    <?php
                                    if ($_GET['esp'] != 'reposicion') {
                                        	if($_SESSION['idtipousuario']==5){
                                        		$tipoprevio="Previo";
                                        	}
                                        
                                        ?>
                                        
                                        <td colspan="4">Resultados del <?= $tipoprevio ?>: <em><?= $titulo ?></em> del Grupo <?= $grupo ?></td>
                                        <?php
                                    } else {
                                    	if($_SESSION['idtipousuario']==5){
                                        ?>
                                        <td colspan="4">Resultados del Previo de Reposición</td>
                                        <?php
										}
										if($_SESSION['idtipousuario']==2){
                                        ?>
                                        <td colspan="4">Resultados del Quiz de Reposición</td>
                                        <?
										}
                                    }
                                    ?>
                                </tr>
                                <tr class="trSubTitulo">
                                    <td width="29%">Ver Intento: </td>
                                    <td width="51%"><select name="cbo_intento" onChange="javascript:document.frm_intentos.submit()">
                                            <option value="0">Definitiva</option>
    <?
    if (empty($_POST["cbo_intento"])) {
        if (empty($_GET["cbo_intento"]))
            $_POST["cbo_intento"] = 0;
        else
            $_POST["cbo_intento"] = $_GET["cbo_intento"];
    }

//while ( list($idintento, $nombre) = mysql_fetch_row($resultado) )
    for ($i = 1; $i <= $_GET["intentos"]; $i++) {
        print "<option value='$i'";
        if ($i == $_POST["cbo_intento"])
            print " selected>";
        else
            print ">";
        print "Intento $i</option>";
    }
    ?>
                                        </select></td>
                                    <td width="20%">Ordenar Lista Por: </td>
                                    <td width="9%"><select name="cbo_ordenar" onChange="javascript:document.frm_intentos.submit()">
                                            <option value="1" <?= $activoCodigo ?>>C&oacute;digo</option>
                                            <option value="2" <?= $activoNombre ?>>Nombre</option>
                                            <option value="3" <?= $activoApellido ?>>Apellido</option>
                                        </select></td>
                                </tr>
                            </table>
                        </form>
                        <form method="post" name="frm_lista">
    <?
//******************************************************************************************
    if ($_GET['esp'] != 'reposicion')
        $sql = "SELECT mei_evamodcalificar.idmodcalificar, mei_evamodcalificar.modcalificar, mei_evaprevio.fechafinalizacion 
			FROM mei_evaprevio, mei_evamodcalificar	WHERE mei_evaprevio.idprevio = '" . $_GET["idprevio"] . "' AND
			mei_evaprevio.idmodcalificar = mei_evamodcalificar.idmodcalificar";
    else
        $sql = "SELECT mei_evamodcalificar.idmodcalificar, mei_evamodcalificar.modcalificar, mei_evaquiz.fechafinalizacion 
			FROM mei_evaquiz, mei_evamodcalificar	WHERE mei_evaquiz.idprevio = '" . $_GET["idprevio"] . "' AND
			mei_evaquiz.idmodcalificar = mei_evamodcalificar.idmodcalificar";
    $resultado = $baseDatos->ConsultarBD($sql);

    list($idmodC, $modC, $fecha_f) = mysql_fetch_row($resultado);

//******************************************************************************************

    function calcularNotaExtra($sql, $tipo) {

        global $baseDatos, $idtiposubgrupo, $notaExtra;
//-------------------------------------------------------- nuevo codigo ------------------------------------------------------------------
	$sql2="SELECT mei_evaprevio.curva FROM mei_evaprevio WHERE idprevio='".$_GET["idprevio"]."'";
        $result = $baseDatos->ConsultarBD($sql2);
	list($curva) = mysql_fetch_row($result);
	$sql3 = "SELECT mei_usuprevio.notaextra, mei_usuprevio.idusuario FROM mei_usuprevio WHERE idprevio='".$_GET["idprevio"]."'";
	//$sql3 = "SELECT mei_usuprevio.notaextra FROM mei_usuprevio WHERE idprevio='".$_GET["idprevio"]."'";
	$resultadouno = $baseDatos->ConsultarBD($sql3);
	//list($incentivo)=mysql_fetch_row($resultadouno);
	while(list($incent,$iduser) = mysql_fetch_row($resultadouno))
	{
		$incentivo[$iduser]=$incent;
	}
//-------------------------------------------------------- nuevo codigo -----------------------------------------------------------------

        $resultado = $baseDatos->ConsultarBD($sql);
		$resultado1 = $baseDatos->ConsultarBD($sql);
        if ($tipo == 1) {// recibe el sql donde se muestra de una vez la nota
            if ($curva == 1) {
                //echo "tiene curva";

                $maxVal = 0;
                while (list($idusu2, $pnombre2, $snombre2, $papellido2, $sapellido2, $nota2, $idintento) = mysql_fetch_row($resultado)) {
                    if ($nota2 > $maxVal) {
                        $maxVal = $nota2;
                    }
                }
				$extra=5-$maxVal;
				
				
while (list($idusu2, $pnombre2, $snombre2, $papellido2, $sapellido2, $nota2, $idintento) = mysql_fetch_row($resultado1)) {
                    	
						/*
                    if ($nota2 > $maxVal) {
                        $maxVal = $nota2;
                    }

                $maxVal = number_format($maxVal, 1, '.', '');

                $decima = explode(".", $maxVal);

                $decima = $decima[1];
                if ($decima == 0) {
                    $diferencia = 0;
                } else {
                    $diferencia = 10 - $decima;
                }
                $bandera = 0;
                if ($diferencia > 0 && $diferencia <= 5) {// se le suma diferencia a todos
                    $bandera = 1;
                } else if ($diferencia > 5 && $diferencia <= 9) {// se la aplica la diferencia como factor a cada nota;
                    $bandera = 2;
                }
                    if ($bandera == 1) {// se le suma le factor
                        if($nota2>0)
                        $notaExtra[$idusu2] = $diferencia / 10;
                        else 
                        $notaExtra[$idusu2] = "0.0";
                    } else if ($bandera == 2) {//se le aplicara el factor a cada nota
                        $notaExtra[$idusu2] = (($diferencia / 10) * $nota2) / 10;
                    } else {
                        $nota_Extra[$idusu2] = 0;
                    }
					*/
					
					//if($nota2>0)
                        $notaExtra[$idusu2] = $extra;
                      //  else 
                       // $notaExtra[$idusu2] = "0.0";
					
					$sqlo="SELECT mei_usuprevio.flag, mei_usuprevio.notaextra FROM mei_usuprevio WHERE mei_usuprevio.idprevio='" . $_GET["idprevio"] . "' and mei_usuprevio.idusuario='" . $idusu2 . "'";
					$rlt = $baseDatos->ConsultarBD($sqlo);
					list($flag, $notaex)=mysql_fetch_row($rlt);
					if($flag==0){
					$sqln2 = "update mei_usuprevio set notaextra='" . number_format(round(($notaExtra[$idusu2]), 2), 2, '.', '') . "', flag=1 where mei_usuprevio.idprevio='" . $_GET["idprevio"] . "' and mei_usuprevio.idusuario='" . $idusu2 . "'";
        $notasextras2 = $baseDatos->ConsultarBD($sqln2);	
					}else {
						$notaExtra[$idusu2]=$notaex;
					}
					
                }



                //echo "esta es la decima 0".$decima;
                //echo "arreglo de notas  ";
                //print_r($notaExtra);
            } else {//tiene nota extra";
                while (list($idusu2, $pnombre2, $snombre2, $papellido2, $sapellido2, $nota2, $idintento) = mysql_fetch_row($resultado)) {
                   // if ($idintento != 0)
                        $notaExtra[$idusu2] = $incentivo[$idusu2];
                   // else
                     //   $notaExtra[$idusu2] = "0.00";
                }

                //print_r($notaExtra);
            }
        }else if ($tipo == 2) {
			
            if ($_GET['esp'] != 'reposicion')
                $sqlr = "SELECT mei_evaprevio.intentos FROM mei_evaprevio WHERE mei_evaprevio.idprevio = '" . $_GET["idprevio"] . "'";
            else
                $sqlr = "SELECT mei_evaquiz.intentos FROM mei_evaquiz WHERE mei_evaquiz.idprevio = '" . $_GET["idprevio"] . "'";
            $res1 = $baseDatos->ConsultarBD($sqlr);
            list($intentos) = mysql_fetch_row($res1);
            $temporal = array();
            $x = 0;
            $maxVal = 0;
			
            if ($curva == 1) {// cuandootiene curva
                $resultados = $baseDatos->ConsultarBD($sql);
			$resultados1 = $baseDatos->ConsultarBD($sql);
                while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados)) {
                    if ($_GET['esp'] != 'reposicion')
                        $sqln = "SELECT COUNT(*), SUM(mei_usuprevio.nota) FROM mei_usuprevio WHERE mei_usuprevio.idprevio = '" . $_GET["idprevio"] . "'
						AND mei_usuprevio.idusuario = '" . $idusu . "' AND mei_usuprevio.idintento<='" . $intentos . "'";
                    else
                        $sqln = "SELECT COUNT(*), SUM(mei_usuquiz.nota) FROM mei_usuquiz WHERE mei_usuquiz.idquiz = '" . $_GET["idprevio"] . "'
						AND mei_usuquiz.idusuario = '" . $idusu . "'";
                    $res = $baseDatos->ConsultarBD($sqln);
                    list($cont, $sumNota) = mysql_fetch_row($res);
					
                    /*if ($cont != 0) {
                        if (($sumNota / $cont) > $maxVal) {
                            $maxVal = $sumNota / $cont;
                        }
                    }*/
					if ($cont != 0) {
                        if (($sumNota) > $maxVal) {
                            $maxVal = $sumNota/$cont;
                        }
                    }	
					echo $idusu." ".$sumNota." ".$maxVal."<br>";				
                    }
					$extra=5-$maxVal;
					echo "jajaja".$extra."<br>";
while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados1)) {
               	
				if ($_GET['esp'] != 'reposicion')
                        $sqln1 = "SELECT COUNT(*), SUM(mei_usuprevio.nota) FROM mei_usuprevio WHERE mei_usuprevio.idprevio = '" . $_GET["idprevio"] . "'
						AND mei_usuprevio.idusuario = '" . $idusu . "' AND mei_usuprevio.idintento<='" . $intentos . "'";
                    else
                        $sqln1 = "SELECT COUNT(*), SUM(mei_usuquiz.nota) FROM mei_usuquiz WHERE mei_usuquiz.idquiz = '" . $_GET["idprevio"] . "'
						AND mei_usuquiz.idusuario = '" . $idusu . "'";
                    $res2 = $baseDatos->ConsultarBD($sqln1);
                    list($cont, $sumNota) = mysql_fetch_row($res2);
               
			   	echo $idusu." ".$sumNota."<br>";
                //************
               /* $maxVal = number_format($maxVal, 1, '.', '');

                $decima = explode(".", $maxVal);

                $decima = $decima[1];
                if ($decima == 0) {
                    $diferencia = 0;
                } else {
                    $diferencia = 10 - $decima;
                }

                $bandera = 0;
                if ($diferencia > 0 && $diferencia <= 5) {// se le suma diferencia a todos
                    $bandera = 1;
                } else if ($diferencia > 5 && $diferencaia <= 9) {// se la aplica la diferencia como factor a cada nota;
                    $bandera = 2;
                }

               

                    if ($bandera == 1) {// se le suma le factor
                         if(($sumNota / $cont)>0)
                        $notaExtra[$idusu2] = $diferencia / 10;
                        else 
                        $notaExtra[$idusu2] = "0.0";
                    } else if ($bandera == 2) {//se le aplicara el factor a cada nota
                        if ($cont != 0) {
                            $notaExtra[$idusu2] = (($diferencia / 10) * ($sumNota / $cont)) / 10;
                            print number_format(round(($sumNota / $cont), 1), 1, '.', '');
                        } else {
                            $notaExtra[$idusu2] = "0.00";
                        }
                    } else {
                        $nota_Extra[$idusu] = 0;
                    }*/
                    //if ($bandera == 1) {// se le suma le factor
                      //  if($sumNota>0)
                        $notaExtra[$idusu] = $extra;
                       // else 
                       // $notaExtra[$idusu] = "0.0";
						
						
						
                    //} else if ($bandera == 2) {//se le aplicara el factor a cada nota
                      //  $notaExtra[$idusu] = (($diferencia / 10) * ($sumNota)) / 10;
                    //} else {
                        //$nota_Extra[$idusu] = 0;
                    //}
					$sqlo="SELECT mei_usuprevio.flag, mei_usuprevio.notaextra FROM mei_usuprevio WHERE mei_usuprevio.idprevio='" . $_GET["idprevio"] . "' and mei_usuprevio.idusuario='" . $idusu . "'";
					$rlt = $baseDatos->ConsultarBD($sqlo);
					list($flag,$notaex)=mysql_fetch_row($rlt);
					echo $flag."/".$notaex."<br>";
					if($flag==0){
					$sqln2 = "update mei_usuprevio set notaextra='" . number_format(round(($notaExtra[$idusu]), 2), 2, '.', '') . "', flag=1 where mei_usuprevio.idprevio='" . $_GET["idprevio"] . "' and mei_usuprevio.idusuario='" . $idusu . "'";
        $notasextras2 = $baseDatos->ConsultarBD($sqln2);					
					}
					else{
						$notaExtra[$idusu]=$notaex;
					}
					
                }

                //************
            } else {// cuando tiene nota extra -
                $resultados = $baseDatos->ConsultarBD($sql);
                while (list($idusu2, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados)) {
                  //  if ($idintento != 0){
                        $notaExtra[$idusu2] = $incentivo[$idusu2];
				//	}else{
                  //      $notaExtra[$idusu2] = "0.00";
				//	}
                }
            }
        } else if ($tipo == 3) {// cuando es tipo 3
            //********************************************************
            $maxVal = 0;

            if ($curva == 1) {// cuandootiene curva
                $resultados = $baseDatos->ConsultarBD($sql);
				$resultados1 = $baseDatos->ConsultarBD($sql);
                while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados)) {
                    if ($_GET['esp'] != 'reposicion')
                        $sql1 = "SELECT mei_usuprevio.nota FROM mei_usuprevio WHERE mei_usuprevio.idusuario = '" . $idusu . "'
						AND mei_usuprevio.idprevio = '" . $_GET["idprevio"] . "' AND
						mei_usuprevio.idintento = '" . $idintento . "'";
                    else
                        $sql1 = "SELECT mei_usuquiz.nota FROM mei_usuquiz WHERE mei_usuquiz.idusuario = '" . $idusu . "'
						AND mei_usuquiz.idquiz = '" . $_GET["idprevio"] . "'";
                    //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************
                    $res = $baseDatos->ConsultarBD($sql1);
                    list($nota) = mysql_fetch_row($res);
                    if (($nota) > $maxVal) {
                        $maxVal = $nota;
                    }
				}
				$extra=5-$maxVal;
				while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados1)) {
                    if ($_GET['esp'] != 'reposicion')
                        $sql1 = "SELECT mei_usuprevio.nota FROM mei_usuprevio WHERE mei_usuprevio.idusuario = '" . $idusu . "'
						AND mei_usuprevio.idprevio = '" . $_GET["idprevio"] . "' AND
						mei_usuprevio.idintento = '" . $idintento . "'";
                    else
                        $sql1 = "SELECT mei_usuquiz.nota FROM mei_usuquiz WHERE mei_usuquiz.idusuario = '" . $idusu . "'
						AND mei_usuquiz.idquiz = '" . $_GET["idprevio"] . "'";
                    //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************
                    $res = $baseDatos->ConsultarBD($sql1);
                    list($nota) = mysql_fetch_row($res);
                   // if (($nota) > $maxVal) {
                     //   $maxVal = $nota;
                    //}

                //echo "max valor".$maxVal;
                //************
               /* $maxVal = number_format($maxVal, 1, '.', '');

                $decima = explode(".", $maxVal);

                $decima = $decima[1];
                if ($decima == 0) {
                    $diferencia = 0;
                } else {
                    $diferencia = 10 - $decima;
                }


                $bandera = 0;
                if ($diferencia > 0 && $diferencia <= 5) {// se le suma diferencia a todos
                    $bandera = 1;
                } else if ($diferencia > 5 && $diferencaia <= 9) {// se la aplica la diferencia como factor a cada nota;
                    $bandera = 2;
                }

                    if ($_GET['esp'] != 'reposicion')
                        $sql1 = "SELECT mei_usuprevio.nota FROM mei_usuprevio WHERE mei_usuprevio.idusuario = '" . $idusu . "'
						AND mei_usuprevio.idprevio = '" . $_GET["idprevio"] . "' AND
						mei_usuprevio.idintento = '" . $idintento . "'";
                    else
                        $sql1 = "SELECT mei_usuquiz.nota FROM mei_usuquiz WHERE mei_usuquiz.idusuario = '" . $idusu . "'
						AND mei_usuquiz.idquiz = '" . $_GET["idprevio"] . "'";
                    //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************
                    $res = $baseDatos->ConsultarBD($sql1);
                    list($nota) = mysql_fetch_row($res);
                    if ($bandera == 1) {// se le suma le factor
                        if($nota>0)
			//$notaExtra[$idusu2] = $diferencia / 10;
                        $notaExtra[$idusu] = $diferencia / 10;
                        else 
                        $notaExtra[$idusu] = "0.0";
                    } else if ($bandera == 2) {//se le aplicara el factor a cada nota
                        $notaExtra[$idusu] = (($diferencia / 10) * ($nota)) / 10;
                    } else {
                        $nota_Extra[$idusu] = 0;
                    }*/
                    
                    //  if($nota>0)
                        $notaExtra[$idusu] = $extra;
                      //  else 
                       // $notaExtra[$idusu] = "0.0";
                    
					$sqlo="SELECT mei_usuprevio.flag, mei_usuprevio.notaextra FROM mei_usuprevio WHERE mei_usuprevio.idprevio='" . $_GET["idprevio"] . "' and mei_usuprevio.idusuario='" . $idusu . "'";
					$rlt = $baseDatos->ConsultarBD($sqlo);
					list($flag,$notaex)=mysql_fetch_row($rlt);
					if($flag==0){
					$sqln2 = "update mei_usuprevio set notaextra='" . number_format(round(($notaExtra[$idusu]), 2), 2, '.', '') . "', flag=1 where mei_usuprevio.idprevio='" . $_GET["idprevio"] . "' and mei_usuprevio.idusuario='" . $idusu . "'";
        $notasextras2 = $baseDatos->ConsultarBD($sqln2);	
					}else{
						$notaExtra[$idusu]=$notaex;
					}
                }

                //************
            } else {// cuando tiene nota extra -
                $resultados = $baseDatos->ConsultarBD($sql);

                while (list($idusu2, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados)) {
                 //   if ($idintento != 0)	
                        $notaExtra[$idusu2] = $incentivo[$idusu2];
			//$notaExtra[$idusu2] = $incentivo;
                   // else
                     //   $notaExtra[$idusu2] = "0.00";
                }
            }

            //********************************************************
        }



        return $notaExtra;
    }

    function mostrarLista($resultado, $notaExtra) {
        //echo "entro a mostrar lista";
        global $baseDatos, $idtiposubgrupo, $notaExtra;
        ?>
                                <table  class="tablaGeneral">
                                    <tr class="trTitulo">
                                        <td class="trSubTitulo" colspan="9" align="left" valign="middle"><img src="imagenes/usuarios.gif" width="16" height="16">Lista de Alumnos </td>
                                    </tr>
                                    <tr class="trSubTitulo">
                                        <td class="trSubTitulo" colspan="9" align="left" valign="middle">
                                            <input name="chk_grupo" type="checkbox" id="chk_grupo" onClick="javascript:chekear()" value="checkbox"> Todo el Grupo </td>
                                    </tr>
                                    <tr class="trSubTitulo">
                                        <td class="trSubTitulo" colspan="2" align="center" valign="middle">C&oacute;digo</td>
                                        <td class="trSubTitulo" width="10%" align="center" valign="middle">SubGrupo</td>
                                        <td class="trSubTitulo" width="45%" align="center" valign="middle">Nombre del Alumno</td>
                                        <td class="trSubTitulo" width="20%" align="center" valign="middle">Nota </td>
                                        <td class="trSubTitulo" width="20%" align="center" valign="middle">Nota Extra</td>
                                        <td class="trSubTitulo" width="20%" align="center" valign="middle">Nota Definitiva </td>
                                        <td class="trSubTitulo" width="20%" align="center" valign="middle"><button title="Continuar el previo a todos los estudiantes" onclick="descongelarevamuchos(<?php echo $_GET["idprevio"]; ?>,<?php echo $_GET["idgrupo"]; ?>)" >Continuar Todos</button></td>
						<td class="trSubTitulo" width="20%" align="center" valign="middle"><button title="Congelar el previo a todos los estudiantes" onclick="congelarevamuchos(<?php echo $_GET["idprevio"]; ?>,<?php echo $_GET["idgrupo"]; ?>)">Congelar Todos</button> </td>
                                    </tr>
                                <?
                                $i = 0;
                                while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $nota) = mysql_fetch_row($resultado)) {//mei_usuprevio.idusuario, mei_usuprevio.idintento, mei_usuprevio.nota
                                    if ($_POST["cbo_ordenar"] != 3)
                                        $ncompleto = $pnombre . " " . $snombre . " " . $papellido . " " . $sapellido;
                                    else
                                        $ncompleto = $papellido . " " . $sapellido . " " . $pnombre . " " . $snombre;

                                    //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************

                                    $sql = "SELECT mei_subgrupo.nombre FROM mei_subgrupo, mei_relususub 
						WHERE mei_relususub.idsubgrupo = mei_subgrupo.idsubgrupo AND 
						mei_subgrupo.idtiposubgrupo = '" . $idtiposubgrupo . "' AND mei_relususub.idusuario = '" . $idusu . "'";
                                    $res1 = $baseDatos->ConsultarBD($sql);
                                    list($nomSubgrupo) = mysql_fetch_row($res1);

                                    //******************************************************************************************

                                    if (($i % 2) == 0)
                                        $clase = "trListaOscuro";
                                    else
                                        $clase = "trListaClaro";
                                    ?>
                                        <tr class="<?= $clase ?>">
                                            <td width="6%" align="right" valign="middle" class="<?= $clase ?>"><?= $i + 1 ?>
                                                <input name="chk_estudiante[]" type="checkbox" id="chk_lista" value="<?= $idusu ?>"></td>
                                            <td width="9%" align="center" valign="middle" class="<?= $clase ?>"><?= $idusu ?></td>
                                            <td class="<?= $clase ?>" align="center" valign="middle"><?
                        if (!empty($nomSubgrupo))
                            print $nomSubgrupo;
                        else
                            print "---";
                        ?></td>								
                                            <td class="<?= $clase ?>" align="left" valign="middle"><?= $ncompleto ?></td>
                                            <td class="<?= $clase ?>" align="center" valign="middle"><?
                        if (!empty($nota)) 
                            print number_format(round(($nota), 2), 1, '.', '');
                        else
                            print "0.0";
                        ?></td>
                        
                         <?php
						    $idprevio = $_GET["idprevio"];
						    
							$sql="SELECT mei_evaprevio.curva FROM mei_evaprevio WHERE mei_evaprevio.idprevio='".$idprevio."'";
							$resu = $baseDatos->ConsultarBD($sql);
							list($cur)=mysql_fetch_row($resu);
							if($cur==0){
								?>
								<td class="<?= $clase ?>" align="center" valign="middle"><a style="text-decoration: none; <?if(number_format(round(($notaExtra[$idusu]), 2), 2, '.', '')!=0){?>color: red;<?}?>" href="javascript:editar('<?=$idusu?>', '<?=$idprevio?>')"><?= number_format(round(($notaExtra[$idusu]), 2), 2, '.', '') ?></a></td>
							<?
							}else{
								?>
								<td class="<?= $clase ?>" align="center" valign="middle"><a style="text-decoration: none; <?if(number_format(round(($notaExtra[$idusu]), 2), 2, '.', '')!=0){?>color: red;<?}?>" href="javascript:editar('<?=$idusu?>', '<?=$idprevio?>')"><?= number_format(round(($notaExtra[$idusu]), 2), 2, '.', '') ?></a></td>
							
								<?
							}
							
						    ?>
                        
                                           <!-- <td class="<?= $clase ?>" align="center" valign="middle"><?= number_format(round(($notaExtra[$idusu] + $nota), 1), 1, '.', '') ?></td> --> 
                                            
                                            <td class="<?= $clase ?>" align="center" valign="middle"><?php 
														$notafinalextra_c = number_format(round(($notaExtra[$idusu] + $nota), 1), 1, '.', '');
													if( $notafinalextra_c > 5){ $notafinalextra_c = 5.0; }
													//if( $nota == 0 ){ $notafinalextra_c=0; }
													echo $notafinalextra_c;													
													?></td>
                                            
                                            <td><input type="button" value="Continuar" onclick="descongelareva(<?php echo $idusu; ?>, <?php echo $_GET["idprevio"]; ?>)" /></td>
<!-- _________________________________________________________________________________________________________________________ -->
<td><input type="button" value="Congelar" onclick="congelareva(<?php echo $idusu; ?>, <?php echo $_GET["idprevio"]; ?>)" /></td>
<!-- _________________________________________________________________________________________________________________________ -->


                                        </tr>
            <?
            $i++;
        }
        ?>
		   
                                    <tr class="trSubTitulo">
                                        <td class="trSubTitulo" colspan="9" align="center" valign="middle">
                                            <input name="btn_recalificar" type="button" id="btn_recalificar" value="Re-Calificar" onClick="javascript:recalificar('<?= $_POST["cbo_intento"] ?>')"></td>
                                    </tr>					  
<!--  
					<tr class="trSubTitulo">
                                        <td class="trSubTitulo" colspan="8" align="center" valign="middle">
                                            <input name="btn_recalificar" type="button" id="btn_recalificar" value="Re-Calificar" onClick=""></td>
                                    	</tr>			-->

                                </table>
                                    <?
//*****************************INICIO CALCULO HASH AUTOMATICO ********************************//

                                    $sql12 = "SELECT mei_usuprevio.nota 
			 FROM mei_usuprevio 
			 WHERE mei_usuprevio.idprevio='" . $_GET['idprevio'] . "'";
                                    $resultado = $baseDatos->ConsultarBD($sql12);
                                    while (list($nota) = mysql_fetch_array($resultado)) {
                                        // echo $nota;
                                        if (empty($notastodos)) {

                                            $notastodos = $nota;
                                        } else {

                                            $notastodos.=";" . $nota;
                                        }
                                    }

                                    $hashAuto = md5($notastodos);


                                    $sql1 = "SELECT mei_integridadant.hash 
			 FROM mei_integridadant 
			 WHERE mei_integridadant.idprevio='" . $_GET['idprevio'] . "'";
                                    $resultado = $baseDatos->ConsultarBD($sql1);
                                    list($hashActividad) = mysql_fetch_array($resultado);

                                    if (empty($hashActividad)) {
                                        $sql = "INSERT INTO mei_integridadant ( idprevio  , hash)
					VALUES ('" . $_GET["idprevio"] . "','" . $hashAuto . "')";
                                        $baseDatos->ConsultarBD($sql);

                                        $sql = "INSERT INTO mei_integridadnew ( idprevio , hash)
					VALUES ('" . $_GET["idprevio"] . "','" . $hashAuto . "')";
                                        $baseDatos->ConsultarBD($sql);
                                    } else {

                                        $sql = "UPDATE mei_integridadant  SET 	hash='" . $hashAuto . "' 
					WHERE mei_integridadant.idprevio='" . $_GET["idprevio"] . "'";
                                        $baseDatos->ConsultarBD($sql);
                                    }

                                    $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                                    $resultado = $baseDatos->ConsultarBD($sql2);
                                    list($hashManual) = mysql_fetch_array($resultado);


                                    if ($hashAuto != $hashManual) {
                                        ?>

                                    &nbsp;
                                    <table class="tablaMarquesina">
                                        <tr align="center" valign="middle">

                                            <td><a>Verificaci&oacute;n Integridad Automatico: </a>
                                                <input type="text" name="hashAuto" size="36" value="<? echo $hashAuto ?>"></td>
                                        </tr>
                                    </table>

                                    <?
                                    $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                                    $resultado = $baseDatos->ConsultarBD($sql2);
                                    list($hashManual) = mysql_fetch_array($resultado);
                                    ?>

                                    &nbsp;
                                    <table class="tablaMarquesina">
                                        <tr align="center" valign="middle">
                                            <td><a href="../actividades/calcularhash.php?ident=3&idprevio=<?= $_GET["idprevio"] ?>&intentos=<?= $intentos ?>&idmateria=<?= $_GET["idmateria"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&esp=<?= $_GET['esp'] ?>&materia=<?= $_GET["materia"] ?>" title="clic para calcular nuevo hash"><blink> Verificaci&oacute;n Integridad Manual:</blink></a></td>
                                        </tr>
                                    </table>
                                    <input type="text" name="hashManual2" size="36" value="<? echo $hashManual ?>">
                                    <?
                                }

                                $sql2 = "SELECT mei_integridadnew.hash
			FROM mei_integridadnew
			WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                                $resultado = $baseDatos->ConsultarBD($sql2);
                                list($hashManual) = mysql_fetch_array($resultado);
                                ?>

                                &nbsp;
                                <table class="tablaMarquesina">
                                    <tr align="center" valign="middle">
                                        <td><a><? if ($hashAuto == $hashManual) { ?> Las notas estan correctas<? } else { ?> Se ha detectado una Violacion de Seguridad, por favor verifique su backup de notas <? } ?></a></td>
                                    </tr>
                                </table>

                                <?
                                //*****************************FIN CALCULO HASH AUTOMATICO ********************************//
                            }

                            // fin de mostrar lista

                            function modCalificar($idgrupo) {
                                global $baseDatos, $idmodC, $modC, $orden, $idtiposubgrupo;
                                ?>
                                <table class="tablaGeneral">
                                    <tr class="trAviso">
                                        <td width="20%"><strong>Modo de Calificación: </strong></td>
                                        <td width="60%"><?= $modC ?>
                                            <input name="hid_modC" type="hidden" id="hid_modC" value="<?= $idmodC ?>"></td>
                                            <?
                                            $sqlcongeva = "SELECT congelar FROM mei_usupreviodtlle WHERE idprevio='".$_GET["idprevio"]."' AND congelar=1";
											$resconge = $baseDatos->ConsultarBD($sqlcongeva);
											list($conge) = mysql_fetch_array($resconge);
											$sqltipopre = "SELECT idtipoprevio FROM mei_evaprevio WHERE idprevio='".$_GET["idprevio"]."' ";
											$restipo = $baseDatos->ConsultarBD($sqltipopre);
											list($tipopre) = mysql_fetch_array($restipo);
											if($tipopre==1){
												$tipopre="Previo";
											}else{
												$tipopre="Quiz";
											}
                                            if(!empty($conge)){
                                            ?>
                                        <td style="color: red;" width="20%"><strong><?=$tipopre?> Congelado </strong></td>
                                        <?
											}
                                        ?>
                                    </tr>
                                </table>

        <?
        if ($_GET['esp'] != 'reposicion')
            $sql = "SELECT mei_evaprevio.intentos FROM mei_evaprevio WHERE mei_evaprevio.idprevio = '" . $_GET["idprevio"] . "'";
        else
            $sql = "SELECT mei_evaquiz.intentos FROM mei_evaquiz WHERE mei_evaquiz.idprevio = '" . $_GET["idprevio"] . "'";
        $res1 = $baseDatos->ConsultarBD($sql);
        list($intentos) = mysql_fetch_row($res1);
		
        //echo "este es el modo de calificar".$idmodC;
        switch ($idmodC) {
            case 1://PROMEDIO
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
						mei_usuario.segundoapellido FROM mei_usuario, mei_relusuvirgru WHERE 
						mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . " AND 
						mei_relusuvirgru.idvirgrupo='" . $_GET['idgrupo'] . "' ORDER BY " . $orden . " ASC";
                } /*else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
						mei_usuario.segundoapellido FROM mei_usuario, mei_relusugru WHERE 
						mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . " AND 
						mei_relusugru.idgrupo='" . $_GET['idgrupo'] . "' ORDER BY " . $orden . " ASC";
                }*/
				else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
						mei_usuario.segundoapellido, mei_evaprevio.intentos FROM mei_usuario, mei_relusugru, mei_evaprevio WHERE 
						mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . " AND 
						mei_relusugru.idgrupo='" . $_GET['idgrupo'] . "' AND mei_evaprevio.idprevio = '" . $_GET["idprevio"] . "' ORDER BY " . $orden . " ASC";
                }
		
                //print $sql."<p>";
                $resultado = $baseDatos->ConsultarBD($sql);

                $notaExtra = calcularNotaExtra($sql, "2");
				
                ?>
                                        <table  class="tablaGeneral">
                                            <tr class="trTitulo">
                                                <td class="trSubTitulo" colspan="9" align="left" valign="middle"><img src="imagenes/usuarios.gif" width="16" height="16"> Lista de Alumnos </td>
                                            </tr>
                                            <tr class="trSubTitulo">
                                                <td class="trSubTitulo" colspan="9" align="left" valign="middle"><input name="chk_grupo" type="checkbox" id="chk_grupo" onClick="javascript:chekear()" value="checkbox">
                                                    Todo el Grupo </td>
                                            </tr>
                                            <tr class="trSubTitulo">
                                                <td class="trSubTitulo" colspan="2" align="center" valign="middle">C&oacute;digo</td>
                                                <td class="trSubTitulo" width="10%" align="center" valign="middle">SubGrupo</td>
                                                <td class="trSubTitulo" width="45%" align="center" valign="middle">Nombre del Alumno</td>
                                                <td class="trSubTitulo" width="20%" align="center" valign="middle">Nota </td>
                                                <td class="trSubTitulo" width="20%" align="center" valign="middle">Nota Extra</td>
                                                <td class="trSubTitulo" width="20%" align="center" valign="middle">Nota Definitiva </td>
                                        <td class="trSubTitulo" width="20%" align="center" valign="middle"><button title="Continuar el previo a todos los estudiantes" onclick="descongelarevamuchos(<?php echo $_GET["idprevio"]; ?>,<?php echo $_GET["idgrupo"]; ?>)" >Continuar Todos</button></td>
						<td class="trSubTitulo" width="20%" align="center" valign="middle"><button title="Congelar el previo a todos los estudiantes" onclick="congelarevamuchos(<?php echo $_GET["idprevio"]; ?>,<?php echo $_GET["idgrupo"]; ?>)">Congelar Todos</button> </td>
                                            </tr>
                                        <?
                                        $i = 0;
                                        while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultado)) {//mei_usuprevio.idusuario, mei_usuprevio.idintento, mei_usuprevio.nota
                                            if ($_GET['esp'] != 'reposicion')
                                                $sql = "SELECT COUNT(*), SUM(mei_usuprevio.nota) FROM mei_usuprevio WHERE mei_usuprevio.idprevio = '" . $_GET["idprevio"] . "'
						AND mei_usuprevio.idusuario = '" . $idusu . "' AND mei_usuprevio.idintento<='" . $intentos . "'";
                                            else
                                                $sql = "SELECT COUNT(*), SUM(mei_usuquiz.nota) FROM mei_usuquiz WHERE mei_usuquiz.idquiz = '" . $_GET["idprevio"] . "'
						AND mei_usuquiz.idusuario = '" . $idusu . "'";
                                            //print $sql."<p>";
                                            if ($_POST["cbo_ordenar"] != 3)
                                                $ncompleto = $pnombre . " " . $snombre . " " . $papellido . " " . $sapellido;
                                            else
                                                $ncompleto = $papellido . " " . $sapellido . " " . $pnombre . " " . $snombre;
                                            //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************

                                            $sql2 = "SELECT mei_subgrupo.nombre FROM mei_subgrupo, mei_relususub 
						WHERE mei_relususub.idsubgrupo = mei_subgrupo.idsubgrupo AND 
						mei_subgrupo.idtiposubgrupo = '" . $idtiposubgrupo . "' AND mei_relususub.idusuario = '" . $idusu . "'";
                                            $res1 = $baseDatos->ConsultarBD($sql2);
                                            list($nomSubgrupo) = mysql_fetch_row($res1);

                                            //******************************************************************************************

                                            if (($i % 2) == 0)
                                                $clase = "trListaOscuro";
                                            else
                                                $clase = "trListaClaro";
                                            $res = $baseDatos->ConsultarBD($sql);
                                            list($cont, $sumNota) = mysql_fetch_row($res);
                                            ?>
                                                <tr class="<?= $clase ?>">
                                                    <td width="6%" align="right" valign="middle" class="<?= $clase ?>"><?= $i + 1 ?>
                                                        <input name="chk_estudiante[]" type="checkbox" id="chk_lista" value="<?= $idusu ?>">				    </td>
                                                    <td width="9%" align="center" valign="middle" class="<?= $clase ?>"><?= $idusu ?></td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><?
                        if (!empty($nomSubgrupo))
                            print $nomSubgrupo;
                        else
                            print "---";
                                            ?></td>
                                                    <td class="<?= $clase ?>" align="left" valign="middle"><?= $ncompleto ?></td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><?
                        if ($cont != 0) {
                            $nota = $sumNota / $cont;
                            print number_format(round(($sumNota / $cont), 2), 1, '.', '');
                        } else {
                            print "0.00";
                            $nota = "0.00";
                        }
                        ?></td>
                        <?php
						    $idprevio = $_GET["idprevio"];
						    
							$sql="SELECT mei_evaprevio.curva FROM mei_evaprevio WHERE mei_evaprevio.idprevio='".$idprevio."'";
							$resu = $baseDatos->ConsultarBD($sql);
							list($cur)=mysql_fetch_row($resu);
							if($cur==0){
								?>
								<td class="<?= $clase ?>" align="center" valign="middle"><a style="text-decoration: none; <?if(number_format(round(($notaExtra[$idusu]), 2), 2, '.', '')!=0){?>color: red;<?}?>" href="javascript:editar('<?=$idusu?>', '<?=$idprevio?>')">
									<?$notaextra_c = number_format(round(($notaExtra[$idusu]), 2), 2, '.', ''); 
													if( $nota == 0 ){ $notaextra_c=0; }
													echo $notaextra_c;?>
								</a></td>
							<?
							}else{
								?>
								<td class="<?= $clase ?>" align="center" valign="middle"><a style="text-decoration: none; <?if(number_format(round(($notaExtra[$idusu]), 2), 2, '.', '')!=0){?>color: red;<?}?>" href="javascript:editar('<?=$idusu?>', '<?=$idprevio?>')">
									<?$notaextra_c = number_format(round(($notaExtra[$idusu]), 2), 2, '.', ''); 
													if( $nota == 0 ){ $notaextra_c=0; }
													echo $notaextra_c;?>
								</a></td>
								<?
							}
							
						    ?>
                                                    <!--<td class="<?= $clase ?>" align="center" valign="middle"><?php 
													 $notaextra_c = number_format(round(($notaExtra[$idusu]), 2), 2, '.', ''); 
													if( $nota == 0 ){ $notaextra_c=0; }
													echo $notaextra_c;
													?></td>-->
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><?php 
														$notafinalextra_c = number_format(round(($notaExtra[$idusu] + $nota), 1), 1, '.', '');
													if( $notafinalextra_c > 5){ $notafinalextra_c = 5.0; }
													//if( $nota == 0 ){ $notafinalextra_c=0; }
													echo $notafinalextra_c;													
													?></td>
                                                    <td><input type="button" value="Continuar" onclick="descongelareva(<?php echo $idusu; ?>, <?php echo $_GET["idprevio"]; ?>)" /></td>
                                                
                                        <!-- _________________________________________________________________________________________________________________________ -->
<td><input type="button" value="Congelar" onclick="congelareva(<?php echo $idusu; ?>, <?php echo $_GET["idprevio"]; ?>)" /></td>
<!-- _________________________________________________________________________________________________________________________ -->

                                                
                                                </tr>
                    <?
                    $i++;
                }
                ?>
			
                                            <tr class="trSubTitulo">
                                                <td class="trSubTitulo" colspan="9" align="center" valign="middle"><input name="btn_recalificar" type="button" id="btn_recalificar" value="Re-Calificar" onClick="javascript:recalificar('<?= $_POST["cbo_intento"] ?>')"></td>
                                            </tr>				
<!--
					    <tr class="trSubTitulo">
                                                <td class="trSubTitulo" colspan="8" align="center" valign="middle"><input name="btn_recalificar" type="button" id="btn_recalificar" value="Re-Calificar" onClick=""></td>
                                            </tr>	-->

                                        </table>
                                            <?
                                            //*****************************INICIO CALCULO HASH AUTOMATICO ********************************//

                                            $sql12 = "SELECT mei_usuprevio.nota 
			 FROM mei_usuprevio 
			 WHERE mei_usuprevio.idprevio='" . $_GET['idprevio'] . "'";
                                            $resultado = $baseDatos->ConsultarBD($sql12);
                                            while (list($nota) = mysql_fetch_array($resultado)) {
                                                // echo $nota;
                                                if (empty($notastodos)) {

                                                    $notastodos = $nota;
                                                } else {

                                                    $notastodos.=";" . $nota;
                                                }
                                            }

                                            $hashAuto = md5($notastodos);


                                            $sql1 = "SELECT mei_integridadant.hash 
			 FROM mei_integridadant 
			 WHERE mei_integridadant.idprevio='" . $_GET['idprevio'] . "'";
                                            $resultado = $baseDatos->ConsultarBD($sql1);
                                            list($hashActividad) = mysql_fetch_array($resultado);

                                            if (empty($hashActividad)) {
                                                $sql = "INSERT INTO mei_integridadant ( idprevio  , hash)
					VALUES ('" . $_GET["idprevio"] . "','" . $hashAuto . "')";
                                                $baseDatos->ConsultarBD($sql);

                                                $sql = "INSERT INTO mei_integridadnew ( idprevio , hash)
					VALUES ('" . $_GET["idprevio"] . "','" . $hashAuto . "')";
                                                $baseDatos->ConsultarBD($sql);
                                            } else {

                                                $sql = "UPDATE mei_integridadant  SET 	hash='" . $hashAuto . "' 
					WHERE mei_integridadant.idprevio='" . $_GET["idprevio"] . "'";
                                                $baseDatos->ConsultarBD($sql);
                                            }



                                            $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                                            $resultado = $baseDatos->ConsultarBD($sql2);
                                            list($hashManual) = mysql_fetch_array($resultado);
                                            if ($hashAuto != $hashManual) {
                                                ?>

                                            &nbsp;
                                            <table class="tablaMarquesina">
                                                <tr align="center" valign="middle">

                                                    <td><a>Verificación Integridad Automatico: </a><input type="text" name="hashAuto" size="36" value="<? echo $hashAuto ?>"></td>
                                                </tr>
                                            </table>

                                            <?
                                            $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                                            $resultado = $baseDatos->ConsultarBD($sql2);
                                            list($hashManual) = mysql_fetch_array($resultado);
                                            ?>

                                            &nbsp;
                                            <table class="tablaMarquesina">
                                                <tr align="center" valign="middle">
                                                    <td><a href="../actividades/calcularhash.php?ident=3&idprevio=<?= $_GET["idprevio"] ?>&intentos=<?= $intentos ?>&idmateria=<?= $_GET["idmateria"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&esp=<?= $_GET['esp'] ?>&materia=<?= $_GET["materia"] ?>" title="clic para calcular nuevo hash"><blink>Verificación Integridad Manual:</blink></a><input type="text" name="hashManual" size="36" value="<? echo $hashManual ?>"></td>
                                                </tr>
                                            </table>
                                            <?
                                        }
                                        $sql2 = "SELECT mei_integridadnew.hash
			FROM mei_integridadnew
			WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                                        $resultado = $baseDatos->ConsultarBD($sql2);
                                        list($hashManual) = mysql_fetch_array($resultado);
                                        ?>
                                        &nbsp;
                                        <table class="tablaMarquesina">
                                            <tr align="center" valign="middle">
                                                <td><a><? if ($hashAuto == $hashManual) { ?> Las notas estan correctas<? } else { ?> Se ha detectado una Violacion de Seguridad, por favor verifique su backup de notas <? } ?></a></td>
                                            </tr>
                                        </table>

                                        <?
                                        //*****************************FIN CALCULO HASH AUTOMATICO ********************************//

                                        break;
                                    case 2://PRIMER INTENTO
                                        if ($_GET['esp'] != 'reposicion') {
                                            if ($_SESSION['idtipousuario'] == 5) {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuprevio.nota, MIN(mei_usuprevio.idintento) FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuprevio ON mei_relusuvirgru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            } else {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuprevio.nota, MIN(mei_usuprevio.idintento) FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuprevio ON mei_relusugru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            }
                                        } else {
                                            if ($_SESSION['idtipousuario'] == 5) {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.nota FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuquiz ON mei_relusuvirgru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY mei_usuario.idusuario ASC";
                                            } else {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.nota FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuquiz ON mei_relusugru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY mei_usuario.idusuario ASC";
                                            }
                                        }
                                        //print "<br><br>".$sql."<p>";
                                        $resultado = $baseDatos->ConsultarBD($sql);

                                        $notaExtra = calcularNotaExtra($sql, "1");
                                        mostrarLista($resultado, $notaExtra);

                                        break;
                                    case 3://ULTIMO INTENTO
                                        if ($_GET['esp'] != 'reposicion') {
                                            if ($_SESSION['idtipousuario'] == 5) {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuprevio.idintento),
		mei_usuprevio.nota FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuprevio ON mei_relusuvirgru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $_GET["idprevio"] . " AND mei_usuprevio.idintento<='" . $intentos . "'
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_relusuvirgru.idvirgrupo=$idgrupo AND
		mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            } else {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuprevio.idintento),
		mei_usuprevio.nota FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuprevio ON mei_relusugru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $_GET["idprevio"] . " AND mei_usuprevio.idintento<='" . $intentos . "'
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_relusugru.idgrupo=$idgrupo AND
		mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            }
                                        } else {
                                            if ($_SESSION['idtipousuario'] == 5) {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.nota FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuquiz ON mei_relusuvirgru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY mei_usuario.idusuario ASC";
                                            } else {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.nota FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuquiz ON mei_relusugru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY mei_usuario.idusuario ASC";
                                            }
                                        }

                                        //print $sql."----caso 3 <p>";
                                        $resultado = $baseDatos->ConsultarBD($sql);
                                        $notaExtra = calcularNotaExtra($sql, "3");
                                        ?>
                                        <table  class="tablaGeneral">
                                            <tr class="trTitulo">
                                                <td class="trSubTitulo" colspan="9" align="left" valign="middle"><img src="imagenes/usuarios.gif" width="16" height="16">Lista de Alumnos </td>
                                            </tr>
                                            <tr class="trSubTitulo">
                                                <td class="trSubTitulo" colspan="9" align="left" valign="middle">
                                                    <input name="chk_grupo" type="checkbox" id="chk_grupo" onClick="javascript:chekear()" value="checkbox"> Todo el Grupo </td>
                                            </tr>
                                            <tr class="trSubTitulo">
                                                <td class="trSubTitulo" colspan="2" align="center" valign="middle">C&oacute;digo</td>
                                                <td class="trSubTitulo" width="10%" align="center" valign="middle">SubGrupo</td>
                                                <td class="trSubTitulo" width="45%" align="center" valign="middle">Nombre del Alumno</td>
                                                <td class="trSubTitulo" width="20%" align="center" valign="middle">Nota </td>
                                                <td class="trSubTitulo" width="20%" align="center" valign="middle">Nota Extra</td>
                                                <td class="trSubTitulo" width="20%" align="center" valign="middle">Nota Definitiva </td>
                                        <td class="trSubTitulo" width="20%" align="center" valign="middle"><button title="Continuar el previo a todos los estudiantes" onclick="descongelarevamuchos(<?php echo $_GET["idprevio"]; ?>,<?php echo $_GET["idgrupo"]; ?>)" >Continuar Todos</button></td>
						<td class="trSubTitulo" width="20%" align="center" valign="middle"><button title="Congelar el previo a todos los estudiantes" onclick="congelarevamuchos(<?php echo $_GET["idprevio"]; ?>,<?php echo $_GET["idgrupo"]; ?>)">Congelar Todos</button> </td>
                                            </tr>
                                        <?
                                        $i = 0;
                                        while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultado)) {//while list inicio
                                            if ($_GET['esp'] != 'reposicion')
                                                $sql1 = "SELECT mei_usuprevio.nota FROM mei_usuprevio WHERE mei_usuprevio.idusuario = '" . $idusu . "'
						AND mei_usuprevio.idprevio = '" . $_GET["idprevio"] . "' AND
						mei_usuprevio.idintento = '" . $idintento . "'";
                                            else
                                                $sql1 = "SELECT mei_usuquiz.nota FROM mei_usuquiz WHERE mei_usuquiz.idusuario = '" . $idusu . "'
						AND mei_usuquiz.idquiz = '" . $_GET["idprevio"] . "'";
                                            //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************

                                            $sql = "SELECT mei_subgrupo.nombre FROM mei_subgrupo, mei_relususub 
						WHERE mei_relususub.idsubgrupo = mei_subgrupo.idsubgrupo AND 
						mei_subgrupo.idtiposubgrupo = '" . $idtiposubgrupo . "' AND mei_relususub.idusuario = '" . $idusu . "'";
                                            $res1 = $baseDatos->ConsultarBD($sql);
                                            list($nomSubgrupo) = mysql_fetch_row($res1);
                                            //echo $sql1;
                                            //******************************************************************************************
                                            if ($_POST["cbo_ordenar"] != 3)
                                                $ncompleto = $pnombre . " " . $snombre . " " . $papellido . " " . $sapellido;
                                            else
                                                $ncompleto = $papellido . " " . $sapellido . " " . $pnombre . " " . $snombre;

                                            if (($i % 2) == 0)
                                                $clase = "trListaOscuro";
                                            else
                                                $clase = "trListaClaro";
                                            $res = $baseDatos->ConsultarBD($sql1);
                                            list($nota) = mysql_fetch_row($res);
											
                                            ?>
                                                <tr class="<?= $clase ?>">
                                                    <td width="6%" align="right" valign="middle" class="<?= $clase ?>"><?= $i + 1 ?>
                                                        <input name="chk_estudiante[]" type="checkbox" id="chk_lista" value="<?= $idusu ?>"></td>
                                                    <td width="9%" align="center" valign="middle" class="<?= $clase ?>"><?= $idusu ?></td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><?
                        if (!empty($nomSubgrupo))
                            print $nomSubgrupo;
                        else
                            print "---";
                                            ?></td>
                                                    <td class="<?= $clase ?>" align="left" valign="middle"><?= $ncompleto ?></td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><?
                        if (!empty($nota))
                            print number_format(round(($nota), 2), 1, '.', '');
                        else
                            print "0.00";
                                            ?></td>
<!------------------------------------------------ nuevo codigo ------------------------------------------------------------------------>
						   <?php
						    $idprevio = $_GET["idprevio"];
						    
							$sql="SELECT mei_evaprevio.curva FROM mei_evaprevio WHERE mei_evaprevio.idprevio='".$idprevio."'";
							$resu = $baseDatos->ConsultarBD($sql);
							list($cur)=mysql_fetch_row($resu);
							if($cur==0){
								?>
								<td class="<?= $clase ?>" align="center" valign="middle"><a style="text-decoration: none; <?if(number_format(round(($notaExtra[$idusu]), 2), 2, '.', '')!=0) {?>color: red;<?}?>" href="javascript:editar('<?=$idusu?>', '<?=$idprevio?>')"><blink><?= number_format(round(($notaExtra[$idusu]), 2), 2, '.', '') ?></blink></a></td>
							<?
							}else{
								?>
									<td class="<?= $clase ?>" align="center" valign="middle"><a style="text-decoration: none; <?if(number_format(round(($notaExtra[$idusu]), 2), 2, '.', '') !=0) {?>color: red;<?}?>" href="javascript:editar('<?=$idusu?>', '<?=$idprevio?>')"><blink><?= number_format(round(($notaExtra[$idusu]), 2), 2, '.', '') ?></blink></a></td>
							
								<?
							}
							
						    ?>
<!------------------------------------------------ nuevo codigo ------------------------------------------------------------------------>
                                                   <!-- <td class="<?= $clase ?>" align="center" valign="middle"><?= number_format(round(($notaExtra[$idusu] + $nota), 1), 1, '.', '') ?></td>-->
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><?php 
														$notafinalextra_c = number_format(round(($notaExtra[$idusu] + $nota), 1), 1, '.', '');
													if( $notafinalextra_c > 5){ $notafinalextra_c = 5.0; }
													//if( $nota == 0 ){ $notafinalextra_c=0; }
													echo $notafinalextra_c;													
													?></td>
                                                    
                                                    
                                                    <td><input type="button" value="Continuar" onclick="descongelareva(<?php echo $idusu; ?>, <?php echo $_GET["idprevio"]; ?>)" /></td>
                                                
                                                <!-- _________________________________________________________________________________________________________________________ -->
<td><input type="button" value="Congelar" onclick="congelareva(<?php echo $idusu; ?>, <?php echo $_GET["idprevio"]; ?>)" /></td>
<!-- _________________________________________________________________________________________________________________________ -->

                                                
                                                </tr>
                                            <?
                                            $i++;
                                        }//aqui es el fin del while list donde hacen la busqueda de la variable $idusu
                                        ?>
		
                                            <tr class="trSubTitulo">
                                                <td class="trSubTitulo" colspan="9" align="center" valign="middle"><input name="btn_recalificar" type="button" id="btn_recalificar" value="Re-Calificar" onClick="javascript:recalificar('<?= $_POST["cbo_intento"] ?>')"></td>
                                            </tr>			
<!--
					    <tr class="trSubTitulo">
                                                <td class="trSubTitulo" colspan="8" align="center" valign="middle"><input name="btn_recalificar" type="button" id="btn_recalificar" value="Re-Calificar" onClick=""></td>
                                            </tr>		-->

                                        </table>
                                        <?
                                        //*****************************INICIO CALCULO HASH AUTOMATICO ********************************//

                                        $sql12 = "SELECT mei_usuprevio.nota 
			 FROM mei_usuprevio 
			 WHERE mei_usuprevio.idprevio='" . $_GET['idprevio'] . "'";
                                        $resultado = $baseDatos->ConsultarBD($sql12);
                                        while (list($nota) = mysql_fetch_array($resultado)) {
                                            // echo $nota;
                                            if (empty($notastodos)) {

                                                $notastodos = $nota;
                                            } else {

                                                $notastodos.=";" . $nota;
                                            }
                                        }

                                        $hashAuto = md5($notastodos);


                                        $sql1 = "SELECT mei_integridadant.hash 
			 FROM mei_integridadant 
			 WHERE mei_integridadant.idprevio='" . $_GET['idprevio'] . "'";
                                        $resultado = $baseDatos->ConsultarBD($sql1);
                                        list($hashActividad) = mysql_fetch_array($resultado);

                                        if (empty($hashActividad)) {
                                            $sql = "INSERT INTO mei_integridadant ( idprevio  , hash)
					VALUES ('" . $_GET["idprevio"] . "','" . $hashAuto . "')";
                                            $baseDatos->ConsultarBD($sql);

                                            $sql = "INSERT INTO mei_integridadnew ( idprevio , hash)
					VALUES ('" . $_GET["idprevio"] . "','" . $hashAuto . "')";
                                            $baseDatos->ConsultarBD($sql);
                                        } else {

                                            $sql = "UPDATE mei_integridadant  SET 	hash='" . $hashAuto . "' 
					WHERE mei_integridadant.idprevio='" . $_GET["idprevio"] . "'";
                                            $baseDatos->ConsultarBD($sql);
                                        }

                                        $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                                        $resultado = $baseDatos->ConsultarBD($sql2);
                                        list($hashManual) = mysql_fetch_array($resultado);

                                        if ($hashAuto == $hashManual) {
                                            ?>

                                            &nbsp;
                                            <table class="tablaMarquesina">
                                                <tr align="center" valign="middle">

                                                    <td><a>Verificación Integridad Automatico: </a><input type="text" name="hashAuto" size="36" value="<? echo $hashAuto ?>"></td>
                                                </tr>
                                            </table>

                    <?
                    $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                    $resultado = $baseDatos->ConsultarBD($sql2);
                    list($hashManual) = mysql_fetch_array($resultado);
                    ?>

                                            &nbsp;
                                            <table class="tablaMarquesina">
                                                <tr align="center" valign="middle">
                                                    <td><a href="../actividades/calcularhash.php?ident=3&idprevio=<?= $_GET["idprevio"] ?>&intentos=<?= $intentos ?>&idmateria=<?= $_GET["idmateria"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&esp=<?= $_GET['esp'] ?>&materia=<?= $_GET["materia"] ?>"title="clic para calcular nuevo hash"><blink>Verificación Integridad Manual:</blink></a><input type="text" name="hashManual" size="36" value="<? echo $hashManual ?>"></td>
                                                </tr>
                                            </table>
                    <?
                    $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                    $resultado = $baseDatos->ConsultarBD($sql2);
                    list($hashManual) = mysql_fetch_array($resultado);
                }
                ?>
                                        &nbsp;
                                        <table class="tablaMarquesina">
                                            <tr align="center" valign="middle">
                                                <td><a><? if ($hashAuto == $hashManual) { ?> Las notas estan correctas<? } else { ?> Se ha detectado una Violacion de Seguridad, por favor verifique su backup de notas <? } ?></a></td>
                                            </tr>
                                        </table>

                                        <?
                                        //*****************************FIN CALCULO HASH AUTOMATICO ********************************//

                                        break;
                                    //NOTA MAS BAJA
                                    case 4:
                                        if ($_GET['esp'] != 'reposicion') {
                                            if ($_SESSION['idtipousuario'] == 5) {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MIN(mei_usuprevio.nota),mei_usuprevio.idintento FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuprevio ON mei_relusuvirgru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            } else {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MIN(mei_usuprevio.nota), mei_usuprevio.idintento FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuprevio ON mei_relusugru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            }
                                        } else {
                                            if ($_SESSION['idtipousuario'] == 5) {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MIN(mei_usuquiz.nota) FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuquiz ON mei_relusuvirgru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            } else {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MIN(mei_usuquiz.nota) FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuquiz ON mei_relusugru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            }
                                        }

                                        $resultado = $baseDatos->ConsultarBD($sql);

                                        $notaExtra = calcularNotaExtra($sql, "1");
                                        mostrarLista($resultado, $notaExtra);
                                        break;
                                    case 5://NOTA MAS ALTA
                                        if ($_GET['esp'] != 'reposicion') {
                                            if ($_SESSION['idtipousuario'] == 5) {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuprevio.nota), mei_usuprevio.idintento FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuprevio ON mei_relusuvirgru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            } else {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuprevio.nota), mei_usuprevio.idintento FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuprevio ON mei_relusugru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            }
                                        } else {
                                            if ($_SESSION['idtipousuario'] == 5) {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuquiz.nota) FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuquiz ON mei_relusuvirgru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            } else {
                                                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuquiz.nota) FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuquiz ON mei_relusugru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY " . $orden . " ASC";
                                            }
                                        }
                                        //print $sql."<p>";
                                        $resultado = $baseDatos->ConsultarBD($sql);
                                        $notaExtra = calcularNotaExtra($sql, "1");
                                        mostrarLista($resultado, $notaExtra);
                                        break;
                                }
                            }

// fin de modo calificar
                            if ($_SESSION['idtipousuario'] == 5) {
                                $sql = "SELECT mei_evavirgrupo.idvirgrupo FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo
		WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND
		mei_evavirgrupo.idevaluacion = mei_evaluacion.idevaluacion AND
		mei_evaprevio.idprevio = '" . $_GET["idprevio"] . "'";
                            } else {
                                $sql = "SELECT mei_evagrupo.idgrupo FROM mei_evaprevio, mei_evaluacion, mei_evagrupo
		WHERE mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND
		mei_evagrupo.idevaluacion = mei_evaluacion.idevaluacion AND
		mei_evaprevio.idprevio = '" . $_GET["idprevio"] . "'";
                            }
                            //print $sql."<p>";
                            $rgrupo = $baseDatos->ConsultarBD($sql);
                            list ($idgrupo) = mysql_fetch_row($rgrupo);

                            if ($_POST["cbo_intento"] != 0) {// si selecciona un intento diferente de la definitiva
                                if ($_GET['esp'] != 'reposicion') {
                                    if ($_SESSION['idtipousuario'] == 5) {
                                        $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuprevio.fechainicio, mei_usuprevio.nota FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuprevio ON mei_relusuvirgru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio = " . $_GET["idprevio"] . " AND mei_usuprevio.idintento = '" . $_POST["cbo_intento"] . "'
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=" . $_GET['idgrupo'] . "
		ORDER BY " . $orden . " ASC";
                                    } else {
                                        $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuprevio.fechainicio, mei_usuprevio.nota FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuprevio ON mei_relusugru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio = " . $_GET["idprevio"] . " AND mei_usuprevio.idintento = '" . $_POST["cbo_intento"] . "'
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=" . $_GET['idgrupo'] . "
		ORDER BY " . $orden . " ASC";
                                    }
                                } else {
                                    if ($_SESSION['idtipousuario'] == 5) {
                                        $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.fecharespuesta, mei_usuquiz.nota FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuquiz ON mei_relusuvirgru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . " WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario
		AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . " AND mei_relusuvirgru.idvirgrupo=" . $_GET['idgrupo'] . " ORDER BY " . $orden . " ASC";
                                    } else {
                                        $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.fecharespuesta, mei_usuquiz.nota FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuquiz ON mei_relusugru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $_GET["idprevio"] . " WHERE mei_usuario.idusuario = mei_relusugru.idusuario
		AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . " AND mei_relusugru.idgrupo=" . $_GET['idgrupo'] . " ORDER BY " . $orden . " ASC";
                                    }
                                }

                                //print $sql;
                                $result = $baseDatos->ConsultarBD($sql);
                                //$notaExtra=calcularNotaExtra($sql);
                                //mostrarLista($resultado);
                                if (mysql_num_rows($result) > 0) {//HAY ALUMNOS EN EL GRUPO
                                    ?>
                                    <table border="1" class="tablaGeneral">
                                        <tr class="trTitulo">
                                            <td colspan="9" align="left" valign="middle" class="trSubTitulo"><img src="imagenes/usuarios.gif" width="16" height="16">Lista de Alumnos </td>
                                        </tr>
                                        <tr class="trSubTitulo">
                                            <td colspan="9" align="left" valign="middle" class="trSubTitulo">
                                                <input name="chk_grupo" type="checkbox" id="chk_grupo" onClick="javascript:chekear()" value="checkbox"> Todo el Grupo </td>
                                        </tr>
                                        <tr class="trSubTitulo">
                                            <td class="trSubTitulo" colspan="2" align="center" valign="middle">C&oacute;digo</td>
                                            <td class="trSubTitulo" width="5%" align="center" valign="middle">Sub-<br>Grupo</td>
                                            <td class="trSubTitulo" width="30%" align="center" valign="middle">Nombre del Alumno</td>
                                            <td class="trSubTitulo" width="30%" align="center" valign="middle">Fecha de Respuesta </td>
                                            <td class="trSubTitulo" width="5%" align="center" valign="middle">Nota</td>
                                            <td class="trSubTitulo" width="5%" align="center" valign="middle">Ver Intento </td>
                                            <td class="trSubTitulo" width="5%" align="center" valign="middle">Editar</td>
                                            <td class="trSubTitulo" width="5%" align="center" valign="middle">Eliminar</td>
                                        </tr>
                                    <?
                                    $i = 0;
                                    while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $fecha_r, $nota) = mysql_fetch_row($result)) {
                                    //while ( $crap = mysql_fetch_row($result) )
                                    //foreach(mysql_fetch_row($result) as $crap )
                                        if ($_POST["cbo_ordenar"] != 3)
                                            $ncompleto = $pnombre . " " . $snombre . " " . $papellido . " " . $sapellido;
                                        else
                                            $ncompleto = $papellido . " " . $sapellido . " " . $pnombre . " " . $snombre;

                                        if (($i % 2) == 0)
                                            $clase = "trListaOscuro";
                                        else
                                            $clase = "trListaClaro";

                                        //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************

                                        $sql = "SELECT mei_subgrupo.nombre FROM mei_subgrupo, mei_relususub 
									WHERE mei_relususub.idsubgrupo = mei_subgrupo.idsubgrupo AND 
									mei_subgrupo.idtiposubgrupo = '" . $idtiposubgrupo . "' AND mei_relususub.idusuario = '" . $idusu . "'";
                                        $res1 = $baseDatos->ConsultarBD($sql);
                                        list($nomSubgrupo) = mysql_fetch_row($res1);

                                        //******************************************************************************************
                                        ?>
                                            <tr class="<?= $clase ?>">
                                                <td class="<?= $clase ?>" width="6%" align="right" valign="middle"><?= $i + 1 ?>
                                                    <input name="chk_estudiante[]" type="checkbox" id="chk_lista" value="<?= $idusu ?>"></td>
                                                <td class="<?= $clase ?>" width="9%" align="center" valign="middle"><?= $idusu ?></td>
                                                <td class="<?= $clase ?>" align="center" valign="middle">
                                        <?
                                        if (!empty($nomSubgrupo))
                                            print $nomSubgrupo;
                                        else
                                            print "---";
                                        ?></td>
                                                <td class="<?= $clase ?>" align="left" valign="middle"><?= $ncompleto ?></td>
                                        <?
                                        if (!empty($fecha_r)) {
                                            ?>

                                                    <td class="<?= $clase ?>" align="center" valign="middle"><?= mostrarFechaTexto($fecha_r, 0) ?></td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><?= number_format($nota, 1, '.', '') ?></td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><a href="verIntentoEvaluacion.php?idusuario=<?= $idusu ?>&idmateria=<?= $_GET["idmateria"] ?>&idprevio=<?= $_GET["idprevio"] ?>&intentos=<?= $_GET["intentos"] ?>&cbo_intento=<?= $_POST["cbo_intento"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&esp=<?= $_GET["esp"] ?>&materia=<?= $_GET["materia"] ?>" class="link">Ver</a></td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><a href="javascript:modificar('INS','<?= $idusu ?>','<?= $_GET["idprevio"] ?>','<?= $_GET["idmateria"] ?>','<?= number_format($nota, 1, '.', '') ?>','<?= $_GET["intentos"] ?>','<?= $_POST["cbo_intento"] ?>')"><img src="../preguntasFrecuentes/imagenes/modificar.gif" alt="Modificar Nota" width="16" height="16" border="0"></a></td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><a onClick="return confirm('Borrar intento.')" href="eliminarIntento.php?accion=DEL&idprevio=<?= $_GET["idprevio"] ?>&idmateria=<?= $_GET["idmateria"] ?>&idusuario=<?= $idusu ?>&intentos=<?= $_GET["intentos"] ?>&cbo_intento=<?= $_POST["cbo_intento"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&esp=<?= $_GET["esp"] ?>&materia=<?= $_GET['materia'] ?>"><img src="../preguntasFrecuentes/imagenes/eliminar.gif" alt="Eliminar Nota" width="16" height="16" border="0"></a></td>
                                            <?
                                        } else {
                                            ?>
                                                    <td class="<?= $clase ?>" align="center" valign="middle">---</td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle">---</td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle">---</td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle"><a href="javascript:modificar('INS','<?= $idusu ?>','<?= $_GET["idprevio"] ?>','<?= $_GET["idmateria"] ?>','<?= number_format($nota, 1, '.', '') ?>','<?= $_GET["intentos"] ?>','<?= $_POST["cbo_intento"] ?>')"><img src="imagenes/undo.jpg" width="16" height="16" border="0" alt="Insertar Nota"></a></td>
                                                    <td class="<?= $clase ?>" align="center" valign="middle">---</td>
                                            <?
                                        }
                                        ?>
                                            </tr>
                                        <?
                                        $i++;
                                    }
                                    ?>

                                        <tr class="trSubTitulo">
                                            <td class="trSubTitulo" colspan="9" align="center" valign="middle"><input name="btn_recalificar" type="button" id="btn_recalificar" value="Re-Calificar" onClick="javascript:recalificar('<?= $_POST["cbo_intento"] ?>')">
                                                <input name="btn_eliminar" type="button" id="btn_eliminar" value="Eliminar" onClick="javascript:eliminar('<?= $_POST["cbo_intento"] ?>')"></td>
                                        </tr>						
<!--
					<tr class="trSubTitulo">
                                            <td class="trSubTitulo" colspan="9" align="center" valign="middle"><input name="btn_recalificar" type="button" id="btn_recalificar" value="Re-Calificar" onClick="">
                                                <input name="btn_eliminar" type="button" id="btn_eliminar" value="Eliminar" onClick="javascript:eliminar('<?= $_POST["cbo_intento"] ?>')"></td>
                                        </tr>				-->

                                    </table>
                                    <?
                                    //*****************************INICIO CALCULO HASH AUTOMATICO ********************************//

                                    $sql12 = "SELECT mei_usuprevio.nota 
			 FROM mei_usuprevio 
			 WHERE mei_usuprevio.idprevio='" . $_GET['idprevio'] . "'";
                                    $resultado = $baseDatos->ConsultarBD($sql12);
                                    while (list($nota) = mysql_fetch_array($resultado)) {
                                        // echo $nota;
                                        if (empty($notastodos)) {

                                            $notastodos = $nota;
                                        } else {

                                            $notastodos.=";" . $nota;
                                        }
                                    }

                                    $hashAuto = md5($notastodos);


                                    $sql1 = "SELECT mei_integridadant.hash 
			 FROM mei_integridadant 
			 WHERE mei_integridadant.idprevio='" . $_GET['idprevio'] . "'";
                                    $resultado = $baseDatos->ConsultarBD($sql1);
                                    list($hashActividad) = mysql_fetch_array($resultado);

                                    if (empty($hashActividad)) {
                                        $sql = "INSERT INTO mei_integridadant ( idprevio  , hash)
					VALUES ('" . $_GET["idprevio"] . "','" . $hashAuto . "')";
                                        $baseDatos->ConsultarBD($sql);

                                        $sql = "INSERT INTO mei_integridadnew ( idprevio , hash)
					VALUES ('" . $_GET["idprevio"] . "','" . $hashAuto . "')";
                                        $baseDatos->ConsultarBD($sql);
                                    } else {

                                        $sql = "UPDATE mei_integridadant  SET 	hash='" . $hashAuto . "' 
					WHERE mei_integridadant.idprevio='" . $_GET["idprevio"] . "'";
                                        $baseDatos->ConsultarBD($sql);
                                    }

                                    $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                                    $resultado = $baseDatos->ConsultarBD($sql2);
                                    list($hashManual) = mysql_fetch_array($resultado);


                                    if ($hashAuto != $hashManual) {
                                        ?>

                                        &nbsp;
                                        <table class="tablaMarquesina">
                                            <tr align="center" valign="middle">

                                                <td><a>Verificación Integridad Automatico: </a><input type="text" name="hashAuto" size="36" value="<? echo $hashAuto ?>"></td>
                                            </tr>
                                        </table>

                <?
                $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                $resultado = $baseDatos->ConsultarBD($sql2);
                list($hashManual) = mysql_fetch_array($resultado);
                ?>

                                        &nbsp;
                                        <table class="tablaMarquesina">
                                            <tr align="center" valign="middle">
                                                <td><a href="../actividades/calcularhash.php?ident=3&idprevio=<?= $_GET["idprevio"] ?>&intentos=<?= $intentos ?>&idmateria=<?= $_GET["idmateria"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&esp=<?= $_GET['esp'] ?>&materia=<?= $_GET["materia"] ?>"  title="clic para calcular nuevo hash"><blink>Verificación Integridad Manual:</blink></a><input type="text" name="hashManual" size="36" value="<? echo $hashManual ?>"></td>
                                            </tr>
                                        </table>
                                            <?
                                            $sql2 = "SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idprevio='" . $_GET['idprevio'] . "'";
                                            $resultado = $baseDatos->ConsultarBD($sql2);
                                            list($hashManual) = mysql_fetch_array($resultado);
                                        }
                                        ?>
                                    &nbsp;
                                    <table class="tablaMarquesina">
                                        <tr align="center" valign="middle">
                                            <td><a><? if ($hashAuto == $hashManual) { ?> Las notas estan correctas<? } else { ?> Se ha detectado una Violacion de Seguridad, por favor verifique su backup de notas <? } ?></a></td>
                                        </tr>
                                    </table>

                                                <?
                                                //*****************************FIN CALCULO HASH AUTOMATICO ********************************//
                                            }//NO HAY ALUMNOS EN EL GRUPO
                                            else {
                                                ?>&nbsp;
                                    <table class="tablaGeneral">
                                        <tr>
                                            <td> No hay Alumnos matriculados en Este Grupo</td>
                                        </tr>
                                    </table>
                                            <?
                                        }
                                    }else
                                        modCalificar($idgrupo,$tipoprevio);
                                    ?>&nbsp;			  
                        </form>
                        <table class="tablaGeneral">
                            <tr class="trSubTitulo" align="center" valign="middle">
                                <td><a href="verDtlleEvaluacion.php?idprevio=<?= $_GET["idprevio"] ?>&idmateria=<?= $_GET["idmateria"] ?>&idgrupo=<?= $_GET["idgrupo"] ?>&materia=<?= $_GET["materia"] ?>" class="link">Volver</a></td>
                            </tr>
                        </table>
                    </td>
                    <td class="tablaEspacio">&nbsp;</td>
                    <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                    <td class="tablaEspacioDerecho">&nbsp;</td>
                </tr>
            </table>
        </body>
    </html>
                                    <?
                                }
                                else
                                    redireccionar('../login/');
                                ?>
