<?
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('evaclass.php');

if(comprobarSession())
{	
$baseDatos=new BD();
list ($idmateria, $materia) = explode("*", $_POST["hid_materia"]);

	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
	{
		if (empty($_POST["cbo_valor"])) $valor = 0;
		else $valor = $_POST["cbo_valor"];
		//*************************ARREGLO DE LA FECHA***********************************
		list($agno_a, $mes_a, $dia_a) = explode("-",$_POST['txt_fecha_a']);		
		if ($mes_a<10) $mes_a = "0".$mes_a;
		if ($dia_a<10) $dia_a = "0".$dia_a;
		
		list($agno_f, $mes_f, $dia_f) = explode("-",$_POST['txt_fecha_f']);		
		if ($mes_f<10) $mes_f = "0".$mes_f;
		if ($dia_f<10) $dia_f = "0".$dia_f;
		//**********************************************************************************
		
		$fecha_a = $agno_a.$mes_a.$dia_a.$_POST["cbo_hora_a"].$_POST["cbo_minuto_a"];		
		$fecha_f = $agno_f.$mes_f.$dia_f.$_POST["cbo_hora_f"].$_POST["cbo_minuto_f"];		
		
		/*print $fecha_a."<p>";
		print $fecha_f."<p>";*/
		
		
	                   
		//**********************************************************************************
	
		/*if ( $_GET["modo"] == 'E' )
		{
			$sql = "UPDATE mei_evaprevio SET
					idevaluacion = '".$_POST['cbo_nota']."',
					titulo = '".$_POST['txt_titulo']."',
					fechaactivacion = '".$fecha_a."',
					fechafinalizacion = '".$fecha_f."',
					comentario = '".base64_encode($_POST['edt_comentario'])."',
					direccionIp1 = '".$_POST['direccionIp1']."',
					direccionIp2 = '".$_POST['direccionIp2']."',
					tiempo = '".round($_POST['txt_tiempo'])."',
					barajarpreg = '".$_POST['cbo_bpreguntas']."',
					barajarresp = '".$_POST['cbo_brespuestas']."',
					intentos = '".$_POST['cbo_intentos']."',
					valorIntento = '".round(($_POST["cbo_valorIntento"]/100),2)."',
					idmodCalificar = '".$_POST["cbo_calificar"]."',
					mostrarnota = '".$_POST["cbo_mnota"]."',
					estado = '".$_POST['cbo_estado']."',
					valor = '".$valor."',
					idtiposubgrupo = ".$_POST["cbo_subgrupo"]."
					WHERE mei_evaprevio.idprevio = '".$_POST['hid_previo']."'";
			$baseDatos->ConsultarBD($sql);
			$idprevio = $_POST['hid_previo'];
			redireccionar("llenarEvaluacion.php");
		}*/
		
		if ( $_GET["modo"] == 'E' )
		{
			$sql = "UPDATE mei_evaquiz SET
					idevaluacion = '".$_POST['cbo_nota']."',
					titulo = '".$_POST['txt_titulo']."',
					fechaactivacion = '".$fecha_a."',
					fechafinalizacion = '".$fecha_f."',
					comentario = '".base64_encode($_POST['edt_comentario'])."',
					clave= '".$_POST['txt_clave']."',
					tiempo = '".round($_POST['txt_tiempo'])."',
					barajarpreg = '".$_POST['cbo_bpreguntas']."',
					barajarresp = '".$_POST['cbo_brespuestas']."',
					intentos = '".$_POST['cbo_intentos']."',
					valorIntento = '".round(($_POST["cbo_valorIntento"]/100),2)."',
					idmodCalificar = '".$_POST["cbo_calificar"]."',
					mostrarnota = '".$_POST["cbo_mnota"]."',
					estado = '".$_POST['cbo_estado']."',
					valor = '".$valor."',
					idtiposubgrupo = ".$_POST["cbo_subgrupo"]."
					WHERE mei_evaprevio.idprevio = '".$_POST['hid_previo']."'";
			$baseDatos->ConsultarBD($sql);
			$idprevio = $_POST['hid_previo'];
			redireccionar("llenarQuiz.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
		}
		elseif($_GET["modo"] == 'DEL')
		{
			$sql = "DELETE FROM mei_evaquiz WHERE mei_evaprevio.idprevio=".$_GET["idprevio"];
			$baseDatos->ConsultarBD($sql);
			
			redireccionar("verEvaluacion.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
		}
		elseif($_GET["modo"] == 'ACT')
		{
			$sql = "UPDATE mei_evaquiz SET 
					mei_evaprevio.estado = '".$_GET["estado"]."'  
					WHERE mei_evaprevio.idprevio = '".$_GET["idprevio"]."'";
			$baseDatos->ConsultarBD($sql);
			//print $sql;
			if ($_GET["tipo"]=='Q')
				redireccionar("../actividades/verActividad.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
			else
				redireccionar("verEvaluacion.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
		}
		else
		{
		/*$sql = "INSERT INTO `mei_evaprevio` ( `idprevio` , `idevaluacion` , `idautor` , `titulo` , `fechacreacion` , 
					`fechaactivacion` , `fechafinalizacion` , `comentario` , `tiempo` , `barajarpreg` , `barajarresp` , 
					`intentos` , `valorIntento` , `idmodCalificar` , `mostrarnota` , `estado` , `clave`,`idtipoprevio` , `valor`, `idtiposubgrupo` )
					VALUES ('','".$_POST["cbo_nota"]."', 
					'".$_SESSION["idusuario"]."', '".$_POST["txt_titulo"]."', '".date('YmdHi')."', '".$fecha_a."',
					'".$fecha_f."', '".base64_encode($_POST["edt_comentario"])."', '".round($_POST["txt_tiempo"])."',
					'".$_POST["cbo_bpreguntas"]."', '".$_POST["cbo_brespuestas"]."', '".$_POST["cbo_intentos"]."', 
					'".round(($_POST["cbo_valorIntento"]/100),2)."', '".$_POST["cbo_calificar"]."', '".$_POST["cbo_mnota"]."', 
					'".$_POST["cbo_estado"]."', '".base64_encode($_POST['txt_clave'])."', '".$_POST["hid_tipoprevio"]."', '".$valor."', ".$_POST["cbo_subgrupo"].")";*/
		
		
		
	
		$sql = "INSERT INTO `mei_evaquiz` ( `idprevio` , `idevaluacion` , `idautor` , `titulo` , `fechacreacion` , 
					`fechaactivacion` , `fechafinalizacion` , `comentario` ,`clave` , `tiempo` , `barajarpreg` , `barajarresp` , 
					`intentos` , `valorIntento` , `idmodCalificar` , `mostrarnota` , `estado` , `idtipoprevio` , `valor`, `idtiposubgrupo` )
					VALUES ('','".$_POST["cbo_nota"]."', 
					'".$_SESSION["idusuario"]."', '".$_POST["txt_titulo"]."', '".date('YmdHi')."', '".$fecha_a."',
					'".$fecha_f."', '".base64_encode($_POST["edt_comentario"])."','".$_POST["txt_clave"]."', '".round($_POST["txt_tiempo"])."',
					'".$_POST["cbo_bpreguntas"]."', '".$_POST["cbo_brespuestas"]."', '".$_POST["cbo_intentos"]."', 
					'".round(($_POST["cbo_valorIntento"]/100),2)."', '".$_POST["cbo_calificar"]."', '".$_POST["cbo_mnota"]."', 
					'".$_POST["cbo_estado"]."', '".$_POST["hid_tipoprevio"]."', '".$valor."', ".$_POST["cbo_subgrupo"].")"; 
			

			/* el de las ip
			$sql = "INSERT INTO `mei_evaprevio` ( `idprevio` , `idevaluacion` , `idautor` , `titulo` , `fechacreacion` , 
					`fechaactivacion` , `fechafinalizacion` , `comentario` ,`direccionIp1` ,`direccionIp2` ,`tiempo` , `barajarpreg` , `barajarresp` , 
					`intentos` , `valorIntento` , `idmodCalificar` , `mostrarnota` , `estado` , `idtipoprevio` , `valor`, `idtiposubgrupo` )
					VALUES ('','".$_POST["cbo_nota"]."', 
					'".$_SESSION["idusuario"]."', '".$_POST["txt_titulo"]."', '".date('YmdHi')."', '".$fecha_a."',
					'".$fecha_f."', '".base64_encode($_POST["edt_comentario"])."','".$cadenafinal."' ,'".$_POST["direccionIp2"]."', '".round($_POST["txt_tiempo"])."',
					'".$_POST["cbo_bpreguntas"]."', '".$_POST["cbo_brespuestas"]."', '".$_POST["cbo_intentos"]."', 
					'".round(($_POST["cbo_valorIntento"]/100),2)."', '".$_POST["cbo_calificar"]."', '".$_POST["cbo_mnota"]."', 
					'".$_POST["cbo_estado"]."', '".$_POST["hid_tipoprevio"]."', '".$valor."', ".$_POST["cbo_subgrupo"].")";*/
					
			$baseDatos->ConsultarBD($sql);
			$idprevio = $baseDatos->InsertIdBD();
			//print $sql;
			$evaluacion = new evaluacion($idprevio,$idmateria,$_POST["hid_tipoprevio"],'');	
			$_SESSION["evaluacion"] = $evaluacion;
			redireccionar("llenarQuiz.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
		}
	}	
	else redireccionar('../login/');
}	
?>

