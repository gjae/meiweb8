<?
include_once ('../librerias/validar.lib.php');
include_once ('../librerias/estandar.lib.php');
include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	
if(comprobarSession())
{//IF COMPROBARSESION 
	//echo $_POST["cbo_tema"];
	$_SESSION['tema']=$_POST["cbo_tema"];
	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
	{//IF SESSION
		//PROCEDIMIENTO PARA INGRESAR LA PREGUNTA
		list ($idtipo) = explode("-",$_POST["hid_tipopreg"]);	
		/*if ( $_POST["cbo_autoeva"] == 'SI') $autoeva = 1;
		else $autoeva = 0;*/
		
		if ( $_POST["hid_modo"] == 'E' )
		{//EDITANDO PREGUNTAS
			
			if ($idtipo==4) $pregunta = base64_encode($_POST["edt_pregunta"]."[$$$]".$_POST["edt_razon"]);
			else $pregunta = base64_encode($_POST["edt_pregunta"]);
			
			$idtema = $_POST["cbo_tema"];
			$idsubtemapost = $_POST["cbo_subtema_post"];
			$idsubtemacambiado = $_POST["cbo_sub_tema"];
			$tema = $idtema;
			if( $idsubtemapost != "" ){
				$tema = $idsubtemapost;
			}
			if( $idsubtemacambiado != "" ){
				$tema = $idsubtemacambiado;
				if($idsubtemacambiado == 0){
					$tema = $idtema;
				}
			}
			
			$sql = "UPDATE mei_evapreguntas SET 
					idgrado = '".$_POST["cbo_gradoP"]."',
					idtema = '".$tema."',
					autoevaluacion = '".$_POST["cbo_autoeva"]."',
					pregunta = '".$pregunta."'
					WHERE mei_evapreguntas.idpregunta = '".$_POST["hid_pregunta"]."'";
			
			$baseDatos->ConsultarBD($sql);
			if (($idtipo!=5)&&($idtipo!=6)){
			while ( list($key, $val) = each( $_POST["txt_respuesta"] ) )
			{//WHILE 1				
				/*print "Num. respuesta: ".$_POST["hid_respuesta"][$key]."<p>";
				print "respuesta: ".$val."<p>";*/
				
				if ( $idtipo !=2 )				
				{
					while ( list(,$nom) = each( $_POST["rad_respuesta"] ) )
					{
						if ( $nom == $key ){
							$valor = 1;
							break;
						}else $valor = 0;
					}
					reset($_POST["rad_respuesta"]);
	
				}
				else
				{
					$valor = $_POST["respuesta"][$key];
				}
				/*print "Valor: ".$valor."<p>";
				print "------------------------------------------------------------------------------<p>";*/
				
				$sql = "UPDATE mei_evarespuestas SET
						respuesta = '".base64_encode($val)."',
						valor = '".$valor."' 
						WHERE mei_evarespuestas.idrespuesta = '".$_POST["hid_respuesta"][$key]."'";
				$baseDatos->ConsultarBD($sql);
				//print $sql."<p>";				
			}//FIN WHILE 1		
			}
			elseif ( $idtipo ==6 )				
			{ 
				while ( list($key, $val) = each( $_POST["txt_respuesta"] ) ){
					$sql = "UPDATE mei_evarespuestas SET
							respuesta = '".base64_encode($val)."'
							WHERE mei_evarespuestas.idrespuesta = '".$_POST["hid_respuesta"][$key]."'";
					$baseDatos->ConsultarBD($sql);			
					
					$sql = "UPDATE mei_evapreguntasrelacioncolumna SET
							columna = '".base64_encode($_POST["preguntacolumna"][$key])."',
							relacion = '".base64_encode($_POST["preguntarelacion"][$key])."'
							WHERE mei_evapreguntasrelacioncolumna.idrespuesta = '".$_POST["hid_respuesta"][$key]."'";
					$baseDatos->ConsultarBD($sql);	
				}
			}
		}//FIN EDITANDO PREGUNTAS
		elseif($_GET["accion"]=="DEL")
		{
			$sql = "DELETE FROM mei_evapreguntas WHERE mei_evapreguntas.idpregunta='".$_GET["idpregunta"]."'";
			$baseDatos->ConsultarBD($sql);
			$sql = "DELETE FROM mei_evapreguntasvalidar WHERE mei_evapreguntasvalidar.idpregunta='".$_GET["idpregunta"]."'";
			$baseDatos->ConsultarBD($sql);
			if(!empty($_GET['validar'])){
			redireccionar('validarPreguntas.php?idmateria='.$_GET["idmateria"]."&materia=".$_GET['materia']);	
			}else{
			redireccionar('verPregunta.php?idmateria='.$_GET["idmateria"]."&materia=".$_GET['materia']."&tema=".$_GET["tema"]);
			}	
		}
		else
		{//INSERTANDO LA PREGUNTA

			/*Agregar Campo*/
			$exists = false;
			$db = "mei_evapreguntas";
			$column = "clonid";
			$column_attr = "int(11) null";
			$columns = "show columns from $db";
			$resultcolumn = $baseDatos->ConsultarBD($resultcolumn);
			while($c = mysql_fetch_assoc($resultcolumn)){
				if($c['Field'] == $column){
					$exists = true;
					break;
				}
			}        
			if(!$exists){
				$sql = "ALTER TABLE `$db` ADD `$column`  $column_attr";
			}					
			$baseDatos->ConsultarBD($sql);		
			/*Fin Agregar Campo*/	

			/*CREAR TABLA RELACION DE COLUMNAS*/
			$tablarelacion="CREATE TABLE IF NOT EXISTS `mei_evapreguntasrelacioncolumna` (
				  `idrelacioncolumna` int(11) NOT NULL AUTO_INCREMENT,
				  `idpregunta` int(11) NOT NULL,
				  `idrespuesta` int(11) NOT NULL,
				  `columna` text NOT NULL,
				  `relacion` text NOT NULL,
				  `valor` float(11) NOT NULL,
				  PRIMARY KEY (`idrelacioncolumna`)
				)";
			$rescreartablarelacion = $baseDatos->ConsultarBD($tablarelacion);
			/*FIN TABLA RELACION DE COLUMNAS*/			
			
			if ( $idtipo == 4 ) $textopreg = base64_encode($_POST["edt_pregunta"]."[$$$]".$_POST["edt_razon"]);
			else $textopreg = base64_encode($_POST["edt_pregunta"]);
			
			$_POST["cbo_autoeva"] = $_POST["cbo_autoeva"] == 'SI' ? 1 : 0;
			$estado=1;
			if($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7){
			$_POST["cbo_autoeva"] = 1;
			$estado=0;	
			}
			$idtema = $_POST["cbo_tema"];
			$idclonpregunta = $_POST["hid_idclonpregunta"];
			$idsubtema1 = $_POST["cbo_subtema"];
			$idsubtema2 = $_POST["cbo_sub_tema"];
			$idsubtema = "";
			if(!empty($idsubtema2)){
				$idsubtema = $idsubtema2;
			}else{
				$idsubtema = $idsubtema1;
			}
			
			if(!empty($idsubtema)){
				$sql = "INSERT INTO `mei_evapreguntas` ( `idpregunta` , `idgrado` , `tipo_pregunta` ,
						`idusuario` , `idtema` , `autoevaluacion` , `pregunta`, `clonid`, `estado` ) VALUES ('',
						'".$_POST["cbo_gradoP"]."','$idtipo', '".$_SESSION['idusuario']."', 
						'".$idsubtema."', '".$_POST["cbo_autoeva"]."', '".$textopreg."', '".$idclonpregunta."','".$estado."')";			
			}
			else{
				$sql = "INSERT INTO `mei_evapreguntas` ( `idpregunta` , `idgrado` , `tipo_pregunta` ,
						`idusuario` , `idtema` , `autoevaluacion` , `pregunta`, `clonid`, `estado` ) VALUES ('',
						'".$_POST["cbo_gradoP"]."','$idtipo', '".$_SESSION['idusuario']."', 
						'".$_POST["cbo_tema"]."', '".$_POST["cbo_autoeva"]."', '".$textopreg."', '".$idclonpregunta."','".$estado."')";				
			}
			if ( $baseDatos->ConsultarBD($sql) )
			{//IF 3
				$idpregunta = $baseDatos->InsertIdBD();
				//print "Listo Pregunta<p>";
				
				if($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7){
					
				$sql="SELECT MAX(idpregunta) FROM mei_evapreguntas";	
				$maxidpre=$baseDatos->ConsultarBD($sql);
				list($idpre)=mysql_fetch_array($maxidpre);
				
				$sql="SELECT idtema FROM mei_evapreguntas WHERE idpregunta='".$idpre."'";
				$idtem=$baseDatos->ConsultarBD($sql);
				list($tema)=mysql_fetch_array($idtem);
				
				$sql="SELECT idmateria, idusuario FROM mei_tema WHERE idtema='".$tema."'";
				$resul=$baseDatos->ConsultarBD($sql);
				list($idmat,$idprofe)=mysql_fetch_array($resul);
					
				$sql = "INSERT INTO `mei_evapreguntasvalidar` ( `idpregunta` , `idautor` , `tipo_pregunta` ,
						`idmateria` , `idprofesor` ) VALUES ('".$idpre."',
						'".$_SESSION['idusuario']."','$idtipo', '$idmat', 
						'$idprofe')";			
				$baseDatos->ConsultarBD($sql);
				}
				
				//PROCEDIMIENTO PARA INGRESAR LA(S) RESPUESTAS			
				
				if ( ($idtipo == 1) or ($idtipo == 3) or ($idtipo == 4) )
				{//IF TIPOS DE PREGUNTAS 1,3
					while ( list($key, $val) = each( $_POST["txt_respuesta"] ) )
					{//WHILE 1
						while ( list(,$nom) = each( $_POST["rad_respuesta"] ) )
						{
							if ( $nom == $key ){
								$ok = 1;
								break;
							}else $ok = 0;
						}
						reset($_POST["rad_respuesta"]);
						
						//INSERTANDO LA(S) RESPUESTA(S)
						$sql = "INSERT INTO `mei_evarespuestas` ( `idrespuesta` , `idpregunta` , 
								`respuesta` , `valor` ) VALUES ('', '".$idpregunta."', 
								'".base64_encode($val)."', '$ok')";
								
						$baseDatos->ConsultarBD($sql);
						//print "Listo Respuesta<p>";
					}//FIN WHILE 1			
				}//FIN IF TIPOS DE PREGUNTAS 1,3
				elseif ($idtipo == 2)
				{//IF TIPOS DE PREGUNTAS 2
					while ( list($key, $val) = each( $_POST["txt_respuesta"] ) )
					{//WHILE 1
						$sql = "INSERT INTO `mei_evarespuestas` ( `idrespuesta` , `idpregunta` , 
								`respuesta` , `valor` ) VALUES ('', '".$idpregunta."', 
								'".base64_encode($val)."', '".$_POST["respuesta"][$key]."')";
								
						$baseDatos->ConsultarBD($sql);
					}
				}//FIN IF TIPOS DE PREGUNTAS 2	
				elseif ($idtipo == 6)
				{//IF TIPOS DE PREGUNTAS 6
				
					$columna = array_filter($_POST["preguntacolumna"]); 
					$relacion = array_filter($_POST["preguntarelacion"]); 
					$respuesta = array_filter($_POST["txt_respuesta"]); 
					
					 $valorrespuesta = 1/count($respuesta);
					
					while ( list($keycolumna, $valcolumna) = each( $columna ) )
					{//WHILE 1
						$sql = "INSERT INTO `mei_evarespuestas` ( `idrespuesta` , `idpregunta` , 
								`respuesta` , `valor` ) VALUES ('', '".$idpregunta."', 
								'".base64_encode($_POST["txt_respuesta"][$keycolumna])."', '".$valorrespuesta."')";
	
						$baseDatos->ConsultarBD($sql);
						
						$idrespuesta = $baseDatos->InsertIdBD();
						$sql = "INSERT INTO `mei_evapreguntasrelacioncolumna` ( `idrelacioncolumna` , `idpregunta` , `idrespuesta`, 
								`columna` , `relacion` , `valor` ) VALUES ('', '".$idpregunta."', '".$idrespuesta."', 
								'".base64_encode($valcolumna)."', '".base64_encode($_POST["preguntarelacion"][$keycolumna])."', '".$valorrespuesta."')";
						
						$baseDatos->ConsultarBD($sql);
						

					}
				}//FIN IF TIPOS DE PREGUNTAS 6				
			}//FIN IF 3
		}//FIN INSERTANDO LA PREGUNTA
		if($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7){
		redireccionar('verEvaluacion.php?idmateria='.$_POST["hid_materia"]."&materia=".$_GET['materia']);
		}
		else {
		if(!empty($_GET['validar'])){
		redireccionar('validarPreguntas.php?idmateria='.$_POST["hid_materia"]."&materia=".$_GET['materia']);	
		}else{
		redireccionar('verPregunta.php?idmateria='.$_POST["hid_materia"]."&materia=".$_GET['materia']."&tema=".$_POST["cbo_tema"]);
		}
		}
	}//FIN IF SESSION
	else
	{
		redireccionar('../login/');					
	}			
}//FIN IF COMPROBARSESION
else redireccionar('../login/');		
?>