<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');
	
 
$baseDatos=new BD();
if(comprobarSession())
	{//if comprobar sesion
	
	?>	
<head>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Documento sin t&iacute;tulo</title>
</head>
<script>
	function activar(estado, idactividad)
	{
			location.replace('guardarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idactividad='+idactividad+'&modo=ACT&estado='+estado);
	}
	function activarvisibilidad(estado, idactividad,visibilidad)
	{
			location.replace('guardarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idactividad='+idactividad+'&modo=ACTVISI&estado='+estado+'&visibilidad='+visibilidad);
	}	
	function activarvisibilidadQ(estado, idprevio, visibilidad,reposicion)
	{
		if(!reposicion)
			location.replace('../evaluaciones/guardarEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&modo=ACTVISI&estado='+estado+'&idprevio='+idprevio+'&tipo=Q&reposicion=1&visibilidad='+visibilidad);
		else
			location.replace('../evaluaciones/guardarEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&modo=ACTVISI&estado='+estado+'&idprevio='+idprevio+'&tipo=Q&reposicion=0&visibilidad='+visibilidad);
	}
	
	function activarQ(estado, idprevio, reposicion)
	{
		if(!reposicion)
			location.replace('../evaluaciones/guardarEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&modo=ACT&estado='+estado+'&idprevio='+idprevio+'&tipo=Q&reposicion=1');
		else
			location.replace('../evaluaciones/guardarEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&modo=ACT&estado='+estado+'&idprevio='+idprevio+'&tipo=Q&reposicion=0');
		
}
function cambiarestado(idprevio,i,value) {

		//alert (idprevio+" "+i+" "+value);
		$.ajax({
    					data:  ({ idprevio : idprevio }),
    					url:   '../evaluaciones/cambiarestadoEvaluaciones.php',
    					type:  'post',
    					
    					
    			});	
		if (value=="imagenes/activo.gif") {
			//alert('desactiva');
			document.getElementById(i).value="imagenes/inactivo.gif";
			document.getElementById(i).src="imagenes/inactivo.gif";
			
		}
		else  {
			//alert('activa');
			document.getElementById(i).value="imagenes/activo.gif";
			document.getElementById(i).src="imagenes/activo.gif";
			
		}
}
function cambiarestadoActividad(idactividad,i,value) {

		//alert (idactividad+i+value);
		$.ajax({
    					data:  ({ idactividad : idactividad }),
    					url:   'cambiarestadoActividad.php',
    					type:  'post',
    					
    					
    			});	
		if (value=="imagenes/activo.gif") {
			//alert('desactiva');
			document.getElementById(i).value="imagenes/inactivo.gif";
			document.getElementById(i).src="imagenes/inactivo.gif";
			
		}
		else  {
			//alert('activa');
			document.getElementById(i).value="imagenes/activo.gif";
			document.getElementById(i).src="imagenes/activo.gif";
			
		}
}
function cambiarvisibilidad(idprevio,i,value) {

		//alert (idprevio+" "+i+" "+value);
			$.ajax({
    					data:  ({ idprevio : idprevio }),
    					url:   'cambiarvisibilidadQ.php',
    					type:  'post',
    					
    					
    			});	
		if (value=="imagenes/activo.gif") {
			//alert('desactiva');
			document.getElementById(i).value="imagenes/inactivo.gif";
			document.getElementById(i).src="imagenes/inactivo.gif";
			
		}
		else  {
			//alert('activa');
			document.getElementById(i).value="imagenes/activo.gif";
			document.getElementById(i).src="imagenes/activo.gif";
			
		}
}
function cambiarvisibilidadActividad(idactividad,i,value) {

		//alert (idactividad+i+value);
		$.ajax({
    					data:  ({ idactividad : idactividad }),
    					url:   'cambiarvisibilidadActividad.php',
    					type:  'post',
    					
    					
    			});	
		if (value=="imagenes/activo.gif") {
			//alert('desactiva');
			document.getElementById(i).value="imagenes/inactivo.gif";
			document.getElementById(i).src="imagenes/inactivo.gif";
			
		}
		else  {
			//alert('activa');
			document.getElementById(i).value="imagenes/activo.gif";
			document.getElementById(i).src="imagenes/activo.gif";
			
		}
}

</script>
<body>
<?php
	/*Agregar Campo*/
	$exists = false;
	$db = "mei_actividad";
	$column = "visibilidad";
	$column_attr = "int(11) null";
	$columns = "show columns from $db";
	$resultcolumn = $baseDatos->ConsultarBD($resultcolumn);
	while($c = mysql_fetch_assoc($resultcolumn)){
		if($c['Field'] == $column){
			$exists = true;
			break;
		}
	}        
	if(!$exists){
		$sql = "ALTER TABLE `$db` ADD `$column`  $column_attr";
	}					
	$baseDatos->ConsultarBD($sql);		
	/*Fin Agregar Campo*/
	
	/*Agregar Campo*/
	$exists2 = false;
	$db2 = "mei_evaprevio";
	$column2 = "visibilidad";
	$column_attr2 = "int(11) null";
	$columns = "show columns from $db2";
	$resultcolumn2 = $baseDatos->ConsultarBD($resultcolumn2);
	while($c = mysql_fetch_assoc($resultcolumn2)){
		if($c['Field'] == $column2){
			$exists2 = true;
			break;
		}
	}        
	if(!$exists2){
		$sql = "ALTER TABLE `$db2` ADD `$column2`  $column_attr2";
	}					
	$baseDatos->ConsultarBD($sql);		
	/*Fin Agregar Campo*/	
?>
<table height="248" style="width:100%;" border="0">
	<tr valign="top">
        <td class="tablaEspacio">&nbsp;</td>
        <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacio">&nbsp;</td>
    	<td>
			<table class="tablaMarquesina" >
				<tr>
<?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($materia) = mysql_fetch_row($resultado); 
?>
		 <td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($materia))?></a><a> -> Actividades</a></td>
		  </tr>
		</table>
		<?
/*echo "<script>window.alert('".$_SESSION['idtipousuario']."')</script>";*/
if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==7 || $_SESSION['idtipousuario']==5)
{//SELECCION DEL GRUPO

	/*print "Ahora: ".date("YmdGi")."<p>";
	$m = date("YmdGi", mktime(17,0,0,6,25,2006) );
	print "FEcha previo".$m*/

?>
<form name="frm1" method="post" action="../actividades/verActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>">
	  &nbsp;
      <table class="tablaGeneral" border="0">
        <tr class="trSubTitulo">		
          <td width="25%">Ordenar por el Grupo  : </td>
          <td width="75%"><select class="link" name="cbo_orden" id="cbo_orden" onChange="javascrip:document.frm1.submit()">
			              <option value="*">--Todos--</option>
<? 
				if( $_SESSION['idtipousuario']==5)
				{
				$sql = "SELECT mei_virgrupo.idvirgrupo, mei_virgrupo.nombre FROM `mei_virgrupo`, mei_relusuvirgru
						WHERE mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo AND 
						mei_relusuvirgru.idusuario=".$_SESSION["idusuario"]." AND 
						mei_virgrupo.idvirmateria=".$_GET["idmateria"];
			}
			else 
			{
				$sql = "SELECT mei_grupo.idgrupo, mei_grupo.nombre FROM `mei_grupo`, mei_relusugru
						WHERE mei_grupo.idgrupo = mei_relusugru.idgrupo AND 
						mei_relusugru.idusuario=".$_SESSION["idusuario"]." AND 
						mei_grupo.idmateria=".$_GET["idmateria"];
			}
					
				$resultado = $baseDatos->ConsultarBD($sql);
				if(isset($_GET["cbo_orden"])){$opcionorden = $_GET["cbo_orden"];}
				else{ $opcionorden = $_POST["cbo_orden"]; }				
				while ( list ($idgrupo, $grupo) = mysql_fetch_row($resultado) )
				{					
					print "<option value = '$idgrupo-$grupo'";
					if ( $opcionorden == ($idgrupo."-".$grupo))
						print " selected>";
					else print ">";
					print "$grupo</option>";

				}
				
			?>		
            </select>          </td>
        </tr>
      </table>
     </form>
<table class="tablaMarquesina" width="100%" border="0">
  <tr class="">
    <td align="center" valign="middle">
	<?php if(isset($_POST["cbo_orden"]) && $_POST["cbo_orden"] != "*"):  ?>
		<a href="ingresarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_grupo=<?=$_POST["cbo_orden"]?>&estado=3&coeva=0" class="link"><img src="imagenes/ver.gif" alt="Taller" border="0"> Crear Actividad </a></td>
		<td align="center" valign="middle"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"> Volver </td>
	<?php  
	     elseif (isset($_GET["cbo_orden"]) && $_GET["cbo_orden"] != "*"):  ?>
		<a href="ingresarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_grupo=<?=$_GET["cbo_orden"]?>&estado=3&coeva=0" class="link"><img src="imagenes/ver.gif" alt="Taller" border="0"> Crear Actividad </a></td>
		<td align="center" valign="middle"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"> Volver </td>
	<?php  
	     else:  ?>
		<a href="ingresarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&coeva=0" class="link"><img src="imagenes/ver.gif" alt="Taller" border="0"> Crear Actividad </a></td>
		<td align="center" valign="middle"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"> Volver </td>
	<?php  endif; ?>
  </tr>
</table>	 
<? 
}//FIN SELECCION DEL GRUPO





function verActividad($sql, $idgrupo, $j)
{//FUNCION verActividad





	global $baseDatos, $banaux;
	$resactividad = $baseDatos->ConsultarBD($sql);

	$sql="SELECT mei_virmateria.fechafin FROM mei_virmateria 
			WHERE mei_virmateria.idvirmateria=".$_GET['idmateria']."";
			$consulta=$baseDatos->ConsultarBD($sql);
			list($fecha)=mysql_fetch_array($consulta);
			
			$hoy=date("Y-m-d");
			
	if($fecha>=$hoy){
			
 
	if ( mysql_num_rows($resactividad)>0 )
	{
		
		
		
		
		//Modificación para mostrar solo los módulos que ha superado el promedio requerido

$sql2="SELECT mei_virmateria.notamin FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria']."";
$consultanota=$baseDatos->ConsultarBD($sql2);
list($notamin)=mysql_fetch_array($consultanota);

$sql1="SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema WHERE mei_tema.idmateria =".$_GET['idmateria']."
			AND mei_tema.tipo = 1 ORDER BY(mei_tema.orden)";
			$consultatemas=$baseDatos->ConsultarBD($sql1);
			while(list($idtema,$nomtema)=mysql_fetch_array($consultatemas)){
				
				$promedio=0;
				
				$sql1="SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion
				WHERE mei_evaluacion.idtema='".$idtema."'";
				$consulta=$baseDatos->ConsultarBD($sql1);
				list($ideva)=mysql_fetch_array($consulta);
				
				$sql1="SELECT COUNT(mei_actividad.idactividad) FROM mei_actividad 
				WHERE mei_actividad.idevaluacion='".$ideva."' AND mei_actividad.visibilidad!=1";
				$consulta1=$baseDatos->ConsultarBD($sql1);
				list($cant1)=mysql_fetch_array($consulta1);
				$sql1="SELECT COUNT(mei_evaprevio.idprevio) FROM mei_evaprevio 
				WHERE mei_evaprevio.idevaluacion='".$ideva."' AND mei_evaprevio.visibilidad!=1";
				$consulta2=$baseDatos->ConsultarBD($sql1);
				list($cant2)=mysql_fetch_array($consulta2);
				
				$cantidad=$cant1+$cant2;
				
				
				$sql11="SELECT mei_actividad.idactividad, mei_actividad.valor FROM mei_actividad 
				WHERE mei_actividad.idevaluacion='".$ideva."' AND mei_actividad.visibilidad!=1";
				$consultaactividades=$baseDatos->ConsultarBD($sql11);
				
				$sql22="SELECT mei_evaprevio.idprevio, mei_evaprevio.valor FROM mei_evaprevio 
				WHERE mei_evaprevio.idevaluacion='".$ideva."' AND mei_evaprevio.visibilidad!=1";
				$consultaprevios=$baseDatos->ConsultarBD($sql22);
				
				while(list($idactividad,$valoractividad)=mysql_fetch_array($consultaactividades)){
					$sql2="SELECT mei_usuactividad.nota FROM mei_usuactividad WHERE 
					mei_usuactividad.idactividad='".$idactividad."' AND mei_usuactividad.idusuario='".$_SESSION["idusuario"]."'";
					$consulta=$baseDatos->ConsultarBD($sql2);
					list($notaactividad)=mysql_fetch_array($consulta);
					if($valoractividad==0){
						$promedio+=$notaactividad*(1/$cantidad);
					}
					else {
						$promedio+=$notaactividad*$valoractividad;
					}
					
				}
				while(list($idprevio,$valorprevio)=mysql_fetch_array($consultaprevios)){
					$sql2="SELECT MAX(mei_usuprevio.nota), mei_usuprevio.notaextra FROM mei_usuprevio WHERE 
					mei_usuprevio.idprevio='".$idprevio."' AND mei_usuprevio.idusuario='".$_SESSION["idusuario"]."'";
					$consulta=$baseDatos->ConsultarBD($sql2);
					list($notaprevio)=mysql_fetch_array($consulta);
					if($valorprevio==0){
						$promedio+=$notaprevio*(1/$cantidad);
					}
					else {
						$promedio+=$notaprevio*$valorprevio;
					}
					
				}
				$promediotemas[$idtema]=$promedio;
				
			}

//Cierra la modificación
		
		
		
		$notaProm = 0;
		
		while( list($ideva, $nomeva, $promediar) = mysql_fetch_row($resactividad) )
		{//WHILE $resactividad
		
		
		
		$sql2="SELECT mei_evaluacion.idtema FROM mei_evaluacion
		WHERE mei_evaluacion.idevaluacion='".$ideva."'";
		$tema=$baseDatos->ConsultarBD($sql2);
		list($idtema)=mysql_fetch_array($tema);
		
		
?>&nbsp;
		<table class="tablaGeneral" width="100%" border="0">
		  <tr class="trSubTitulo" style="background:#99B3FF;">
			<td align="left" valign="middle">Nota: <?=$nomeva?></td>
		  </tr>
	  </table>
<?		
		$sql = "SELECT DISTINCT(mei_evadtlleprevio.idprevio) FROM mei_evadtlleprevio UNION SELECT DISTINCT(mei_evadtllequiz.idprevio) FROM mei_evadtllequiz";
		//print $sql."<p>";
		$evadtlle = $baseDatos->ConsultarBD($sql);
		$i = 0;
		while ( list($id) = mysql_fetch_row($evadtlle) )
		{
			$idlista[$i] = $id;
			$i++;
		}
		
		if( count($idlista) > 0 )
			$previoLista = implode(',', $idlista);
		else $previoLista = 0;
			/*if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2)
			{//USUARIO ADMINISTRADOR*/
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql = "SELECT COUNT(*)
						FROM mei_actividad, mei_evaluacion, mei_evavirgrupo WHERE mei_actividad.idevaluacion=mei_evaluacion.idevaluacion AND 
						mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND 
						mei_evaluacion.idevaluacion = '".$ideva."'";
				//print $sql."<p>";
				$sql2= "SELECT mei_foro.idforo, mei_foro.mensaje, mei_foro.fechaactivacion, mei_foro.valor
						FROM mei_foro, mei_evaluacion, mei_evavirgrupo WHERE  mei_foro.idevaluacion=mei_evaluacion.idevaluacion 
						AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion
						AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
						ORDER BY mei_foro.fechaactivacion desc";
				//print $sql2."<p>";
				$sql3= "SELECT mei_evaprevio.idprevio, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, 
						mei_evaprevio.fechafinalizacion, mei_evaprevio.valor, mei_evaprevio.estado
						FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo WHERE  mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion 
						AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion
						AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
						ORDER BY mei_evaprevio.fechaactivacion desc";
						
				$sql4= "SELECT eq.idprevio, eq.titulo, eq.fechaactivacion, eq.fechafinalizacion, eq.valor, eq.estado 
						from mei_relusuvirgru ru inner join mei_evaquiz eq on (ru.idusuario = eq.idautor) inner 
						join mei_virgrupo m on (ru.idvirgrupo = m.idvirgrupo) where ru.idvirgrupo = '".$idgrupo."' 
						and m.idvirmateria = ".$_GET['idmateria']." ORDER BY eq.fechaactivacion desc";
		}
		else
			{	
				$sql = "SELECT COUNT(*)
						FROM mei_actividad, mei_evaluacion, mei_evagrupo WHERE mei_actividad.idevaluacion=mei_evaluacion.idevaluacion AND 
						mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = '".$idgrupo."' AND 
						mei_evaluacion.idevaluacion = '".$ideva."'";
				//print $sql."<p>";
				$sql2= "SELECT mei_foro.idforo, mei_foro.mensaje, mei_foro.fechaactivacion, mei_foro.valor
						FROM mei_foro, mei_evaluacion, mei_evagrupo WHERE  mei_foro.idevaluacion=mei_evaluacion.idevaluacion 
						AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion
						AND mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
						ORDER BY mei_foro.fechaactivacion desc";
				//print $sql2."<p>";
				$sql3= "SELECT mei_evaprevio.idprevio, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, 
						mei_evaprevio.fechafinalizacion, mei_evaprevio.valor, mei_evaprevio.estado
						FROM mei_evaprevio, mei_evaluacion, mei_evagrupo WHERE  mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion 
						AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion
						AND mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
						ORDER BY mei_evaprevio.fechaactivacion desc";
				$sql4= "SELECT eq.idprevio, eq.titulo, eq.fechaactivacion, eq.fechafinalizacion, eq.valor, eq.estado from mei_relusugru ru inner join mei_evaquiz eq on (ru.idusuario = eq.idautor) inner join mei_grupo m on (ru.idgrupo = m.idgrupo) where ru.idgrupo = '".$idgrupo."' and m.idmateria = ".$_GET['idmateria']." ORDER BY eq.fechaactivacion desc";
			}
				//print $sql3;
				$res=$baseDatos->ConsultarBD($sql);	
				$resultado2=$baseDatos->ConsultarBD($sql2);
				$resultado3=$baseDatos->ConsultarBD($sql3);
				$resultado4=$baseDatos->ConsultarBD($sql4);
				list($b) = mysql_fetch_row($res);
				if ($promediar == 1) 
				{
					$notaProm = (mysql_num_rows($resultado2) + mysql_num_rows($resultado3) + $b);
					//print $notaProm."<p>";
					if ($notaProm>0) $valorProm = round( (1/$notaProm),3);
					else $valorProm = 1;
				}
			//print $valorProm."<p>";
			//}//USUARIO ADMINISTRADOR
			$sql = "SELECT mei_tipoactividad.idtipoactividad, mei_tipoactividad.tipoactividad FROM mei_tipoactividad";
			$result=$baseDatos->ConsultarBD($sql);		
			$porcentajeNota = 0;
			while ( list($idtipoactividad,$tipoactividad) = mysql_fetch_row($result) )
			{ //While idtipoactividad

				if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $banaux==1 || $_SESSION['idtipousuario']==5)
				{//USUARIO ADMINISTRADOR
					if ($_SESSION['idtipousuario']==5)
					{
						$sql = "SELECT mei_actividad.idactividad, mei_actividad.titulo, mei_actividad.fechaactivacion, mei_actividad.fechafinalizacion,
							mei_actividad.valor, mei_actividad.estado,mei_actividad.fechaprimfinalizacion,mei_actividad.visibilidad  
							FROM mei_actividad, mei_evaluacion, mei_evavirgrupo WHERE mei_actividad.idtipoactividad ='".$idtipoactividad."' 
							AND mei_actividad.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion
							AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
							ORDER BY mei_actividad.fechaactivacion desc";
							
						$sql2= "SELECT mei_foro.idforo, mei_foro.mensaje, mei_foro.fechaactivacion, mei_foro.fechacaducidad, mei_foro.valor, mei_foro.estado
							FROM mei_foro, mei_evaluacion, mei_evavirgrupo WHERE  mei_foro.idevaluacion=mei_evaluacion.idevaluacion 
							AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion
							AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
							ORDER BY mei_foro.fechaactivacion desc";
							
						$sql3= "SELECT mei_evaprevio.idprevio, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, 
							mei_evaprevio.fechafinalizacion, mei_evaprevio.valor, mei_evaprevio.estado,mei_evaprevio.visibilidad 
							FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo WHERE  mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion 
							AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion
							AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
							ORDER BY mei_evaprevio.fechaactivacion desc";

						$sql4= "SELECT eq.idprevio, eq.titulo, eq.fechaactivacion, eq.fechafinalizacion, eq.valor, eq.estado from mei_relusuvirgru ru inner join mei_evaquiz eq on (ru.idusuario = eq.idautor)
								inner join mei_virgrupo m on (ru.idvirgrupo = m.idvirgrupo) where ru.idvirgrupo = '".$idgrupo."' and m.idvirmateria = ".$_GET['idmateria']." ORDER BY eq.fechaactivacion desc";
					}
					else
					{
						$sql = "SELECT mei_actividad.idactividad, mei_actividad.titulo, mei_actividad.fechaactivacion, mei_actividad.fechafinalizacion,
							mei_actividad.valor, mei_actividad.estado,mei_actividad.fechaprimfinalizacion,mei_actividad.visibilidad  
							FROM mei_actividad, mei_evaluacion, mei_evagrupo WHERE mei_actividad.idtipoactividad ='".$idtipoactividad."' 
							AND mei_actividad.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion
							AND mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
							ORDER BY mei_actividad.fechaactivacion desc";
							
						$sql2= "SELECT mei_foro.idforo, mei_foro.mensaje, mei_foro.fechaactivacion, mei_foro.fechacaducidad, mei_foro.valor, mei_foro.estado
							FROM mei_foro, mei_evaluacion, mei_evagrupo WHERE  mei_foro.idevaluacion=mei_evaluacion.idevaluacion 
							AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion
							AND mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
							ORDER BY mei_foro.fechaactivacion desc";
							
						$sql3= "SELECT mei_evaprevio.idprevio, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, 
							mei_evaprevio.fechafinalizacion, mei_evaprevio.valor, mei_evaprevio.estado,mei_evaprevio.visibilidad 
							FROM mei_evaprevio, mei_evaluacion, mei_evagrupo WHERE  mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion 
							AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion
							AND mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_evaluacion.idevaluacion = '".$ideva."'
							ORDER BY mei_evaprevio.fechaactivacion desc";

						$sql4= "SELECT eq.idprevio, eq.titulo, eq.fechaactivacion, eq.fechafinalizacion, eq.valor, eq.estado from mei_relusugru ru inner join mei_evaquiz eq 
								on (ru.idusuario = eq.idautor) inner join mei_grupo m on (ru.idgrupo = m.idgrupo) where ru.idgrupo = '".$idgrupo."' and m.idmateria = ".$_GET['idmateria']." ORDER BY eq.fechaactivacion desc";
					}
				//print $sql2;
				}//USUARIO ADMINISTRADOR
				else 
				{	
					if($_SESSION['idtipousuario']==6)
					{
						$sql = "SELECT mei_actividad.idactividad, mei_actividad.titulo, mei_actividad.fechaactivacion, mei_actividad.fechafinalizacion,
								mei_actividad.valor, mei_actividad.estado,mei_actividad.fechaprimfinalizacion,mei_actividad.visibilidad 
								FROM mei_actividad, mei_evaluacion, mei_evavirgrupo WHERE mei_actividad.idtipoactividad ='".$idtipoactividad."' 
								AND mei_actividad.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion
								AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_actividad.fechaactivacion<'".date('YmdHi')."' 
								AND mei_actividad.fechafinalizacion>'".date('YmdHi')."' AND mei_actividad.estado=1 
								AND mei_evaluacion.idevaluacion = '".$ideva."' 
								ORDER BY mei_actividad.fechaactivacion desc";
							
						$sql2 = "SELECT mei_foro.idforo, mei_foro.mensaje, mei_foro.fechaactivacion, mei_foro.fechacaducidad, mei_foro.valor
								FROM mei_foro, mei_evaluacion, mei_evavirgrupo WHERE mei_foro.idevaluacion=mei_evaluacion.idevaluacion 
								AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion
								AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_foro.fechaactivacion<'".date('Y-m-d')."'
								AND mei_foro.fechacaducidad>'".date('Y-m-d')."' AND mei_foro.estado=1 
								AND mei_evaluacion.idevaluacion = '".$ideva."' 
								ORDER BY mei_foro.fechaactivacion desc";
							
						$sql3 = "SELECT mei_evaprevio.idprevio, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, 
								mei_evaprevio.fechafinalizacion, mei_evaprevio.valor,mei_evaprevio.visibilidad 
								FROM mei_evaprevio, mei_evaluacion, mei_evavirgrupo WHERE mei_evaprevio.idprevio IN (".$previoLista.") AND 
								mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion
								AND mei_evavirgrupo.idvirgrupo = '".$idgrupo."' AND mei_evaprevio.fechaactivacion<'".date('YmdHi')."' 
								AND mei_evaprevio.fechafinalizacion>'".date('YmdHi')."' AND mei_evaprevio.estado=1 
								AND mei_evaluacion.idevaluacion = '".$ideva."' 
								ORDER BY mei_evaprevio.fechaactivacion desc";

						$sql4= "SELECT eq.idprevio, eq.titulo, eq.fechaactivacion, eq.fechafinalizacion, eq.valor, eq.estado from
							mei_relusuvirgru ru inner join mei_evaquiz eq on (ru.idusuario = eq.idautor) inner join mei_virgrupo m on (ru.idvirgrupo = m.idvirgrupo) 
							where ru.idvirgrupo = '".$idgrupo."' and m.idvirmateria = ".$_GET['idmateria']." and eq.fechaactivacion<'".date('YmdHi')."'
							AND eq.fechafinalizacion>'".date('YmdHi')."' AND eq.estado=1 ORDER BY eq.fechaactivacion desc";
				
				}
				else
				{
						$sql = "SELECT mei_actividad.idactividad, mei_actividad.titulo, mei_actividad.fechaactivacion, mei_actividad.fechafinalizacion, mei_actividad.valor, mei_actividad.estado,mei_actividad.fechaprimfinalizacion,mei_actividad.visibilidad 
								FROM mei_actividad, mei_evaluacion, mei_evagrupo WHERE mei_actividad.idtipoactividad ='".$idtipoactividad."' 
								AND mei_actividad.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion
								AND mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_actividad.fechaactivacion<'".date('YmdHi')."' 
								AND mei_actividad.fechafinalizacion>'".date('YmdHi')."' AND mei_actividad.estado=1 
								AND mei_evaluacion.idevaluacion = '".$ideva."' 
								ORDER BY mei_actividad.fechaactivacion desc";
								
						$sql2 = "SELECT mei_foro.idforo, mei_foro.mensaje, mei_foro.fechaactivacion, mei_foro.fechacaducidad, mei_foro.valor
								FROM mei_foro, mei_evaluacion, mei_evagrupo WHERE mei_foro.idevaluacion=mei_evaluacion.idevaluacion 
								AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion
								AND mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_foro.fechaactivacion<'".date('Y-m-d')."' 
								AND mei_foro.fechacaducidad>'".date('Y-m-d')."' AND mei_foro.estado=1 
								AND mei_evaluacion.idevaluacion = '".$ideva."' 
								ORDER BY mei_foro.fechaactivacion desc";
								
						$sql3 = "SELECT mei_evaprevio.idprevio, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion, 
								mei_evaprevio.fechafinalizacion, mei_evaprevio.valor, mei_evaprevio.visibilidad 
								FROM mei_evaprevio, mei_evaluacion, mei_evagrupo WHERE mei_evaprevio.idprevio IN (".$previoLista.") AND 
								mei_evaprevio.idevaluacion=mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion
								AND mei_evagrupo.idgrupo = '".$idgrupo."' AND mei_evaprevio.fechaactivacion<'".date('YmdHi')."' 
								AND mei_evaprevio.fechafinalizacion>'".date('YmdHi')."' AND mei_evaprevio.estado=1 
								AND mei_evaluacion.idevaluacion = '".$ideva."' 
								ORDER BY mei_evaprevio.fechaactivacion desc";
						$sql4= "SELECT eq.idprevio, eq.titulo, eq.fechaactivacion, eq.fechafinalizacion, eq.valor, eq.estado from mei_relusugru ru inner join mei_evaquiz eq on (ru.idusuario = eq.idautor) inner join mei_grupo m on (ru.idgrupo = m.idgrupo) where ru.idgrupo = '".$idgrupo."' and m.idmateria = ".$_GET['idmateria']." and eq.fechaactivacion<'".date('YmdHi')."' AND eq.fechafinalizacion>'".date('YmdHi')."' AND eq.estado=1 ORDER BY eq.fechaactivacion desc";
						
			}
				
				//print $sql2."<p>";
			}//
				$resultado=$baseDatos->ConsultarBD($sql);
				$resultado2=$baseDatos->ConsultarBD($sql2);
				$resultado3=$baseDatos->ConsultarBD($sql3);
				$resultado4=$baseDatos->ConsultarBD($sql4);
				//print $sql."<p>";
				if ($promediar == 1) 
				{
					$notaProm += mysql_num_rows($resultado);
				}
				
				if(mysql_num_rows($resultado)> 0)
				{//if existen actividades
                if($tipoactividad == "Actividad"){$tipoactividad = "Co-Evaluacion";}
		?>&nbsp;
		  <table class="tablaGeneral" width="100%" border="0">
			<tr class="trTitulo" align="left" valign="middle">
			  <td colspan="7"><?=$tipoactividad?></td>
		    </tr>
			<tr class="trInformacion" align="center" valign="middle">
			  <td width="25%" class="trSubtitulo"><strong>Titulo</strong></td>
			  <?
				if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{		  	
			  ?>
			  <td width="25%" class="trSubtitulo"><strong>Fecha Activaci&oacute;n Actividad </strong></td>
			  <?
				}
			  ?>
			   <td width="25%" class="trSubtitulo"><strong> Primera Fecha de Entrega  </strong></td>
			  <td width="25%" class="trSubtitulo"><strong>Fecha Finalizaci&oacute;n Actividad </strong></td>
			  <td width="10%" class="trSubtitulo"><strong>Valor</strong></td>
			  <!--<td width="5%">Valor  </td>-->
			  <?
				if ($_SESSION['idtipousuario']==2 or $banaux==1 || $_SESSION['idtipousuario']==5)
				{
			  ?>
			  <td width="5%" class="trSubtitulo"><strong>Estado</strong></td>
			  <td width="5%" class="trSubtitulo"><strong>Visibilidad</strong></td>
			  <?
				}
			  ?>
			</tr>
		<?
					$i = 0;
					while ( list($idactividad, $titulo, $fecha_a, $fecha_f, $valor, $estado,$fecha_f1, $visibilidad) = mysql_fetch_row($resultado) )
					{//while 1
					$e=2*$j;
		$v=(2*$j)+1;
						if ( ($i%2)==0 ) $clase = "trListaOscuro";
						else $clase = "trInformacion";
		?>   
			<tr class="<?=$clase?>"align="center" valign="middle">
			<?php if(isset($_POST["cbo_orden"]) && $_POST["cbo_orden"] != "*" ): ?>
				<td  class="trListaClaro"><b><a href="verDtlleActividad.php?idactividad=<?=$idactividad?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_POST["cbo_orden"]?>" class="link"><?=$titulo?></a></b></td>
				<? elseif (isset($_GET["cbo_orden"])):  ?>
				<td  class="trListaClaro"><b><a href="verDtlleActividad.php?idactividad=<?=$idactividad?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_GET["cbo_orden"]?>" class="link"><?=$titulo?></a></b></td>
			<?php else: ?>
				<td  class="trListaClaro"><b><a href="verDtlleActividad.php?idactividad=<?=$idactividad?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=$titulo?></a></b></td>			
			<?php endif; ?>
			  <?
				if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $banaux==1  or $_SESSION['idtipousuario']==5)
				{		  	
			  ?>
			  <td class="trListaClaro"><?=mostrarFechaTexto($fecha_a,1)?></td>
			  <?
				}
			  ?>
			   <td class="trListaClaro"><?if($fecha_f1!="")print mostrarFechaTexto($fecha_f1,1);else print "---";?></td>
			  <td class="trListaClaro"><?=mostrarFechaTexto($fecha_f,1)?></td>
			  <td class="trListaClaro"><? 
			  		if ($promediar == 1) $valor = $valorProm;
					print ($valor*100);
				  ?>%
			  </td>
			  <!--<td><? //=($valor*100)."%"?></td>-->
			  <?
				if ($_SESSION['idtipousuario']==2 or $banaux==1 || $_SESSION['idtipousuario']==5)
				{
			  ?>
			  <td class="trListaClaro">		  	<?
					if ($estado == 0)
					{
				?>
				<input type="image" id="<?=$e?>" value="imagenes/inactivo.gif" src="imagenes/inactivo.gif"  onclick="cambiarestadoActividad(<?=$idactividad?>,this.id,this.value)" />
				<?
					}
					else
					{
				?>
				<input type="image" id="<?=$e?>" value="imagenes/activo.gif" src="imagenes/activo.gif"  onclick="cambiarestadoActividad(<?=$idactividad?>,this.id,this.value)" />
				<?
					}
				?>&nbsp;			  </td>
			  <?
				}
			  ?>
			  <?
				if ($_SESSION['idtipousuario']==2 or $banaux==1 || $_SESSION['idtipousuario']==5)
				{
			  ?>
			  <td class="trListaClaro">		  	<?
					if ($visibilidad == 0)
					{
				?>
				<input type="image" id="<?=$v?>" value="imagenes/activo.gif" src="imagenes/activo.gif"  onclick="cambiarvisibilidadActividad(<?=$idactividad?>,this.id,this.value)" />
				<?
					}
					else
					{
				?>
				<input type="image" id="<?=$v?>" value="imagenes/inactivo.gif" src="imagenes/inactivo.gif"  onclick="cambiarvisibilidadActividad(<?=$idactividad?>,this.id,this.value)" />
				<?
					}
				?>&nbsp;			  </td>
			  <?
				}
			  ?>			  
			</tr>
		<?				$i++;
						$porcentajeNota += $valor;
					
		$j++;
}//fin while 1

		?>
			<tr class="trListaClaro" align="center" valign="middle">
			  <td colspan="7">&nbsp;</td>
		    </tr>
	  </table>
	  <table class="tablaGeneral">
		  <tr class="trSubTitulo">
			<td align="right" valign="middle">Porcentaje total de las Actividades asignadas a esta nota : <?=(round($porcentajeNota,2)*100)?>%</td>
		  </tr>
		</table>	  

	<? 
				}//fin if existen actividades				
			}//Fin While idtipoactividad	
	?>
<!--*********************************************************************************************************************-->
<?
$porcentajeNota=0;
		if(mysql_num_rows($resultado2)> 0)
		{//if existen foros
		
			if ($promediar == 1) 
			{
				$notaProm += mysql_num_rows($resultado2);
			}
?>
&nbsp;
<table class="tablaGeneral" width="100%" border="0">
			<tr class="trTitulo" align="left" valign="middle">
			  <td colspan="5">Foros</td>
		    </tr>
			<tr class="trInformacion" align="center" valign="middle">
			   <td class="trSubtitulo" width="25%"><strong>Titulo</strong></td>
			  <?
				if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{		  	
			  ?>
			  <td class="trSubtitulo" width="30%"><strong>Fecha Activaci&oacute;n Actividad </strong></td>
			  <?
				}
			  ?>
			  <td class="trSubtitulo" width="30%"><strong>Fecha Finalizaci&oacute;n Actividad </strong></td>
			  <td class="trSubtitulo" width="10%"><strong>Valor</strong></td>
			  <!--<td width="5%">Valor  </td>-->
			  <?
				if ($_SESSION['idtipousuario']==0)
				{
			  ?>
			  <td class="trSubtitulo" width="5%"><strong>Estado</strong></td>
			  <?
				}
			  ?>
		
		<?
					$i = 0;
					while ( list($idforo, $mensaje, $fecha_a, $fecha_f, $valor, $estado) = mysql_fetch_row($resultado2) )
					{//while 1
						if ( ($i%2)==0 ) $clase = "trListaOscuro";
						else $clase = "trInformacion";
					
		?>   
			<tr class="<?=$clase?>"align="center" valign="middle">
			  <td class="trListaClaro" ><b><a href="../actividades/resultadosForo.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&codigoMensaje=<?=$idforo?>" class="link">
			  <? 
				  $mensaje=strip_tags(stripslashes(base64_decode($mensaje)));
				  if (strlen($mensaje)>20) print substr($mensaje,0,20)."...";
				  else print $mensaje;?></a></b></td>
			  <?
				if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{		  	
			  ?>
			  <td class="trListaClaro"><?=mostrarFecha($fecha_a,false)?></td>
			  <?
				}
			  ?>
			  <td class="trListaClaro"><?=mostrarFecha($fecha_f,false)?></td>
			  <td class="trListaClaro"> <? 
			  		if ($promediar == 1) $valor = $valorProm;
					print ($valor*100);
				  ?>%</td>
			  <!--<td><? //=($valor*100)."%"?></td>-->
			  <?
				if ($_SESSION['idtipousuario']==0)
				{
			  ?>
			  <td class="trListaClaro">		  	<?
					if ($estado == 0)
					{
				?>
				<a href="javascript:activar(1, <?=$idactividad?>)"><img src="imagenes/inactivo.gif" alt="Clic para Activar" width="16" height="16" border="0"></a>
				<?
					}
					else
					{
				?>
				<a href="javascript:activar(0, <?=$idactividad?>)"><img src="imagenes/activo.gif" alt="Clic para Inactivar" width="16" height="16" border="0"></a>
				<?
					}
				?>&nbsp;			  </td>
			  <?
				}
			  ?>
			</tr>
		<?				$i++;
						$porcentajeNota += $valor;
					}//fin while 1
		?>
			<tr class="trListaClaro" align="center" valign="middle">
			  <td colspan="5">&nbsp;</td>
		    </tr>
	  </table>
<?
		}//fin if existen foros
		

		if(mysql_num_rows($resultado3)> 0)
		{//if existen foros
		
			if ($promediar == 1) 
			{
				$notaProm += mysql_num_rows($resultado2);
				$porcentajeNota=0;
			}
?>&nbsp;
<table class="tablaGeneral" width="100%" border="0">
			<tr class="trTitulo" align="left" valign="middle">
			  <td colspan="7">Previos</td>
		    </tr>
			<tr class="trInformacion" align="center" valign="middle">
			  <td width="25%" colspan="2"><strong>Titulo</strong></td>
			  <?
				if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{		  	
			  ?>
			  <td width="30%"><strong>Fecha Activaci&oacute;n Actividad </strong></td>
			  <?
				}
			  ?>
			  <td width="30%"><strong>Fecha Finalizaci&oacute;n Actividad </strong></td>
			  <td width="10%"><strong>Valor</strong></td>
			  <!--<td width="5%">Valor  </td>-->
			  <?
				if ($_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{
			  ?>
			  <td width="5%"><strong>Estado</strong></td>
			  <td width="5%"><strong>Visibilidad</strong></td>
			  <?
				}
			  ?>
			</tr>
			<tr class="trListaClaro" align="center" valign="middle">
			  <td colspan="7">&nbsp;</td>
			</tr>
		<?
					$i = 0;
					while ( list($idforo, $mensaje, $fecha_a, $fecha_f, $valor, $estado, $visibilidad) = mysql_fetch_row($resultado3) )
					{//while 1
					$e=2*$j;
					$v=(2*$j)+1;
						if ( ($i%2)==0 ) $clase = "trListaOscuro";
						else $clase = "trInformacion";
					
		?>   
			<tr class="<?=$clase?>"align="center" valign="middle">
			  <?php if(isset($_POST["cbo_orden"]) && $_POST["cbo_orden"] != "*"):  ?>
		<td><b><a href="../evaluaciones/verDtlleEvaluacion.php?idprevio=<?=$idforo?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$idgrupo?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_POST["cbo_orden"] ?>" class="link"><?=$mensaje?></a></b></td>
	<?php  
	     elseif (isset($_GET["cbo_orden"])):  ?>
		<td><b><a href="../evaluaciones/verDtlleEvaluacion.php?idprevio=<?=$idforo?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$idgrupo?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_GET["cbo_orden"] ?>" class="link"><?=$mensaje?></a></b></td>
	<?php  
	     else:  ?>
		<td><b><a href="../evaluaciones/verDtlleEvaluacion.php?idprevio=<?=$idforo?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$idgrupo?>&materia=<?=$_GET["materia"]?>&cbo_orden=*" class="link"><?=$mensaje?></a></b></td>
	<?php  endif; ?>

			  <td><a href="../evaluaciones/verDtlleEvaluacion.php?idprevio=<?=$idforo?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$idgrupo?>&materia=<?=$_GET["materia"]?>&cbo_orden=*" class="link">
			<?
		  	if (@!in_array($idforo,$idlista))
			{
		  ?>
            <img src="imagenes/error.gif" alt="Este Quiz No tiene Asignadas Preguntas" width="17" height="18" border="0">
            <?
		  	}
		  ?>
		  </a>			  </td>
			  <?
				if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{		  	
			  ?>
			  <td><?=mostrarFechaTexto($fecha_a,1)?></td>
			  <?
				}
			  ?>
			  <td><?=mostrarFechaTexto($fecha_f,1)?></td>
			  <td><? 
			  		if ($promediar == 1) $valor = $valorProm;
					print ($valor*100);
				  ?>%</td>
			  <!--<td><? //=($valor*100)."%"?></td>-->
			  <?
				if ($_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{
			  ?>
			  <td>		  	<?
					if ($estado == 0)
					{
				?>
				<input type="image" id="<?=$e?>" value="imagenes/inactivo.gif" src="imagenes/inactivo.gif"  onclick="cambiarestado(<?=$idforo?>,this.id,this.value)" />
				<?
					}
					else
					{
				?>
				<input type="image" id="<?=$e?>" value="imagenes/activo.gif" src="imagenes/activo.gif"  onclick="cambiarestado(<?=$idforo?>,this.id,this.value)" />
				<?
					}
				?>&nbsp;			  </td>
			  <?
				}
			  ?>
			  
			  <?
				if ($_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{
			  ?>
			  <td class="trListaClaro">		  	<?
					if ($visibilidad == 0)
					{
				?>
				<input type="image" id="<?=$v?>" value="imagenes/activo.gif" src="imagenes/activo.gif"  onclick="cambiarvisibilidad(<?=$idforo?>,this.id,this.value)" />
				<?
					}
					else
					{
				?>
				<input type="image" id="<?=$v?>" value="imagenes/inactivo.gif" src="imagenes/inactivo.gif"  onclick="cambiarvisibilidad(<?=$idforo?>,this.id,this.value)" />
				<?
					}
				?>&nbsp;			  </td>
			  <?
				}
			  ?>			  
			</tr>
		<?				$i++;
						$porcentajeNota += $valor;
						$j++;
					}//fin while 1

		?>
			<tr class="trListaClaro" align="center" valign="middle">
			  <td colspan="7">&nbsp;</td>
		    </tr>
	  </table>
	  		<table class="tablaGeneral">
		  <tr class="trSubTitulo">
			<td align="right" valign="middle">Porcentaje total de las Actividades asignadas a esta nota : <?=(round($porcentajeNota,2)*100)?>%</td>
		  </tr>
		</table>&nbsp;
	  <table class="tablaGeneral" width="100%" border="0">
			<tr class="trTitulo" align="left" valign="middle">
			  <td colspan="6">Previos de Reposición</td>
		    </tr>
			<tr class="trInformacion" align="center" valign="middle">
			  <td class="trListaClaro" width="25%" colspan="2"><strong>Titulo</strong></td>
			  <?
			  $porcentajeNota=0;
				if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{		  	
			  ?>
			  <td  class="trListaClaro" width="30%"><strong>Fecha Activaci&oacute;n Actividad </strong></td>
			  <?
				}
			  ?>
			  <td class="trListaClaro" width="30%"><strong>Fecha Finalizaci&oacute;n Actividad </strong></td>
			  <td class="trListaClaro" width="10%"><strong>Valor</strong></td>
			  <!--<td width="5%">Valor  </td>-->
			  <?
				if ($_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{
			  ?>
			  <td class="trListaClaro" width="5%"><strong>Estado</strong></td>
			  <?
				}
			  ?>
			</tr>
			<tr class="trListaClaro" align="center" valign="middle">
			  <td colspan="6">&nbsp;</td>
			</tr>
		<?
					$i = 0;
					while ( list($idforo, $mensaje, $fecha_a, $fecha_f, $valor, $estado) = mysql_fetch_row($resultado4) )
					{//while 1
						if ( ($i%2)==0 ) $clase = "trListaOscuro";
						else $clase = "trInformacion";
					
		?>   
			<tr class="<?=$clase?>"align="center" valign="middle">
			  <td class="trListaClaro"><b><a href="../evaluaciones/verDtlleEvaluacion.php?idprevio=<?=$idforo?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idgrupo=<?=$idgrupo?>&esp=reposicion" class="link"><?=$mensaje?></a></b></td>
			  <td class="trListaClaro"><a href="../evaluaciones/verDtlleEvaluacion.php?idprevio=<?=$idforo?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idgrupo=<?=$idgrupo?>" class="link">
			<?
		  	if (@!in_array($idforo,$idlista))
			{
		  ?>
            <img src="imagenes/error.gif" alt="Este Quiz No tiene Asignadas Preguntas" width="17" height="18" border="0">
            <?
		  	}
		  ?>
		  </a>			  </td>
			  <?
				if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{		  	
			  ?>
			  <td class="trListaClaro"><?=mostrarFechaTexto($fecha_a,1)?></td>
			  <?
				}
			  ?>
			  <td class="trListaClaro"><?=mostrarFechaTexto($fecha_f,1)?></td>
			  <td class="trListaClaro"><? 
			  		if ($promediar == 1) $valor = $valorProm;
					print ($valor*100);
				  ?>%</td>
			  <!--<td><? //=($valor*100)."%"?></td>-->
			  <?
				if ($_SESSION['idtipousuario']==2 or $banaux==1 or $_SESSION['idtipousuario']==5)
				{
			  ?>
			  <td class="trListaClaro">		  	<?
					if ($estado == 0)
					{
				?>
				<a href="javascript:activarQ(1, <?=$idforo?>,'0')"><img src="imagenes/inactivo.gif" alt="Clic para Activar" width="16" height="16" border="0"></a>
				<?
					}
					else
					{
				?>
				<a href="javascript:activarQ(0, <?=$idforo?>,'0')"><img src="imagenes/activo.gif" alt="Clic para Inactivar" width="16" height="16" border="0"></a>
				<?
					}
				?>&nbsp;			  </td>
			  <?
				}
			  ?>
			</tr>
		<?				$i++;
						$porcentajeNota += $valor;
					}//fin while 1
		?>
			<tr class="trListaClaro" align="center" valign="middle">
			  <td colspan="6">&nbsp;</td>
		    </tr>
	  </table>
	  		<table class="tablaGeneral">
		  <tr class="trSubTitulo">
			<td align="right" valign="middle">Porcentaje total de las Actividades asignadas a esta nota : <?=(round($porcentajeNota,2)*100)?>%</td>
		  </tr>
		</table>
<?
		}//fin if existen foros
		
		
		if($promediotemas[$idtema]<$notamin){
						break;
					}
		
		$j++;
		
		}//FIN WHILE $resactividad
		
		
}//FIN $resactividad
}
	else{//ELSE $resactividad
	?>&nbsp;
			<table class="tablaGeneral" width="200" border="0">
				<tr class="trSubtitulo">
				  <td align="center">&nbsp;</td>
			  </tr>
				<tr class="trSubtitulo">
					<td align="center">No existe ninguna Actividad definida para este Grupo</td>
				</tr>
				<tr class="trSubtitulo">
				  <td align="center">&nbsp;</td>
			  </tr>
			</table>
	 
	<?
		}//FIN ELSE $resactividad*/
return $j;	}//FIN FUNCION verActividad

if ($_SESSION['idtipousuario']==1 or $_SESSION['idtipousuario']==2 or $_SESSION['idtipousuario']==7 or $_SESSION['idtipousuario']==5)
{//USUARIO ADMINISTRADOR

   $banaux=1;

	if ( empty($_POST["cbo_orden"]) ) $_POST["cbo_orden"] = "*";
	
	if(isset($_GET["cbo_orden"])){$opcionorden = $_GET["cbo_orden"];}
	else{ $opcionorden = $_POST["cbo_orden"]; }
	
	
	if ($opcionorden == "*")
	{
		if($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
		{
			$sql = "SELECT mei_virgrupo.idvirgrupo, mei_virgrupo.nombre FROM `mei_virgrupo`, mei_relusuvirgru 
				WHERE mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo AND 
				mei_relusuvirgru.idusuario=".$_SESSION["idusuario"]." AND 
				mei_virgrupo.idvirmateria=".$_GET["idmateria"];
		}
		else
		{
		   $sql = "SELECT mei_grupo.idgrupo, mei_grupo.nombre FROM `mei_grupo`, mei_relusugru 
				WHERE mei_grupo.idgrupo = mei_relusugru.idgrupo AND 
				mei_relusugru.idusuario=".$_SESSION["idusuario"]." AND 
				mei_grupo.idmateria=".$_GET["idmateria"];
		}
		
				//print $sql."<p>";
		$resultado = $baseDatos->ConsultarBD($sql);
		while ( list($idgrupo, $grupo) = mysql_fetch_row($resultado) )
		{
			/*$sql = "SELECT me.idevaluacion FROM mei_actividad m, mei_evaluacion me, mei_evagrupo mei 
					WHERE m.idevaluacion=me.idevaluacion  AND me.idevaluacion=mei.idevaluacion 
					AND mei.idgrupo=".$idgrupo;*/
					if($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
						$sql = "SELECT DISTINCT(me.idevaluacion), me.nombre, me.promediar FROM mei_evaluacion me, mei_evavirgrupo mei 
				WHERE me.idevaluacion=mei.idevaluacion AND me.tipoevaluacion = 2
				AND mei.idvirgrupo=".$idgrupo;
				//print $sql."<p>";
					}
					else
					{
						$sql = "SELECT DISTINCT(me.idevaluacion), me.nombre, me.promediar FROM mei_evaluacion me, mei_evagrupo mei 
				WHERE me.idevaluacion=mei.idevaluacion AND me.tipoevaluacion = 2
				AND mei.idgrupo=".$idgrupo;
					}
		
				//print $sql."<p>";
	?>&nbsp;
		<table class="tablaGeneral" width="100%" border="0">
			<tr class="trSubTitulo">
			  <td width="10%" class="trOscuro" style="color:red;">Grupo:</td>
			  <td width="90%" class="trOscuro" style="color:red;"><?=$grupo?></td>
			</tr>
		</table><?
			$j = verActividad($sql, $idgrupo, $j);
		}	
	}
	else
	{
		list ($idgrupo, $grupo) = explode("-", $opcionorden);
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
		{
			$sql = "SELECT DISTINCT(me.idevaluacion), me.nombre, me.promediar FROM mei_evaluacion me, mei_evavirgrupo mei 
				WHERE me.idevaluacion=mei.idevaluacion AND me.tipoevaluacion = 2 
				AND mei.idvirgrupo=".$idgrupo;
		}
		else
		{
			$sql = "SELECT DISTINCT(me.idevaluacion), me.nombre, me.promediar FROM mei_evaluacion me, mei_evagrupo mei 
				WHERE me.idevaluacion=mei.idevaluacion AND me.tipoevaluacion = 2 
				AND mei.idgrupo=".$idgrupo;
		}
		
		
		//print "<p>$sql<p>";
		verActividad($sql, $idgrupo);
	}
/*print "<script>window.alert('".$_POST["cbo_orden"]."')</script>";*/
?>
&nbsp;
<table class="tablaMarquesina" width="100%" border="0">
  <tr class="">
   <td align="center" valign="middle">
	<?php if(isset($_POST["cbo_orden"]) && $_POST["cbo_orden"] != "*"):  ?>
		<a href="ingresarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_grupo=<?=$_POST["cbo_orden"]?>&estado=3&coeva=0" class="link"><img src="imagenes/ver.gif" alt="Taller" border="0"> Crear Actividad </a></td>
		<td align="center" valign="middle"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"> Volver </td>
	<?php  
	     elseif (isset($_GET["cbo_orden"]) && $_GET["cbo_orden"] != "*" ):  ?>
		<a href="ingresarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_grupo=<?=$_GET["cbo_orden"]?>&estado=3&coeva=0" class="link"><img src="imagenes/ver.gif" alt="Taller" border="0"> Crear Actividad </a></td>
		<td align="center" valign="middle"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"> Volver </td>
	<?php  
	     else:  ?>
		<a href="ingresarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&coeva=0" class="link"><img src="imagenes/ver.gif" alt="Taller" border="0"> Crear Actividad </a></td>
		<td align="center" valign="middle"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"> Volver </td>
	<?php  endif; ?>
  </tr>
</table>
<?
}//FIN USUARIO ADMINISTRADOR
?>
<?
	if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
	{ //if idtipousuario = 2
		
		$banaux=0;
		
		if($_SESSION['idtipousuario']==6)
		
		{
			$sql = "SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo, mei_relusuvirgru WHERE mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo
					AND mei_relusuvirgru.idusuario = ".$_SESSION["idusuario"]." AND mei_virgrupo.idvirmateria=".$_GET["idmateria"];
		}
		else
		{
			$sql = "SELECT mei_grupo.idgrupo FROM mei_grupo, mei_relusugru WHERE mei_grupo.idgrupo = mei_relusugru.idgrupo
					AND mei_relusugru.idusuario = ".$_SESSION["idusuario"]." AND mei_grupo.idmateria=".$_GET["idmateria"];
		}
		
		$resultado = $baseDatos->ConsultarBD($sql);
		list ($idgrupo) = mysql_fetch_row($resultado);	
			
		/*$sql = "SELECT me.idevaluacion FROM mei_actividad m, mei_evaluacion me, mei_evagrupo mei 
				WHERE m.idevaluacion=me.idevaluacion  AND me.idevaluacion=mei.idevaluacion 
				AND mei.idgrupo=".$idgrupo;*/
		if ($_SESSION['idtipousuario']==6)
		{
			
			$sql = "SELECT DISTINCT(me.idevaluacion), me.nombre, me.promediar FROM mei_evaluacion me, mei_evavirgrupo mei 
					WHERE me.idevaluacion=mei.idevaluacion AND me.tipoevaluacion = 2
					AND mei.idvirgrupo=".$idgrupo;	
	}
	else
	{
			
		$sql = "SELECT DISTINCT(me.idevaluacion), me.nombre, me.promediar FROM mei_evaluacion me, mei_evagrupo mei 
				WHERE me.idevaluacion=mei.idevaluacion AND me.tipoevaluacion = 2
				AND mei.idgrupo=".$idgrupo;	
	}
	
			
		//print "<p>$sql<p>";
		verActividad($sql, $idgrupo);
	}//fin tipousuario = 2
?>
&nbsp;


</td>
<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
</tr>
     </table>
</body>
</html>
<?
}//fin if comprobar sesion
?>		
