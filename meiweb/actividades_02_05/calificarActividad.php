<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	
	if(comprobarSession())
	{	
	$baseDatos= new BD();	
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
function enviar()
{
	document.frm.submit();
	window.close();
}//
</script>
<title>Calificar Actividad</title></head>
<body onLoad="window.focus()">
<form name="frm" method="post" action="guardarCalificacion.php?modo=<?=$_GET["mod"]?>&idusuario=<?=$_GET["idusuario"]?>&idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&idsubgrupo=<?=$_GET["idsubgrupo"]?>&idtiposubgrupo=<?=$_GET["idtiposubgrupo"]?>&materia=<?=$_GET["materia"]?>" target="envioActividad">
  <table width="180px" border="0" cellpadding="0" cellspacing="0">
    <tr class="trTitulo">
      <td colspan="2" align="center" valign="middle">Calificar Actividad </td>
    </tr>
    <tr class="trInformacion">
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td width="20%" align="center" valign="middle">Calificacion: </td>
      <td width="80%" align="center" valign="middle">
        <select name="cbo_calificacion">
		<?
			for ($i=0.0;$i<=5.0;$i=$i+0.1) 
		   {				
				print "<option value='$i'";								
				if ($i == $_GET["nota"])
					print " selected>";
				else print ">";				
				print "$i</option>";							   
		   }
	  ?>
        </select>
      </td>
    </tr>
    
    <tr class="trInformacion">
      <td colspan="2">&nbsp;</td>
    </tr>
     <tr class="trInformacion">
					  	<td colspan="2"><div align="center">
                       <a><h5 align="justify">Nota: Señor Docente al realizar este cambio en la notas, usted debe actualizar manualmente el hash para el control de seguridad e integridad de las notas.</h5></a>
</div></td>
					  </tr>
    <tr class="trInformacion">
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr class="trSubTitulo">
      <td colspan="2" align="center" valign="middle">
        <input type="button" name="Submit" value="Aceptar" onClick="javascript:enviar()">
      </td>
    </tr>
  </table>
</form>
</body>
</html>
<?
}
else redireccionar('../login/');
?>