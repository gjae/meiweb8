<?
   include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
   include_once ('../librerias/estandar.lib.php');
   include_once ('../librerias/vistas.lib.php');
   include_once("../editor/fckeditor.php") ;
   include_once('../calendario/FrmCalendario.class.php');
if(comprobarSession())
{
/*  Nombre  : vertaller.php
	Autor   : Claudia
	Fecha   : 06/03/2006
	Detalle : Guardar Taller
	Versión :
*/

	// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , [Barra de herramientas] ,
	//$editor=new FCKeditor('edt_taller' , '100%' , '200' , 'barraCorreo' , '' ) ;

	//*************************ARREGLO DE LA FECHA***********************************
	list($agno_a, $mes_a, $dia_a) = explode("-",$_POST['txt_fecha_a']);
	if ($mes_a<10) $mes_a = "0".$mes_a;
	if ($dia_a<10) $dia_a = "0".$dia_a;

	list($agno_f, $mes_f, $dia_f) = explode("-",$_POST['txt_fecha_f']);
	if ($mes_f<10) $mes_f = "0".$mes_f;
	if ($dia_f<10) $dia_f = "0".$dia_f;
	
	if($_POST['txt_fecha_f1']!=""){
	list($agno_f1, $mes_f1, $dia_f1) = explode("-",$_POST['txt_fecha_f1']);
	if ($mes_f1<10) $mes_f1 = "0".$mes_f1;
	if ($dia_f1<10) $dia_f1 = "0".$dia_f1;
	}
	//**********************************************************************************

	$fecha_a = $agno_a.$mes_a.$dia_a.$_POST["cbo_hora_a"].$_POST["cbo_minuto_a"];
	$fecha_f = $agno_f.$mes_f.$dia_f.$_POST["cbo_hora_f"].$_POST["cbo_minuto_f"];
	if($_POST['txt_fecha_f1']!=""){
	$fecha_f1 = $agno_f1.$mes_f1.$dia_f1.$_POST["cbo_hora_f1"].$_POST["cbo_minuto_f1"];
	}else{
		$fecha_f1="";
	}
	/*print $fecha_a."<p>";
	print $fecha_f."<p>";*/
	//**********************************************************************************

	if ( $_GET["modo"] == 'E' )
	{//EDITAR ACTIVIDAD
		/*$fecha_a = $_POST['txt_fecha_a']."-".$_POST["cbo_hora_a"]."-".$_POST["cbo_minuto_a"];
		$fecha_f = $_POST['txt_fecha_f']."-".$_POST["cbo_hora_f"]."-".$_POST["cbo_minuto_f"];*/

		
		  if ($_SESSION['idtipousuario']==5)
		{
			$sql = "UPDATE `mei_actividad` SET 
				idevaluacion = '".$_POST["cbo_nota"]."',
				idtipoactividad = '".$_POST["cbo_actividad"]."',
				numeromod= '".$_POST["cbo_numeromod"]."',
				idtiposubgrupo = ".$_POST["cbo_subgrupo"].",
				valor = '".$_POST["cbo_valor"]."',
				idautor = '".$_SESSION['idusuario']."',
				titulo = '".$_POST["txt_titulo"]."',
				fechaactivacion = '".$fecha_a."',
				fechafinalizacion = '".$fecha_f."',
				descripcion = '".base64_encode($_POST["edt_actividad"])."',
				estado = '".$_POST["cbo_estado"]."',
				fechaprimfinalizacion = '".$fecha_f1."'
				WHERE `idactividad` = ".$_POST["hid_actividad"];
		}
		else
		{
			$sql = "UPDATE `mei_actividad` SET
				idevaluacion = '".$_POST["cbo_nota"]."',
				idtipoactividad = '".$_POST["cbo_actividad"]."',
				idtiposubgrupo = ".$_POST["cbo_subgrupo"].",
				valor = '".$_POST["cbo_valor"]."',
				idautor = '".$_SESSION['idusuario']."',
				titulo = '".$_POST["txt_titulo"]."',
				fechaactivacion = '".$fecha_a."',
				fechafinalizacion = '".$fecha_f."',
				descripcion = '".base64_encode($_POST["edt_actividad"])."',
				estado = '".$_POST["cbo_estado"]."',
				fechaprimfinalizacion = '".$fecha_f1."'
				WHERE `idactividad` = ".$_POST["hid_actividad"];
		}
		
		$baseDatos->ConsultarBD($sql);

		if ( !empty($_POST["chk_archivo"]) )
		{
			$archivos = implode(",", $_POST["chk_archivo"]);

			$sql = "SELECT mei_archivoactividad.localizacion FROM mei_archivoactividad WHERE mei_archivoactividad.idarchivo NOT IN (".$archivos.")
					AND mei_archivoactividad.idactividad = '".$_POST["hid_actividad"]."'";
			//print $sql."<P>";

			$resultado = $baseDatos->ConsultarBD($sql);

			while ( list($localizacion) = mysql_fetch_row($resultado) )
			{
				@unlink('../../datosMEIWEB/archivosActividades/'.$localizacion);
			}

			$sql = "DELETE FROM mei_archivoactividad WHERE mei_archivoactividad.idarchivo NOT IN (".$archivos.")
					AND mei_archivoactividad.idactividad = '".$_POST["hid_actividad"]."'";
			//print $sql."<P>";
			$baseDatos->ConsultarBD($sql);
		}
		else
		{
			$sql = "SELECT mei_archivoactividad.localizacion FROM mei_archivoactividad WHERE mei_archivoactividad.idactividad = '".$_POST["hid_actividad"]."'";
			//print $sql."<P>";

			$resultado = $baseDatos->ConsultarBD($sql);

			while ( list($localizacion) = mysql_fetch_row($resultado) )
			{
				@unlink('../../datosMEIWEB/archivosActividades/'.$localizacion);
			}

			$sql = "DELETE FROM mei_archivoactividad WHERE mei_archivoactividad.idactividad = '".$_POST["hid_actividad"]."'";
			//print $sql."<P>";
			$baseDatos->ConsultarBD($sql);
		}
			$idactividad = $_POST["hid_actividad"];

        //INICIO MODIFICACION MENSAJE CALENDARIO
			if ($_SESSION['idtipousuario']==5)
                    $sql="SELECT mei_relcalvirgru.idcalendario FROM mei_relcalvirgru WHERE mei_relcalvirgru.idactividad='".$_POST["hid_actividad"]."' ";
			else
                    $sql="SELECT mei_relcalgru.idcalendario FROM mei_relcalgru WHERE mei_relcalgru.idactividad='".$_POST["hid_actividad"]."' ";
					$resultado=$baseDatos->ConsultarBD($sql);
              	    list ($idCalendario) = mysql_fetch_row($resultado);

              	    if (!empty($idCalendario))
              	    {

						if(!comprobarEditor($_POST['edt_actividad']))
						{
							$descripcion=$_POST['txt_titulo'];
						}
						else
						{
	            	 		$descripcion=$_POST['edt_actividad'];
	            		}

						registrarBitacora(2,5,false);
								
						if ($_SESSION['idtipousuario']==5)
							$sql="SELECT mei_relcalvirgru.idcalendario FROM mei_relcalvirgru WHERE mei_relcalvirgru.idactividad='".$_POST["hid_actividad"]."' ";
						else
							$sql="SELECT mei_relcalgru.idcalendario FROM mei_relcalgru WHERE mei_relcalgru.idactividad='".$_POST["hid_actividad"]."' ";
						$resultado=$baseDatos->ConsultarBD($sql);
	              	    list ($idCalendario) = mysql_fetch_row($resultado);

                        $sql="UPDATE mei_calendario
						SET
						mensaje =  '".base64_encode($_POST['txt_titulo']."[$$$]". eliminarEspacios($descripcion))."',
						fechamensaje = '".$_POST['txt_fecha_f']."',
						estado = '0',
						fechacreacion = '".date('Y-n-j')."' ,
						cartelera = '1' ,
						destino = '1'
						WHERE `idcalendario` ='$idCalendario' ";

						$consulta=$baseDatos->ConsultarBD($sql);

                    }
					//FIN MODIFICACION MENSAJE CALENDARIO


	}//FIN EDITAR ACTIVIDAD
	elseif($_GET["modo"] == 'ACT')
	{
	
		$sql = "UPDATE mei_actividad
				SET mei_actividad.estado = '".$_GET["estado"]."'
				WHERE mei_actividad.idactividad = '".$_GET["idactividad"]."'";		

		$baseDatos->ConsultarBD($sql);
		//print $sql;
	
		redireccionar("../actividades/verActividad.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET["materia"]);
	}
	elseif($_GET["modo"] == 'ACTVISI')
	{
		if($_GET["visibilidad"] == 1){
		$sql = "UPDATE mei_actividad
				SET 
				mei_actividad.visibilidad = '".$_GET["visibilidad"]."',
				mei_actividad.valor = 0
				WHERE mei_actividad.idactividad = '".$_GET["idactividad"]."'";
		}
		else{
		$sql = "UPDATE mei_actividad
				SET mei_actividad.visibilidad = '".$_GET["visibilidad"]."'
				WHERE mei_actividad.idactividad = '".$_GET["idactividad"]."'";		
		}

		$baseDatos->ConsultarBD($sql);
		//print $sql;
	
		redireccionar("../actividades/verActividad.php?idmateria=".$_GET["idmateria"]."&materia=".$_GET["materia"]);
	}	
	else
	{//INSERTAR ACTIVIDAD
	
		/*Agregar Campo*/
		$exists = false;
		$db = "mei_actividad";
		$column = "coevaluacion";
		$column_attr = "int(11) null";
		$columns = "show columns from $db";
		$resultcolumn = $baseDatos->ConsultarBD($resultcolumn);
		while($c = mysql_fetch_assoc($resultcolumn)){
			if($c['Field'] == $column){
				$exists = true;
				break;
			}
		}        
		if(!$exists){
			$sql = "ALTER TABLE `$db` ADD `$column`  $column_attr";
		}					
		$baseDatos->ConsultarBD($sql);		
		/*Fin Agregar Campo*/
		
		/*Agregar Campo*/
		$exists2 = false;
		$db2 = "mei_actividad";
		$column2 = "idcoevaluacion";
		$column_attr2 = "int(11) null";
		$columns2 = "show columns2 from $db2";
		$resultcolumn2 = $baseDatos->ConsultarBD($resultcolumn2);
		while($c2 = mysql_fetch_assoc($resultcolumn2)){
			if($c2['Field'] == $column2){
				$exists2 = true;
				break;
			}
		}        
		if(!$exists2){
			$sql2 = "ALTER TABLE `$db2` ADD `$column2`  $column_attr2";
		}					
		$baseDatos->ConsultarBD($sql2);		
		/*Fin Agregar Campo*/
		
		$hidcoevaluacion = $_POST["hid_coevaluacion2"];
		$hididcoevaluacion = $_POST["hid_idcoevaluacion2"];
		
			if ($_SESSION['idtipousuario']==2)
			{
	
			list($idTipoActividad) = explode("*",$_POST["cbo_actividad"]);
			list($idNota) = explode("*",$_POST["cbo_nota"]);
				if (!empty($hidcoevaluacion)){				
					$sql = "INSERT INTO `mei_actividad` ( `idactividad` , `idevaluacion` , `idtipoactividad` ,
						`idtiposubgrupo` , `valor` , `idautor` , `titulo` , `fechacreacion` ,
						`fechaactivacion` , `fechafinalizacion`, `descripcion` , `archivo` , `estado`,`fechaprimfinalizacion`,`coevaluacion`,`idcoevaluacion` )
						VALUES ('','".$_POST["cbo_nota"]."','".$_POST["cbo_actividad"]."', ".$_POST["cbo_subgrupo"].",
						'".$_POST['cbo_valor']."','".$_SESSION['idusuario']."','".$_POST['txt_nombreTaller']."',
						'".date('YmdHi')."','".$fecha_a."','".$fecha_f."', '".base64_encode($_POST['edt_actividad'])."','','".$_POST["cbo_estado"]."','".$fecha_f1."','".$hidcoevaluacion."','".$hididcoevaluacion."') ";
				}
				else{
					$sql = "INSERT INTO `mei_actividad` ( `idactividad` , `idevaluacion` , `idtipoactividad` ,
						`idtiposubgrupo` , `valor` , `idautor` , `titulo` , `fechacreacion` ,
						`fechaactivacion` , `fechafinalizacion`, `descripcion` , `archivo` , `estado`,`fechaprimfinalizacion`,`coevaluacion` )
						VALUES ('','".$_POST["cbo_nota"]."','".$_POST["cbo_actividad"]."', ".$_POST["cbo_subgrupo"].",
						'".$_POST['cbo_valor']."','".$_SESSION['idusuario']."','".$_POST['txt_nombreTaller']."',
						'".date('YmdHi')."','".$fecha_a."','".$fecha_f."', '".base64_encode($_POST['edt_actividad'])."','','".$_POST["cbo_estado"]."','".$fecha_f1."','".$_POST["coevaluacion"]."') ";				
				}
			}
			else
			{
				list($numeromod) = explode("*",$_POST["cbo_numeromod"]);
				list($idNota) = explode("*",$_POST["cbo_nota"]);
				if (!empty($hidcoevaluacion)){				
					$sql = "INSERT INTO `mei_actividad` ( `idactividad` , `idevaluacion` , `idtipoactividad` , `numeromod` ,
					`idtiposubgrupo` , `valor` , `idautor` , `titulo` , `fechacreacion` , 
					`fechaactivacion` , `fechafinalizacion`, `descripcion` , `archivo` , `estado`,`fechaprimfinalizacion`,`coevaluacion`,`idcoevaluacion`  ) 
					VALUES ('','".$_POST["cbo_nota"]."','".$_POST["cbo_actividad"]."','".$_POST["cbo_numeromod"]."', ".$_POST["cbo_subgrupo"].", 
					'".$_POST['cbo_valor']."','".$_SESSION['idusuario']."','".$_POST['txt_nombreTaller']."',
					'".date('YmdHi')."','".$fecha_a."','".$fecha_f."', '".base64_encode($_POST['edt_actividad'])."','','".$_POST["cbo_estado"]."','".$fecha_f1."','".$hidcoevaluacion."','".$hididcoevaluacion."') ";
				}
				else{
					$sql = "INSERT INTO `mei_actividad` ( `idactividad` , `idevaluacion` , `idtipoactividad` , `numeromod` ,
					`idtiposubgrupo` , `valor` , `idautor` , `titulo` , `fechacreacion` , 
					`fechaactivacion` , `fechafinalizacion`, `descripcion` , `archivo` , `estado`,`fechaprimfinalizacion`,`coevaluacion`  ) 
					VALUES ('','".$_POST["cbo_nota"]."','".$_POST["cbo_actividad"]."','".$_POST["cbo_numeromod"]."', ".$_POST["cbo_subgrupo"].", 
					'".$_POST['cbo_valor']."','".$_SESSION['idusuario']."','".$_POST['txt_nombreTaller']."',
					'".date('YmdHi')."','".$fecha_a."','".$fecha_f."', '".base64_encode($_POST['edt_actividad'])."','','".$_POST["cbo_estado"]."','".$fecha_f1."','".$_POST["coevaluacion"]."') ";
				}				

			}
			
		//print $sql;
		$baseDatos->ConsultarBD($sql);
		$idactividad = $baseDatos->InsertIdBD();

		//INICIO Agregar en el Calendario y cartelera. (Creacion por primera vez desde una actividad)

				if(!empty($_POST['chk_calendario']))
				{

					if(!comprobarEditor($_POST['edt_actividad']))
					{
						$descripcion=$_POST['txt_nombreTaller'];
					}
					else
					{
	            	 	$descripcion=$_POST['edt_actividad'];
	            	}


					registrarBitacora(2,2,false);

					$sql="INSERT INTO mei_calendario ( idcalendario , mensaje , fechamensaje , estado , idusuario , fechacreacion , cartelera, destino)
						 VALUES ('', '".base64_encode($_POST['txt_nombreTaller']."[$$$]". eliminarEspacios($descripcion))."', '".$_POST['txt_fecha_f']."', '0',
						 '".$_SESSION['idusuario']."', '".date('Y-n-j')."' , '1' , '1' )";
					$baseDatos->ConsultarBD($sql);

					$idCalendario=$baseDatos->InsertIdBD();
					if ($_SESSION['idtipousuario']==5)
		                $sql="INSERT INTO mei_relcalvirgru ( idcalendario , idvirgrupo , idactividad) VALUES ( '$idCalendario' , '".$_POST['hid_grupo']."' , '$idactividad' )";
					else
						 $sql="INSERT INTO mei_relcalgru ( idcalendario , idgrupo , idactividad) VALUES ( '$idCalendario' , '".$_POST['hid_grupo']."' , '$idactividad' )";
					$baseDatos->ConsultarBD($sql);

	 			}

			//FIN Agregar en el Calendario y cartelera. (Creacion por primera vez desde una actividad)



	}//FIN INSERTAR ACTIVIDAD

	//$resultado=$baseDatos->ConsultarBD($sql);
	for($i=1;$i<=5;$i++)
	{//FOR 1
		if($_FILES['fil_archivo'.$i])
		{
			$archivo= $_FILES['fil_archivo'.$i]['name'];
			$nombreArchivo="(".$idactividad.")".$archivo;
			$nombreArchivo=corregirCaracteresURL($nombreArchivo);
			$archivo=corregirCaracteresURL($archivo);
			if (cargarArchivo('fil_archivo'.$i,corregirCaracteresURL($nombreArchivo),'../../datosMEIWEB/archivosActividades/'))
			{
				$sql = "INSERT INTO `mei_archivoactividad` ( `idarchivo` , `idactividad` , `archivo` , `localizacion` )
						VALUES ('','".$idactividad."','".$archivo."','".$nombreArchivo."') ";
				$resultado=$baseDatos->ConsultarBD($sql);

			}
			else
			{
				if(!empty($archivo))
				{
						?> <script> alert('Algunos de los archivos no se cargaron correctamente ')</script> 
						<?
					break;
					if(empty($noAdjuntados))
						$noAdjuntados.=$archivo;
					else
						$noAdjuntados.=' | '.$archivo;
				}
			}
		}
	}//FIN FOR 1
	if(isset($_GET['idmateria'])){ 
		$idmateria = $_GET['idmateria'];
	}
	else{
		list ($idmateria, $materia) = explode("*", $_POST["hid_materia"]);
	}
	/*echo "idmateria: #".$idmateria."#";*/
	redireccionar("../actividades/verActividad.php?idmateria=".$idmateria."&materia=".$_GET['materia']."&cbo_orden=".$_GET['cbo_orden']);
	
}//FIN COMPROBARSESSION
?>
