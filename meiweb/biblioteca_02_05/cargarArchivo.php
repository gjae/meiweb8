<?
include_once ('../librerias/estandar.lib.php');
include_once('../baseDatos/BD.class.php');  
include_once ('../librerias/vistas.lib.php');
include_once("../editor/fckeditor.php") ;
include_once('../calendario/FrmCalendario.class.php');
 include_once ('../menu1/Menu1.class.php');

$baseDatos=new BD();
// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , 
//			[Barra de herramientas] , [Texto inicial] ) ;

$editor=new FCKeditor('edt_biblioteca' , '100%' , '100%' , 'barraBasica' , '' ) ;
if(comprobarSession())
{//IF COMPROBAR SESSION

	
	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==7 || $_SESSION['idtipousuario']==5)
	{//IF TIPOUSUARIO
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>MEIWEB</title>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">

<script> 

function functionMateria()
{ 
	document.frm_cargarArchivo.submit();
}

function validar()
{
	if(document.frm2.cbo_aplicacion.value!="" && document.frm2.cbo_tema.value!="" && 
		document.frm2.txt_tituloarchivo.value!="" && document.frm2.fil_archivo.value!="")

		document.frm2.submit();
	else alert ("Debe llenar todos los campos");
}

</script>
</head>

<body>
<table class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
            <td>
	
	<table class="tablaGeneral" >
      <tr class="trSubTitulo">
      
        <td>       <a href="../scripts/" class="link">Inicio</a><a> : </a> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>" class="link">
        	<?=ucwords(strtolower($_GET['materia']))?> </a>: <a href="index.php" class="link"> Biblioteca</a> : Cargar Archivos </td>
      </tr>
    </table>
	&nbsp;
    <table class="tablaGeneral" border="0">
       <tr class="trTitulo">
         <td><img src="imagenes/cargar.gif" width="15" height="15" border="0"> Cargar Archivos </td>
       </tr>
    </table>
<?
if ( $_GET["estado"] == 2 )
{//IF ESTADO = 2
	list($cm, $nm) = explode("-", $_POST["cbo_materia"]);		
	
	$sql = "SELECT DISTINCT(mei_tema.idtema),mei_tema.titulo FROM mei_tema WHERE
		mei_tema.idmateria = $cm"; 
			//mei_tema.idmateria = $cm AND mei_tema.tipo = '1' ";
	$resultado = $baseDatos->ConsultarBD($sql);
	if (mysql_num_rows($resultado)>0)
	{//HAY TEMAS
?>
 <table class="tablaGeneral">
  <form action="guardarArchivo.php?idmateria=<?=$_GET['idmateria']?>&tema=<?= $tema?>&materia=<?=$_GET['materia']?>" method="post" enctype="multipart/form-data" name="frm2">
   <tr class="trInformacion">
      <td width="15%" align="left" valign="middle">&nbsp; </td>
      <td width="75%">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td width="15%" align="left" valign="middle"><b>Usuario : </b></td>
      <td width="75%"><strong>
        <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?>
      </strong></td>
    </tr>
    <tr class="trInformacion">
    <?                        
			if ($_SESSION['idtipousuario']==5)
			{
?>				
					<td width="25%"><b>Curso:</b></td>
<?                        
			}
			else
			{
?>	
					<td width="25%"><b>Materia:</b></td>
<?                        
			}
?>	
        <td width="75%"><?=$nm;?>
          <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_POST["cbo_materia"]?>"></td>
    </tr>
    <tr class="trInformacion">
      <td width="15%" align="left" valign="middle"><b>Tema : </b></td>
      <td><select name="cbo_tema">
      <?
		while ( list($codtema, $nomtema) = mysql_fetch_row($resultado) )		
		{		
			print "<option class='link' value='$codtema-$nomtema'";
			if ( ($codtema == $_POST["hid_tema"]) or ( $codtema == $_POST["cbo_tema"] ) )
				print "selected";
			
			print ">$nomtema</option>";			
		}
		mysql_free_result($resultado);
	  ?>
      </select></td>
    </tr>
    <tr class="trInformacion">
      <td width="15%" align="left" valign="middle"><b>Tipo de Aplicaci&oacute;n : </b></td>
      <td><select name="cbo_aplicacion" id="cbo_aplicacion">
<!--			  <option class='link' value="Materia"></option> -->

	  	  <?
			$sql = "SELECT * FROM `mei_tipoaplicacion`";
			$resultado = $baseDatos->ConsultarBD($sql);
			while ( list($idaplica, $tipoaplica) = mysql_fetch_row($resultado) )
			{
				print "<option class='link' value='$idaplica-$tipoaplica'>$tipoaplica</option>\n";
			}				
			mysql_free_result($resultado);
		  ?>
          </select>      </td>
    </tr>
    <tr class="trInformacion">
      <td width="15%" align="left" valign="middle"><b>Titulo del Archivo : </b></td>
      <td>
        <input class="link" name="txt_tituloarchivo" type="text" id="txt_tituloarchivo"></td>
    </tr>
    <tr class="trInformacion">
      <td width="15%" align="left" valign="middle"><b>Archivo : </b></td>
      <td><input class="link" name="fil_archivo" type="file" id="fil_archivo"></td>
    </tr>
    <tr class="trInformacion">
      <td width="15%" align="left" valign="middle"><b>Descripci&oacute;n del Archivo </b></td>
      <td><? 

			if(empty($_POST['hid_biblioteca']))
				$editor->Value='';
			else
				$editor->Value=$_POST['hid_biblioteca'];
				$editor->crearEditor();
		  ?>	  </td>
    </tr>
    
    <tr>
      <td class="trSubTitulo" colspan="2" align="center" valign="middle">
        <input type="button" name="Submit" value="Aceptar" onClick="javascript:validar()"></td>
      </tr>
  </form>
  </table>
<?
	}//FIN HAY TEMAS
	else
	{//NO HAY TEMAS
?>
	<table class="tablaGeneral">
		<tr class="trInformacion">
			<td>&nbsp;</td>
		</tr>
		<tr align="center" valign="middle" class="trInformacion">
			<td><strong>Actualmente no hay temas Definidos</strong></td>
		</tr>
		<tr class="trInformacion">
			<td>&nbsp;</td>
		</tr>
	</table>
<?
	}//FIN NO HAY TEMAS
}//FIN IF ESTADO = 2
else{//PANTALLA INICIAL
?>	
    <table border="0" class="tablaGeneral">
  <form name="frm1" method="post" action="<? print $PHP_SELF?>?estado=2&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>">
     <tr class="trInformacion">
      <td width="25%" align="left" valign="middle">&nbsp; </td>
      <td width="75%">&nbsp;</td>
    </tr>
	<tr class="trInformacion">
      <td width="25%"><b>Usuario : </b></td>
      <td width="75%">&nbsp;
          <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?></td>
    </tr>
    <tr class="trInformacion">
                <td height="23"><b>Materia : </b></td>
     
      <td>&nbsp;<select name="cbo_materia" onChange="javascript:document.frm1.submit()">
          <option class='link' value="Materia">--Seleccione a--</option>
          <?
		  if($_SESSION['idtipousuario']==5)
		  {
			  $sql ="SELECT DISTINCT(mei_virmateria.idvirmateria), mei_virmateria.nombre FROM mei_virmateria, mei_virgrupo,
					mei_relusuvirgru WHERE mei_virmateria.idvirmateria = mei_virgrupo.idvirmateria AND
					mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo 	AND 
					mei_relusuvirgru.idusuario =".$_SESSION['idusuario'];
		  }
		  else
		  {
	  		$sql = "SELECT DISTINCT(mei_materia.idmateria), mei_materia.nombre FROM mei_materia, mei_grupo,
					mei_relusugru WHERE mei_materia.idmateria = mei_grupo.idmateria AND
					mei_grupo.idgrupo = mei_relusugru.idgrupo 	AND 
					mei_relusugru.idusuario =".$_SESSION['idusuario'];
		  }
				
			$resultado = $baseDatos->ConsultarBD($sql);
			while ( list($idmateria, $nommateria) = mysql_fetch_row($resultado) )
			print "<option class='link' value='$idmateria-$nommateria'>$nommateria</option>\n";	
			
			mysql_free_result($resultado);
		?>
        </select>      </td>
             
    </tr>
	</form>
  </table>
<?
}//FIN PANTALLA INICIAL
?>
  
</td>
<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
  </tr>
</table>
</body>
</html>
<? 
	}//FIN IF TIPOUSUARIO
	else redireccionar('../login/');					
}//FIN IF COMPROBAR SESSION
else redireccionar('../login/');					
?>

