-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-02-2018 a las 15:11:00
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `portal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `titulo_categoria` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`created_at`, `updated_at`, `deleted_at`, `id`, `titulo_categoria`) VALUES
('2018-02-03 14:58:06', '2018-02-03 14:58:06', NULL, 1, 'UNIVERSIDAD'),
('2018-02-03 14:58:16', '2018-02-03 14:58:16', NULL, 2, 'SEDES'),
('2018-02-03 14:58:21', '2018-02-03 14:58:21', NULL, 3, 'GENERAL'),
('2018-02-03 14:58:27', '2018-02-03 14:58:27', NULL, 4, 'TECNOLOGIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `codigo_curso` varchar(5) NOT NULL,
  `nombre_curso` varchar(30) NOT NULL,
  `descripcion` text,
  `usuario_id` int(10) UNSIGNED NOT NULL,
  `fecha_inicio` date NOT NULL,
  `duracion` int(11) NOT NULL,
  `modalidad` enum('VIRT','PRES') DEFAULT 'PRES',
  `creditos` int(11) NOT NULL,
  `nivel` enum('INTERMEDIO','BAJO','EXPERTO') DEFAULT 'BAJO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`created_at`, `updated_at`, `deleted_at`, `id`, `codigo_curso`, `nombre_curso`, `descripcion`, `usuario_id`, `fecha_inicio`, `duracion`, `modalidad`, `creditos`, `nivel`) VALUES
('2018-02-03 14:54:11', '2018-02-03 15:09:33', '2018-02-03 15:09:33', 1, '21018', 'Sistemas Operacionales', 'El objetivo de esta asignatura es proporcionar al estudiante los conocimientos básicos, tanto a nivel teórico como empírico, para comprender y analizar los problemas principales a los que se enfrentan los sistemas operativos, así como sus posibles soluciones. Además le enseña a utilizar el razonamiento analítico mediante la construcción y resolución de escenarios computacionales sencillos.', 3, '2018-02-18', 24, 'PRES', 4, 'INTERMEDIO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso_imagenes`
--

CREATE TABLE `curso_imagenes` (
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `curso_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `curso_imagenes`
--

INSERT INTO `curso_imagenes` (`created_at`, `updated_at`, `deleted_at`, `id`, `nombre`, `curso_id`) VALUES
('2018-02-03 14:54:11', '2018-02-03 14:54:11', NULL, 1, 'a9a47db8d20ce8b0cb09320f16b0e1f9.jpeg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etiquetas`
--

CREATE TABLE `etiquetas` (
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `nombre_etiqueta` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `etiquetas`
--

INSERT INTO `etiquetas` (`created_at`, `updated_at`, `deleted_at`, `id`, `nombre_etiqueta`) VALUES
('2018-02-03 15:00:18', '2018-02-03 15:00:18', NULL, 1, 'ORQUESTA'),
('2018-02-03 15:00:18', '2018-02-03 15:00:18', NULL, 2, 'UIS'),
('2018-02-03 15:02:23', '2018-02-03 15:02:23', NULL, 3, 'COGESTEC'),
('2018-02-03 15:02:23', '2018-02-03 15:02:23', NULL, 4, 'INNOVACIóN'),
('2018-02-03 15:04:01', '2018-02-03 15:04:01', NULL, 5, 'UNIVERSIDAD'),
('2018-02-03 15:04:01', '2018-02-03 15:04:01', NULL, 6, 'PAGO'),
('2018-02-03 15:05:35', '2018-02-03 15:05:35', NULL, 7, 'MEIWEB'),
('2018-02-03 15:05:35', '2018-02-03 15:05:35', NULL, 8, 'CONNUS'),
('2018-02-03 15:07:52', '2018-02-03 15:07:52', NULL, 9, 'AHORRO'),
('2018-02-03 15:07:52', '2018-02-03 15:07:52', NULL, 10, 'CONSUMO'),
('2018-02-03 15:09:07', '2018-02-03 15:09:07', NULL, 11, 'CARRERA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etiqueta_noticia`
--

CREATE TABLE `etiqueta_noticia` (
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `etiqueta_id` int(10) UNSIGNED NOT NULL,
  `noticia_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `etiqueta_noticia`
--

INSERT INTO `etiqueta_noticia` (`created_at`, `updated_at`, `deleted_at`, `id`, `etiqueta_id`, `noticia_id`) VALUES
(NULL, NULL, NULL, 1, 1, 1),
(NULL, NULL, NULL, 2, 2, 1),
(NULL, NULL, NULL, 3, 3, 2),
(NULL, NULL, NULL, 4, 4, 2),
(NULL, NULL, NULL, 5, 5, 3),
(NULL, NULL, NULL, 6, 6, 3),
(NULL, NULL, NULL, 7, 7, 4),
(NULL, NULL, NULL, 8, 8, 4),
(NULL, NULL, NULL, 9, 9, 5),
(NULL, NULL, NULL, 10, 10, 5),
(NULL, NULL, NULL, 11, 11, 6),
(NULL, NULL, NULL, 12, 2, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `texto` text NOT NULL,
  `usuario_id` int(10) UNSIGNED NOT NULL,
  `numero_visitas` int(11) DEFAULT '0',
  `categoria_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`created_at`, `updated_at`, `deleted_at`, `id`, `titulo`, `texto`, `usuario_id`, `numero_visitas`, `categoria_id`) VALUES
('2018-02-03 15:00:18', '2018-02-03 15:02:27', NULL, 1, 'ESPECIALES PERIODÍSTICOS TELEUIS - ORQUESTA SINFÓNICA UIS', 'Este lunes 18 de marzo, en los Especiales Periodísticos Teleuis Comunicaciones“la Orquesta Sinfónica de la Universidad Industrial de Santander: nuevo aporte del alma máter al desarrollo cultural de la región”.\r\n\r\nEl pasado martes 18 de marzo, en el Auditorio Luis A. Calvo de la UIS se llevó a cabo el lanzamiento ante la comunidad universitaria y ciudadanía en general, de la orquesta dirigida por el Maestro Nelson Henry Cruz Rivas.\r\n\r\n70 músicos componen esta iniciativa del alma máter liderada por la Escuela de Artes – Música, con el apoyo de la Rectoría y la Dirección Cultural de la institución.\r\n\r\n“Se trata de dar respuesta a una necesidad propia de la Escuela, para brindar espacios propicios  a los instrumentistas, con el propósito de complementar su aprendizaje con este formato de gran dimensión, y especialmente, que tengan la posibilidad de ampliar y conocer el repertorio universal orquestal como también las diferentes épocas y estilos de los grandes compositores” señaló el Director de la Orquesta Sinfónica UIS', 1, 3, 2),
('2018-02-03 15:02:23', '2018-02-03 15:02:23', NULL, 2, 'COGESTEC 2016', 'La Universidad Industrial de Santander como líder del desarrollo regional en el oriente colombiano, con el auspicio de la Asociación Latino-Iberoamericana de Gestión Tecnológica, ALTEC, celebrarán la quinta versión del congreso de COGESTEC en Bucaramanga del 25 al 27 de octubre próximo.\r\n\r\nEn el evento se discutirán temas actuales de interés como territorios inteligentes, capacidades de innovación, innovación social, gestión de la tecnología y la innovación, ecosistemas de innovación, ideación y creatividad.\r\n\r\nAlgunas nociones de referencia sobre las capacidades de innovación es que son aquellas habilidades y aptitudes de un territorio para crear valor y generar innovaciones continuas incrementales y disruptivas; impulsando así territorios inteligentes, caracterizados por el desarrollo económico sostenible y los altos niveles de calidad de vida de sus habitantes, a través de una gestión de los recursos y estrategias de participación y compromiso ciudadano.\r\n\r\n¿Quiénes pueden participar?\r\nEste es un evento internacional dirigido a líderes académicos, estudiantiles, empresariales e investigadores que quieren estar a la vanguardia y generar redes de conocimiento alrededor de las temáticas de gestión de la tecnología y la innovación.\r\n\r\nActividades\r\nConferencias Magistrales\r\nWorkshops\r\nSesiones de Debate\r\nMesas Interactivas\r\nVisita Parque Tecnológico de Guatiguará\r\nCena de Gala\r\nSesiones Paralelas (ponencias)', 1, 0, 1),
('2018-02-03 15:04:01', '2018-02-03 15:06:24', NULL, 3, 'Universidad Industrial de Santander', 'La universidad industrial de Santander es la primera universidad pública del nororiente colombiano, en implementar al pago en línea.\r\n\r\nLos estudiantes de pregrado presencial (sede principal, sedes regionales) y pregrado a distancia de la universidad industrial de Santander podrán realizar el pago en línea de la liquidación de su matrícula; acorde a sus principios de modernización y tecnificación, la Universidad Industrial de Santander puso en marcha la opción de pago electrónico de las matrículas de los programas de pregrado presencial, sedes regionales y a distancia, al acceder a la herramienta PSE.\r\n\r\nPSE - proveedor de servicios electrónicos - a través del Helm Bank -antes banco de crédito- , permitiendo que desde cualquier parte del mundo, las 24 horas del día, desde cualquier cuenta de ahorro o corriente sin importar la entidad bancaria, se pueda hacer el pago del semestre a la comunidad UIS.', 1, 1, 1),
('2018-02-03 15:05:35', '2018-02-03 15:05:35', NULL, 4, 'Grupo CONUSS', 'Un grupo que está enfocado en dos áreas principalmente, estas son el desarrollo web y la administración de servidores. Este grupo es dirigido por el profesor Manuel Guillermo Florez Becerra y ha desarrollado varios proyectos importantes desde su fundación.', 1, 0, 1),
('2018-02-03 15:07:52', '2018-02-03 15:07:52', NULL, 5, ' COMPROMETIDOS CON EL AHORRO DE ENERGÍA', 'Ante las altas temperaturas por el fenómeno del Niño, y la crisis energética que vive el país, los estudiantes Valderrama y Martínez encaminaron su proyecto de grado hacia el desarrollo de un manual de estrategias para disminuir el consumo de energía eléctrica en el campus central de la Universidad a través del uso racional y eficiente de este recurso. \r\n\r\nCon el acompañamiento de la División de Planta Física y la mascota ambiental, ‘Babilio’, los jóvenes realizan charlas pedagógicas en las diferentes unidades académico- administrativas de la Institución para concientizar a los funcionarios, estudiantes y profesores de la importancia del ahorro de energía.   \r\n\r\n“En las charlas estamos entregando un separador de libros, mientras se termina la cartilla que contiene de forma más amplia y didáctica las recomendaciones sobre el uso de la energía. La difusión del material se hará a toda la institución a través de medios electrónicos para utilizar el papel cuando sea estrictamente necesario”, afirmó el estudiante Miguel Valderrama.\r\n\r\nAgregó que con recomendaciones como “Aprovecha la luz natural y apaga la luz artificial cuando no la necesites”; “Desconecta todos los aparatos electrónicos que no estés utilizando, al estar conectados siguen consumiendo energía”, se busca que la comunidad se comprometa con esta cruzada.  \r\n\r\n“Es importante disminuir una hora el uso del aire acondicionado, además utilizar la temperatura de confort, que es de 22 grados centígrados o 72 grados Fahrenheit, más o menos un grado, ya que en algunas dependencias hay personas que los tienen de 18 o 20 grados centígrados, y por cada grado que tengamos de más, estamos consumiendo un 7% más de este aparato”, afirmó la estudiante Cristina Martínez. \r\n\r\nEl Jefe de la División de Planta Física y codirector de este proyecto, ingeniero Iván Augusto Rojas Camargo al respecto expresó: “La importancia fundamental de este tema, se basa en que la energía es un recurso no renovable, y como seres vivos que ocupamos este planeta, estamos en la obligación y la responsabilidad de conservar esos recursos no renovables para las generaciones futuras”.\r\n\r\nPor su parte Víctor Pérez, coordinador del Sistema de Gestión Ambiental de la Universidad también se refirió a este trabajo: “Si ahorramos 30 minutos, si seguimos unas pautas en el uso adecuado de los elementos eléctricos, el planeta y la Universidad se van a beneficiar”. El ahorro de iluminación, aparatos electrónicos y aires acondicionados, son los puntos principales a “atacar” para disminuir los niveles de consumo dentro y fuera del campus universitario.', 1, 0, 3),
('2018-02-03 15:09:07', '2018-02-03 15:09:07', NULL, 6, 'MÁS DE 3 MIL PERSONAS PARTICIPARON EN LA CARRERA ATLÉTICA UIS', 'En la vigésimo sexta edición de la Carrera Atlética UIS realizada dentro de la conmemoración de los 68 años de la Universidad se contó con la participación de 3.860 personas entre atletas profesionales, comunidad universitaria y ciudadanía en general, en un espacio dedicado a la recreación.\r\n\r\nEste certamen deportivo que cada vez toma más fuerza a nivel nacional como prueba profesional ofreció a la ciudad una gran competencia de resistencia, inteligencia y fortaleza.\r\n\r\n“Para la Universidad ofrecer no solo a nuestra comunidad sino a la ciudad este espacio deportivo, es uno de los esfuerzos más importantes del año, aquí abrimos las puertas a todos para que vengan participen y vayan escalando deportivamente teniendo la seguridad que están en una carrera profesional, con expertos en la educación del deporte y la logística necesaria para obtener un buen resultado”, indicó Milton Arciniegas, director del Departamento de Educación Física y Deportes de la UIS.\r\n\r\nLa competencia entregó a los ganadores incentivos económicos, deportivos, tecnológicos y becas. Se premió los primeros lugares en las categorías: Élite abierta femenina, Élite abierta masculina, Universitaria nacional femenina, Universitaria nacional masculina, Estudiantes UIS femenina, Estudiantes UIS masculina, Máster A (40 a 49 años) masculina, Máster B (50 años en adelante) femenina y masculina, Comunidad UIS administrativa, docente femenina y masculina, Egresados UIS femenina y masculina.\r\n\r\n“Destaco la labor que hace la Universidad realizando este espacio que ya para nosotros los profesionales del atletismo es uno de los referentes de medición más importantes en Colombia, esta prueba es rápida y exigente, aquí debe ser uno muy inteligente y no perder el control”, expresó Andrés Camargo ganador de la competencia con el mejor tiempo registrado en esta versión.\r\n\r\nLa competencia tuvo el acompañamiento de la Policía Nacional, la Dirección de Tránsito de Bucaramanga y la Defensa Civil, quienes gracias a su trabajo se logró mantener el orden del trayecto y la asistencia integral a los deportistas', 1, 0, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia_imagenes`
--

CREATE TABLE `noticia_imagenes` (
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(62) NOT NULL,
  `noticia_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticia_imagenes`
--

INSERT INTO `noticia_imagenes` (`created_at`, `updated_at`, `deleted_at`, `id`, `nombre`, `noticia_id`) VALUES
('2018-02-03 15:00:18', '2018-02-03 15:00:18', NULL, 1, 'e9f20f8352e10405201edd315d338875.jpeg', 1),
('2018-02-03 15:02:23', '2018-02-03 15:02:23', NULL, 2, 'ee5f57c03b4a695500a3015caec7e277.jpeg', 2),
('2018-02-03 15:04:01', '2018-02-03 15:04:01', NULL, 3, 'dbb5db27d88b8d0107b7d68a31823926.jpeg', 3),
('2018-02-03 15:05:35', '2018-02-03 15:05:35', NULL, 4, '39fa886019e2a429cb1e146556768b0c.jpeg', 4),
('2018-02-03 15:07:52', '2018-02-03 15:07:52', NULL, 5, 'aa155f415b1e09e4479a876171ebb598.jpeg', 5),
('2018-02-03 15:09:07', '2018-02-03 15:09:07', NULL, 6, '8a183589dc78184b6911f486f83068d0.jpeg', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `telefono` varchar(17) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`created_at`, `updated_at`, `deleted_at`, `id`, `nombre`, `apellido`, `telefono`, `fecha_nacimiento`) VALUES
(NULL, NULL, NULL, 1, 'ADMIN', 'ADMIN', NULL, '1990-01-01'),
('2018-02-03 14:29:29', '2018-02-03 14:29:29', NULL, 2, 'MANUEL ', 'GUILLERMO', '6443855', '2018-02-01'),
('2018-02-03 14:31:55', '2018-02-03 14:31:55', NULL, 3, 'MANUEL', 'GUILLERMO', '3182206317', '2018-02-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `correo` varchar(150) NOT NULL,
  `clave` varchar(40) DEFAULT '-----------',
  `tipo_usuario` enum('ADM','PRF','EST') DEFAULT 'EST',
  `persona_id` int(10) UNSIGNED NOT NULL,
  `avatar` varchar(100) DEFAULT 'default-avatar.png',
  `sobre_mi` text,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_inicio_profesoral` date DEFAULT NULL,
  `titulo_profesion` varchar(110) DEFAULT 'NO APLICA'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`created_at`, `updated_at`, `deleted_at`, `id`, `correo`, `clave`, `tipo_usuario`, `persona_id`, `avatar`, `sobre_mi`, `fecha_ingreso`, `fecha_inicio_profesoral`, `titulo_profesion`) VALUES
(NULL, NULL, NULL, 1, 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 'ADM', 1, 'default-avatar.png', NULL, NULL, NULL, 'NO APLICA'),
('2018-02-03 14:29:29', '2018-02-03 14:30:09', '2018-02-03 14:30:09', 2, 'anueal@gmail.com', '-----------', 'PRF', 2, 'default-avatar.png', '', '2018-02-08', '2018-01-29', 'Ingeniero de Sistemas'),
('2018-02-03 14:31:55', '2018-02-03 15:09:24', '2018-02-03 15:09:24', 3, 'manuel23@gmail.com', '-----------', 'PRF', 3, 'default-avatar.png', '', '2018-02-21', '2018-02-07', 'Ingeniero de Sistemas');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo_curso` (`codigo_curso`);

--
-- Indices de la tabla `curso_imagenes`
--
ALTER TABLE `curso_imagenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `etiqueta_noticia`
--
ALTER TABLE `etiqueta_noticia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticia_imagenes`
--
ALTER TABLE `noticia_imagenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `curso_imagenes`
--
ALTER TABLE `curso_imagenes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `etiqueta_noticia`
--
ALTER TABLE `etiqueta_noticia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `noticia_imagenes`
--
ALTER TABLE `noticia_imagenes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
