@extends('layouts.layout')
@section('title','Bienvenido :: Login')
@section('body')
<section id="login">
	
	<div class="container">
		<div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-lg<-offset-3 col-md-6 col-lg-6">
			
			<div class="panel panel-primary">
				<div class="panel-heading">
					<strong>Por favor inicie session</strong>
				</div>
				<div class="panel-body">
					
					<form action="{{ host().'/index.php?cont=index&meth=login' }}" class="form-horizontal" method="post">
						<div class="container-fluid">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">
									<i class="glyphicon glyphicon-user"></i>
								</span>
								<input type="email" class="form-control" name="correo" required="Usted debe completar este campo" id="correo">
							</div>
							<br>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">
									<i class="glyphicon glyphicon-lock"></i>
								</span>
								<input type="password" class="form-control" name="clave" required="Usted debe completar este campo" id="clave">
							</div>
								@if(isset($request['error']))
									<strong class="text-danger">
										Usuario invalido
									</strong>
								@endif
							<br><br>
							<button class="btn btn-success btn-block">Ingresar</button>
						</div>
					</form>

				</div>
			</div>

		</div>
	</div>

</section>
@endsection