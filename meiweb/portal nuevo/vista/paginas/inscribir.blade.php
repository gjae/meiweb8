@extends('layouts.layout')
@section('title', 'Bienvenido :: Estudiantes')
@section('brand', 'Conectado como '.$persona->persona->nombre)
@section('css-plugins')
<link rel="stylesheet" href="{{ assets('css/plugins/dataTables.bootstrap.css') }}">

@endsection
@section('body')

<div class="container-fluid clearfix">
	
	<div class="row">
		
		<div class="col-sm-4 col-md-4 col-lg-4 col-md-offset-4 col-sm-offset-4 col-lg-offset-4">
			
			<form action="{{ host().'/index.php?cont=estudiantes&meth=inscribir' }}" method="POST">
				<input type="hidden" name="usuario_id" value="{{ $usuario->id }}">
				@foreach($materias as $key => $materia)
					<label for="materia">{{ $materia->nombre_materia }}</label>
					<select name="materia_seccion_profesor_id[]" class="form-control">
						<option value="">-- ELIJA UNO --</option>
						@foreach($matsecprof::where('materia_id', $materia->id)->get() as $mat )
							<option value="{{ $mat->id }}">
								{{ $mat->seccion->cod_seccion }}
							</option>
						@endforeach
					</select>
				@endforeach
				<br>
				<div class="row">
					<div class="col-sm-4">
						<button class="btn btn-success" type="submit">Guardar</button>
					</div>
				</div>
			</form>

		</div>

	</div>

</div>
<div class="modal fade" id="modal_forms" tabindex="-1" role="dialog" aria-labelledby="modal_formsLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Gestión y registro de estudiantes &nbsp;&nbsp;&nbsp;&nbsp; <span id="verificando"></span> </h4>
        <strong id="nombre_persona"></strong>
      </div>
      <div class="modal-body">

        <div class="container">
            <div class="row">
                <form action="{{ host().'/index.php?cont=estudiantes&meth=formulario' }}" method="post" class="form-horizontal"  id="cargar_info">
                    <div class="row" id="row-form">
                  		<div id="form-load"></div>      
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
</div>

@endsection
@section('js-plugins')
<script src="{{ assets('js/plugins/jquery.dataTables.min.js') }}"></script>
<script src="{{ assets('js/plugins/dataTables.bootstrap.js') }}"></script>
<script>

$(document).ready(function(){
	var table = $("#tabla1").DataTable({
		"ajax": location.href+'&meth=getEstudiantes'
	});

	$("#tabla1 tbody").on( 'click', 'tr', function(){
		var data = table.row( this ).data()
		var url = location.href+'&meth=buscar&correo='+data[2];
		$.getJSON(url, function(data){
			var modal = $("#modal_forms");
			$("#form-load").html(data.formulario);
			modal.modal('show');

		});
	} );

});

function acciones(e, b){
	switch(b.getAttribute('role')){
		case 'formularios':{
			var url = location.href+'&meth=formularios&tipo='+b.getAttribute('tipo-formulario');
			$.getJSON(url, {}, function(data){
				var modal = $("#modal_forms");

				$("#form-load").html(data.formulario);
				modal.modal('show')
			})
			break;
		}
	}
}
	
</script>
@endsection