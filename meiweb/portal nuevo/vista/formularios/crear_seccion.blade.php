<div class="container">
<input type="hidden" name="accion_formulario" value="guardar">
<input type="hidden" name="seccion_id" value="0">
	<div class="row">
		<div class="col-sm-3 col-sm-3 col-md-3">
			<label for="">Codigo de la seccion</label>
			<input type="text" maxlength="5" required class="form-control" id="cod_seccion" name="cod_seccion">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-2 col-md-2 col-lg-2">
			<label for="">Est. Minimos</label>
			<input type="number" class="form-control" id="estudiantes_minimos" name="estudiantes_minimos">
		</div>
		<div class="col-sm-2 col-md-2 col-lg-2">
			<label for="">Est. Maximos</label>
			<input type="number" class="form-control" id="estudiantes_maximos" name="estudiantes_maximos">
		</div>
	</div>
</div>
@include('includes.modal_footer')