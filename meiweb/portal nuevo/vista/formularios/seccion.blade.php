<div class="container">
	<div class="row">
		<div class="col-sm-9 col-lg-9 col-md-9">
			<div class="container">
				<input type="hidden" name="accion_formulario" value="guardar">
				<input type="hidden" name="seccion_id" value="{{ (!is_null($seccion))? $seccion->id : 0 }}">
				<div class="row">
					<div class="col-sm-3 col-md-3 col-lg-3">
						<a onclick="eliminar(event, {{ $seccion->toJson() }})" class="btn btn-danger">Eliminar</a>
						<a onclick="imprimir(event, this)" codigo-seccion="{{$seccion->cod_seccion}}" class="btn btn-success">Listas de inscritos</a>
					</div>
					<div class="col-sm-2 col-lg-2 col-md-2">
						<input type="text" class="form-control" disabled="" name="cod_seccion" readonly="" value="{{ $seccion->cod_seccion }}">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 col-lg-4 col-md-4">
						<h3 class="page-header">Asignar materias</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2 col-lg-2 col-md-2">
						<select name="profesor_id[]" required id="" class="form-control">
							<option value="">-- SELECCIONE UNO --</option>
							@foreach($profesores as $key => $profesor)
								<option value="{{ $profesor->id }}">
									{{ $profesor->persona->nombre.' '.$profesor->persona->apellido }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-3 col-lg-3 col-md-3">
						<select name="materia_id[]" required id="" class="form-control">
							<option value="">-- SELECCIONE UNO --</option>
							@foreach($materias as $key => $materia)
								<option value="{{ $materia->id }}">
									( {{ $materia->codigo_materia }} ) {{ $materia->nombre_materia }}
								</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row" id="add_row"></div>
				<br>
				<div class="row">
					<div class="col-sm-4 col-md-4 col-lg-4">
						<a onclick="nuevo(event)" class="btn btn-success">Agregar otra</a>
					</div>
				</div>
				<div class="hidden" id="plantilla">
					<br>
					<div class="col-sm-3 col-lg-3 col-md-3">
						<select name="profesor_id[]" id="" class="form-control">
							<option value="">-- SELECCIONE UNO --</option>
							@foreach($profesores as $key => $profesor)
								<option value="{{ $profesor->id }}">
									{{ $profesor->persona->nombre.' '.$profesor->persona->apellido }}
								</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-4 col-lg-4 col-md-4">
						<select name="materia_id[]" id="" class="form-control">
							<option value="">-- SELECCIONE UNO --</option>
							@foreach($materias as $key => $materia)
								<option value="{{ $materia->id }}">
									( {{ $materia->codigo_materia }} ) {{ $materia->nombre_materia }}
								</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('includes.modal_footer')