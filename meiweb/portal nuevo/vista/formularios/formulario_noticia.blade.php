@php	
	require_once('modelos/Categoria.php');
@endphp
<div class="container">
	<input type="hidden" name="accion_formulario" value="guardar">
	<input type="hidden" id="is_submit" name="is_submit" value="{{ !is_null($noticia) ? 'no' : 'si' }}">
	<input type="hidden" name="usuario_id" value="{{$user->id}}">
	@if(!is_null($noticia))
		<div class="row">
			<div class="col-sm-3 col-md-3 col-lg-3">
				<a onclick="eliminar(event, {{ $noticia->toJson() }})" class="btn btn-danger">Eliminar</a>
			</div>

		</div>
	@endif
	<div class="row">
		<div class="col-sm-3 col-md-3 col-lg-3">
			<label for="">Titulo de la publicación</label>
			<input type="text" value="{{ !is_null($noticia) ? $noticia->titulo : '' }}" maxlength="200" class="form-control" id="titulo" name="titulo" placeholder="Ingresa el titulo de la publicación">
		</div>
		<div class="col-sm-3 col-md-3 col-lg-3">
			<label for="">Categoria</label>
			<select name="categoria_id" id="" class="form-control" required>
				<option value="">-- ELIJA UNA --</option>
				@foreach(Categoria::all() as $categoria)
					<option {{ (!is_null($noticia) && $noticia->categoria->id == $categoria->id ) ? 'selected' : '' }} value="{{ $categoria->id }}">
						{{ $categoria->titulo_categoria }}
					</option>
				@endforeach
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-md-6 col-lg-6">
			<label for="">Etiquetas (SEPARADAS POR ;)</label>
			<input type="text" maxlength="200" class="form-control" id="titulo" name="etiquetas" value="{{ !is_null($noticia) ? implode(';', $etiquetas) : '' }}" placeholder="Ej: ETIQUETA1;ETIQUETA2; / ETIQUETA1;">
		</div>
	</div>

	<div class="hidden" id="plantilla_archivos">
		<br>
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6">
				<label for="">Imagen</label>
				<input type="file" class="form-control" name="imagenes[]">
			</div>
		</div>
	</div>
	<div id="inputs"></div>
	<br>
	<div class="row">
		<div class="col-sm-4 col-lg-4 col-md-4">
			<a onclick="otra_imagen(event, this)" class="btn btn-success">Agregar imagen</a>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-lg-6 col-md-6">
			<label for="">Cuerpo de la noticia</label>
			<textarea name="texto" id="texto" cols="30" rows="5" class="form-control">{{ !is_null($noticia) ? $noticia->texto : '' }}</textarea>
		</div>
	</div>
</div>
@include('includes.modal_footer')