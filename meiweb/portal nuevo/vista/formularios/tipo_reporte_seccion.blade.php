<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tipo de reporte</title>
	<link rel="stylesheet" href="{{ assets('css/bootstrap.css') }}">
</head>
<body>
	
	<form action="{{ host().'/index.php?cont=secciones&meth=reportes' }}" method="post" class="form-inline">
		<input type="hidden" name="codigo_seccion" value="{{$seccion}}">
		<div class="container">
			
			<div class="jumbotron" style="margin-top: 57px;">
				<div class="row">
					
					<div class="col-sm-9 col-lg-9 col-md-9 col-lg-offset-4 col-md-offset-4 col-lg-offset-4">
						
						<label for="tipo">Tipo de reporte</label>
						<select required="elija un tipo" name="tipo_reporte" id="" class="form-control">
							<option value="">-- ELIJA UN TIPO --</option>
							<option value="por_materia">Reporte por materia</option>
							<!--<option value="general">Reporte general</option>-->
						</select>
					</div>

				</div>
				<!--<div class="row">
					<div class="col-sm-9 col-lg-9 col-md-9 col-lg-offset-4 col-md-offset-4 col-lg-offset-4">
						<label for="">Ordenar por</label>
					</div>
				</div>
				<div class="row">
					
					<div class="col-sm-9 col-lg-9 col-md-9 col-lg-offset-4 col-md-offset-4 col-lg-offset-4">
						
						<label for="">Nombre</label>
						<input type="checkbox" name="tipo_orden[]" class="form-control">
						<label for="">Apellido</label>
						<input type="checkbox" name="tipo_orden[]" class="form-control">
						<label for="">Fecha de inscripcion</label>
						<input type="checkbox" name="tipo_orden[]" class="form-control">
					</div>

				</div>-->
				<div class="row">
					<div class="col-sm-9 col-lg-9 col-md-9 col-lg-offset-4 col-md-offset-4 col-lg-offset-4">
						<button type="submit" class="btn btn-success">Generar</button>
					</div>
				</div>
			</div>
		</div>

	</form>

</body>
</html>