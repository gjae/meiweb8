<br>
<div class="row">
	<div class="col-sm-5 col-lg-5 col-md-5">
		<div class="modal-footer">
		    <a type="button" class="btn btn-default" data-dismiss="modal" onClick="limpiarTotal()"><?php echo e($btn_default); ?></a>
		    <?php if($tipo_boton == 'submit'): ?>
		    	<button type="<?php echo e($tipo_boton); ?>" class="btn btn-primary" id="modal-click"><?php echo e($btn_primary); ?></button>
		    <?php endif; ?>
		</div>			
	</div>	
</div>	
