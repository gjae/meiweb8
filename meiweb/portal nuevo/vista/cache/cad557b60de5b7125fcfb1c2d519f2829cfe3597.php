<div class="container">
	<input type="hidden" name="accion_formulario" value="guardar">
	<input type="hidden" name="curso_id" value="<?php echo e((is_null($materia)) ? 0 : $materia->id); ?>">
	<?php if(!is_null($materia)): ?>
		<div class="row">
			<div class="col-sm-3 col-md-3 col-lg-3">
				<a onclick="eliminar(event, <?php echo e($materia->toJson()); ?>)" class="btn btn-danger">Eliminar</a>
			</div>

		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-sm-2 col-md-2 col-lg-2">
			<label for="codigo">Codigo</label>
			<input type="text" maxlength="5" required placeholder="Codigo de la nueva materia" id="codigo_curso" class="form-control" value="<?php echo e((!is_null($materia)) ? $materia->codigo_curso : ''); ?>" name="codigo_curso">
		</div>
		<div class="col-sm-4 col-md-4 col-lg-4">	
			<label for="nombre_curso">Nombre del curso</label>
			<input type="text" required placeholder="Nombre de la nueva materia" name="nombre_curso" id="nombre_curso" value="<?php echo e((!is_null($materia)) ? $materia->nombre_curso : ''); ?>" class="form-control">	
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-6 col-lg-6">
			<label for="">Descripcion</label>
			<input type="text" required placeholder="Descripcion de la materia" name="descripcion" value="<?php echo e((!is_null($materia)) ? $materia->descripcion : ''); ?>" id="descripcion" class="form-control">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-md-3 col-lg-3">
			<label for="correo">Duracion (en semanas)</label>
			<input type="number" required placeholder="Cantidad de hora por semanas" name="duracion" value="<?php echo e((!is_null($materia)) ? $materia->duracion : ''); ?>" id="duracion" class="form-control">
		</div>
		<div class="col-sm-3 col-md-3 col-lg-3">
			<label for="">Inicio</label>
			<input type="date" class="form-control" name="fecha_inicio" placeholder="FORMATO: DD-MM-YYYY" id="fecha_inicio">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-2 col-md-2 col-lg-2">
			<label for="">Modalidad</label>
			<select name="modalidad" required id="" class="form-control">
				<option value="">-- ELIJA UNO --</option>
				<option <?php echo e(!is_null($materia)?$materia->modalidad == 'VIRT'? 'selected' : '' : ''); ?> value="VIRT">VIRTUAL</option>
				<option <?php echo e(!is_null($materia)?$materia->modalidad == 'PRES'? 'selected' : '' :''); ?> value="PRES">PRESENCIAL</option>
			</select>
		</div>
		<div class="col-sm-2 col-md-2 col-lg-2">
			<label for="">Creditos</label>
			<input type="number" value="<?php echo e(!is_null($materia) ? $materia->creditos : ''); ?>" class="form-control" name="creditos" id="creditos" placeholder="cantidad de creditos" required>
		</div>
		<div class="col-sm-2 col-md-2 col-lg-2">
			<label for="">Nivel</label>
			<select name="nivel" required id="" class="form-control">
				<option value="">-- ELIJA UNO --</option>
				<option <?php echo e(!is_null($materia)? $materia->nivel == 'INTERMEDIO' ? 'selected' : '' : ''); ?> value="INTERMEDIO">INTERMEDIO</option>
				<option <?php echo e(!is_null($materia)? $materia->nivel ==  'EXPERTO' ? 'selected' :'' : ''); ?> value="EXPERTO">EXPERTO</option>
				<option <?php echo e(!is_null($materia)? $materia->nivel == 'BAJO' ?  'selected' : '' : ''); ?> value="BAJO">BAJO</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-6 col-lg-6">
			<label for="">Profesor asignado</label>
			<select name="usuario_id" id="" required class="form-control">
				<option value="">-- ELIJA UNO --</option>
				<?php $__currentLoopData = $profesores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $profesor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<option <?php echo e(!is_null($materia)? $materia->usuario_id == $profesor->id ? 'selected' : '' : ''); ?> value="<?php echo e($profesor->id); ?>">
						<?php echo e($profesor->persona->nombre.' '.$profesor->persona->apellido); ?>

					</option>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</select>
		</div>
	</div>
	<div id="inputs"></div>
	<br>
	<div class="row">
		<div class="col-sm-4 col-lg-4 col-md-4">
			<a onclick="otra_imagen(event, this)" class="btn btn-success">Agregar imagen</a>
		</div>
	</div>
	<div class="hidden" id="plantilla_archivos">
		<br>
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6">
				<label for="">Imagen</label>
				<input type="file" class="form-control" name="imagenes[]">
			</div>
		</div>
	</div>
</div>
<?php echo $__env->make('includes.modal_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>