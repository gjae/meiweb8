<div class="container">
	<div class="row">
		<div class="container">
			<input type="hidden" name="accion_formulario" value="guardar">
			<input type="hidden" name="tipo_usuario" value="<?php echo e($tipo_usuario); ?>">

			<?php if(!is_null($profesor) && $tipo_usuario == 'PRF'): ?>
			<div class="row">
				<div class="col-sm-3 col-md-3 col-lg-3">
					<a onclick="eliminar(event, <?php echo e($profesor->toJson()); ?>)" class="btn btn-danger">Eliminar</a>
				</div>
			</div>
			<br>
			<?php endif; ?>
			<div class="row">
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="nombre">Nombre(s)</label>
					<input type="text" required placeholder="Nombre(s)" id="nombre" class="form-control" name="nombre" value="<?php echo e((!is_null($profesor)) ? $profesor->persona->nombre : ''); ?>">
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">	
					<label for="apellido">Apellido(s)</label>
					<input type="text" value="<?php echo e((!is_null($profesor)) ? $profesor->persona->apellido : ''); ?>" required placeholder="Apellido(s)" name="apellido" id="apellido" class="form-control">	
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-6">
					<label for="">Telefono</label>
					<input type="text" value="<?php echo e((!is_null($profesor)) ? $profesor->persona->telefono : ''); ?>" placeholder="Telefono" name="telefono" id="telefono" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="correo">Correo</label>
					<input type="mail" value="<?php echo e((!is_null($profesor)) ? $profesor->correo : ''); ?>" required placeholder="Correo del estudiante" name="correo" id="correo" class="form-control">
				</div>
				<div class="col-sm-3 col-lg-3 col-md-3">
					<label for="">Fecha de nacimiento</label>
					<input type="date" value="<?php echo e((!is_null($profesor)) ? $profesor->persona->fecha_nacimiento->format('d-m-Y') : ''); ?>" required placeholder="Fecha de nacimiento de la persona" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Fecha de inicio laboral</label>
					<input type="date"  value="<?php echo e((!is_null($profesor)) ? $profesor->fecha_ingreso->format('d-m-Y') : ''); ?>" required name="fecha_ingreso" placeholder="INGRESO AL UIS" class="form-control">
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<label for="">Fecha de inicio de docencia</label>
					<input type="date" value="<?php echo e((!is_null($profesor)) ? $profesor->fecha_inicio_profesoral->format('d-m-Y') : ''); ?>" required name="fecha_inicio_profesoral" placeholder="FECHA DE INICIO COMO DOCENTE" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4 col-md-4 col-lg-4 col-sm-offset-1 col-lg-offset-1 col-md-offset-1">
					<label for="">Titulo profesional</label>
					<input  type="text" value="<?php echo e((!is_null($profesor)) ? $profesor->titulo_profesion : ''); ?>" name="titulo_profesion" placeholder="Ej.: INGENIERO EN SISTEMAS" required="" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3 col-lg-3 col-md-3 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
					<label for="">Avatar / perfil</label>
					<?php if(!is_null($profesor)): ?>
						<img src="<?php echo e(assets('cargas/'.$profesor->avatar)); ?>" alt="">
						<?php else: ?>
						<input type="file"  name="avatar" class="form-control">
					<?php endif; ?>
					
				</div>
			</div>
			<?php if($tipo_usuario == 'PRF'): ?>
			<div class="row">
				<div class="col-sm-6 col-lg-6 col-md-6">
					<label for="">Sobre el profesor</label>
					<textarea name="sobre_mi" id="" cols="30" rows="10" class="form-control"><?php echo e((!is_null($profesor)) ? $profesor->sobre_mi : ''); ?></textarea>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php echo $__env->make('includes.modal_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>