<!DOCTYPE html>
<html lang="es">
<head>
  <title>Portal Meiweb </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo e(assets('css/bootstrap_real.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(assets('css/estilos.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(assets('css/estilos-hover.css')); ?>">
  <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
  <script src="https://use.fontawesome.com/67e95bbc71.js"></script>
  <script src="<?php echo e(assets('js/jquery.min.js')); ?>"></script>
  <script src="<?php echo e(assets('js/bootstrap_real.min.js')); ?>"></script>
  <link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
</head>


<body>


<!-- Barra superior comienzo -->
      <div id="barra" >
          <div class="container">
              <!-- Texto a la izquierda-->
              <div class="pull-left">
              <a href="<?php echo e(host()); ?>" class="login"><i class="glyphicon glyphicon-equalizer login"></i> Portal Educativo Meiweb</a> 
              </div>
            

            <!-- Texto a la derecha-->
              <div class="pull-right">
               <!--<a href="https://meiweb.uis.edu.co/meiweb/login/index.php?entrar=1" target="_blank" class="login"><i class="glyphicon glyphicon glyphicon-user login"></i>Ingresar</a>-->
            	<a href="<?php echo e(host().'/index.php?cont=index&meth=ingresar'); ?>" target="_blank" class="login"><i class="glyphicon glyphicon glyphicon-user login"></i>Ingresar</a>
                  
              </div>
        
          </div>
      </div>

      <!-- Barra superior finaliza -->

<!-- Menu de navegacion comienzo -->

	<nav class="navbar navbar-default menu"  id="custom-bootstrap-menu" >
  <div class="container-fluid ">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>


     </div> 
     <!-- LOGO-->
     <img height="65" width="120" class="logo" src="<?php echo e(assets('img/logo-meiweb.png')); ?>">
    <div class="collapse navbar-collapse navbar-right " id="myNavbar">
     
      <ul class="nav navbar-nav ">
        
        <li class="active"><a href="<?php echo e(host()); ?>"><i class="fa fa-home iconomenu1"></i></i> INICIO</a></li>


        <!-- Menu Cursos con clase dropdown para el submenu-->
		        <li class="dropdown">
		        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-book iconomenu2"></i> CURSOS
		        <span class="caret"></span></a>


		<!-- Submenu Cursos-->
								<ul class="dropdown-menu">
						          <li><a href="<?php echo e(host().'/index.php?cont=meiweb&meth=cursos&tipo=PRES'); ?>">Presenciales</a></li>
						          <li><a href="<?php echo e(host().'/index.php?cont=meiweb&meth=cursos&tipo=VIRT'); ?>">Virtuales</a></li>
						      	 </ul>
		

		<!-- Menu Docentes con clase dropdown para el submenu-->

		       <li ><a href="<?php echo e(host().'/index.php?cont=meiweb&meth=docentes_presenciales'); ?>"><i class="fa fa-users iconomenu3" aria-hidden="true"></i> DOCENTES</a></li>
  
    <!-- Menu normal Noticias-->

            <li ><a href="<?php echo e(host().'/index.php?meth=noticias'); ?>"><i class="fa fa-newspaper-o iconomenu4" aria-hidden="true"></i> NOTICIAS</a></li>
   

			

				 <form class="navbar-form navbar-left">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar">
      </div>
      
    </form>
      </ul>
      
    </div>
  </div>

 <!-- Termina el menu de arriba principal-->
<?php $__env->startSection('cuerpo'); ?>
<?php echo $__env->yieldSection(); ?>
<!-- Empieza footer-->
<div class="footer">
  <div class="container">

  <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <h3>
              Información de Contacto
            </h3><br>
            <p class="texto-footer-small"><i class="fa fa-home" aria-hidden="true"></i> Bucaramanga - Colombia. Cra 27 calle 9</p>

            <p class="texto-footer-small"><i class="fa fa-phone" aria-hidden="true"></i> PBX: (57) (7) 6344000</p>

            <p class="texto-footer-small"><i class="fa fa-envelope-o" aria-hidden="true"></i> soporte@meiweb.uis.edu.co</p>
           
        </div>
      <?php 
        require_once('modelos/Noticia.php');
        $noticias = Noticia::orderBy('created_at', 'DESC')->take(2)->get();
       ?>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h3 class="small-title">
              Ultimas Noticias
            </h3><br>
                    <?php $__currentLoopData = $noticias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $noticia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <img height="80px" width="80px" src="<?php echo e(assets('cargas/noticias/'.$noticia->imagenes[0]->nombre)); ?>" align="left"></img><a class="link-noticias-minipost-footer " href="<?php echo e(host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id); ?>"><?php echo e(substr($noticia->titulo, 0, 20)); ?></a><br><br><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


            
      </div>
     <?php 
      require_once('modelos/Curso.php');
      $cursos = Curso::orderBy('fecha_inicio', 'DESC')->take(3)->get();
      ?>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h3 class="small-title">
              Nuestros Cursos
            </h3><br>
            <?php $__currentLoopData = $cursos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $curso): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <a class="texto-footer-small" href="<?php echo e(host().'/index.php?cont=meiweb&meth=cursos&curso_id='.$curso->id); ?>">
                <?php echo e($curso->nombre_curso); ?>

              </a><br><br>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <br>
            
      </div>

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h3 class="small-title">
              Galeria
            </h3><br>
            

      </div>



 </div><br> <br> <br><br> <br> <br>
</div>
</div>

<!-- Empieza el Subfooter-->

<div class="subfooter">
  <div class="container">

  <div class="row">
    <br>
    <br>
      <div class="col-lg-12">© Todos los derechos reservados- Portal Meiweb <?php echo e(Carbon\Carbon::now()->format('Y')); ?>

              <div class="pull-right">Sitios de Interés
          <a href="http://www.uis.edu.co/webUIS/es/index.jsp" target="_blank">
            <img class="img-responsive logofooter" src="assets/img/uis.png"></a>

          <a href="http://www.sena.edu.co/es-co/Paginas/default.aspx" target="_blank">
            <img class="img-responsive logofooter" src="assets/img/sena.png"></a>

          <a href="http://www.mineducacion.gov.co/portal/" target="_blank">
            <img class="img-responsive logofooter" src="assets/img/ministerio.png"></a>

          <a href="http://es.wikipedia.org/" target="_blank"><img class="img-responsive logofooter" src="assets/img/wikipedia.png"></a>

              </div>


      </div>
      
  
 </div><br><br>
</div>
</div>
<!-- Termina el footer-->


</body>
</html>