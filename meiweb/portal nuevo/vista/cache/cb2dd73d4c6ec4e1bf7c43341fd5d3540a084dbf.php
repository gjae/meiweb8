<?php $__env->startSection('title','Bienvenido :: Login'); ?>
<?php $__env->startSection('body'); ?>
<section id="login">
	
	<div class="container">
		<div class="col-sm-6 col-md-offset-3 col-sm-offset-3 col-lg<-offset-3 col-md-6 col-lg-6">
			
			<div class="panel panel-primary">
				<div class="panel-heading">
					<strong>Por favor inicie session</strong>
				</div>
				<div class="panel-body">
					
					<form action="<?php echo e(host().'/index.php?cont=index&meth=login'); ?>" class="form-horizontal" method="post">
						<div class="container-fluid">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">
									<i class="glyphicon glyphicon-user"></i>
								</span>
								<input type="email" class="form-control" name="correo" required="Usted debe completar este campo" id="correo">
							</div>
							<br>
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">
									<i class="glyphicon glyphicon-lock"></i>
								</span>
								<input type="password" class="form-control" name="clave" required="Usted debe completar este campo" id="clave">
							</div>
								<?php if(isset($request['error'])): ?>
									<strong class="text-danger">
										Usuario invalido
									</strong>
								<?php endif; ?>
							<br><br>
							<button class="btn btn-success btn-block">Ingresar</button>
						</div>
					</form>

				</div>
			</div>

		</div>
	</div>

</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>