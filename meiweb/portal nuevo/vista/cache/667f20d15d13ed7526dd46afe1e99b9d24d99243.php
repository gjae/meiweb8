 <!-- Empieza el carrusel-->
<?php $__env->startSection('cuerpo'); ?>
<div class="container conpadding ">

        <div class="row ">
           <div class="col-md-12">

            <!-- Titulo de la seccion de noticias-->  

                  <h2 class="section-title textofuente h3negro">ÚLTIMAS NOTICIAS</h2><br>
                </div>    

			<?php $__currentLoopData = $noticias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $noticia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				
			
          	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                <div class="cajanoticias  grow fade hovereffect">

			        <!-- Empieza imagen de noticias-->
			        <img class="img-responsive imgcajanoticias" src="<?php echo e(assets('cargas/noticias/'.$noticia->imagenes[0]->nombre)); ?>" alt="">
			        <div class="overlay">
			           <h2>Noticias</h2>
			           <a class="info" href="<?php echo e(host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id); ?>">VER NOTICIA COMPLETA</a>
			            
			    	</div>
			     <!-- Termina imagen de noticias-->
			                            <br>
                         
                                
                     <a href="<?php echo e(host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id); ?>"><h4 class="titulos"><?php echo e($noticia->titulo); ?></h4></a>
                                <p class="parrafoscajacursos paddingcajas"><?php echo e(substr($noticia->texto, 0, 320)); ?>...</p>
                           
                  
                </div>

            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <div class="container">

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12  ">
                              <center><ul class=" succsess pagination pagination pagination-lg">

                        <?php for($i = 1; $i <= $noticias->lastPage(); $i++ ): ?>
                          <?php if($i == $noticias->currentPage()): ?>
                            <li class="<?php echo e(($i == $noticias->currentPage()) ? 'active' :''); ?>">
                              <a href="<?php echo e(host().'/index.php?meth=noticias&page='.$noticias->currentPage()); ?>">
                                  <?php echo e($i); ?>

                              </a>
                            </li>
                            <?php else: ?>
                            <li>
                              <a href="<?php echo e(host().'/index.php?meth=noticias&page='.$i); ?>">
                                  <?php echo e($i); ?>

                              </a>
                            </li>
                          <?php endif; ?>

                        <?php endfor; ?>


                      </ul>
                                  
                    </center>


                </div>


            </div>

          </div>

      </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.meiweb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>