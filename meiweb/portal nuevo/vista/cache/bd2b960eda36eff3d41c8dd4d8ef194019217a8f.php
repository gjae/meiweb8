 <!-- Empieza el carrusel-->
<?php $__env->startSection('cuerpo'); ?>


 <!-- Inicia la lista de Cursos Presenciales-->
<div class="container">

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        <center><h3 class="titulo-rojo">Cursos <?php echo e($tipo == 'VIRT' ? 'Virtuales' : 'Presenciales'); ?></h3><br><br>
        </div>

        <div class="col-md-8 col-sm-8 col-xs-12 contenedor">
          <?php if( $tipo == 'PRES' ): ?>
            <img class="img-responsive imagen-cursos-presenciales" src="<?php echo e(assets('img/cursos-presenciales-barra-derecha.jpg')); ?>">
            <?php elseif( $tipo == 'VIRT' ): ?>
            <img src="<?php echo e(assets('img/Cursos-virtuales-emprendimiento.jpg')); ?>" alt="" class="img-responsive imagen-cursos-presenciales">
          <?php endif; ?>

          <br>
          <br>

         <div class="list-group success cajacursospresenciales">
         <!-- <a href="perfil-docentes-1.html" class="list-group-item ">1 - EMILIANO DE JESUS LINCE MERCADO</a> -->
          <?php $__currentLoopData = $cursos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $curso): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <a href="<?php echo e(host().'/index.php?cont=meiweb&meth=cursos&curso_id='.$curso->id); ?>"  class="list-group-item"><?php echo e($key); ?> - <?php echo e($curso->nombre_curso); ?></a>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </center>


        </div></div>


         <!--  Empieza la seccion Noticias Populares -->
        <div class="col-md-4 col-sm-4 col-xs-12  categorias-noticias contenedor ">
             
         
                    <h3 class="titulos">Noticias Populares</h3>
                    <br>
                    
                  <?php 
                    require_once('modelos/Noticia.php');
                    $noticias = Noticia::orderBy('numero_visitas', 'DESC')->take(3)->get();
                   ?>
                  <?php $__currentLoopData = $noticias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $noticia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <img src="<?php echo e(assets('cargas/noticias/'.$noticia->imagenes[0]->nombre)); ?>" class="postpopularminiatura" align="left">
                    </img>
                    <a class="link-noticias-minipost " href="<?php echo e(host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id); ?>">
                      <?php echo e(substr($noticia->titulo, 0, 20)); ?>

                    </a>
                    <br>
                    <br>
                    <br>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  

                   <!--  Empieza Imagen Grande -->
                   <center>
                     <h3 class="titulos">Cursos Virtuales</h3>

                   <a href="<?php echo e(host().'/index.php?cont=meiweb&meth=cursos&tipo=VIRT'); ?>"><img src="<?php echo e(assets('img/Cursos-virtuales-emprendimiento.jpg')); ?>" class="img-responsive imagen-cursos-barra-isquierda"></a>
                    </center>

                     <!--  Termina Imagen Grande -->

                     <!--  Empieza Imagen Grande 2 -->
                   <center>
                     <h3 class="titulos">Cursos Presenciales</h3>

                   <a href="<?php echo e(host().'/index.php?cont=meiweb&meth=cursos&tipo=PRES'); ?>" ><img src="<?php echo e(assets('img/cursos-presenciales-barra-derecha.jpg')); ?>" class="img-responsive imagen-cursos-barra-isquierda"></a>
                    </center>

                     <!--  Termina Imagen Grande -->
                                
                           <!--  Termina la seccion Noticias Populares  -->
                        </div>
                        </div>
                        </div>
                              <!--  Termina la lista de Cursos Presenciales- -->


                        <br>
                        <br>
                        <br>
                        <br>
</center>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.meiweb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>