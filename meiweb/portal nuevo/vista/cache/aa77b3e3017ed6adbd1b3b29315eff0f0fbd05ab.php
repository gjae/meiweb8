<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $__env->yieldContent('title', 'Bienvenido'); ?></title>
	<link rel="stylesheet" href="<?php echo e(assets('css/bootstrap_real.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(assets('css/styles.css')); ?>">
  <?php $__env->startSection('css-plugins'); ?>
  <?php echo $__env->yieldSection(); ?>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><?php echo $__env->yieldContent('brand', 'Bienvenido'); ?></a>
    </div>

    <?php if( !is_null(getSessionUser()) && sessionActive()): ?>
      <?php echo $__env->make('includes.navbar_options', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
  </div><!-- /.container-fluid -->
</nav>
<body>
<?php $__env->startSection('body'); ?>
<?php echo $__env->yieldSection(); ?>
</body>
<script src="<?php echo e(assets('js/jquery.js')); ?>"></script>
<script src="<?php echo e(assets('js/bootstrap_real.min.js')); ?>"></script>
<?php $__env->startSection('js-plugins'); ?>
<?php echo $__env->yieldSection(); ?>
</body>
</html>