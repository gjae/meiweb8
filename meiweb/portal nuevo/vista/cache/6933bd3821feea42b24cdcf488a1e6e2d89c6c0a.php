 <!-- Empieza el carrusel-->

<?php $__env->startSection('cuerpo'); ?>
    <div class="container">

      <div class="row">

          <div class="col-md-8 col-sm-8 col-xs-12 contenedor">

            <div >
  <!--  Empieza la imagen de post-->
              <h3 class="titulo-noticias"><?php echo e($noticia->titulo); ?></h3>
              <!--  iconos -->
                  <i class="glyphicon glyphicon-user icono-autor"></i> Por <?php echo e($noticia->usuario->persona->nombre.' '.$noticia->usuario->persona->apellido); ?>

                  <i class="glyphicon glyphicon-time icono-fecha"></i> <?php echo e($noticia->created_at->format('d M Y')); ?>

                   
              <hr>

          
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicadores -->
				  <ol class="carousel-indicators">
				  	<?php $__currentLoopData = $noticia->imagenes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $imagen): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				    <li data-target="#myCarousel" data-slide-to="<?php echo e($key); ?>" class="<?php echo e($key == 0 ? 'active' : ''); ?>"></li>
				    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				  </ol>

				  <!-- Contenido slides -->
				  <div class="carousel-inner">
				  	<?php $__currentLoopData = $noticia->imagenes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $imagen): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					    <div class="item <?php echo e($key == 0 ? 'active' : ''); ?>">
					      <img src="<?php echo e(assets('cargas/noticias/'.$imagen->nombre)); ?>" style="width:100%;">

					    </div>
				  	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				  </div>

				  <!-- Controles de izquierda y derecha -->
				  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left"></span>
				    <span class="sr-only">Anterior</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				    <span class="sr-only">Siguiente</span>
				  </a>
				</div>

              <br>
              
            
             
               <!--  Empieza Parrafo del post -->
                <p class="parrafonoticasposts"><?php echo e($noticia->texto); ?><br><br></p>
               <!--  Termina el Parrafo del post -->
            </div>

            </div>
            


            <!--  Empieza la seccion de categorias -->

            <?php 
              require_once('modelos/Categoria.php');
             ?>
            <div class="col-md-4 col-sm-4 col-xs-12  categorias-noticias contenedor">
                     <h3 class="titulos">Categorias</h3>
                  <br>
                     <ul>
                     <?php $__currentLoopData = Categoria::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <li >
                        <a class="categorias-noticias" 
                            href="<?php echo e(host().'/index.php?meth=noticias&categoria_id='.$categoria->id); ?>">
                            <?php echo e($categoria->titulo_categoria); ?>

                          </a>
                      </li>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  
                </ul>
                <br>
                    <h3 class="titulos">Etiquetas</h3>
                    <br>
                 
                 <?php $__currentLoopData = $noticia->etiquetas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $etiqueta): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <a href="#">
                    <button class="btn btn-success">
                      <?php echo e($etiqueta->nombre_etiqueta); ?>

                    </button>
                  </a>
                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  
                  <br>
                   <br>

                <!--  Termina la seccion de categorias -->


                  <!--  Empieza la seccion Noticias Populares -->
        
             
           <h3 class="titulos">Noticias Populares</h3>
                    <br>
                    
                  <?php 
                    require_once('modelos/Noticia.php');
                    $noticias = Noticia::orderBy('numero_visitas', 'DESC')->take(3)->get();
                   ?>
                  <?php $__currentLoopData = $noticias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $noticia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <img src="<?php echo e(assets('cargas/noticias/'.$noticia->imagenes[0]->nombre)); ?>" class="postpopularminiatura" align="left">
                    </img>
                    <a class="link-noticias-minipost " href="<?php echo e(host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id); ?>">
                      <?php echo e(substr($noticia->titulo, 0, 20)); ?>

                    </a>
                    <br>
                    <br>
                    <br>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                     <!--  Empieza Imagen Grande 2 -->
                   <center>
                     <h3 class="titulos">Cursos Presenciales</h3>

                   <a href="<?php echo e(host().'/index.php?cont=meiweb&meth=cursos&tipo=VIRT'); ?>"><img src="<?php echo e(assets('img/cursos-presenciales-barra-derecha.jp')); ?>g" class="img-responsive imagen-cursos-barra-isquierda"></a>
                    </center>

                     <!--  Termina Imagen Grande -->
                                
                           <!--  Termina la seccion Noticias Populares  -->
                        </div>
                        </div>
                     
                              <!--  Termina la lista de Cursos Presenciales- -->

            </div>
           




      </div>





    </div>  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.meiweb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>