<?php 
require_once('modelos/Noticia.php');
require_once('modelos/NoticiaImagenes.php');
	$populares = Noticia::orderBy('numero_visitas', 'DESC')->take(4)->get();
 ?>
<div class="col-md-12 col-sm-12 col-xs-12  categorias-noticias contenedor">
<h3 class="titulos">Noticias Populares</h3>
    <br>
                            
   <?php $__currentLoopData = $populares; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $noticia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <img src="<?php echo e(assets('cargas/noticias/'.$noticia->imagenes[0]->nombre)); ?>" class="postpopularminiatura" align="left"></img>
         <a 
         	class="link-noticias-minipost " 
         	href="<?php echo e(host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id); ?>"
         >
               <?php echo e($noticia->titulo); ?>

        </a>
        <br>
        <br>
        <br>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


    <center>
        <h3 class="titulos">Cursos Virtuales</h3>

         <a href="<?php echo e(host().'/index.php?cont=meiweb&meth=cursos&tipo=VIRT'); ?>">
         	<img src="<?php echo e(assets('img/Cursos-virtuales-emprendimiento.jpg')); ?>" class="img-responsive imagen-cursos-barra-isquierda">
         </a>
     </center>

                     <!--  Termina Imagen Grande -->

                     <!--  Empieza Imagen Grande 2 -->
    <center>
    	<h3 class="titulos">Cursos Presenciales</h3>

        <a href="<?php echo e(host().'/index.php?cont=meiweb&meth=cursos&tipo=PRES'); ?>">
        	<img src="<?php echo e(assets('img/cursos-presenciales-barra-derecha.jpg')); ?>" class="img-responsive imagen-cursos-barra-isquierda">
        </a>
    </center>
                               
</div>
