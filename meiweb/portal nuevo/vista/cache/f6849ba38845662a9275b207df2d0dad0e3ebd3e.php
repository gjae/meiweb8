 <!-- Empieza el carrusel-->
<?php $__env->startSection('cuerpo'); ?>

 <div class="container">

      <div class="row">

          <div class="col-md-8 col-sm-8 col-xs-12">

  
              <h3 class="tituloscursos">Profesor <?php echo e($profesor->persona->nombre.' '.$profesor->persona->apellido); ?></h3>
              <h5 class="tituloscursos2"><?php echo e($profesor->titulo_profesion); ?></h5>
            
          
          <center><img class="fotoperfil img-responsive" src="<?php echo e(assets('cargas/'.$profesor->avatar)); ?>"></center>
           <br>
              
                 <!--  iconos -->                          
                 <span class="titulosbiografia" ><!--<img src="img/perfil1.jpg" class="perfilavatar">-->Biografia</span>
              <hr>
               <!--  Empieza Biografia -->
                <p class="parrafonoticasposts">
                <?php echo e($profesor->sobre_mi); ?></p>
               <!--  Fin -->
          </div>  
      
      
    
      
        <div class="col-md-4 col-sm-8 col-xs-12  datosbiografia ">
                     <h3 class="titulos">Datos del Profesor</h3>
                  <br>
                    <div>
                       

                        <p >

                          <p class="pull-left">Tiempo laborando en la UIS:</p>
                           <p class="pull-right">
                           		<b>
                           			<?php echo e(($profesor->fecha_ingreso->diffInYears(Carbon\Carbon::now()) >= 1 ) 
                           				? $profesor->fecha_ingreso->diffInYears(Carbon\Carbon::now()).' Año(s)'
                           				: $profesor->fecha_ingreso->diffInMonths(Carbon\Carbon::now()).' Mes(es)'); ?>  
                           		</b>
                           	</p>


                        </p>

                        <br><hr>
                            <p >

                          <p class="pull-left">Materias Encargadas:</p>
                           <p class="pull-right"><b><?php echo e($profesor->cursos->count()); ?>.</b></p>


                        </p>
                         <br><hr>
                            <p >

                          <p class="pull-left">Materia que dicta actualmente:</p>
                           <p class="pull-right">
                           		<b>
                           			<?php $__currentLoopData = $profesor->cursos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $curso): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           				<?php if($key > 0): ?>
                           					/
                           				<?php endif; ?>
										<?php echo e($curso->nombre_curso); ?>

                           			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           		</b>
                           </p>
                           <br>


                        </p>
                          <br><hr>
                            <p >

                          <p class="pull-left">Años de labor profesoral:</p>
                           <p class="pull-right"><b><?php echo e($profesor->fecha_inicio_profesoral->diffInYears(Carbon\Carbon::now())); ?> Años</b></p>


                        </p>
                         <br><hr>
                            <p >

                          <p class="pull-left">Cargo actual en la UIS:</p>
                           <p class="pull-right"><b>Profesor</b></p>


                        </p>
                        <br> <br> <br>
                      
                    </div>  
                  
                <br>

                <!--  Termina datos profesor -->
            </div>
    </div>
  </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.meiweb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>