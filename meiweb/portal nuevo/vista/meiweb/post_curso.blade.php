@extends('layouts.meiweb')
 <!-- Empieza el carrusel-->
@section('cuerpo')


       <!--  Empieza post de noticias completo-->

    <div class="container">

      <div class="row">

          <div class="col-md-8 col-sm-8 col-xs-12">

            <div class="">
  <!--  Empieza la imagen de post-->
              <h3 class="tituloscursos">
              	{{ $curso->nombre_curso }}
              </h3>
              <h5 class="tituloscursos2">Curso {{ $curso->modalidad == 'VIRT' ? 'Virtual': 'Presencial' }}</h5>
              <hr>


              <!-- Empieza el carrusel-->

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicadores -->
  <ol class="carousel-indicators">
  	@foreach($curso->imagenes as $key=> $imagen)
    <li data-target="#myCarousel" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
    @endforeach
  </ol>

  <!-- Contenido slides -->
  <div class="carousel-inner">
  	@foreach($curso->imagenes as $key=> $imagen)
	    <div class="item {{ $key == 0 ? 'active' : '' }}">
	      <img src="{{ assets('cargas/cursos/'.$imagen->nombre) }}" style="width:100%;">

	    </div>
  	@endforeach
  </div>

  <!-- Controles de izquierda y derecha -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Siguiente</span>
  </a>
</div>


<!-- Termina el Carrusel-->

              <br>
              
                 <!--  iconos -->                          
                 <span ><img src="{{ assets('cargas/'.$curso->profesor->avatar) }}" class="perfilavatar">Por <a href="{{ host().'/index.php?cont=meiweb&meth=docente&docente_id='.$curso->usuario_id }}">
                 	{{ $curso->profesor->persona->nombre.' '.$curso->profesor->persona->apellido }}
                 </a></span>
              <hr>
               <!--  Empieza Parrafo del post -->
                <p class="parrafos-cursos">{{ $curso->descripcion }}

                <br><br></p>
               <!--  Termina el Parrafo del post -->
            </div>

            </div>
            


            <!--  Empieza la seccion de  informacion del Curso -->
            <div class="col-md-4 col-sm-4 col-xs-12  informacion-cursos ">
                     <h3 class="titulos">Información del Curso</h3>
                  <br>
                    <div>
                       

                        <p >

                          <p class="pull-left">Comienzo: </p>
                           <p class="pull-right"><b>{{ $curso->fecha_inicio->format('d M Y') }}</b></p>


                        </p>

                        <br><hr>
                            <p >

                          <p class="pull-left">Duración: </p>
                           <p class="pull-right"><b>{{ $curso->duracion }} Semanas</b></p>


                        </p>
                         <br><hr>
                            <p >

                          <p class="pull-left">Modalidad: </p>
                           <p class="pull-right"><b>{{ $curso->modalidad == 'VIRT' ? 'Virtual': 'Presencial' }}</b></p>


                        </p>
                          <br><hr>
                            <p >

                          <p class="pull-left">Numero de Créditos: </p>
                           <p class="pull-right"><b>{{ $curso->creditos }}</b></p>


                        </p>
                         <br><hr>
                            <p >

                          <p class="pull-left">Nivel: </p>
                           <p class="pull-right"><b>{{ $curso->nivel }}</b></p>


                        </p>
                        <br> <br> <br>
                       <center> <a href="https://meiweb.uis.edu.co/meiweb/login/index.php?entrar=1"><button class="btn btn-success btn-lg">INSCRIBIRSE</button></a></center>
                    </div>  
                  
                <br>

                <!--  Termina la seccion de Cursos Virtuales -->
                <br>
                 <br>
            </div>
           




      </div>





    </div>  	
@endsection