@extends('layouts.meiweb')
 <!-- Empieza el carrusel-->
@section('cuerpo')
<div class="container conpadding ">

        <div class="row ">
           <div class="col-md-12">

            <!-- Titulo de la seccion de noticias-->  

                  <h2 class="section-title textofuente h3negro">ÚLTIMAS NOTICIAS</h2><br>
                </div>    

			@foreach ($noticias as $noticia)
				{{-- expr --}}
			
          	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                <div class="cajanoticias  grow fade hovereffect">

			        <!-- Empieza imagen de noticias-->
			        <img class="img-responsive imgcajanoticias" src="{{ assets('cargas/noticias/'.$noticia->imagenes[0]->nombre) }}" alt="">
			        <div class="overlay">
			           <h2>Noticias</h2>
			           <a class="info" href="{{ host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id }}">VER NOTICIA COMPLETA</a>
			            
			    	</div>
			     <!-- Termina imagen de noticias-->
			                            <br>
                         
                                
                     <a href="{{ host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id }}"><h4 class="titulos">{{ $noticia->titulo }}</h4></a>
                                <p class="parrafoscajacursos paddingcajas">{{ substr($noticia->texto, 0, 320) }}...</p>
                           
                  
                </div>

            </div>
            @endforeach
          <div class="container">

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12  ">
                              <center><ul class=" succsess pagination pagination pagination-lg">

                        @for($i = 1; $i <= $noticias->lastPage(); $i++ )
                          @if($i == $noticias->currentPage())
                            <li class="{{ ($i == $noticias->currentPage()) ? 'active' :'' }}">
                              <a href="{{ host().'/index.php?meth=noticias&page='.$noticias->currentPage() }}">
                                  {{ $i }}
                              </a>
                            </li>
                            @else
                            <li>
                              <a href="{{ host().'/index.php?meth=noticias&page='.$i }}">
                                  {{ $i }}
                              </a>
                            </li>
                          @endif

                        @endfor


                      </ul>
                                  
                    </center>


                </div>


            </div>

          </div>

      </div>
</div>

@endsection