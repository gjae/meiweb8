@extends('layouts.meiweb')
 <!-- Empieza el carrusel-->

@section('cuerpo')
    <div class="container">

      <div class="row">

          <div class="col-md-8 col-sm-8 col-xs-12 contenedor">

            <div >
  <!--  Empieza la imagen de post-->
              <h3 class="titulo-noticias">{{ $noticia->titulo }}</h3>
              <!--  iconos -->
                  <i class="glyphicon glyphicon-user icono-autor"></i> Por {{ $noticia->usuario->persona->nombre.' '.$noticia->usuario->persona->apellido }}
                  <i class="glyphicon glyphicon-time icono-fecha"></i> {{ $noticia->created_at->format('d M Y') }}
                   
              <hr>

          
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicadores -->
				  <ol class="carousel-indicators">
				  	@foreach($noticia->imagenes as $key=> $imagen)
				    <li data-target="#myCarousel" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
				    @endforeach
				  </ol>

				  <!-- Contenido slides -->
				  <div class="carousel-inner">
				  	@foreach($noticia->imagenes as $key=> $imagen)
					    <div class="item {{ $key == 0 ? 'active' : '' }}">
					      <img src="{{ assets('cargas/noticias/'.$imagen->nombre) }}" style="width:100%;">

					    </div>
				  	@endforeach
				  </div>

				  <!-- Controles de izquierda y derecha -->
				  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left"></span>
				    <span class="sr-only">Anterior</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				    <span class="sr-only">Siguiente</span>
				  </a>
				</div>

              <br>
              
            
             
               <!--  Empieza Parrafo del post -->
                <p class="parrafonoticasposts">{{$noticia->texto}}<br><br></p>
               <!--  Termina el Parrafo del post -->
            </div>

            </div>
            


            <!--  Empieza la seccion de categorias -->

            @php
              require_once('modelos/Categoria.php');
            @endphp
            <div class="col-md-4 col-sm-4 col-xs-12  categorias-noticias contenedor">
                     <h3 class="titulos">Categorias</h3>
                  <br>
                     <ul>
                     @foreach(Categoria::all() as $categoria)
                      <li >
                        <a class="categorias-noticias" 
                            href="{{ host().'/index.php?meth=noticias&categoria_id='.$categoria->id }}">
                            {{ $categoria->titulo_categoria }}
                          </a>
                      </li>
                     @endforeach
                  
                </ul>
                <br>
                    <h3 class="titulos">Etiquetas</h3>
                    <br>
                 
                 @foreach($noticia->etiquetas as $etiqueta)
                  <a href="#">
                    <button class="btn btn-success">
                      {{ $etiqueta->nombre_etiqueta }}
                    </button>
                  </a>
                 @endforeach
                  
                  <br>
                   <br>

                <!--  Termina la seccion de categorias -->


                  <!--  Empieza la seccion Noticias Populares -->
        
             
           <h3 class="titulos">Noticias Populares</h3>
                    <br>
                    
                  @php
                    require_once('modelos/Noticia.php');
                    $noticias = Noticia::orderBy('numero_visitas', 'DESC')->take(3)->get();
                  @endphp
                  @foreach($noticias as $noticia)
                    <img src="{{ assets('cargas/noticias/'.$noticia->imagenes[0]->nombre) }}" class="postpopularminiatura" align="left">
                    </img>
                    <a class="link-noticias-minipost " href="{{ host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id }}">
                      {{ substr($noticia->titulo, 0, 20) }}
                    </a>
                    <br>
                    <br>
                    <br>
                  @endforeach


                     <!--  Empieza Imagen Grande 2 -->
                   <center>
                     <h3 class="titulos">Cursos Presenciales</h3>

                   <a href="{{ host().'/index.php?cont=meiweb&meth=cursos&tipo=VIRT' }}"><img src="{{ assets('img/cursos-presenciales-barra-derecha.jp')}}g" class="img-responsive imagen-cursos-barra-isquierda"></a>
                    </center>

                     <!--  Termina Imagen Grande -->
                                
                           <!--  Termina la seccion Noticias Populares  -->
                        </div>
                        </div>
                     
                              <!--  Termina la lista de Cursos Presenciales- -->

            </div>
           




      </div>





    </div>  
@endsection