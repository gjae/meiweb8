@extends('layouts.meiweb')
 <!-- Empieza el carrusel-->
@section('cuerpo')

 <div class="container">

      <div class="row">

          <div class="col-md-8 col-sm-8 col-xs-12">

  
              <h3 class="tituloscursos">Profesor {{ $profesor->persona->nombre.' '.$profesor->persona->apellido }}</h3>
              <h5 class="tituloscursos2">{{ $profesor->titulo_profesion }}</h5>
            
          
          <center><img class="fotoperfil img-responsive" src="{{ assets('cargas/'.$profesor->avatar) }}"></center>
           <br>
              
                 <!--  iconos -->                          
                 <span class="titulosbiografia" ><!--<img src="img/perfil1.jpg" class="perfilavatar">-->Biografia</span>
              <hr>
               <!--  Empieza Biografia -->
                <p class="parrafonoticasposts">
                {{ $profesor->sobre_mi }}</p>
               <!--  Fin -->
          </div>  
      
      
    
      
        <div class="col-md-4 col-sm-8 col-xs-12  datosbiografia ">
                     <h3 class="titulos">Datos del Profesor</h3>
                  <br>
                    <div>
                       

                        <p >

                          <p class="pull-left">Tiempo laborando en la UIS:</p>
                           <p class="pull-right">
                           		<b>
                           			{{ 
                           				($profesor->fecha_ingreso->diffInYears(Carbon\Carbon::now()) >= 1 ) 
                           				? $profesor->fecha_ingreso->diffInYears(Carbon\Carbon::now()).' Año(s)'
                           				: $profesor->fecha_ingreso->diffInMonths(Carbon\Carbon::now()).' Mes(es)'
                           			}}  
                           		</b>
                           	</p>


                        </p>

                        <br><hr>
                            <p >

                          <p class="pull-left">Materias Encargadas:</p>
                           <p class="pull-right"><b>{{ $profesor->cursos->count() }}.</b></p>


                        </p>
                         <br><hr>
                            <p >

                          <p class="pull-left">Materia que dicta actualmente:</p>
                           <p class="pull-right">
                           		<b>
                           			@foreach($profesor->cursos as $key => $curso)
                           				@if($key > 0)
                           					/
                           				@endif
										{{ $curso->nombre_curso }}
                           			@endforeach
                           		</b>
                           </p>
                           <br>


                        </p>
                          <br><hr>
                            <p >

                          <p class="pull-left">Años de labor profesoral:</p>
                           <p class="pull-right"><b>{{ $profesor->fecha_inicio_profesoral->diffInYears(Carbon\Carbon::now()) }} Años</b></p>


                        </p>
                         <br><hr>
                            <p >

                          <p class="pull-left">Cargo actual en la UIS:</p>
                           <p class="pull-right"><b>Profesor</b></p>


                        </p>
                        <br> <br> <br>
                      
                    </div>  
                  
                <br>

                <!--  Termina datos profesor -->
            </div>
    </div>
  </div>

@endsection