@extends('layouts.meiweb')
 <!-- Empieza el carrusel-->

@section('cuerpo')
<div id="myCarousel " class="carousel slide carrusel-alto" data-ride="carousel">
  <!-- Indicadores -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Contenido slides -->
  <div class="carousel-inner carrusel-alto">
    <div class="item active">
      <img  src="{{ assets('img/slider-1.jpg') }}" style="width:100%;">
      <!-- Caption dentro de lo slides -->
      <div class="carousel-caption">
        <h2>Bienvenido a Meiweb</h2>
        <p>Portal de Educación</p>
      </div>
    </div>

    <div class="item">
      <img src="{{ assets('img/slider-2.jpg') }}" style="width:100%;">
      <div class="carousel-caption">
      <h2>Bienvenido a Meiweb</h2>
        <p>Portal de Educación</p>
        
      </div>
    </div>

    <div class="item">
      <img src="{{ assets('img/slider-3.jpg') }}" style="width:100%;">
      <div class="carousel-caption">
        <h2>Bienvenido a Meiweb</h2>
        <p>Portal de Educación</p>
        
      </div>
    </div>
  </div>

  <!-- Controles de izquierda y derecha -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Siguiente</span>
  </a>
</div>
</nav>

   <!-- El carrusel se metio dentro del nav para que no quedara un espacio -->
<!-- Menu Principal Finalizado-->



 
<!-- Cajas se uso css para hacer los iconos redondeados y se llamaron los iconos de fontawesome.io-->
<div class="container conpadding ">

    <div class="row">
        <div class="col-md-12">
                  <h2 class="section-title textofuente h3negro">EDUCACIÓN, INVESTIGACIÓN Y ENSEÑANZA EN EL MUNDO</h2>
                </div>                
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                 
                    <div class="cajamenu textofuente ">
                      <center>
                              <div class="icono1">
                              <i class="fa fa-flash"></i> </div>

                             </center>
                       <h3 class="h3negro">Cursos Virtuales</h3>
                        <p>La formación virtual es una manera de aprendizaje que se acopla al tiempo y de quien la adelanta especial para los colombianos que se encuentran dentro y fuera del país.</p>
                        <a href="{{ host().'/index.php?cont=meiweb&meth=cursos&tipo=VIRT' }}"><button class="btn btn-success">Explorar</button></a>
                    </div>
                  
                        
                </div>
                
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                     
                        <div class="cajamenu textofuente">
                          <center>
                              <div class="icono2">
                              <i class="fa fa-graduation-cap"></i> </div>
                          </center>

                       <h3 class="h3negro" >Cursos Presenciales</h3>
                       
                        <p>Los programas académicos en la Universidad Industrial de Santander, tienen por principio el desarrollo de la investigación en un área específica del saber.</p>
                        <a href="{{ host().'/index.php?cont=meiweb&meth=cursos&tipo=PRES' }}"><button class="btn btn-success">Explorar</button></a>
                    </div>
                   
                        
                </div>

                 <div class="col-md-4 col-sm-6 col-xs-12">
                   
                    <div class="cajamenu textofuente">
                       <center>
                              <div class="icono3">
                              <i class="fa fa-newspaper-o"></i> </div>
                          </center>

                       <h3 class="h3negro">Noticias</h3>
                        <p>Encuentra las ultimas noticias de la Universidad Industrial de Santander .</p>
                        <a href="noticias.html"><button class="btn btn-success">Explorar</button></a>
                    </div>
                    </div>
    
        </div>  
    </div>  

<div>
  <br>
  <br>
  <br>

           
    <!-- Empieza la seccion de cursos recomendados-->    

      <div class="container ">

        <div class="row">
           <div class="col-md-12">

                  <h2 class="section-title textofuente h3negro">CURSOS RECOMENDADOS</h2><br><br>
                </div>    
            @foreach($cursos as $curso)
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 polaroid ">
                <div class="cajacursos  grow fade">
                    
                             <img src="{{ assets('cargas/cursos/'.$curso->imagenes[0]->nombre) }}" class="img-responsive imgcajacursos">
                             <br>
                             
                          
                                <span ><img src="{{ assets('cargas/'.$curso->profesor->avatar) }}" class="perfilavatar">Por <a href="{{ host().'/index.php?cont=meiweb&meth=docente&docente_id='.$curso->usuario_id }}">{{ $curso->profesor->persona->nombre.' '.$curso->profesor->persona->apellido }}</a></span>
                                
                                <h4 class="titulos">{{ $curso->nombre_curso }}</h4>
                                <p class="parrafoscajacursos">{{ $curso->descripcion }}</p>
                                <center><a href="{{ host().'/index.php?cont=meiweb&meth=cursos&curso_id='.$curso->id }}"><button class="btn btn-success">Continuar leyendo</button></a></center>
                  
                </div>

            </div>
            @endforeach
        </div>


      </div>

<!-- Termina la seccion de cursos recomendados-->    


<br>
<br>
<br>
<br>
@endsection













