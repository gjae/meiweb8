@extends('layouts.layout')
@section('title', 'Bienvenido :: Estudiantes')
@section('brand', 'Conectado como '.$persona->persona->nombre)
@section('css-plugins')
<link rel="stylesheet" href="{{ assets('css/plugins/dataTables.bootstrap.css') }}">
@endsection
@section('body')
<div class="container-fluid clearfix">
	
	<div class="row">

		<div class="col-sm-8 col-md-8 col-lg-8 col-md-offset-1 col-lg-offset-1">
			<button class="btn btn-success" onclick="acciones(event, this)" role="formularios" tipo-formulario="formulario_noticia">Nueva noticia</button>
			<button class="btn btn-success" onclick="acciones(event, this)" role="formularios" tipo-formulario="nueva_categoria">Nueva categoria</button>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-9 col-md-9 col-lg-9 col-md-offset-1 col-lg-offset-1 col-md-offset-1">
			<div class="table-responsive">
				<table id="tabla1" class="table table-striped">
					<thead>
						<tr>
							<th>ID#</th>
							<th>Titulo</th>
							<th>Fecha de publicacion</th>
							<th>Responsable</th>
							<th>Categoria</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

</div>
<div class="modal fade" id="modal_forms" tabindex="-1" role="dialog" aria-labelledby="modal_formsLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Gestión y registro de noticias&nbsp;&nbsp;&nbsp;&nbsp; <span id="verificando"></span> </h4>
        <strong id="nombre_persona"></strong>
      </div>
      <div class="modal-body">

        <div class="container">
            <div class="row">
                <form onsubmit="pre_submit(event, this)" enctype="multipart/form-data" action="{{ host().'/index.php?cont=noticias&meth=formulario' }}" method="post" class="form-horizontal"  id="cargar_info">
                    <div class="row" id="row-form">
                  		<div id="form-load"></div>      
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection

@section('js-plugins')
<script src="{{ assets('js/plugins/jquery.dataTables.min.js') }}"></script>
<script src="{{ assets('js/plugins/dataTables.bootstrap.js') }}"></script>
<script>
$(document).ready(function(){
	var table = $("#tabla1").DataTable({
		"ajax": location.href+'&meth=getNoticias'
	});

	$("#tabla1 tbody").on( 'click', 'tr', function(){
		var data = table.row( this ).data()
		var url = location.href+'&meth=buscar&codigo='+data[0];
		$.getJSON(url, function(data){
			var modal = $("#modal_forms");
			$("#form-load").html(data.formulario);
			modal.modal('show');

		});
	} );
})

function acciones(e, b){
	switch(b.getAttribute('role')){
		case 'formularios':{
			var url = location.href+'&meth=formularios&tipo='+b.getAttribute('tipo-formulario');
			$.getJSON(url, {}, function(data){
				var modal = $("#modal_forms");

				$("#form-load").html(data.formulario);
				modal.modal('show')
			})
			break;
		}
	}
}
	

function eliminar(e, datos){
	if(confirm('¿Seguro que desea eliminar esta seccion?')){
		var url = location.href +'&meth=eliminar';
		$.post(url, {id: datos.id}, function(data){
			data = JSON.parse(data);
			if( !data.error ){
				alert(data.mensaje);
				if( data.recargar )
					location.reload();
				return true;
			}
			alert(data.mensaje);
		});
	}
}

function otra_imagen(e, b){
	var plantilla = $("#plantilla_archivos").html();
	$("#inputs").append(plantilla);
}
	
function pre_submit(e, f){
	var submit = document.getElementById('is_submit');
	if(submit.value == 'si')
		f.submit()
	else{
		alert('ESTE FORMULARIO ES DE SOLO LECTURA')
		return false;
	}
}

</script>
@endsection