<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Reporte por materia</title>
</head>
<body>
<style>
	*{
		font-family: Helvetica;
	}
	td.head{
		border-top: 2px solid black;
		border-bottom: 2px solid black;
	}
	.page-break{
		page-break-before: always;
	}
</style>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	
	<thead>
		<tr>
			<td style="text-align: center;" width="100%">
				<strong>Lista inscritos en la seccion {{ $seccion->cod_seccion }}</strong>
			</td>
		</tr>
	</thead>

</table>


<table border="0" cellpadding="0" cellspacing="0" width="100%">
	
	<thead>
		<tr>
			<td align="center" width="100%">
				<h4>## LISTADO DE INSCRITOS POR MATERIA ##</h4>
			</td>
		</tr>
	</thead>
</table>

@foreach( $materias as $key => $materia )
<table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="100%" style="text-align: center;">
			<strong>
				Personas inscritas en la materia {{ $materia->materia->nombre_materia }} ({{ $materia->materia->codigo_materia }})
			</strong>
			<br>
			<strong>
				Profesor: {{ $materia->profesor->persona->nombre.' '.$materia->profesor->persona->apellido }}
			</strong>
		</td>
	</tr>
</table>
<table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
	<tr align="center">
		<td class="head">NOMBRE</td>
		<td class="head">APELLIDO</td>
		<td class="head">CORREO</td>
		<td class="head">F/NACIMIENTO</td>
	</tr>
	@foreach( $materia->inscritos as $numero => $inscrito )
	<tr align="center" style="background-color: {{ ($numero%2 == 0) ? '#ffffff' : '#E1F2FF' }};">
		<td>{{ $inscrito->estudiante->persona->nombre }}</td>
		<td>{{ $inscrito->estudiante->persona->apellido }}</td>
		<td>{{ $inscrito->estudiante->correo }}</td>
		<td>{{ $inscrito->estudiante->persona->fecha_nacimiento->format('d/m/Y') }}</td>
	</tr>
	@endforeach
</table>
@if( isset( $materias[ ($key+1) ] ) )
<div class="page-break"></div>
@endif
@endforeach

</body>
</html>