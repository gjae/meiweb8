    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="#">Inicio</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administracion<span class="caret"></span></a>
          <ul class="dropdown-menu">
          	@if( $user->tipo_usuario == 'ADM')
            <!--<li><a href="{{ host().'/index.php?cont=secciones' }}">Secciones</a></li>-->
            <!--<li><a href="{{ host().'/index.php?cont=estudiantes' }}">Estudiantes</a></li>-->
            <li><a href="{{ host().'/index.php?cont=profesores' }}">Profesores</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{ host().'/index.php?cont=cursos' }}">Cursos</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{ host().'/index.php?cont=noticias' }}">Noticias</a></li>
            @endif
          </ul>
        </li>
        <li><a href="{{ host().'/index.php?cont=index&meth=salir' }}">Salir</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->