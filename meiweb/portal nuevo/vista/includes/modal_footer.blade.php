<br>
<div class="row">
	<div class="col-sm-5 col-lg-5 col-md-5">
		<div class="modal-footer">
		    <a type="button" class="btn btn-default" data-dismiss="modal" onClick="limpiarTotal()">{{ $btn_default }}</a>
		    @if($tipo_boton == 'submit')
		    	<button type="{{ $tipo_boton }}" class="btn btn-primary" id="modal-click">{{ $btn_primary }}</button>
		    @endif
		</div>			
	</div>	
</div>	
