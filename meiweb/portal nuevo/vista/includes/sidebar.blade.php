@php
require_once('modelos/Noticia.php');
require_once('modelos/NoticiaImagenes.php');
	$populares = Noticia::orderBy('numero_visitas', 'DESC')->take(4)->get();
@endphp
<div class="col-md-12 col-sm-12 col-xs-12  categorias-noticias contenedor">
<h3 class="titulos">Noticias Populares</h3>
    <br>
                            
   @foreach($populares as $noticia)
        <img src="{{ assets('cargas/noticias/'.$noticia->imagenes[0]->nombre) }}" class="postpopularminiatura" align="left"></img>
         <a 
         	class="link-noticias-minipost " 
         	href="{{ host().'/index.php?cont=noticias&meth=ver&id='.$noticia->id }}"
         >
               {{ $noticia->titulo }}
        </a>
        <br>
        <br>
        <br>
    @endforeach


    <center>
        <h3 class="titulos">Cursos Virtuales</h3>

         <a href="{{ host().'/index.php?cont=meiweb&meth=cursos&tipo=VIRT' }}">
         	<img src="{{ assets('img/Cursos-virtuales-emprendimiento.jpg') }}" class="img-responsive imagen-cursos-barra-isquierda">
         </a>
     </center>

                     <!--  Termina Imagen Grande -->

                     <!--  Empieza Imagen Grande 2 -->
    <center>
    	<h3 class="titulos">Cursos Presenciales</h3>

        <a href="{{ host().'/index.php?cont=meiweb&meth=cursos&tipo=PRES' }}">
        	<img src="{{ assets('img/cursos-presenciales-barra-derecha.jpg') }}" class="img-responsive imagen-cursos-barra-isquierda">
        </a>
    </center>
                               
</div>
