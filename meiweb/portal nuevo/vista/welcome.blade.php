<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Indice</title>
	<link rel="stylesheet" href="{{ assets('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ assets('css/styles.css') }}">
</head>
<body>
	
<div class="container">
	<div class="row">
		<div class="col-sm-4 pull-md-2">
			<h2>Bienvenido</h2>
		</div>
	</div>
</div>

<script src="{{ assets('js/jquery.js') }}"></script>
<script src="{{ assets('js/bootstrap.min.js') }}"></script>
</body>
</html>