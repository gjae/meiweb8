<?php  
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Usuario extends Eloquent{

	use SoftDeletes;
	protected $table = 'usuarios';
	public $dates = ['deleted_at', 'fecha_ingreso', 'fecha_inicio_profesoral'];
	protected $fillable = [
		'correo', 'clave', 'persona_id', 'tipo_usuario', 'avatar', 'sobre_mi',
		'fecha_ingreso', 'fecha_inicio_profesoral', 'titulo_profesion'
	];
	protected $casts = ['fecha_ingreso' => 'date', 'fecha_inicio_profesoral' => 'date'];
	protected $guard = ['persona_id', 'clave'];
	protected $hidden = ['clave'];


	/**
	 * LAS FUNCIONES "SET...ATTRIBUTE"
	 * PERMITEN ARREGLAR UN VALOR ANTES DE INSERTARLO
	 * EN LA BD
	 */
	public function setClaveAttribute($value)
	{
		$this->attributes['clave'] = md5( trim($value) );
	}

	public function setCorreoAttribute($value)
	{
		$this->attributes['correo'] = trim($value);
	}

	public function setFechaIngresoAttribute($old){
		$this->attributes['fecha_ingreso'] = Carbon::parse($old)->format('Y-m-d');
	}

	public function setFechaInicioProfesoralAttribute($old){
		$this->attributes['fecha_inicio_profesoral'] = Carbon::parse($old)->format('Y-m-d');
	}

	public function persona(){
		require_once('modelos/Persona.php');
		return $this->belongsTo(Persona::class);
	}

	public function cursos(){
		require_once('modelos/Curso.php');
		return $this->hasMany(Curso::class);
	}

	public function noticias(){
		require_once('modelos/Noticia.php');
		return $this->hasMany(Noticia::class);
	}
}