<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Categoria extends Eloquent{
	use SoftDeletes;
	protected $table = 'categorias';
	protected $dates = ['deleted_at'];
	protected $fillable = ['titulo_categoria'];

	public function noticias(){
		require('modelos/Noticia.php');
		return $this->hasMany(Noticia::class);
	}

	public function setTituloCategoriaAttribute($old){
		$this->attributes['titulo_categoria'] = strtoupper($old);
	}
}