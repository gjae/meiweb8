<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class CursoImagenes extends Eloquent{
	use SoftDeletes;
	protected $table = 'curso_imagenes';
	protected $fillable = [
		'nombre',
		'curso_id'
	];
	public $dates = ['deleted_at'];

	public function curso(){
		require_once('modelos/Curso.php');
		return $this->hasMany(Curso::class);
	}

}