<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Etiqueta extends Eloquent{
	use SoftDeletes;
	protected $table = 'etiquetas';
	protected $dates = ['deleted_at'];
	protected $fillable = ['nombre_etiqueta'];

	public function noticias(){
		require('modelos/Noticia.php');
		return $this->belongsToMany(Noticia::class);
	}

	public function setNombreEtiquetaAttribute($old){
		$this->attributes['nombre_etiqueta'] = strtoupper($old);
	}

}