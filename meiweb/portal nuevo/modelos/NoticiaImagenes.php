<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class NoticiaImagenes extends Eloquent{

	use SoftDeletes;
	protected $table = 'noticia_imagenes';
	public $dates = ['deleted_at'];
	protected $fillable = [
		'nombre', 'noticia_id'
	];

	public function noticia(){
		require_once('modelos/Noticia.php');
		return $this->belongsTo(Noticia::class);
	}
}