<?php 
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Seccion extends Eloquent{
	use SoftDeletes;
	public $dates = ['deleted_at'];
	protected $table = 'seccion';
	protected $fillable = [
		'cod_seccion', 'estudiantes_minimos', 'estudiantes_maximos'
	];

	public function materia_profesor(){
		require_once('modelos/MatSecProf.php');
		return $this->hasMany(MatSecProf::class);
	}
}