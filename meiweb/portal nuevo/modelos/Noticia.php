<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Noticia extends Eloquent{
	use SoftDeletes;
	protected $table = 'noticias';
	protected $fillable = [
		'titulo', 'texto', 'usuario_id', 'numero_visitas', 'categoria_id'
	];

	public $dates = ['deleted_at'];

	public function imagenes(){
		require_once('modelos/NoticiaImagenes.php');
		return $this->hasMany(NoticiaImagenes::class);
	}

	public function usuario(){
		require_once('modelos/Usuario.php');
		return $this->belongsTo(Usuario::class);
	}

	public function etiquetas(){
		require_once('modelos/Etiqueta.php');
		return $this->belongsToMany(Etiqueta::class);
	}

	public function categoria(){
		require_once('modelos/Categoria.php');
		return $this->belongsTo(Categoria::class);
	}

}