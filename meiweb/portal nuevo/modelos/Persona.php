<?php 
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Persona extends Eloquent{
	use SoftDeletes;
	public $dates = ['deleted_at', 'fecha_nacimiento'];
	protected $table = 'personas';
	protected $fillable = [
		'nombre', 'apellido', 'telefono', 'fecha_nacimiento'
	];
	protected $casts = ['fecha_nacimiento' => 'date'];


	public function setFechaNacimientoAttribute($old){
		$this->attributes['fecha_nacimiento'] = Carbon::parse($old)->format('Y-m-d');
	}

	public function setNombreAttribute($old){
		$this->attributes['nombre'] = strtoupper($old);
	}

	public function setApellidoAttribute($old){
		$this->attributes['apellido'] = strtoupper($old);
	}

	public function usuario(){
		require_once('modelos/Usuario.php');
		return $this->hasOne(Usuario::class);
	}
}