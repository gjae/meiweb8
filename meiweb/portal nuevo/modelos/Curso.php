<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Curso extends Eloquent{
	use SoftDeletes;
	protected $table = 'cursos';
	protected $fillable = [
		'nombre_curso',
		'descripcion',
		'duracion',
		'codigo_curso',
		'usuario_id',
		'fecha_inicio',
		'duracion',
		'modalidad',
		'creditos',
		'nivel'
	];
	public $dates = ['fecha_inicio'];
	protected $casts = ['fecha_inicio' => 'date'];

	public function profesor(){
		require_once('modelos/Usuario.php');
		return $this->belongsTo(Usuario::class, 'usuario_id');
	}

	public function imagenes(){
		require_once('modelos/CursoImagenes.php');
		return $this->hasMany(CursoImagenes::class);
	}

	public function setFechaInicioAttribute($old){
		$this->attributes['fecha_inicio'] = Carbon::parse($old)->format('Y-m-d');
	}

}