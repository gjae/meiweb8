create table IF NOT EXISTS personas(
	created_at datetime default null,
	updated_at datetime default null,
	deleted_at datetime default null,
	id integer unsigned auto_increment primary key,
	nombre varchar(100) not null,
	apellido varchar(100) not null,
	telefono varchar(17) default null,
	fecha_nacimiento date
);

create table IF NOT EXISTS usuarios(
	created_at datetime default null,
	updated_at datetime default null,
	deleted_at datetime default null,
	id integer unsigned auto_increment primary key,
	correo varchar(150) unique not null,
	clave varchar(40) default '-----------',
	tipo_usuario enum('ADM', 'PRF', 'EST') default 'EST',
	persona_id integer unsigned not null,
	avatar varchar(100) default 'default-avatar.png',
	sobre_mi text default null,
	fecha_ingreso date default null,
	fecha_inicio_profesoral date default null,
	titulo_profesion varchar(110) default 'NO APLICA'
);

create table IF NOT EXISTS cursos(
	created_at datetime default null,
	updated_at datetime default null,
	deleted_at datetime default null,
	id integer unsigned auto_increment primary key,
	codigo_curso varchar(5) unique not null,
	nombre_curso varchar(30) not null,
	descripcion text default null,
	usuario_id integer unsigned not null,
	fecha_inicio date not null,
	duracion integer not null,
	modalidad enum('VIRT','PRES') default 'PRES',
	creditos integer not null,
	nivel enum('INTERMEDIO', 'BAJO', 'EXPERTO') default 'BAJO'
);

create table IF NOT EXISTS curso_imagenes(
	created_at datetime default null,
	updated_at datetime default null,
	deleted_at datetime default null,
	id integer unsigned auto_increment primary key,
	nombre varchar(60) not null,
	curso_id integer unsigned not null

);

create table IF NOT EXISTS etiquetas(
	created_at datetime default null,
	updated_at datetime default null,
	deleted_at datetime default null,
	id integer unsigned auto_increment primary key,
	nombre_etiqueta varchar(100)

);

create table IF NOT EXISTS etiqueta_noticia(
	created_at datetime default null,
	updated_at datetime default null,
	deleted_at datetime default null,
	id integer unsigned auto_increment primary key,
	etiqueta_id integer unsigned not null,
	noticia_id integer unsigned not null
);

create table IF NOT EXISTS categorias(
	created_at datetime default null,
	updated_at datetime default null,
	deleted_at datetime default null,
	id integer unsigned auto_increment primary key,
	titulo_categoria varchar(40) not null
);

create table IF NOT EXISTS noticias(
	created_at datetime default null,
	updated_at datetime default null,
	deleted_at datetime default null,
	id integer unsigned auto_increment primary key,
	titulo varchar(200) not null,
	texto text not null,
	usuario_id integer unsigned not null,
	numero_visitas integer default 0,
	categoria_id integer unsigned not null
);

create table IF NOT EXISTS noticia_imagenes(
	created_at datetime default null,
	updated_at datetime default null,
	deleted_at datetime default null,
	id integer unsigned auto_increment primary key,
	nombre varchar(62) not null,
	noticia_id integer unsigned not null
);

delete from personas;
delete from usuarios;

insert into personas(id, nombre, apellido, fecha_nacimiento)
	VALUES(1, 'ADMIN', 'ADMIN', '1990-01-01');

INSERT INTO usuarios(id, persona_id, correo, clave, tipo_usuario)
	VALUES(1, 1, 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 'ADM');