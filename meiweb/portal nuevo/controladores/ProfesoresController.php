<?php 

require_once('modelos/Usuario.php');
require_once('modelos/Persona.php');
use Carbon\Carbon;

class ProfesoresController {
	private $user;

	public function __construct(){
		$this->user = toObject(getSessionUser());
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
	}
	
	public function index( $serv ){
		extract($serv);
		return $view->make('paginas.profesores', [
				'user' => $this->user->user,
				'persona' => $this->user,
				'request' => $request
			]);
	}

	public function formularios( $serv ){
		updateTime();
		extract($serv);

		$form = $view->make('formularios.'.$request['tipo'], [
				'tipo_usuario' => 'PRF',
				'tipo_boton' => 'submit',
				'btn_primary' => 'Guardar datos',
				'btn_default' => 'Cerrar ventana',
				'profesor' => null
			])->render();
		return json_encode(['error' => false, 'formulario' => $form]);
	}

	public function buscar( $serv ){
		updateTime();
		$u = Usuario::where('correo', $serv['request']['correo'])->first();

		$form = $serv['view']->make('formularios.profesor', [
			'profesor' => $u,
			'tipo_usuario' => 'PRF',
			'tipo_boton' => 'not_submit',
			'btn_primary' => 'Guardar datos',
			'btn_default' => 'Cerrar ventana',

		])->render();
		return json_encode(['error' => false, 'formulario' => $form]);
	}

	public function eliminar( $ser ){
		$u = Usuario::find($_POST['profesor_id']);

		if($u->delete()){
			return json_encode(['error' => false, 'mensaje' => 'PROFESOR ELIMINADO CORRECTAMENTE', 'recargar' => true]);
		}
		return json_encode(['error' => true, 'mensaje' => 'ERROR AL INTENTAR ELIMINAR EL PROFESOR', 'recargar' => true]);
	}

	public function formulario( $serv ){
		updateTime();
		extract($serv);

		return call_user_func_array([$this, $_POST['accion_formulario']],[
				'datos' => $_POST,
				'serv' => $serv
			]);
	}

	private function guardar( $datos , $serv ){
		updateTime();

		$acceptedFormats = [
			'png', 'PNG', 'jpg','JPG',
			'jpeg', 'JPEG','gif', 'GIF'
		];

		$serv['DB_CLASS']::beginTransaction();
		try {
			$persona = new Persona($datos);
			if($persona->save()){
				$persona->usuario()->save(
					new Usuario($datos)
				);
				var_dump($_FILES['avatar']);
				if(! is_null($persona->usuario)){
					if( !empty($_FILES['avatar']) && !empty($_FILES['avatar']['name']) ){

						$name = $_FILES['avatar']['name'];
						$type = explode('/', $_FILES['avatar']['type'])[1];
						$name = md5($name).'.'.$type;
						if(!in_array($type, $acceptedFormats)){
							throw new \Exception("Error en el formato del archivo", 1);
							
						}
						file_put_contents( $serv['conf']['assets'].'/cargas/'.$name, file_get_contents($_FILES['avatar']['tmp_name']));

					 	$u = $persona->usuario;
					 	$u->avatar = $name;
					 	$u->save();
					}
					$serv['DB_CLASS']::commit();
					return redirect('index.php?cont=profesores&error=false');
				}
			}
			throw new \Exception("Error al procesar la transaccion", 1);
			
		} catch (\Exception $e) {
			echo $e->getMessage();
			$serv['DB_CLASS']::rollback();
			exit;
			return redirect('index.php?cont=profesores&error=true');
		}
	}

	public function getProfesores( $serv ){
		updateTime();

		$estudiantes = Usuario::where('tipo_usuario', 'PRF')->get();

		$array = [];
		foreach ($estudiantes as $key => $estudiante) {
			$array[$key][] = $estudiante->persona->nombre;
			$array[$key][] = $estudiante->persona->apellido;
			$array[$key][] = $estudiante->correo;
			$array[$key][] = $estudiante->persona->telefono;
			$array[$key][] = $estudiante->persona->fecha_nacimiento->format('d/m/Y'); 
		}

		$datos = [
			'data' => $array
		];

		return json_encode($datos);
	}

}