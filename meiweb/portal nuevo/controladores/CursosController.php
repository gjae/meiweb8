<?php

require_once('modelos/Curso.php');
require_once('modelos/CursoImagenes.php');
require_once('modelos/Usuario.php');
use Carbon\Carbon;
class CursosController {
	private $user;

	public function __construct(){
		$this->user = toObject(getSessionUser());
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
	}
	public function index( $serv ){
		extract($serv);
		updateTime();

		return $view->make('paginas.cursos', [
				'user' => $this->user->user,
				'persona' => $this->user,
				'request' => $request
			]);
	}

	public function getCursos( $serv ){
		updateTime();

		$cursos = Curso::all();

		$array = [];
	
		foreach ($cursos as $key => $curso) {
			$array[$key][] = $curso->codigo_curso;
			$array[$key][] = $curso->nombre_curso;
			$array[$key][] = $curso->descripcion;
			$array[$key][] = $curso->fecha_inicio->format('d/m/Y');
			$array[$key][] = $curso->duracion;
			$array[$key][] = $curso->modalidad;
			$array[$key][] = $curso->nivel;
		}
		$datos = [
			'data' => $array
		];

		return json_encode($datos);
	}

	public function formulario( $serv ){
		updateTime();
		extract($serv);

		$accion_formulario = ( $_POST['curso_id'] == '0' ) ? $_POST['accion_formulario'] : 'editar';

		return call_user_func_array([$this,$accion_formulario],[
				'datos' => $_POST,
				'serv' => $serv
			]);
	}

	public function eliminar( $serv ){
		updateTime();

		$mat = Curso::find($_POST['materia_id']);
		if($mat->delete()){
			return json_encode(['error' => false, 'mensaje' => 'MATERIA ELIMINADA SATISFACTORIAMENTE', 'recargar' => true]);
		}
		return json_encode(['error' => true, 'mensaje' => 'MATERIA ELIMINADA HA OCURRIDO UN ERROR IMPROVISTO', 'recargar' => true]);
	}

	public function buscar( $serv ){
		updateTime();
		$curso = Curso::where('codigo_curso', trim($serv['request']['codigo']))->first();
		$form = $serv['view']->make('formularios.materia', [
			'tipo_boton' => 'submit',
			'btn_primary' => 'Guardar datos',
			'btn_default' => 'Cerrar ventana',
			'materia' => $curso
		])->render();

		return json_encode(['error' => false, 'formulario' => $form]);
	}

	private function guardar( $datos, $serv ){

		$extensiones = [
			'JPG', 'jpg', 'png', 'PNG', 'gif', 'GIF', 'jpeg', 'JPEG'
		];
		$serv['DB_CLASS']::beginTransaction();

		try {
			$curso = new Curso($datos);
			if( $curso->save() ){
				$archivos = $_FILES['imagenes'];
				if(!empty($archivos)){
					foreach ($archivos['name'] as $key => $archivo) {
						if( empty($archivos['name'][$key]) ) continue;

						$type = explode( '/', $archivos['type'][$key] )[1];
						if(in_array($type, $extensiones)){

							$fecha = Carbon::now()->format('Y-m-d/H:m:i-A');
							$name = md5($fecha.'00'.$key.$archivos['name'][$key]).'.'.$type;

							move_uploaded_file(
								$archivos['tmp_name'][$key], 
								$serv['conf']['assets'].'/cargas/cursos/'.$name);

							$curso->imagenes()->save(
								new CursoImagenes(['nombre' => $name])
							);
						}
					}
				}

				$serv['DB_CLASS']::commit();
				return redirect('index.php?cont=cursos&error=false');
			}
			throw new \Exception("ERROR AL GUARDAR EL CURSO", 1);
			
		} catch (\Exception $e) {
			$serv['DB_CLASS']::rollback();
			echo $e->getMessage();
			exit;
			return redirect('index.php?cont=cursos&error=true');
		}

		
	}

	private function editar( $datos, $serv ){
		$curso = Materia::find($datos['curso_id']);

		$cursos_update = $curso->toArray();
		foreach ($cursos_update as $dato => $valor) {
			if(array_key_exists($dato, $datos))
				$curso->$dato = $datos[$dato];
		}
		if($curso->save()){
			return redirect('index.php?cont=cursos&error=false');
		}

		return redirect('index.php?cont=cursos&error=true');
	}

	public function formularios( $serv ){
		updateTime();
		extract($serv);

		$form = $view->make('formularios.'.$request['tipo'], [
				'tipo_boton' => 'submit',
				'btn_primary' => 'Guardar datos',
				'btn_default' => 'Cerrar ventana',
				'materia' => null,
				'profesores' => Usuario::where('tipo_usuario', 'PRF')->get()
			])->render();
		return json_encode(['error' => false, 'formulario' => $form]);
	}

}