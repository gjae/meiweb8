<?php
include_once('modelos/Persona.php');
include_once('modelos/Usuario.php');
include_once('modelos/Curso.php');
include_once('modelos/Noticia.php');

class IndexController
{
	
	public function index($service){
		extract($service);

		$cursos = Curso::orderBy('created_at', 'DESC')->take(2)->get();
		return $view->make('meiweb.index', ['request' => $request, 'cursos' => $cursos]);
	}

	public function noticias( $service ){
		extract($service);

		$page = (isset($request['page']) && !empty($request['page'])) ? $request['page']: 1;
		$noticias  = new Noticia;
		if( isset($request['categoria_id']) && !empty($request['categoria_id']) )
			$noticias = $noticias->where('categoria_id', $request['categoria_id']);
		return $view->make('meiweb.posts_noticias', [
				'noticias' => $noticias->orderBy('created_at', 'DESC')
                                       ->paginate(3, ['*'], 'page', $page), 
			]);
	}

	public function ingresar($service){
		extract($service);
		return $view->make('paginas.login', ['request' => $request]);

	}

	public function login($service){
		extract($service);
		$usuario = Usuario::where('correo', $_POST['correo'])
					->where('clave', md5(trim($_POST['clave'])))->first();


		if(! is_null($usuario)){
			setSessionWith($usuario);
			return redirect('index.php?cont=index&meth=dashboard');
		}	
		return redirect('index.php?cont=index&error=true');
	}

	public function dashboard($service){
		extract($service);
		$user = toObject(getSessionUser());
		if( !is_null($user) && checkMinutes($user->tiempo, $conf['session_expire']) ){
			updateTime();
			return $view->make('paginas.dashboard', ['user' => $user->user]);
		}
		else redirect('index.php?cont=index');
	}

	public function salir($service){
		if(sessionActive()){
			sessionDestroy();
		}
	}
}