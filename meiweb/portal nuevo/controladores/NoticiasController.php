<?php  

require_once('modelos/Noticia.php');
require_once('modelos/NoticiaImagenes.php');

require_once('modelos/Etiqueta.php');
require_once('modelos/Categoria.php');
use Carbon\Carbon;
class NoticiasController {

	private $user;

	public function __construct(){
		$this->user = toObject(getSessionUser());
	}

	public function ver( $serv ){
		extract($serv);
		$noticia = Noticia::find($request['id']);
		$noticia->numero_visitas = $noticia->numero_visitas + 1;
		$noticia->save();


		$populares = Noticia::where('id', '<>', $request['id'])
						->orderBy('numero_visitas', 'DESC')->take(4)->get();

		$mas_vistas = Noticia::orderBy('numero_visitas', 'DESC')->take(3)->get();
		return $view->make('meiweb.post', [
			'noticia' => $noticia,
			'mas_vistas' => $mas_vistas,
			'populares' => $populares
		]);
	}

	public function guardar_categoria( $serv ){
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
		updateTime();

		$categoria = new Categoria($_POST);
		if($categoria->save()){
			return redirect('index.php?cont=noticias&error=false');
		}
		return redirect('index.php?cont=noticias&error=true');

	}

	public function index( $serv ){
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
		updateTime();
		extract($serv);
		return $view->make('meiweb.noticias',[
				'user' => $this->user->user,
				'persona' => $this->user,
				'request' => $request
			]);
	}

	public function formularios( $serv ){
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}

		updateTime();
		extract($serv);

		$form = $view->make('formularios.'.$request['tipo'], [
				'tipo_boton' => 'submit',
				'btn_primary' => 'Guardar datos',
				'btn_default' => 'Cerrar ventana',
				'user' => $this->user->user,
				'noticia' => null
			])->render();
		return json_encode(['error' => false, 'formulario' => $form]);
	}

	public function formulario( $serv ){
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
		updateTime();
		extract($serv);

		return call_user_func_array([$this, $_POST['accion_formulario']],[
				'datos' => $_POST,
				'serv' => $serv
			]);
	}

	private function guardar( $datos , $serv ){
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
		updateTime();

	
		$acceptedFormats = [
			'png', 'PNG', 'jpg','JPG',
			'jpeg', 'JPEG','gif', 'GIF'
		];

		$serv['DB_CLASS']::beginTransaction();
		try {
			$noticia = new Noticia($datos);
			if($noticia->save()){
				$archivos = $_FILES['imagenes'];
				if(!empty($archivos)){
					foreach ($archivos['name'] as $key => $archivo) {
						if( empty($archivos['name'][$key]) ) continue;

						$type = explode( '/', $archivos['type'][$key] )[1];

						
						if(in_array($type, $acceptedFormats)){
							$fecha = Carbon::now()->format('Y-m-d/H:m:i-A');
							$name = md5($fecha.'00'.$key.$archivos['name'][$key]).'.'.$type;

							move_uploaded_file(
								$archivos['tmp_name'][$key], 
								$serv['conf']['assets'].'/cargas/noticias/'.$name);

							$noticia->imagenes()->save(
								new NoticiaImagenes(['nombre' => $name])
							);
						}else{
							echo "ENTRO EN EL ELSE";
							echo $type;
							
						}
					}
					$etiquetas = trim($_POST['etiquetas']);
					if( !empty($etiquetas) )
					{
						if ( !is_bool( strstr($etiquetas, ';') ) ) {
							$array_etiquetas = explode(';', $etiquetas);
							foreach ($array_etiquetas as $key => $etiqueta) {
								$objEtiqueta = Etiqueta::where('nombre_etiqueta', strtoupper($etiqueta))->first();

								if(!$objEtiqueta){
									$objEtiqueta = Etiqueta::create(['nombre_etiqueta' => $etiqueta]);

								}
								$noticia->etiquetas()->attach($objEtiqueta->id);
							}
						}else{
							$objEtiqueta = Etiqueta::where('nombre_etiqueta', strtoupper($etiquetas))->first();

							if(!$objEtiqueta){
								$objEtiqueta = Etiqueta::create(['nombre_etiqueta' => $etiqueta]);

							}
							$noticia->etiquetas()->attach($objEtiqueta->id);
						}
					}
					
					$serv['DB_CLASS']::commit();
					return redirect('index.php?cont=noticias&error=true');
				}

			}
			throw new \Exception("Error al procesar la transaccion", 1);
			
		} catch (\Exception $e) {
			$serv['DB_CLASS']::rollback();
			echo $e->getMessage();
			exit;
			return redirect('index.php?cont=noticias&error=true');
		}
	}

	public function buscar( $serv ){
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
		updateTime();

		$noticia = Noticia::find($serv['request']['codigo']);
		$etiquetas = $noticia->etiquetas->toArray();
		$array = [];
		foreach ($etiquetas as $key => $et) {
			array_push($array, $et['nombre_etiqueta']);
		}
		$form = $serv['view']->make('formularios.formulario_noticia', [
				'tipo_boton' => 'not_submit',
				'btn_primary' => 'Guardar datos',
				'btn_default' => 'Cerrar ventana',
				'user' => $this->user->user,
				'noticia' => $noticia,
				'etiquetas' => $array
			])->render();

		return json_encode(['error' => false, 'formulario' => $form]);
	}

	public function eliminar( $serv ){
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
		updateTime();
		$noticia = Noticia::find($_POST['id']);
		if($noticia->delete()){
			return json_encode(['error'=> false, 'mensaje' => 'REGISTRO SUPRIMIDO CORRECTAMENTE', 'recargar' => true]);
		}

		return json_encode(['error'=> true, 'mensaje' => 'ERROR INESPERADO AL SUPRIMIR EL REGISTRO', 'recargar' => true]);
	}

	public function getNoticias( $serv ){
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
		updateTime();
		$noticias = Noticia::all();

		$array = [];
		foreach ($noticias as $key => $noticia) {
			$array[$key][] = $noticia->id;
			$array[$key][] = $noticia->titulo;
			$array[$key][] = $noticia->created_at->format('d/m/Y');
			$array[$key][] = $noticia->usuario->persona->nombre.' '.$noticia->usuario->persona->apellido;
			$array[$key][] = $noticia->categoria->titulo_categoria;
		}

		$data = [
			'data' => $array
		];

		return json_encode($data);
	}
}