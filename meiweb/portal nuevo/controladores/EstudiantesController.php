<?php 

require_once('modelos/Usuario.php');
require_once('modelos/Persona.php');
require_once('modelos/ListadoSeccion.php');
require_once('modelos/MatSecProf.php');
require_once('modelos/Materia.php');
use Carbon\Carbon;
class EstudiantesController{
	private $user;

	public function __construct(){
		$this->user = toObject(getSessionUser());
		if( is_null($this->user) || !sessionActive() ){
			sessionDestroy();
			redirect();
		}
	}

	public function index( $serv ){
		extract($serv);
		updateTime();
		return $view->make('paginas.estudiantes', [
				'user' => $this->user->user,
				'persona' => $this->user,
				'request' => $request,
			]);
	}

	public function asignar_materias( $serv ){
		updateTime();
		extract($serv);
		$usuario = Usuario::find($request['user_id']);
		return $view->make('paginas.inscribir',[
				'user' => $this->user->user,
				'persona' => $this->user,
				'usuario' => $usuario,
				'request' => $request,
				'materias' => Materia::all(),
				'matsecprof' => MatSecProf::class
			]);

	}

	public function inscribir( $serv ){
		extract($serv);
		$DB_CLASS::beginTransaction();
		try {
			foreach ($_POST['materia_seccion_profesor_id'] as $key => $dato) {
				if( !empty($dato) ){
					$registro = new ListadoSeccion([
						'materia_seccion_profesor_id' => $dato,
						'usuario_id' => $_POST['usuario_id']
					]);
					if(!$registro->save()){
						throw new \Exception("ERROR AL PROCESAR LA ASIGNACION", 1);
						
					}
				}
			}		
			$DB_CLASS::commit();
			return redirect('index.php?cont=estudiantes&error=false');
		} catch (\Exception $e) {
			$DB_CLASS::rollback();
			var_dump($e->getMessage());
			exit;
			return redirect('index.php?cont=estudiantes&error=true');
		}

	}

	public function buscar( $serv ){
		updateTime();
		$u = Usuario::where('correo', $serv['request']['correo'])->first();
		$form = $serv['view']->make('formularios.crear_estudiante', [
			'estudiante' => $u,
			'tipo_usuario' => 'EST',
			'tipo_boton' => 'submit',
			'btn_primary' => 'Guardar datos',
			'btn_default' => 'Cerrar ventana',

		])->render();

		return json_encode(['error' => false, 'formulario' => $form]);
	}

	public function formularios( $serv ){
		updateTime();
		extract($serv);

		$form = $view->make('formularios.'.$request['tipo'], [
				'tipo_usuario' => 'EST',
				'tipo_boton' => 'submit',
				'btn_primary' => 'Guardar datos',
				'btn_default' => 'Cerrar ventana',
				'estudiante' => null
			])->render();
		return json_encode(['error' => false, 'formulario' => $form]);
	}

	public function getEstudiantes( $serv ){
		updateTime();

		$estudiantes = Usuario::where('tipo_usuario', 'EST')->get();

		$array = [];
		foreach ($estudiantes as $key => $estudiante) {
			$array[$key][] = $estudiante->persona->nombre;
			$array[$key][] = $estudiante->persona->apellido;
			$array[$key][] = $estudiante->correo;
			$array[$key][] = $estudiante->persona->telefono;
			$array[$key][] = $estudiante->persona->fecha_nacimiento->format('d/m/Y'); 
		}

		$datos = [
			'data' => $array
		];

		return json_encode($datos);
	}

	public function formulario( $serv ){
		updateTime();
		extract($serv);

		return call_user_func_array([$this, $_POST['accion_formulario']],[
				'datos' => $_POST,
				'serv' => $serv
			]);
	}

	private function guardar( $datos , $serv ){
		updateTime();

		$serv['DB_CLASS']::beginTransaction();
		try {
			$persona = new Persona($datos);
			if($persona->save()){
				$persona->usuario()->save(
					new Usuario($datos)
				);
				if(! is_null($persona->usuario)){
					$serv['DB_CLASS']::commit();
					return redirect('index.php?cont=estudiantes&error=false');
				}
			}
			throw new \Exception("Error al procesar la transaccion", 1);
			
		} catch (\Exception $e) {
			$serv['DB_CLASS']::rollback();
			return redirect('index.php?cont=estudiantes&error=true');
		}
	}
}