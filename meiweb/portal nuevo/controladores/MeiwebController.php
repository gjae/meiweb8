<?php
include_once('modelos/Persona.php');
include_once('modelos/Usuario.php');
include_once('modelos/Curso.php');

class MeiwebController
{
	public function docentes_presenciales($serv){
		extract($serv);
		$profesores = Usuario::where('tipo_usuario', 'PRF')->get();
		return $view->make('meiweb.docentes_presenciales', ['profesores' => $profesores]);
	}

	public function docente( $serv ){
		extract($serv);
		$docente = Usuario::find($request['docente_id']);
		return $view->make('meiweb.docente', ['profesor' => $docente]);
	}

	public function cursos( $serv ){
		extract($serv);

		if (isset($request['curso_id'])) {
			$curso = Curso::find($request['curso_id']);
			return $view->make('meiweb.post_curso',['curso' => $curso]);
		}
		else{
			$cursos = Curso::where('modalidad', $request['tipo'])->get();
			return $view->make('meiweb.cursos_virtuales', ['cursos' => $cursos, 'tipo' =>  $request['tipo']]);
		}
	} 

}