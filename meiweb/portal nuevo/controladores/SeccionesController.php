<?php 

require_once('modelos/Seccion.php');
require_once('modelos/MatSecProf.php');
require_once('modelos/HorarioMateria.php');
require_once('modelos/Usuario.php');
require_once('modelos/ListadoSeccion.php');
require_once('modelos/Materia.php');

use Dompdf\Dompdf as PDF;

class SeccionesController {

	private $user = null;

	public function __construct(){
		$this->user = toObject(getSessionUser());
		if( is_null($this->user) || !sessionActive() ){
			redirect();
		}
	}
	public function index($service){
		extract($service);
		updateTime();
		return $view->make('paginas.secciones', ['user' => $this->user->user, 'persona' => $this->user]);
	}

	public function getSecciones(){
		updateTime();
		$secciones = Seccion::select('cod_seccion', 'estudiantes_minimos', 'estudiantes_maximos')->get();

		$array = [];
		foreach ($secciones as $key => $seccion) {
			$array[$key][] = $seccion->cod_seccion;
			$array[$key][] = $seccion->estudiantes_minimos;
			$array[$key][] = $seccion->estudiantes_maximos;
		}

		$data = [
			'data' => $array
		];
		return json_encode($data);
	}

	
	public function formulario( $serv ){
		updateTime();
		extract($serv);

		$accion_formulario = ( $_POST['seccion_id'] == '0' ) ? $_POST['accion_formulario'] : 'editar';

		return call_user_func_array([$this,$accion_formulario],[
				'datos' => $_POST,
				'serv' => $serv
			]);
	}

	public function tipo_reporte( $serv ){
		updateTime();
		extract($serv);

		return $view->make('formularios.tipo_reporte_seccion', ['seccion' => $request['codigo_seccion']]);
	}

	public function reportes( $serv ){
		updateTime();

		return call_user_func_array([$this, $serv['request']['tipo_reporte']], ['parametros' => $serv]);
	}

	private function por_materia( $serv ){
		updateTime();
		extract($serv);


		$seccion = Seccion::where('cod_seccion', $request['codigo_seccion'])->first();
		$pdf = new PDF;
		$vista = $view->make('reportes.lista_seccion_por_materia', [
			'seccion' => $seccion,
			'materias' => $seccion->materia_profesor()->orderBy('materia_id')->get()
		])->render();

		$pdf->loadHtml($vista);
		$pdf->setPaper('A4');
		$pdf->render();
		return $pdf->stream('reporte', ['Attachment' => false]);
	}

	private function editar( $datos, $serv ){

		$serv['DB_CLASS']::beginTransaction();

		try {
			foreach ($datos['profesor_id'] as $key => $value) {
				if( !empty($value) ){
					$asignar = new MatSecProf([
						'seccion_id' => $datos['seccion_id'],
						'materia_id' => $datos['materia_id'][$key],
						'usuario_id' => $datos['profesor_id'][$key]
					]);
					if( !$asignar->save() ){
						throw new \Exception("Error al insertar materia y profesores en la seccion", 1);
						
					}
				}
				else
					continue;
			}

			$serv['DB_CLASS']::commit();
			return redirect('index.php?cont=secciones&error=false');			
		} catch (\Exception $e) {
			$serv['DB_CLASS']::rollback();
			return redirect('index.php?cont=secciones&error=true');	
			
		}
	}

	public function buscar( $serv ){
		updateTime();
		$seccion = Seccion::where('cod_seccion', trim($serv['request']['codigo']))->first();
		$form = $serv['view']->make('formularios.seccion', [
			'seccion' => $seccion,
			'tipo_boton' => 'submit',
			'btn_primary' => 'Guardar datos',
			'btn_default' => 'Cerrar ventana',
			'materias' => Materia::all(),
			'profesores' => Usuario::where('tipo_usuario', 'PRF')->get()
		])->render();

		return json_encode(['error' => false, 'formulario' => $form]);
	}

	public function eliminar( $service ){
		updateTime();
		$seccion = Seccion::find($_POST['seccion_id']);
		if($seccion->delete()){
			return json_encode(['error' => false, 'mensaje' => 'LA SECCION HA SIDO ELIMINADA CORRECTAMENTE', 'recargar' => true]);
		}
		return json_encode(['error' => true, 'mensaje' => 'LA SECCION HA SIDO ELIMINADA CORRECTAMENTE', 'recargar' => true]);
	}

	public function formularios( $serv ){
		updateTime();
		extract($serv);

		$form = $view->make('formularios.'.$request['tipo'], [
				'tipo_boton' => 'submit',
				'btn_primary' => 'Guardar datos',
				'btn_default' => 'Cerrar ventana',
			])->render();
		return json_encode(['error' => false, 'formulario' => $form]);
	}


	public function guardar( $datos, $serv ){
		$seccion = new Seccion($_POST);
		if( $seccion->save() ){
			return redirect('index.php?cont=secciones&error=false');
		}
		return redirect('index.php?cont=secciones&error=true');
	}
}