<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	

	$baseDatos=new BD();
	
if(comprobarSession())
{	
	$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=7";
	$resultado=$baseDatos->ConsultarBD($sql);
	list($estado)=mysql_fetch_array($resultado);
	if($estado==1)
	{
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<style type="text/css">
			<!--
			.Estilo1 {color: #FF0000}
			-->
        </style>
</head>
<body>
	<style type="text/css">
		<!--
.Estilo1 {color: #FF0000}
-->
	</style>
<script> 
	function enviarCancelar()
	{
		location.replace("../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>");
	}
		
	function validar()
	{
		if(document.frm_cargarHerramienta.hid_url.value==1) //archivo
		{
			if(document.frm_cargarHerramienta.txt_nombreHerramienta.value!=false && document.frm_cargarHerramienta.fil_archivo.value!=false)
			{
				document.frm_cargarHerramienta.submit();
			}
			else
			{
				alert ("Debe llenar completamente la información solicitada.");
			}
		}
		else if(document.frm_cargarHerramienta.hid_url.value==2)
		{
			if(document.frm_cargarHerramienta.txt_nombreHerramienta.value!=false && (document.frm_cargarHerramienta.txt_url.value!=false && document.frm_cargarHerramienta.txt_url.value!="http://"))
			{
				document.frm_cargarHerramienta.submit();
			}
			else
			{
				alert ("Debe llenar completamente la información solicitada.");
			}
		}
		
	}
	
	function chk_habilitar()
	{
		for(i=0;i<document.frm_cargarHerramienta.elements.length;i++)
		{
			if(document.frm_cargarHerramienta.elements[i].id=="grupo")
				document.frm_cargarHerramienta.elements[i].checked=true;
		}	
	}
	
	function chk_deshabilitar()
	{
		for(i=0;i<document.frm_cargarHerramienta.elements.length;i++)
		{
			if(document.frm_cargarHerramienta.elements[i].id=="grupo")
				document.frm_cargarHerramienta.elements[i].checked=false;
		}
	}
	
	function chk_click ()
	{
		var radioTodos=document.getElementById('radioT');
		var radioSelec=document.getElementById('radioS');	
		radioTodos.checked=false;
		radioSelec.checked=true;	
	}
			
	function activarHerramientaURL()
	{
		document.frm_cargarHerramienta.fil_archivo.disabled=true;
		document.frm_cargarHerramienta.txt_url.disabled=false;
		document.frm_cargarHerramienta.hid_url.value=2;
	}
	
	function activarHerramientaArchivo()
	{
		document.frm_cargarHerramienta.fil_archivo.disabled=false;
		document.frm_cargarHerramienta.txt_url.disabled=true;
		document.frm_cargarHerramienta.txt_url.value='http://';
		document.frm_cargarHerramienta.hid_url.value=1;
	}
</script> 
		
<table class="tablaPrincipal">
	<tr valign="top">
    	<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacio">&nbsp;</td>
		<td>
                  <table class="tablaMarquesina" >
                    <tr>
            <?	 				
                    if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
                        $sql123 = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
                    else
                        $sql123 = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
                    $resul = $baseDatos->ConsultarBD($sql123);
                    list($nom) = mysql_fetch_row($resul); ?>	  
                      <td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nom))?></a><a> -> </a>Agregar Herramienta</td>
                    </tr>
                  </table><br>
        <form  enctype="multipart/form-data" name="frm_cargarHerramienta" method="post" action="guardarHerramienta.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>">
			<table class="tablaGeneral" ><!-- Agregar-->
				<tr class="trTitulo" >
					<td colspan="4"><img src="imagenes/herramientas.gif" width="16" height="16" align="texttop"> Agregar Herramienta</td>
				</tr>
<?
		if($_GET['error'] == '0x001')
		{
?>
				<tr class="trAviso">
					<td colspan="3"><img src="imagenes/error.gif" width="17" height="18" align="texttop">
						<span class="Estilo1"> Error: No se pudo cargar la herramienta, excede el tamaño permitido por el servidor. Consulte con el administrador del sistema.</span></td>
				</tr>
<?
		}
		if($_GET['error'] == '0x002')
		{
?>
				<tr class="trAviso">
					<td colspan="3"><img src="imagenes/error.gif" width="17" height="18" align="texttop">
						<span class="Estilo1"> Error: No se pudo cargar la herramienta, el icono excede el tamaño permitido por el servidor. Consulte con el administrador del sistema.</span></td>
				</tr>
<?
		}
		
?>
				<tr class="trInformacion">
					<td width="24%"><b>Autor:</b></td>
					<td colspan="2"><?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?></td>
				</tr>
				<tr class="trInformacion">
					<td><b>Nombre Herramienta:</b></td>
					<td colspan="2"><input type="text" name="txt_nombreHerramienta" size="20" maxlength="100" class="link" value="<?=$_GET['nombre']?>"></td>
				</tr>
<?
	$valor=recuperarVariableSistema("sistemagregararchivo");
	if ($valor==1)
	{
?>				
                <tr class="trInformacion">
					<td><b>Agregar Herramienta:</b></td>
					<td width="18%"><input name="rad_herramienta" type="radio" value="1" onClick="javascript:activarHerramientaArchivo()" checked>
					  Archivo:</td>
					<td width="58%"><input name="fil_archivo" type="file"></td>
				</tr>
				<tr class="trInformacion">
					<td width="24%"></td>
					<td><input name="rad_herramienta" type="radio" value="2" onClick="javascript:activarHerramientaURL()" >
					URL:</td>
					<td><input name="txt_url" type="text" disabled value="http://">
						<input type="hidden" name="hid_url" value="1"></td>
				</tr>
<?
	}
	else
	{
?>		
                <tr class="trInformacion">
					<td><b>Agregar Herramienta:</b></td>
					<td><input name="rad_herramienta" type="radio" value="2" onClick="javascript:activarHerramientaURL()" checked>
					URL:</td>
					<td><input name="txt_url" type="text" value="http://">
						<input type="hidden" name="hid_url" value="1"></td>
				</tr>
<?
	}
?>	
				<tr class="trInformacion">
					<td><b>Icono para mostrar:</b></td>
					<td colspan="2"><input name="fil_imagen" type="file"></td>
				</tr>
				<tr class="trInformacion">
					<td><b>Descripci&oacute;n:</b></td>
					<td colspan="2"><input type="text" name="txt_descripcionHerramienta" size="80" maxlength="200" class="link" value="<?=$_GET['descripcion']?>"></td>
				</tr>
				<tr class="trInformacion">
					<td colspan="3" valign="top">
						<table class="tablaPrincipal" width="100%" bgcolor="#DDDDDD">					
                        	<tr>
								<td colspan="6"><b>Destino:</b></td>
							</tr>
							<tr>
								<td width="30">&nbsp;</td>
								<td height="40" width="21">
                                	<input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar()" id="radioT" checked></td>
								<td width="115">Todos mis Grupos </td>
								<td width="20"><input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()" id="radioS"></td>
								<td colspan="2" width="364">Grupos Seleccionados</td>
							</tr>
<?
		$contChk=0;
		if ($_SESSION['idtipousuario']==5)
		{
			$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
				IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
					IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].")) 
						GROUP BY (mei_virmateria.idvirmateria)"; 
		}
		else if ($_SESSION['idtipousuario']==2)
		{
			$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
				IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
					IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].")) 
						GROUP BY (mei_materia.idmateria)"; 
		}
		$materias=$baseDatos->ConsultarBD($sql);
		while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
		{
?>
							<tr>
								<td colspan="4">&nbsp;</td>
								<td><b><?= $materiaNombre?></b></td>
							</tr>
<?
			if ($_SESSION['idtipousuario']==5)
			{
				$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
					IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].") 
						AND mei_virgrupo.idvirmateria =".$materiaCodigo;
			}
			else if ($_SESSION['idtipousuario']==2)
			{
				$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
					IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].") 
						AND mei_grupo.idmateria =".$materiaCodigo;
			}
			$grupos=$baseDatos->ConsultarBD($sql);
			while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
			{
?>
							<tr>
							  <td colspan="4">&nbsp;</td>
								<td width="364">&nbsp;&nbsp;&nbsp;
                                	<input id="grupo" name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>"><?= $grupoNombre?></td>
							</tr>
<?
				$contChk++;
			}
		}  
?>
							<input type="hidden" name="hid_contGrupo" value="<?=$contChk?>">
						</table>
					</td>
				</tr>
				<tr class="trInformacion">
					<td colspan="3"><div align="center"><input type="button" name="btn_cargarHerramienta" value="Aceptar" onClick="javascript:validar()">
						<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div>
                     </td>
				</tr>
			</table></form></td>
		<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
	</tr>
</table>
</body>
</html>
<? 
	}
	else
	{
		redireccionar('../scripts/');					
	}
}
?>