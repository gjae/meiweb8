<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/vistas.lib.php');
	include_once ('../calendario/FrmCalendario.class.php');
	include_once ('../editor/fckeditor.php');


	$baseDatos=new BD();
	$editor=new FCKeditor('edt_herramientas' , '100%' , '200' , 'barraEstandar' , '' );
	
if(comprobarSession())
{	
	$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=17";
	$resultado=$baseDatos->ConsultarBD($sql);
	list($estado)=mysql_fetch_array($resultado);
	if($estado==1)
	{
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<style type="text/css">
			<!--
				.Estilo1 {color: #FF0000}
			-->
		</style>
</head>
<body>
	<style type="text/css">
		<!--
			.Estilo1 {color: #FF0000}
		-->
	</style>
		
<script> 
	function enviarCancelar()
	{
		location.replace("../scripts/indexAdministrador.php");
	}
		
	function validar()
	{
		if(document.frm_cargarHerramienta.hid_url.value==1)
		{
			if(document.frm_cargarHerramienta.txt_nombreHerramienta.value!=false && document.frm_cargarHerramienta.fil_archivo.value!=false)
			{
				document.frm_cargarHerramienta.submit();
			}
			else
			{
				alert ("Debe llenar completamente la información solicitada");
			}
		}
		else if(document.frm_cargarHerramienta.hid_url.value==2)
		{
			if(document.frm_cargarHerramienta.txt_nombreHerramienta.value!=false && (document.frm_cargarHerramienta.txt_url.value!=false && document.frm_cargarHerramienta.txt_url.value!="http://"))
			{
				document.frm_cargarHerramienta.submit();
			}
			else
			{
				alert ("Debe llenar completamente la información solicitada");
			}
		}
	}

	function activarHerramientaURL()
	{
		document.frm_cargarHerramienta.fil_archivo.disabled=true;
		document.frm_cargarHerramienta.txt_url.disabled=false;
		document.frm_cargarHerramienta.hid_url.value=2;
	}
	
	function activarHerramientaArchivo()
	{
		document.frm_cargarHerramienta.fil_archivo.disabled=false;
		document.frm_cargarHerramienta.txt_url.disabled=true;
		document.frm_cargarHerramienta.txt_url.value='http://';
		document.frm_cargarHerramienta.hid_url.value=1;
	}
	
	function chekear()
	{
		var todo=document.getElementById('todo');
		if (todo.checked==true)
		{
			for(i=0;i<document.frm_herramientas.elements.length;i++)
			{
				if(document.frm_herramientas.elements[i].id=="codigoHerramienta")
					document.frm_herramientas.elements[i].checked=true;
			}
		}
		else
		{
			for(i=0;i<document.frm_herramientas.elements.length;i++)
			{
				if(document.frm_herramientas.elements[i].id=="codigoHerramienta")
					document.frm_herramientas.elements[i].checked=false;
			}
		}
	}
	
	function eliminar() //varias
	{
		if(confirm("¿Está seguro de eliminar las Herramientas seleccionadas?"))
		{
			document.frm_herramientas.action="eliminarHerramientaAdmin.php";
			document.frm_herramientas.submit();					
		}
	}

	function eliminarHerramienta(codigoHerramienta,herramienta) //solo 1
	{
		if(confirm("¿Está seguro de eliminar esta Herramienta?"))
		{
			location.replace("eliminarHerramientaAdmin.php?codigoHerramienta="+codigoHerramienta+"&herramienta="+herramienta);					
		}
	}
	
</script> 

<table class="tablaPrincipal">
	<tr valign="top">
    	<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaIzquierdo"><?=menu(1);?></td>
        <td class="tablaEspacio">&nbsp;</td>
		<td><form  enctype="multipart/form-data" name="frm_cargarHerramienta" method="post" action="guardarHerramientaAdmin.php">
			<table class="tablaGeneral" > <!-- Agregar-->
				<tr class="trTitulo" >
					<td colspan="4"><img src="imagenes/herramientas.gif" width="16" height="16" align="texttop"> Agregar Herramienta</td>
				</tr>
<?
		if($_GET['error'] == '0x001')
		{
?>
				<tr class="trAviso">
					<td colspan="8"><img src="imagenes/error.gif" width="17" height="18" align="texttop">
						<span class="Estilo1"> Error: No se pudo cargar la herramienta, excede el tama&ntilde;o permitido por el servidor.</span></td>
				</tr>
<?
		}
		if($_GET['error'] == '0x002')
		{
?>
				<tr class="trAviso">
					<td colspan="8"><img src="imagenes/error.gif" width="17" height="18" align="texttop">
						<span class="Estilo1"> Error: No se pudo cargar la herramienta, el icono excede el tamaño permitido por el servidor.</span></td>
				</tr>
<?
		}
?>
				<tr class="trInformacion">
					<td><b>Nombre Herramienta:</b></td>
					<td colspan="2"><input type="text" name="txt_nombreHerramienta" size="20" maxlength="100" value="<?=$_GET['nombre']?>"></td>
				</tr>
				<tr class="trInformacion">
					<td><b>Agregar Herramienta:</b></td>
					<td width="18%"><input name="rad_herramienta" type="radio" value="1" onClick="javascript:activarHerramientaArchivo()" checked>
					  Archivo:</td>
					<td width="58%"><input name="fil_archivo" type="file"></td>
				</tr>
				<tr class="trInformacion">
					<td width="24%"></td>
					<td><input name="rad_herramienta" type="radio" value="2" onClick="javascript:activarHerramientaURL()">
					URL:</td>
					<td><input name="txt_url" type="text" disabled value="http://">
						<input type="hidden" name="hid_url" value="1"></td>
				</tr>
				<tr class="trInformacion">
					<td><b>Icono para mostrar:</b></td>
					<td colspan="2"><input name="fil_archivo2" type="file"></td>
				</tr>
				<tr class="trInformacion">
					<td><b>Descripci&oacute;n:</b></td>
					<td colspan="2">
<?
	$editor->Value="Debe dar una breve descripción de la herramienta";
	$editor->crearEditor();
?>
				</tr>
				<tr class="trInformacion">
					<td colspan="3"><div align="center"><input type="button" name="btn_cargarHerramienta" value="Aceptar" onClick="javascript:validar()">
						<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div>
					</td>
				</tr>
                
			</table>
		</form>
         <br>
         <br>
			<?
				$sql="SELECT mei_herramientas.idherramienta FROM mei_herramientas";
				$resultado=$baseDatos->ConsultarBD($sql);
				list($codigoHerramienta)=mysql_fetch_array($resultado);
				if(!empty($codigoHerramienta))
				{
					registrarBitacora(10,30,false);
					$sql="SELECT * FROM mei_herramientas ORDER BY ".$_GET['ordenar'];
					$resultado=$baseDatos->ConsultarBD($sql);
					
			?>
			<form name="frm_herramientas" method="post">
			<table class="tablaGeneral" ><!-- Ver -->
				<tr class="trTitulo" >
					<td colspan="10"><img src="imagenes/herramientas.gif" width="16" height="16" align="texttop"> Herramientas</td>
				</tr>
				<tr class="trSubTitulo">
					<td width="8%" class="trSubTitulo"><div align="right">
						<input id="todo" type="checkbox" name="chk_todaHerramienta" onClick="javascript:chekear()" ></div></td>
					<td width="7%" class="trSubTitulo" align="center">Icono</td>
					<td width="13%" class="trSubTitulo" align="center"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.nombre ASC">Nombre</a></td>
					<td width="15%" class="trSubTitulo" align="center"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.idusuario">Autor</a></td>
					<td width="8%" class="trSubTitulo" align="center"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.herramienta DESC">Herramienta</a></td>
					<td width="32%" class="trSubTitulo" align="center"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.descripcion">Descripci&oacute;n</a></td>
					<td width="8%" class="trSubTitulo" align="center"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.tamano DESC">Tama&ntilde;o</a></td>
					<td width="9%" class="trSubTitulo" align="center">Editar</td>
				</tr>
<?
					$cont=0;
					while (list($codigoHerramienta,$autor,$nombre,$descripcion,$herramienta,$ubicacion,$destino,$imagen,$admin,$tamano)=mysql_fetch_array($resultado))
					{ 
						$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario='".$autor."'";
						$autor=$baseDatos->ConsultarBD($sql);
						list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
						if($cont%2==0)
							$color="trListaClaro";
						else
							$color="trListaOscuro";
						$tituloAlt="Archivo: ".$herramienta."\n";
						$tituloAlt.="Autor: ".$nombre1." ".$nombre2." ".$apellido1." ".$apellido2;
						$autor=$nombre1." ".$apellido1;
						$nombreImagen='../../datosMEIWEB/archivosHerramientas/'.$imagen;
						if(strlen($herramienta) > 30)
						{
							$herramienta=substr($herramienta,0,30)."...";
						}
?>
				<tr class="<?=$color?>">
					<td class="<?=$color?>"><div align="right"><?=$cont+1?>
						<input id="codigoHerramienta" type="checkbox" name="<?= "chk_herramienta".$cont?>" value="<?=$codigoHerramienta?>"></div></td>
<?
							if (empty ($imagen)) //si esta vacio
							{
?>
					<td class="<?=$color?>" valign="middle"><div align="center"><img src="imagenes/herramientas.gif" width="20" height="20"></div></td>
<?
							}
							else
							{
?>
					<td class="<?=$color?>" valign="middle"><div align="center"><img src="<?php print $nombreImagen?>" width="20" height="20"></div></td>
<?
							}
?>   
					<td class="<?=$color?>" title="<?= $tituloAlt?>"><?=$nombre?></td>
					<td class="<?=$color?>" title="<?= $tituloAlt?>"><?=$autor?></td>
					<td class="<?=$color?>" title="<?= $tituloAlt?>"><a href="<?=$ubicacion?>" class="link" target="_blank"><?=$herramienta?></a></td>
					<td class="<?=$color?>" title="<?= $tituloAlt?>"><?=$descripcion?></td>
					<td class="<?=$color?>" align="center"><?=$tamano/1000?>&nbsp;KB</td>
					<td class="<?=$color?>">
						<table class="tablaPrincipal" align="center">
							<tr>
								<td><div align="center"><a href="modificarHerramientaAdmin.php?codigoHerramienta=<?=$codigoHerramienta?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Herramienta" width="16" height="16" border="0"></a></div></td>
								<td>&nbsp;</td>
								<td><div align="center"><a href="javascript: eliminarHerramienta('<?= $codigoHerramienta?>','<?= $herramienta?>')"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Herramienta" width="16" height="16" border="0"></a></div></td>
							</tr>
						</table>		
					</td>
				</tr>
<?
							$cont++;
						}//FIN WHILE
?>
						<tr class="trInformacion">
							<td colspan="10"><div align="center">
								<input type="button" name="btn_eliminar" value="Eliminar Seleccionados" onClick="javascript:eliminar()"></div>
							</td>
						</tr>
			</table>
		<br>
				<?
				}
				else
				{
				?>
					<table class="tablaGeneral" >
						<tr class="trTitulo" >
							<td><img src="imagenes/herramientas.gif" width="16" height="16" align="texttop">Herramientas</td>
						</tr>
						<tr class="trAviso">
							<td class="trAviso">Actualmente no Existen Herramientas</td>
						</tr>
					</table>
				<?
				} 
				?>
					<input name="hid_contHerramientas" type="hidden" value="<?=$cont?>"> 
				  </form>        
		</td>
        <td class="tablaEspacio">&nbsp;</td>
        <td class="tablaDerecho">&nbsp;</td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
	</tr>
</table>
</body>
</html>
<? 
	}
	else
	{
		redireccionar('../scripts/');
	}
}
?>