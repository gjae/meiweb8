<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/vistas.lib.php');
	include_once ('../calendario/FrmCalendario.class.php');

	$baseDatos=new BD();
if(comprobarSession())
{
	if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==2)
	{			
		if($_POST['rad_herramienta']==1)
		{
			if($_FILES['fil_archivo'])
			{
				if (!is_uploaded_file($_FILES['fil_archivo']['tmp_name']))
					redireccionar("../scripts/homeMateria.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
				else
				{
					$tamanoHerramienta= $_FILES['fil_archivo']['size']; 
					if ($tamanoHerramienta <= recuperarVariableSistema("sistematamanoherramienta"))
					{
						if($_POST['rad_grupo']=="todos")
							$destino=0;			
						else if($_POST['rad_grupo']=="seleccionados")
							$destino=1;
						$nombre=$_POST['txt_nombreHerramienta'];
						$descripcionHerramienta=$_POST['txt_descripcionHerramienta'];
						$herramienta= $_FILES['fil_archivo']['name']; 
						
						$sql="SELECT MAX(mei_herramientas.idherramienta) FROM mei_herramientas";
						$consulta=$baseDatos->ConsultarBD($sql);
						list($maxId)=mysql_fetch_array($consulta);
						if(empty($maxId))
							$maxId=1;
						else
							$maxId=$maxId+1; 
						
						$nombreHerramienta="id".$maxId.$herramienta;
						if (cargarArchivo("fil_archivo",corregirCaracteresURL($nombreHerramienta),"../../datosMEIWEB/archivosHerramientas/"))
						{
							registrarBitacora(17,31,false); 
							if(!is_uploaded_file($_FILES['fil_imagen']['tmp_name']))
							{
								$sql="INSERT INTO mei_herramientas (idherramienta,idusuario,nombre,descripcion,herramienta,ubicacion,destino,imagen,tamano) 
									VALUES('','".$_SESSION['idusuario']."','$nombre','$descripcionHerramienta','".corregirCaracteresURL($nombreHerramienta)."','../../datosMEIWEB/archivosHerramientas/".corregirCaracteresURL($nombreHerramienta)."','$destino','','".$tamanoHerramienta."')";
								$resultado=$baseDatos->ConsultarBD($sql);
								$idherramienta = $baseDatos->InsertIdBD();
							}
							else 
							{
								$tamanoImagen=$_FILES['fil_imagen']['size'];
								if ($tamanoImagen <= recuperarVariableSistema("sistematamanoicono"))
								{
									$imagen=$_FILES['fil_imagen']['name'];									
									$nombreImagen="id".$maxId.$imagen;
									if (cargarArchivo("fil_imagen",corregirCaracteresURL($nombreImagen),"../../datosMEIWEB/archivosHerramientas/"))
									{
										$sql="INSERT INTO mei_herramientas (idherramienta,idusuario,nombre,descripcion,herramienta,ubicacion,destino,imagen,tamano) 
											VALUES('','".$_SESSION['idusuario']."','$nombre','$descripcionHerramienta','".corregirCaracteresURL($nombreHerramienta)."','../../datosMEIWEB/archivosHerramientas/".corregirCaracteresURL($nombreHerramienta)."','$destino','".corregirCaracteresURL($nombreImagen)."','".$tamanoHerramienta."')";
										$resultado=$baseDatos->ConsultarBD($sql);
										$idherramienta = $baseDatos->InsertIdBD();
									}
								}
								else
								{
									redireccionar("../herramientas/agregarHerramienta.php?error=0x002&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['txt_descripcionHerramienta']."&idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
								}
							}
							if ($destino==0)
							{
								if ($_SESSION['idtipousuario']==5)
								{
									$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
									IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
									IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")) 
									GROUP BY (mei_virmateria.idvirmateria)"; 
								}
								else if ($_SESSION['idtipousuario']==2)
								{
									$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
									IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
									IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")) 
									GROUP BY (mei_materia.idmateria)"; 
								}
								$materias=$baseDatos->ConsultarBD($sql);
								while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
								{
									if ($_SESSION['idtipousuario']==5)
									{
										$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
										IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].") 
										AND mei_virgrupo.idvirmateria =".$materiaCodigo;
									}
									else if ($_SESSION['idtipousuario']==2)
									{
										$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
										IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].") 
										AND mei_grupo.idmateria =".$materiaCodigo;
									}
									$grupos=$baseDatos->ConsultarBD($sql);
									while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
									{
										if ($_SESSION['idtipousuario']==5)
										{
											$sql="INSERT INTO mei_relhervirgru (idherramienta,idvirgrupo) VALUES ('".$idherramienta."','".$grupoCodigo."')";
											$respuesta=$baseDatos->ConsultarBD($sql);
										}
										else if ($_SESSION['idtipousuario']==2)
										{
											$sql="INSERT INTO mei_relhergru (idherramienta,idgrupo) VALUES ('".$idherramienta."','".$grupoCodigo."')";
											$respuesta=$baseDatos->ConsultarBD($sql);
										}
									}
								}
							}
							else if($destino==1)
							{
								$sql="SELECT MAX(mei_herramientas.idherramienta) FROM mei_herramientas";
								$consulta=$baseDatos->ConsultarBD($sql);
								list($maxId)=mysql_fetch_array($consulta);
								for($i=0;$i<$_POST['hid_contGrupo'];$i++) //agrega un registro en la tabla mei_relhergru (relacion grupo con herramienta)
								{ 
									if(!empty($_POST['chk_idgrupo'.$i]))
									{ 
										if ($_SESSION['idtipousuario']==5)									
											$sql="INSERT INTO mei_relhervirgru (idherramienta,idvirgrupo) VALUES ('".$idherramienta."','".$_POST['chk_idgrupo'.$i]."')";
										else if ($_SESSION['idtipousuario']==2)											
											$sql="INSERT INTO mei_relhergru (idherramienta,idgrupo) VALUES ('".$idherramienta."','".$_POST['chk_idgrupo'.$i]."')";											
										$baseDatos->ConsultarBD($sql);
									}					
								} 
							}
						}
						else
						{
							redireccionar("../scripts/homeMateria.php?error=0x001&idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
						}
					}
					else
					{
						redireccionar("../herramientas/agregarHerramienta.php?error=0x001&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['txt_descripcionHerramienta']."&idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
					}
				}
			}
		}
		if($_POST['rad_herramienta']==2)
		{
			if($_POST['rad_grupo']=="todos")
				$destino=0;
			else if($_POST['rad_grupo']=="seleccionados")
				$destino=1;
			$nombre=$_POST['txt_nombreHerramienta'];
			$descripcionHerramienta=$_POST['txt_descripcionHerramienta'];
			$herramienta = $_POST['txt_url'];
			$sql="SELECT MAX(mei_herramientas.idherramienta) FROM mei_herramientas";
			$resultado=$baseDatos->ConsultarBD($sql);
			list($maxId)=mysql_fetch_array($resultado);	
			if(empty($maxId))
				$maxId=1;
			else
				$maxId=$maxId+1;
			
			if (!is_uploaded_file($_FILES['fil_imagen']['tmp_name']))
			{
				$sql="INSERT INTO mei_herramientas (idherramienta,idusuario,nombre,descripcion,herramienta,ubicacion,destino,imagen) 
					VALUES('','".$_SESSION['idusuario']."','$nombre','$descripcionHerramienta','$herramienta','$herramienta','$destino','')";
				$resultado=$baseDatos->ConsultarBD($sql);
				$idherramienta = $baseDatos->InsertIdBD();
			}
			else
			{
				$tamanoImagen=$_FILES['fil_imagen']['size'];
				if ($tamanoImagen <= recuperarVariableSistema("sistematamanoicono"))
				{
					$imagen=$_FILES['fil_imagen']['name'];
					$nombreImagen="id".$maxId.$imagen;
					if (cargarArchivo("fil_imagen",corregirCaracteresURL($nombreImagen),"../../datosMEIWEB/archivosHerramientas/"))
					{
						$sql="INSERT INTO mei_herramientas (idherramienta,idusuario,nombre,descripcion,herramienta,ubicacion,destino,imagen) 
						VALUES('','".$_SESSION['idusuario']."','$nombre','$descripcionHerramienta','$herramienta','$herramienta','$destino','".corregirCaracteresURL($nombreImagen)."')";
						$resultado=$baseDatos->ConsultarBD($sql);
						$idherramienta = $baseDatos->InsertIdBD();
					}
				}
				else
				{
					redireccionar("../herramientas/agregarHerramienta.php?error=0x002&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['txt_descripcionHerramienta']."&idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
				}
			}
			
							if ($destino==0)
							{
								if ($_SESSION['idtipousuario']==5)
								{
									$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
									IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
									IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")) 
									GROUP BY (mei_virmateria.idvirmateria)"; 
								}
								else if ($_SESSION['idtipousuario']==2)
								{
									$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia WHERE mei_materia.idmateria
									IN (SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo
									IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")) 
									GROUP BY (mei_materia.idmateria)"; 
								}
								$materias=$baseDatos->ConsultarBD($sql);
								while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
								{
									if ($_SESSION['idtipousuario']==5)
									{
										$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
										IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].") 
										AND mei_virgrupo.idvirmateria =".$materiaCodigo;
									}
									else if ($_SESSION['idtipousuario']==2)
									{
										$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
										IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].") 
										AND mei_grupo.idmateria =".$materiaCodigo;
									}
									$grupos=$baseDatos->ConsultarBD($sql);
									while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
									{
										if ($_SESSION['idtipousuario']==5)
											$sql="INSERT INTO mei_relhervirgru (idherramienta,idvirgrupo) VALUES ('".$idherramienta."','".$grupoCodigo."')";
										else if ($_SESSION['idtipousuario']==2)
											$sql="INSERT INTO mei_relhergru (idherramienta,idgrupo) VALUES ('".$idherramienta."','".$grupoCodigo."')";
										$baseDatos->ConsultarBD($sql);
									}
								}
							}
							else if($destino==1)
							{
								$sql="SELECT MAX(mei_herramientas.idherramienta) FROM mei_herramientas";
								$consulta=$baseDatos->ConsultarBD($sql);
								list($maxId)=mysql_fetch_array($consulta);
								for($i=0;$i<$_POST['hid_contGrupo'];$i++) //agrega un registro en la tabla mei_relhergru (relacion grupo con herramienta)
								{ 
									if(!empty($_POST['chk_idgrupo'.$i]))
									{ 
										if ($_SESSION['idtipousuario']==5)									
											$sql="INSERT INTO mei_relhervirgru (idherramienta,idvirgrupo) VALUES ('".$idherramienta."','".$_POST['chk_idgrupo'.$i]."')";
										else if ($_SESSION['idtipousuario']==2)											
											$sql="INSERT INTO mei_relhergru (idherramienta,idgrupo) VALUES ('".$idherramienta."','".$_POST['chk_idgrupo'.$i]."')";											
										$baseDatos->ConsultarBD($sql);
									}					
								} 
							}
		}
		redireccionar("../herramientas/verHerramienta.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
	}
	else
	{
		redireccionar('../login/');
	}
}
else
{
	redireccionar('../login/');
}
?>