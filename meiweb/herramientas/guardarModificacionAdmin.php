<?
	include_once ('../librerias/validar.lib.php');
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	
if(comprobarSession())
{ 
	if ($_SESSION['banderaAdmnistrador']==1)
	{
		if($_POST['rad_herramienta']==1)
		{
			if(is_uploaded_file($_FILES['fil_archivo']['tmp_name']))
			{
					$sql="SELECT mei_herramientas.ubicacion,mei_herramientas.imagen FROM mei_herramientas WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
					$consultaArchivo=$baseDatos->ConsultarBD($sql);
					list($archivo,$imagen)=mysql_fetch_array($consultaArchivo);
					if(file_exists($archivo))
					{
						unlink($archivo);
					}
					$nombreFichero="id".$_GET['codigoHerramienta'].$_FILES['fil_archivo']['name']; 					
					$tamanoHerramienta= $_FILES['fil_archivo']['size']; 					
					if ($tamanoHerramienta <= recuperarVariableSistema("sistematamanoherramienta"))
					{
						if (cargarArchivo("fil_archivo",corregirCaracteresURL($nombreFichero),"../../datosMEIWEB/archivosHerramientas/"))
						{  
								$sql="UPDATE mei_herramientas SET mei_herramientas.ubicacion='../../datosMEIWEB/archivosHerramientas/".corregirCaracteresURL($nombreFichero)."' , mei_herramientas.herramienta='".corregirCaracteresURL($nombreFichero)."', mei_herramientas.tamano='$tamanoHerramienta' WHERE  mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
								$baseDatos->ConsultarBD($sql);
						}
					}
					else
					{
						redireccionar("../herramientas/modificarHerramientaAdmin.php?error=0x001&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['edt_herramientas']."&codigoHerramienta=".$_GET['codigoHerramienta']);
					}
			}
		}
		if($_POST['rad_herramienta']==2)
		{
			$sql="SELECT mei_herramientas.imagen,mei_herramientas.ubicacion FROM mei_herramientas WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
			$resultado=$baseDatos->ConsultarBD($sql);
			list($imagen,$archivo)=mysql_fetch_array($resultado);
			if(file_exists($archivo))
			{
				unlink($archivo);
			}
			$sql="UPDATE mei_herramientas SET mei_herramientas.ubicacion='".$_POST['txt_url']."' , mei_herramientas.herramienta='".$_POST['txt_url']."', mei_herramientas.tamano='' WHERE  mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
			$baseDatos->ConsultarBD($sql);
		}
		
		if ($_FILES['fil_archivo2'] && is_uploaded_file($_FILES['fil_archivo2']['tmp_name']))
		{
			$sql="SELECT mei_herramientas.imagen FROM mei_herramientas WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
			$resultado=$baseDatos->ConsultarBD($sql);
			list($imagen)=mysql_fetch_array($resultado);
			if(!empty($imagen))
			{
				if(file_exists("../../datosMEIWEB/archivosHerramientas/".$imagen))
				{
					unlink("../../datosMEIWEB/archivosHerramientas/".$imagen);
				}			
			}
			$nombreImagen="id".$_GET['codigoHerramienta'].$_FILES['fil_archivo2']['name']; 
			$tamanoImagen=$_FILES['fil_archivo2']['size'];
			if ($tamanoImagen <= recuperarVariableSistema("sistematamanoicono"))
			{
				if (cargarArchivo("fil_archivo2",corregirCaracteresURL($nombreImagen),"../../datosMEIWEB/archivosHerramientas/"))
				{	
					$sql="UPDATE mei_herramientas SET mei_herramientas.imagen='".corregirCaracteresURL($nombreImagen)."' WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
					$baseDatos->ConsultarBD($sql);
				}
			}
			else
			{
				redireccionar("../herramientas/modificarHerramientaAdmin.php?error=0x002&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['edt_herramientas']."&codigoHerramienta=".$_GET['codigoHerramienta']); 
			}
		}
		
		registrarBitacora(17,32,false);
		$sql="UPDATE mei_herramientas SET mei_herramientas.nombre='".$_POST['txt_nombreHerramienta']."',
			mei_herramientas.descripcion='".$_POST['edt_herramientas']."'
			WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
		$resultado=$baseDatos->ConsultarBD($sql);
		redireccionar("../herramientas/agregarHerramientaAdmin.php?ordenar=mei_herramientas.idherramienta");
	}
	else
	{
		redireccionar('../login/');
	}
}
else
		redireccionar('../login/');
?>