<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');

	$baseDatos=new BD();
	
if(comprobarSession())
{	
	$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=17";
	$resultado=$baseDatos->ConsultarBD($sql);
	list($estado)=mysql_fetch_array($resultado);
	if($estado==1)
	{
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<style type="text/css">
			<!--
				.Estilo1 {color: #FF0000}
			-->
		</style>
</head>
<body>
	<style type="text/css">
		<!--
			.Estilo1 {color: #FF0000}
		-->
	</style>
		
<script> 
	function chekear()
	{
		var todo=document.getElementById('todo');
		if (todo.checked==true)
		{
			for(i=0;i<document.frm_herramientas.elements.length;i++)
			{
				if(document.frm_herramientas.elements[i].id=="codigoHerramienta")
					document.frm_herramientas.elements[i].checked=true;
			}
		}
		else
		{
			for(i=0;i<document.frm_herramientas.elements.length;i++)
			{
				if(document.frm_herramientas.elements[i].id=="codigoHerramienta")
					document.frm_herramientas.elements[i].checked=false;
			}
		}
	}
	
	function eliminar() //varias
	{
		if(confirm("¿Está seguro de eliminar las Herramientas seleccionadas?"))
		{
			document.frm_herramientas.action="eliminarHerramientaAdmin.php";
			document.frm_herramientas.submit();					
		}
	}

	function eliminarHerramienta(codigoHerramienta,herramienta) //solo 1
	{
		if(confirm("¿Está seguro de eliminar esta Herramienta?"))
		{
			location.replace("eliminarHerramientaAdmin.php?codigoHerramienta="+codigoHerramienta+"&herramienta="+herramienta);					
		}
	}
	
</script> 
<table class="tablaPrincipal">
	<tr valign="top">
	    <td class="tablaEspacio">&nbsp;</td>
		<td class="tablaIzquierdo"><? menu(1); ?></td>
        <td class="tablaEspacio">&nbsp;</td>
		<td><?
				$sql="SELECT mei_herramientas.idherramienta FROM mei_herramientas WHERE mei_herramientas.herramienta <> mei_herramientas.ubicacion";
				$resultado=$baseDatos->ConsultarBD($sql);
				list($codigoHerramienta)=mysql_fetch_array($resultado);
				if(!empty($codigoHerramienta))
				{
					registrarBitacora(10,30,false);
					$sql="SELECT * FROM mei_herramientas WHERE mei_herramientas.herramienta <> mei_herramientas.ubicacion ORDER BY ".$_GET['ordenar'];
					$resultado=$baseDatos->ConsultarBD($sql);
					
			?>
			<form name="frm_herramientas" method="post">
			<table class="tablaGeneral" ><!-- Ver -->
				<tr class="trTitulo" >
					<td colspan="10"><img src="imagenes/herramientas.gif" width="16" height="16" align="texttop"> Herramientas</td>
				</tr>
				<tr class="trSubTitulo">
					<td width="7%" class="trSubTitulo"><div align="right">
						<input id="todo" type="checkbox" name="chk_todaHerramienta" onClick="javascript:chekear()" ></div></td>
				  	<td width="6%" class="trSubTitulo" align="center">Icono</td>
					<td width="17%" class="trSubTitulo"align="center"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.nombre">Nombre</a></td>
					<td width="18%"align="center" class="trSubTitulo"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.idusuario">Autor</a></td>
					<td width="12%" class="trSubTitulo"align="center"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.herramienta">Herramienta</a></td>
					<td width="33%" class="trSubTitulo"align="center"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.descripcion">Descripci&oacute;n</a></td>
					<td width="9%" class="trSubTitulo" align="center"><a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_herramientas.tamano DESC">Tama&ntilde;o</a></td>
					<td width="9%" class="trSubTitulo" align="center">Editar</td>
				</tr>
<?
						$cont=0;
						while (list($codigoHerramienta,$autor,$nombre,$descripcion,$herramienta,$ubicacion,$destino,$imagen,$admin,$tamano)=mysql_fetch_array($resultado))
						{
							$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario='".$autor."'";
							$autor=$baseDatos->ConsultarBD($sql);
							list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
							if($cont%2==0)
								$color="trListaClaro";
							else
								$color="trListaOscuro";
							$titulo="Archivo: ".$herramienta."\n";
							$autor=$nombre1." ".$apellido1;
							$nombreImagen='../../datosMEIWEB/archivosHerramientas/'.$imagen;
							if(strlen($herramienta) > 30)
							{
								$herramienta=substr($herramienta,0,30)."...";
							}
?>
				<tr class="<?=$color?>">
					<td width="7%" class="<?=$color?>"><div align="right"><?=$cont+1?>
						<input id="codigoHerramienta" type="checkbox" name="<?= "chk_herramienta".$cont?>" value="<?=$codigoHerramienta?>"></div></td>
<?
							if (empty ($imagen)) //si esta vacio
							{
?>
				  	<td width="6%" class="<?=$color?>" valign="middle"><div align="center"><img src="imagenes/herramientas.gif" width="20" height="20"></div></td>
<?
							}
							else
							{
?>
					<td width="6" class="<?=$color?>" valign="middle"><div align="center"><img src="<?php print $nombreImagen?>" width="20" height="20"></div></td>
<?
							}
?>   
					<td width="17%"class="<?=$color?>" title="<?= $titulo?>"><?=$nombre?></td>
				 	<td width="18%" class="<?=$color?>" title="<?= $titulo?>"><?=$autor?></td>
				 	<td width="12%" class="<?=$color?>" title="<?= $titulo?>"><?=$herramienta?></td>
					<td width="33%" class="<?=$color?>" title="<?= $titulo?>"><?=$descripcion?></td>
					<td width="9%" class="<?=$color?>" align="center"><?=$tamano/1000?>&nbsp;KB</td>
					<td width="9%" class="<?=$color?>">
						<table class="tablaPrincipal" align="center">
							<tr>
								<td><div align="center"><a href="modificarHerramientaAdmin.php?codigoHerramienta=<?=$codigoHerramienta?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Herramienta" width="16" height="16" border="0"></a></div></td>
								<td>&nbsp;</td>
								<td><div align="center"><a href="javascript: eliminarHerramienta('<?= $codigoHerramienta?>','<?= $herramienta?>')"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Herramienta" width="16" height="16" border="0"></a></div></td>
							</tr>
						</table>		
					</td>
				</tr>
<?
							$cont++;
						}//FIN WHILE
?>
						<tr class="trInformacion">
							<td colspan="10"><div align="center">
								<input type="button" name="btn_eliminar" value="Eliminar Seleccionados" onClick="javascript:eliminar()"></div>
							</td>
						</tr>
			</table>
<br>
				<?
				}
				else
				{
				?>
<table class="tablaGeneral" >
						<tr class="trTitulo" >
							<td><img src="imagenes/herramientas.gif" width="16" height="16" align="texttop">Herramientas</td>
						</tr>
						<tr class="trAviso">
							<td class="trAviso">Actualmente no Existen Herramientas</td>
						</tr>
					</table>
				<?
				} 
				?>
					<input name="hid_contHerramientas" type="hidden" value="<?=$cont?>"> 
		  </form>        
	  </td>
      <td class="tablaEspacio">&nbsp;</td>
      <td class="tablaDerecho">&nbsp;</td>
      <td class="tablaEspacioDerecho">&nbsp;</td>
	</tr>
</table>
</body>
</html>
<? 
	}
	else
	{
		redireccionar('../scripts/');
	}
}
?>