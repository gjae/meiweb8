<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/vistas.lib.php');
	include_once ('../calendario/FrmCalendario.class.php');
	include_once ('../editor/fckeditor.php');

	$baseDatos=new BD();
	$editor=new FCKeditor('edt_herramientas' , '100%' , '200' , 'barraEstandar' , '' );
	if(comprobarSession())
	{
		$codigoHerramienta=$_GET['codigoHerramienta'];
		$sql="SELECT mei_herramientas.idherramienta,mei_herramientas.nombre,mei_herramientas.descripcion, mei_herramientas.herramienta, mei_herramientas.destino, mei_herramientas.imagen FROM mei_herramientas WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
		$resultado=$baseDatos->ConsultarBD($sql);
		list($codigoHerramienta,$nombre,$descripcion,$herramienta,$desrino,$imagen)=mysql_fetch_array($resultado);

		if($destino==0)
		{
			$destinoTodos="checked";
			$gruposCheck="checked";
		}
		else if($destino==1)
		{
			$destinoSeleccionados="checked";
			$sql="SELECT mei_relhergru.idgrupo FROM mei_relhergru WHERE mei_relhergru.idherramienta =".$_GET['codigoHerramienta'];
			$consulta=$baseDatos->ConsultarBD($sql);
			$cont=0;
			while(list($datoGrupo)=mysql_fetch_array($consulta))
			{
				$listaGrupos[$cont++]=$datoGrupo;
			}					
		} 	
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		<!-- 
			.Estilo1 {color: #FF0000}
		-->
	</style>
</head>
<body>
<script> 
	function activarHerramientaArchivo()
	{
		document.frm_modificarHerramienta.fil_archivo.disabled=false;
		document.frm_modificarHerramienta.txt_url.disabled=true;
		document.frm_modificarHerramienta.txt_url.value='';
		document.frm_modificarHerramienta.hid_url.value=1;
	}

	function activarHerramientaURL()
	{
		document.frm_modificarHerramienta.fil_archivo.disabled=true;
		document.frm_modificarHerramienta.txt_url.disabled=false;
		document.frm_modificarHerramienta.hid_url.value=2;
	}
			
	function enviarCancelar()
	{
		location.replace("../herramientas/agregarHerramientaAdmin.php?ordenar=mei_herramientas.idherramienta");
	}
				
	function validar()
	{
		if(document.frm_modificarHerramienta.txt_nombreHerramienta.value!="" && document.frm_modificarHerramienta.edt_herramientas.value!="")
			document.frm_modificarHerramienta.submit();
		else
			alert ("Debe llenar completamente la información solicitada");
	}
		
	function chk_habilitar()
	{
		for(i=0;i<document.frm_cartelera.elements.length;i++)
		{
			if(document.frm_cartelera.elements[i].id=="grupo")
				document.frm_cartelera.elements[i].disabled=false;
		}	
	}
		
	function chk_deshabilitar()
	{
		for(i=0;i<document.frm_cartelera.elements.length;i++)
		{
			if(document.frm_cartelera.elements[i].id=="grupo")
				document.frm_cartelera.elements[i].checked=true;
		}
	}
	
	function chk_click ()
	{
		var radioTodos=document.getElementById('radioT');
		var radioSelec=document.getElementById('radioS');	
		radioTodos.checked=false;
		radioSelec.checked=true;	
	}

</script> 
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><? menu(1); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td>
				<table class="tablaGeneral">
					<form name="frm_modificarHerramienta" method="post" action="guardarModificacionAdmin.php?codigoHerramienta=<?=$codigoHerramienta?>" enctype="multipart/form-data">
					<tr>
						<td colspan="2" class="trTitulo"><img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar Herramienta </td>
					</tr>
<?
			if($_GET['error'] == "0x001")
			{
?>
					<tr class="trInformacion">
						<td colspan="2"><img src="imagenes/error.gif" width="17" height="18"> <SPAN class=Estilo1>Error: No se pudo cargar la herramienta, excede el tama&ntilde;o permitido por el servidor.</SPAN></td>
					</tr>
<?
			}
			if($_GET['error'] == "0x002")
			{
?>
					<tr class="trInformacion">
						<td colspan="2"><img src="imagenes/error.gif" width="17" height="18"> <SPAN class=Estilo1>Error: No se pudo cargar la herramienta, el icono excede el tama&ntilde;o permitido por el servidor. </SPAN></td>
					</tr>
<?
			}

?>
					<tr class="trInformacion">
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td width="25%"><strong>&nbsp;Nombre Herramienta:</strong></td>
						<td width="75%"><input name="txt_nombreHerramienta" type="text" value="<?=$nombre?>" size="80" maxlength="80"></td>
					</tr>
					<tr class="trInformacion">
						<td><strong>&nbsp;Descripci&oacute;n Herramienta:</strong></td>
						<td>
<?
					if (!empty($descripcion) ) 
						$editor->Value =$descripcion;
					else
						$editor->Value =' ';
					$editor->crearEditor();

?>
                        </td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td><b>&nbsp;Reemplazar Herramienta:</b></td>
						<td colspan="2"><input name="rad_herramienta" type="radio" value="1" onClick="javascript:activarHerramientaArchivo()" checked>
					    Nuevo Archivo:&nbsp;&nbsp;<input name="fil_archivo" type="file"></td>
					</tr>
					<tr class="trInformacion">
						<td></td>
						<td colspan="2"><input name="rad_herramienta" type="radio" value="2" onClick="javascript:activarHerramientaURL()">
					    Nueva URL:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input name="txt_url" type="text" disabled value="http://">
									<input type="hidden" name="hid_url" value="1"></td>
					</tr>
					<tr class="trInformacion">
						<td>&nbsp;</td>
				  		<td colspan="2">&nbsp;</td>					
					</tr>
					<tr class="trInformacion">
				  		<td><b>&nbsp;Cambiar Icono:</b></td>
						<td colspan="2"><input name="fil_archivo2" type="file"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;</td>
					</tr>
                    <tr class="trInformacion">
						<td colspan="2"><div align="center"><input type="button" name="btn_modificarHerramienta" value="Modificar" onClick="javascript:validar()">
							<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div>
                        </td>
					</tr>
					</form>
				</table>
	     </td>
         <td class="tablaEspacio">&nbsp;</td>
         <td class="tablaDerecho">&nbsp;</td>
         <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
?>
