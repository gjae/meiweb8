<?
	include_once ('../librerias/validar.lib.php');
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	
if(comprobarSession())
{ 
	if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==2)
	{
		if($_POST['rad_herramienta']==1)
		{
			if($_FILES['fil_archivo'] and is_uploaded_file($_FILES['fil_archivo']['tmp_name']) )
			{
					$sql="SELECT mei_herramientas.ubicacion,mei_herramientas.imagen FROM mei_herramientas WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
					$consultaArchivo=$baseDatos->ConsultarBD($sql);
					list($archivo,$imagen)=mysql_fetch_array($consultaArchivo);
					if(file_exists($archivo))
						unlink($archivo);
					
					$nombreFichero="id".$_GET['codigoHerramienta'].$_FILES['fil_archivo']['name']; 
					$tamanoHerramienta=$_FILES['fil_archivo']['size'];
					
					if ($tamanoHerramienta <= recuperarVariableSistema("sistematamanoherramienta"))
					{
						if (cargarArchivo("fil_archivo",corregirCaracteresURL($nombreFichero),"../../datosMEIWEB/archivosHerramientas/"))
						{  
							$sql="UPDATE mei_herramientas SET mei_herramientas.ubicacion='../../datosMEIWEB/archivosHerramientas/".corregirCaracteresURL($nombreFichero)."' , mei_herramientas.herramienta='".corregirCaracteresURL($nombreFichero)."',mei_herramientas.tamano='".$tamanoHerramienta."' WHERE  mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
							$baseDatos->ConsultarBD($sql);
						}
					}
					else
					{
						redireccionar("../herramientas/modificarHerramienta.php?error=0x001&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['txt_descripcionHerramienta']);
					}
			}

		}
		if($_POST['rad_herramienta']==2)
		{
			$sql="SELECT mei_herramientas.imagen,mei_herramientas.ubicacion FROM mei_herramientas WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
			$resultado=$baseDatos->ConsultarBD($sql);
			list($imagen,$archivo)=mysql_fetch_array($resultado);
			if(file_exists($archivo))
			{
				unlink($archivo);
			}
			$sql="UPDATE mei_herramientas SET mei_herramientas.ubicacion='".$_POST['txt_url']."',mei_herramientas.herramienta='".$_POST['txt_url']."', mei_herramientas.tamano='' WHERE  mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
			$baseDatos->ConsultarBD($sql);
		}
		
		if ($_FILES['fil_archivo2'] && is_uploaded_file($_FILES['fil_archivo2']['tmp_name']))
		{
			$sql="SELECT mei_herramientas.imagen FROM mei_herramientas WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
			$resultado=$baseDatos->ConsultarBD($sql);
			list($imagen)=mysql_fetch_array($resultado);
			
			if(file_exists("../../datosMEIWEB/archivosHerramientas/".$imagen))
			{
				unlink("../../datosMEIWEB/archivosHerramientas/".$imagen);
			}			
			$nombreImagen="id".$_GET['codigoHerramienta'].$_FILES['fil_archivo2']['name']; 
			$tamanoImagen=$_FILES['fil_archivo2']['size'];		
			if ($tamanoImagen <= recuperarVariableSistema("sistematamanoicono"))
			{
				if (cargarArchivo("fil_archivo2",corregirCaracteresURL($nombreImagen),"../../datosMEIWEB/archivosHerramientas/"))
				{
					$sql="UPDATE mei_herramientas SET mei_herramientas.imagen='".corregirCaracteresURL($nombreImagen)."' WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
					$baseDatos->ConsultarBD($sql);
				}
			}
			else
			{
				redireccionar("../herramientas/modificarHerramienta.php?error=0x002&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['txt_descripcionHerramienta']); 
			}
		}
		
		registrarBitacora(17,32,false);
			
		if($_POST['rad_grupo']=="todos")
		{
			$destino=0;
		}
		else if($_POST['rad_grupo']=="seleccionados")
		{
			$destino=1;
		}
		$sql="UPDATE mei_herramientas SET mei_herramientas.nombre='".$_POST['txt_nombreHerramienta']."',
			mei_herramientas.descripcion='".$_POST['txt_descripcionHerramienta']."' ,
			mei_herramientas.destino='".$destino."'
			WHERE mei_herramientas.idherramienta=".$_GET['codigoHerramienta'];
		$resultado=$baseDatos->ConsultarBD($sql);
		
		$sql="DELETE FROM mei_relhervirgru WHERE mei_relhervirgru.idherramienta = '".$_GET['codigoHerramienta']."'";
		$consulta=$baseDatos->ConsultarBD($sql);		
		if ($destino==0)
		{
			$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
					IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
					IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")) 
					GROUP BY (mei_virmateria.idvirmateria)"; 
			$materias=$baseDatos->ConsultarBD($sql);
			while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
			{
				$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
					IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].") 
					AND mei_virgrupo.idvirmateria =".$materiaCodigo;
				$grupos=$baseDatos->ConsultarBD($sql);
				while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
				{
					$sql="INSERT INTO mei_relhervirgru (idherramienta,idvirgrupo) VALUES ('".$_GET['codigoHerramienta']."','".$grupoCodigo."')";
					$baseDatos->ConsultarBD($sql);
				}
			}
		}
		else if($destino==1)
		{
			for($i=0;$i<$_POST['hid_contGrupo'];$i++) //agrega un registro en la tabla mei_relhergru (relacion grupo con herramienta)
			{ 
				if(!empty($_POST['chk_idgrupo'.$i]))
				{ 
					$sql="INSERT INTO mei_relhervirgru (idherramienta,idvirgrupo) VALUES ('".$_GET['codigoHerramienta']."','".$_POST['chk_idgrupo'.$i]."')";
					$baseDatos->ConsultarBD($sql);
				}					
			} 
		}
		redireccionar("../herramientas/verHerramienta.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
	}
	else
	{
		redireccionar('../login/');
	}
}
else
		redireccionar('../login/');
?>