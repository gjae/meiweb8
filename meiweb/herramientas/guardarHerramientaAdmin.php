<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/vistas.lib.php');
	include_once ('../calendario/FrmCalendario.class.php');

	$baseDatos=new BD();
if(comprobarSession())
{
	if($_SESSION['banderaAdmnistrador']==1)
	{
		if($_POST['rad_herramienta']==1)
		{
			if($_FILES['fil_archivo'])
			{
				if(is_uploaded_file($_FILES['fil_archivo']['tmp_name']))
				{
					$tamanoHerramienta= $_FILES['fil_archivo']['size']; 	
					
					if ($tamanoHerramienta <= recuperarVariableSistema("sistematamanoherramienta"))
					{	
						$destino=0;			
						$nombre=$_POST['txt_nombreHerramienta'];
						$descripcionHerramienta=$_POST['edt_herramientas'];
						$herramienta= $_FILES['fil_archivo']['name']; 
						$sql="SELECT MAX(mei_herramientas.idherramienta) FROM mei_herramientas";
						$consulta=$baseDatos->ConsultarBD($sql);
						list($maxId)=mysql_fetch_array($consulta);
				
						if(empty($maxId))
								$maxId=1;
						else
							$maxId=$maxId+1;
						$nombreHerramienta="id".$maxId.$herramienta;
						if (cargarArchivo('fil_archivo',corregirCaracteresURL($nombreHerramienta),'../../datosMEIWEB/archivosHerramientas/'))
						{
							registrarBitacora(17,31,false); 
							if(!is_uploaded_file($_FILES['fil_archivo2']['tmp_name']))
							{
								$sql="INSERT INTO mei_herramientas (idherramienta,idusuario,nombre,descripcion,herramienta,ubicacion,destino,imagen,admin,tamano) 
									VALUES('','".$_SESSION['idusuario']."','$nombre','$descripcionHerramienta','".corregirCaracteresURL($nombreHerramienta)."','../../datosMEIWEB/archivosHerramientas/".corregirCaracteresURL($nombreHerramienta)."','$destino','',1,'".$tamanoHerramienta."')";
								$resultado=$baseDatos->ConsultarBD($sql);
								$idherramienta = $baseDatos->InsertIdBD();
							}
							else 
							{
								$tamanoImagen=$_FILES['fil_archivo2']['size'];
								if ($tamanoImagen <= recuperarVariableSistema("sistematamanoicono"))
								{
									$imagen=$_FILES['fil_archivo2']['name'];								
									$nombreImagen="id".$maxId.$imagen;
									if (cargarArchivo("fil_archivo2",corregirCaracteresURL($nombreImagen),"../../datosMEIWEB/archivosHerramientas/"))
									{
										$sql="INSERT INTO mei_herramientas (idherramienta,idusuario,nombre,descripcion,herramienta,ubicacion,destino,imagen,admin,tamano) 
												VALUES('','".$_SESSION['idusuario']."','$nombre','$descripcionHerramienta','".corregirCaracteresURL($nombreHerramienta)."','../../datosMEIWEB/archivosHerramientas/".corregirCaracteresURL($nombreHerramienta)."','$destino','".corregirCaracteresURL($nombreImagen)."',1,'".$tamanoHerramienta."')";
										$resultado=$baseDatos->ConsultarBD($sql);
										$idherramienta = $baseDatos->InsertIdBD();
									}
								}
								else
								{
									redireccionar("../herramientas/agregarHerramientaAdmin.php?ordenar=mei_herramientas.idherramienta&error=0x002&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['edt_herramientas']); 
								}
							}
														
							$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
									IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
									IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru)) 
									GROUP BY (mei_virmateria.idvirmateria)"; 
							$materias=$baseDatos->ConsultarBD($sql);
							while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
							{
								$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
									IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru) 
									AND mei_virgrupo.idvirmateria =".$materiaCodigo;
								$grupos=$baseDatos->ConsultarBD($sql);
								while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
								{
									$sql="INSERT INTO mei_relhervirgru (idherramienta,idvirgrupo) VALUES ('".$idherramienta."','".$grupoCodigo."')";
									$baseDatos->ConsultarBD($sql);
								}
							}
						}
						else
						{
							redireccionar("../herramientas/agregarHerramientaAdmin.php?ordenar=mei_herramientas.idherramienta&error=0x001&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['edt_herramientas']); 
						}
					}
					else
					{
						redireccionar("../herramientas/agregarHerramientaAdmin.php?ordenar=mei_herramientas.idherramienta&error=0x001&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['edt_herramientas']); 
				  	}
				}
				else
				{
					redireccionar("../scripts/indexAdministrador.php?error=0x001"); 							
				}				
			}
		}
		if($_POST['rad_herramienta']==2)
		{
			$destino=0;			
			$nombre=$_POST['txt_nombreHerramienta'];
			$descripcionHerramienta=$_POST['edt_herramientas'];
			$herramienta = $_POST['txt_url'];
			if (is_uploaded_file($_FILES['fil_archivo2']['tmp_name']))
			{
				$sql="SELECT MAX(mei_herramientas.idherramienta) FROM mei_herramientas";
				$resultado=$baseDatos->ConsultarBD($sql);
				list($maxId)=mysql_fetch_array($resultado);
				if(empty($maxId)) 
					$maxId=0;
				else
					$maxId=$maxId+1;
				
				$imagen=$_FILES['fil_archivo2']['name'];
				$tamanoImagen=$_FILES['fil_archivo2']['size'];
				$nombreImagen="id".$maxId.$imagen;
				if ($tamanoImagen <= recuperarVariableSistema("sistematamanoicono"))
				{
					if (cargarArchivo("fil_archivo2",corregirCaracteresURL($nombreImagen),"../../datosMEIWEB/archivosHerramientas/"))
					{
						$sql="INSERT INTO mei_herramientas (idherramienta,idusuario,nombre,descripcion,herramienta,ubicacion,destino,imagen,admin) 
						VALUES('','".$_SESSION['idusuario']."','$nombre','$descripcionHerramienta','$herramienta','$herramienta','$destino','".corregirCaracteresURL($nombreImagen)."',1)";
						$resultado=$baseDatos->ConsultarBD($sql);
						$idherramienta = $baseDatos->InsertIdBD();
					}
				}
				else
				{
					redireccionar("../herramientas/agregarHerramientaAdmin.php?ordenar=mei_herramientas.idherramienta&error=0x002&nombre=".$_POST['txt_nombreHerramienta']."&herramienta=".$_FILES['fil_archivo']."&descripcion=".$_POST['edt_herramientas']); 				
				}
				
			}
			else
			{
				$sql="INSERT INTO mei_herramientas (idherramienta,idusuario,nombre,descripcion,herramienta,ubicacion,destino,imagen,admin) 
						VALUES('','".$_SESSION['idusuario']."','$nombre','$descripcionHerramienta','$herramienta','$herramienta','$destino','','1')";
				$resultado=$baseDatos->ConsultarBD($sql);
				$idherramienta = $baseDatos->InsertIdBD();
			}
			
			$sql="SELECT MAX(mei_herramientas.idherramienta) FROM mei_herramientas";
			$resultado=$baseDatos->ConsultarBD($sql);
			list($maxId)=mysql_fetch_array($resultado);
			
			$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria WHERE mei_virmateria.idvirmateria
					IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo
					IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru)) 
					GROUP BY (mei_virmateria.idvirmateria)"; 
			$materias=$baseDatos->ConsultarBD($sql);
			while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
			{
				$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
					IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru) 
					AND mei_virgrupo.idvirmateria =".$materiaCodigo;
				$grupos=$baseDatos->ConsultarBD($sql);
				while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
				{
					$sql="INSERT INTO mei_relhervirgru (idherramienta,idvirgrupo) VALUES ('".$idherramienta."','".$grupoCodigo."')";
					$baseDatos->ConsultarBD($sql);
				}
			}
		}
		redireccionar("../herramientas/agregarHerramientaAdmin.php?ordenar=mei_herramientas.idherramienta"); 
	}
	else
	{
		redireccionar('../login/');					
	} 
}
else
{
	redireccionar('../login/');					
}
?>