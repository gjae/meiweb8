<?
	session_start();
	
	include_once("InstaladorMEIWEB.class.php");	
	$instalador= new InstaladorMEIWEB();
	
		$instalador->ComprobarRecursosMEIWEB();		
		$instalador->RecursosSistemaMEIWEB();
		
		if(!$instalador->CrearDirectoriosDatos($_SESSION['pathInstalacion']))
		{
			$banderaError=false;
			$btnSiguiente="disabled";
		}
		else
		{
			$banderaError=true;
		}
		
		$instalador->InicioArchivosMEIWEB();		
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link rel="stylesheet" href="css/style.css">
<title>Instalación y Configuración de MEIWEB</title>
	<script language="javascript">

	function enviarInstalar()
	{
		document.frm_analizar.submit();
	}
	
	function enviarCancelar()
	{
		if(confirm("¿Está seguro que desea salir de la instalación de MeiWeb 7.0?"))
		{
			window.close();
		}		
	}
	
	function enviarAtras()
	{
		window.location.replace("index.php");
	}
	
</script>
		        <style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
                </style>
		</head>
<body>
<div class="login-page" style="padding: 2% 0 0;">
  <div class="form">	
  	<img src="../portal/imagenes/logo.png" style="width:180px; height: 40px;"/>
	<img src="../portal/imagenes/UIScom.png" style="width: 80px; height: 40px;"/>
	<br><br>	
<form action="conectarInstalacion.php" method="post" name="frm_analizar">
Instalación y Configuración de MeiWeb 7.0<img src="imagenes/transparente.gif" width="16" height="16"><br><br>
       <img src="imagenes/servidor.gif" width="22" height="22" align="texttop"> Análisis de los Recursos del Servidor 
          	<br><br>
          <b>Información sobre Directorios de Intalación </b><br><br>
          <img src="imagenes/vineta.gif" width="14" height="14" align="texttop"> Directorio del Aula:
            <?= $instalador->m_raiz;?><br><br>
          <!--<img src="imagenes/vineta.gif" width="14" height="14" align="texttop"> Directorio de la Instalacón:
            <?= $instalador->m_directorio;?><br><br>-->
         
          <b>Información Host </b>
          <br><br><img src="imagenes/vineta.gif" width="14" height="14" align="texttop"> Versión CGI: 
            <?= $instalador->m_versionCGI;?>
          <br><img src="imagenes/vineta.gif" width="14" height="14" align="texttop"> Puerto de Comunicación del Servidor: 
            <?= $instalador->m_puertoHTTP;?>
          <br><br><b>Comprobación de Recursos del Sistema </b><br><br>
          <table>
          <tr>
            <td width="33%"><b>Recurso Comprobado </b></td>
            <td width="33%"><b>Información</b></td>
            <td width="33%"><b>Estado de la comprobación </b></td>
          </tr>
          
          <tr class="trListaClaro">
            <td class="trListaClaro"><img src="imagenes/vineta.gif" width="14" height="14" align="texttop"> Versi&oacute;n de PHP : </td>
            <td class="trListaClaro"><?= $instalador->m_phpVersion;?></td>
			<?
				if($instalador->m_phpVersionBandera)
				{	
										
					?>
                        <td width="24%" class="trListaClaro"><table width="80" border="0" class="tablaPrincipal">
          				<tr>
                              <td width="16"><img src="imagenes/correcto.gif" width="16" height="16"></td>
                              <td width="54">Correcto</td>
                            </tr>
                          </table></td>
					<?
				}
				else
				{
					$btnSiguiente="disabled";
					?>
                            <td width="12" class="trListaClaro">
                              <table width="200" border="0" class="tablaPrincipal">
                                <tr>
                                  <td><img src="imagenes/error.gif" width="16" height="16"></td>
                                  <td>Error...</td>
                                </tr>
                              </table>
                            </td>
						<?
				}
					
			?>
          </tr>
          <tr class="trInformacion">
            <td class="trInformacion"><img src="imagenes/vineta.gif" width="14" height="14" align="texttop"> Versi&oacute;n del Servidor: </td>
            <td class="trInformacion"><?= $instalador->m_versionServidor;?></td>
			<?
				if($instalador->m_versionServidorBandera)
				{
					?>
                        <td class="trInformacion"><table width="80" border="0" class="tablaPrincipal">
                          <tr>
                            <td width="16"><img src="imagenes/correcto.gif" width="16" height="16"></td>
                            <td width="174">Correcto</td>
                          </tr>
                        </table></td>
					<?
				}
				else
				{
					?>
                        <td class="trInformacion"><table width="80" border="0" class="tablaPrincipal">
                            <tr>
                              <td><img src="imagenes/error.gif" width="16" height="16"></td>
                              <td>Error...</td>
                            </tr>
                          </table></td>
					<?
				}
					
			?>
          </tr>
          <tr class="trListaClaro">
            <td class="trListaClaro"><img src="imagenes/vineta.gif" width="14" height="14" align="texttop"> Protocolo:</td>
            <td class="trListaClaro"><?= $instalador->m_versionProtocolo;?></td>
			<?
				if($instalador->m_versionProtocoloBandera)
				{
					?>
                        <td class="trListaClaro"><table width="80" border="0" class="tablaPrincipal">
                          <tr>
                            <td><img src="imagenes/correcto.gif" width="16" height="16"></td>
                            <td>Correcto</td>
                          </tr>
                        </table></td>
					<?
				}
				else
				{
					?>
                            <td class="trListaClaro"><table width="80" border="0" class="tablaPrincipal">
                                <tr>
                                  <td><img src="imagenes/error.gif" width="16" height="16"></td>
                                  <td>Error...</td>
                                </tr>
                              </table></td>
						<?
				}
					
			?>
          </tr>
          <tr class="trInformacion">
            <td class="trInformacion"><img src="imagenes/vineta.gif" width="14" height="14" align="texttop"> Activaci&oacute;n de Sesiones : </td>
            <td class="trInformacion"><?
				if($instalador->m_activacionSession)
				{
				?>
    Activado
      <?
				}
				else
				{
				?>
    Imposible activar las Sesiones
    <?
				}
			?>
            </td>
            <?
				if($instalador->m_activacionSessionBanadera)
				{
					?>
            <td class="trInformacion" ><table width="80" border="0" class="tablaPrincipal">
                <tr>
                  <td ><img src="imagenes/correcto.gif" width="16" height="16"></td>
                  <td>Correcto</td>
                </tr>
            </table></td>
            <?
				}
				else
				{
					?>
            <td class="trInformacion"><table width="80" border="0" class="tablaPrincipal">
                <tr>
                  <td><img src="imagenes/error.gif" width="16" height="16"></td>
                  <td>Error...</td>
                </tr>
            </table>
              </td>
            <?
				}
					
			?>
          </tr>
          <tr class="trListaClaro">
            <td class="trListaClaro"><img src="imagenes/vineta.gif" width="14" height="14" align="texttop"> Creando Directorios: </td>
            <td class="trListaClaro">
			<?
				if($banderaError)
				{
				?>Creados<?
				}
				else
				{
				?> Imposible crear directorios, no hay permisos de escritura <?
				}
			?>
			</td>
			<?
				if($banderaError)
				{
					?>
                        <td class="trListaClaro"><table width="80" border="0" class="tablaPrincipal">
                          <tr>
                            <td><img src="imagenes/correcto.gif" width="16" height="16"></td>
                            <td>Correcto</td>
                          </tr>
                        </table>
                          </td>
<?
				}
				else
				{
					?>
                            <td class="trListaClaro"><table width="80" border="0" class="tablaPrincipal">
                                <tr>
                                  <td><img src="imagenes/error.gif" width="16" height="16"></td>
                                  <td>Error...</td>
                                </tr>
                              </table></td>
						<?
				}
					
			?>
			</tr>
        </table>
		
		<?
			if(!empty($instalador->m_erroInstalacion))
			{
		?>
		<table class="tablaCentral">
          <tr class="trAviso">
            <td class="trInformacion"><span class="Estilo1">No es posible continuar con la Instalación de MEIWEB.</span></td>
          </tr>
		  <?
		  	if(!$instalador->m_phpVersionBandera)
			{
		  ?>
          <tr class="trAviso">
            <td class="trInformacion"><span class="Estilo1"><img src="imagenes/error.gif" width="12" height="12" align="texttop"> Error: La versión de PHP no es correcta. Pida a su Administrador actualizar PHP a una versión mayor o igual a 5.0.1 </span></td>
          </tr>
		  <?
		  	}
			
			if(!$instalador->m_activacionSessionBanadera)
			{
		  ?>
          <tr class="trAviso">
            <td class="trInformacion"><span class="Estilo1"><img src="imagenes/error.gif" width="12" height="12" align="texttop"> Error: Imposible activar una Session en PHP. Revise el archivo de configuración php.ini y habilite el uso de sesiones. </span></td>
          </tr>
		  <?
		  	}
			
		  ?>
        </table>
		<?
			
		}
		?>
		
              <br><br>
              <input name="btn_siguiente" type="button" id="btn_siguiente" onClick="enviarInstalar()" value="Siguiente" <?= $btnSiguiente;?>>              
              <input name="btn_atras" type="button" id="btn_atras" value="Atras" onClick="javascript: enviarAtras();">
              <input type="button" name="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar()">
		      
	</td>
     </tr>
</table>
		
		</form>
		</div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>
	
</body>
		</html>
