<?php
	include_once ("../librerias/estandar.lib.php");
	include_once("../configuracion/InstaladorMEIWEB.class.php");
	
	if (!file_exists("archivos/install.conf"))
	{ 
	 session_start();
	 $_SESSION['pathInstalacion'];



	$instalador= new InstaladorMEIWEB();
	$_SESSION['pathInstalacion']=$instalador->DirectorioInstalacionMEIWEB();

	if(!$instalador->CrearSessionMEIWEB())
	{
		$archivoDirectorio="../configuracion/archivos/indicePermisos.dat";
		
		if(file_exists($archivoDirectorio))
		{
			$listaDirectorios=file($archivoDirectorio);	
		}
		else
		{
			$cadenaError="<b><i>Error, no se encontrado el directorio de permisos</i></b>";
		}
		
		$banderaError=true;
		$botonDesactivo="disabled";
	}
	

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link rel="stylesheet" href="css/style.css">
<title>Instalación y Configuración de MEIWEB</title>
<script language="javascript">

	function enviarInstalar()
	{
		document.frm_instalar.submit();
	}
	
	function enviarCancelar()
	{
		if(confirm("¿Está seguro que desea salir de la instalación de MeiWeb 7.0?"))
		{
			parent.close();
		}		
	}
	
</script>
<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
</style>
</head>

<body>
<div class="login-page" style="padding: 2% 0 0;">
  <div class="form">	
  	<img src="../portal/imagenes/logo.png" style="width:180px; height: 40px;"/>
	<img src="../portal/imagenes/UIScom.png" style="width: 80px; height: 40px;"/>
	<br><br>	
<form method="post" name="frm_instalar" action="analizarInstalacion.php">
Instalación y Configuración de MeiWeb 7.0<img src="imagenes/transparente.gif" width="16" height="16"><br>
<img src="imagenes/instalar.gif" width="22" height="22" align="texttop"> Información de la Instalación<br><br>
		<?
			  if($banderaError)
			  {
			  
			  ?>
			  
			   <img src="imagenes/error.gif" width="17" height="18" align="texttop"><br>
                Error: No tiene permisos de escritura. Comuniquese con el Administrador del servidor.
                <br>
                Para la instalación de MeiWeb 7.0 el Administrador debe dar permiso de escritura a los siguientes directorios: <?= $cadenaError?>
                  
                <?
							
					if(sizeof($listaDirectorios))
					{
						foreach($listaDirectorios as $directorio)
						{
			?>
                <img src="imagenes/flecha.gif" width="7" height="7">
                  <?= $_SESSION['pathInstalacion'].$directorio?>
                <br>
                <?
						}
					}
				
			  ?>
              
			 <?
			 }
			 ?> 
			
			  
			    <p>MeiWeb 7.0 ha sido desarrollado en La Escuela de Ingeniería de Sistemas de la Universidad Industrial de Santander bajo la dirección de Manuel Guillermo Florez Becerra.</p>
			    <p>El uso y distribución de este software se rige por las normas de derechos de autor estipuladas por la Universidad Industrial de Santander.    </p>
			    <p><b>Requerimientos Software para la instalación.</b></p>
			    <p> Servidor Apache 2.0 o superior.<br>
        Servidor de Bases de Datos MySQL 4.1.10 o superior.
        <br>PHP 4.3.0 o superior.</p>
			    <p><b>Requerimientos Hardware para la instalación. </b></p>
			    <p>15 MB de espacio en disco. </p></td>
			<br>
			  
			 	  <input type="button" name="btn_instalar" value="Instalar" onClick="enviarInstalar()" <?= $botonDesactivo?>>
			 	  <input type="button" name="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar()">
		       
</form>

</div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>
	
</body>
</html>
<?php
}
else
{
redireccionar('../login/');	
}
?>
