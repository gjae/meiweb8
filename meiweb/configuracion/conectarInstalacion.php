<?php
	session_start();
	
	//if(session_is_registered("hostBD"))	
	if(isset($_SESSION['hostBD']))
	{
		$hostBD=$_SESSION['hostBD'];
	}
	else
	{
		$hostBD="192.168.94.150";
	}

	
	include_once("InstaladorMEIWEB.class.php");	
	include_once("../librerias/estandar.lib.php");
	$instalador= new InstaladorMEIWEB();
	
	if($instalador->ComprobarSessionMEIWEB())
	{
	
	?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link rel="stylesheet" href="css/style.css">
<title>Instalación y Configuración de MEIWEB</title>
		<script language="javascript">

	function enviarInstalar()
	{
		if(validarDatos())
		{
			document.frm_conectarBD.btn_crearConexion.disabled = true;
			document.frm_conectarBD.submit();
		}
	}
	
	function enviarCancelar()
	{
		if(confirm("¿Está seguro que desea salir de la instalación de MeiWeb 7.0 ?"))
		{
			window.close();
		}		
	}
	
	function enviarAtras()
	{
		window.location.replace("analizarInstalacion.php");
	}
	
	function validarDatos()
	{
		if(document.frm_conectarBD.txt_hostBD.value ==false)
		{
			alert("Debe seleccionar la ubicación del servidor de base de datos MySQL");
			return false;
		}
		else if(document.frm_conectarBD.txt_usuario.value ==false)
		{
			alert("Debe escribir el nombre del usuario de MySQL habilitado para la instalación de MeiWeb 7.0");
			return false;
		}
		if(document.frm_conectarBD.txt_clave.value == false)
		{
			alert("Debe escribir una contraseña correcta");
			document.frm_conectarBD.txt_clave.value="";
			return false;
		}
		else if(document.frm_conectarBD.txt_reclave.value == false)
		{
			alert("Debe escribir una contraseña correcta");
			document.frm_conectarBD.txt_reclave.value="";
			return false;
		}
		else if(document.frm_conectarBD.txt_nombreBaseDatos.value == false)
		{
			alert("Debe escribir el nombre de la Base de Datos creada para la instalación de MeiWeb 7.0");
			return false;
		}
		else if(document.frm_conectarBD.txt_clave.value != document.frm_conectarBD.txt_reclave.value)
		{
			alert("Las contraseñas que ha escrito no coinciden. Vuelva a escribir las contraseñas.");
			document.frm_conectarBD.txt_clave.value="";
			document.frm_conectarBD.txt_reclave.value="";
			return false;
		}
		else
		{
			return true;
		}
	}
	
</script>
		<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
        </style>
		</head>
		<body>		
		
	<?		
	if($instalador->ComprobarVersionPHP())
	{
	?>
	
<div class="login-page" style="padding: 2% 0 0;">
  <div class="form">	
  	<img src="../portal/imagenes/logo.png" style="width:180px; height: 40px;"/>
	<img src="../portal/imagenes/UIScom.png" style="width: 80px; height: 40px;"/>
	<br><br>	
    <form name="frm_conectarBD" method="post" action="crearConexionInstalacion.php">
<p><b>Instalación y Configuración de MeiWeb 7.0</b></p><img src="imagenes/transparente.gif" width="16" height="16">
       <br><img src="imagenes/bd.gif" width="22" height="22"> Creación de la Base de Datos y de la Conexión al Servidor MySQL
       <br><br> 
		<?
			if($_SESSION['m_error']=="0x0001")
			{
		?>
        <img src="imagenes/error.gif" width="16" height="16" align="texttop"> Error: Los parametros de la conexión no son correctos. Compruebe la ubicación de la Base de Datos, usuario MySQL y la contraseña.
        
		<?
			}
			else if($_SESSION['m_error']=="0x0002")
			{
			?>
		<img src="imagenes/error.gif" width="16" height="16" align="texttop"> Error: El nombre de la Base de Datos es incorrecto, consulte con el Administrador de MySQL el nombre de la Base de Datos asignado para esta instalación e intentelo nuevamente.
        
		<?
			}
		?>
        <input name="txt_hostBD" placeholder="Ubicación de la Base de Datos" type="text" id="txt_hostBD"  value="<?= $hostBD;?>">
        
        <input name="txt_usuario" placeholder="Usuario MySQL" type="text" value="<?= $_SESSION['usuario'];?>" >
        
        <input name="txt_clave" placeholder="Contraseña" type="password" >
        
        <input name="txt_reclave" placeholder="Re-escriba la contraseña" type="password" >
        
        <input name="txt_nombreBaseDatos" placeholder="Nombre de la Base de Datos" type="text" value="<?= $_SESSION['nombreBD'];?>">
        
        <br><br><input name="chk_copiaSeguridad" type="checkbox" id="chk_copiaSeguridad" value="checkbox">                
              Crear la Base de Datos a partir de una Copia de Seguridad MEIWEB </td>
        <br><br>
            <input name="btn_crearConexion" type="button" id="btn_crearConexion" onClick="enviarInstalar();" value="Crear conexión" <?= $btnSiguiente;?>>
            <input type="reset" name="button" value="Restablecer">
            <input name="btn_atras" type="button"  value="Atras" onClick="javascript: enviarAtras();">
            <input type="button" name="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar();">
	          
	  
    </form>		
    <?		
	}
	else
	{
		print "Version Incorrecta de PHP";
	}
?>
		</div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>
	
</body>
		</html>
<?
 }
 else
 {
 	redireccionar("../");
 }

?>
