CREATE TABLE `mei_actividad` ( 
`idactividad` int(11) NOT NULL auto_increment, 
`idevaluacion` int(11) default NULL, 
`idtipoactividad` int(11) NOT NULL default '0', 
`idtiposubgrupo` int(11) default NULL, 
`valor` float NOT NULL default '0', 
`idautor` int(11) NOT NULL default '0', 
`titulo` varchar(100) collate utf8_spanish_ci NOT NULL default '', 
`fechacreacion` varchar(20) collate utf8_spanish_ci NOT NULL default '', 
`fechaactivacion` varchar(20) collate utf8_spanish_ci NOT NULL default '', 
`fechafinalizacion` varchar(20) collate utf8_spanish_ci NOT NULL default '', 
`descripcion` longtext collate utf8_spanish_ci NOT NULL, 
`archivo` varchar(100) collate utf8_spanish_ci NOT NULL default '0', 
`estado` int(11) NOT NULL default '0', 
PRIMARY KEY (`idactividad`), 
KEY `idtipoactividad` (`idtipoactividad`), 
KEY `idautor` (`idautor`), 
KEY `idevaluacion` (`idevaluacion`), 
KEY `idtiposubgrupo` (`idtiposubgrupo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=13 ; 



CREATE TABLE `mei_archivoactividad` ( 
`idarchivo` int(11) NOT NULL auto_increment, 
`idactividad` int(11) NOT NULL default '0', 
`archivo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`localizacion` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idarchivo`), 
KEY `idactividad` (`idactividad`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_archivos` ( 
`idarchivo` int(11) NOT NULL auto_increment, 
`iddirectorio` int(11) NOT NULL default '0', 
`titulo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`descripcion` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`tamano` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`fechacarga` date NOT NULL default '0000-00-00', 
`archivo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idarchivo`), 
KEY `iddirectorio` (`iddirectorio`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ; 



CREATE TABLE `mei_arcusuactividad` ( 
`idarchivo` int(11) NOT NULL auto_increment, 
`idactividad` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`archivo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`localizacion` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idarchivo`), 
KEY `idactividad` (`idactividad`,`idusuario`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ; 



CREATE TABLE `mei_bibliografia` ( 
`idbibliografia` int(11) NOT NULL auto_increment, 
`idmateria` int(11) NOT NULL default '0', 
`titulo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`descripcion` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`url` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`estado` int(11) NOT NULL default '0', 
PRIMARY KEY (`idbibliografia`), 
KEY `idmateria` (`idmateria`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_biblioteca` ( 
`idarchivo` int(11) NOT NULL default '0', 
`idtipoarchivo` int(11) NOT NULL default '0', 
`idtipoaplicacion` int(11) NOT NULL default '0', 
`idtema` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`titulo` varchar(100) character set utf8 collate utf8_spanish_ci NOT NULL default '', 
`archivo` varchar(225) character set utf8 collate utf8_spanish_ci NOT NULL default '', 
`tamano` varchar(50) character set utf8 collate utf8_spanish_ci NOT NULL default '', 
`fechacarga` date NOT NULL default '0000-00-00', 
`descripcion` varchar(225) character set utf8 collate utf8_spanish_ci NOT NULL default '', 
`numdescargas` int(11) NOT NULL default '0', 
PRIMARY KEY (`idarchivo`), 
KEY `idtipoarchivo` (`idtipoarchivo`), 
KEY `idtema` (`idtema`), 
KEY `idusuario` (`idusuario`), 
KEY `idtipoaplicacion` (`idtipoaplicacion`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 



CREATE TABLE `mei_bitacora` ( 
`idbitacora` int(11) NOT NULL auto_increment, 
`idusuario` int(11) NOT NULL default '0', 
`idmodulo` int(11) NOT NULL default '0', 
`idtipoaccion` int(11) NOT NULL default '0', 
`fecha` date NOT NULL default '0000-00-00', 
`hora` time NOT NULL default '00:00:00', 
`descripcion` varchar(255) character set latin1 NOT NULL default '', 
PRIMARY KEY (`idbitacora`), 
KEY `idusuario` (`idusuario`), 
KEY `idtipoaccion` (`idtipoaccion`), 
KEY `idmodulo` (`idmodulo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_calendario` ( 
`idcalendario` int(11) NOT NULL auto_increment, 
`idusuario` int(11) NOT NULL default '0', 
`mensaje` longtext collate utf8_spanish_ci NOT NULL, 
`fechamensaje` date NOT NULL default '0000-00-00', 
`estado` int(11) NOT NULL default '0', 
`fechacreacion` date NOT NULL default '0000-00-00', 
`cartelera` int(11) NOT NULL default '0', 
`destino` int(11) default '0', 
PRIMARY KEY (`idcalendario`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ; 



CREATE TABLE `mei_cartelera` ( 
`idcartelera` int(11) NOT NULL auto_increment, 
`mensaje` longtext collate utf8_spanish_ci NOT NULL, 
`estado` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`fechacreacion` date NOT NULL default '0000-00-00', 
`caducidad` int(11) NOT NULL default '0', 
`fechacaducidad` date NOT NULL default '0000-00-00', 
`destino` int(11) default '0', 
PRIMARY KEY (`idcartelera`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ; 



CREATE TABLE `mei_chat` ( 
`idchat` int(11) NOT NULL auto_increment, 
`idusuario` int(11) NOT NULL default '0', 
`idsalachat` int(11) NOT NULL default '0', 
`mensaje` longtext collate utf8_spanish_ci NOT NULL, 
`fechahoramensaje` datetime NOT NULL default '0000-00-00 00:00:00', 
`colornombre` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`destino` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`tipomensaje` int(11) NOT NULL default '0', 
PRIMARY KEY (`idchat`), 
KEY `idsalachat` (`idsalachat`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_comentarioforo` ( 
`idcomentario` int(11) NOT NULL auto_increment, 
`idforo` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`comentario` longtext collate utf8_spanish_ci NOT NULL, 
`fecha` date NOT NULL default '0000-00-00', 
`hora` time NOT NULL default '00:00:00', 
`calificacion` float NOT NULL default '0', 
`fechacalificacion` date NOT NULL default '0000-00-00', 
`grupal` int(11) NOT NULL default '0', 
PRIMARY KEY (`idcomentario`), 
KEY `idforo` (`idforo`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_configuracion` ( 
`idconfiguracion` int(11) NOT NULL auto_increment, 
`variable` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`valor` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`valordefecto` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idconfiguracion`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=18 ; 



CREATE TABLE `mei_copiaseguridad` ( 
`idcopiaseguridad` int(11) NOT NULL auto_increment, 
`fecha` datetime NOT NULL default '0000-00-00 00:00:00', 
`nombre` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`tamano` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`tipo` int(11) NOT NULL default '0', 
`copia` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idcopiaseguridad`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_correo` ( 
`idcorreo` int(11) NOT NULL auto_increment, 
`asunto` varchar(50) collate utf8_spanish_ci NOT NULL default '', 
`remitente` varchar(50) collate utf8_spanish_ci NOT NULL default '', 
`fecha` date NOT NULL default '0000-00-00', 
`contenido` longtext collate utf8_spanish_ci NOT NULL, 
PRIMARY KEY (`idcorreo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ; 



CREATE TABLE `mei_directorio` ( 
`iddirectorio` int(11) NOT NULL auto_increment, 
`idusuario` int(11) NOT NULL default '0', 
`directorio` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`ubicacion` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`nombre` varchar(255) collate utf8_spanish_ci default NULL, 
PRIMARY KEY (`iddirectorio`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ; 



CREATE TABLE `mei_errorsistema` ( 
`codigoerror` int(11) NOT NULL auto_increment, 
`mensajeerror` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`codigoerror`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ; 



CREATE TABLE `mei_evadtlleprevio` ( 
`idpregunta` int(11) NOT NULL default '0', 
`idprevio` int(11) NOT NULL default '0', 
`valor_pregunta` float default NULL, 
PRIMARY KEY (`idpregunta`,`idprevio`), 
KEY `mei_eva_previo_has_mei_eva_preguntas_FKIndex1` (`idprevio`), 
KEY `mei_eva_previo_has_mei_eva_preguntas_FKIndex2` (`idpregunta`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_evagradopregunta` ( 
`idgrado` int(11) NOT NULL default '0', 
`grado` varchar(11) collate utf8_spanish_ci NOT NULL default '0', 
PRIMARY KEY (`idgrado`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_evagrupo` ( 
`idgrupo` int(11) NOT NULL default '0', 
`idevaluacion` int(11) NOT NULL default '0', 
PRIMARY KEY (`idgrupo`,`idevaluacion`), 
KEY `mei_evaluacion_has_mei_grupo_FKIndex1` (`idevaluacion`), 
KEY `mei_evaluacion_has_mei_grupo_FKIndex2` (`idgrupo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_evaluacion` ( 
`idevaluacion` int(11) NOT NULL default '0', 
`tipoevaluacion` int(11) NOT NULL default '0', 
`idautor` int(11) NOT NULL default '0', 
`nombre` varchar(30) collate utf8_spanish_ci default NULL, 
`valor_eval` float default NULL, 
`promediar` tinyint(4) NOT NULL default '0', 
PRIMARY KEY (`idevaluacion`), 
KEY `mei_evaluacion_FKIndex1` (`tipoevaluacion`), 
KEY `idautor` (`idautor`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_evamodcalificar` ( 
`idmodcalificar` int(11) NOT NULL default '0', 
`modcalificar` varchar(25) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idmodcalificar`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_evapreguntas` ( 
`idpregunta` int(11) NOT NULL auto_increment, 
`idgrado` int(11) NOT NULL default '0', 
`tipo_pregunta` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`idtema` int(11) NOT NULL default '0', 
`autoevaluacion` tinyint(4) NOT NULL default '0', 
`pregunta` text collate utf8_spanish_ci, 
PRIMARY KEY (`idpregunta`), 
KEY `mei_preguntas_FKIndex1` (`idtema`), 
KEY `mei_preguntas_FKIndex2` (`idusuario`), 
KEY `mei_eva_preguntas_FKIndex3` (`idgrado`), 
KEY `mei_eva_preguntas_FKIndex4` (`tipo_pregunta`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=18 ; 



CREATE TABLE `mei_evaprevio` ( 
`idprevio` int(11) NOT NULL auto_increment, 
`idevaluacion` int(11) NOT NULL default '0', 
`idautor` int(11) NOT NULL default '0', 
`titulo` varchar(100) collate utf8_spanish_ci NOT NULL default '', 
`fechacreacion` varchar(20) collate utf8_spanish_ci NOT NULL default '', 
`fechaactivacion` varchar(20) collate utf8_spanish_ci NOT NULL default '', 
`fechafinalizacion` varchar(20) collate utf8_spanish_ci NOT NULL default '', 
`comentario` longtext collate utf8_spanish_ci, 
`tiempo` float NOT NULL default '0', 
`barajarpreg` tinyint(4) NOT NULL default '0', 
`barajarresp` tinyint(4) NOT NULL default '0', 
`intentos` tinyint(4) NOT NULL default '0', 
`valorIntento` float NOT NULL default '0', 
`idmodCalificar` int(11) NOT NULL default '0', 
`mostrarnota` tinyint(4) NOT NULL default '0', 
`estado` tinyint(4) NOT NULL default '0', 
`idtipoprevio` int(11) NOT NULL default '0', 
`valor` float default '1', 
`idtiposubgrupo` int(11) default NULL, 
PRIMARY KEY (`idprevio`), 
KEY `mei_previo_FKIndex2` (`idevaluacion`), 
KEY `idmodCalificar` (`idmodCalificar`), 
KEY `idtipoprevio` (`idtipoprevio`), 
KEY `idtiposubgrupo` (`idtiposubgrupo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=30 ; 



CREATE TABLE `mei_evarespuestas` ( 
`idrespuesta` int(11) NOT NULL auto_increment, 
`idpregunta` int(11) NOT NULL default '0', 
`respuesta` text collate utf8_spanish_ci, 
`valor` float NOT NULL default '0', 
PRIMARY KEY (`idrespuesta`), 
KEY `mei_eva_respuestas_FKIndex2` (`idpregunta`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=59 ; 



CREATE TABLE `mei_evatipopregunta` ( 
`tipo_pregunta` int(11) NOT NULL default '0', 
`tipo` varchar(25) collate utf8_spanish_ci default NULL, 
PRIMARY KEY (`tipo_pregunta`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_favorito` ( 
`idfavorito` int(11) NOT NULL auto_increment, 
`idusuario` int(11) NOT NULL default '0', 
`favorito` int(11) NOT NULL default '0', 
PRIMARY KEY (`idfavorito`), 
KEY `idusuario` (`idusuario`), 
KEY `favorito` (`favorito`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ; 



CREATE TABLE `mei_foro` ( 
`idforo` int(11) NOT NULL auto_increment, 
`idmateria` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`fechacreacion` date NOT NULL default '0000-00-00', 
`fechaactivacion` date NOT NULL default '0000-00-00', 
`fechacaducidad` date NOT NULL default '0000-00-00', 
`horacreacion` time NOT NULL default '00:00:00', 
`horaactivacion` time NOT NULL default '00:00:00', 
`horacaducidad` time NOT NULL default '00:00:00', 
`mensaje` longtext collate utf8_spanish_ci NOT NULL, 
`estado` int(11) NOT NULL default '0', 
`numvisitas` int(11) NOT NULL default '0', 
`votacion` int(11) NOT NULL default '0', 
`numvotos` int(11) NOT NULL default '0', 
`calificable` int(11) NOT NULL default '0', 
`grupal` int(11) NOT NULL default '0', 
`idevaluacion` int(11) default NULL, 
`valor` float default NULL, 
`buscador` longtext collate utf8_spanish_ci, 
`idgrupo` int(11) default NULL, 
PRIMARY KEY (`idforo`), 
KEY `idmateria` (`idmateria`), 
KEY `idusuario` (`idusuario`), 
KEY `idevaluacion` (`idevaluacion`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_glosario` ( 
`idpalabra` int(11) NOT NULL auto_increment, 
`idmateria` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`palabra` varchar(224) NOT NULL default '', 
`significado` longtext NOT NULL, 
PRIMARY KEY (`idpalabra`), 
KEY `idtema` (`idmateria`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 8192 kB' AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_grupo` ( 
`idgrupo` int(11) NOT NULL auto_increment, 
`idmateria` int(11) NOT NULL default '0', 
`nombre` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`ubicacion` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`horario` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idgrupo`), 
KEY `idmateria` (`idmateria`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ; 



CREATE TABLE `mei_ipactivo` ( 
`idipactivo` int(11) NOT NULL auto_increment, 
`idusuario` int(11) NOT NULL default '0', 
`ipactivo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`hora` time NOT NULL default '00:00:00', 
PRIMARY KEY (`idipactivo`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=11 ; 



CREATE TABLE `mei_lanzador` ( 
`idlanzador` int(11) NOT NULL auto_increment, 
`idusuario` int(11) NOT NULL default '0', 
`titulo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`descripcion` longtext collate utf8_spanish_ci NOT NULL, 
`tamano` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`fechacarga` date NOT NULL default '0000-00-00', 
`lanzador` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`ubicacion` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`tipo` int(11) NOT NULL default '0', 
`destino` int(11) default '0', 
PRIMARY KEY (`idlanzador`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ; 



CREATE TABLE `mei_materia` ( 
`idmateria` int(11) NOT NULL auto_increment, 
`nombre` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`nombrecorto` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`codigo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`creditos` int(11) NOT NULL default '0', 
`fechacreacion` date NOT NULL default '0000-00-00', 
`introduccion` longtext collate utf8_spanish_ci NOT NULL, 
PRIMARY KEY (`idmateria`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=3 ; 



CREATE TABLE `mei_modulo` ( 
`idmodulo` int(11) NOT NULL auto_increment, 
`nombre` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`estado` int(11) NOT NULL default '0', 
`orden` int(11) NOT NULL default '0', 
`estadistica` int(11) NOT NULL default '0', 
PRIMARY KEY (`idmodulo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=17 ; 



CREATE TABLE `mei_preguntafrecuente` ( 
`idpreguntafrecuente` int(11) NOT NULL auto_increment, 
`idtema` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`pregunta` text collate utf8_spanish_ci NOT NULL, 
`respuesta` text collate utf8_spanish_ci NOT NULL, 
`estadorespuesta` int(11) NOT NULL default '0', 
PRIMARY KEY (`idpreguntafrecuente`), 
KEY `idtema` (`idtema`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ; 



CREATE TABLE `mei_relcalgru` ( 
`idcalendario` int(11) NOT NULL default '0', 
`idgrupo` int(11) NOT NULL default '0', 
KEY `idgrupo` (`idgrupo`), 
KEY `idcalendario` (`idcalendario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_relcargru` ( 
`idcartelera` int(11) NOT NULL default '0', 
`idgrupo` int(11) NOT NULL default '0', 
KEY `idgrupo` (`idgrupo`), 
KEY `idcartelera` (`idcartelera`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_relcarusu` ( 
`idcartelera` int(11) NOT NULL default '0', 
`estado` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
KEY `idcartelera` (`idcartelera`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_relcorarc` ( 
`idcorreo` int(11) NOT NULL default '0', 
`archivo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`localizacion` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
KEY `idcorreo` (`idcorreo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_relcorusu` ( 
`idusuario` int(11) NOT NULL default '0', 
`idcorreo` int(11) NOT NULL default '0', 
`estado` int(11) NOT NULL default '0', 
KEY `idusuario` (`idusuario`), 
KEY `idcorreo` (`idcorreo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_relforgru` ( 
`idforo` int(11) NOT NULL default '0', 
`idgrupo` int(11) NOT NULL default '0', 
KEY `idforo` (`idforo`), 
KEY `idgrupo` (`idgrupo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_rellangru` ( 
`idlanzador` int(11) NOT NULL default '0', 
`idgrupo` int(11) NOT NULL default '0', 
KEY `idlanzador` (`idlanzador`), 
KEY `idgrupo` (`idgrupo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_relsalgru` ( 
`idsalachat` int(11) NOT NULL default '0', 
`idgrupo` int(11) NOT NULL default '0', 
KEY `idsalachat` (`idsalachat`,`idgrupo`), 
KEY `idgrupo` (`idgrupo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_relsalusu` ( 
`idusuario` int(11) NOT NULL default '0', 
`idsalachat` int(11) NOT NULL default '0', 
`fechaingreso` datetime NOT NULL default '0000-00-00 00:00:00', 
KEY `idusuario` (`idusuario`), 
KEY `idsalachat` (`idsalachat`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_relusugru` ( 
`idusuario` int(11) NOT NULL default '0', 
`idgrupo` int(11) NOT NULL default '0', 
KEY `idusuario` (`idusuario`), 
KEY `idgrupo` (`idgrupo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_relususub` ( 
`idsubgrupo` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
KEY `idsubgrupo` (`idsubgrupo`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_salachat` ( 
`idsalachat` int(11) NOT NULL auto_increment, 
`idusuario` int(11) NOT NULL default '0', 
`nombresala` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`descripcion` longtext collate utf8_spanish_ci NOT NULL, 
`fechacreacion` date NOT NULL default '0000-00-00', 
`estado` int(11) NOT NULL default '0', 
`cartelera` int(11) NOT NULL default '0', 
`destino` int(11) default '0', 
PRIMARY KEY (`idsalachat`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_subgrupo` ( 
`idsubgrupo` int(11) NOT NULL auto_increment, 
`idgrupo` int(11) NOT NULL default '0', 
`idtiposubgrupo` int(11) NOT NULL default '1', 
`nombre` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`descripcion` varchar(255) collate utf8_spanish_ci default NULL, 
PRIMARY KEY (`idsubgrupo`), 
KEY `idgrupo` (`idgrupo`), 
KEY `idtiposubgrupo` (`idtiposubgrupo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=44 ; 



CREATE TABLE `mei_tema` ( 
`idtema` int(11) NOT NULL default '0', 
`idtemapadre` int(11) default NULL, 
`idmateria` int(11) NOT NULL default '0', 
`titulo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`contenido` longtext collate utf8_spanish_ci NOT NULL, 
`estado` int(11) NOT NULL default '0', 
`orden` int(11) NOT NULL default '0', 
`tipo` tinyint(4) default NULL, 
`idusuario` int(11) default NULL, 
PRIMARY KEY (`idtema`), 
KEY `idmateria` (`idmateria`), 
KEY `idtemapadre` (`idtemapadre`), 
KEY `idusuario` (`idusuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_tematemp` ( 
`idtematemp` int(11) NOT NULL auto_increment, 
`idtemapadre` int(11) default NULL, 
`idmateria` int(11) NOT NULL default '0', 
`titulo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`contenido` longtext collate utf8_spanish_ci NOT NULL, 
`estado` int(11) NOT NULL default '0', 
`tipo` tinyint(4) default NULL, 
`fecha` date NOT NULL default '0000-00-00', 
`diahora` varchar(10) character set latin1 NOT NULL default '', 
PRIMARY KEY (`idtematemp`), 
KEY `idmateria` (`idmateria`), 
KEY `idtemapadre` (`idtemapadre`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ; 



CREATE TABLE `mei_tipoaccion` ( 
`idtipoaccion` int(11) NOT NULL auto_increment, 
`tipoaccion` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`estadistica` int(11) NOT NULL default '0', 
PRIMARY KEY (`idtipoaccion`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=40 ; 



CREATE TABLE `mei_tipoactividad` ( 
`idtipoactividad` int(11) NOT NULL auto_increment, 
`tipoactividad` varchar(100) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idtipoactividad`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ; 



CREATE TABLE `mei_tipoalumno` ( 
`idtipoalumno` int(11) NOT NULL auto_increment, 
`tipoalumno` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idtipoalumno`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=3 ; 



CREATE TABLE `mei_tipoaplicacion` ( 
`idtipoaplicacion` int(11) NOT NULL auto_increment, 
`tipoaplicacion` varchar(100) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idtipoaplicacion`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ; 



CREATE TABLE `mei_tipoarchivo` ( 
`idtipoarchivo` int(11) NOT NULL auto_increment, 
`tipoarchivo` varchar(100) collate utf8_spanish_ci NOT NULL default '', 
`nombre` varchar(50) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idtipoarchivo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ; 



CREATE TABLE `mei_tipoevaluacion` ( 
`idtipoevaluacion` int(11) NOT NULL default '0', 
`tipoevaluacion` varchar(20) collate utf8_spanish_ci default NULL, 
PRIMARY KEY (`idtipoevaluacion`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_tipoprevio` ( 
`idtipoprevio` int(11) NOT NULL default '0', 
`tipoprevio` varchar(20) character set utf8 collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idtipoprevio`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 



CREATE TABLE `mei_tiposubgrupo` ( 
`idtiposubgrupo` int(11) NOT NULL auto_increment, 
`tiposubgrupo` text collate utf8_spanish_ci NOT NULL, 
PRIMARY KEY (`idtiposubgrupo`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=3 ; 



CREATE TABLE `mei_tipousuario` ( 
`idtipousuario` int(11) NOT NULL auto_increment, 
`tipousuario` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idtipousuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ; 



CREATE TABLE `mei_usuactividad` ( 
`idactividad` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`fecharespuesta` varchar(20) collate utf8_spanish_ci NOT NULL default '', 
`descripcion` longtext collate utf8_spanish_ci, 
`nota` float default NULL, 
PRIMARY KEY (`idactividad`,`idusuario`), 
KEY `mei_usuario_has_mei_actividad_FKIndex1` (`idusuario`), 
KEY `mei_usuario_has_mei_actividad_FKIndex2` (`idactividad`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_usuario` ( 
`idusuario` int(11) NOT NULL default '0', 
`idtipousuario` int(11) NOT NULL default '0', 
`idtipoalumno` int(11) NOT NULL default '1', 
`primernombre` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`segundonombre` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`primerapellido` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`segundoapellido` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`sexo` varchar(10) collate utf8_spanish_ci NOT NULL default '', 
`clave` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`alias` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`correo` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`telefono` varchar(50) collate utf8_spanish_ci NOT NULL default '', 
`fechanacimiento` date NOT NULL default '0000-00-00', 
`estado` int(11) NOT NULL default '0', 
`errorclave` int(11) NOT NULL default '0', 
`clavesession` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`pregunta` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`respuesta` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`administrador` int(11) NOT NULL default '0', 
PRIMARY KEY (`idusuario`), 
KEY `idtipousuario` (`idtipousuario`), 
KEY `idtipoalumno` (`idtipoalumno`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_usuariotemp` ( 
`idusuarioTemp` int(11) NOT NULL auto_increment, 
`idtipousuario` int(11) NOT NULL default '0', 
`idgrupo` int(11) NOT NULL default '0', 
`datos` varchar(255) collate utf8_spanish_ci NOT NULL default '', 
`separador` varchar(10) collate utf8_spanish_ci NOT NULL default '', 
PRIMARY KEY (`idusuarioTemp`), 
KEY `idtipousuario` (`idtipousuario`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ; 



CREATE TABLE `mei_usuprevio` ( 
`idprevio` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`idintento` int(11) NOT NULL default '0', 
`fechainicio` varchar(20) collate utf8_spanish_ci NOT NULL default '', 
`fechafinal` varchar(20) collate utf8_spanish_ci default NULL, 
`nota` float unsigned default NULL, 
`notaMod` tinyint(4) NOT NULL default '0', 
PRIMARY KEY (`idprevio`,`idusuario`,`idintento`), 
KEY `mei_usuario_has_mei_eva_previo_FKIndex1` (`idusuario`), 
KEY `mei_usuario_has_mei_eva_previo_FKIndex2` (`idprevio`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 



CREATE TABLE `mei_usupreviodtlle` ( 
`idprevio` int(11) NOT NULL default '0', 
`idusuario` int(11) NOT NULL default '0', 
`idintento` int(11) NOT NULL default '0', 
`idpregunta` int(11) NOT NULL default '0', 
`idrespuesta` varchar(20) collate utf8_spanish_ci NOT NULL default '0', 
PRIMARY KEY (`idprevio`,`idusuario`,`idintento`,`idpregunta`), 
KEY `idusuario` (`idusuario`), 
KEY `idpregunta` (`idpregunta`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci; 

