ALTER TABLE `mei_archivoactividad` 
ADD CONSTRAINT `mei_archivoactividad_ibfk_1` FOREIGN KEY (`idactividad`) REFERENCES `mei_actividad` (`idactividad`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_archivos` 
ADD CONSTRAINT `mei_archivos_ibfk_1` FOREIGN KEY (`iddirectorio`) REFERENCES `mei_directorio` (`iddirectorio`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_arcusuactividad` 
ADD CONSTRAINT `mei_arcusuactividad_ibfk_1` FOREIGN KEY (`idactividad`) REFERENCES `mei_usuactividad` (`idactividad`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_arcusuactividad_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuactividad` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_bibliografia` 
ADD CONSTRAINT `mei_bibliografia_ibfk_1` FOREIGN KEY (`idmateria`) REFERENCES `mei_materia` (`idmateria`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_biblioteca` 
ADD CONSTRAINT `mei_biblioteca_ibfk_1` FOREIGN KEY (`idtipoarchivo`) REFERENCES `mei_tipoarchivo` (`idtipoarchivo`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_biblioteca_ibfk_2` FOREIGN KEY (`idtipoaplicacion`) REFERENCES `mei_tipoaplicacion` (`idtipoaplicacion`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_biblioteca_ibfk_3` FOREIGN KEY (`idtema`) REFERENCES `mei_tema` (`idtema`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_biblioteca_ibfk_4` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_bitacora` 
ADD CONSTRAINT `mei_bitacora_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_bitacora_ibfk_3` FOREIGN KEY (`idtipoaccion`) REFERENCES `mei_tipoaccion` (`idtipoaccion`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_bitacora_ibfk_4` FOREIGN KEY (`idmodulo`) REFERENCES `mei_modulo` (`idmodulo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_calendario` 
ADD CONSTRAINT `mei_calendario_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_cartelera` 
ADD CONSTRAINT `mei_cartelera_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_chat` 
ADD CONSTRAINT `mei_chat_ibfk_1` FOREIGN KEY (`idsalachat`) REFERENCES `mei_salachat` (`idsalachat`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_chat_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_comentarioforo` 
ADD CONSTRAINT `mei_comentarioforo_ibfk_1` FOREIGN KEY (`idforo`) REFERENCES `mei_foro` (`idforo`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_comentarioforo_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_directorio` 
ADD CONSTRAINT `mei_directorio_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_evadtlleprevio` 
ADD CONSTRAINT `mei_evadtlleprevio_ibfk_1` FOREIGN KEY (`idprevio`) REFERENCES `mei_evaprevio` (`idprevio`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_evadtlleprevio_ibfk_2` FOREIGN KEY (`idpregunta`) REFERENCES `mei_evapreguntas` (`idpregunta`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_evagrupo` 
ADD CONSTRAINT `mei_evagrupo_ibfk_1` FOREIGN KEY (`idevaluacion`) REFERENCES `mei_evaluacion` (`idevaluacion`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_evagrupo_ibfk_2` FOREIGN KEY (`idgrupo`) REFERENCES `mei_grupo` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_evaluacion` 
ADD CONSTRAINT `mei_evaluacion_ibfk_1` FOREIGN KEY (`tipoevaluacion`) REFERENCES `mei_tipoevaluacion` (`idtipoevaluacion`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_evaluacion_ibfk_2` FOREIGN KEY (`idautor`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_evapreguntas` 
ADD CONSTRAINT `mei_evapreguntas_ibfk_1` FOREIGN KEY (`idtema`) REFERENCES `mei_tema` (`idtema`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_evapreguntas_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_evapreguntas_ibfk_3` FOREIGN KEY (`tipo_pregunta`) REFERENCES `mei_evatipopregunta` (`tipo_pregunta`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_evapreguntas_ibfk_4` FOREIGN KEY (`idgrado`) REFERENCES `mei_evagradopregunta` (`idgrado`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_evaprevio` 
ADD CONSTRAINT `mei_evaprevio_ibfk_1` FOREIGN KEY (`idevaluacion`) REFERENCES `mei_evaluacion` (`idevaluacion`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_evaprevio_ibfk_2` FOREIGN KEY (`idmodCalificar`) REFERENCES `mei_evamodcalificar` (`idmodcalificar`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_evaprevio_ibfk_3` FOREIGN KEY (`idtipoprevio`) REFERENCES `mei_tipoprevio` (`idtipoprevio`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_evaprevio_ibfk_4` FOREIGN KEY (`idtiposubgrupo`) REFERENCES `mei_tiposubgrupo` (`idtiposubgrupo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_evarespuestas` 
ADD CONSTRAINT `mei_evarespuestas_ibfk_1` FOREIGN KEY (`idpregunta`) REFERENCES `mei_evapreguntas` (`idpregunta`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_favorito` 
ADD CONSTRAINT `mei_favorito_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_favorito_ibfk_2` FOREIGN KEY (`favorito`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_foro` 
ADD CONSTRAINT `mei_foro_ibfk_4` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_foro_ibfk_5` FOREIGN KEY (`idmateria`) REFERENCES `mei_materia` (`idmateria`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_foro_ibfk_6` FOREIGN KEY (`idevaluacion`) REFERENCES `mei_evaluacion` (`idevaluacion`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_glosario` 
ADD CONSTRAINT `mei_glosario_ibfk_1` FOREIGN KEY (`idmateria`) REFERENCES `mei_materia` (`idmateria`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_glosario_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_grupo` 
ADD CONSTRAINT `mei_grupo_ibfk_1` FOREIGN KEY (`idmateria`) REFERENCES `mei_materia` (`idmateria`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_ipactivo` 
ADD CONSTRAINT `mei_ipactivo_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_lanzador` 
ADD CONSTRAINT `mei_lanzador_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_preguntafrecuente` 
ADD CONSTRAINT `mei_preguntafrecuente_ibfk_1` FOREIGN KEY (`idtema`) REFERENCES `mei_tema` (`idtema`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_preguntafrecuente_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relcalgru` 
ADD CONSTRAINT `mei_relcalgru_ibfk_1` FOREIGN KEY (`idgrupo`) REFERENCES `mei_grupo` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_relcalgru_ibfk_2` FOREIGN KEY (`idcalendario`) REFERENCES `mei_calendario` (`idcalendario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relcargru` 
ADD CONSTRAINT `mei_relcargru_ibfk_1` FOREIGN KEY (`idgrupo`) REFERENCES `mei_grupo` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_relcargru_ibfk_2` FOREIGN KEY (`idcartelera`) REFERENCES `mei_cartelera` (`idcartelera`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relcarusu` 
ADD CONSTRAINT `mei_relacionucar_ibfk_1` FOREIGN KEY (`idcartelera`) REFERENCES `mei_cartelera` (`idcartelera`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_relacionucar_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relcorarc` 
ADD CONSTRAINT `mei_relacionca_ibfk_1` FOREIGN KEY (`idcorreo`) REFERENCES `mei_correo` (`idcorreo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relcorusu` 
ADD CONSTRAINT `mei_relacionuc_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_relacionuc_ibfk_2` FOREIGN KEY (`idcorreo`) REFERENCES `mei_correo` (`idcorreo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relforgru` 
ADD CONSTRAINT `mei_relforgru_ibfk_1` FOREIGN KEY (`idforo`) REFERENCES `mei_foro` (`idforo`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_relforgru_ibfk_2` FOREIGN KEY (`idgrupo`) REFERENCES `mei_grupo` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_rellangru` 
ADD CONSTRAINT `mei_rellangru_ibfk_1` FOREIGN KEY (`idlanzador`) REFERENCES `mei_lanzador` (`idlanzador`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_rellangru_ibfk_2` FOREIGN KEY (`idgrupo`) REFERENCES `mei_grupo` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relsalgru` 
ADD CONSTRAINT `mei_relsalgru_ibfk_1` FOREIGN KEY (`idsalachat`) REFERENCES `mei_salachat` (`idsalachat`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_relsalgru_ibfk_2` FOREIGN KEY (`idgrupo`) REFERENCES `mei_grupo` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relsalusu` 
ADD CONSTRAINT `mei_relsalusu_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_relsalusu_ibfk_2` FOREIGN KEY (`idsalachat`) REFERENCES `mei_salachat` (`idsalachat`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relusugru` 
ADD CONSTRAINT `mei_relacionug_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_relacionug_ibfk_2` FOREIGN KEY (`idgrupo`) REFERENCES `mei_grupo` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_relususub` 
ADD CONSTRAINT `mei_relususub_ibfk_1` FOREIGN KEY (`idsubgrupo`) REFERENCES `mei_subgrupo` (`idsubgrupo`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_relususub_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_salachat` 
ADD CONSTRAINT `mei_salachat_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_subgrupo` 
ADD CONSTRAINT `mei_subgrupo_ibfk_2` FOREIGN KEY (`idgrupo`) REFERENCES `mei_grupo` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_subgrupo_ibfk_3` FOREIGN KEY (`idtiposubgrupo`) REFERENCES `mei_tiposubgrupo` (`idtiposubgrupo`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_tematemp` 
ADD CONSTRAINT `mei_tematemp_ibfk_1` FOREIGN KEY (`idmateria`) REFERENCES `mei_materia` (`idmateria`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_tematemp_ibfk_2` FOREIGN KEY (`idtemapadre`) REFERENCES `mei_tema` (`idtema`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_usuactividad` 
ADD CONSTRAINT `mei_usuactividad_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_usuactividad_ibfk_2` FOREIGN KEY (`idactividad`) REFERENCES `mei_actividad` (`idactividad`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_usuario` 
ADD CONSTRAINT `mei_usuario_ibfk_1` FOREIGN KEY (`idtipousuario`) REFERENCES `mei_tipousuario` (`idtipousuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_usuariotemp` 
ADD CONSTRAINT `mei_usuariotemp_ibfk_1` FOREIGN KEY (`idtipousuario`) REFERENCES `mei_tipousuario` (`idtipousuario`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_usuprevio` 
ADD CONSTRAINT `mei_usuprevio_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_usuprevio_ibfk_2` FOREIGN KEY (`idprevio`) REFERENCES `mei_evaprevio` (`idprevio`) ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE `mei_usupreviodtlle` 
ADD CONSTRAINT `mei_usupreviodtlle_ibfk_17` FOREIGN KEY (`idusuario`) REFERENCES `mei_usuario` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_usupreviodtlle_ibfk_22` FOREIGN KEY (`idprevio`) REFERENCES `mei_evaprevio` (`idprevio`) ON DELETE CASCADE ON UPDATE CASCADE, 
ADD CONSTRAINT `mei_usupreviodtlle_ibfk_23` FOREIGN KEY (`idpregunta`) REFERENCES `mei_evapreguntas` (`idpregunta`) ON DELETE CASCADE ON UPDATE CASCADE; 

