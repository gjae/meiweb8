INSERT INTO `mei_configuracion` VALUES (1, 'sistemanumerror', '3', '3');
INSERT INTO `mei_configuracion` VALUES (2, 'sistematema', 'tema2', 'tema2');
INSERT INTO `mei_configuracion` VALUES (3, 'sistemanumcorreo', '3', '3');
INSERT INTO `mei_configuracion` VALUES (4, 'sistemanumforo', '3', '3');
INSERT INTO `mei_configuracion` VALUES (5, 'sistematamanoarchivo', '1000000', '1000000');
INSERT INTO `mei_configuracion` VALUES (6, 'sistemaaltocartelera', '150', '150');
INSERT INTO `mei_configuracion` VALUES (7, 'sistemavelcartelera', '2', '2');
INSERT INTO `mei_configuracion` VALUES (8, 'sistemavelmaxcartelera', '7', '7');
INSERT INTO `mei_configuracion` VALUES (9, 'sistemalongmaxcartelera', '300', '300');
INSERT INTO `mei_configuracion` VALUES (10, 'sistematextocalendario', '10', '10');
INSERT INTO `mei_configuracion` VALUES (11, 'sistemaagnoinicial', '2003', '2000');
INSERT INTO `mei_configuracion` VALUES (12, 'sistemaagnosfuturo', '4', '4');
INSERT INTO `mei_configuracion` VALUES (13, 'sistematiempoactivo', '60', '60');
INSERT INTO `mei_configuracion` VALUES (14, 'sistematiempocopia', '15', '15');
INSERT INTO `mei_configuracion` VALUES (15, 'sistemanobitacora', '1', '1');
INSERT INTO `mei_configuracion` VALUES (17, 'sistemacopiaautomatica', '1', '1');

INSERT INTO `mei_errorsistema` VALUES (1, 'La informaci�n del usuario no es correcta');
INSERT INTO `mei_errorsistema` VALUES (2, 'El usuario no se encuentra registrado en el sistema');
INSERT INTO `mei_errorsistema` VALUES (3, 'El usuario se encuentra bloqueado');
INSERT INTO `mei_errorsistema` VALUES (4, 'La informaci�n del usuario no es correcta');
INSERT INTO `mei_errorsistema` VALUES (5, 'El usuario no se encuentra registrado en ning�n 
grupo');
INSERT INTO `mei_errorsistema` VALUES (6, '&nbsp;');


INSERT INTO `mei_modulo` VALUES (1, 'registro', 1, 0, 0);
INSERT INTO `mei_modulo` VALUES (2, 'calendario', 1, 2, 1);
INSERT INTO `mei_modulo` VALUES (3, 'cartelera', 1, 3, 1);
INSERT INTO `mei_modulo` VALUES (4, 'foro', 1, 4, 1);
INSERT INTO `mei_modulo` VALUES (5, 'correo', 1, 5, 1);
INSERT INTO `mei_modulo` VALUES (6, 'chat', 1, 6, 1);
INSERT INTO `mei_modulo` VALUES (7, 'configuracion', 1, 10, 0);
INSERT INTO `mei_modulo` VALUES (8, 'estadistica', 1, 11, 0);
INSERT INTO `mei_modulo` VALUES (9, 'ayuda', 1, 12, 0);
INSERT INTO `mei_modulo` VALUES (10, 'lanzadores', 1, 8, 1);
INSERT INTO `mei_modulo` VALUES (11, 'listas', 1, 9, 0);
INSERT INTO `mei_modulo` VALUES (12, 'biblioteca', 1, 7, 0);
INSERT INTO `mei_modulo` VALUES (13, 'materias', 1, 1, 0);
INSERT INTO `mei_modulo` VALUES (14, 'actividades', 1, 13, 0);
INSERT INTO `mei_modulo` VALUES (15, 'faq', 1, 14, 0);
INSERT INTO `mei_modulo` VALUES (16, 'glosario', 1, 15, 0);

INSERT INTO `mei_tipoaccion` VALUES (1, 'Ingreso a MeiWeb', 0);
INSERT INTO `mei_tipoaccion` VALUES (2, 'Ingreso al Modulo de Calendario', 0);
INSERT INTO `mei_tipoaccion` VALUES (3, 'Ver Cronograma General', 1);
INSERT INTO `mei_tipoaccion` VALUES (4, 'Ver Citas', 1);
INSERT INTO `mei_tipoaccion` VALUES (5, 'Modificar Cita', 0);
INSERT INTO `mei_tipoaccion` VALUES (6, 'Eliminar Cita', 0);
INSERT INTO `mei_tipoaccion` VALUES (7, 'Ingreso al Modulo de Cartelera', 1);
INSERT INTO `mei_tipoaccion` VALUES (8, 'Ver Mensajes', 1);
INSERT INTO `mei_tipoaccion` VALUES (9, 'Crear Mensajes', 0);
INSERT INTO `mei_tipoaccion` VALUES (10, 'Modificar Mensaje', 0);
INSERT INTO `mei_tipoaccion` VALUES (11, 'Eliminar Mensaje', 0);
INSERT INTO `mei_tipoaccion` VALUES (12, 'Ingreso al Modulo de Cartelera', 0);
INSERT INTO `mei_tipoaccion` VALUES (13, 'Ver Foros', 1);
INSERT INTO `mei_tipoaccion` VALUES (14, 'Crear Foro', 0);
INSERT INTO `mei_tipoaccion` VALUES (15, 'Modificar Foro', 0);
INSERT INTO `mei_tipoaccion` VALUES (16, 'Eliminar Foro', 0);
INSERT INTO `mei_tipoaccion` VALUES (17, 'Comentar Foro', 1);
INSERT INTO `mei_tipoaccion` VALUES (18, 'Votar Foro', 0);
INSERT INTO `mei_tipoaccion` VALUES (19, 'Eliminar Comentario', 0);
INSERT INTO `mei_tipoaccion` VALUES (20, 'Ingreso al Modulo de Correo', 0);
INSERT INTO `mei_tipoaccion` VALUES (21, 'Ver Correo', 1);
INSERT INTO `mei_tipoaccion` VALUES (22, 'Redactar Correo', 1);
INSERT INTO `mei_tipoaccion` VALUES (23, 'Eliminar Correo', 0);
INSERT INTO `mei_tipoaccion` VALUES (24, 'Ingreso al Modulo de Chat', 0);
INSERT INTO `mei_tipoaccion` VALUES (25, 'Crear Sala de Chat', 0);
INSERT INTO `mei_tipoaccion` VALUES (26, 'Visitar Sala de Chat', 1);
INSERT INTO `mei_tipoaccion` VALUES (27, 'Modificar Sala de Chat', 0);
INSERT INTO `mei_tipoaccion` VALUES (28, 'Eliminar Sala de Chat', 0);
INSERT INTO `mei_tipoaccion` VALUES (29, 'Modificar Perfil', 0);
INSERT INTO `mei_tipoaccion` VALUES (30, 'Ingreso al Modulo de Lanzadores', 1);
INSERT INTO `mei_tipoaccion` VALUES (31, 'Crear Lanzador', 0);
INSERT INTO `mei_tipoaccion` VALUES (32, 'Modificar Lanzador', 0);
INSERT INTO `mei_tipoaccion` VALUES (33, 'Eliminar Lanzador', 0);
INSERT INTO `mei_tipoaccion` VALUES (34, 'Agregar Alumno', 0);
INSERT INTO `mei_tipoaccion` VALUES (35, 'Carga Masiva de Alumnos', 0);
INSERT INTO `mei_tipoaccion` VALUES (36, 'Modificar Alumno', 0);
INSERT INTO `mei_tipoaccion` VALUES (37, 'Reiniciar Alumno', 0);
INSERT INTO `mei_tipoaccion` VALUES (38, 'Eliminar Alumno', 0);
INSERT INTO `mei_tipoaccion` VALUES (39, 'Salir de MeiWeb', 0);

INSERT INTO `mei_tipoactividad` VALUES (1, 'Taller');
INSERT INTO `mei_tipoactividad` VALUES (2, 'Laboratorio');
INSERT INTO `mei_tipoactividad` VALUES (3, 'Exposici�n');
INSERT INTO `mei_tipoactividad` VALUES (4, 'Tarea');


INSERT INTO `mei_tipoalumno` VALUES (1, 'pregrado');
INSERT INTO `mei_tipoalumno` VALUES (2, 'maestria');

INSERT INTO `mei_tipoaplicacion` VALUES (1, 'Software');
INSERT INTO `mei_tipoaplicacion` VALUES (2, 'Multimedia');
INSERT INTO `mei_tipoaplicacion` VALUES (3, 'Libro');
INSERT INTO `mei_tipoaplicacion` VALUES (4, 'Manual');
INSERT INTO `mei_tipoaplicacion` VALUES (5, 'Art�culo');

INSERT INTO `mei_tipoarchivo` VALUES (1, 'image/gif', 'GIF');
INSERT INTO `mei_tipoarchivo` VALUES (2, 'image/pjpeg', 'JPEG');
INSERT INTO `mei_tipoarchivo` VALUES (3, 'application/x-zip-compressed', 'ZIP');
INSERT INTO `mei_tipoarchivo` VALUES (4, 'application/pdf', 'PDF');
INSERT INTO `mei_tipoarchivo` VALUES (5, 'varios', 'VARIOS');


INSERT INTO `mei_tipoevaluacion` VALUES (1, 'Previo');
INSERT INTO `mei_tipoevaluacion` VALUES (2, 'Actividades');

INSERT INTO `mei_tipoprevio` VALUES (1, 'Previo');
INSERT INTO `mei_tipoprevio` VALUES (2, 'Quiz');

INSERT INTO `mei_tiposubgrupo` VALUES (1, 'Clase');
INSERT INTO `mei_tiposubgrupo` VALUES (2, 'Investigaci�n');

INSERT INTO `mei_tipousuario` VALUES (1, 'Administrador');
INSERT INTO `mei_tipousuario` VALUES (2, 'Profesor');
INSERT INTO `mei_tipousuario` VALUES (3, 'Estudiante');
INSERT INTO `mei_tipousuario` VALUES (4, 'Invitado');	

INSERT INTO `mei_evagradopregunta` VALUES (1, 'F�cil');
INSERT INTO `mei_evagradopregunta` VALUES (2, 'Medio');
INSERT INTO `mei_evagradopregunta` VALUES (3, 'Dificil');

INSERT INTO `mei_evatipopregunta` VALUES (1, 'Unica Respuesta');
INSERT INTO `mei_evatipopregunta` VALUES (2, 'Multiple Respuesta');
INSERT INTO `mei_evatipopregunta` VALUES (3, 'Falso o Verdadero');
INSERT INTO `mei_evatipopregunta` VALUES (4, 'An�lisis de Relaci�n');

INSERT INTO `mei_evamodcalificar` VALUES (1, 'Promedio');
INSERT INTO `mei_evamodcalificar` VALUES (2, 'Primer Intento');
INSERT INTO `mei_evamodcalificar` VALUES (3, 'Ultimo Intento');
INSERT INTO `mei_evamodcalificar` VALUES (4, 'Nota mas Baja');
INSERT INTO `mei_evamodcalificar` VALUES (5, 'Nota mas Alta');	  