<?
	session_start();
	include_once("../baseDatos/BD.class.php");
	include_once("../configuracion/InstaladorMEIWEB.class.php");
	include_once("../librerias/estandar.lib.php");
	
	function enviarError($baseDatos)
	{	
		if(isset($_SESSION['m_error']))
		{
			$_SESSION['m_error'];			
		}
		if(isset($_SESSION['hostBD']))
		{
			$_SESSION['hostBD'];				
		}
		if(isset($_SESSION['usuario']))
		{
			$_SESSION['usuario'];				
		}
		if(isset($_SESSION['nombreBD']))
		{
			$_SESSION['nombreBD'];			
		}
		
		$_SESSION['m_error']=$baseDatos->m_error;
		$_SESSION['hostBD']=$_POST['txt_hostBD'];
		$_SESSION['usuario']=$_POST['txt_usuario'];
		$_SESSION['nombreBD']=$_POST['txt_nombreBaseDatos'];	
		
		redireccionar("conectarInstalacion.php");
	}
	
	$baseDatos=new BD();
	$baseDatos->m_archivoSQL="bdMEIWEB.sql";
	
	$instalador=new InstaladorMEIWEB();		
	
	if($instalador->ComprobarSessionMEIWEB())
	{	
	
	if($baseDatos->ComprobarBD($_POST['txt_hostBD'],$_POST['txt_usuario'],$_POST['txt_clave'],$_POST['txt_nombreBaseDatos']))
	{
		session_destroy();
		
		$instalador->CrearBdConfMEIWEB($_POST['txt_hostBD'],$_POST['txt_usuario'],$baseDatos->CodificarClaveBD($_POST['txt_clave']),$_POST['txt_nombreBaseDatos']);
		$instalador->CrearControlConfMEIWEB();
		
		if(empty($_POST['chk_copiaSeguridad']))
		{
			if(!$baseDatos->CargarSqlBD($_POST['txt_hostBD'],$_POST['txt_usuario'],$baseDatos->CodificarClaveBD($_POST['txt_clave']),$_POST['txt_nombreBaseDatos'],false))
			{
				enviarError(baseDatos);
			}
			else
			{
				redireccionar("crearAdminInstalacion.php");
			}		
		}
		else
		{

				//session_register("hostBD");			
				$_SESSION['hostBD'];
				//session_register("usuario");			
				$_SESSION['usuario'];
				//session_register("nombreBD");	
				$_SESSION['nombreBD'];
				//session_register("clave");	
				$_SESSION['clave'];
				
				$_SESSION['hostBD']=$_POST['txt_hostBD'];
				$_SESSION['usuario']=$_POST['txt_usuario'];
				$_SESSION['nombreBD']=$_POST['txt_nombreBaseDatos'];	
				$_SESSION['clave']=$_POST['txt_clave'];		
				
			redireccionar("conectarRestauracion.php");
		}
	}
	else
	{
		enviarError($baseDatos);		
	}
	
		
	}
	else
	{
		redireccionar("../");
	}
?>
