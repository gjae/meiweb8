<?
	session_start();
	include_once("../librerias/estandar.lib.php");
	include_once("../baseDatos/BD.class.php");
	include_once("../configuracion/InstaladorMEIWEB.class.php");
	
	$baseDatos=new BD();
	$baseDatos->m_archivoSQL="bdMEIWEBCS.mcs";
	
	$instalador=new InstaladorMEIWEB();
	
	if($instalador->ComprobarSessionMEIWEB())
	{
		
	if($_FILES['fil_archivo'])
	{		
		if($instalador->CargarArchivoCS('fil_archivo'))
		{
			if(!$baseDatos->CargarSqlBD($_SESSION['hostBD'],$_SESSION['usuario'],$baseDatos->CodificarClaveBD($_SESSION['clave']),$_SESSION['nombreBD'],true))
			{
				// Error en la firma
							redireccionar("conectarRestauracion.php?error=0x002");
			}
		}
		else
		{
			//No se cargo el archivo
			redireccionar("conectarRestauracion.php?error=0x001");
		}
	}
	redireccionar("crearAdminInstalacion.php");
	
	}
	else
	{
		redireccionar("../");
	}
?>
