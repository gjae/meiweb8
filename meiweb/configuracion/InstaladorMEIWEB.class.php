<?
	class InstaladorMEIWEB
	{
		var $m_phpVersion;
		var $m_versionProtocolo;
		var $m_versionServidor;
		var $m_activacionSession;
		var $m_raiz;
		var $m_directorio;
		var $m_versionCGI;
		var $m_puertoHTTP;
		var $m_phpVersionBandera;
		var $m_versionProtocoloBandera;
		var $m_versionServidorBandera;
		var $m_activacionSessionBanadera;
		var $m_saltoArchivo;
		var $m_separadorCadena;
		var $m_nombreArchivoCS;
		var $m_nombreDirectorioCS;
		
		var $m_erroInstalacion;
		
		function InstaladorMEIWEB()
		{
			$this->m_erroInstalacion=0;
			$this->m_saltoArchivo="\n";
			$this->m_separadorCadena=";";
			
			$this->m_nombreArchivoCS="bdMEIWEBCS.mcs";
			$this->m_nombreDirectorioCS="archivos/";
									
			$this->RecursosSistemaMEIWEB();
		}
		
		function CrearSessionMEIWEB()
		{
					
			if(file_exists("archivos/session.conf"))
			{
				if(!@unlink("archivos/session.conf"))
				{
					return false;				
				}		
			}
			
			$archivoEnlace=@fopen("archivos/session.conf","w+");
			
			if($archivoEnlace)
			{			
				fputs($archivoEnlace,md5(sha1(date("H-d"))));
				fclose($archivoEnlace);
				return true;
			}
			else
			{
				return false;
			}
			
		}
		
		function ComprobarSessionMEIWEB()
		{
			if(file_exists("archivos/session.conf"))
			{
				$session=file_get_contents("archivos/session.conf");
				
				if($session==md5(sha1(date("H-d"))))
				{
					return true;
				}
				else
				{
					
					$this->InicioArchivosMEIWEB();
					return false;
				}
			}
			else
			{
				$this->InicioArchivosMEIWEB();
				return false;
			}			
		}
		
		function CargarArchivoCS($a_archivo)
		{
			if (move_uploaded_file($_FILES[$a_archivo]['tmp_name'], $this->m_nombreDirectorioCS.$this->m_nombreArchivoCS))
			{ 
				return true;					
			}
			else
			{ 
				return false;
			} 			
		}	
		
		function InicioArchivosMEIWEB()
		{
			if(file_exists("archivos/BD.conf"))
			{
				unlink("archivos/BD.conf");
			}
			
			if(file_exists("archivos/control.conf"))
			{
				unlink("archivos/control.conf");
			}
			
			if(file_exists("archivos/install.conf"))
			{
				unlink("archivos/install.conf");
			}
			
			if(file_exists("archivos/admin.conf"))
			{
				unlink("archivos/admin.conf");
			}
			
			if(file_exists("archivos/conf.conf"))
			{
				unlink("archivos/conf.conf");
			}
		}
		
		function CrearBdConfMEIWEB($a_hostBD,$a_usuarioBD,$a_claveBDCodif,$a_nombreBD)
		{
			$archivoEnlace=fopen("archivos/BD.conf","w+");
			
			if($archivoEnlace)
			{	
				$bdConf=$a_nombreBD.";";
				$bdConf.=$a_usuarioBD.";";
				$bdConf.=$a_claveBDCodif.";";
				$bdConf.=$a_hostBD.";";
				
				fputs($archivoEnlace,base64_encode($bdConf));
														
				fclose($archivoEnlace);
			}
				else
			{
				print "No se ha podido crear el archivo BD.conf";
			}
		}
		
		function CrearFirmaControlMEIWEB()
		{
			$firmaControlMEIWEB=md5(sha1(date("Y-j-d")));
			return $firmaControlMEIWEB;		
		}
		
		function CrearControlConfMEIWEB()
		{
			$archivoEnlace=fopen("archivos/control.conf","w+");
			
			if($archivoEnlace)
			{			
				
				fputs($archivoEnlace,$this->CrearFirmaControlMEIWEB());
										
				fclose($archivoEnlace);
			}
			else
			{
				print "No se ha podido crear el archivo control.conf";
			}
		}
		
		function CrearInstallConfMEIWEB()
		{
			$archivoEnlace=fopen("archivos/install.conf","w+");
			
			if($archivoEnlace)
			{			

				fputs($archivoEnlace,"Fin de la instalacion ".date("Y-n-j"));
				fclose($archivoEnlace);
			}
			else
			{
				print "No se ha podido crear el archivo install.conf";
			}
		}
		
		function CrearConfConfMEIWEB()
		{
						
			$archivoEnlace=fopen("archivos/conf.conf","w+");			
			if($archivoEnlace)
			{			
				fputs($archivoEnlace,"Fecha y parametros informativos de instalacion");
				fclose($archivoEnlace);
			}
			else
			{
				print "No se ha podido crear el archivo install.conf";
			}
		}
		
		function CrearAdminConfMEIWEB($a_administrador,$a_pregunta,$a_respuesta,$a_clave)
		{
			
			if(file_exists("../configuracion/archivos/admin.conf"))
			{
				unlink("../configuracion/archivos/admin.conf");
			}
			$archivoEnlace=fopen("../configuracion/archivos/admin.conf","w+");
			
			$cadenaAdministracion=base64_encode($a_administrador).$this->m_separadorCadena.base64_encode($a_pregunta).$this->m_separadorCadena.md5(sha1($a_respuesta)).$this->m_separadorCadena.md5(sha1($a_clave));
			
			if($archivoEnlace)
			{			

				fputs($archivoEnlace,$cadenaAdministracion);
									
				fclose($archivoEnlace);
			}
			else
			{
				print "No se ha podido crear el archivo admin.conf";
			}
		}
		
		function ComprobarRecursosMEIWEB()
		{
			
			if(!$this->ComprobarVersionPHP())
			{
					$this->m_erroInstalacion++;
				
			}
							
			if(!$this->ComprobarSession())
			{
					$this->m_erroInstalacion++;
			}
									
			$this->ComprobarProtocolo();
			$this->ComprobarServidor();
			
		}
		
		function CrearDirectoriosDatos($a_path)
		{
			$archivoDirectorios="../configuracion/archivos/indiceDirectorios.dat";
			
			if(file_exists($archivoDirectorios))
			{
				$indiceDirectorios=file($archivoDirectorios);

				if(sizeof($indiceDirectorios))
				{
					foreach($indiceDirectorios as $directorio)
					{
						$directorio=trim($directorio);
						
						if(!file_exists($directorio))
						{
							if(@mkdir($directorio, 0777))
							{
								if($archivoPrueba=@fopen($directorio."indice.conf","w+"))
								{
									fclose($archivoPrueba);
									@unlink($directorio."indice.conf");
								}
								else
								{
									return false;
								}
							}
							else
							{
								return false;
							}
						}
					}
					return true;	
				}			
			}
			else
			{
				return true;			
			}

		
		}
		function DirectorioInstalacionMEIWEB()
		{
			$directorio=str_replace ("meiweb/configuracion/index.php","",$_SERVER['PATH_TRANSLATED']);
			$directorio=str_replace ("meiweb/configuracion/index.php","",$directorio);
			
			return $directorio;		
		}
		
		function RecursosSistemaMEIWEB()
		{
			$this->m_raiz=$_SERVER['DOCUMENT_ROOT'];
			
			$this->m_directorio=str_replace ("meiweb/configuracion/analizarInstalacion.php","",$_SERVER['PATH_TRANSLATED']);
			$this->m_directorio=str_replace ("meiweb/configuracion/analizarinstalacion.php","",$this->m_directorio);
		
			$this->m_versionCGI=$_SERVER['GATEWAY_INTERFACE'];
			$this->m_puertoHTTP=$_SERVER['SERVER_PORT'];
		}
		
		function ComprobarVersionPHP()
		{
			$this->m_phpVersion=PHP_VERSION;
			$arrayVersion=explode(".",PHP_VERSION);
			$version=$arrayVersion[0];
			
			if($version < 4)
			{
				$this->m_phpVersionBandera=false;
				return false;
			}
			else 
			{
				$this->m_phpVersionBandera=true;
				return true;
			}
		}
		
		function ComprobarProtocolo()
		{
			$this->m_versionProtocolo=$_SERVER['SERVER_PROTOCOL'];
			if(empty($this->m_versionProtocolo))
			{
				$this->m_versionProtocoloBandera=false;
				return false;
			}
			else
			{
				$this->m_versionProtocoloBandera=true;
				return true;
			}
		}
		
		function ComprobarServidor()
		{
			$this->m_versionServidor=$_SERVER['SERVER_SOFTWARE'];
			if(empty($this->m_versionServidor))
			{
				$this->m_versionServidorBandera=false;
				return false;
			}
			else
			{
				$this->m_versionServidorBandera=true;
				return true;
			}
		}
		
		function ComprobarSession()
		{
			if(session_start())
			{
				$this->m_activacionSessionBanadera=true;
				$this->m_activacionSession=true;
				return true;
			}
			else 
			{
				$this->m_activacionSessionBanadera=false;
				$this->m_activacionSession=false;
				return false;
			}
		}
	
	}
?>
