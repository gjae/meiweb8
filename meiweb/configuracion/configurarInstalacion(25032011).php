<?
	session_start();
	session_destroy();
	
	include_once("../baseDatos/BD.class.php");
	include_once("../librerias/estandar.lib.php");	
	
	if(!file_exists("archivos/conf.conf"))
	{
	$baseDatos= new BD();	
	
	$sql="SELECT mei_modulo.nombre,mei_modulo.estado FROM mei_modulo";	
	$consultaModulos=$baseDatos->ConsultarBD($sql);

	while(list($nombreModulo,$estadoModulo)=mysql_fetch_array($consultaModulos))
	{
	
		switch(strtolower($nombreModulo))
		{
			case "cartelera":
			{
				if($estadoModulo==1)
				{
					$chkCartelera="checked";
					
				}
				else
				{
					$activoCartelera="disabled";
				}				
			}
			break;
			
			case "calendario":
			{
				if($estadoModulo==1)
				{
					$chkCalendario="checked";
					
				}
				else
				{
					$activoCalendario="disabled";
				}				
			}
			break;
			
			case "foro":
			{
				if($estadoModulo==1)
				{
					$chkForo="checked";
					
				}
				else
				{
					$activoForo="disabled";
				}				
			}
			break;
			
			case "correo":
			{
				if($estadoModulo==1)
				{
					$chkCorreo="checked";
					
				}
				else
				{
					$activoCorreo="disabled";
				}				
			}
			break;
			
			case "chat":
			{
				if($estadoModulo==1)
				{
					$chkChat="checked";
					
				}
				else
				{
					$activoChat="disabled";
				}				
			}
			break;
			
			case "lanzadores":
			{
				if($estadoModulo==1)
				{
					$chkLanzadores="checked";
					
				}
				else
				{
					$activoLanzadores="disabled";
				}				
			}
			break;
		case "BancodePreguntas":
			{
				if($estadoModulo==1)
				{
					$chkBancodePreguntas="checked";
					
				}
				else
				{
					$activoBancodePreguntas="disabled";
				}				
			}
			break;
		}
	}
	
		
	$directorioBase="../temas";
	
	$pathBase=opendir($directorioBase);
	$cont=0;
		
	while ($directorio = readdir($pathBase)) 
	{
		if(!is_file($directorio) && $directorio!= "." && $directorio!= "..")
		{
			$listaIndexTema[$cont]=$directorio.";";
			$listaIndexTema[$cont].=file_get_contents($directorioBase."/".$directorio."/indiceTema.conf");
			
			$cont++;
		}
	}
	closedir($pathBase);
	
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../temas/tema2/estilo1024x768.css" rel="stylesheet" type="text/css">
<title>Instalaci&oacute;n y Configuraci&oacute;n MEIWEB</title>

<script language="javascript">
	function refrescar()
	{
		document.frm_opciones.action="configurarInstalacion.php";
		document.frm_opciones.submit();
	}

	function enviarInstalar(opcion)
	{
		if(opcion == 1)
		{
			document.frm_opciones.action="guardarInstalacion.php?opcion=1";
			document.frm_opciones.reset();
			document.frm_opciones.submit();
		}
		else if(opcion == 2)
		{
			if(validarDatos())
			{
				document.frm_opciones.action="guardarInstalacion.php?opcion=2";
				document.frm_opciones.submit();
			}
			
		}	
		
	}

	
	function activarItems(opcion)
	{
		switch(opcion)
		{
			case 1:
			{
				if(document.frm_opciones.chk_cartelera.checked==true)
				{
					document.frm_opciones.txt_tamanoCartelera.disabled=false;
					document.frm_opciones.txt_velCartelera.disabled=false;
					document.frm_opciones.txt_velMaxCartelera.disabled=false;
					document.frm_opciones.txt_lonCartelera.disabled=false;
				}
				else
				{
					document.frm_opciones.txt_tamanoCartelera.disabled=true;
					document.frm_opciones.txt_velCartelera.disabled=true;
					document.frm_opciones.txt_velMaxCartelera.disabled=true;
					document.frm_opciones.txt_lonCartelera.disabled=true;
				}
				
			}break;
			
			case 2:
			{
				if(document.frm_opciones.chk_calendario.checked==true)
				{
					document.frm_opciones.txt_lonCalendario.disabled=false;
					document.frm_opciones.txt_fechaInicial.disabled=false;
					document.frm_opciones.txt_anosFuturos.disabled=false;					
				}
				else
				{
					
					document.frm_opciones.txt_lonCalendario.disabled=true;
					document.frm_opciones.txt_fechaInicial.disabled=true;
					document.frm_opciones.txt_anosFuturos.disabled=true;
				}
				
			}break;
			
			case 3:
			{
				if(document.frm_opciones.chk_foro.checked==true)
				{
					document.frm_opciones.txt_numForo.disabled=false;
				}
				else
				{
					document.frm_opciones.txt_numForo.disabled=true;
				}
				
			}break;
			
			case 4:
			{
				if(document.frm_opciones.chk_correo.checked==true)
				{
					document.frm_opciones.txt_numCorreo.disabled=false;
				}
				else
				{
					document.frm_opciones.txt_numCorreo.disabled=true;					
				}
				
			}break;
		
		}
	}
	
	
	function validarDatos()
	{
		if(isNaN(document.frm_opciones.txt_tamanoArchivo.value) || parseInt(document.frm_opciones.txt_tamanoArchivo.value) < 10)
		{
			alert("El Tama�o m�ximo permitido en la carga de archivos no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 10.");
			document.frm_opciones.txt_tamanoArchivo.value="";
		}
		else if(isNaN(document.frm_opciones.txt_numerror.value) || parseInt(document.frm_opciones.txt_numerror.value) < 3)
		{
			alert("El N�mero de intentos de login antes de bloquear un usuario no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 3.");
			document.frm_opciones.txt_numerror.value="";
		}
		else if(isNaN(document.frm_opciones.txt_tamanoCartelera.value) || parseInt(document.frm_opciones.txt_tamanoCartelera.value) < 100)
		{
			alert("El Tama�o de la Cartelera no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 100.");
			document.frm_opciones.txt_tamanoCartelera.value="";
		}
		
		else if(isNaN(document.frm_opciones.txt_velCartelera.value) || parseInt(document.frm_opciones.txt_velCartelera.value) < 1)
		{
			alert("La Velocidad de la visualizaci�n de mensajes no es correcta. Aseg�rese de que el valor sea un n�mero mayor que 1.");
			document.frm_opciones.txt_velCartelera.value="";
		}
		
		else if(isNaN(document.frm_opciones.txt_velMaxCartelera.value) || parseInt(document.frm_opciones.txt_velMaxCartelera.value) < 1)
		{
			alert("La Velocidad m�xima de la visualizaci�n de mensajes no es correcta. Aseg�rese de que el valor sea un n�mero mayor que 1.");
			document.frm_opciones.txt_velMaxCartelera.value="";
		}
		else if(isNaN(document.frm_opciones.txt_lonCartelera.value) || parseInt(document.frm_opciones.txt_lonCartelera.value) < 10)
		{
			alert("El N�mero m�ximo de caracteres en la visualizaci�n de los mensajes de Cartelera no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 10.");
			document.frm_opciones.txt_lonCartelera.value="";
		}
		else if(isNaN(document.frm_opciones.txt_lonCalendario.value) || parseInt(document.frm_opciones.txt_lonCalendario.value) < 10)
		{
			alert("El N�mero m�ximo de caracteres en la visualizaci�n de los mensajes de Calendario no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 10.");
			document.frm_opciones.txt_lonCalendario.value="";
		}
		else if(isNaN(document.frm_opciones.txt_fechaInicial.value) || parseInt(document.frm_opciones.txt_fechaInicial.value) < 2000)
		{
			alert("El A�o inicial en el selector de fechas no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 2000.");
			document.frm_opciones.txt_fechaInicial.value="";
		}
		else if(isNaN(document.frm_opciones.txt_anosFuturos.value) || parseInt(document.frm_opciones.txt_anosFuturos.value) < 4)
		{
			alert("El N�mero de a�os futuros en el selector de fechas no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 4.");
			document.frm_opciones.txt_anosFuturos.value="";
		}
		else if(isNaN(document.frm_opciones.txt_numForo.value) || parseInt(document.frm_opciones.txt_numForo.value) < 3)
		{
			alert("El N�mero de Foros visualizados en la bandeja de entrada no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 3.");
			document.frm_opciones.txt_numForo.value="";
		}
		else if(isNaN(document.frm_opciones.txt_numCorreo.value) || parseInt(document.frm_opciones.txt_numCorreo.value) < 3)
		{
			alert("El N�mero de Correos visualizados en la bandeja de entrada no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 3.");
			document.frm_opciones.txt_numCorreo.value="";
		}
		else if(isNaN(document.frm_opciones.txt_tiePenalizacion.value) || parseInt(document.frm_opciones.txt_tiePenalizacion.value) < 1)
		{
			alert("El Tiempo de penalizacion para el cierre de sesi�n no es correcto. Aseg�rese de que el valor sea un n�mero mayor que 1.");
			document.frm_opciones.txt_tiePenalizacion.value="";
		}
		else if(isNaN(document.frm_opciones.txt_freCopia.value) || parseInt(document.frm_opciones.txt_freCopia.value) < 1)
		{
			alert("La Frecuencia de generaci�n de copias de seguridad autom�ticas no es correcta. Aseg�rese de que el valor sea un n�mero mayor que 1.");
			document.frm_opciones.txt_freCopia.value="";
		}
		else 
		{
			return true;
		}
		
	}	
	
</script>
</head>

<body>
	<form name="frm_opciones" method="post">
		<table width="100%" class="tablaPrincipal">
			<tr valign="top">
			  <td>
		  			<table align="center" class="tablaCentral">
					<tr class="trTitulo">
          				<td colspan="3" class="trTitulo"><div align="center">Instalaci&oacute;n y configuraci&oacute;n de MeiWeb 4.0<img src="imagenes/transparente.gif" width="16" height="16"></div></td>
        			</tr>
					<tr class="trSubTitulo">
          				<td colspan="3" class="trSubTitulo"><img src="imagenes/modulos.gif" width="16" height="16" align="texttop">Configuraci&oacute;n de los M&oacute;dulos del Sistema</td>
              		</tr>
					<tr class="trSubTitulo">
              			<td colspan="3" >Opciones Generales de Configuraci&oacute;n </td>
            		</tr>
                  	<tr valign="top" class="trListaOscuro">
                  	  <td width="334" class="trListaOscuro">Seleccione un tema de visualizaci&oacute;n: </td>
                  	  <td colspan="2" class="trListaOscuro"><select name="cbo_tema" onChange="javascript:refrescar('<?=$PHP_SELF?>');">
                        <?
							
							if(sizeof($listaIndexTema))
							{
							
								foreach($listaIndexTema as $tema)
								{
									list($directorioTema,$nombreTema)=explode(";",$tema);							
																
									if(isset($_POST['cbo_tema']))
									{
										if($_POST['cbo_tema']==$directorioTema)
										{
											$temaSeleccionado="selected";
											$imagenTema="../temas/".$directorioTema."/temaPic.gif";
										}
										else
										{
											$temaSeleccionado="";
										}
										
									}
									else
									{
										if($directorioTema==recuperarVariableSistema("sistematema"))
										{
											$temaSeleccionado="selected";
											$imagenTema="../temas/".$directorioTema."/temaPic.gif";
										}
										else
										{
											$temaSeleccionado="";
										}
									}						
								?>
                        <option value="<?= $directorioTema?>" <?= $temaSeleccionado?>>
                        <?= $nombreTema?>
                        </option>
                        <?
								}	
												
							}
							?>
                      </select></td>
                  	  </tr>
                  	<tr valign="top" class="trListaOscuro">
						<td colspan="3" class="trListaOscuro">					<div align="center"><img src="<?= $imagenTema?>" width="177" height="159"></div></td>
					  </tr>
					<tr class="trListaClaro">
					  <td colspan="2" class="trListaClaro"><img src="imagenes/flecha.gif" width="7" height="7">
					  Tama&ntilde;o m&aacute;ximo permitido en la carga de archivos:</td>
					  <td width="185" class="trListaClaro"><input name="txt_tamanoArchivo" type="text" value="<?= recuperarVariableSistema("sistematamanoarchivo")/1000;?>" size="5"> 
					  KB</td>
					</tr>
            <tr class="trListaOscuro">
              <td colspan="2" class="trListaOscuro"><img src="imagenes/flecha.gif" width="7" height="7">
              N&uacute;mero de intentos de login antes de bloquear un usuario:</td>
              <td class="trListaOscuro"><input name="txt_numerror" type="text" value="<?= recuperarVariableSistema("sistemanumerror");?>" size="5"> 
              Intentos </td>
            </tr>
            <tr class="trListaClaro">
              <td colspan="2" class="trListaClaro"><img src="imagenes/flecha.gif" width="7" height="7">
              Tiempo de penalizaci&oacute;n al no cerrar correctamente una sesi&oacute;n: </td>
              <td class="trListaClaro"><input name="txt_tiePenalizacion" type="text" id="txt_tiePenalizacion" value="<?=recuperarVariableSistema("sistematiempoactivo")/60;?>" size="5">              
              Minutos </td>
            </tr>
            <tr class="trListaOscuro">
              <td colspan="2" class="trListaOscuro"><img src="imagenes/flecha.gif" width="7" height="7">
              Frecuencia de generaci&oacute;n de copias de seguridad autom&aacute;ticas:</td>
              <td class="trListaOscuro"><input name="txt_freCopia" type="text" id="txt_freCopia" value="<?= recuperarVariableSistema("sistematiempocopia");?>" size="5"> 
              D&iacute;as </td>
            </tr>
            <tr class="trSubTitulo">
              <td colspan="3">Opciones de Configuraci&oacute;n por M&oacute;dulos</td>
            </tr>
            <tr class="trInformacion">
              <td colspan="3" class="trInformacion"><input name="chk_cartelera" type="checkbox" value="1" <?= $chkCartelera?> onClick="javascript: activarItems(1);">
              Cartelera</td>
            </tr>
            <tr class="trListaOscuro">
              <td colspan="2" class="trListaOscuro"><img src="imagenes/flecha.gif" width="7" height="7">
              Tama&ntilde;o de la Cartelera:</td>
              <td class="trListaOscuro"><input name="txt_tamanoCartelera" type="text" value="<?= recuperarVariableSistema("sistemaaltocartelera");?>" size="5" <?= $activoCartelera?>>              
              L&iacute;neas</td>
            </tr>
            <tr class="trListaClaro">
              <td colspan="2" class="trListaClaro"><img src="imagenes/flecha.gif" width="7" height="7">
              Velocidad de la visualizaci&oacute;n de mensajes:</td>
              <td class="trListaClaro"><input name="txt_velCartelera" type="text" value="<?= recuperarVariableSistema("sistemavelcartelera");?>" size="5" <?= $activoCartelera?>></td>
            </tr>
            <tr class="trListaOscuro">
              <td colspan="2" class="trListaOscuro"><img src="imagenes/flecha.gif" width="7" height="7">
              Velocidad m&aacute;xima de la visualizaci&oacute;n de mensajes:</td>
              <td class="trListaOscuro"><input name="txt_velMaxCartelera" type="text" value="<?= recuperarVariableSistema("sistemavelmaxcartelera");?>" size="5" <?= $activoCartelera?>></td>
            </tr>
            <tr class="trListaClaro">
              <td colspan="2" class="trListaClaro"><img src="imagenes/flecha.gif" width="7" height="7">
              N&uacute;mero m&aacute;ximo de caracteres en la visualizaci&oacute;n de los mensajes:
              <td class="trListaClaro"><input name="txt_lonCartelera" type="text" value="<?= recuperarVariableSistema("sistemalongmaxcartelera");?>" size="5" <?= $activoCartelera?>>              
              Caracteres</td>
            </tr>
            <tr class="trInformacion">
              <td colspan="3" class="trInformacion"><input name="chk_calendario" type="checkbox" value="1" <?= $chkCalendario?> onClick="javascript: activarItems(2);">
              Calendario</td>
            </tr>
            <tr class="trListaOscuro">
              <td colspan="2" class="trListaOscuro"><img src="imagenes/flecha.gif" width="7" height="7">
              N&uacute;mero m&aacute;ximo de caracteres en la visualizaci&oacute;n de los mensajes:</td>
              <td class="trListaOscuro"><input name="txt_lonCalendario" type="text" value="<?= recuperarVariableSistema("sistematextocalendario");?>" size="5" <?= $activoCalendario?>> 
              Caracteres </td>
            </tr>
            <tr class="trListaClaro">
              <td colspan="2" class="trListaClaro"><img src="imagenes/flecha.gif" width="7" height="7">
              A&ntilde;o inicial en el selector de fechas:</td>
              <td class="trListaClaro"><input name="txt_fechaInicial" type="text" value="<?= recuperarVariableSistema("sistemaagnoinicial");?>" size="5" <?= $activoCalendario?>></td>
            </tr>
            <tr class="trListaOscuro"> 
              <td colspan="2" class="trListaOscuro"><img src="imagenes/flecha.gif" width="7" height="7">
              N&uacute;mero de a&ntilde;os futuros en el selector de fechas:</td>
              <td class="trListaOscuro"><input name="txt_anosFuturos" type="text"  value="<?= recuperarVariableSistema("sistemaagnosfuturo");?>" size="5" <?= $activoCalendario?>> 
              A&ntilde;os </td>
            </tr>
            <tr class="trInformacion">
              <td colspan="3" class="trInformacion"><input name="chk_foro" type="checkbox" value="1" <?= $chkForo ?> onClick="javascript: activarItems(3);">
              Foro</td>
            </tr>
            <tr class="trListaOscuro">
              <td colspan="2" class="trListaOscuro"><img src="imagenes/flecha.gif" width="7" height="7">
              N&uacute;mero de Foros visualizados en la bandeja de entrada:</td>
              <td class="trListaOscuro"><input name="txt_numForo" type="text" value="<?= recuperarVariableSistema("sistemanumforo");?>" size="5" <?= $activoForo?>> 
              Foros </td>
            </tr>
              <td colspan="3" class="trInformacion"><input name="chk_correo" type="checkbox" value="1" <?= $chkCorreo?> onClick="javascript: activarItems(4);">
              Correo</td>
            </tr>
            <tr class="trListaOscuro">
              <td colspan="2" class="trListaOscuro"><img src="imagenes/flecha.gif" width="7" height="7">
              N&uacute;mero de Correos visualizados en la bandeja de entrada:</td>
              <td class="trListaOscuro"><input name="txt_numCorreo" type="text" value="<?= recuperarVariableSistema("sistemanumcorreo ");?>" size="5" <?= $activoCorreo?>> 
              Correos</td>
            </tr>
            <tr class="trInformacion">
              <td colspan="3" class="trInformacion"><input name="chk_chat" type="checkbox" value="1" <?= $chkChat?>>
              Chat</td>
            </tr>
            <tr class="trInformacion">
              <td colspan="3" class="trInformacion"><input name="chk_lanzadores" type="checkbox" value="1" <?= $chkLanzadores?>>
              Lanzadores</td>
            </tr>
			
			<tr class="trInformacion">
			  <td colspan="3" class="trInformacion">
			      <div align="center">
				      <input type="reset" name="Submit" value="Restablecer">
					    <input type="button" name="Submit" value="Valores por defecto" onClick="javascript:enviarInstalar(1)">
			  		    <input name="btn_guardarOpciones" type="button" value="Finalizar" onClick="javascript: enviarInstalar(2);">
		        </div></td></tr>
      </table>
  		</td>
    </tr>
  </table>
</form>
</body>
</html>
<?
	}
	else
	{
		redireccionar("../login/");
	}
?>