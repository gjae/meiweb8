<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../temas/tema2/estilo1024x768.css" rel="stylesheet" type="text/css">
<title>Instalaci&oacute;n y Configuraci&oacute;n MEIWEB</title>

<script language="javascript">

	
	function enviarInstalar()
	{
		if(validarDatos())
		{
			document.frm_restaurar.btn_crearRestauracion.disabled = true;
			document.frm_restaurar.submit();
		}
	}
	
	function enviarCancelar()
	{
		if(confirm("�Est� seguro que desea salir de la instalaci�n de MeiWeb 3.0?"))
		{
			window.close();
		}		
	}
	
	function enviarAtras()
	{
		window.location.replace("conectarInstalacion.php");
	}
	
	function validarDatos()
	{
		if(document.frm_restaurar.fil_archivo.value== false)
		{
			alert("Debe seleccionar la ubicaci�n del archivo de restauraci�n");
		}
		else
		{
			return true;
		}		
		
	}
	
</script>
<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
</style>
</head>

<body>
<table align="center" class="tablaPrincipal">
	<tr valign="top">
		<td>
<form action="cargarRestauracion.php" method="post" name="frm_restaurar" enctype="multipart/form-data">
      <table class="tablaCentral">
        <tr class="trTitulo">
          <td colspan="2"><div align="center">Instalaci&oacute;n y Configuraci&oacute;n de MeiWeb 4.0<img src="imagenes/transparente.gif" width="16" height="16"></div></td>
        </tr>
        <tr class="trSubTitulo">
      		<td class="trSubTitulo"><img src="imagenes/archivo.gif" width="22" height="22" align="texttop"> Carga del Archivo de Recuperaci&oacute;n de Base De datos</td>
    </tr>
	  <?
	  if($_GET['error']== '0x001')
	  {
	  ?>
	  <tr class="trInformacion">
	    <td>&nbsp;</td>
	    </tr>
	  <tr class="trAviso">
          <td class="trAviso Estilo1"><img src="imagenes/error.gif" width="16" height="16" align="texttop">Error: No se pudo cargar el archivo de restauraci&oacute;n. Consulte con el Administrador del sistema el tama&ntilde;o m&aacute;ximo de carga de archivos permitido al servidor e int&eacute;ntelo nuevamente. </td>
    </tr>
	  <?
	  }	
	  else if($_GET['error']== '0x002')
	  {
	  ?>
	  <tr class="trAviso">
          <td class="trAviso Estilo1"><img src="imagenes/error.gif" width="16" height="16" align="texttop">Error: No se pudo cargar el archivo de restauraci&oacute;n. La firma digital del archivo de restauraci&oacute;n no es v&aacute;lida.</td>
    </tr>
	  <?
	  
	  }  
	  ?>
      <tr class="trInformacion">
        <td>&nbsp;</td>
      </tr>
      <tr class="trInformacion">
      <td class="trInformacion">Ubicaci&oacute;n del archivo de restauraci&oacute;n de la Base de Datos. 
        <input type="file" name="fil_archivo"></td>
    </tr>
    <tr class="trInformacion">
      <td>
          <div align="center">
            <input name="restablecer" type="reset" id="restablecer" value="Restablecer">
            <input name="btn_atras" type="button"  value="&lt; Atras" onClick="javascript: enviarAtras();">
            <input name="btn_crearRestauracion" type="button" id="btn_crearRestauracion" onClick="enviarInstalar();" value="Crear Conexi&oacute;n &gt;" <?= $btnSiguiente;?>>
            <input type="button" name="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar();">
	    
	      </div></td>
    </tr>
  </table>
</form>
	  	</td>
       </tr>
     </table>

</body>
</html>
