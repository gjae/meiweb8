<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link rel="stylesheet" href="css/style.css">
<title>Instalación y Configuración de MEIWEB</title>

<script language="javascript">

	function enviarInstalar()
	{
		if(validarDatos())
		{
			document.frm_crearAdministrador.submit();
		}
	}
	
	function enviarCancelar()
	{
		if(confirm("�Est� seguro que desea salir de la instalaci�n de MeiWeb 3.0 ?"))
		{
			window.close();
		}		
	}
		
	function validarDatos()
	{
		if(document.frm_crearAdministrador.txt_administrador.value == false)
		{
			alert("Debe escribir un nombre para el Administrador de MeiWeb 3.0");
			document.frm_crearAdministrador.txt_administrador.value="";
			return false;
		}
		else if(document.frm_crearAdministrador.txt_clave.value == false)
		{
			alert("Debe escribir una contrase�a correcta");
			document.frm_crearAdministrador.txt_clave.value="";
			return false;
		}
		else if(document.frm_crearAdministrador.txt_reclave.value == false)
		{
			alert("Debe escribir una contrase�a correcta");
			document.frm_crearAdministrador.txt_reclave.value="";
			return false;
		}
		else if(document.frm_crearAdministrador.txt_clave.value != document.frm_crearAdministrador.txt_reclave.value)
		{
			alert("Las contrase�as que ha escrito no coinciden. Vuelva a escribir las contrase�as");
			document.frm_crearAdministrador.txt_clave.value="";
			document.frm_crearAdministrador.txt_reclave.value="";
			return false;
		}
		if(document.frm_crearAdministrador.txt_pregunta.value == false)
		{
			alert("Debe escribir una palabra o frase para usarla como pregunta secreta");
			document.frm_crearAdministrador.txt_pregunta.value="";
			return false;
		}
		else if(document.frm_crearAdministrador.txt_respuesta.value == false)
		{
			alert("Debe escribir una palabra o frase para usarla como respuesta a su pregunta secreta");
			document.frm_crearAdministrador.txt_respuesta.value="";
			return false;
		}
		else
		{
			return true;
		}
	}
	
</script>
</head>
<body>
<div class="login-page" style="padding: 2% 0 0;">
  <div class="form">	
  	<img src="../portal/imagenes/logo.png" style="width:180px; height: 40px;"/>
	<img src="../portal/imagenes/UIScom.png" style="width: 80px; height: 40px;"/>
	<br><br>	

	<form  name="frm_crearAdministrador" action="finalizarInstalacion.php" method="post">
     <p><b>Instalación y Configuración de MEIWEB 7.0</b></p><img src="imagenes/transparente.gif" width="16" height="16">
     <br>
       <img src="imagenes/administrador.gif" width="22" height="22" align="texttop"> Creación de los Datos del Administrador<br><br>
       <input placeholder="Nombre Administrador" name="txt_administrador" type="text" id="txt_administrador">
        <input placeholder="Contraseña" name="txt_clave" type="password" id="txt_clave">
       <input placeholder="Re-escriba la contraseña" name="txt_reclave" type="password" id="txt_reclave">
        <input placeholder="Pregunta Secreta" name="txt_pregunta" type="text" id="txt_pregunta">
        <input placeholder="Respuesta Secreta" name="txt_respuesta" type="text" id="txt_respuesta">
       
           <input name="btn_crearConexion" type="button" id="btn_crearConexion" onClick="enviarInstalar();" value="Crear administrador">
            <input type="reset" name="Submit" value="Restablecer">
           <input type="button" name="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar();">
		    
</form>
	  
</div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>
	
</body>
</html>