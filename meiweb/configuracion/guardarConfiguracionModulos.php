<?
	include_once("../baseDatos/BD.class.php");
	include_once("../librerias/estandar.lib.php");
	include_once("../configuracion/InstaladorMEIWEB.class.php");
	
	$instalador=new InstaladorMEIWEB();
	$baseDatos= new BD();
	
	
	if($_GET['opcion']==1)		
	{
		$sql="UPDATE mei_modulo SET mei_modulo.estado='1'";
		$baseDatos->ConsultarBD($sql);
		
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor=mei_configuracion.valordefecto";				
		$baseDatos->ConsultarBD($sql);		
		redireccionar("configurarModulos.php");	
	}
	else if($_GET['opcion']==2)
	{
		 if(empty($_POST['chk_cartelera']))
		{
			$estado=0;
		}
		else
		{
			$estado=1;
		}
			
		$sql="UPDATE mei_modulo SET mei_modulo.estado = '".$estado."' WHERE mei_modulo.nombre ='cartelera' ";
		$baseDatos->ConsultarBD($sql);
			
		if(empty($_POST['chk_calendario']))
		{
			$estado=0;
		}
		else
		{
			$estado=1;
		}
		
		$sql="UPDATE mei_modulo SET mei_modulo.estado = '".$estado."' WHERE mei_modulo.nombre ='calendario' ";
		$baseDatos->ConsultarBD($sql);
			
		if(empty($_POST['chk_foro']))
		{
			$estado=0;
		}
		else
		{
			$estado=1;
		}
		
		$sql="UPDATE mei_modulo SET mei_modulo.estado = '".$estado."' WHERE mei_modulo.nombre ='foro' ";
		$baseDatos->ConsultarBD($sql);
		
		if(empty($_POST['chk_correo']))
		{
			$estado=0;
		}
		else
		{
			$estado=1;
		}
		
		$sql="UPDATE mei_modulo SET mei_modulo.estado = '".$estado."' WHERE mei_modulo.nombre ='correo' ";
		$baseDatos->ConsultarBD($sql);
		
		if(empty($_POST['chk_chat']))
		{
			$estado=0;
		}
		else
		{
			$estado=1;
		}
		
		$sql="UPDATE mei_modulo SET mei_modulo.estado = '".$estado."' WHERE mei_modulo.nombre ='chat' ";
		$baseDatos->ConsultarBD($sql);
		
		if(empty($_POST['chk_lanzadores']))
		{
			$estado=0;
		}
		else
		{
			$estado=1;
		}
		
		$sql="UPDATE mei_modulo SET mei_modulo.estado = '".$estado."' WHERE mei_modulo.nombre ='lanzadores' ";
		$baseDatos->ConsultarBD($sql);
		
		if(empty($_POST['chk_herramientas']))
		{
			$estado=0;
		}
		else
		{
			$estado=1;
		}
		
		$sql="UPDATE mei_modulo SET mei_modulo.estado = '".$estado."' WHERE mei_modulo.nombre ='herramientas' ";
		$baseDatos->ConsultarBD($sql);
		
		/**/
		
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".$_POST['cbo_tema']."' WHERE variable ='sistematema' ";
		$baseDatos->ConsultarBD($sql);
		
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_numCorreo']."' WHERE variable ='sistemanumcorreo' ";
		$baseDatos->ConsultarBD($sql);
		
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_caracUsuario']."' WHERE variable ='sistemalongitudlogin' ";
		$baseDatos->ConsultarBD($sql);
		
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_numForo']."' WHERE variable ='sistemanumforo' ";
		$baseDatos->ConsultarBD($sql);
		
	
		$tamanoArchivo=$_POST['txt_tamanoArchivo']*1000;
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$tamanoArchivo."' WHERE variable ='sistematamanoarchivo' ";
		$baseDatos->ConsultarBD($sql);
			
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_tamanoCartelera']."' WHERE variable ='sistemaaltocartelera' ";
		$baseDatos->ConsultarBD($sql);
			
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_velCartelera']."' WHERE variable ='sistemavelcartelera' ";
		$baseDatos->ConsultarBD($sql);
			
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_velMaxCartelera']."' WHERE variable ='sistemavelmaxcartelera' ";
		$baseDatos->ConsultarBD($sql);
			
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_lonCartelera']."' WHERE variable ='sistemalongmaxcartelera' ";
		$baseDatos->ConsultarBD($sql);
			
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_lonCalendario']."' WHERE variable ='sistematextocalendario' ";
		$baseDatos->ConsultarBD($sql);
	
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_fechaInicial']."' WHERE variable ='sistemaagnoinicial' ";
		$baseDatos->ConsultarBD($sql);
	
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_anosFuturos']."' WHERE variable ='sistemaagnosfuturo' ";
		$baseDatos->ConsultarBD($sql);
			
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_numerror']."' WHERE variable ='sistemanumerror' ";		
		$baseDatos->ConsultarBD($sql);
			
		$tiempoActivo=$_POST['txt_tiePenalizacion']*60;
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$tiempoActivo."' WHERE variable ='sistematiempoactivo' ";		
		$baseDatos->ConsultarBD($sql);
		
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['txt_freCopia']."' WHERE variable ='sistematiempocopia' ";		
		$baseDatos->ConsultarBD($sql);
				
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$_POST['chk_archivo']."' WHERE variable ='sistemagregararchivo' ";		
		$baseDatos->ConsultarBD($sql);
		
		$tamanoHerramienta=$_POST['txt_tamanoHerramienta']*1000;
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$tamanoHerramienta."' WHERE variable ='sistematamanoherramienta' ";
		$baseDatos->ConsultarBD($sql);
		
		$tamanoIcono=$_POST['txt_tamanoIcono']*1000;
		$sql="UPDATE mei_configuracion SET mei_configuracion.valor = '".(int)$tamanoIcono."' WHERE variable ='sistematamanoicono' ";
		$baseDatos->ConsultarBD($sql);
		
		$instalador->CrearConfConfMEIWEB();
		redireccionar("../scripts/indexAdministrador.php");
	}
?>