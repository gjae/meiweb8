<?PHP	session_start();
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  
	$baseDatos=new BD();
	
	if (isset($_SESSION['errorsistema'])) {
		$error=$_SESSION['errorsistema'];
	} else {
		$error=0;
	}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script src="jquery.js" type="text/javascript"></script>
<script language="javascript">

		
	function enviarCancelar()
	{
		location.replace("../login/");	
	}
</script>
<script type="text/javascript">

	
	$(document).ready(function(){
		
	
	  	$('#correoE').blur(function(){
	  		var correo=$(this).val();
	  		
	  			 $(this).css("background","white");
	  			       $('#correo1').css('background','white');
	  			       
	  			       $.ajax({
                           type:"GET",
                           data:"Correo="+correo,
                           url:"peticionPregunta.php",
                           dataType: "json",
                           success: function(data){
                              if(data.Respuesta!="Nulo"){
                                   	$("#pregunta").val(''+data.Respuesta+'');
                                   	$("#buttone").attr("disabled",false);
                               }else{
                               	alert("El usuario con el Correo "+ correo+ " No esta registrado ");
                                    $("#buttone").attr("disabled",true);
								}
                           }
                       });
	  			         
	  		
	  	});
	  		  	
	});
</script>

<style type="text/css">
<!--
.Estilo5 {
	color: #FFFFFF;
	font-weight: bold;
}
-->
</style>
</head>
<body>

	<table width="756" align="center" class="tablaPrincipal">
		<tr>
			<td>
             
			<table width="756" border="0" align="center" class="tablaLogin">
      <tr>
        <td width="98">&nbsp;</td>
        <td width="456">&nbsp;</td>
        <td width="188">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><div align="center"><span class="Estilo5">Si desea que el sistema le envi&eacute; sus datos de la nueva contrase&ntilde;a por favor ingrese su direccion de correo con el que se registro en el sistema</span></div></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="center"><table width="300" class="tablaRegistro">
            <form action="generarDatos.php" method="post" name="frm_generar" id="frm_generar">
              <tr>
              	<td align="center"><b>Correo Electronico</b></td>
                <td width="" >
                	<div align="center">
                    <input class="link" type="text" id="correoE" name="txt_correo" />
                </div>
                </td>
              </tr>
              <tr>
              	<td align="center"> <b>Pregunta Secreta</b></td>
                <td width="" ><div align="center">
                    <input class="link" type="text" id="pregunta" name="txt_pregunta" disabled="true"/>
                </div>
                </td>
              </tr>
              <tr>
              	<td align="center"><b>Respuesta Secreta</b></td>
                <td width="" ><div align="center">
                    <input class="link" type="password" name="txt_respuesta" />
                </div>
                </td>
              </tr>
              <tr>
              	<td></td>
                <td><div align="center">
                    <table width="100%"  border="0">
                      <tr>
                        <td>
                        	<div align="right"></div>  
                        	 <div align="center">
                              <input type="submit" name="button" id="buttone" value="Enviar" disabled="true"/>
                             <input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onclick="javascript: enviarCancelar();" />
                        </div>
                        </td>
                        </tr>
                    </table>
                </div></td>
              </tr>
              <tr>
                              </tr>
            </form>
        </table></td>
        <td align="right" valign="middle"><div align="left"></div></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>   <?PHP
	 if($error == 1)
	{
	
	?>
	<script language="javascript">
			  alert("No existe usuario con este Correo ");
	 </script>
     <?PHP }?>
     </td>
        <td><?PHP
	 if($error == 2)
	{
			$_SESSION['errorsistema']=0;
	?>
	<script language="javascript">
			  alert("Sus datos fueron enviados a su correo  ");
	 </script>
     <?PHP }?></td>
        <td><?PHP
	 if($error == 3)
	{
	
	?>
	<script language="javascript">
			  alert("No coincide la respuesta");
	 </script>
     <?PHP }?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
				
		  </td>
		</tr>
</table>
    
</body>
</html>
