<?PHP	session_start();
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	
	
if(isset($_GET["entrar"]) && $_GET["entrar"]==1)
{

	if(isset($_SERVER['errorsistema']))
	{	
		$mensaje=mostrarError($_SESSION['errorsistema']);	
		$error=$_SESSION['errorsistema'];	
	} else { $error = 0; $mensaje=""; }


?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<title>MEIWEB</title>
	<script Language='JavaScript'>
            <!-- Esconde esto de navegadores antiguos
              if (window != window.top)
                 top.location.href = location.href;
            // --> 
			
			function borrarFormulario()
			{
				document.frm_registro.reset();			
				document.frm_registro.txt_idUsuario.value="";
			}
			
			function display_error_ip()
			{
				alert("Ha iniciado sesion en un equipo diferente");
			}
			
	</script> 
	
    <link rel="stylesheet" type="text/css" href="tecvir/keyboard.css" />
 
    <script type="text/javascript" src="tecvir/keyboard.js" charset="UTF-8"></script> 
 <script type="text/javascript" src="tecvir/kb.js" charset="UTF-8"></script> 
</head>

<body>
<table width="756" border="0" align="center" class="tablaLogin">
  <tr>
    <td width="62">&nbsp;</td>
    <td width="370">&nbsp;</td>
    <td width="310">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td  width="300">&nbsp;</td>
    <td>
    	<table width="100%" class="tablaRegistro">
      <form name="frm_registro" method="post" action="loginRegistro.php">
        <tr>
          <td width="145">Digite su Login</td>
          <td width="213"><input type="password" class="link keyboardInput" lang="es" name="txt_idUsuario" value="" title="su login registrado"></td>
        </tr>
        <tr>
          <td>Digite su Contrase&ntilde;a</td>
          <td><input type="password" class="link keyboardInput" lang="es" name="txt_clave" title="Contrase&ntilde;a registrada"></td>
        </tr>
        <tr>
          <td colspan="2"><center>
                    </center>            
            <table width="187" border="0" align="center">
            <tr>
              <td width="84">
                <div align="right">
                  <input type="submit" name="sub_registro" value="Ingresar" >            
                    </div></td>
              <td width="93"><div align="center">
                <input name="btn_reset" type="button" id="btn_reset" value="Restablecer" onClick="javascript: borrarFormulario();">
              </div></td>
            </tr>
          </table></td>
          </tr>
        <tr>
          <td colspan="2"><?PHP echo $mensaje; ?>
          </td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2"><table width="358" border="0" class="tablaRegistro">
            <tr>
              <td width="46">&nbsp;</td>
              <td width="302">&nbsp;
			  <?PHP
			  	if($error == 3)
				{
			  ?>
			  	 <a href="../login/desbloquearUsuario.php" class="linkLogin">Desbloquear Usuario</a> 
			  <?PHP
			  }
			  ?>
				</td>
              </tr>
          </table></td>
          </tr>
        <tr>
          <td colspan="2"><table width="357" border="0" class="tablaRegistro">
		  <?PHP
		  if($error != 3)
		  {
		  ?>
              <tr>
                <td width="76">&nbsp;</td>
                <td width="271"><a href="../login/recordarClave.php" title="Recordar Contrase&ntilde;a" class="linkLogin">&iquest;Ha olvidado su Contrase&ntilde;a?</a> </td>
              </tr> 
		<?PHP
			}
		?>
            </table></td>
          </tr>
      </form>
    </table></td>
    <td valign="bottom"><table class="tablaRegistro">
           <tr>
          <td colspan="2">&nbsp; </td>
        </tr>
        <tr>
          <td colspan="2">&nbsp; </td>
        </tr>
       </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<?PHP
	if($error == 5)
	{
?>
	<script language="javascript">
		alert("Actualmente no se encuentra registrado en ningn grupo. Solicite al administrador de MeiWeb su registro en un grupo.");
	</script>
<?PHP
	}
	else if($error == 3)
	{
	
	?>
	<script language="javascript">
			  alert("Demasiados intentos de ingresos fallidos a MeiWeb. Su cuenta ha sido bloqueada por seguridad.");
	 </script>
	 <?PHP
	}
	else if($error == 6)
	{
	
	?>
	<script language="javascript">
			  alert("Se ha desbloqueado satisfactoriamente el usuario. Puede ingresar normalmente a MeiWeb.");
	 </script>
	 <?PHP
	}
else if($error == 22)
	{
	
	?>
	<script language="javascript">
			  alert("Sus datos fueron enviados a su correo");
	 </script>
	 <?PHP
	}
?>
</body>
</html>
<?PHP
	if(isset($_GET['error_ip']))
	{
	?>
	<script>
    	display_error_ip();
    </script>
	<?PHP
	}
}
else
{
	redireccionar('../portal/');
}
?>




