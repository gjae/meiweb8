<?PHP	session_start();
	 include_once("../librerias/estandar.lib.php");
	 include_once("../configuracion/InstaladorMEIWEB.class.php");
	 
	$instalador=new InstaladorMEIWEB();

	 if(!empty($_POST['txt_respuesta']))
	 {
	 	if(file_exists("../configuracion/archivos/admin.conf"))
		{
			$cadenaAdmin=file_get_contents("../configuracion/archivos/admin.conf");
			list($administrador,$pregunta,$respuesta,$clave)=explode(";",$cadenaAdmin);
			
			if(md5(sha1($_POST['txt_respuesta']))==$respuesta && $_POST['txt_admin']==base64_decode($administrador))
			{
				session_unset("errorAdminClave");
				
				$instalador->CrearAdminConfMEIWEB($_POST['txt_admin'],$_POST['hid_pregunta'],$_POST['txt_respuesta'],$_POST['txt_confirmacion']);
				
				redireccionar("loginAdmin.php");
			}
			else
			{
				if(isset($_SERVER["errorAdminClave"]))
				{
					$_SESSION['errorAdminClave']="0x001";
				}
				else
				{
					$_SESSION['errorAdminClave']="0x001";					
				}
				
				redireccionar("recordarClaveAdmin.php");
			}
				
		}
		else
		{
			print "Error, el archivo de configuracion de administracion no existe";
		}
		
	 }
	 else
	 {
	 		print "Salir del sistema";
	 }

?>