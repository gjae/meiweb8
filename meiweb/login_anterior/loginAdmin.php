<?PHP session_start();
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<title>MEIWEB</title>

<script language="javascript">

	function enviarRegistro()
	{
		if(validarDatos())
		{
			document.frm_ingresoAdmin.submit();
		}
	}
	
	function validarDatos()
	{
		if(document.frm_ingresoAdmin.txt_clave.value ==false || document.frm_ingresoAdmin.txt_admin.value ==false) 
		{
			alert("Datos Incorrectos");
			return false;			
		}
		else
		{
			return true;
		}
	}
	
	function enviarCancelar()
	{
		location.replace("../login/");	
	}



</script>

</head>

<body>
<table width="756" border="0" align="center" class="tablaLogin">
  <tr>
    <td width="58">&nbsp;</td>
    <td width="367">&nbsp;</td>
    <td width="317">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>      <form action="loginRegistroAdmin.php" method="post" name="frm_ingresoAdmin" id="frm_ingresoAdmin">
      <table width="100%" class="tablaRegistro">
        <tr>
          <td width="51%">Administrador</td>
          <td width="49%"><input type="text" class="link" name="txt_admin"></td>
        </tr>
        <tr>
          <td>Digite su Contrase&ntilde;a</td>
          <td><input name="txt_clave" type="password" id="txt_clave"></td>
        </tr>
        <tr>
          <td colspan="2"><div align="center">              <table width="100%" border="0">
                <tr>
                  <td><div align="right">
                    <input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">
                  </div></td>
                  <td><div align="center">
                    <input name="btn_ingresar" type="button" id="btn_ingresar" value="Ingresar" onClick="javascript: enviarRegistro();">
                  </div></td>
                  <td><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar();"></td>
                </tr>
              </table>
</div></td>
        </tr>
        <tr>
          <td colspan="2"><a href="recordarClaveAdmin.php" class="linkLogin" title="Recordar Contraseña">&iquest;Ha olvidado su contrase&ntilde;a?</a></td>
        </tr>
        <?PHP
			  if($_SESSION['errorAdminClave']=="0x001")
			  {
			?>
        <tr>
          <td colspan="2">Datos Incorrectos</td>
        </tr>
        <?PHP
			  }
			?>
      </table>
          </form>    </td>
    <td valign="bottom">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
