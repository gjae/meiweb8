<?PHP	session_start();
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  
	$baseDatos=new BD();

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script language="javascript">

	function enviarRegistro()
	{
		if(validarDatos())
		{
			document.frm_recordar.submit();
		}
	}
	
	function validarDatos()
	{
		if(document.frm_recordar.txt_respuesta.value ==false ||  document.frm_recordar.txt_nuevaClave.value ==false || document.frm_recordar.txt_confirmacion.value ==false) 
		{
			alert("Es necesario que llene completamente la información solicitada");
			return false;			
		}
		else
		{
			if(document.frm_recordar.txt_nuevaClave.value == document.frm_recordar.txt_confirmacion.value)
				return true;
			else
			{
			alert("La contraseña y su confirmación no son y guales");
			return false;			
			}
			
		}
	}
	
	function enviarAceptar()
	{
		if(document.frm_recordar.txt_idUsuario.value !=false || document.frm_recordar.txt_alias.value !=false)
		{
			document.frm_recordar.submit();
		}
		else
		{
			alert("Es necesario que llene completamente la información solicitada");
		}
	}
	
	function enviarCancelar()
	{
		location.replace("../login/");	
	}
	function enviarDatos(idmateria)
	{
		location.replace("../login/enviarContrasena.php");	
	}

</script>

</head>
<body>
<?PHP	
if(isset($_GET['idUsuario']))
{	
	if(isset($_SERVER['errorsistema']))
		$mensajeError=mostrarError($_SESSION['errorsistema']);
		
					
	destruirSession();	
	$sql="SELECT mei_usuario.alias, mei_usuario.pregunta FROM mei_usuario WHERE mei_usuario.login='".$_GET['idUsuario']."'";
	$consultar=$baseDatos->ConsultarBD($sql);
	list($alias,$pregunta)=mysql_fetch_array($consultar);	
?>	
	<table width="756" align="center" class="tablaPrincipal">
		<tr>
			<td>
                <table width="756" border="0" align="center" class="tablaLogin">
                  <tr>
                    <td width="105">&nbsp;</td>
                    <td width="381">&nbsp;</td>
                    <td width="256">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
				  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td><table width="100%" class="tablaRegistro">
                      <form name="frm_recordar" method="post" action="loginRecordar.php">
                        <tr>
                          <td width="45%">Nombre de usuario</td>
                          <td width="55%"><?PHP echo $_GET['idUsuario']; ?>
                              <input type="hidden" id="txt_idUsuario" name="txt_idUsuario" value="<?PHP echo $_GET['idUsuario']; ?>"></td>
                        </tr>
                        <tr>
                          <td>Pregunta Secreta </td>
                          <td><?PHP echo $pregunta; ?>
                              <input type="hidden" id="txt_pregunta" name="txt_pregunta" value="<?PHP echo $pregunta; ?>"></td>
                        </tr>
                        <tr >
                          <td>Respuesta Secreta </td>
                          <td><input class="link" id="txt_respuesta" name="txt_respuesta" type="password"></td>
                        </tr>
                        <tr>
                          <td>Nueva Contrase&ntilde;a </td>
                          <td><input class="link" id="txt_nuevaClave" name="txt_nuevaClave" type="password"></td>
                        </tr>
                        <tr>
                          <td>Confirmar Contrase&ntilde;a</td>
                          <td><input class="link" id="txt_confirmacion" name="txt_confirmacion" type="password"></td>
                        </tr>
                        <tr>
                          <td colspan="2"><div align="center">
                              <table width="100%"  border="0">
                                <tr>
                                  <td width="36%"><div align="right">
                                    <input name="btn_ingresar" type="button" value="Cambiar Contrase&ntilde;a" onClick="javascript: enviarRegistro();">
                                  </div></td>
                                  <td width="35%"><input type="reset" name="Submit2" value="Restablecer"></td>
                                  <td width="29%"><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar();"></td>
                                </tr>
                              </table>
                          </div></td>
                        </tr>
                      </form>
                    </table></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="bottom">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td valign="bottom">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
              <?PHP
}
else 
{
	if(isset($_SERVER['errorsistema'])) {
		$mensajeError=mostrarError($_SESSION['errorsistema']);		
	} else { $mensajeError=""; }
	destruirSession();
?>
				<table width="756" border="0" align="center" class="tablaLogin">
      <tr>
        <td width="69">&nbsp;</td>
        <td width="374">&nbsp;</td>
        <td width="299">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="center">
        	<table width="100%" class="tablaRegistro">
          <form name="frm_recordar" method="post" action="loginRecordar.php">
            <tr>
              <td width="30%"  align="center">Login:</td>
              <td width="70%" align="left">
              	<input class="link" type="password"  name="txt_idUsuario"></td>
              </tr>
            <tr>
              <td></td>
              <td></td>
              </tr>
            <tr>
              <td colspan="2" align="center">
              	<div align="center">
                  <table width="100%"  border="0">
                    <tr>
                      <td  align="center">
                      	<div align="center">
                        <input type="submit" align="center"  name="sub_recordar" value="Aceptar">
                      </div>
                      </td>
                      <td ><div align="center">
                        <input type="reset" name="Submit" align="center" value="Restablecer">
                      </div></td>
                      <td >
                      	<input name="btn_cancelar" align="center" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar();">
                      	</td>
                    </tr>
                  </table>
              </div>
              </td>
            </tr>
            <tr>
              <td colspan="2"><?PHP echo $mensajeError; ?></td>
            </tr>
          </form>
        </table></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="250">&nbsp;</td>
        <td >
        	<div align="center">
            <input  type="button" name="recordarDatos" id="recordarDatos" value="Recordar Datos" onClick="javascript: enviarDatos( <?PHP print $idmateria=1; ?>)" >
            </div>
            </td>
        <td>
        	
          </td> 
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
				
		  </td>
		</tr>
</table>
    <?PHP		
}
?>
</body>
</html>
