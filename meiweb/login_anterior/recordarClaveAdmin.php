<?PHP
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');
	
	session_start();
	$baseDatos=new BD();
	
	if(file_exists("../configuracion/archivos/admin.conf"))
	{
		$cadenaAdmin=file_get_contents("../configuracion/archivos/admin.conf");
		list($administrador,$pregunta,$respuesta,$clave)=explode(";",$cadenaAdmin);
		$preguntaSecreta=$baseDatos->DecodificarClaveBD($pregunta);
	}
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script language="javascript">

	function enviarRegistro()
	{
		if(validarDatos())
		{
			document.frm_recordarAdmin.submit();
		}
	}
	
	function validarDatos()
	{
		if(document.frm_recordarAdmin.txt_respuesta.value ==false || document.frm_recordarAdmin.txt_admin.value ==false || document.frm_recordarAdmin.txt_nuevaClave.value ==false || document.frm_recordarAdmin.txt_confirmacion.value ==false) 
		{
			alert("Es necesario que llene completamente la información solicitada");
			return false;			
		}
		else
		{
			if(document.frm_recordarAdmin.txt_nuevaClave.value == document.frm_recordarAdmin.txt_confirmacion.value)
				return true;
			else
			{
			alert("La contraseña y su confirmación no son y guales");
			return false;			
			}
			
		}
	}
	
	function enviarCancelar()
	{
		location.replace("../login/");	
	}


</script>



</head>

<body>
<table width="756" border="0" align="center" class="tablaLogin">
  <tr>
    <td width="62">&nbsp;</td>
    <td width="393">&nbsp;</td>
    <td width="287">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
	<form action="loginRecordarAdmin.php" method="post" name="frm_recordarAdmin" id="frm_recordarAdmin">
	  <table width="100%" class="tablaRegistro">
        <tr>
          <td>Administrador</td>
          <td><input type="text" class="link" name="txt_admin"></td>
        </tr>
        <tr>
          <td>Pregunta Secreta </td>
          <td><?PHP echo $preguntaSecreta?>
              <input type="hidden" name="hid_pregunta" value="<?PHP echo $preguntaSecreta?>"></td>
        </tr>
        <tr >
          <td>Respuesta Secreta </td>
          <td><input class="link" name="txt_respuesta" type="text"></td>
        </tr>
        <tr>
          <td>Nueva Clave </td>
          <td><input class="link" name="txt_nuevaClave" type="password"></td>
        </tr>
        <tr>
          <td>Confirmar Contrase&ntilde;a </td>
          <td><input class="link" name="txt_confirmacion" type="password"></td>
        </tr>
        <?PHP
				if($_SESSION['errorAdminClave']=="0x001")
				{
				?>
        <?PHP
				}
				?>
        <tr>
          <td colspan="2"><div align="center">              <table width="100%"  border="0">
                <tr>
                  <td width="41%"><div align="right">
                    <input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">                    
                  </div></td>
                  <td width="24%"><div align="center">
                    <input name="btn_ingresar" type="button" id="btn_ingresar" value="Recordar" onClick="javascript: enviarRegistro();">
                  </div></td>
                  <td width="35%"><input name="btn_reset" type="button" id="btn_reset" value="Cancelar" onClick="javascript: enviarCancelar();"></td>
                </tr>
              </table>
</div></td>
        </tr>
      </table>
      </form></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td valign="bottom">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
