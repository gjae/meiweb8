<?
	include_once("../editor/fckeditor.php");
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{
		$baseDatos= new BD();	
	
		$editor=new FCKeditor('edt_descripcionSalaChat' , '100%' , '200' , 'barraCartelera' , '' ) ;
?>

		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
		<script language="javascript">
			function enviarCancelar()
			{
				location.replace("../chat/index.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>");		
			}
			
			function chk_habilitar()
			{
				for(i=0;i<document.frm_salaChat.elements.length;i++)
				{
					if(document.frm_salaChat.elements[i].id=="grupo")
					{
						document.frm_salaChat.elements[i].disabled=false;
					}
				}
			}
			
			function chk_deshabilitar()
			{
				for(i=0;i<document.frm_salaChat.elements.length;i++)
				{
					if(document.frm_salaChat.elements[i].id=="grupo")
					{
						document.frm_salaChat.elements[i].checked=true;
					}
				}
			}
			 
			function chk_click ()
			{
				var radioTodos=document.getElementById('radioT');
				var radioSelec=document.getElementById('radioS');
				radioTodos.checked=false;
				radioSelec.checked=true;
			}
			 
			function enviar()
			{
				var contBandera=0;
				for(i=0;i<document.frm_salaChat.elements.length;i++)
				{
					if(document.frm_salaChat.elements[i].checked==true && document.frm_salaChat.elements[i].id=="grupo")
					{
						contBandera++;
					}
				}
			
				if(contBandera == 0)
				{
					alert('Debe seleccionar por lo menos un grupo destino');
				}
				else if(document.frm_salaChat.txt_salaChat.value == false)
				{
					alert('Debe llenar completamente la información solicitada');
				}
				else 
				{
				document.frm_salaChat.submit(); 
				}
			}
		</script>
				<style type="text/css">
				<!--
				.Estilo1 {color: #FF0000}
				-->
				</style>
		
		</head>
		<body>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td valign="top" class="tdGeneral">
					<form action="insertarSalaChat.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" name="frm_salaChat" method="post" >
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td colspan="4"><img src="imagenes/chat.gif" width="16" height="16" align="texttop"> Crear Sala de Chat</td>
						</tr>
<?
			if($_GET['error'] == '0x001')
			{
?>
								<tr class="trAviso">
									<td colspan="4"><img src="imagenes/error.gif" width="17" height="18" align="texttop"><span class="Estilo1"> Error: Debe escribir la descripción de la Sala de Chat</span></td>
								</tr>
<?
			}
?>
						
						<tr class="trInformacion">
							<td width="25%"><div align="left"><b>Sala : </b></div></td>
							<td width="75%" colspan="3"><b><input name="txt_salaChat" type="text" id="txt_salaChat" size="50" value="<?=$_GET['titulo']?>"></b></td>
						</tr>						
						<tr class="trInformacion">
						  <td colspan="4" valign="middle"><b>Descripci&oacute;n:</b></td>
						  </tr>
						<tr class="trInformacion">
							<td colspan="4" valign="middle"><? $editor->crearEditor();?></td>
						</tr>
						<tr class="trInformacion">
						  <td colspan="4"><b>
						  <input name="chk_cartelera" type="checkbox" id="chk_cartelera" value="checkbox" checked>
						  Publicar en Cartelera</b></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4">
                            	<table width="100%" bgcolor="#DDDDDD" class="tablaPrincipal">
                                  <tr>
                                    <td colspan="7"><b>Destino:</b> </td>
                                  </tr>
                                  <tr>
                                    <td width="1%">&nbsp;</td>
                                    <td width="3%"><input name="rad_grupo" type="radio" value="general"></td>
<?                        
			if ($_SESSION['idtipousuario']==5)
			{
?>				
                              <td width="25%">Todos los Grupos Virtuales</td>
<?                        
			}
			else
			{
?>	
                              <td width="25%">Todos los Grupos Presenciales</td>
<?                        
			}
?>
                                    <td width="3%"><input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar()" checked id="radioT"></td>
                                    <td width="21%">Todos mis Grupos </td>
                                    <td width="3%"><input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()" id="radioS"></td>
                                    <td width="44%">Grupos Seleccionados</td>
                                  </tr>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>
                                    	<table class="tablaPrincipal">
<?
			$contChk=0;
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria 
						WHERE mei_virmateria.idvirmateria IN (SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo 
						WHERE mei_virgrupo.idvirgrupo IN ( SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
						WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."))
						GROUP BY (mei_virmateria.idvirmateria)"; 
			}
			else
			{
				$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia 
						WHERE mei_materia.idmateria IN (SELECT mei_grupo.idmateria FROM mei_grupo 
						WHERE mei_grupo.idgrupo IN ( SELECT mei_relusugru.idgrupo FROM mei_relusugru 
						WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."))
						GROUP BY (mei_materia.idmateria)"; 
			}
			$materias=$baseDatos->ConsultarBD($sql);
							
			while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
			{
?>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="3"><b><?= $materiaNombre?></b></td>
                                    </tr>
<?
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
						IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].") 
						AND mei_virgrupo.idvirmateria =".$materiaCodigo;
				}
				else
				{
					$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo 
						IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].") 
						AND mei_grupo.idmateria =".$materiaCodigo;
				}
				$grupos=$baseDatos->ConsultarBD($sql);
				
				while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
				{
?>
                                    <tr>
                                      <td width="26">&nbsp;</td>
                                      <td width="26">&nbsp;</td>
                                      <td width="33"><input id="grupo" name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>" checked></td>
                                      <td width="280"><?= $grupoNombre?></td>
                                    </tr>
<?
					$contChk++;
				}
			}  
?>
                                		</table>
									</td>
                              	</tr>
	                            </table>
                            </td>
						</tr>
						<tr class="trInformacion">
								<td colspan="4"><div align="center">
								  <table border="0">
                                    <tr>
                                      <td width="25"><input name="hid_contGrupo" type="hidden" value="<?= $contChk?>"></td>
                                      <td width="89"><div align="right">
                                          <input name="btn_agregar" type="button" value="Crear Sala" onClick="javascript:enviar()">
                                      </div></td>
                                      <td width="141"><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript:enviarCancelar()"></td>
                                    </tr>
                                  </table>
							    </div></td>
						</tr>
					</table>
					</form>
				</td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
                </body>
		</html>
<?
	}
	else
		redireccionar('../login/');
?>
