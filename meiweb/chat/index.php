<?
	include_once ('../menu1/Menu1.class.php');	
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../librerias/vistas.lib.php');
	$baseDatos= new BD();
	
	if(comprobarSession())
	{
	
	registrarBitacora(6,24,false);
	
	$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=6";
	$resultado=$baseDatos->ConsultarBD($sql);
	list($estado)=mysql_fetch_array($resultado);
	
	if($estado==1)
	{
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
		<script language="javascript">
		
			function eliminarMensaje(idSalaChat)
			{
				if(confirm("¿Está seguro de eliminar esta Sala de Chat?"))
				{
					location.replace("eliminarSalaChat.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idSalaChat="+idSalaChat);					
				}
			}
			
			function iniciarVentanaChat(idUsuario,idSalaChat)
			{
				if (typeof ventanaChat.document == 'object')
				{
					ventanaChat.close()
				}
				ventanaChat = window.open("ventanaChat.php?idSalaChat="+idSalaChat,"ventanaSalaChat"+idUsuario,"width=820,height=580,left=150,top=100,scrollbars=NO,menubars=NO,status=NO,statusbar=NO,resizable=NO,location=NO")
			}
			var ventanaChat=false
		</script>
		</head>
		<body>
		
<?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
		{
			$sql="SELECT mei_salachat.idsalachat, mei_salachat.nombresala, mei_salachat.descripcion, mei_salachat.idusuario, mei_salachat.fechacreacion
				FROM mei_salachat WHERE mei_salachat.idsalachat
							IN (
								SELECT mei_relsalvirgru.idsalachat
									FROM mei_relsalvirgru WHERE mei_relsalvirgru.idvirgrupo
										IN (
											SELECT mei_relusuvirgru.idvirgrupo
												FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario = '".$_SESSION['idusuario']."'
											)
								)";
		}
		else
		{
			$sql="SELECT mei_salachat.idsalachat, mei_salachat.nombresala, mei_salachat.descripcion, mei_salachat.idusuario, mei_salachat.fechacreacion
				FROM mei_salachat WHERE mei_salachat.idsalachat
							IN (
								SELECT mei_relsalgru.idsalachat
									FROM mei_relsalgru WHERE mei_relsalgru.idgrupo
										IN (
											SELECT mei_relusugru.idgrupo
												FROM mei_relusugru WHERE mei_relusugru.idusuario = '".$_SESSION['idusuario']."'
											)
								)";
		}
		$consulta=$baseDatos->ConsultarBD($sql);			
			
		$contRegistro=0;		
		while(list($idSalaChat,$nombreSala,$descripcion,$autor,$fechacreacion)=mysql_fetch_array($consulta))
		{
			$sql="SELECT count( idusuario )FROM mei_relsalusu WHERE mei_relsalusu.idsalachat =".$idSalaChat;
			list($numeroUsuariosSala)=mysql_fetch_array($baseDatos->ConsultarBD($sql));		
			
			$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario = '".$autor."'";
			$datosAutor=$baseDatos->ConsultarBD($sql);						
			
			list($nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($datosAutor);			
						
			$descripcion=stripslashes(base64_decode($descripcion));
			
			$listaMensajes[$contRegistro]=$idSalaChat.'[$$$]'.$nombreSala.'[$$$]'.$descripcion.'[$$$]'.$autor.'[$$$]'.$nombre1.'[$$$]'.$nombre2.'[$$$]'.$apellido1.'[$$$]'.$apellido2.'[$$$]'.$fechacreacion.'[$$$]'.$numeroUsuariosSala;
			$contRegistro++;
		} 
						
		
			?>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td><img src="imagenes/chat.gif" width="16" height="16" border="0" align="texttop"> Salas de Chat </td>
							<? if (empty($_GET['idmateria'])) {?>
								<td colspan="2" align="right"> <a href="../scripts/homeUsuario.php" class="link"> Volver  </td>..
						<?	} else {?>
							<td colspan="2" align="right"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"> Volver  </td>
							<?}?>
						</tr>
					</table>
				
					<?
		if(!empty($listaMensajes))
		{
			
					
		foreach($listaMensajes as $datos)
		{
			 
				list($idSalaChat,$nombresala,$descripcion,$autor,$nombre1,$nombre2,$apellido1,$apellido2,$fechacreacion,$numeroUsuariosSala)=explode("[$$$]",$datos);
				
				if($numeroUsuariosSala == 1)
				{
					$cadenaNumUsuarios=$numeroUsuariosSala." Usuario activo";
				}
				else
				{
					$cadenaNumUsuarios=$numeroUsuariosSala." Usuarios Activos";
				}
				
		?>&nbsp;
					<table class="tablaGeneral" >
                    &nbsp;
						<tr class="trSubTitulo">
							<td colspan="2"> <a href="javascript:iniciarVentanaChat(<?= $_SESSION['idusuario']?>,<?= $idSalaChat?>)" class="link">
							&nbsp;Sala de Chat: <?= $nombresala?></a></td>
					  	  	<td colspan="2"><div align="right">Hay 
				  	  	      <?= $cadenaNumUsuarios?></div></td>
						</tr>
						<tr class="trInformacion">
                             <td width="25%"><b>Autor:</b></td>
                              <td width="25%"><?= $nombre1.' '.$nombre2.' '.$apellido1.' '.$apellido2?></td>
                              <td width="25%"><div align="right"><b>Fecha de Creación:</b></div></td>
                              <td width="25%"><div align="right"></div>
                                <div align="right"><?= mostrarFecha($fechacreacion,false);?>
                              </div></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4"><strong>Descripci&oacute;n:</strong></td>
						</tr>
						<tr class="trDescripcion">
						  	<td colspan="4"  class="trDescripcion"><?= $descripcion?></td>
						</tr>
						<tr class="trInformacion">
						  	<td colspan="4">
							<?
								if($_SESSION['idusuario']==$autor && ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5))
								{			
							?>
									<table class="tablaPrincipal" align="center">
										<tr>
										  <td><div align="center"><a href="javascript:iniciarVentanaChat(<?= $_SESSION['idusuario']?>,<?= $idSalaChat?>)"><img src="imagenes/chat.gif" alt="Ingresar a la Sala de Chat" width="16" height="16" border="0"></a></div></td>
											<td><div align="center"><a href="modificarSalaChat.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idSalaChat=<?= $idSalaChat?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Mensaje" width="16" height="16" border="0"></a></div></td>
											<td><div align="center"><a href="javascript: eliminarMensaje(<?= $idSalaChat?>)"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Mensaje" width="16" height="16" border="0"></a>
										      </div>
										    </div></td>
										</tr>
										<tr>
										  <td><div align="center"><a href="javascript:iniciarVentanaChat(<?= $_SESSION['idusuario']?>,<?= $idSalaChat?>)" class="link">Ingresar</a></div></td>
												<td><div align="center"><a href="modificarSalaChat.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idSalaChat=<?= $idSalaChat?>" class="link">Modificar</a></div></td>
												 <td><div align="center"><a href="javascript: eliminarMensaje(<?= $idSalaChat?>)"  class="link">Eliminar</a></div></td>
										</tr>
									</table>		
							<?
								}
								else
								{
							?>
									<table class="tablaPrincipal" align="center">
										<tr>
										  <td><div align="center"><a href="javascript:iniciarVentanaChat(<?= $_SESSION['idusuario']?>,<?= $idSalaChat?>)"><img src="imagenes/chat.gif" width="16" height="16" border="0"></a></div></td>
										</tr>
										<tr>
										  <td><div align="center"><a href="javascript:iniciarVentanaChat(<?= $_SESSION['idusuario']?>,<?= $idSalaChat?>)" class="link">Ingresar</a></div></td>
										</tr>
									</table>		
							<?
								}
							?>
						  	</td>
						</tr>
	<?
		}
		?>
						
				  </table>
			
		<?
		}
		else
		{
			?>
			<table class="tablaGeneral">
				<tr class="trAviso">
				<td>Actualmente no existen Salas de Chat</td>
				</tr>
			</table>
			<?
		}#end if
	?>
    </td>
	<td class="tablaEspacio">&nbsp;</td>
	<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
	<td class="tablaEspacioDerecho">&nbsp;</td>
    </tr>
 </table>
</body>
</html>
<?
}
else
{
	redireccionar('../scripts/');
	}
}
else
{
	redireccionar('../login/');
}
	
?>
