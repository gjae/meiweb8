<?
	session_start();
	include_once('../baseDatos/BD.class.php');
	include_once('../librerias/estandar.lib.php');
	
	$basedatos= new BD();
	
	$sql="SELECT mei_usuario.primernombre, mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario=".$_GET['idUsuarioDestino'];
	$consulta=$basedatos->ConsultarBD($sql);
	
	list($primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($consulta);
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<title><?= $primerNombre." ".$segundoNombre." ".$primerApellido." ".$segundoApellido?></title>
<script type="text/javascript" src="../editor/fckeditor.js"></script>
<script type="text/javascript">

	function ventanaSelectorColor(formulario,variableColor)
	{
		if (typeof ventanaColor.document == 'object')
		{
				ventanaColor.close()
		}
		ventanaColor = window.open("ventanaSelectorColor.php?frm="+formulario+"&vColor="+variableColor,"ventanaColor","width=200,height=180,left=300,top=300,scrollbars=NO,menubars=NO,status=NO,statusbar=NO,resizable=NO,location=NO");
	}
	var ventanaColor=false

	function limpiarEditor()
	{
		var oEditor = FCKeditorAPI.GetInstance('edt_mensajeChat') ;
		oEditor.Commands.GetCommand('NewPage').Execute() ;
	}

	function enviarMensaje()
	{
		document.frm_privadoChat.submit();
		limpiarEditor();
		
		if(document.frm_privadoChat.chk_cerrarAutomatico.checked)
		{
			window.close();
		}
	}	
</script>
<style type="text/css">
<!--
.Estilo1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
-->
</style>
</head>
<body marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">

<form name="frm_privadoChat" action="ventanaMensajeChat.php" target="ventanaMensajeChat<?= $_SESSION['idusuario']?>" method="post">
  <table width="100%" cellpadding="0" cellspacing="0" bordercolor="#999999" bgcolor="#DDDDDD" class="tablaPrincipal">
  <tr>
    <td bordercolor="#DDDDDD" bgcolor="#CCCCCC">&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
      <tr>
        <td width="3%">&nbsp;</td>
        <td width="97%" bordercolor="#DDDDDD" class="tablaPrincipal">Enviar Mensaje Privado a: <b><?= $primerNombre." ".$segundoNombre." ".$primerApellido." ".$segundoApellido?></b></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>
<script language="javascript">
		var oFCKeditor = new FCKeditor('edt_mensajeChat','100%','150','barraChat');	
		oFCKeditor.Create();
</script></td>
  </tr>
  <tr>
    <td><table width="100%" align="center">
      <tr>
        <td width="3%"  align="center" valign="middle" bordercolor="#CCCCCC" background="imagenes/fondoBoton.gif"><a href="javascript:ventanaSelectorColor('frm_privadoChat','hid_colorNombre');"><img src="imagenes/colorNombre.png" alt="Selector de color de Usuario" width="21" height="21" border="0" /></a></td>
        <td width="69%"><div align="left" title="<?= $titulo?>">
            <input name="chk_cerrarAutomatico" type="checkbox" value="checkbox" checked>
            <span class="Estilo1">Cerrar Ventana al Enviar un Mensaje</span>            
            <input type="hidden" value="1" name="hid_guardar">
            <input type="hidden" value="<?= $_GET['idUsuarioDestino']?>" name="hid_tipoMensaje">
            <input type="hidden" value="#FF0000" name="hid_colorNombre">
            <input type="hidden" value="<?= $_GET['idSalaChat']?>" name="hid_idSalaChat">
        </div></td>
        <td width="14%" ><div align="center">
            <input name="btn_borrarMensaje" type="submit" value="Borrar Mensaje" onClick="javascript:limpiarEditor();">
        </div></td>
        <td width="14%" ><div align="center">
            <input name="btn_enviarMEnsaje" type="button" value="Enviar Mensaje" onClick="javascript:enviarMensaje();">
        </div></td>
      </tr>
    </table></td>
  </tr>
</table>
</form>
</body>
</html>