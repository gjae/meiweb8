<?
	include_once ('../menu1/Menu1.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	
	if(comprobarSession())
	{
		$baseDatos=new BD();
			
		if(!empty($_GET['idSalaChat']))
		{
			$sql="SELECT mei_salachat.nombresala, mei_salachat.descripcion , mei_salachat.cartelera , mei_salachat.destino FROM mei_salachat 
						WHERE mei_salachat.idsalachat =".$_GET['idSalaChat'];
		
			$consulta=$baseDatos->ConsultarBD($sql);
			list($nombreSala,$descripcion,$cartelera,$destino)= mysql_fetch_array($consulta);
			$descripcion=stripslashes(base64_decode($descripcion));
			
			
			if($destino==0)
			{
				$destinoTodos="checked";
				$gruposCheck="checked";
			}
			else if($destino==1)
			{
				$destinoSeleccionados="checked";
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					$sql="SELECT mei_relsalvirgru.idvirgrupo FROM mei_relsalvirgru WHERE mei_relsalvirgru.idsalachat =".$_GET['idSalaChat'];
				else
					$sql="SELECT mei_relsalgru.idgrupo FROM mei_relsalgru WHERE mei_relsalgru.idsalachat =".$_GET['idSalaChat'];
				$consulta=$baseDatos->ConsultarBD($sql);
				
				$cont=0;
				
				while(list($datoGrupo)=mysql_fetch_array($consulta))
				{
					$listaGrupos[$cont++]=$datoGrupo;
				}					
							
			}
			else if($destino==2)
			{
				$destinoGeneral="checked";
				$gruposCheck="checked";
			}
			
			
			
			if($cartelera==1)
			{
				$estadoCartelera="checked";
			}
			else
			{
				$estadoCartelera='';
			}
		
		?>
		
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
		
		<script language="javascript">
			function enviarCancelar()
			{
				location.replace("../chat/index.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>");
			}
			function chk_habilitar()
			{
				for(i=0;i<document.frm_salaChat.elements.length;i++)
				{
					if(document.frm_salaChat.elements[i].id=="grupo")
						document.frm_salaChat.elements[i].disabled=false;
				}
			}
			
			function chk_deshabilitar()
			{
				for(i=0;i<document.frm_salaChat.elements.length;i++)
				{
					if(document.frm_salaChat.elements[i].id=="grupo")
						document.frm_salaChat.elements[i].checked=true;
				}			
			}
			 
			function chk_click ()
			{
				var radioTodos=document.getElementById('radioT');
				var radioSelec=document.getElementById('radioS');			
				radioTodos.checked=false;
				radioSelec.checked=true;			
			}
			 
			function enviar()
			{
				var contBandera=0;
				for(i=0;i<document.frm_salaChat.elements.length;i++)
				{
					if(document.frm_salaChat.elements[i].checked==true && document.frm_salaChat.elements[i].id=="grupo")
					contBandera++;
				}

				if(contBandera == 0)
				{
					alert('Debe seleccionar por lo menos un grupo destino');
				}
				else if(document.frm_salaChat.txt_salaChat.value == false)
				{
					alert('Debe llenar completamente la información solicitada');
				}
				else 
				{
					document.frm_salaChat.submit(); 
				}
			}
		
		</script>
		<style type="text/css">
<!--
.Estilo1 {
	color: #FF0000;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
-->
        </style>
		</head>
		<body>
		<?		
			
			$editor=new FCKeditor('edt_descripcionSalaChat' , '100%' , '200' , 'barraCartelera' , $descripcion) ;
		?>
		
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td valign="top" class="tdGeneral">
					<form action="actualizarSalaChat.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" name="frm_salaChat" method="post" >
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td colspan="4" class="trTitulo"><img src="imagenes/chat.gif" width="16" height="16" align="texttop"> Modificar Sala de Chat </td>
						</tr>
						<?
							if($_GET['error'] == '0x001')
							{
						?>
								<tr class="trAviso">
									<td colspan="4"><img src="imagenes/error.gif" width="17" height="18" align="texttop"><span class="Estilo1"> Error: Debe escribir la descripción de la Sala de Chat</span></td>
								</tr>
						<?
							}
						?>
					
						<tr class="trInformacion">
							<td width="15%"><div align="left"><b>Titulo :</b></div></td>
							<td width="35%"><b><input name="txt_salaChat" type="text" id="txt_salaChat" value="<?= $nombreSala?>" size="50">
							</b></td>
							<td width="15%">&nbsp;</td>
							<td width="35%">&nbsp; </td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4"><? $editor->crearEditor();?></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4"><p align="left"><input name="chk_cartelera" type="checkbox" id="chk_cartelera" value="checkbox" <?= $estadoCartelera?>>
							Publicar este Mensaje en Cartelera</p></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4">
                            <table width="100%" border="0" bgcolor="#DDDDDD" class="tablaPrincipal">
                              <tr>
                                <td colspan="6"><b>Destino:</b> </td>
                              </tr>
                              <tr>
                                <td width="3%"><input name="rad_grupo" type="radio" value="general" <?= $destinoGeneral?>></td>
<?                        
			if ($_SESSION['idtipousuario']==5)
			{
?>				
                              <td width="25%">Todos los Grupos Virtuales</td>
<?                        
			}
			else
			{
?>	
                              <td width="25%">Todos los Grupos Presenciales</td>
<?                        
			}
?>
                                <td width="3%"><input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar()" <?= $destinoTodos?> id="radioT"></td>
                                <td width="22%">Todos mis Grupos </td>
                                <td width="3%"><input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()" id="radioS" <?= $destinoSeleccionados?>></td>
                                <td width="44%">Grupos Seleccionados </td>
                              </tr>
                              <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><table class="tablaPrincipal">
                                  <?
				  
			$contChk=0;
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
		{
			$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria 
						WHERE mei_virmateria.idvirmateria 
							IN (
								SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo 
										WHERE mei_virgrupo.idvirgrupo 
											IN (
												SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
														WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
												)
								) GROUP BY (mei_virmateria.idvirmateria)"; 
		}
		else
		{
			$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia 
						WHERE mei_materia.idmateria 
							IN (
								SELECT mei_grupo.idmateria FROM mei_grupo 
										WHERE mei_grupo.idgrupo 
											IN (
												SELECT mei_relusugru.idgrupo FROM mei_relusugru 
														WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
												)
								) GROUP BY (mei_materia.idmateria)"; 
		}
			$materias=$baseDatos->ConsultarBD($sql);
				
			while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
			{
		?>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><b><?= $materiaNombre?></b></td>
                                  </tr>
                                  <?
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo 
							WHERE mei_virgrupo.idvirgrupo 
								IN (
									SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
											WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
									) AND mei_virgrupo.idvirmateria =".$materiaCodigo;
			}
			else
			{
				$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo 
							WHERE mei_grupo.idgrupo 
								IN (
									SELECT mei_relusugru.idgrupo FROM mei_relusugru 
											WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
									) AND mei_grupo.idmateria =".$materiaCodigo;
			}
				
				$grupos=$baseDatos->ConsultarBD($sql);
				
				while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
				{
				
					if(sizeof($listaGrupos) > 0)
					{
						if(in_array($grupoCodigo,$listaGrupos))
							{
								$checkedGrupo="checked";
							}
							else
							{
								$checkedGrupo=" ";
							}
					}
		?>
                                  <tr>
                                    <td width="26">&nbsp;</td>
                                    <td width="26">&nbsp;</td>
                                    <td width="33"><input id="grupo" name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>" <?= $gruposCheck?> <?= $checkedGrupo?>></td>
                                    <td width="281"><?= $grupoNombre?></td>
                                  </tr>
                                  <?
					$contChk++;
				}
			}  
				  
		?>
                                </table></td>
                              </tr>
                            </table></td>
					  </tr>
						<tr class="trInformacion">
						<td colspan="4"><div align="center">
						  <table border="0">
                            <tr>
                              <td width="25"><input type="hidden" name="hid_idSalaChat" value="<?= $_GET['idSalaChat']?>">
                                  <input type="hidden" name="hid_contGrupo" value="<?= $contChk?>"></td>
                              <td width="89"><div align="right">
                                  <input name="btn_actualizar" type="button" value="Modificar Sala" onClick="javascript:enviar()">
                              </div></td>
                              <td width="141"><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript:enviarCancelar()"></td>
                            </tr>
                          </table>
						  </div></td>
						</tr>
					</table>
				  </form>
				</td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
                </body>
	</html>
	<?
	}
	else
	{
		redireccionar("../chat/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
	}
	
	}
else
	redireccionar('../login/');
?>
