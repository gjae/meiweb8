<?

	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');
	
	if(comprobarSession())
	{
	
		$baseDatos=new BD();
	
		if(!empty($_POST['txt_salaChat']))
		{	
			if($_POST['rad_grupo']=="todos")
			{
				$destino=0;			
			}
			else if($_POST['rad_grupo']=="seleccionados")
			{
				$destino=1;
			}
			else if($_POST['rad_grupo']=="general")
			{
				$destino=2;
			}
			
			
			
			if(!comprobarEditor($_POST['edt_descripcionSalaChat']))
			{
					redireccionar("agregarSalaChat.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&error=0x001&titulo=".$_POST['txt_salaChat']);
			}
			else
			{
			
				if(empty($_POST['chk_cartelera']))
				{
					$cartelera=0;
				}
				else
				{
					$cartelera=1;
				}
				
				registrarBitacora(6,25,false);
				
				$sql="INSERT INTO mei_salachat ( idsalachat , nombresala , descripcion , fechacreacion , estado , idusuario , cartelera , destino ) 
						VALUES ('', '".$_POST['txt_salaChat']."', '".base64_encode($_POST['edt_descripcionSalaChat'])."', '".date('Y-n-j')."', '0', '".$_SESSION['idusuario']."', '".$cartelera."', ".$destino.")";
				$baseDatos->ConsultarBD($sql);					
				$idChat=$baseDatos->InsertIdBD();
				
				
				
				if($_POST['rad_grupo'] =="general")
				{
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
						$sql="SELECT DISTINCT mei_virgrupo.idvirgrupo FROM mei_virgrupo";
					else
						$sql="SELECT DISTINCT mei_grupo.idgrupo FROM mei_grupo";
					$consultaGrupo=$baseDatos->ConsultarBD($sql);	
						
					while(list($idGrupo)=mysql_fetch_array($consultaGrupo))						
					{
						if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
							$sql="INSERT INTO mei_relsalvirgru ( idsalachat , idvirgrupo ) VALUES ('".$idChat."','".$idGrupo."')";
						else
							$sql="INSERT INTO mei_relsalgru ( idsalachat , idgrupo ) VALUES ('".$idChat."','".$idGrupo."')";
						$baseDatos->ConsultarBD($sql);						
					}
				}
				else
				{
					$valores='';					
					
					for($i=0;$i<$_POST['hid_contGrupo'];$i++)
					{	
						if(!empty($_POST['chk_idgrupo'.$i]))
						{
							if(empty($valores))
							{
								$valores="('".$idChat."', '".$_POST['chk_idgrupo'.$i]."')"; 
							}
							else
							{
								$valores.=" , ('".$idChat."', '".$_POST['chk_idgrupo'.$i]."')"; 	
							}
						}					
					}
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
						$sql="INSERT INTO `mei_relsalvirgru` ( `idsalachat` , `idvirgrupo` ) VALUES ".$valores;
					else
						$sql="INSERT INTO `mei_relsalgru` ( `idsalachat` , `idgrupo` ) VALUES ".$valores;
				}
			}
		}
		$baseDatos->ConsultarBD($sql);
		redireccionar("../chat/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
	
	}
	else
		redireccionar('../login/');
?>