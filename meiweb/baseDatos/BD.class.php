<? 
class BD
{
	var $m_baseDatos;
	var $m_host;
	var $m_usuario;
	var $m_clave;
	var $m_coneccion;
	var $m_error;
	var $m_archivoSQL;
	var $m_tablasSQL;
	var $m_alterSQL;
	var $m_dirCopias;
	var $m_nombreArchivoCifrado;
	var $m_tipoCopia;
	var $m_tamanoCopia;
	var $m_nombreCifrado;
	var $m_nombreArchivoMCSBD;
	
	function BD()
	{
		$this->m_coneccion = false;	
		$this->m_tablasSQL="bdTablasMEIWEB.sql";
		$this->m_alterSQL="bdAlterMEIWEB.sql";
		$this->m_dirCopias="../../datosMEIWEB/archivosCopias/";
		$this->m_nombreArchivoMCSBD="../configuracion/archivos/mcsBD.conf";
		
			
		if($this->CrearDatosBD())
		{
			$this->ConectarBD();
		}
	}
	
	function ComprobarBD($a_host,$a_usuario,$a_clave,$a_nombreBaseDatos)
	{
		$conexxion = mysql_connect($a_host,$a_usuario,$a_clave,true);
		mysql_set_charset('UTF-8',$conexxion);
		if(empty($conexxion))
		{
			$this->m_error="0x0001";	
		}
		else
		{
			if(mysql_select_db($a_nombreBaseDatos,$conexxion))
				{
					return true;
				}
				else
				{
					$this->m_error="0x0002";
				}
			mysql_close($conexxion);
		}
	}
	
	function ConectarBD()
	{
		$this->m_coneccion = mysql_connect($this->m_host, $this->m_usuario, $this->m_clave) 
			or die("Imposible abrir la base de datos"); 
						
		 mysql_select_db($this->m_baseDatos, $this->m_coneccion)
		  	or die("Imposible abrir la base de datos");		
	}
	
	function DatosHostConexionBD()
	{
		return 	$this->m_host;
	}	
	function DatosUsuarioConexionBD()
	{
		return 	$this->m_usuario;
	}
	function DatosNombreDBConexionBD()
	{
		return 	$this->m_baseDatos;
	}
	 
	 function ConsultarBD($a_sql)
	 {	
	 	$consulta=mysql_query($a_sql,$this->m_coneccion);
 		//or die ("No se pudo hacer la consulta: ".$a_sql." - ".mysql_error() );
		return $consulta;
	 }	
	 
	 function InsertIdBD()
	 {	
	 	$id=mysql_insert_id($this->m_coneccion)
			or die ("No se pudo hacer la consulta: $sql");
		return $id;
	 }
	 
	 function CodificarClaveBD($a_cadena)
	 {	
	 	return base64_encode($a_cadena);
	 }
	 
	 function DecodificarClaveBD($a_cadena)
	 {	
	 	return base64_decode($a_cadena);
	 }	 
	 
	 function CrearDatosBD()
	 {	
		if(file_exists("../configuracion/archivos/BD.conf"))
		{
				$datosBD=$this->DecodificarClaveBD(file_get_contents("../configuracion/archivos/BD.conf"));
				list($this->m_baseDatos,$this->m_usuario,$this->m_clave,$this->m_host)=explode(";",$datosBD);				
			
				$this->m_baseDatos=trim($this->m_baseDatos);
				$this->m_usuario=trim($this->m_usuario);
				$this->m_clave=$this->DecodificarClaveBD(trim($this->m_clave));
				$this->m_host=trim($this->m_host);	

				return true;
					}
		else
		{
			if(file_exists("../configuracion/archivos/control.conf"))
			{
				print ("Falta el archivo de configuracion BD.conf");
			}
		}
	 }
	 
	 function CargarSqlBD($a_hostBD,$a_usuarioBD,$a_claveBDCodif,$a_nombreBD,$a_mcs) 
	 {
	 	$conexxion=@mysql_connect($a_hostBD,$a_usuarioBD,$this->DecodificarClaveBD($a_claveBDCodif));
		
		if(!empty($conexxion))
		{  				  
			if(mysql_select_db($a_nombreBD, $conexxion))
			{
				
				if(file_exists("../configuracion/archivos/".$this->m_archivoSQL) && file_exists("../configuracion/archivos/".$this->m_tablasSQL) && file_exists("../configuracion/archivos/".$this->m_alterSQL))
					{
						
						$bdTablasMEIWEB=file_get_contents("archivos/".$this->m_tablasSQL);
						$arrayTablasSql=explode(";",$bdTablasMEIWEB);
						
						foreach($arrayTablasSql as $sql)
						{
							mysql_query(stripslashes($sql),$conexxion);
						}
						
						$bdMEIWEB=file_get_contents("archivos/".$this->m_archivoSQL);
						
						if($a_mcs)
						{
							list($arraySqlCifrado,$firma)=explode(";",$bdMEIWEB);							
																							
							$arraySql=$this->DecodificarClaveBD($arraySqlCifrado);
							$firmaMCS=md5(sha1($arraySql));
							

							$arraySql=trim($arraySql);
							$arraySql=explode(";",$arraySql);
						
						}						
						else
						{
							$arraySql=explode(";",$bdMEIWEB);
						}						
						
						if($firma == $firmaMCS)							
						{
							foreach($arraySql as $sql)
							{
								mysql_query($sql,$conexxion);
							}
						}
						else
						{
							return false;
						}
						
						$bdAlterMEIWEB=file_get_contents("archivos/".$this->m_alterSQL);
						$arrayAlterSql=explode(";",$bdAlterMEIWEB);
						
						foreach($arrayAlterSql as $sql)
						{
							mysql_query($sql,$conexxion);
						}
						
						// Base de datos creada exitosamente
						return true;
					}
					else
					{
						// Imposible Abrir el archivo de la base de datos 
						return false;
					}
				
			}
			else
			{
				// Imposible realizar una conexion con el seridor de MYSQL
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function NumeroDias($a_fecha1,$fecha2)
	{
		list($agno1,$mes1,$dia1)=explode("-",$a_fecha1);
		list($agno2,$mes2,$dia2)=explode("-",$fecha2);
				
		$numeroDias=360*($agno2-$agno1)+30*($mes2-$mes1)+($dia2-$dia1);		
		
		return abs($numeroDias);
	}
	
	function CentinelaCopiasBD()
	{
		if(recuperarVariableSistema("sistemacopiaautomatica")== 1)
		{
			if(file_exists($this->m_nombreArchivoMCSBD))
			{
				$fechaMCSBD=file_get_contents($this->m_nombreArchivoMCSBD);
				
				if((int)$this->NumeroDias($fechaMCSBD,date("y-m-d")) >= (int)recuperarVariableSistema("sistematiempocopia"))
				{
					$this->CrearCopiaBD(true);
				}		
			}
			else
			{
				$this->CrearCopiaBD(true);
			}		
		}
	}
	
	function CrearCopiaBD($a_automatica)
	{
		if($a_automatica == false)
		{
			$this->m_tipoCopia=0;
		}
		else
		{
			$this->m_tipoCopia=1;
		}
		
		$this->CrearArchivoCifrado($this->CrearSQLCifrado($this->CrearListaDatosSQL()));
		$this->CrearRegistroCopia();
		$this->CrearMCSBD();		
	}
	
	function CrearMCSBD()
	{
		if($this->m_tipoCopia ==1)
		{
			$archivoMCSBD=fopen($this->m_nombreArchivoMCSBD,"w+");
				
			fputs($archivoMCSBD,date("y-m-d"));
		
			fclose($archivoMCSBD);
		}
	}
	
	function CrearArchivoCifrado($a_cadenaCifrada)
	{
		if(!file_exists("../../datosMEIWEB/"))
		{
			mkdir("../../datosMEIWEB/");
			mkdir("../../datosMEIWEB/archivosCopias");	
		}
		else if(!file_exists("../../datosMEIWEB/archivosCopias"))
		{
			mkdir("../../datosMEIWEB/archivosCopias");	
		}
		
		
		$this->m_fechaCopia=date("y-m-d--H.i.s");		
		$this->m_nombreCifrado="MEIWEB".$this->m_fechaCopia.".mcs";
		$this->m_nombreArchivoCifrado=$this->m_dirCopias.$this->m_nombreCifrado;
			
		$archivoCifrado=fopen($this->m_nombreArchivoCifrado,"w+");
	
		fputs($archivoCifrado,$a_cadenaCifrada);
			
		fclose($archivoCifrado);
		
		$this->m_tamanoCopia=filesize($this->m_nombreArchivoCifrado);		
		
	}
	
	function CrearRegistroCopia()
	{
		$sql="INSERT INTO mei_copiaseguridad ( idcopiaseguridad , fecha , nombre , tamano , tipo , copia ) 
				VALUES 
					('', '".$this->m_fechaCopia."', '".$this->m_nombreCifrado."' , '".$this->m_tamanoCopia."', '".$this->m_tipoCopia."', '".$this->m_nombreArchivoCifrado."')";
					
		$this->ConsultarBD($sql);
		


	}
	
	function CrearSQLCifrado($a_cadenaSQL)
	{
				
		if(empty($a_cadenaSQL))
		{
			print "No se ha encontrado elementos SQL para cifrar";
			return false;
		}
		else
		{
			$firma=md5(sha1($a_cadenaSQL));	
			$archivoCifrado=base64_encode($a_cadenaSQL);
			$archivoMCS=$archivoCifrado.";".$firma;			
			return $archivoMCS;
		}	
	}
	
	function CrearListaDatosSQL()
	{
		$tablaBitacora="mei_bitacora";
		$tablaChat="mei_chat";
		
		$listaTablas=mysql_list_tables($this->m_baseDatos,$this->m_coneccion);
		
				
		while(list($nombreTabla)=mysql_fetch_array($listaTablas))
		{
						
			if(recuperarVariableSistema("sistemanobitacora")== 1)
			{
				if($nombreTabla != $tablaBitacora && $nombreTabla != $tablaChat)
				{
					$listaDatosSQL.=$this->CrearInsertSQL($nombreTabla);
				}				
			}
			else
			{
				$listaDatosSQL.=$this->CrearInsertSQL($nombreTabla);
			}
		}		
				
		return $listaDatosSQL;
	}
	
	
	function CrearInsertSQL($a_nombreTabla)
	{
		$sql="SELECT * FROM ".$a_nombreTabla;
		
			$datosTabla=$this->ConsultarBD($sql);
			$numColumnas=mysql_num_fields($datosTabla);
			
			while($datosRegistro=mysql_fetch_array($datosTabla))
			{
				$sqlRegistro="INSERT INTO `".$a_nombreTabla."` VALUES (";	
				$listaDatos=false;
							
				for($i=0;$i<$numColumnas;$i++)
				{
					if(empty($listaDatos))
					{
						$listaDatos="'".$datosRegistro[$i]."'";
					}
					else
					{
						$listaDatos.=",'".$datosRegistro[$i]."'";
					}
				}
				
				$sqlRegistro.=$listaDatos.");";

				$listaDatosSQL.=$sqlRegistro;				
			}
			
		return	$listaDatosSQL;
	}
	
	
}
	
?>