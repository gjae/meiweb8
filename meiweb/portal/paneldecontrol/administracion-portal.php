<?
require_once "includes/header.php";
?>

<main>
	<section id="wrapper">
		<section id="content">
			<section id="administracion-portal">
				<h1>Registrar Nuevo Administrador</h1>
				<form action="acciones/registrar-administrador.php" method="post" enctype="multipart/form-data">
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td>Nombre:</td>
							<td><input type="text" name="adm-nombre" required /></td>
						</tr>
						<tr>
							<td>Email:</td>
							<td><input type="email" name="adm-email" required /></td>
						</tr>
						<tr>
							<td>Usuario:</td>
							<td><input type="text" name="adm-user" required /></td>
						</tr>
						<tr>
							<td>Contraseña:</td>
							<td><input type="password" name="adm-pass" required maxlength="15"/></td>
						</tr>
						<tr>
							<td colspan="2" align="right"><input type="submit" value="Registrar Administrador" /></td>
						</tr>
					</table>
				</form>
			</section> <!-- administracion-portal -->
			<section id="administracion-portal-cd">
				<h1>Administradores Registrados</h1>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>Nombre</td>
						<td>Foto de Perfil</td>
						<td>Email</td>
						<!--<td>Usuario</td>-->
						<td>Artículos Publicados</td>
					</tr>
					<?
					$SQL = mysql_query("SELECT * FROM administradores");
					while($lh = mysql_fetch_array($SQL)){
						$idAdm = $lh['id'];
					$SQL_COUNT_NT = mysql_query("SELECT * FROM noticias WHERE id_autor='$idAdm'");
					$SQL_COUNT = mysql_num_rows($SQL_COUNT_NT);
					?>
					<tr>
						<td><? echo $lh['nombre']; ?></td>
						<td><img src="imagenes/perfil/<? echo $lh['imgperfil']; ?>" width="70" height="80" /></td>
						<td><? echo $lh['email']; ?></td>
						<!--<td><? echo $lh['usuario']; ?></td>-->
						<td><? echo $SQL_COUNT; ?></td>
					</tr>
					<?
					}
					?>
				</table>
			</section> <!-- administracion-portal-cd -->
		</section> <!-- content -->
	</section> <!-- wrapper -->
</main>
<?
require_once "includes/footer.php";
?>