<?php 

require_once "includes/header.php";

?>
<link rel="stylesheet" type="text/css" href="css/default.css" media="screen" />
<script type="text/javascript" src="js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/default.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		height: "900",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/content.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Style formats
		style_formats : [
			{title : 'Bold text', inline : 'b'},
			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			{title : 'Example 1', inline : 'span', classes : 'example1'},
			{title : 'Example 2', inline : 'span', classes : 'example2'},
			{title : 'Table styles'},
			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
		],

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>

	<main>
		<section id="wrapper">
			
			<section id="content">
			
			<section id="submenu">
				<?
				$SQL_TN=mysql_query("SELECT status FROM noticias WHERE status=1");
				$SQL_BO=mysql_query("SELECT status FROM noticias WHERE status=0");
				$SQL_PA=mysql_query("SELECT status FROM noticias WHERE status=2");
				
				$CNT_TN = mysql_num_rows($SQL_TN);
				$CNT_BO = mysql_num_rows($SQL_BO);
				$CNT_PA = mysql_num_rows($SQL_PA);
				
				?>
				<ul>
					<li id="publicar-noticia-menu" class="activeColor">Publicar noticia</li>
					<li id="todas-noticias-menu">Ver todas las noticias <?if($CNT_TN!=0):echo"($CNT_TN)"; endif;?></li>
					<li id="borradores-menu" >Borradores<?if($CNT_BO!=0):echo"($CNT_BO)"; endif;?></li>
					<li id="papelera-menu">Papelera <?if($CNT_PA!=0):echo"($CNT_PA)"; endif;?></li>
				</ul>
			</section>
			
			<section id="publicar-noticia">
					
					<?
						if(!isset($_GET["id_nt_ed"])){
							
					?>
			<form action="acciones/publicar-noticia.php" method="POST" enctype="multipart/form-data">
				<section id="publicar-noticia-left">
				<table>
					<tbody>
						<tr>
							<td><input type="text" name="titulo-noticia" placeholder="Coloque el título aquí" /></td>
						</tr>
						<tr>
							<td><textarea name="contenido-noticia" id="maxW"></textarea></td>
						</tr>
					</tbody>	
				</table>		
				</section>
					<section id="publicar-noticia-right">
						
						<section id="publicar-nota-informaciones">
							<table>
								<tbody>
									<tr>
										<td align="left"><input type="submit" name="publicar" value="Publicar Noticia" /></td>
										<td align="right"><input type="submit" name="guardar-borrador" value="Guardar Noticia" /></td>
									</tr>
									<tr>
										<td>Fecha de Publicación:</td>
										<td align="right"><?php echo date("d/m/Y"); ?></td>
									</tr>
									<tr>
										<td>Autor:</td>
										<td align="right"><?php echo $nombreusuario; ?></td>
									</tr>
								</tbody>
							</table>
						</section>
						
						<section id="publicar-nota-informaciones">
							<table>
								<tbody>
									<tr>
										<td>Tags de Búsqueda</td>
									</tr>
									<tr>
										<td><input type="text" name="tags-busqueda" placeholder="Digite sus tags de búsqueda" /></td>
									</tr>
									<tr>
										<td id="tags-escogidos"></td>
									</tr>
								</tbody>
							</table>
						</section>
						
							<section id="publicar-nota-informaciones">
							<table>
								<tbody>
									<tr>
										<td>Imagen de la Noticia</td>
									</tr>
									<tr>
										<td>
											<input type="text" hidden id="imagen-noticia-cargar" placeholder="Seleccione la imagen de su noticia" />
											<input type="file" id="imagen-cargada" name="imagen-cargada" /></td>
									</tr>
								</tbody>
							</table>				
						</section>
						
						<section id="publicar-nota-informaciones">
							<table>
								<tbody>
									<tr>
										<td>Categoría de la Noticia</td>
									</tr>
									<tr>
										<td>
											<select name="categoria-noticia">
												<option	selected="selected" value="">Selecione una Categoria</option>
           									<?
           									$SQL_C=mysql_query("SELECT * FROM categoria");
											while($ct=mysql_fetch_array($SQL_C)){
												
											
           									?>
           									<option value="<?echo $ct['id_categoria'];?>"><?echo $ct['nombre_categoria'];?></option>
           									<?}?>
           									</select>
										</td>
									</tr>
								</tbody>
							</table>				
						</section>
						
					</section>
					</form>
					<?
						}
						else{
							$id_nt_ed = $_GET["id_nt_ed"];
							$SQL_SL = mysql_query("SELECT * FROM noticias WHERE id_noticia=$id_nt_ed");
							while($ln = mysql_fetch_array($SQL_SL)){
								$id_nt = $ln["id_noticia"];
								$titulo_ntE = $ln["titulo"];
								$contenido_ntE = $ln["contenido"];
								$dataPub_ntE = $ln["dataPub"];
								$autorPub_ntE = $ln["autorPub"];
								$tagSear_ntE = $ln["tags"];
								$imagen_ntE = $ln["imagen"];
								$ct_se = $ln['categoria']; 
							}
					?>
			<form action="acciones/actualizar-noticia.php?id_nt_up=<? echo $id_nt; ?>" method="POST" enctype="multipart/form-data">
				<section id="publicar-noticia-left">
				<table>
					<tbody>
						<tr>
							<td><input type="text" name="titulo-noticia" value="<? echo $titulo_ntE; ?>" /></td>
						</tr>
						<tr>
							<td><textarea name="contenido-noticia" id="maxW"><? echo $contenido_ntE; ?></textarea></td>
						</tr>
					</tbody>	
				</table>		
				</section>
					<section id="publicar-noticia-right">
						
						<section id="publicar-nota-informaciones">
							<table>
								<tbody>
									<tr>
										<td align="left"><input type="submit" name="publicar" value="Publicar Noticia" /></td>
										<td align="right"><input type="submit" name="guardar-borrador" value="Guardar Noticia" /></td>
									</tr>
									<tr>
										<td>Fecha de Publicación:</td>
										<td align="right"><?php echo $dataPub_ntE; ?></td>
									</tr>
									<tr>
										<td>Autor:</td>
										<td align="right"><?php echo $autorPub_ntE; ?></td>
									</tr>
								</tbody>
							</table>
						</section>
						
						<section id="publicar-nota-informaciones">
							<table>
								<tbody>
									<tr>
										<td>Tags de Búsqueda</td>
									</tr>
									<tr>
										<td><input type="text" name="tags-busqueda" value="<? echo $tagSear_ntE; ?>" /></td>
									</tr>
									<tr>
										<td id="tags-escogidos"></td>
									</tr>
								</tbody>
							</table>
						</section>
						
							<section id="publicar-nota-informaciones">
							<table>
								<tbody>
									<tr>
										<td>Imagen de la Noticia</td>
									</tr>
									<tr>
										<td>
											<input type="text" id="imagen-noticia-cargar" value="<? echo $imagen_ntE ?>"/>
											<input type="file" hidden id="imagen-cargada" name="imagen-cargada" value="<? echo $imagen_ntE ?>" /></td>
									</tr>
								</tbody>
							</table>				
						</section>
						
						<section id="publicar-nota-informaciones">
							<table>
								<tbody>
									<tr>
										<td>Categoría de la Noticia</td>
									</tr>
									<tr>
										<td>
											<select name="categoria-noticia">
											<?
											$SQL_SEL = mysql_query("SELECT nombre_categoria FROM categoria WHERE id_categoria=$ct_se");
											while($ctsel=mysql_fetch_array($SQL_SEL)){
												$cts = $ctsel['nombre_categoria'];
												$idcts = $ctsel['id_categoria'];
											}
											?>
												<option	selected="selected" value="<? echo $idcts; ?>"><? echo $cts; ?></option>
           									<?
           									$SQL_C=mysql_query("SELECT * FROM categoria");
											while($ct=mysql_fetch_array($SQL_C)){
												$cat = $ct['nombre_categoria'];
												if(strcmp($cat, $cts)!==0){
											
           									?>
           									<option value="<?echo $ct['id_categoria'];?>"><?echo $ct['nombre_categoria'];?></option>
           									<?
												}
           									}
           									?>
           									</select>
										</td>
									</tr>
								</tbody>
							</table>				
						</section>
						
					</section>
				 </form>
					<?
						}
					?>
			</section> <!-- Publicar Noticia -->
			
			<section id="todas-las-noticias">
				<table cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td>Título de la Noticia</td>
							<td>Autor</td>
							<td>Categorías</td>
							<td>Tags</td>
							<td>Fecha</td>
							<td>Editar</td>
							<td>Excluír</td>
						</tr>
						<?
							$SQL_TDN = mysql_query("SELECT id_noticia, titulo, dataPub, autorPub, tags, nombre_categoria FROM noticias INNER JOIN categoria ON (noticias.categoria = categoria.id_categoria) WHERE status=1");
							
							while ($TND = mysql_fetch_array($SQL_TDN)) {
								
							
						?>
						<tr>
							<td><? echo $TND['titulo']; ?></td>
							<td><? echo $TND['autorPub']; ?></td>
							<td><? echo $TND['nombre_categoria']; ?></td>
							<td><? echo $TND['tags']; ?></td>
							<td><? 
							
							$dataExp = explode("-", $TND['dataPub']); 
							echo $dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
							?></td>
							<td><a href="gerenciar-noticia.php?id_nt_ed=<?echo $TND['id_noticia'];?>">Editar</a></td>
							<td><a href="acciones/papelera.php?idN=<?echo $TND['id_noticia'];?>">Excluír</a></td>
						</tr>
						<?
							}
						?>
					</tbody>
				</table>
			</section> <!-- Todas las noticias -->
			
			<section id="borradores">
				<table cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td>Título de la Noticia</td>
							<td>Autor</td>
							<td>Categorías</td>
							<td>Tags</td>
							<td>Fecha</td>
							<td>Editar</td>
						</tr>
						<?
							$SQL_TDN = mysql_query("SELECT id_noticia, titulo, dataPub, autorPub, tags, nombre_categoria FROM noticias INNER JOIN categoria ON (noticias.categoria = categoria.id_categoria) WHERE status=0");
							
							while ($TND = mysql_fetch_array($SQL_TDN)) {
										$id_nt_ed = $TND["id_noticia"];
							
						?>
						<tr>
							<td><? echo $TND['titulo']; ?></td>
							<td><? echo $TND['autorPub']; ?></td>
							<td><? echo $TND['nombre_categoria']; ?></td>
							<td><? echo $TND['tags']; ?></td>
							<td><? 
							
							$dataExp = explode("-", $TND['dataPub']); 
							echo $dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
							?></td>
							<td><a href="gerenciar-noticia.php?id_nt_ed=<? echo $id_nt_ed; ?>">Editar</a></td>
						</tr>
						<?
							}
						?>
					</tbody>
				</table>
			</section> <!-- Borradores -->
			
			<section id="papelera">
				<table cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td>Título de la Noticia</td>
							<td>Autor</td>
							<td>Categorías</td>
							<td>Tags</td>
							<td>Fecha</td>
							<td>Restaurar</td>
							<td>Excluír</td>
						</tr>
						<?
							$SQL_TDN = mysql_query("SELECT id_noticia, titulo, dataPub, autorPub, tags, nombre_categoria FROM noticias INNER JOIN categoria ON (noticias.categoria = categoria.id_categoria) WHERE status=2");
							
							while ($TND = mysql_fetch_array($SQL_TDN)) {
								
							
						?>
						<tr>
							<td><? echo $TND['titulo']; ?></td>
							<td><? echo $TND['autorPub']; ?></td>
							<td><? echo $TND['nombre_categoria']; ?></td>
							<td><? echo $TND['tags']; ?></td>
							<td><? 
							
							$dataExp = explode("-", $TND['dataPub']); 
							echo $dataExp[2]."/".$dataExp[1]."/".$dataExp[0];
							?></td>
							<td><a href="acciones/restaurar.php?idN=<?echo $TND['id_noticia'];?>">Restaurar</a></td>
							<td><a href="acciones/excluir-noticia.php?idNX=<?echo $TND['id_noticia'];?>">Excluír</a></td>
						</tr>
						<?
							}
						?>
					</tbody>
				</table>
			</section> <!-- Papelera -->
			
			</section> <!-- Content -->
				
		</section> <!-- Wrapper -->
		
	</main>

<?php require "includes/footer.php"; ?>