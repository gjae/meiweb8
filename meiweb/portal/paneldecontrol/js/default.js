$(function(){
	var boxImg = $("#imagen-noticia-cargar");
	var imgCar = $("#imagen-cargada");
	
	$(boxImg).click(function(){
		$(imgCar).click();
	});
	$(imgCar).change(function(){
		document.getElementById("imagen-noticia-cargar").value = document.getElementById("imagen-cargada").value;
	});
	
	
	
	var linkpublicar = $("#publicar-noticia-menu");
	var linktodasnot = $("#todas-noticias-menu");
	var linkborradores = $("#borradores-menu");
	var linkpapelera = $("#papelera-menu");
	
	
	var publicarnoticia = $("#publicar-noticia");
	var todaslasnoticias = $("#todas-las-noticias");
	var borradores = $("#borradores");
	var papelera = $("#papelera");
	
	$(todaslasnoticias).hide();
	$(borradores).hide();
	$(papelera).hide();
	
	$(linkpublicar).click(function(){
		
		$(linktodasnot).removeClass("activeColor");
		$(linkborradores).removeClass("activeColor");
		$(linkpapelera).removeClass("activeColor");
		$(linkpublicar).addClass("activeColor");
		
		if(!($(todaslasnoticias).hide())){
		$(todaslasnoticias).slideUp(300, function(){
			$(todaslasnoticias).hide();
		});}
		if(!($(borradores).hide())){
		$(borradores).slideUp(300, function(){
			$(borradores).hide();
		});}
		if(!($(papelera).hide())){
		$(papelera).slideUp(300, function(){
			$(papelera).hide();
		});}
		$(publicarnoticia).slideDown(500);
	});
	
	$(linktodasnot).click(function(){
		
		$(linkpublicar).removeClass("activeColor");
		$(linkborradores).removeClass("activeColor");
		$(linkpapelera).removeClass("activeColor");
		$(linktodasnot).addClass("activeColor");
		
		if(!($(publicarnoticia).hide())){
		$(publicarnoticia).slideUp(300, function(){
			$(publicarnoticia).hide();
		});}
		if(!($(borradores).hide())){
		$(borradores).slideUp(300, function(){
			$(borradores).hide();
		});}
		if(!($(papelera).hide())){
		$(papelera).slideUp(300, function(){
			$(papelera).hide();
		});}
		$(todaslasnoticias).slideDown(500);
	});
	
	$(linkborradores).click(function(){
		
		$(linkpublicar).removeClass("activeColor");
		$(linkpapelera).removeClass("activeColor");
		$(linktodasnot).removeClass("activeColor");
		$(linkborradores).addClass("activeColor");
		
		if(!($(publicarnoticia).hide())){
		$(publicarnoticia).slideUp(300, function(){
			$(publicarnoticia).hide();
		});}
		if(!($(todaslasnoticias).hide())){
		$(todaslasnoticias).slideUp(300, function(){
			$(todaslasnoticias).hide();
		});}
		if(!($(papelera).hide())){
		$(papelera).slideUp(300, function(){
			$(papelera).hide();
		});}
		$(borradores).slideDown(500);
	});
	
	$(linkpapelera).click(function(){
		
		$(linkpublicar).removeClass("activeColor");
		$(linktodasnot).removeClass("activeColor");
		$(linkborradores).removeClass("activeColor");
		$(linkpapelera).addClass("activeColor");
		
		if(!($(publicarnoticia).hide())){
		$(publicarnoticia).slideUp(300, function(){
			$(publicarnoticia).hide();
		});}
		if(!($(todaslasnoticias).hide())){
		$(todaslasnoticias).slideUp(300, function(){
			$(todaslasnoticias).hide();
		});}
		if(!($(borradores).hide())){
		$(borradores).slideUp(300, function(){
			$(borradores).hide();
		});}
		$(papelera).slideDown(500);
	});
	
	
	
	//verificación de campo categorías
	
	var input_cat = $("#categorias-nombres").value();
	
	$("#registrar-cat").submit(function(){
		if(input_cat == ""){
			alert("Debe registrar al menos una categoría");
		}
	});
	
});