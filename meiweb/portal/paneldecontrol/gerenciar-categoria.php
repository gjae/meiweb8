<?
require_once "includes/header.php";
?>
<script type="text/javascript" src="js/validaciones.js"></script>
<main>
	<section id="wrapper">
		<section id="content">
			<section id="registrar-categoria">
				<h1>Registrar nueva categoría</h1>
				<form action="acciones/registrar-categoria.php" method="post" onsubmit="return validarFormCat();">
					<table>
						<tr>
							<td colspan="2">Escriba el nombre de la(s) categoría(s):</td>
						</tr>
						<tr>
							<td><input type="text" name="categorias-nombres" id="categorias-nombres" /></td>
							<td><input type="submit" value="Registrar" id="registrar-cat"/></td>
						</tr>
						<tr>
							<td colspan="2" class="nota-table">*Separe con una coma (,) el nombre de las categorías.</td>
						</tr>
					</table>
				</form>
			</section>
			
			<section id="categorias-existentes">
				<h1 class="btm">Categorías registradas:</h1>
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>Nombre de la Categoría</td>
						<td>N° de Artículos</td>
						<td>Excluír</td>
					</tr>
					<?
					$SQL_CT = mysql_query("SELECT * FROM categoria");
					while($CTN = mysql_fetch_array($SQL_CT)){
						$id_ct = $CTN['id_categoria'];
						$SQL_COUNT_NT = mysql_query("SELECT * FROM noticias WHERE categoria='$id_ct'");
						$COUNT_NUM = mysql_num_rows($SQL_COUNT_NT); 	
					?>
					<tr>
						<td><? echo $CTN['nombre_categoria']; ?></td>
						<td><? echo $COUNT_NUM; ?></td>
						<td><a href="acciones/excluir-categoria.php?id_ct=<? echo $id_ct; ?>">Excluír</a></td>
					</tr>
					<?
					}
					?>
				</table>
			</section>
			
		</section> <!-- content -->
	</section> <!-- wrapper -->
</main>

<? require "includes/footer.php"; ?>