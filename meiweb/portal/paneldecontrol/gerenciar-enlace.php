<?
require_once "includes/header.php";
?>
<script type="text/javascript" src="js/default.js"></script>
<main>
	<section id="wrapper">
		<section id="content">
			<section id="administracion-portal">
				<?
						if(!isset($_GET["id_en"])){
							
				?>
				<h1>Registrar Nuevo Enlace</h1>
				<form action="acciones/registrar-enlace.php" method="post" enctype="multipart/form-data">
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td>Nombre del Enlace:</td>
							<td><input type="text" name="nom-enlace" required /></td>
						</tr>
						<tr>
							<td>Dirección del Enlace:</td>
							<td><input type="text" name="dir-enlace" required value="http://"/></td>
						</tr>
						<tr>
							<td>Imagen del Enlace:</td>
							<td><input type="text" id="imagen-noticia-cargar" placeholder="Selecciona una imágen" />
								<input type="file" hidden id="imagen-cargada" name="enlace-img" />
							</td>
						</tr>
						<tr>
							<td colspan="2" align="right"><input type="submit" value="Registrar Enlace" /></td>
						</tr>
					</table>
				</form>
				<?
						}
						else{
							$id_en_ed = $_GET["id_en"];
							$SQL_EN = mysql_query("SELECT * FROM enlaces WHERE idenlace=$id_en_ed");
							while($en = mysql_fetch_array($SQL_EN)){
								$id_enlace = $en["idenlace"];
								$nombre_en = $en["nombre"];
								$direccion_en = $en["direccion"];
								$imagen_en = $en["imagen"];
							}
					?>
					<h1>Editar Enlace</h1>
				<form action="acciones/actualizar-enlace.php?id_en_up=<? echo $id_enlace; ?>" method="post" enctype="multipart/form-data">
					<table cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td>Nombre del Enlace:</td>
							<td><input type="text" name="nom-enlace" required value="<? echo $nombre_en; ?>"/></td>
						</tr>
						<tr>
							<td>Dirección del Enlace:</td>
							<td><input type="text" name="dir-enlace" required value="<? echo $direccion_en; ?>"/></td>
						</tr>
						<tr>
							<td>Imagen del Enlace:</td>
							<td><input type="text" id="imagen-noticia-cargar" value="<? echo $imagen_en; ?>" />
								<input type="file" hidden id="imagen-cargada" name="enlace-img" value="<? echo $imagen_en; ?>"/>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="right"><input type="submit" value="Editar Enlace" /></td>
						</tr>
					</table>
				</form>
				<?
						}
					?>
			</section> <!-- administracion-enlaces -->
			<section id="administracion-portal-cd">
				<h1>Enlaces Registrados</h1>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>Nombre</td>
						<td>Dirección</td>
						<td>Imagen</td>
						<td>Editar</td>	
						<td>Excluír</td>							
					</tr>
					<?
					$SQL = mysql_query("SELECT * FROM enlaces");
					while($lh = mysql_fetch_array($SQL)){
						$idenlace = $lh['idenlace'];
					
					?>
					<tr>
						<td><? echo $lh['nombre']; ?></td>
						<td><? echo $lh['direccion']; ?></td>
						<td><img src="imagenes/imgenlace/<? echo $lh['imagen']; ?>" width="70" height="80" /></td>
						<td><a href="gerenciar-enlace.php?id_en=<? echo $idenlace; ?>">Editar</a></td>
						<td><a href="acciones/excluir-enlace.php?id_en=<? echo $idenlace; ?>" onclick="return confirm('Esta seguro de eliminar el enlace de <?echo $lh['nombre'];?>?');">Excluír</a></td>
					</tr>
					<?
					}
					?>
				</table>
			</section> <!--  -->
		</section> <!-- content -->
	</section> <!-- wrapper -->
</main>
<?
require_once "includes/footer.php";
?>