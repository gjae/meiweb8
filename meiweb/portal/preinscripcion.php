<?
require_once 'includes/header.php';
include_once('../baseDatos/BD.class.php'); 
$baseDatos=new BD();
?>
<main>
	<style>
		td{
			padding: 10px;
		}
	
	</style>
	<script>
	function enviarCancelar()
			{
				location.replace("../portal/cursosVirtuales.php");			
			}
			function enviarCrear()
			{
				if(validarDatos())
				{
					document.frm_activar.submit();
				}				
			}
			function validarDatos()
			{
				if(validarCorreo())
				{
					if(document.frm_activar.txt_Documento.value == false)
					{
						alert("Ingrese un valor de Documento");
					}
					else if(document.frm_activar.txt_reDocumento.value == false)
					{
						alert("Ingrese un valor para confirmar su Documento");
					}
					else if(document.frm_activar.txt_reDocumento.value != document.frm_activar.txt_Documento.value)
					{
						alert("El Documento y su confirmación no son iguales, favor confirmar");
					}
					else if(isNaN(parseInt(document.frm_activar.txt_telefono.value))) 
					{
						alert("El número de teléfono no es correcto");
					}
					else if( document.frm_activar.txt_Documento.value == false || document.frm_activar.txt_correo.value == false || document.frm_activar.txt_telefono.value == false ||  document.frm_activar.txt_Nombre1.value == false ||  document.frm_activar.txt_Apellido1.value == false)
					{
						alert ("Es necesario que llene completamente la información solicitada");
					}
					else
					{
						return true;
					}
				}
				else
				{
					return false;
				}
				
			}
			
          	function validarCorreo()
			{
				if(document.frm_activar.txt_correo.value=="")
				{
					alert("Es necesario que llene completamente la información solicitada");
					document.frm_activar.txt_correo.focus();
					return false;
				}
				else if(document.frm_activar.txt_correo.value.search(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig))
				{
					alert("El correo electronico no es válido, escribalo en forma correcta");
					document.frm_activar.txt_correo.focus();
					return false;
				}
				else
				{
					return true;
				}
			}	
          	
          	
	</script> 
	<section id="content">
		<section id="contenido">
			<section id="categoria-news">
		
           <form name="frm_activar" method="post" action="activarPreinscripcion.php?idvirmateria=<?=$_GET["idvirmateria"]?>&virmateria=<?=$_GET["virmateria"]?>">
           
                       
                       
                    
					<h1>PREINSCRIPCION</h1>
					<p>
                      Diligencie el breve formulario a continuaci&oacute;n para poder preinscribirse al curso: <?=$_GET["virmateria"]?></p>	
                      
                      <p><strong>NOTA</strong>: Todos los campos marcados con asterisco (<strong>*</strong>) son obligatorios.</p>
 
					<br>
					<div align="center">
					<table border="0" >
					<tr><td><p>Tipo de documento: </p></td>
                  <td><select name="tipodocumento">
              <option value="CC" selected>Cedula de Ciudadania</option>
              <option value="TI">Tarjeta de Identidad</option>
              <option value="CE">Cedula de Extranjeria</option>
              <option value="P">Pasaporte</option>
              <option value="RC">Registro Civil</option>
          </select></td></tr>
          
          
                   <tr><td> <p>Documento (<strong>*</strong>) : </p></td>
                   <td><input name="txt_Documento" type="text" id="txt_Documento" ></td></tr>
                   <tr><td><p>Confirmar Número de Documento (<strong>*</strong>) : </p></td>
                   <td><input name="txt_reDocumento" type="text"></td></tr>
                    <tr><td><p>Primer Nombre (<strong>*</strong>) : </p></td>
                   <td> <input name="txt_Nombre1" type="text" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"> </td></tr>
                   <tr><td> <p>Segundo Nombre: </p></td>
                   <td> <input name="txt_Nombre2" type="text" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></td></tr>
                    <tr><td><p>Primer Apellido (<strong>*</strong>) : </p></td>
                   <td> <input name="txt_Apellido1" type="text" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></td></tr>
                    <tr><td><p>Segundo Apellido: </p></td>
                    <td><input name="txt_Apellido2" type="text" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();"></a></td></tr>
                     <tr><td><p>Teléfono (<strong>*</strong>) : </p></td>
                    <td> <input name="txt_telefono" type="text"></td></tr>
                     <tr><td><p>Correo Electronico (<strong>*</strong>) : </p></td>
                    <td><input type="text" name="txt_correo"></td></tr>
                    
                </table> 
                </div>
                
                    <br><br>
                   <input name="btn_crearPreinscripcion" type="button" id="btn_crearPreinscripcion" value="Crear Preinscripción" onClick="javascript: enviarCrear();">		
                   	<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></center>
                    
                    					
                    
                   
</hr></td></tr>
             </form>
        				

         </section>
		</section> <!-- Contenido -->
		
		<section id="sidebar"><? require_once 'includes/sidebar.php'; ?></section>
	</section> <!-- Content -->
</main>
<?
require_once 'includes/footer.php';
?>