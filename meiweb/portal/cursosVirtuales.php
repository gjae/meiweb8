<?
require_once 'includes/header.php';
include_once('../baseDatos/BD.class.php'); 
$baseDatos=new BD();
?>
<main>
	

  <!--  Empieza post de noticias completo-->

    <div class="container">

      <div class="row">

          <div class="col-md-8 col-sm-8 col-xs-12">

            <div class="">
  <!--  Empieza la imagen de post-->
              <h3 class="tituloscursos">PROGRAMACIÓN WEB<br>
              (HTML,CSS,JAVASCRIPT,PHP,MYSQL)</h3>
              <h5 class="tituloscursos2">Curso Virtual</h5>
              <hr>


              <!-- Empieza el carrusel-->

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicadores -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Contenido slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="img/curso1virtual-img1.jpg" style="width:100%,;">

    </div>

    <div class="item">
     <img src="img/curso1virtual-img2.jpg" style="width:100%,;">
      <div class="carousel-caption">
        
      </div>
    </div>

    <div class="item">
      <img src="img/curso1virtual-img3.png" style="width:100%,;">
      <div class="carousel-caption">
        
      </div>
    </div>
  </div>

  <!-- Controles de izquierda y derecha -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Siguiente</span>
  </a>
</div>


<!-- Termina el Carrusel-->

              <br>
              
                 <!--  iconos -->                          
                 <span ><img src="img/avatar1.png" class="perfilavatar">Por <a href="#">Manuel Guillermo Flórez Becerra</a></span>
              <hr>
               <!--  Empieza Parrafo del post -->
                <p class="parrafos-cursos">
                En este curso aprenderemos  a maquetar sitios Web con HTML5 y a brindar estilos con CSS. No necesitas conocimientos previos, pues empezaremos desde cero con una serie de ejemplos prácticos y al término del curso, desarrollaremos un proyecto real en el cual aplicaremos todo lo aprendido

                Ademas estudiaremos los lenguajes PHP, Javascript y Mysql.

                <br><br></p>
               <!--  Termina el Parrafo del post -->
            </div>

            </div>
            


            <!--  Empieza la seccion de  informacion del Curso -->
            <div class="col-md-4 col-sm-4 col-xs-12  informacion-cursos ">
                     <h3 class="titulos">Información del Curso</h3>
                  <br>
                    <div>
                       

                        <p >

                          <p class="pull-left">Comienzo: </p>
                           <p class="pull-right"><b>06 de Octubre 2017</b></p>


                        </p>

                        <br><hr>
                            <p >

                          <p class="pull-left">Duración: </p>
                           <p class="pull-right"><b>6 meses</b></p>


                        </p>
                         <br><hr>
                            <p >

                          <p class="pull-left">Modalidad: </p>
                           <p class="pull-right"><b>Virtual</b></p>


                        </p>
                          <br><hr>
                            <p >

                          <p class="pull-left">Numero de Créditos: </p>
                           <p class="pull-right"><b>4</b></p>


                        </p>
                         <br><hr>
                            <p >

                          <p class="pull-left">Nivel: </p>
                           <p class="pull-right"><b>Fácil</b></p>


                        </p>
                        <br> <br> <br>
                       <center> <a href="https://meiweb.uis.edu.co/meiweb/login/index.php?entrar=1"><button class="btn btn-success btn-lg">INSCRIBIRSE</button></a></center>
                    </div>  
                  
                <br>

                <!--  Termina la seccion de Cursos Virtuales -->
            </div>
           




      </div>





    </div>  
 <!--  Termina la seccion de  categorias y etiquetas -->


<br>
<br>
<br>
<br>
<br>
<br>
<br>

	
</main>
<?
require_once 'includes/footer.php';
?>