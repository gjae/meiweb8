<footer class="footer">
	


<!-- Empieza footer-->
<div class="footer">
  <div class="container">

  <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <h3>
              Información de Contacto
            </h3><br>
            <p class="texto-footer-small"><i class="fa fa-home" aria-hidden="true"></i> Bucaramanga - Colombia. Cra 27 calle 9</p>

            <p class="texto-footer-small"><i class="fa fa-phone" aria-hidden="true"></i> PBX: (57) (7) 6344000</p>

            <p class="texto-footer-small"><i class="fa fa-envelope-o" aria-hidden="true"></i> soporte@meiweb.uis.edu.co</p>
           
        </div>

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h3 class="small-title">
              Ultimas Noticias
            </h3><br>
                    <img src="img/noticias-miniatura-1.jpg" align="left"></img><a class="link-noticias-minipost-footer " href="noticias-post1.html">CARRERA ATLÉTICA UIS</a><br><br><br>

                   <img src="img/noticias-miniatura-3.jpg" align="left"></img><a class="link-noticias-minipost-footer" href="noticias-post3.html">GRUPO CONUS</a><br><br><br>


            
      </div>
     

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h3 class="small-title">
              Nuestros Cursos
            </h3><br>
            <a class="texto-footer-small" href="curso-post1.html">Sistemas Operacionales</a><br><br>
            <a class="texto-footer-small" href="curso-post2.html">Programacion Orientada a Objetos</a><br><br>
            <a class="texto-footer-small" href="curso-virtual1.html">Programación Web</a><br><br>
            
      </div>

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <h3 class="small-title">
              Galeria
            </h3><br>
            

      </div>



 </div><br> <br> <br><br> <br> <br>
</div>
</div>

<!-- Empieza el Subfooter-->

<div class="subfooter">
  <div class="container">

  <div class="row">
    <br>
    <br>
      <div class="col-lg-12">© Todos los derechos reservados- Portal Meiweb 2017
              <div class="pull-right">Sitios de Interés
          <a href="http://www.uis.edu.co/webUIS/es/index.jsp" target="_blank">
            <img class="img-responsive logofooter" src="img/uis.png"></a>

          <a href="http://www.sena.edu.co/es-co/Paginas/default.aspx" target="_blank">
            <img class="img-responsive logofooter" src="img/sena.png"></a>

          <a href="http://www.mineducacion.gov.co/portal/" target="_blank">
            <img class="img-responsive logofooter" src="img/ministerio.png"></a>

          <a href="http://es.wikipedia.org/" target="_blank"><img class="img-responsive logofooter" src="img/wikipedia.png"></a>

              </div>


      </div>
      
  
 </div><br><br>
</div>
</div>
<!-- Termina el footer-->








	
</footer>
</body>
</html>