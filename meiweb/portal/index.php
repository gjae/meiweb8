<?php require_once 'includes/header.php'; ?>
<?require_once 'includes/configuracion.php';?>

<main>
	
 <!-- Empieza el carrusel-->

<div id="myCarousel " class="carousel slide carrusel-alto" data-ride="carousel">
  <!-- Indicadores -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Contenido slides -->
  <div class="carousel-inner carrusel-alto">
    <div class="item active">
      <img  src="img/slider-1.jpg" style="width:100%;">
      <!-- Caption dentro de lo slides -->
      <div class="carousel-caption">
         <h2>Bienvenido a Meiweb</h2>
        <p>Portal de Educación</p>
      </div>
    </div>

    <div class="item">
      <img src="img/slider-2.jpg" style="width:100%;">
      <div class="carousel-caption">
        <h2>Bienvenido a Meiweb</h2>
        <p>Portal de Educación</p>
        
      </div>
    </div>

    <div class="item">
      <img src="img/slider-3.jpg" style="width:100%;">
      <div class="carousel-caption">
         <h2>Bienvenido a Meiweb</h2>
        <p>Portal de Educación</p>
        
      </div>
    </div>
  </div>

  <!-- Controles de izquierda y derecha -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Siguiente</span>
  </a>
</div>
</nav>

   <!-- El carrusel se metio dentro del nav para que no quedara un espacio -->
<!-- Menu Principal Finalizado-->



 
<!-- Cajas se uso css para hacer los iconos redondeados y se llamaron los iconos de fontawesome.io-->
<div class="container conpadding ">

    <div class="row">
        <div class="col-md-12">
                  <h2 class="section-title textofuente h3negro">EDUCACIÓN, INVESTIGACIÓN Y ENSEÑANZA EN EL MUNDO</h2>
                </div>                
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                 
                    <div class="cajamenu textofuente ">
                      <center>
                              <div class="icono1">
                              <i class="fa fa-flash"></i> </div>

                             </center>
                       <h3 class="h3negro">Cursos Virtuales</h3>
                        <p>La formación virtual es una manera de aprendizaje que se acopla al tiempo y de quien la adelanta especial para los colombianos que se encuentran dentro y fuera del país.</p>
                        <a href="cursosVirtuales.php"><button class="btn btn-success">Explorar</button></a>
                    </div>
                  
                        
                </div>
                
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                     
                        <div class="cajamenu textofuente">
                          <center>
                              <div class="icono2">
                              <i class="fa fa-graduation-cap"></i> </div>
                          </center>

                       <h3 class="h3negro" >Cursos Presenciales</h3>
                       
                        <p>Los programas académicos en la Universidad Industrial de Santander, tienen por principio el desarrollo de la investigación en un área específica del saber.</p>
                        <a href="cursosPresenciales.php"><button class="btn btn-success">Explorar</button></a>
                    </div>
                   
                        
                </div>

                 <div class="col-md-4 col-sm-6 col-xs-12">
                   
                    <div class="cajamenu textofuente">
                       <center>
                              <div class="icono3">
                              <i class="fa fa-newspaper-o"></i> </div>
                          </center>

                       <h3 class="h3negro">Noticias</h3>
                        <p>Encuentra las ultimas noticias de la Universidad Industrial de Santander .</p>
                        <a href="categoria.php?id=1"><button class="btn btn-success">Explorar</button></a>
                    </div>
                    </div>
    
        </div>  
    </div>  

<div>
  <br>
  <br>
  <br>

           
    <!-- Empieza la seccion de cursos recomendados-->    

      <div class="container ">

        <div class="row">
           <div class="col-md-12">

                  <h2 class="section-title textofuente h3negro">CURSOS RECOMENDADOS</h2><br><br>
                </div>    

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 polaroid ">
                <div class="cajacursos  grow fade">
                    
                             <img src="img/cajacursos1.jpg" class="img-responsive imgcajacursos">
                             <br>
                             
                          
                                <span ><img src="img/avatar1.png" class="perfilavatar">Por <a href="perfil-profesor-1.html">Manuel Guillermo Flórez Becerra</a></span>
                                
                                <h4 class="titulos">SISTEMAS OPERACIONALES</h4>
                                <p class="parrafoscajacursos2">El objetivo de esta asignatura es proporcionar al estudiante los conocimientos básicos y analizis de los problemas principales a los que se enfrentan los sistemas operativos.</p>
                                <center><a href="curso-sistemas-operacionales.php"><button class="btn btn-success">Continuar leyendo</button></a></center>
                  
                </div>

            </div>


             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 polaroid ">
                <div class="cajacursos  grow fade">

                   <img src="img/curso1virtual-img1.jpg" class="img-responsive imgcajacursos">
                   <br>
                          
                                <span ><img src="img/avatar1.png" class="perfilavatar">Por <a href="perfil-profesor-1.html">Manuel Guillermo Flórez Becerra</a></span>
                                
                                <h4 class="titulos">PROGRAMACIÓN WEB</h4>
                                <p class="parrafoscajacursos2">En este curso aprenderemos a maquetar sitios Web con HTML5, y a brindar estilos con CSS.  </p><br>
                                <center><a href="cursosVirtuales.php"><button class="btn btn-success">Continuar leyendo</button></a></center>
                  
                </div>

            </div>





        </div>


      </div>

<!-- Termina la seccion de cursos recomendados-->    


<br>
<br>
<br>
<br>



  

</main>
<?php require_once 'includes/footer.php'; ?>