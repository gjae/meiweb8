<?
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../menu1/Menu1.class.php');	

	// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , [Barra de herramientas] , [Texto inicial] ) ;
	$editorP=new FCKeditor('edt_preguntaFrecuente' , '100%' , '100%' , 'barraBasica' , '' ) ;
	$editorR=new FCKeditor('edt_respuesta' , '100%' , '100%' , 'barraBasica' , '' ) ;
	if(comprobarSession())
	{
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{

?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<script> 
			function functionMateria()
			{ 
				document.frm_crearPreguntaFrecuente.submit();
			}
			function validar(idmateria,materia)
			{
				if(document.frm_crearPreguntaFrecuente.cbo_tema.value!="")
				{
				document.frm_crearPreguntaFrecuente.action="guardarPreguntaFrecuente.php?idmateria="+idmateria+"&materia="+materia;
				document.frm_crearPreguntaFrecuente.submit();
				}
				else
				alert ("debe llenar todos los campos");
			}
			
			function editar(idmateria)
			{
				document.frm_crearPreguntaFrecuente.action="crearPreguntaFrecuente.php?idmateria="+idmateria;
				document.frm_crearPreguntaFrecuente.submit();
			}
			
		</script> 

		<table class="tablaPrincipal">
		 	<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td valign="top">
					<table class="tablaGeneral" >
						<tr class="trSubTitulo">
						<?
						if ($_SESSION['idtipousuario']==5)
							 $sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
						else
							 $sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
						$resultado = $baseDatos->ConsultarBD($sql);
						list($nombre) = mysql_fetch_row($resultado); ?>
							<td><a href="../scripts/" class="link">Inicio</a><a> : </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nombre))?></a><a> : </a><a href="index.php?idmateria=<?= $_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link">Preguntas Frecuentes </a> : Crear Pregunta Frecuente</td>
						</tr>
					</table><br>
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td><img src="imagenes/agregarIntroduccion.gif" width="22" height="20" border="0">Crear Pregunta Frecuente</td>
						</tr>
					</table>
				<?
					$sql="SELECT mei_tema.titulo,mei_tema.idtema FROM mei_tema WHERE mei_tema.idmateria='".$_GET['idmateria']."' AND mei_tema.tipo =1";

					$resultado=$baseDatos->ConsultarBD($sql);
					if (mysql_num_rows($resultado) > 0)
					{
				?>
					<table class="tablaGeneral" >
					<form name="frm_crearPreguntaFrecuente" method="post" action="guardarPreguntaFrecuente.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>">
						<tr class="trSubTitulo">
							<td width="25%">Autor:</td>
							<td width="75%"><?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?></td>
						</tr>
						<tr class="trListaOscuro">
							<td>Seleccione un Tema:</td>
							<td>
							<? 
							list($hidCodigoTema,$hidTema)=explode('*',$_POST['hid_tema']);
							?>
								<select name="cbo_tema">
								<option class="link"> </option>
							<?
						while (list($tema,$codigoTema)=mysql_fetch_array($resultado))
						{
							if($tema ==$hidTema)
							{
							?>
								<option class="link" value="<?= $codigoTema.'*'.$tema?>" selected><?= $tema ?></option>
							<?							
							}
							else
							{
							?>
								<option class="link" value="<?= $codigoTema.'*'.$tema?>"><?= $tema ?></option>
						<?	
							}									
						}
						?>
							  </select>&nbsp;
							</td>
						</tr>
						<tr class="trListaClaro">
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr class="trListaOscuro">
							<td colspan="2">Pregunta:</td>
						</tr>
						<tr class="trListaClaro">
							<td colspan="2">
							<? 
								if(empty($_POST['hid_preguntaFrecuente']))
									$editorP->Value='';
								else
									$editorP->Value=stripslashes(base64_decode($_POST['hid_preguntaFrecuente']));
									$editorP->crearEditor();
							?>
							</td>
						</tr>
						
						<tr class="trListaOscuro">
							<td colspan="2">Respuesta:</td>
						</tr>
						<tr class="trListaClaro">
							<td colspan="2">
							<? 
								if(empty($_POST['hid_respuesta']))
									$editorR->Value='';
								else
									$editorR->Value=stripslashes(base64_decode($_POST['hid_respuesta']));
									$editorR->crearEditor();
							?>
							</td>
						</tr>
						<tr class="trListaOscuro">
							<td>Estado Respuesta:</td>
							<td>
								<select name="cbo_estadoR">
									<option class="link"> </option>
									<option class="link" value="1" selected>Activo</option>
									<option class="link" value="0">Inactivo</option>
							  </select>
							</td>
						</tr>
						
						<tr class="trListaClaro">
							<td colspan="2"><div align="center"><input type="button" name="btn_crearPreguntaFrecuente" value="Aceptar" onClick="javascript:validar('<?= $_GET['idmateria']?>','<?=$_GET["materia"]?>')"></div></td>
						</tr>
					</form>
					</table>
				  <?
						}
						else
						{
					?>&nbsp;
				  <table class="tablaGeneral">
                      <tr class="trInformacion">
                        <td>&nbsp;</td>
                      </tr>
                      <tr align="center" valign="middle" class="trInformacion">
                        <td><strong>No se han Definido temas para esta Materia</strong></td>
                      </tr>
                      <tr class="trInformacion">
                        <td>&nbsp;</td>
                      </tr>
                    </table>
				<?
						}
					?>
                </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
		</body>
		</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
?>
