<?
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once ('../menu/Menu.class.php');	
	include_once ('../menu1/Menu1.class.php');	

	// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , [Barra de herramientas] , [Texto inicial] ) ;
	$editorP=new FCKeditor('edt_preguntaFrecuente' , '100%' , '100%' , 'barraBasica' , '' ) ;
	$editorR=new FCKeditor('edt_respuesta' , '100%' , '100%' , 'barraBasica' , '' ) ;

	if(comprobarSession())
	{
	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
	{
		
		if($_GET['estado']== 1)
		{
				$sql="UPDATE mei_preguntafrecuente SET mei_preguntafrecuente.pregunta='".base64_encode($_POST['edt_preguntaFrecuente'])."' ,mei_preguntafrecuente.respuesta='".base64_encode($_POST['edt_respuesta'])."',mei_preguntafrecuente.estadorespuesta='".$_POST['cbo_estadoR']."' WHERE mei_preguntafrecuente.idpreguntafrecuente=".$_GET['codigoPreguntaFrecuente'];
				$resultado=$baseDatos->ConsultarBD($sql);

						
				redireccionar("index.php?idmateria=".$_GET['idmateria']);		
					
		}
		else
		{
		if ($_SESSION['idtipousuario']==5) 	
			$sql="SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql="SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$materias=$baseDatos->ConsultarBD($sql);
		list($materia) = mysql_fetch_array($materias);

		$sql="SELECT mei_preguntafrecuente.pregunta,mei_preguntafrecuente.respuesta,mei_preguntafrecuente.estadorespuesta FROM mei_preguntafrecuente WHERE mei_preguntafrecuente.idpreguntafrecuente=".$_GET['codigoPreguntaFrecuente'];
		$pregunta=$baseDatos->ConsultarBD($sql);
		list($pregunta,$respuesta,$estadoRespuesta) = mysql_fetch_array($pregunta);
			
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaMarquesina" >
						<tr class="trSubTitulo">
                        
                        <?
						if ($_SESSION['idtipousuario']==5)
							 $sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
						else
							 $sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
						$resultado = $baseDatos->ConsultarBD($sql);
						list($nombre) = mysql_fetch_row($resultado); ?>
							<td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nombre))?></a><a> -> </a><a href="index.php?idmateria=<?= $_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link">Preguntas Frecuentes</a> -> Modificar Pregunta </td>
                            
                            
							                           
                            
						</tr>
					</table><br>
					<table class="tablaGeneral">
						<tr class="trTitulo">
							<td><img src="imagenes/modificar.gif" width="16" height="16" border="0">
						    Modificar Pregunta Frecuente</td>
						</tr>
					</table>
		
					<table class="tablaGeneral">
					<form name="frm_modificarPreguntaFrecuente" method="post" action="<?= $PHP_SELF ?>?estado=1&codigoPreguntaFrecuente=<?= $_GET['codigoPreguntaFrecuente']?>&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>">
						<tr class="trSubTitulo">
							<td class="trSubTitulo" colspan="2">Modificacion de Datos</td>
						</tr>
						<tr class="trListaOscuro">
							<td colspan="2">Pregunta:</td>
						</tr>
						<tr class="trListaClaro">
							<td colspan="2">
							<? 
							$editorP->Value=$pregunta=stripslashes(base64_decode($pregunta));
							$editorP->crearEditor();
							?>
							</td>
						</tr>
						<tr class="trListaOscuro">
							<td colspan="2">Respuesta:</td>
						</tr>
						<tr class="trListaClaro">
							<td colspan="2">
							<? 
							$editorR->Value=stripslashes(base64_decode($respuesta));
							$editorR->crearEditor();
							?>
							</td>
						</tr>
						<tr class="trListaOscuro">
							<td width="25%">Estado Respuesta:</td>
							<td width="75%">
								<select name="cbo_estadoR">
									<option class="link"> </option>
									<option class="link" value="1" selected>Activo</option>
									<option class="link" value="0">Inactivo</option>
							  </select>
							</td>
						</tr>
						<tr class="trListaClaro">
							<td colspan="2"><div align="center"><input type="submit" name="sub_modificarPreguntaFrecuente" value="Modificar"></div></td>
						</tr>
					</form>
					</table>
		<?
		}
		?>			
				</td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
		</body>
		</html>
<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
?>
