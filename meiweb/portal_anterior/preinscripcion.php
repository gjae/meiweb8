<?
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
$baseDatos=new BD();
?>

<html>
<head>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" /><title>MEIWEB</title>
	
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script Language='JavaScript'>
            <!-- Esconde esto de navegadores antiguos
              if (window != window.top)
                 top.location.href = location.href;
            // --> 
             function enviarCancelar()
			{
				location.replace("../portal/cursosVirtuales.php");			
			}
			function enviarCrear()
			{
				if(validarDatos())
				{
					document.frm_activar.submit();
				}				
			}
			function validarDatos()
			{
				if(validarCorreo())
				{
					if(document.frm_activar.txt_Documento.value == false)
					{
						alert("Ingrese un valor de Documento");
					}
					else if(document.frm_activar.txt_reDocumento.value == false)
					{
						alert("Ingrese un valor para confirmar su Documento");
					}
					else if(document.frm_activar.txt_reDocumento.value != document.frm_activar.txt_Documento.value)
					{
						alert("El Documento y su confirmación no son iguales, favor confirmar");
					}
					else if(isNaN(parseInt(document.frm_activar.txt_telefono.value))) 
					{
						alert("El número de teléfono no es correcto");
					}
					else if( document.frm_activar.txt_Documento.value == false || document.frm_activar.txt_correo.value == false || document.frm_activar.txt_telefono.value == false ||  document.frm_activar.txt_Nombre1.value == false ||  document.frm_activar.txt_Apellido1.value == false)
					{
						alert ("Es necesario que llene completamente la información solicitada");
					}
					else
					{
						return true;
					}
				}
				else
				{
					return false;
				}
				
			}
			
          	function validarCorreo()
			{
				if(document.frm_activar.txt_correo.value=="")
				{
					alert("Es necesario que llene completamente la información solicitada");
					document.frm_activar.txt_correo.focus();
					return false;
				}
				else if(document.frm_activar.txt_correo.value.search(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ig))
				{
					alert("El correo electronico no es válido, escribalo en forma correcta");
					document.frm_activar.txt_correo.focus();
					return false;
				}
				else
				{
					return true;
				}
			}	
          	
          	
	</script> 
	
<link href="SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
</head>

<body>

<table align="center"  width="0" cellspacing="0" cellpadding="0">
	
	  
	  <tr> <!-- 1 -->
       			<td colspan="6"><img src="imagenes/<? echo recuperarVariableSistema("sistematema"); ?>/encabezado.png" ></td>
	  </tr>
     
	 
	 
      <tr> <!-- 2 -->
				<td colspan="2"  align="left"><img src="imagenes/<? echo recuperarVariableSistema("sistematema"); ?>/encabezado1.png"></td>
				<td width="42"></td>
				
				<td width="693">
						<ul id="MenuBar1" class="MenuBarHorizontal">
						  <li><a href="index.php">INICIO</a>	    </li>
						  <li><a href="#" class="MenuBarItemSubmenu">CURSOS</a>
						    <ul>
                              <li><a href="cursosPresenciales.php">PRESENCIALES</a></li>
                              <li><a href="cursosVirtuales.php">VIRTUALES</a></li>
						    </ul>
						  </li>
						  <li><a href="noticias.php">NOTICIAS</a>					      </li>
                          <li><a href="#" class="MenuBarItemSubmenu">DOCENTES</a>
                            <ul>
                              <li><a href="docentesPresenciales.php">PRESENCIALES</a>                              </li>
                              <li><a href="docentesVirtuales.php">VIRTUALES</a></li>
                            </ul>
                          </li>
                          <li><a href="#">ESTUDIANTES</a></li>
						</ul>
				</td>   
				<td width="6"></td>
				<td width="18"><img height="67" width="20" src="imagenes/<? echo recuperarVariableSistema("sistematema"); ?>/lateral.png"></td>
			
      </tr>
     
	 
	 
      <tr> <!-- 3 -->
				<td align="left" width="20"><img src="imagenes/<? echo recuperarVariableSistema("sistematema"); ?>/lateral.png"></td>
				<td width="200"> <!-- inicio de tr3 -->
				  <table width="170"  align="center" >
				    <tr bgcolor="#C4C4C4" align="center">
				      <td bgcolor="#C4C4C4">INICIAR SESION</td>
			        </tr>
				    <tr bgcolor="#C4C4C4">
				      <td><table width="100%" border="0" cellspacing="0" cellpadding="4">
				        <tr bgcolor="#FFFFFF">
				          <td bgcolor="#DDDDDD"><hr color="#CCCCCC">
				            <a href="../login/index.php?entrar=1" class="link"><img src="imagenes/foro.gif" width="23">Presencial</a>
				            <hr color="#CCCCCC">
				            <a href="../login/index.php?entrar=1" class="link"><img  src="imagenes/usuarios.gif" width="23">Virtual</a>
				            <hr color="#CCCCCC"></td>
			            </tr>
				        </table></td>
			        </tr>
			      </table>
				  <table width="170" align="center"> 
<tr bgcolor="#C4C4C4" align="center"> 
											<td bgcolor="#C4C4C4">UNIVERSIDAD</td> 
											</tr> 
											<tr bgcolor="#C4C4C4"> 
											<td> 
											<table width="100%" border="0" cellspacing="0" cellpadding="4" > 
											<tr bgcolor="#FFFFFF"> 

											<td bgcolor="#DDDDDD">
											<hr color="#CCCCCC">
											<a href="laUis.php" class="link"><img src="imagenes/correo.gif" width="23"> La UIS</a>
											<hr color="#CCCCCC">
											<a href="eisi.php" class="link"><img src="imagenes/biblioteca.gif" width="23"> EISI</a>
											<hr color="#CCCCCC">
											<a  href="educacionVirtual.php" class="link"><img src="imagenes/materias.gif"width="23"> Educaci&oacute;n Virtual</a>
											<hr color="#CCCCCC">
											<a href="ventajasEV.php" class="link"><img src="imagenes/directorio.gif" width="23">Ventajas de la Educacón Virtual</a>
											
											<hr color="#CCCCCC"> </a> </td> 
											</tr> 
											</table> 
											</td> 
											</tr> 
									</table>
									
									
				  <table width="170" align="center"> 
											<tr bgcolor="#C4C4C4" align="center"> 
											<td bgcolor="#C4C4C4">DESCARGAS</td> 
											</tr> 
											<tr bgcolor="#C4C4C4"> 
											<td> 
											<table width="100%" border="0" cellspacing="0" cellpadding="4"> 
											<tr bgcolor="#FFFFFF"> 

											<td bgcolor="#DDDDDD">
											<hr color="#CCCCCC"><a href="http://get.adobe.com/es/reader/" target="_blank" class="link"><img src="imagenes/PDF.gif" width="23"> Adobe Reader</a></hr>
											<hr color="#CCCCCC"><a href="http://get.adobe.com/es/flashplayer/" target="_blank" class="link"><img src="imagenes/icono_flash.jpg" width="25">Adobe Flash Player</a></hr>
											<hr color="#CCCCCC" align="center"><a href="http://www.winrar.es/descargas" target="_blank" class="link"><img src="imagenes/winrar.jpg" width="25"> WinRar</a></hr>
											<hr color="#CCCCCC"><a href="http://www.winzip.com/es/downwz.htm" target="_blank" class="link"><img src="imagenes/ZIP.gif" width="23">WinZip</a></hr>
											<hr color="#CCCCCC">


											</tr> 
											</table> 
											</td> 
											</tr> 
									</table>
									
													 <p><?
$tipo_semana = 1;
$tipo_mes = 1;

$MESCOMPLETO[1] = 'Enero';
$MESCOMPLETO[2] = 'Febrero';
$MESCOMPLETO[3] = 'Marzo';
$MESCOMPLETO[4] = 'Abril';
$MESCOMPLETO[5] = 'Mayo';
$MESCOMPLETO[6] = 'Junio';
$MESCOMPLETO[7] = 'Julio';
$MESCOMPLETO[8] = 'Agosto';
$MESCOMPLETO[9] = 'Septiembre';
$MESCOMPLETO[10] = 'Octubre';
$MESCOMPLETO[11] = 'Noviembre';
$MESCOMPLETO[12] = 'Diciembre';

$MESABREVIADO[1] = 'Ene';
$MESABREVIADO[2] = 'Feb';
$MESABREVIADO[3] = 'Mar';
$MESABREVIADO[4] = 'Abr';
$MESABREVIADO[5] = 'May';
$MESABREVIADO[6] = 'Jun';
$MESABREVIADO[7] = 'Jul';
$MESABREVIADO[8] = 'Ago';
$MESABREVIADO[9] = 'Sep';
$MESABREVIADO[10] = 'Oct';
$MESABREVIADO[11] = 'Nov';
$MESABREVIADO[12] = 'Dic';

$SEMANACOMPLETA[0] = 'Domingo';
$SEMANACOMPLETA[1] = 'Lunes';
$SEMANACOMPLETA[2] = 'Martes';
$SEMANACOMPLETA[3] = 'Miércoles';
$SEMANACOMPLETA[4] = 'Jueves';
$SEMANACOMPLETA[5] = 'Viernes';
$SEMANACOMPLETA[6] = 'Sábado';

$SEMANAABREVIADA[0] = 'Dom';
$SEMANAABREVIADA[1] = 'Lun';
$SEMANAABREVIADA[2] = 'Mar';
$SEMANAABREVIADA[3] = 'Mie';
$SEMANAABREVIADA[4] = 'Jue';
$SEMANAABREVIADA[5] = 'Vie';
$SEMANAABREVIADA[6] = 'Sáb';

////////////////////////////////////
if($tipo_semana == 0){
					$ARRDIASSEMANA = $SEMANACOMPLETA;
}elseif($tipo_semana == 1){
					$ARRDIASSEMANA = $SEMANAABREVIADA;
}
if($tipo_mes == 0){
					$ARRMES = $MESCOMPLETO;
}elseif($tipo_mes == 1){
					$ARRMES = $MESABREVIADO;
}

if(!$dia) $dia = date(d);
if(!$mes) $mes = date(n);
if(!$ano) $ano = date(Y);

$TotalDiasMes = date(t,mktime(0,0,0,$mes,$dia,$ano));
$DiaSemanaEmpiezaMes = date(w,mktime(0,0,0,$mes,1,$ano));
$DiaSemanaTerminaMes = date(w,mktime(0,0,0,$mes,$TotalDiasMes,$ano));
$EmpiezaMesCalOffset = $DiaSemanaEmpiezaMes;
$TerminaMesCalOffset = 6 - $DiaSemanaTerminaMes;
$TotalDeCeldas = $TotalDiasMes + $DiaSemanaEmpiezaMes + $TerminaMesCalOffset;


if($mes == 1){
					$MesAnterior = 12;
					$MesSiguiente = $mes + 1;
					$AnoAnterior = $ano - 1;
					$AnoSiguiente = $ano;
}elseif($mes == 12){
					$MesAnterior = $mes - 1;
					$MesSiguiente = 1;
					$AnoAnterior = $ano;
					$AnoSiguiente = $ano + 1;
}else{
					$MesAnterior = $mes - 1;
					$MesSiguiente = $mes + 1;
					$AnoAnterior = $ano;
					$AnoSiguiente = $ano;
					$AnoAnteriorAno = $ano - 1;
					$AnoSiguienteAno = $ano + 1;
}

print "<table style=\"font-family:arial;font-size:9px\" bordercolor=navy align=center border=0 cellpadding=1 cellspacing=1>";
print " <tr>";
print " <td colspan=10>";
print " <table border=0 align=center width=\"1%\" style=\"font-family:arial;font-size:9px\">";
print " <tr>";
print " <td width=\"1%\"><a href=\"$PHP_SELF?mes=$mes&ano=$AnoAnteriorAno\"><img src=../portal/imagenes/retroceder.gif border=0></a></td>";
print " <td width=\"1%\"><a href=\"$PHP_SELF?mes=$MesAnterior&ano=$AnoAnterior\"><img src=../portal/imagenes/retroceder.gif border=0></a></td>";
print " <td width=\"1%\" colspan=\"1\" align=\"center\" nowrap><b>".$ARRMES[$mes]." - $ano</b></td>";
print " <td width=\"1%\"><a href=\"$PHP_SELF?mes=$MesSiguiente&ano=$AnoSiguiente\"><img     src=../portal/imagenes/avanzar.gif border=0></a></td>";
print " <td width=\"1%\"><a href=\"$PHP_SELF?mes=$mes&ano=$AnoSiguienteAno\"><img src=../portal/imagenes/avanzar.gif border=0></a></td>";
print " </tr>";
print " </table>";
print " </td>";
print "</tr>";
print "<tr>";
foreach($ARRDIASSEMANA AS $key){
					print "<td bgcolor=#ccccff><b>$key</b></td>";
}
print "</tr>";

for($a=1;$a <= $TotalDeCeldas;$a++){ 
					if(!$b) $b = 0;
					if($b == 7) $b = 0;
					if($b == 0) print '<tr>';
					if(!$c) $c = 1;
					if($a > $EmpiezaMesCalOffset AND $c <= $TotalDiasMes){
						if($c == date(d) && $mes == date(m) && $ano == date(Y)){
							print "<td bgcolor=\"#ffcc99\">$c<br></td>";
						}elseif($b == 0 OR $b == 6){
							print "<td bgcolor=#99cccc>$c</td>";
						}else{
							print "<td bgcolor=\"#EEEEEE\">$c</td>";
						}
						$c++;
					}else{
						print "<td> </td>";
					}
					if($b == 6) print '</tr>';
					$b++;
}
print "<tr><td align=center colspan=10></a></td></tr>";
print "</table>";

?>
</p>
													 <p>&nbsp;</p>
													 <p>&nbsp;</p>
													 <p>&nbsp;</p>
												
															
				</td> <!--fin de tr3 -->
				    <td></td>
				<td  align="center" valign="top">
						
						<style type="text/css">

div.scrolled {
 
  height: 800px;
  overflow: scroll;
}

</style>

<div class="scrolled">



	
       <table cellspacing="4" cellpadding="4"  width=650> <br>
           <form name="frm_activar" method="post" action="activarPreinscripcion.php?idvirmateria=<?=$_GET["idvirmateria"]?>&virmateria=<?=$_GET["virmateria"]?>">
           
                       
                       
                        <tr align="justify" width>
           			<td colspan="0" align="justify" width="90%"> <hr color="#CCCCCC"><fieldset>
					<legend>PREINSCRIPCION

					</legend>
					<ol>
                      Diligencie el breve formulario a continuaci&oacute;n para poder preinscribirse al curso: <?=$_GET["virmateria"]?>
                      
                      <p><strong>NOTA</strong>: Todos los campos marcados con asterisco (<strong>*</strong>) son obligatorios.</p>
 
					<hr color="#CCCCCC"><center>
					<a>Tipo de documento: </a><br>
                  <select name="tipodocumento">
              <option value="CC" selected>Cedula de Ciudadania</option>
              <option value="TI">Tarjeta de Identidad</option>
              <option value="CE">Cedula de Extranjeria</option>
              <option value="P">Pasaporte</option>
              <option value="RC">Registro Civil</option>
          </select><br>
                    <a>Documento (<strong>*</strong>) : </a><br>
                   <input name="txt_Documento" type="text" id="txt_Documento" ><br>
                    <a>Primer Nombre (<strong>*</strong>) : </a><br>
                    <input name="txt_Nombre1" type="text" > <br>
                    <a>Segundo Nombre: </a><br>
                    <input name="txt_Nombre2" type="text" ><br>
                    <a>Primer Apellido (<strong>*</strong>) : </a><br>
                    <input name="txt_Apellido1" type="text"><br>
                    <a>Segundo Apellido: </a><br>
                    <input name="txt_Apellido2" type="text" ></a><br>
                     <a>Teléfono (<strong>*</strong>) : </a><br>
                     <input name="txt_telefono" type="text"><br>
                     <a>Correo Electronico (<strong>*</strong>) : </a><br>
                    <input type="text" name="txt_correo"><br><br>
                    <a>Confirmar N&uacute;mero de Documento (<strong>*</strong>) : </a><br>
                   <input name="txt_reDocumento" type="text"><br><br>
                 
                     <hr color="#CCCCCC">
                   <input name="btn_crearPreinscripcion" type="button" id="btn_crearPreinscripcion" value="Crear Preinscripcion" onClick="javascript: enviarCrear();">		
                   	<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></center>
                    
                    					</ol>	</fieldset>
                    
                   
</hr></td></tr>
             </form>
         </table>
       
				       
       
				       
								
				</div>  
				
				</td>
				
               
                 <td></td>
				<td ><img src="imagenes/<? echo recuperarVariableSistema("sistematema"); ?>/lateral.png"></td>
			
       </tr>
         
		 
		 
    	<tr> <!-- 4 -->
       			<td colspan="6"><img src="imagenes/<? echo recuperarVariableSistema("sistematema"); ?>/inferior.png"></td>
    	</tr>
     
	 
	 
       <tr> <!-- 5 -->
       <td colspan="6"  align="center" bgcolor="#9F9F9F"><p><br>
		         <font color="white">Universidad Industrial de Santander</font><br>
		         <font color="white">Escuela de Ingerniera de Sistemas e Informatica</font><br>
		         <font color="white">Bucaramanga-Colombia, Cra 27 calle 9, pbx:(57)(7)6344000</font></p></td>	  
	  
   	   </tr>
 

  		<tr> <!-- 6 -->
       			<td colspan="6"><img src="imagenes/<? echo recuperarVariableSistema("sistematema"); ?>/inferior2.png"></td>
       </tr>


</table>
<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</body>
</html>



