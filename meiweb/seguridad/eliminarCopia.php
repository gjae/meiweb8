<?PHP
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
		if(isset($_GET['idCopia']))
		{
			$sql="SELECT mei_copiaseguridad.copia FROM mei_copiaseguridad WHERE mei_copiaseguridad.idcopiaseguridad=".$_GET['idCopia'];
			$copiaSeguridad=$baseDatos->ConsultarBD($sql);		
			list($archivoCopia)=mysql_fetch_array($copiaSeguridad);
			
			if(file_exists($archivoCopia))
			{
				unlink($archivoCopia);
			}

			$sql="DELETE FROM mei_copiaseguridad WHERE idcopiaseguridad = ".$_GET['idCopia'];
			$baseDatos->ConsultarBD($sql);		
		}
		else
		{
			if(isset($_POST['chk_idCopia']))
			{
				foreach($_POST['chk_idCopia'] as $idCopia)
				{
					$sql="SELECT mei_copiaseguridad.copia FROM mei_copiaseguridad WHERE mei_copiaseguridad.idcopiaseguridad=".$idCopia;
					$copiaSeguridad=$baseDatos->ConsultarBD($sql);		
					list($archivoCopia)=mysql_fetch_array($copiaSeguridad);
					
					if(file_exists($archivoCopia))
					{
						unlink($archivoCopia);
					}
		
					$sql="DELETE FROM mei_copiaseguridad WHERE idcopiaseguridad = ".$idCopia;
					$baseDatos->ConsultarBD($sql);
				}
			}
		
		}
		
		redireccionar("../seguridad/");	
	}
	else
	{
		redireccionar("../login/");
	}
?>