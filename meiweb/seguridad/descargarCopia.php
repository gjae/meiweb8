<?PHP
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');

	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
		$baseDatos->CrearCopiaBD(false);
		
		if(isset($_GET['idDescarga']))
		{
			$sql="SELECT mei_copiaseguridad.nombre,mei_copiaseguridad.copia FROM mei_copiaseguridad WHERE mei_copiaseguridad.idcopiaseguridad=".$_GET['idDescarga'];
			$consulta=$baseDatos->ConsultarBD($sql);

			list($nombreDescarga,$nombreArchivo)=mysql_fetch_array($consulta);
			descargarArchivo($nombreArchivo,$nombreDescarga);
			
		}
	}
	else
	{
		redireccionar("../login/");
	}
?>