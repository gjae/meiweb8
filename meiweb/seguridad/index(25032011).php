<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../librerias/vistas.lib.php');
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{	
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		if(recuperarVariableSistema("sistemacopiaautomatica") != 0)
		{
			$chkCopiaAutomatica="checked";
			
		}
		else
		{
			$activarCasilla="disabled";
		}
		
		if(recuperarVariableSistema("sistemanobitacora") != 0)
		{
			$chkBitacora="checked";
		}
		
		$sql="SELECT mei_copiaseguridad.idcopiaseguridad,mei_copiaseguridad.fecha, mei_copiaseguridad.nombre, mei_copiaseguridad.tamano, mei_copiaseguridad.tipo, mei_copiaseguridad.copia
				FROM mei_copiaseguridad 
					WHERE mei_copiaseguridad.fecha =
						(
							SELECT MAX(mei_copiaseguridad.fecha) 
								FROM mei_copiaseguridad
						)";
				
		$consultaCopiaReciente=$baseDatos->ConsultarBD($sql);
		list($idCopiaReciente,$fechaCopiaReciente,$nombreCopiaReciente,$tamanoCopiaReciente,$tipoCopiaReciente,$archivoCopiaReciente)=mysql_fetch_array($consultaCopiaReciente);
		
		$tituloCopiaReciente="Copia de seguridad: ".$nombreCopiaReciente."\n";
		$tituloCopiaReciente.="Fecha de Generacion: ".$fechaCopiaReciente." \n";
		
		if(empty($tipoCopiaReciente))
		{
			$tipo="Manualmente";
		}
		else
		{
			$tipo="Automaticamente";
		}
		
		$tituloCopiaReciente.="Copia generada: ".$tipo;
		
		
		$sql="SELECT mei_copiaseguridad.idcopiaseguridad,mei_copiaseguridad.fecha,mei_copiaseguridad.nombre,mei_copiaseguridad.tamano,mei_copiaseguridad.tipo,mei_copiaseguridad.copia 
				FROM mei_copiaseguridad ORDER BY mei_copiaseguridad.fecha DESC";
				
		$consultaCopias=$baseDatos->ConsultarBD($sql);
		$numeroRegistros=mysql_num_rows($consultaCopias);
?>	
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
		<script language="javascript">
		
		function chekear()
		{
			var todo=document.getElementById('todo');
			
			if (todo.checked==true)
				{
					for(i=0;i<document.frm_opciones.elements.length;i++)
					{
						if(document.frm_opciones.elements[i].id=="codigoCopia")
						document.frm_opciones.elements[i].checked=true;
					}
				}
			else
				{
					for(i=0;i<document.frm_opciones.elements.length;i++)
					{
						if(document.frm_opciones.elements[i].id=="codigoCopia")
						document.frm_opciones.elements[i].checked=false;
					}
				}
		}
			
			function validarOpciones()
			{
				if(document.frm_opciones.txt_tiempoCopia.value==false || parseInt(document.frm_opciones.txt_tiempoCopia.value) < 1)
				{
					alert("La Frecuencia de generaci�n de copias de seguridad autom�ticas no es correcta. Aseg�rese de que el valor sea un n�mero mayor que 1.");
					document.frm_opciones.txt_tiempoCopia.value="";
				}
				else
				{
					return true;
				}
				
			}		
			
			function enviarOpciones()
			{
				if(validarOpciones())
				{
					document.frm_opciones.action="guardarOpciones.php";
					document.frm_opciones.submit();								
				}
			}
			
			function enviarGenerarCopia()
			{
				document.frm_opciones.action="generarCopia.php";
				document.frm_opciones.submit();	
			}
			
			function enviarEliminar()
			{
				if(confirm("�Esta seguro que desea eliminar las copias de seguridad seleccionadas?"))
				{
					document.frm_opciones.action="eliminarCopia.php";
					document.frm_opciones.submit();
				}
			}
			
			function enviarDescarga(idDescarga)
			{
				document.frm_opciones.action="descargarCopia.php?idDescarga="+idDescarga;
				document.frm_opciones.submit();
			}
			
			function activarCasilla()
			{
				if(document.frm_opciones.chk_copiaAutomatica.checked == true)
				{
					document.frm_opciones.txt_tiempoCopia.disabled=false;
				}
				else
				{
					document.frm_opciones.txt_tiempoCopia.disabled=true;
				}
			}
			
			function eliminarCopia(idCopia)
			{
				
				if(confirm("�Esta seguro que desea eliminar la copia de seguridad seleccionada?"))
				{
					location.replace("eliminarCopia.php?idCopia="+idCopia);
				}
			}
		
		</script>
		</head>
		<body>
		
		<table width="100%" class="tablaPrincipal">
			<tr valign="top">
				<td><? menu(1); ?></td>
				<td>
				  <form method="post" name="frm_opciones">
					<table width="568" class="tablaGeneral">
						<tr class="trTitulo">
							<td colspan="6" class="trTitulo"><img src="imagenes/generarCopia.gif" width="16" height="16"> Copias de Seguridad</td>
						</tr>
						<tr class="trSubTitulo">
                        	<td colspan="6" class="trSubTitulo">Opciones de Generaci&oacute;n de Copias de Seguridad</td>
                      	</tr>
                      	<tr class="trInformacion">
                             <td width="4%"><input name="chk_copiaAutomatica" type="checkbox" id="chk_copiaAutomatica" value="1" onClick="javascript: activarCasilla();" <?= $chkCopiaAutomatica?>></td>
                             <td colspan="5">Habilitar la generaci&oacute;n de copias de seguridad autom&aacute;ticamente cada 
                                  <input name="txt_tiempoCopia" type="text" id="txt_tiempoCopia" value="<?= recuperarVariableSistema("sistematiempocopia");?>" size="5" <?= $activarCasilla?>>
								d&iacute;as. </td>
                        </tr>
                      	<tr class="trInformacion">
                              <td><input name="chk_bitacoraCopia" type="checkbox" id="chk_bitacoraCopia" value="1" <?= $chkBitacora?>></td>
                              <td colspan="5">No incluir en la copia de seguridad el contenido de la Bitacora. </td>
                            </tr>
                      	<tr class="trListaClaro">
                              <td colspan="4">Guardar Opciones de generaci&oacute;n de copias de seguridad</td>
                              <td colspan="2"><input name="btn_guardarOpciones" type="button" value="Guardar opciones" onClick="javascript:enviarOpciones();"></td>
                            </tr>
                      	<tr class="trListaClaro">
                              <td colspan="4">Generar una nueva copia de seguridad ahora</td>
                              <td colspan="2"><input name="btn_generarCopia" type="button" value="Generar copia" onClick="javascript: enviarGenerarCopia();"></td>
                            </tr>
                      <tr class="trSubTitulo">
                        <td colspan="7" class="trSubTitulo">Historial de Copias de Seguridad </td>
                      </tr>
                            <?
							  if(empty($numeroRegistros))
							  {
								$activoEliminar="disabled";
							  ?>
                            <tr class="trAviso">
                              <td colspan="6">No se han encontrado copias de seguridad <?= $nombreCopiaReciente?></td>
                            </tr>
                            <?
							  }
							  else
							  {
							  ?>
							 <tr class="trAviso">
                              <td colspan="6">
							  <table width="100%" bordercolor="#DDDDDD" bgcolor="#DDDDDD" class="tablaPrincipal">
								  <tr>
									<td width="4%"><img src="imagenes/copia.gif" width="16" height="16"></td>
									<td>La copia de seguridad mas reciente es: <a href="javascript:enviarDescarga('<?= $idCopiaReciente?>');" title="<?= $tituloCopiaReciente?>" class="link"><b>
									  <?= $nombreCopiaReciente?>
									</b></a>
									</td>
								  </tr>
								</table>
							   </td>
                            </tr>
                            <tr class="trSubTitulo">
                              <td class="trSubTitulo" width="4%"><div align="left"> <input id="todo" type="checkbox" name="chk_todoCopia" onClick="javascript:chekear()" title="Seleccionar todo">
                               </div></td>
                              <td class="trSubTitulo" width="22%"><div align="center">Fecha de Generaci&oacute;n</div></td>
                              <td class="trSubTitulo" width="10%"><div align="center">Tama&ntilde;o</div></td>
                              <td class="trSubTitulo" width="14%"><div align="center">Tipo </div></td>
                              <td class="trSubTitulo" width="35%"><div align="center">Descargar </div></td>
                              <td class="trSubTitulo" width="15%"><div align="center">Eliminar</div></td>
                            </tr>
                            <?
		$contCopias=0;
	  	while(list($idCopia,$fechaCopia,$nombreCopia,$tamanoCopia,$tipoCopia,$archivoCopia)=mysql_fetch_array($consultaCopias))
	  	{
			if(file_exists($archivoCopia))
			{
								if($contCopias%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}
								?>
								<tr class="<?=$color?>">
                              <td class="<?=$color?>"><input id="codigoCopia" type="checkbox" name="chk_idCopia[<?= $contCopias?>]" value="<?= $idCopia?>"></td>
                              <td class="<?=$color?>"><div align="center"><?= $fechaCopia?>
                                </div></td>
                              <td class="<?=$color?>"> <div align="center"><?= (int)($tamanoCopia/1000)." Kb"?> </div></td>
                              <td class="<?=$color?>"><div align="center">
							  <? 
							  	if($tipoCopia == 0)
								{
								?>
									Manual
								      <?
								}
								else
								{
								?>
									Automatica
								    <?
									
								}
							  
							  ?>
								  </div></td>
                              <td class="<?=$color?>"><div align="center"><a href="javascript:enviarDescarga('<?= $idCopia?>');" class="link"> <?= $nombreCopia?>
                              </a></div></td>
                              <td class="<?=$color?>"><div align="center"><a href="javascript:eliminarCopia('<?= $idCopia?>');" class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Copia" width="16" height="16" border="0"></a></div></td>
                            </tr>
                            <?
				$contCopias++;
			}
			else
			{
				$sqlEliminar="DELETE FROM mei_copiaseguridad WHERE idcopiaseguridad = ".$idCopia;
				$baseDatos->ConsultarBD($sqlEliminar);
			}
	 }
	}
	?>
                      <tr class="trInformacion">
                              <td colspan="6">Eliminar Elementos Seleccionados 
                                <input name="btn_eliminarCopia" type="button" id="btn_eliminarCopia" value="Eliminar Copias" onClick="javascript: enviarEliminar();" <?= $activoEliminar?>></td>
                      </tr>
                    </table>
			      </form>
			  </td>
			</tr>
    </table>

<? 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
?>
</body>
</html>