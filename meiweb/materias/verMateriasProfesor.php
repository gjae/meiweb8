<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
	if ($_SESSION['idtipousuario']==2)
	{
			$sql="SELECT DISTINCT(mei_materia.idmateria),mei_materia.nombre,mei_materia.nombrecorto,mei_materia.codigo
					FROM mei_grupo, mei_materia WHERE mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo 
					FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")
					AND mei_grupo.idmateria=mei_materia.idmateria ";
		$consulta=$baseDatos->ConsultarBD($sql);
		$numeroMaterias=mysql_num_rows($consulta);
		
		?>
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<title>MEIWEB</title>
					<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">					
					<script language="javascript">
					
						function eliminarMateria(idmateria)
						{
							if(confirm("Esta seguro de querer eliminar esta materia"))
							{
								location.replace("eliminarMateria.php?idmateria="+idmateria);
							}
						}
						
						function modificarMateria(idmateria)
						{
							location.replace("modificarMateria.php?idmateria="+idmateria);
						}
						
						function crearAlumno(idGrupo)
						{
							location.replace("../usuario/crearAlumno.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idGrupo="+idGrupo);
						}
						
						
						function eliminarGrupo(idGrupo)
						{
							if(confirm("¿Está seguro de querer eliminar este grupo?"))
							{
								location.replace("eliminarGrupo.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idGrupo="+idGrupo);
							}
						}
						
						function modificarGrupo(idGrupo)
						{
							location.replace("modificarGrupo.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idGrupo="+idGrupo);
						}
					
					</script>
				</head>
				<body>
				
			<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']); ?>	</td>
                <td class="tablaEspacio">&nbsp;</td>
			 <td>
					<table class="tablaGeneral">
						<tr class="trTitulo">
						  <td colspan="4"><img src="imagenes/materias.gif" width="16" height="16" align="texttop"> Ver Materias</td>
					  </tr>
					 </table>
					 							
					<?
				if(empty($numeroMaterias))
				{
				?>
					<table class="tablaGeneral">
                       <tr class="trAviso">
                           <td colspan="4" class="trAviso">Actualmente no existen Materias </td>
                      </tr>
					 </table>
				
				<?
				}
				else
				{					
					while(list($idmateria,$nombreMateria,$nombreCorto,$codigoMateria)=mysql_fetch_array($consulta))
					{
							$sql="SELECT mei_grupo.idgrupo,mei_grupo.nombre,mei_grupo.ubicacion,mei_grupo.horario
										FROM mei_grupo, mei_materia WHERE mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo 
										FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")
										AND mei_grupo.idmateria=mei_materia.idmateria AND mei_materia.idmateria=".$idmateria;
							
										
							$consultaGrupos=$baseDatos->ConsultarBD($sql);
							
							$numeroGrupos=mysql_num_rows($consultaGrupos);
					?>
						<table class="tablaGeneral" >
									<tr class="trSubTitulo">
										<td class="trSubTitulo">Materia:</td>
										<td colspan="3" class="trSubTitulo"><?= $nombreMateria?></td>
									</tr>
									<tr class="trSubTitulo">
										<td class="trSubTitulo">C&oacute;digo:</td>
										<td colspan="3" class="trSubTitulo"><?=$codigoMateria?> </td>
									</tr>
										<?
										
										if(empty($numeroGrupos))
										{
										?>
                                          <tr class="trAviso">
                                            <td colspan="4">Actualmente no existen Grupos </td>
                                          </tr>
										 <?
										 }
										 else
										 {
										 ?>
                                          <tr class="trSubTitulo">
                                            <td width="10%" class="trSubTitulo"><div align="center">Grupo</div></td>
                                            <td width="20%" class="trSubTitulo"><div align="center">Ubicaci&oacute;n</div></td>
											 <td width="20%" class="trSubTitulo"><div align="center">N&uacute;mero de Alumnos</div></td>
                                            <td width="50%" class="trSubTitulo"><div align="center">Horario </div></td>
                                          </tr>
										  <?
										  while(list($idGrupo,$nombreGrupo,$ubicacionGrupo,$horarioGrupo)=mysql_fetch_array($consultaGrupos))
										  {
										  		   $sql="SELECT COUNT(*) FROM mei_relusugru WHERE mei_relusugru.idgrupo=".$idGrupo;
													$resultado=$baseDatos->ConsultarBD($sql);
													list($numeroAlumnos)=mysql_fetch_array($resultado);
													
												$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario,mei_relusugru 
														WHERE mei_usuario.idusuario=mei_relusugru.idusuario 
															AND mei_relusugru.idgrupo=".$idGrupo."
																AND mei_usuario.idtipousuario=".$_SESSION['idtipousuario'];

												$consultaProfesor=$baseDatos->ConsultarBD($sql);
												
												$numProfesor=mysql_num_rows($consultaProfesor);
												
												if(!empty($numProfesor))
												{
													list($primerNombreProfesor,$segundoNombreProfesor,$primerApellidoProfesor,$segundoApellidoProfesor)=mysql_fetch_array($consultaProfesor);
													$cadenaProfesor="Profesor: ".$primerNombreProfesor." ".$segundoNombreProfesor." ".$primerApellidoProfesor." ".$segundoApellidoProfesor;
												}
												else
												{
													$cadenaProfesor="No se asignado profesor a este grupo";
												}
												
																								
										  ?>
                                          <tr class="trListaClaro">
                                            <td class="trListaClaro"><?= $nombreGrupo?></td>
                                            <td class="trListaClaro"><?= $ubicacionGrupo?></td>
											<td class="trListaClaro"><div align="center"><?= $numeroAlumnos-1?>
											</div></td>
                                            <td class="trListaClaro"><?= $horarioGrupo?></td>
                                          </tr>
										  	 <tr class="trInformacion">
                                            <td colspan="4"><div align="center">
                                              <input name="btn_agregarAlumno" type="button" value="Agregar Alumno" onClick="javascript: crearAlumno('<?= $idGrupo?>')">
                                              
                                            </div></td>
                                          </tr>

										  <?
										   }
										  ?>
										  <?										   										   
										    }//
										  ?>
							</table><br>
				<?
				}
			}
				?>
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><?=menu1($_SESSION['idtipousuario']); ?>	</td>            
                <td class="tablaEspacioDerecho">&nbsp;</td>              
					</tr>
				</table>
	        </body>
		</html>
	<?
	}
	else
	{
		redireccionar('../login/');
	}
	
	}
	else
	{
		redireccionar('../login/');
	}
?>
