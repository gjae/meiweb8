<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	
	$baseDatos=new BD();
	ini_set('max_execution_time',1800);
	if(comprobarSession())
	{ 
		if($_SESSION['banderaAdmnistrador']==1)
		{
//			if ($_POST['rad_reinicio']==2)
//			{
				if($_GET['mod']==1 )	//borrado de correo
				{
					if($_GET['bol']=='true')
				  	{
						//$sql=" SELECT mei_correo.*, mei_relcorarc.localizacion  FROM mei_correo LEFT JOIN mei_relcorarc ON mei_correo.idcorreo= mei_relcorarc.idcorreo";
						$sql="SELECT * FROM mei_correo, mei_relcorarc WHERE mei_correo.idcorreo = mei_relcorarc.idcorreo AND mei_correo.remitente
								IN (SELECT mei_usuario.idusuario FROM mei_usuario WHERE (mei_usuario.idtipousuario=5 OR mei_usuario.idtipousuario=6))";				
						
						$resultado=$baseDatos->ConsultarBD($sql);
						while($recorrerCorreo=mysql_fetch_assoc($resultado))
						{
						    $idmateria_correo = $recorrerCorreo['idmateria'];  
                            
                            $sql = "SELECT * FROM mei_materia WHERE mei_materia.idmateria=".$idmateria_correo." limit 1";
                            $consulta=$baseDatos->ConsultarBD($sql);
                            list($grupo)=mysql_fetch_array($consulta);
                            if($grupo != 1){
    							if(!empty($recorrerCorreo['localizacion']))
    							{
    								 @unlink('../../datosMEIWEB/archivosCorreo/'.$recorrerCorreo['localizacion']);
    							}
    							//$sql=" TRUNCATE TABLE mei_correo";
    							$sql="DELETE FROM mei_correo WHERE mei_correo.remitente 
    								IN (SELECT mei_usuario.idusuario FROM mei_usuario WHERE (mei_usuario.idtipousuario=5 OR mei_usuario.idtipousuario=6))";
    							$baseDatos->ConsultarBD($sql);                                
                            }

						}
?>		
						<script language="javascript">
                            alert("Ha borrados los correos de los usuarios de Educación Virtual.");
                            window.location.href=("VirReinicioModulos.php");
                        </script>
<?
					}
					else
					{
?>
						<script language="javascript">if(confirm("¿Está seguro que desea eliminar los correos de todos los usuarios de Educación Virtual?"))
							window.location.href=("VirReinicioSemestre.php?mod=1&bol=true");
						</script>
<?
		 			}
				}
				
				if($_GET['mod']==2 )	//borrado de foro (por grupo)
				{
					 //$sql=" SELECT mei_comentarioforo.*, mei_relcomarc.localizacion  FROM mei_comentarioforo 
						//LEFT JOIN mei_relcomarc ON mei_comentarioforo.idcomentario= mei_relcomarc.idcomentario";
					for($i=0;$i<$_POST['hid_contGrupo'];$i++)
					{ 
						if(!empty($_POST['chk_idgrupo'.$i]))
						{ 
							$sql="SELECT mei_comentarioforo.* ,mei_relcomarc.localizacion FROM mei_comentarioforo, mei_relcomarc WHERE mei_comentarioforo.idusuario
								IN ( SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo ='".$_POST['chk_idgrupo'.$i]."' AND mei_relusuvirgru.idusuario 
								IN ( SELECT mei_usuario.idusuario FROM mei_usuario WHERE (mei_usuario.idtipousuario =5 OR mei_usuario.idtipousuario =6)))";					
							
							$resultado=$baseDatos->ConsultarBD($sql);
							while($recorrerforo=mysql_fetch_assoc($resultado))
							{
								
								if(!empty($recorrerforo['localizacion']))
								{
									@unlink('../../datosMEIWEB/archivosForo/'.$recorrerforo['localizacion']);
									$sql=" DELETE FROM mei_relcomarc WHERE mei_relcomarc.idcomentario=".$recorrerforo['idcomentario'];
									$baseDatos->ConsultarBD($sql);
								}
								
								$sql="DELETE FROM mei_comentarioforo WHERE mei_comentarioforo.idusuario
									IN ( SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo ='".$_POST['chk_idgrupo'.$i]."'
									AND mei_relusuvirgru.idusuario IN ( SELECT mei_usuario.idusuario FROM mei_usuario
									WHERE (mei_usuario.idtipousuario=5 OR mei_usuario.idtipousuario=6)))";
								$baseDatos->ConsultarBD($sql);
							}
						}
					}
?>		
					<script language="javascript">
						alert("Ha borrado los comenatrios de los foros de Educación Virtual.");
						window.location.href=("VirReinicioModulos.php");
					</script>
<?
				}
				
				if($_GET['mod']==3 )	//borrado de actividades (por grupo)
				{
					//$sql="SELECT mei_arcusuactividad.*  FROM mei_arcusuactividad WHERE mei_arcusuactividad.idusuario IN (SELECT mei_usuario.idusuario FROM mei_usuario WHERE (mei_usuario.idtipousuario=5 OR mei_usuario.idtipousuario=6))";
					for($i=0;$i<$_POST['hid_contGrupo'];$i++)
					{ 
						if(!empty($_POST['chk_idgrupo'.$i]))
						{ 
							$sql="	SELECT mei_usuactividad.* FROM mei_usuactividad WHERE mei_usuactividad.idactividad
								IN	(SELECT mei_actividad.idactividad FROM mei_actividad WHERE mei_actividad.idevaluacion
								IN	(SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion WHERE mei_evaluacion.tipoevaluacion=2
								AND mei_evaluacion.idevaluacion IN (SELECT mei_evavirgrupo.idevaluacion FROM mei_evavirgrupo
								WHERE mei_evavirgrupo.idvirgrupo='".$_POST['chk_idgrupo'.$i]."')))";
							
							$resultado=$baseDatos->ConsultarBD($sql);
							while($recorrerActividad=mysql_fetch_assoc($resultado))
							{
								if(!empty($recorrerActividad['localizacion']))
								{
									 @unlink('../../datosMEIWEB/archivosActividades/archivosRespuestas/'.$recorrerActividad['localizacion']);
								}
								$sql="DELETE FROM mei_usuactividad WHERE mei_usuactividad.idactividad
									IN	(SELECT mei_actividad.idactividad FROM mei_actividad WHERE mei_actividad.idevaluacion
									IN	(SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion WHERE mei_evaluacion.tipoevaluacion=2
									AND mei_evaluacion.idevaluacion IN (SELECT mei_evavirgrupo.idevaluacion FROM mei_evavirgrupo
									WHERE mei_evavirgrupo.idvirgrupo='".$_POST['chk_idgrupo'.$i]."')))";
								$baseDatos->ConsultarBD($sql);
							}
						}
					}
?>
					<script language="javascript">
						alert("Ha borrado la respuesta a las Actividades de Educación Virtual.");
						window.location.href=("VirReinicioModulos.php");
					</script>
<?
				}
				
				if($_GET['mod']==4 )	//borrado de usuarios (por grupo)
				{
/*					if($_GET['bol']=='true')
				  	{ */
						for($i=0;$i<$_POST['hid_contGrupo'];$i++)
						{ 
							if(!empty($_POST['chk_idgrupo'.$i]))
							{ 
								$sql="SELECT mei_usuario.idusuario FROM mei_usuario WHERE mei_usuario.idtipousuario=6 AND mei_usuario.idusuario 
										IN (SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo='".$_POST['chk_idgrupo'.$i]."')";
								$resultado=$baseDatos->ConsultarBD($sql);
								while (list($idusuario)=mysql_fetch_array($resultado))
								{
									$sql="SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario='".$idusuario."' 
											AND mei_relusuvirgru.idvirgrupo<> '".$_POST['chk_idgrupo'.$i]."'";
									$respuesta=$baseDatos->ConsultarBD($sql); 
									$numeroGrupos=mysql_num_rows($respuesta);
									if (empty($numeroGrupos))
									{
										$sql="DELETE FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario='".$idusuario."'";
										$baseDatos->ConsultarBD($sql);
										
										$sql="DELETE FROM mei_usuario WHERE mei_usuario.idusuario='".$idusuario."'";
										$baseDatos->ConsultarBD($sql);
									}
									else
									{
										$sql="DELETE FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario='".$idusuario."' AND mei_relusuvirgru.idvirgrupo='".$_POST['chk_idgrupo'.$i]."'";
										$baseDatos->ConsultarBD($sql);
									}
								}
							}
						}
						echo("<script>alert('Ha borrado la informacion de los estudiantes pertenecientes a los grupos Virtuales seleccionados.');
							window.location.href=('VirReinicioModulos.php');</script>");
/*					}
					else
					{
?>
						<script language="javascript">if(confirm("Desea eliminar los usuarios de este grupo con sus respectivas notas?"))
							window.location.href=("VirReinicioSemestre.php?mod=4&bol=true");
						</script>
<?
		 			}*/
				}
//			}			
		}
			//redireccionar('../materias/VirReinicioModulos.php');
	}
	else
	{
		redireccionar('../login/');
	}
?>