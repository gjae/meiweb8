<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		?>
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">					
					<title>MEIWEB</title>
					<script language="javascript">
                        function guardarmateriacisco(){
                            var codigomateria = document.getElementById("codmateria").value;
                            var nivelcisco = document.getElementById("nivelcisco").value;
                            
                            if( (codigomateria != "")&&(nivelcisco != "") ){
                                document.getElementById("ciscomat").submit();
                            }
                            else{
                                alert("Por favor llene todos los campos");
                            }
                        }
                        
                        function guardarmateriarecurso(){
                            var recurso = document.getElementById("nombrerecurso").value;
                            var directorio = document.getElementById("posicionrecurso").value;
                            
                            if( (recurso != "")&&(directorio != "") ){
                                document.getElementById("agregarRecurso").submit();
                            }
                            else{
                                alert("Por favor llene todos los campos");
                            }
                        }
                        
                        function soloNumeros(e) {
                            key = e.keyCode || e.which;
                            tecla = String.fromCharCode(key).toLowerCase();
                            letras = " 1234567890";
                            especiales = [8, 37, 39, 46];
                        
                            tecla_especial = false
                            for(var i in especiales) {
                                if(key == especiales[i]) {
                                    tecla_especial = true;
                                    break;
                                }
                            }
                        
                            if(letras.indexOf(tecla) == -1 && !tecla_especial)
                             {   
                        		alert("Digite solo numero");
                        		return false;	 
                        	 }
                        }
                        function eliminarmateriacisco(idmateria){
                            var r = confirm("Esta seguro que desea eliminar la materia en cisco");
                            if (r == true) {
                                document.getElementById("codmateliminar").value = idmateria;
                                document.getElementById("eliminarmatcisco").submit();
                            }                            
                        }   
                        
                        function eliminarrecurso(idrecurso){
                            var r = confirm("Esta seguro que desea eliminar el recurso");
                            if (r == true) {
                                document.getElementById("codmateliminarrecurso").value = idrecurso;
                                document.getElementById("eliminarmatrecurso").submit();
                            }                            
                        }                     
					</script>
				</head>
				<body>
                <?php
                    if(isset($_POST['codmateria'])){
                        $sql = "SELECT * FROM mei_cisco WHERE mei_cisco.idmateria=".$_POST['codmateria']." AND mei_cisco.idrecurso=".$_POST['nivelcisco'];
                        $verficar = $baseDatos->ConsultarBD($sql);
                        $contverificar = mysql_num_rows($verficar);
                        if( $contverificar == 0){
                            $sql = "INSERT INTO mei_cisco(idmateria,idrecurso) values (".$_POST['codmateria'].",".$_POST['nivelcisco'].")";
                            $baseDatos->ConsultarBD($sql);                            
                        }

                    }
                    elseif(isset($_POST['nombrerecurso'])){
                        $sql = "SELECT * FROM mei_recursos WHERE mei_recursos.recurso=".$_POST['nombrerecurso'];
                        $verficar = $baseDatos->ConsultarBD($sql);
                        $contverificar = mysql_num_rows($verficar);
                        if( $contverificar == 0){
                            $sql = "INSERT INTO mei_recursos(recurso,directorio) values ('".$_POST['nombrerecurso']."','".$_POST['posicionrecurso']."')";
                            $baseDatos->ConsultarBD($sql);                            
                        }

                    }                    
                    elseif(isset($_POST['codmateliminar'])){
                        $sql="DELETE FROM mei_cisco where mei_cisco.id =".$_POST['codmateliminar'];
                        $baseDatos->ConsultarBD($sql);
                    }
                    elseif(isset($_POST['codmateliminarrecurso'])){
                        $sql="DELETE FROM mei_recursos where mei_recursos.id =".$_POST['codmateliminarrecurso'];
                        $baseDatos->ConsultarBD($sql);
                    }                                        
                ?>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td><img src="imagenes/materias.gif" width="16" height="16">Recursos</td>
						</tr>
						
					</table>
                    <table width="39%" class="tablaGeneral">
                     <tr class="trInformacion">
                          
                     </tr></table>
                     
                     <br/>
                  <form name="agregarRecurso" id="agregarRecurso" method="POST" action="">
                  <table class="tablaGeneral">
                    <tr class="trSubTitulo">
                      <td colspan="4">Agregar Recurso</td>
                    </tr>
                    <tr class="trInformacion">
                      <td width="25%">&nbsp;</td>
                      <td width="25%">&nbsp;</td>
                      <td width="25%">&nbsp;</td>
                      <td width="25%">&nbsp;</td>
                    </tr>
                    <tr class="trInformacion">
                      <td align="left" valign="middle" title="Este es el nombre que verá el estudiante para ingresar al recurso"><strong>Nombre del Recurso:</strong></td>
                      <td align="left" valign="middle">
                            <input type="text" id="nombrerecurso" name="nombrerecurso"/>
  
                      </td>
                      <td align="left" valign="middle" title="La ruta es a partir de la carpeta principal Meiweb"><strong>Agregar Ruta del Directorio:</strong></td>
                      <td align="left" valign="middle">
                            <input type="text" id="posicionrecurso" name="posicionrecurso"/>  
                      </td>            
                    </tr> 
                    <tr class="trInformacion">
                      <td width="100%" colspan="4">
                        <?php
                            if(isset($_POST['nombrerecurso'])){
                                echo "Ha agregador un recurso";
                            }
                        ?>                      
                      </td>
                    </tr>  
                    <tr class="trInformacion">
                        <td colspan="4" align="left" valign="middle" style="text-align: center;">
                            <input type="button" value="Agregar Recurso" onclick="guardarmateriarecurso()" />
                        </td>
                    </tr>           
                  </table>
			     </form>                     
                     
                  <form name="ciscomat" id="ciscomat" method="POST" action="">
                  <table class="tablaGeneral">
                    <tr class="trSubTitulo">
                      <td colspan="4">Agregar Materia a Recurso</td>
                    </tr>
                    <tr class="trInformacion">
                      <td width="25%">&nbsp;</td>
                      <td width="25%">&nbsp;</td>
                      <td width="25%">&nbsp;</td>
                      <td width="25%">&nbsp;</td>
                    </tr>
                    <tr class="trInformacion">
                      <td align="left" valign="middle"><strong>Codigo de la materia:</strong></td>
                      <td align="left" valign="middle">
                            <input type="text" id="codmateria" name="codmateria" onkeypress="return soloNumeros(event)"/>
  
                      </td>
                      <td align="left" valign="middle"><strong>Nombre del Recurso:</strong></td>
                      <td align="left" valign="middle">
                        <select name="nivelcisco" id="nivelcisco">
                            <option value="" selected=""></option>
                            <?php
                                $sql = "SELECT mei_recursos.id, mei_recursos.recurso FROM mei_recursos ORDER BY mei_recursos.recurso ASC";
                                $datosrecurso = $baseDatos->ConsultarBD($sql);
                                while(list($idrecurso,$recurso)=mysql_fetch_array($datosrecurso)){
                            ?>
                                <option value="<?=$idrecurso?>" ><?=$recurso?></option>
                            <?php
                                }
                            ?>
                        </select>   
                      </td>            
                    </tr> 
                    <tr class="trInformacion">
                      <td width="100%" colspan="4">
                        <?php
                            if(isset($_POST['codmateria'])){
                                echo "Ha agregado una materia a un recurso";
                            }
                        ?>                      
                      </td>
                    </tr>  
                    <tr class="trInformacion">
                        <td colspan="4" align="left" valign="middle" style="text-align: center;">
                            <input type="button" value="Agregar Materia Cisco" onclick="guardarmateriacisco()" />
                        </td>
                    </tr>           
                  </table>
			     </form>
                 
                 <form name="eliminarmatrecurso" id="eliminarmatrecurso" method="POST" action="">
                    <input type="hidden" name="codmateliminarrecurso" id="codmateliminarrecurso" value="" />
                 </form>
                  <table class="tablaGeneral">
                    <tr class="trSubTitulo">
                      <td colspan="4">Lista de Recursos</td>
                    </tr>
                    <tr class="trInformacion">
                      <td width="25%" align="center"><strong>Codigo</strong></td>
                      <td width="25%" align="center"><strong>Nombre del Recurso</strong></td>
                      <td width="25%" align="center"><strong>Directorio</strong></td>
                      <td width="25%" align="center"><strong>Eliminar</strong></td>
                    </tr>
                    <?php
                        $sql = "SELECT mei_recursos.id, mei_recursos.recurso, mei_recursos.directorio FROM mei_recursos ORDER BY mei_recursos.recurso ASC";
                        $datosrecurso = $baseDatos->ConsultarBD($sql);
                        while(list($idrecurso,$recurso,$directorio)=mysql_fetch_array($datosrecurso)){
                    ?>
                    <tr class="trInformacion">
                      <td align="center" valign="middle"><?=$idrecurso?></td>
                      <td align="center" valign="middle"><?=$recurso?></td>
                      <td align="center" valign="middle"><?=$directorio?></td>
                      <td align="center" valign="middle"><a href="#" onclick="eliminarrecurso('<?=$idrecurso?>')"><img src="../evaluaciones/imagenes/eliminar.gif" alt="eliminar" width="16" height="16" border="0"></a></a></td>            
                    </tr>                     
                    <?php
                        }
                    ?>
                    <tr class="trInformacion">
                      <td width="100%" colspan="4">
                       
                      </td>
                    </tr>  
                  </table>                  
                 
                 <form name="eliminarmatcisco" id="eliminarmatcisco" method="POST" action="">
                    <input type="hidden" name="codmateliminar" id="codmateliminar" value="" />
                 </form>
                  <table class="tablaGeneral">
                    <tr class="trSubTitulo">
                      <td colspan="4">Lista de Materias Cisco</td>
                    </tr>
                    <tr class="trInformacion">
                      <td width="25%" align="center"><strong>Codigo</strong></td>
                      <td width="25%" align="center"><strong>Nombre de la materia</strong></td>
                      <td width="25%" align="center"><strong>Nombre del Recurso</strong></td>
                      <td width="25%" align="center"><strong>Eliminar</strong></td>
                    </tr>
                    <?php
                        $sql = "SELECT mei_cisco.id,mei_cisco.idmateria, mei_cisco.idrecurso FROM mei_cisco ORDER BY mei_cisco.idmateria ASC";
                        $datoscisco = $baseDatos->ConsultarBD($sql);
                        while(list($idmat,$idmateriacisco,$idrecurso)=mysql_fetch_array($datoscisco)){
                            $sql = "SELECT mei_materia.nombre  FROM mei_materia WHERE mei_materia.codigo=".$idmateriacisco." limit 1";
                            $nombremateria = $baseDatos->ConsultarBD($sql);                            
                            list($nombremateriacisco)=mysql_fetch_array($nombremateria);
                            
                            $sqlrecurso = "SELECT mei_recursos.recurso  FROM mei_recursos WHERE mei_recursos.id=".$idrecurso." limit 1";
                            $nombremateriarec = $baseDatos->ConsultarBD($sqlrecurso);                            
                            list($nombrerecurso)=mysql_fetch_array($nombremateriarec);                            
                    ?>
                    <tr class="trInformacion">
                      <td align="center" valign="middle"><?=$idmateriacisco?></td>
                      <td align="center" valign="middle"><?=$nombremateriacisco?></td>
                      <td align="center" valign="middle"><?=$nombrerecurso?></td>
                      <td align="center" valign="middle"><a href="#" onclick="eliminarmateriacisco('<?=$idmat?>')"><img src="../evaluaciones/imagenes/eliminar.gif" alt="eliminar" width="16" height="16" border="0"></a></a></td>            
                    </tr>                     
                    <?php
                        }
                    ?>
                    <tr class="trInformacion">
                      <td width="100%" colspan="4">
                       
                      </td>
                    </tr>  
                  </table>                 
				</td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho">&nbsp;</td>            
                <td class="tablaEspacioDerecho">&nbsp;</td>                            
					</tr>
				</table>
	        </body>
		</html>
	<?
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>
