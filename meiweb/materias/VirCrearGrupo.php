<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	$tipoProfesor=5;
if(comprobarSession())
{
	if($_SESSION['banderaAdmnistrador']==1)
	{
		if(!empty($_GET['idMateria']))
		{
			$sql="SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idMateria'];
			$consultaMateria=$baseDatos->ConsultarBD($sql);
			list($nombreMateria)=mysql_fetch_array($consultaMateria);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
	<script language="javascript">
		function enviarCrear()
		{
			if(comprobarDatos())
			{
				document.frm_crearGrupo.submit();
			}
		}
				
		function enviarCancelar()
		{
			location.replace("VirVerMateriasAdministrador.php");
		}
				
		function comprobarDatos()
		{
			if(document.frm_crearGrupo.txt_codigoGrupo.value == false)
			{
				alert("Debe llenar completamente la información solicitada");
				document.frm_crearGrupo.txt_codigoGrupo.focus();
			}
			else if (document.frm_crearGrupo.cbo_profesor.value==0)
			{
				alert("Debe seleccionar un profesor");
				document.frm_crearGrupo.cbo_profesor.focus();
			}
			else
			{
				return true;
			}
		}								

	</script>
	<style type="text/css">
		<!--
			.Estilo1 {color: #FF0000}
		-->
	</style>
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><? menu(1);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td>
				<table class="tablaGeneral">
					<form action="VirInsertarGrupo.php?idMateria=<?= $_GET['idMateria']?>" method="post" name="frm_crearGrupo">
					<tr class="trTitulo">
						<td colspan="3" class="trTitulo">Crear nuevo Grupo en <?= $nombreMateria?> </td>
					</tr>
<?
			if($_GET['error'] == '0x001')
			{
?>
					<tr class="trAviso">
						<td width="7%"><img src="imagenes/error.gif" width="17" height="18"></td>
						<td colspan="2"><span class="Estilo1">Error: Ya existe un grupo registrado con ese nombre </span></td>
					</tr>
<?
			}
?>
					<tr class="trInformacion">
						<td colspan="2"><b>Nombre del Grupo :</b></td>
						<td width="69%"><input name="txt_codigoGrupo" type="text" id="txt_codigoGrupo" value="<?= $_GET['codigoGrupo']?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><b>Agregar Profesor:</b>
							<table width="100%" bordercolor="#DDDDDD" bgcolor="#DDDDDD" class="tablaPrincipal">
								<tr>
                        			<td width="38%" align="right">Profesores Educaci&oacute;n Virtual: </td>
									<td width="62%"><select name="cbo_profesor" id="cbo_profesor">
													<option value="0" selected>-------Seleccionar-------</option>
<?
			$sql="SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido, mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idtipousuario=".$tipoProfesor;
			$consultaProfesor=$baseDatos->ConsultarBD($sql);
			while(list($idProfesor,$primerNombreProfesor,$segundoNombreProfesor,$primerApellidoProfesor,$segundoApellidoProfesor)=mysql_fetch_array($consultaProfesor))
			{
?>
													<option value="<?= $idProfesor?>"><?=$primerNombreProfesor." ".$segundoNombreProfesor." ".$primerApellidoProfesor." ".$segundoApellidoProfesor.""?></option>
<?			
			}			
?>
													</select></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center"><input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">
						<input name="btn_crearGrupo" type="button" id="btn_crearGrupo" value="Crear Grupo" onClick="javascript: enviarCrear();">
						<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div></td>
					</tr>
					</form>
				</table>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho">&nbsp;</td>            
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<? 
		}
		else
		{
			redireccionar('../materias/VirVerMateriasAdministrador.php');			
		}
	}
	else
	{
		redireccionar('../login/');
	}
}
else
{
	redireccionar('../login/');
}
?>