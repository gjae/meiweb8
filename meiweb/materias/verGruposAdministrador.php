<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		$tipoProfesor=2;
		
		if(!empty($_GET['idMateria']))
		{
			$sql="SELECT DISTINCT mei_materia.idmateria,mei_materia.nombre,mei_materia.nombrecorto,mei_materia.codigo 
					FROM mei_materia 
						WHERE mei_materia.idmateria=".$_GET['idMateria']." ORDER BY (mei_materia.nombre)";
		}
		else
		{
			$sql="SELECT DISTINCT mei_materia.idmateria,mei_materia.nombre,mei_materia.nombrecorto,mei_materia.codigo,mei_materia.codigosec FROM mei_materia WHERE mei_materia.grupo = 1 ORDER BY (mei_materia.nombre)";
		}
		
		$consulta=$baseDatos->ConsultarBD($sql);
		$numeroMaterias=mysql_num_rows($consulta);
		?>
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">					
					<title>MEIWEB</title>
					<script language="javascript">
					
						function eliminarMateria(idMateria)
						{
							if(confirm("Esta seguro de querer eliminar esta materia"))
							{
								location.replace("eliminarMateria.php?idMateria="+idMateria);
							}
						}
						
						function modificarMateria(idMateria)
						{
							location.replace("modificarMateria.php?idMateria="+idMateria);
						}
						
						function crearGrupo(idMateria)
						{
							location.replace("crearGrupo.php?idMateria="+idMateria);
						}
						
						function eliminarGrupo(idGrupo,idMateria)
						{
							if(confirm("¿Está seguro de querer eliminar este grupo?"))
							{
								location.replace("eliminarGrupo.php?idGrupo="+idGrupo+"&idmateria="+idMateria);
							}
						}
						
						function modificarGrupo(idGrupo)
						{
							location.replace("modificarGrupo.php?idGrupo="+idGrupo);
						}
						function reinicioSemestre()
						{
						  if(confirm("¿Está seguro que desea hecer un reinicio de semestre?"))
						  {
						    location.replace('../materias/reinicioSemestre.php');
						   }
						  
						}
						
					
					</script>
				</head>
				<body>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td><img src="imagenes/materias.gif" width="16" height="16">Ver Materias</td>
						</tr>
						
					</table>
                    <table width="39%" class="tablaGeneral">
                     <tr class="trInformacion">
                          
                     </tr></table>
					<?
					if(empty($numeroMaterias))
					{
					?>
					<table width="39%" class="tablaGeneral">
                     <tr class="trInformacion">
                          <td colspan="6">Actualmente no existen Materias</div></td>
                     </tr></table>
					<?
					}
					else
					{
					while(list($idMateria,$nombreMateria,$nombreCorto,$codigoMateria,$codigoMateriasec)=mysql_fetch_array($consulta))
					{
							$sql="SELECT DISTINCT mei_grupo.idgrupo, mei_grupo.nombre, mei_grupo.ubicacion, mei_grupo.horario
									FROM mei_grupo
										WHERE mei_grupo.idmateria =".$idMateria;
							
										
							$consultaGrupos=$baseDatos->ConsultarBD($sql);
							
							$numeroGrupos=mysql_num_rows($consultaGrupos);
					?>
					<table width="39%" class="tablaGeneral">
					
                         <tr class="trInformacion">
							<td colspan="2"><b>Materia</b></td>
							<td colspan="2"><?= $nombreMateria?></td>
							<td width="12%" rowspan="3%"><div align="right"><strong>Editar Materia
							  </strong>
							  <table width="60" align="center" class="Principal">
                                <tr>
                                  <td><div align="center"><a href="javascript: modificarMateria('<?= $idMateria?>');"><img src="imagenes/modificar.gif" alt="Modificar Materia" width="16" height="16" border="0"></a></div></td>
                                  <td><div align="center"><a href="javascript: eliminarMateria('<?= $idMateria?>');"><img src="imagenes/eliminar.gif" alt="Eliminar Materia" width="16" height="16" border="0"></a></div></td>
                                </tr>
                              </table>
							</div></td>
						</tr>
                         <tr class="trInformacion">
							<td colspan="2"><b>C&oacute;digo</b></td>
							<td colspan="2"><?= $codigoMateria?></td>
							
						</tr>
						<tr class="trInformacion">
							<td colspan="2"><b>C&oacute;digo Secundario</b></td>
							<td colspan="2"><?= $codigoMateriasec?></td>
							
						</tr>
                         <tr class="trInformacion">
							<td colspan="2"><b>Nombre Corto</b></td>
							<td colspan="3" ><?= $nombreCorto?></td>
							
						</tr>
										<?
										
										if(empty($numeroGrupos))
										{
										?>
                                          <tr class="trInformacion">
                                            <td  colspan="2"><div align="center">&nbsp;</div></td>
                                            <td width="43%">Actualmente no existen Grupos                                              </div></td>
                                            <td width="31%" ><div align="center">&nbsp; </div></td>
                                            <td width="12%"><div align="center">&nbsp;</div></td>
                                          </tr>
										 <?
										 }
										 else
										 {
										 ?>
                                          <tr class="trSubTitulo">
                                            <td class="trSubTitulo" colspan="2"><div align="center">Grupos</div></td>
                                            <td width="43%" class="trSubTitulo"><div align="center">Ubicaci&oacute;n</div></td>
                                            <td width="31%" class="trSubTitulo"><div align="center">Horario </div></td>
                                            <td width="12%" class="trSubTitulo"><div align="center">Editar Grupo </div></td>
                                          </tr>
										  <?
										  while(list($idGrupo,$nombreGrupo,$ubicacionGrupo,$horarioGrupo)=mysql_fetch_array($consultaGrupos))
										  {
										  		    
												$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario,mei_relusugru 
														WHERE mei_usuario.idusuario=mei_relusugru.idusuario 
															AND mei_relusugru.idgrupo=".$idGrupo."
																AND mei_usuario.idtipousuario=".$tipoProfesor;

												$consultaProfesor=$baseDatos->ConsultarBD($sql);
												
												$numProfesor=mysql_num_rows($consultaProfesor);
												
												if(!empty($numProfesor))
												{
													list($primerNombreProfesor,$segundoNombreProfesor,$primerApellidoProfesor,$segundoApellidoProfesor)=mysql_fetch_array($consultaProfesor);
													$cadenaProfesor="Profesor: ".$primerNombreProfesor." ".$segundoNombreProfesor." ".$primerApellidoProfesor." ".$segundoApellidoProfesor;
												}
												else
												{
													$cadenaProfesor="No se asignado profesor a este grupo";
												}
												
																								
										  ?>
                                          <tr class="trListaClaro">
                                            <td class="trListaClaro" colspan="2"><?= $nombreGrupo?>
                                            <div align="center"></div></td>
                                            <td class="trListaClaro"><?= $ubicacionGrupo?>
                                            <div align="center"></div></td>
                                            <td class="trListaClaro"><?= $horarioGrupo?>
                                            <div align="center"></div></td>
                                            <td class="trListaClaro"><div align="center">
                                              <table width="60" align="center" class="tablaPrincipal">
                                                <tr>
                                                  <td width="50%"><div align="center"><a href="javascript: modificarGrupo('<?= $idGrupo?>')"><img src="imagenes/modificar.gif" alt="Modificar Grupo" width="16" height="16" border="0"></a></div></td>
                                                  <td width="50%"><div align="center"><a href="javascript: eliminarGrupo('<?= $idGrupo?>','<?= $idMateria?>')"><img src="imagenes/eliminar.gif" alt="Eliminar Grupo" width="16" height="16" border="0"></a></div></td>
                                                </tr>
                                              </table>
                                            </div></td>
                                          </tr>
                                          <tr class="trInformacion">
                                            <td colspan="2">&nbsp;</td>
                                            <td colspan="2"><?= $cadenaProfesor?></td>
                                            <td>&nbsp;</td>
                                          </tr>
										  <?
										   }
										  ?>
                                         
										  <?										   										   
										    }//
										  ?>
										   <tr class="trInformacion">
                                            <td colspan="5" class="trInformacion"><div align="center">
                                              <input name="btn_agregarGrupo" type="button" value="Agregar Grupo" onClick="javascript: crearGrupo('<?= $idMateria?>')">
                                            </div></td>
									</tr>
								</table><br>
				<?
				}
				}
				?>
				</td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho">&nbsp;</td>            
                <td class="tablaEspacioDerecho">&nbsp;</td>                            
					</tr>
				</table>
	        </body>
		</html>
	<?
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>
