<?
	include_once('../baseDatos/BD.class.php');  
	include_once('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	
	$baseDatos=new BD();
	 
if(comprobarSession())
{
	if($_SESSION['banderaAdmnistrador']==1)
	{
		$tipoProfesor=5;
		if(!empty($_GET['idMateria']))
		{
			$sql="SELECT DISTINCT mei_virmateria.idvirmateria,mei_virmateria.nombre,mei_virmateria.nombrecorto,mei_virmateria.codigo,mei_virmateria.codigosec, mei_virmateria.formato, mei_virmateria.numeromod, mei_virmateria.fechainicio, mei_virmateria.fechafin, mei_virmateria.requisitos, mei_virmateria.introduccion, mei_virmateria.ofertar, mei_virmateria.precio FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idMateria']." ORDER BY (mei_virmateria.nombre)";
		}
		else
		{
			$sql="SELECT DISTINCT mei_virmateria.idvirmateria,mei_virmateria.nombre,mei_virmateria.nombrecorto,mei_virmateria.codigo,mei_virmateria.codigosec, mei_virmateria.formato, mei_virmateria.numeromod, mei_virmateria.fechainicio, mei_virmateria.fechafin, mei_virmateria.requisitos, mei_virmateria.introduccion, mei_virmateria.ofertar, mei_virmateria.precio FROM mei_virmateria ORDER BY (mei_virmateria.nombre)";
		}
		$consulta=$baseDatos->ConsultarBD($sql);
		$numeroMaterias=mysql_num_rows($consulta);
		
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<title>MEIWEB</title>
<script language="javascript">
	function eliminarMateria(idMateria)
	{
		if(confirm("Esta seguro de querer eliminar este Curso"))
		{
			location.replace("VirEliminarMateria.php?idMateria="+idMateria);
		}
	}
						
	function modificarMateria(idMateria)
	{
		location.replace("VirModificarMateria.php?idMateria="+idMateria);
	}
					
	function crearGrupo(idMateria)
	{
		location.replace("VirCrearGrupo.php?idMateria="+idMateria);
	}
						
	function eliminarGrupo(idGrupo)
	{
		if(confirm("¿Está seguro de querer eliminar este grupo?"))
		{
			location.replace("VirEliminarGrupo.php?idGrupo="+idGrupo);
		}
	}
						
	function modificarGrupo(idGrupo)
	{
		location.replace("VirModificarGrupo.php?idGrupo="+idGrupo);
	}

	function agregarAlumnos(idGrupo)
	{
		location.replace("../usuario/VirCrearAlumno.php?idGrupo="+idGrupo);				
	}
	
	function reinicioSemestre()
	{
		if(confirm("¿Está seguro que desea hecer un reinicio de semestre?"))
		{
		    location.replace('../materias/reinicioSemestre.php');
		}
	}
</script>
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><?=menu(1);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td>
				<table class="tablaGeneral" >
					<tr>
						<td class="trTitulo"><img src="imagenes/materias.gif" width="16" height="16"> Cursos Virtuales</td>
					</tr>
				</table><br>
<?
		if(empty($numeroMaterias))
		{
?>
				<table class="tablaGeneral">
					<tr class="trInformacion">
						<td colspan="6">Actualmente no existen Cursos</td>
					</tr>
				</table>
<?
		}
		else
		{
			while(list($idMateria,$nombreMateria,$nombreCorto,$codigoMateria,$codigoMateriasec,$formato,$modulos,$fecha_ini,$fecha_fin,$requisitos,$introduccion,$ofertar,$precio)=mysql_fetch_array($consulta))
			{
				$sql="SELECT DISTINCT mei_virgrupo.idvirgrupo, mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$idMateria;
				$consultaGrupos=$baseDatos->ConsultarBD($sql);
				$numeroGrupos=mysql_num_rows($consultaGrupos);
?>
				<table class="tablaGeneral">
					<tr class="trInformacion">
						<td colspan="2"><b>Curso:</b></td>
						<td colspan="2"><b><?= $nombreMateria?></b></td>
<?	 			if($ofertar==1)
				{
?>
						<td><b>Ofertado</b></td>
<?
				}
				else
				{
?>				
						<td><b>Sin Ofertar</b></td>
<?
				}
?>				
						<td width="17%" rowspan="4" align="center"><div align="center"><strong>Editar Curso</strong>
							<table width="60" align="center" class="Principal">
								<tr>
									<td><div align="center"><a href="javascript: modificarMateria('<?= $idMateria?>');">
											<img src="imagenes/modificar.gif" alt="Modificar Materia" width="16" height="16" border="0"></a></div></td>
									<td><div align="center"><a href="javascript: eliminarMateria('<?= $idMateria?>');"> 
											<img src="imagenes/eliminar.gif" alt="Eliminar Materia" width="16" height="16" border="0"></a></div></td>
								</tr>
							</table></div>
						</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Nombre Corto:</b></td>
						<td colspan="2"><?= $nombreCorto?></td>
						<td><b>Precio: $<?echo $precio;?></b></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>C&oacute;digo:</b></td>
						<td width="17%"><?= $codigoMateria?></td>
						<td width="24%"><b>C&oacute;digo Secundario:</b></td>
						<td><?= $codigoMateriasec?></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Formato:</b></td>
						<td><?= $formato ?></td>
						<td><b>N&deg; Modulos:</b></td>
						<td><?= $modulos?></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Fecha inicio:</b></td>
						<td><?= $fecha_ini ?></td>
						<td><b>Fecha finalizaci&oacute;n:</b></td>
						<td><?= $fecha_fin ?></td>
						<td width="17%"></td>
					</tr>
<?
				if(empty($numeroGrupos))
				{
?>
					<tr class="trInformacion">
						<td  colspan="2"><div align="center">&nbsp;</div></td>
						<td colspan="2">Actualmente no existen Grupos</div></td>
						<td width="20%" ><div align="center">&nbsp; </div></td>
						<td width="17%"><div align="center">&nbsp;</div></td>
					</tr>
<?
				}
				else
				{
?>
					<tr class="trSubTitulo">
						<td class="trSubTitulo" colspan="2"><div align="center">Grupo</div></td>
						<td colspan="2" class="trSubTitulo"><div align="center">Docente</div></td>
						<td width="20%" class="trSubTitulo"><div align="center">N° Estudiantes</div></td>
						<td width="17%" class="trSubTitulo"><div align="center">Editar Grupo </div></td>
					</tr>
<?
					while(list($idGrupo,$nombreGrupo)=mysql_fetch_array($consultaGrupos))
					{
						$sql="SELECT  COUNT(mei_usuario.idusuario) FROM mei_usuario WHERE mei_usuario.idtipousuario=6 AND mei_usuario.idusuario IN (
SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$idGrupo.")";
						$consultaAlumnos=$baseDatos->ConsultarBD($sql);
						list($numeroAlumnos)=mysql_fetch_array($consultaAlumnos);
						$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
								FROM mei_usuario,mei_relusuvirgru WHERE mei_usuario.idusuario=mei_relusuvirgru.idusuario 
									AND mei_relusuvirgru.idvirgrupo=".$idGrupo." AND mei_usuario.idtipousuario=".$tipoProfesor;
						$consultaProfesor=$baseDatos->ConsultarBD($sql);
						list($primerNombreProfesor,$segundoNombreProfesor,$primerApellidoProfesor,$segundoApellidoProfesor)=mysql_fetch_array($consultaProfesor);
						if(empty($primerNombreProfesor) && empty($segundoNombreProfesor) && empty($primerApellidoProfesor) && empty($segundoApellidoProfesor))
						{
							$cadenaProfesor="No se ha asignado profesor a este grupo";
						}
						else
						{
							$cadenaProfesor=" ".$primerNombreProfesor." ".$segundoNombreProfesor." ".$primerApellidoProfesor." ".$segundoApellidoProfesor;

						}
?>
					<tr class="trListaClaro">
						<td class="trListaClaro" colspan="2"><?= $nombreGrupo?><div align="center"></div></td>
						<td colspan="2" class="trListaClaro"><?= $cadenaProfesor?><div align="center"></div></td>
						<td class="trListaClaro" align="center"><?= $numeroAlumnos?><div align="center"></div></td>
						<td class="trListaClaro"><div align="center">
							<table width="60" align="center" class="tablaPrincipal">
								<tr>
									<td width="30"><div align="center"><a href="javascript: agregarAlumnos('<?= $idGrupo?>');"> 
											<img src="imagenes/cargar.png" alt="Agregar Alumnos" width="16" height="16" border="0"></a></div></td>
									<td width="30%"><div align="center"><a href="javascript: modificarGrupo('<?= $idGrupo?>')">
                                    			<img src="imagenes/modificar.gif" alt="Modificar Grupo" width="16" height="16" border="0"></a></div></td>
									<td width="30%"><div align="center"><a href="javascript: eliminarGrupo('<?= $idGrupo?>')">
                                    			<img src="imagenes/eliminar.gif" alt="Eliminar Grupo" width="16" height="16" border="0"></a></div></td>
								</tr>
							</table></div>
						</td>
					</tr>
<?
					}
				}
?>
					<tr class="trInformacion">
						<td colspan="6" class="trInformacion"><div align="center">
							<input name="btn_agregarGrupo" type="button" value="Agregar Grupo" onClick="javascript: crearGrupo('<?= $idMateria?>')"></div>
						</td>
					</tr>
				</table><br>
<?
			}
		}
?>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho">&nbsp;</td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
	}
	else
	{
		redireccionar('../login/');
	}
}
else
{
	redireccionar('../login/');
}
?>