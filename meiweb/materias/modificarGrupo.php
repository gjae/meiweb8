<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	
	$tipoProfesor=2;
	 
	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		if(!empty($_GET['idGrupo']))
		{
			
			$sql="SELECT DISTINCT mei_usuario.idusuario,mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido, mei_usuario.segundoapellido
					FROM mei_usuario, mei_relusugru
						WHERE mei_usuario.idusuario = mei_relusugru.idusuario
								AND mei_usuario.idtipousuario= ".$tipoProfesor." 
									AND mei_relusugru.idgrupo =".$_GET['idGrupo'];
			
			$consultaProfesor=$baseDatos->ConsultarBD($sql);
			$numProfesorActivo=mysql_num_rows($consultaProfesor);
			
			if(!empty($numProfesorActivo))
			{
				list($idProfesorActivo,$primerNombreProfesor,$segundoNombreProfesor,$primerApellidoProfesor,$segundoApellidoProfesor)=mysql_fetch_array($consultaProfesor);
				$cadenaProfesor=$primerNombreProfesor." ".$segundoNombreProfesor." ".$primerApellidoProfesor." ".$segundoApellidoProfesor;
				$cadenaTablaProfesor="Modificar profesor";				
				$defectoSelected="";
			}
			else
			{
				$cadenaProfesor="No se ha agregado profesor a este grupo";
				$cadenaTablaProfesor="Agregar profesor";
				$defectoSelected="selected";
			}
			
			
			$sql="SELECT mei_grupo.idmateria,mei_grupo.nombre,mei_grupo.ubicacion,mei_grupo.horario,mei_materia.nombre, mei_materia.grupo
					FROM mei_grupo,mei_materia 
						WHERE mei_materia.idmateria=mei_grupo.idmateria AND mei_grupo.idgrupo=".$_GET['idGrupo'];
						
			//$sql="SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idMateria'];
			$consultaGrupo=$baseDatos->ConsultarBD($sql);
			list($idMateriaGrupo,$nombreGrupo,$ubicacionGrupo,$horarioGrupo,$nombreMateria, $gru)=mysql_fetch_array($consultaGrupo);
		?>
			<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
				<script language="javascript">
				
					function restablecerFormulario()
					{
						document.frm_modificarGrupo.btn_agregarProfesor.disabled=true;
						document.frm_modificarGrupo.btn_borrarHorario.disabled=false;
						document.frm_modificarGrupo.cbo_profesor.disabled=false;
						document.frm_modificarGrupo.btn_modificarGrupo.disabled=false;
						document.frm_modificarGrupo.reset();
					}
					
					function activarBotonProfesor()
					{
						document.frm_modificarGrupo.btn_agregarProfesor.disabled=false;
						document.frm_modificarGrupo.cbo_profesor.disabled=true;
						document.frm_modificarGrupo.btn_modificarGrupo.disabled=true;
						
						document.frm_modificarGrupo.cbo_profesor.value=0;
					}
					
					function desactivarBotonProfesor()
					{
						document.frm_modificarGrupo.btn_agregarProfesor.disabled=true;
						document.frm_modificarGrupo.cbo_profesor.disabled=false;
						document.frm_modificarGrupo.btn_modificarGrupo.disabled=false;
					}
					
					function enviarModificar()
					{
						if(comprobarDatos())
						{
							document.frm_modificarGrupo.action="actualizarGrupo.php";
							document.frm_modificarGrupo.submit();
						}
					}
					
					function enviarCancelar(gru)
					{
						if (gru==1) {
							location.replace("verGruposAdministrador.php");	
						}
						else
						{
							location.replace("verMateriasAdministrador.php");
						}
					}
					
					function agregarProfesor()
					{
						document.frm_modificarGrupo.action="actualizarGrupo.php?nuevoProfesor=1";
						document.frm_modificarGrupo.submit();
					}
				
					function comprobarDatos()
					{
						if(document.frm_modificarGrupo.txt_codigoGrupo.value == false)
						{
							alert("Es necesario que llene completamente la información solicitada");
						}
						else if(document.frm_modificarGrupo.txt_ubicacionGrupo.value == false)
						{
							alert("Es necesario que llene completamente la información solicitada");
						}						
						else
						{
							return true;
						}
												
					}
					
					function comprobarContenido()
					{
						if(document.frm_modificarGrupo.txt_horarioGrupo.value != "")
						{
							document.frm_modificarGrupo.btn_borrarHorario.disabled=false;
						}
						else
						{
							document.frm_modificarGrupo.btn_borrarHorario.disabled=true;
						}
					}
					
					function borrarContenido()
					{
						document.frm_modificarGrupo.txt_horarioGrupo.value="";
						document.frm_modificarGrupo.btn_borrarHorario.disabled=true;
					}
					
					function validarHorario()
					{
						if(parseInt(document.frm_modificarGrupo.cbo_diaSemana.value) == 0 || parseInt(document.frm_modificarGrupo.cbo_horaInicio.value) == 0 ||parseInt( document.frm_modificarGrupo.cbo_horaFin.value == 0))
						{
							document.frm_modificarGrupo.btn_agregarHorario.disabled=true;						
						}
						else
						{
							if(parseInt(document.frm_modificarGrupo.cbo_horaInicio.value) < parseInt(document.frm_modificarGrupo.cbo_horaFin.value))
							{
								document.frm_modificarGrupo.btn_agregarHorario.disabled=false;
							}
							else
							{
								document.frm_modificarGrupo.btn_agregarHorario.disabled=true;
							}
						}
					}
					
					function retornarHora(hora24)
					{
						if(parseInt(hora24) <= 12)
						{
							hora12=hora24+":00 am";
						}
						else
						{
							hora12=(parseInt(hora24)-12)+":00 pm";
						}
						return hora12;
					}
					
					function inicializarHorario()
					{
						document.frm_modificarGrupo.cbo_diaSemana.value=0;
						document.frm_modificarGrupo.cbo_horaInicio.value=0;
						document.frm_modificarGrupo.cbo_horaFin.value=0;
						document.frm_modificarGrupo.btn_agregarHorario.disabled=true;
					}					
										
					function agregarHorario()
					{
						var txtDia="";
						
						switch(parseInt(document.frm_modificarGrupo.cbo_diaSemana.value))
						{
							case 1:
							{
								txtDia="Lunes";
							}
							break;
							
							case 2:
							{
								txtDia="Martes";
							}
							break;
							
							case 3:
							{
								txtDia="Miercoles";
							}
							break;
							
							case 4:
							{
								txtDia="Jueves";
							}
							break;
							
							case 5:
							{
								txtDia="Viernes";
							}
							break;
							
							case 6:
							{
								txtDia="Sabado";
							}
							break;				
						}												
					
						if(document.frm_modificarGrupo.txt_horarioGrupo.value==false)
						{
							document.frm_modificarGrupo.txt_horarioGrupo.value=txtDia+" de "+retornarHora(document.frm_modificarGrupo.cbo_horaInicio.value)+" a "+retornarHora(document.frm_modificarGrupo.cbo_horaFin.value);
						}
						else
						{
							document.frm_modificarGrupo.txt_horarioGrupo.value+=",   "+txtDia+" de "+retornarHora(document.frm_modificarGrupo.cbo_horaInicio.value)+" a "+retornarHora(document.frm_modificarGrupo.cbo_horaFin.value);
						}
						
						comprobarContenido();
						inicializarHorario();											
					}

				</script>
				
				<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
                </style>
				</head>
				<body>
				
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
							<form  method="post" name="frm_modificarGrupo">
					<table class="tablaGeneral" >
								<tr class="trTitulo">
									<td colspan="3" class="trTitulo"><img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar Grupo de <?= $nombreMateria?> </td>
								</tr>
				<?
					if($_GET['error'] == '0x001')
					{
				?>
								<tr class="trAviso">
									<td width="3%"><img src="imagenes/error.gif" width="17" height="18"></td>
									<td width="97%" colspan="2"><span class="Estilo1"><span class="Estilo1">Error: Ya existe el Grupo 
								    <?=$_GET['grupoError']?> 
								    en	esta Materia
							      </span></tr>
				<?
					}
				?>
					<tr class="trInformacion">
						<td width="25%" colspan="2"><b>C&oacute;digo del Grupo :</b></td>
						<td width="75%"><input name="txt_codigoGrupo" type="text" id="txt_codigoGrupo" value="<?= $nombreGrupo?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Ubicaci&oacute;n:</b></td>
						<td><input name="txt_ubicacionGrupo" type="text" id="txt_ubicacionGrupo" value="<?= $ubicacionGrupo?>" size="50"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Horario:</b></td>
						<td><input name="txt_horarioGrupo" type="text" value="<?= $horarioGrupo?>" size="100" readonly>
						  <table width="100%" class="tsblaPrincipal">
                            <tr>
                              <td><select name="cbo_diaSemana" onChange="javascript:validarHorario()">
                              <option value="0">Dia</option>
                              <option value="1">Lunes</option>
                              <option value="2">Martes</option>
                              <option value="3">Miercoles</option>
                              <option value="4">Jueves</option>
                              <option value="5">Viernes</option>
                              <option value="6">Sabado</option>
                                </select></td>
                              <td width="12%"> <select name="cbo_horaInicio" onChange="javascript:validarHorario()">
                                <option value="0">hora inicio</option>
                                <option value="6">6:00 am</option>
                                <option value="7">7:00 am</option>
                                <option value="8">8:00 am</option>
                                <option value="9">9:00 am</option>
                                <option value="10">10:00 am</option>
                                <option value="11">11:00 am</option>
                                <option value="12">12:00 pm</option>
                                <option value="13">1:00 pm</option>
                                <option value="14">2:00 pm</option>
                                <option value="15">3:00 pm</option>
                                <option value="16">4:00 pm</option>
                                <option value="17">5:00 pm</option>
                                <option value="18">6:00 pm</option>
                                <option value="19">7:00 pm</option>
                                <option value="20">8:00 pm</option>
                                <option value="21">9:00 pm</option>
                                <option value="22">10:00 pm</option>
                              </select></td>
                              <td width="38%"><select name="cbo_horaFin" onChange="javascript:validarHorario()">
                                <option value="0" selected>hora finalizacion</option>
                                <option value="6">6:00 am</option>
                                <option value="7">7:00 am</option>
                                <option value="8">8:00 am</option>
                                <option value="9">9:00 am</option>
                                <option value="10">10:00 am</option>
                                <option value="11">11:00 am</option>
                                <option value="12">12:00 pm</option>
                                <option value="13">1:00 pm</option>
                                <option value="14">2:00 pm</option>
                                <option value="15">3:00 pm</option>
                                <option value="16">4:00 pm</option>
                                <option value="17">5:00 pm</option>
                                <option value="18">6:00 pm</option>
                                <option value="19">7:00 pm</option>
                                <option value="20">8:00 pm</option>
                                <option value="21">9:00 pm</option>
                                <option value="22">10:00 pm</option>
                              </select></td>
                              <td width="16%"><input name="btn_borrarHorario" type="button" value="Borrar horario" onClick="javascript:borrarContenido()"></td>
                              <td width="23%"><input name="btn_agregarHorario" type="button" value="Agregar horario"  onClick="javascript:agregarHorario()" disabled></td>
                            </tr>
                          </table></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Profesor:</b></td>
						<td><?= $cadenaProfesor?> </td>
					</tr>
					<tr class="trInformacion">
					<tr class="trInformacion">
                          <td colspan="3"><b><?= $cadenaTablaProfesor?></b></td>
                        </tr>
					
					  <td colspan="3">
					  <table width="100%" bordercolor="#DDDDDD" bgcolor="#DDDDDD" class="tablaPrincipal">
					<tr>
                          <td width="3%"><input name="rad_agregarProfesor" type="radio" value="0" onClick="javascript: desactivarBotonProfesor();" checked></td>
                          <td width="21%">Seleccionar de una Lista: </td>
                          <td width="36%"><select name="cbo_profesor" id="cbo_profesor">
                              <option value="0" <?= $defectoSelected?>>&nbsp;</option>
                              <?
					
					$sql="SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido, mei_usuario.segundoapellido 
							FROM mei_usuario 
								WHERE mei_usuario.idtipousuario=".$tipoProfesor;
					  
					$consultaProfesor=$baseDatos->ConsultarBD($sql);
							  
							while(list($idProfesor,$primerNombreProfesor,$segundoNombreProfesor,$primerApellidoProfesor,$segundoApellidoProfesor)=mysql_fetch_array($consultaProfesor))
							{
								if($idProfesorActivo == $idProfesor)
								{
									$idSelected="selected";
								}
								else
								{
									$idSelected="";
								}
								
							?>
                              <option value="<?= $idProfesor?>" <?= $idSelected?>>
                             	 <?= $idProfesor." - ".$primerNombreProfesor." ".$segundoNombreProfesor." ".$primerApellidoProfesor." ".$segundoApellidoProfesor.""?>
                              </option>
                              <?
							}
							?>
                          </select></td>
                          <td width="3%"><input name="rad_agregarProfesor" type="radio" value="1" onClick="javascript: activarBotonProfesor();"></td>
                          <td width="17%">Nuevo profesor </td>
                          <td width="20%"><input name="btn_agregarProfesor" type="button" value="Agregar profesor" onClick="javascript: agregarProfesor()" disabled>
                          </td>
                        </tr>
                      </table></td>
					  </tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center">	<input name="btn_reset" type="button" value="Restablecer" onClick="javascript: restablecerFormulario()">
						<input name="btn_modificarGrupo" type="button" value="Modificar Grupo" onClick="javascript: enviarModificar();">
						<input name="btn_finalizar" type="button" value="Cancelar" onClick="javascript:enviarCancelar('<?echo $gru;?>')"></div></td>
				  </tr>
				</table>
				<input name="hid_idGrupo" type="hidden" value="<?= $_GET['idGrupo']?>">
                <input name="hid_idProfesorAnterior" type="hidden" value="<?= $idProfesorActivo?>">
				<input name="hid_idMateria" type="hidden" value="<?= $idMateriaGrupo?>">
				<input name="hid_grupoAnterior" type="hidden" value="<?= $nombreGrupo?>">

                            </form>
				     </td>
                    <td class="tablaEspacio">&nbsp;</td>
                    <td class="tablaDerecho">&nbsp;</td>            
                    <td class="tablaEspacioDerecho">&nbsp;</td>                                   
					</tr>
				</table>
				</body>
			</html>
		<? 
		}
		else
		{
			redireccionar('../materias/verMateriasAdministrador.php');			
		}
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>
