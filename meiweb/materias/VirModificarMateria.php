<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
   	include_once('../calendario/FrmCalendario.class.php');

	$baseDatos=new BD();
	$editorRequisitos=new FCKeditor('edt_requisitos' , '100%' , '200' , 'barraEstandar' , '' ) ;	
	$editorIntroduccion=new FCKeditor('edt_introduccion' , '100%' , '200' , 'barraEstandar' , '' ) ;	 
	 
	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		if(!empty($_GET['idMateria']))
		{
			$sql="SELECT mei_virmateria.idvirmateria,mei_virmateria.nombre,mei_virmateria.nombrecorto,mei_virmateria.codigo,mei_virmateria.codigosec, mei_virmateria.formato, mei_virmateria.numeromod, mei_virmateria.fechainicio, mei_virmateria.fechafin, mei_virmateria.requisitos, mei_virmateria.introduccion, mei_virmateria.ofertar, mei_virmateria.precio 
				FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idMateria'];
			$consulta=$baseDatos->ConsultarBD($sql);
			
			list($idMateria,$nombreMateria,$nombreCorto,$codigoMateria,$codigoMateriasec,$formato,$modulos,$fecha_i,$fecha_f,$requisitos,$introduccion,$ofertar,$precio)=mysql_fetch_array($consulta);

			if ($formato=="Semanas")
				$activoSemanas="selected";
			else if ($formato=="Temas")
				$activoTemas="selected";
			
			if ($ofertar==1)
				$chkofertar="checked";
			else
				$chkofertar='';

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
	<script language="javascript">
		function enviarCrear()
		{
			if(comprobarDatos())
			{
				document.frm_modificarMateria.submit();
			}
		}
					
		function enviarCancelar()
		{
			location.replace("VirVerMateriasAdministrador.php");
		}
					
		function comprobarDatos()
		{
			if(document.frm_modificarMateria.txt_nombreMateria.value == false)
			{
				alert("Debe ingresar el nombre del curso");
			}
			else if(document.frm_modificarMateria.txt_nombreCorto.value == false)
			{
				alert("Debe ingresar el nombre corto del curso");
			}
			else if(document.frm_modificarMateria.txt_codigoMateria.value == false)
			{
				alert("Debe ingresar el código del curso");
			}
			else if(document.frm_modificarMateria.txt_codigoMateriasec.value == false)
			{
				alert("Debe ingresar el código secundario del curso");
			}
			else if(document.frm_modificarMateria.txt_modulos.value == false || isNaN(parseInt(document.frm_modificarMateria.txt_modulos.value)))
			{
				alert("Debe ingresar el número de modulos");
			}
			else if(document.frm_modificarMateria.txt_fecha_ini.value == false)
			{
				alert("Debe llenar completamente la información solicitada");
			}
			else if(document.frm_modificarMateria.txt_fecha_fin.value == false)
			{
				alert("Debe llenar completamente la información solicitada");
			}
			else if(document.frm_modificarMateria.edt_requisitos.value == false)
			{
				alert("Debe ingresar los requisitos del curso");
			}			
			else if(document.frm_modificarMateria.edt_introduccion.value == false)
			{
				alert("Debe ingresar una introducción");
			}
			else if(document.frm_modificarMateria.txt_fecha_ini.value == document.frm_modificarMateria.txt_fecha_fin.value)
			{
				alert("La fecha de inicio no debe ser igual a la de finalización. Ingrese un intervalo correcto.");
			}
			else
			{
				return true;
			}
		}
	</script>
	<script language="JavaScript">
//Código para colocar 
//los indicadores de miles mientras se escribe
//script por tunait!
function puntitos(donde,caracter){
	
	pat = /[\*,\+,\(,\),\?,\,$,\[,\],\^]/;
	valor = donde.value;
	largo = valor.length;
	crtr = true;
	
	//alert(donde+' '+caracter+' '+valor+' '+largo);
	
	if(isNaN(caracter) || pat.test(caracter) == true){
		if (pat.test(caracter)==true){ 
			caracter = "" + caracter;
		}
		carcter = new RegExp(caracter,"g");
		valor = valor.replace(carcter,"");
		donde.value = valor;
		crtr = false;
	}
	else{
		var nums = new Array();
		cont = 0;
		for(m=0;m<largo;m++){
			if(valor.charAt(m) == "." || valor.charAt(m) == " ")
				{continue}
			else{
				nums[cont] = valor.charAt(m);
				cont++;
			}
		}
	
	var cad1="",cad2="",tres=0;
	if(largo > 3 && crtr == true){
		for (k=nums.length-1;k>=0;k--){
			cad1 = nums[k];
			cad2 = cad1 + cad2;
			tres++;
			if((tres%3) == 0){
				if(k!=0){
					cad2 = "." + cad2;
				}
			}
		}
		donde.value = cad2;
	}
}
}
</script>
<?
			$fecha_ini=new FrmCalendario('frm_modificarMateria','txt_fecha_ini','formulario','false','');	
			$fecha_fin=new FrmCalendario('frm_modificarMateria','txt_fecha_fin','formulario','false','');		
?>	
	<style type="text/css">
		.Estilo1 {color: #FF0000}
	</style>
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><?=menu(1);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td><form action="VirActualizarMateria.php" method="post" name="frm_modificarMateria">
				<table width="39%" class="tablaGeneral" >
					<tr class="trTitulo">
						<td colspan="3" class="trTitulo">
                           	<img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar Curso <?= $nombreMateria?></td>
					</tr>
<?
			if($_GET['error'] == '0x001')
			{
?>
						<tr class="trAviso">
							<td width="3%"><img src="imagenes/error.gif" width="17" height="18"></td>
							<td width="97%" colspan="2"><span class="Estilo1">Error: Ya existe un curso registrado con ese c&oacute;digo.</span></td>
						</tr>
<?
			}
			if($_GET['error'] == '0x002')
			{
?>
						<tr class="trAviso">
							<td width="3%"><img src="imagenes/error.gif" width="17" height="18"></td>
							<td width="97%" colspan="2"><span class="Estilo1">Error: Debe ingresar un intervalo correcto de fechas.</span></td>
						</tr>
<?
			}
?>
					<tr class="trInformacion">
						<td colspan="2"><strong>Nombre del Curso:</strong></td>
						<td width="74%"><input name="txt_nombreMateria" type="text" id="txt_nombreMateria" value="<?= $nombreMateria?>" size="50"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Nombre Corto: </strong></td>
						<td><input name="txt_nombreCorto" type="text" id="txt_nombreCorto" value="<?= $nombreCorto?>" size="50"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>C&oacute;digo:</strong></td>
						<td><input name="txt_codigoMateria" type="text" id="txt_codigoMateria" value="<?= $codigoMateria ?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>C&oacute;digo Secundario:</strong></td>
						<td><input name="txt_codigoMateriasec" type="text" id="txt_codigoMateriasec" value="<?= $codigoMateriasec ?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Formato:</strong></td>
						<td><select name="cbo_formato">
								<option value="Semanas" <?=$activoSemanas?>>Semanas</option>
								<option value="Temas" <?=$activoTemas?>>Temas</option>
							</select>
						</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Numero de Modulos:</strong></td>
						<td><strong>
                        		<input name="txt_modulos" type="text" id="txt_modulos" value="<?= $modulos?>" size="5" maxlength="5">
							</strong></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Fecha de inicio:</strong></td>
						<td><? 
							$fecha_ini->m_valor=$fecha_i;
							$fecha_ini->CrearFrmCalendario(); ?></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Fecha de finalizacion:</strong></td>
						<td><? 
							$fecha_fin->m_valor=$fecha_f;
							$fecha_fin->CrearFrmCalendario(); ?></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Precio:</strong></td>
					<td>
						<input name="txt_precio" type="text" id="txt_precio" value="<?=$precio?>" onchange = "puntitos(this,this.value.charAt(this.value.length-1))" size="25">
						<!--<input type="text" name="pepe" onkeyup = "puntitos(this,this.value.charAt(this.value.length-1))">-->
						</td>
					</tr>
                    <tr class="trInformacion">
                    	<td colspan="2"><strong>Requisitos:</strong></td>
						<td><? 
							$editorRequisitos->Value=$requisitos;
							$editorRequisitos->crearEditor(); ?>	</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Introducci&oacute;n:</strong></td>
						<td><? 
							$editorIntroduccion->Value=$introduccion;
							$editorIntroduccion->crearEditor(); ?></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">
							<input type="checkbox" name="chk_ofertar" id="chk_ofertar" value="1" <?= $chkofertar ?>>
								<strong>Ofertar </strong></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center"><input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">
							<input name="btn_crearMateria" type="button" id="btn_crearMateria" value="Modificar Curso" onClick="javascript: enviarCrear();">
							<input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar()"></div></td>
					</tr>
				</table>
				<input name="hid_idMateria" type="hidden" value="<?= $_GET['idMateria']?>">
				</form> 
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho">&nbsp;</td>            
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<? 
			}
			else
			{
				redireccionar('../materias/VirVerMateriasAdministrador.php');
			}
		}
		else
		{
			redireccionar('../login/');
		}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>