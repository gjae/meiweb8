	function moverItem()  
	{
		var longitudLista = document.frm_listaSubGrupo.cbo_listaSubgrupo.length;
		var itemSeleccionado = document.frm_listaSubGrupo.cbo_alumnos.selectedIndex;
		var textoSeleccionado = document.frm_listaSubGrupo.cbo_alumnos.options[itemSeleccionado].text;
		var valorSeleccionado = document.frm_listaSubGrupo.cbo_alumnos.options[itemSeleccionado].value;
		var i;
		var nuevoItem = true;
		
		if(longitudLista != 0)
		{
			for(i = 0; i < longitudLista; i++)
			{
				itemActual = document.frm_listaSubGrupo.cbo_listaSubgrupo.options[i].text;
				if(itemActual == textoSeleccionado)
				{
					nuevoItem = false;
					break;
				}
			}
		} 
		
		if(nuevoItem)
		{
			nuevaOpcion = new Option(textoSeleccionado, valorSeleccionado, false, false);
			document.frm_listaSubGrupo.cbo_listaSubgrupo.options[longitudLista] = nuevaOpcion;
		}
		document.frm_listaSubGrupo.cbo_alumnos.selectedIndex=-1;
		actualizarLista();
	}
	
	function borrarItem()
	{
		var longitudLista = document.frm_listaSubGrupo.cbo_listaSubgrupo.length;
		arraySeleccionados = new Array();
		var cont = 0;
		
		for(i = 0; i < longitudLista; i++)
		{
			if(document.frm_listaSubGrupo.cbo_listaSubgrupo.options[i].selected)
			{
				arraySeleccionados[cont] = document.frm_listaSubGrupo.cbo_listaSubgrupo.options[i].value;
			}
			cont++;
		}
		
		var k;
		
		for(i = 0; i < longitudLista; i++)
		{
			for(k = 0; k < arraySeleccionados.length; k++)
			{
				if(document.frm_listaSubGrupo.cbo_listaSubgrupo.options[i].value == arraySeleccionados[k])
				{
					document.frm_listaSubGrupo.cbo_listaSubgrupo.options[i] = null;
				}
			}
			longitudLista = document.frm_listaSubGrupo.cbo_listaSubgrupo.length;
		}
		
		actualizarLista();
	}
	
	
	function guardarLista()
	{
		if (validarDatos())
		{
			document.frm_listaSubGrupo.submit();
		}	
	}
	
	
	
	function validarDatos()
	{
		if(document.frm_listaSubGrupo.txt_nombreSubgrupo.value == false)
		{
			alert("Debe llenar la información solicitada.  No ha llenado el campo Nombre Subgrupo.");
		}		
		else if(document.frm_listaSubGrupo.hid_listaValores.value.length == 0)
		{
			alert("No ha agregado ningún estudiante al Subgrupo. Agregue estudiantes dando clic sobre la lista.");
			return false;
		}
		else
		{
			return true;
		}
		
		
	}	
	
	function actualizarLista() 
	{
		document.frm_listaSubGrupo.hid_listaValores.value="";
		var longitudLista = document.frm_listaSubGrupo.cbo_listaSubgrupo.length;
		var cont=0;
		
		if(longitudLista != 0)
		{
			for(i=0;i < longitudLista;i++) 
			{
				if(cont == 0)
				{
					document.frm_listaSubGrupo.hid_listaValores.value = document.frm_listaSubGrupo.cbo_listaSubgrupo.options[i].value;
				}
				else
				{
					document.frm_listaSubGrupo.hid_listaValores.value+=";"+document.frm_listaSubGrupo.cbo_listaSubgrupo.options[i].value;
				}
				cont++;
			}
		}		
	}
	
	
