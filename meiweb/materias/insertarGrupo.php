<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	
	$baseDatos=new BD();
	
	if(comprobarSession())
	{
		if(isset($_POST['txt_codigoGrupo']) && isset($_POST['txt_ubicacionGrupo']) && isset($_GET['idMateria']))
		{
						
			if(empty($_POST['txt_horarioGrupo']))
			{
				$horarioGrupo="Sin horario";
			}
			else
			{
				$horarioGrupo=$_POST['txt_horarioGrupo'];
			}
			
			$sql="SELECT mei_grupo.idgrupo FROM mei_grupo 
					WHERE mei_grupo.idmateria =".$_GET['idMateria']." 
						AND mei_grupo.nombre='".strtoupper($_POST['txt_codigoGrupo'])."'";
					
			$consultaGrupo=$baseDatos->ConsultarBD($sql);
						
			$numGrupos=mysql_num_rows($consultaGrupo);
			
			if(empty($numGrupos))
			{
			
				$sql="INSERT INTO mei_grupo ( idgrupo , idmateria , nombre , ubicacion , horario ) 
						VALUES ('', '".$_GET['idMateria']."', '".strtoupper($_POST['txt_codigoGrupo'])."', '".strtoupper($_POST['txt_ubicacionGrupo'])."', '".$horarioGrupo."')";
	
				$baseDatos->ConsultarBD($sql);
				
				$idGrupo=$baseDatos->InsertIdBD();
	
				redireccionar("../materias/modificarGrupo.php?idGrupo=".$idGrupo);
			}
			else
			{
				redireccionar("../materias/crearGrupo.php?idMateria=".$_GET['idMateria']."&error=0x001&codigoGrupo=".strtoupper($_POST['txt_codigoGrupo'])."&ubicacionGrupo=".strtoupper($_POST['txt_ubicacionGrupo'])."&horarioGrupo=".$horarioGrupo);
			}
		}
		redireccionar("../materias/verMateriasAdministrador.php");
	}
	else
	{
		redireccionar('../login/');
	}
?>
