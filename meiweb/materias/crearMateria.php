<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{

		?>
			<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
				<script language="javascript">
				
					function enviarCrear()
					{
						if(comprobarDatos())
						{
							document.frm_crearMateria.submit();
						}
					}
					
					function enviarCancelar()
					{
						location.replace("verMateriasAdministrador.php");
					}
				
					function comprobarDatos()
					{
						if(document.frm_crearMateria.txt_nombreMateria.value == false)
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(document.frm_crearMateria.txt_nombreCorto.value == false)
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(document.frm_crearMateria.txt_codigoMateria.value == false)
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(document.frm_crearMateria.txt_codigoMateriasec.value == false)
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(document.frm_crearMateria.txt_creditosMateria.value == false || isNaN(parseInt(document.frm_crearMateria.txt_creditosMateria.value)))
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else
						{
							return true;
						}
					}
				
				</script>
				
				<style type="text/css">
				<!--
				.Estilo1 {color: #FF0000}
				-->
				</style>
				
				</head>
				<body>
				
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral">
							<form action="insertarMateria.php" method="post" name="frm_crearMateria">
						<tr>
							<td width="4%" class="trTitulo"><img src="imagenes/materias.gif" width="17" height="18"></td>
							<td colspan="2" class="trTitulo">Crear Materia </td>
						</tr>
							
								<tr class="trSubTitulo">
									<td colspan="3" class="trSubTitulo">Ingrese la informaci&oacute;n de la nueva Materia </td>
								</tr>
				<?
					if($_GET['error'] == '0x001')
					{
				?>
								<tr class="trAviso">
									<td width="4%"><img src="imagenes/error.gif" width="17" height="18"></td>
									<td colspan="2"><span class="Estilo1">Error: Ya existe una materia registrada con ese c&oacute;digo</span></td>
								</tr>
				<?
					}
				?>
					<tr class="trInformacion">
					  <td colspan="2">&nbsp;</td>
					  <td>&nbsp;</td>
					  </tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Nombre de la Materia :</strong></td>
						<td width="76%"><input name="txt_nombreMateria" type="text" id="txt_nombreMateria" value="<?= $_GET['nombreMateria']?>" size="50"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Nombre Corto: </strong></td>
						<td><input name="txt_nombreCorto" type="text" id="txt_nombreCorto" value="<?= $_GET['nombreCorto']?>" size="50"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>C&oacute;digo:</strong></td>
						<td><input name="txt_codigoMateria" type="text" id="txt_codigoMateria" value="<?= $_GET['codigoMateria']?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>C&oacute;digo Secundario:</strong></td>
						<td><input name="txt_codigoMateriasec" type="text" id="txt_codigoMateriasec" value="<?= $_GET['codigoMateriasec']?>" size="25"></td>
					</tr>
					
					<tr class="trInformacion">
						<td colspan="2"><strong>Cr&eacute;ditos:</strong></td>
						<td><input name="txt_creditosMateria" type="text" id="txt_creditosMateria" value="<?= $_GET['creditosMateria']?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>Grupo:</strong></td>
						<td>
							<select id="txt_grupo" name="txt_grupo">
								<option value="0" selected>No</option>
								<option value="1">Si</option>
							</select>
						</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center">	<input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">
						<input name="btn_crearMateria" type="button" id="btn_crearMateria" value="Crear Materia" onClick="javascript: enviarCrear();">
						<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div></td>
					 </tr>
							</form>
					 
				</table>
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho">&nbsp;</td>            
                <td class="tablaEspacioDerecho">&nbsp;</td>              
					</tr>
				</table>
				</body>
			</html>
		<? 
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>
