<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	$tipoProfesor=5;
	if(comprobarSession())
	{
		if($_SESSION['banderaAdmnistrador']==1)
		{
			if(!empty($_GET['idGrupo']))
			{
				$sql="SELECT DISTINCT mei_usuario.idusuario,mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido, mei_usuario.segundoapellido FROM mei_usuario,mei_relusuvirgru WHERE mei_usuario.idusuario=mei_relusuvirgru.idusuario 
					AND mei_usuario.idtipousuario=".$tipoProfesor." AND mei_relusuvirgru.idvirgrupo =".$_GET['idGrupo'];
			
				$consultaProfesor=$baseDatos->ConsultarBD($sql);
				$numProfesorActivo=mysql_num_rows($consultaProfesor);
		
				if(!empty($numProfesorActivo))
				{
					list($idProfesorActivo,$primerNombreProfesor,$segundoNombreProfesor,$primerApellidoProfesor,$segundoApellidoProfesor)=mysql_fetch_array($consultaProfesor);
					$cadenaProfesor=$primerNombreProfesor." ".$segundoNombreProfesor." ".$primerApellidoProfesor." ".$segundoApellidoProfesor;
					$cadenaTablaProfesor="Cambiar Profesor";				
					$defectoSelected="";
				}
				else //no se va ver por que siempre hay un profesor asignado
				{
					$cadenaProfesor="No se ha asignado profesor a este grupo";
					$cadenaTablaProfesor="Agregar profesor";
					$defectoSelected="selected";
				}
			
				$sql="SELECT mei_virgrupo.idvirmateria,mei_virgrupo.nombre,mei_virmateria.nombre 
						FROM mei_virgrupo,mei_virmateria WHERE mei_virmateria.idvirmateria=mei_virgrupo.idvirmateria AND mei_virgrupo.idvirgrupo=".$_GET['idGrupo'];
						
				//$sql="SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idMateria'];
				$consultaGrupo=$baseDatos->ConsultarBD($sql);
				list($idMateriaGrupo,$nombreGrupo,$nombreMateria)=mysql_fetch_array($consultaGrupo);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
	<script language="javascript">
		function restablecerFormulario()
		{
			document.frm_modificarGrupo.cbo_profesor.disabled=false;
			document.frm_modificarGrupo.btn_modificarGrupo.disabled=false;
			document.frm_modificarGrupo.reset();
		}
								
		function enviarModificar()
		{
			if(comprobarDatos())
			{
				document.frm_modificarGrupo.action="VirActualizarGrupo.php";
				document.frm_modificarGrupo.submit();
			}
		}
					
		function enviarCancelar()
		{
			location.replace("VirVerMateriasAdministrador.php");
		}
								
		function comprobarDatos()
		{
			if(document.frm_modificarGrupo.txt_codigoGrupo.value == false)
			{
				alert("Debe llenar completamente la información solicitada");
				document.frm_modificarGrupo.txt_codigoGrupo.focus();
			}
			else if (document.frm_modificarGrupo.cbo_profesor.value==0)
			{
				alert("Debe seleccionar un profesor");
				document.frm_modificarGrupo.cbo_profesor.focus();
			}
			else
			{
				return true;
			}
		}
	</script>
	<style type="text/css">
		<!--
			.Estilo1 {color: #FF0000}
		-->
	</style>
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><?=menu(1);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td><form  method="post" name="frm_modificarGrupo">
				<table class="tablaGeneral" >
					<tr class="trTitulo">
						<td colspan="3" class="trTitulo">
							<img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar Grupo de <?= $nombreMateria?> </td>
					</tr>
<?
				if($_GET['error'] == '0x001')
				{
?>
					<tr class="trAviso">
						<td width="6%"><img src="imagenes/error.gif" width="17" height="18"></td>
						<td colspan="2"><span class="Estilo1">Error: Ya existe el Grupo <?=$_GET['grupoError']?> en	este Curso</span></td>
					</tr>
<?
				}
?>
					<tr class="trInformacion">
						<td colspan="2"><b>C&oacute;digo del Grupo :</b></td>
						<td width="69%"><input name="txt_codigoGrupo" type="text" id="txt_codigoGrupo" value="<?= $nombreGrupo?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Profesor:</b></td>
						<td><?= $cadenaProfesor?> </td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><b><?= $cadenaTablaProfesor?></b></td>
					</tr>
					<tr>
						<td colspan="3">
							<table width="100%" bordercolor="#DDDDDD" bgcolor="#DDDDDD" class="tablaPrincipal">
								<tr>
									<td width="38%"align="right">Profesores Educaci&oacute;n Virual </td>
									<td width="62%"><select name="cbo_profesor" id="cbo_profesor">
										<option value="0" <?= $defectoSelected?>>-------Seleccionar-------</option>
<?
				$sql="SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido, mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idtipousuario=".$tipoProfesor;
				$consultaProfesor=$baseDatos->ConsultarBD($sql);
				while(list($idProfesor,$primerNombreProfesor,$segundoNombreProfesor,$primerApellidoProfesor,$segundoApellidoProfesor)=mysql_fetch_array($consultaProfesor))
				{
					if($idProfesorActivo == $idProfesor)
					{
						$idSelected="selected";
					}
					else
					{
						$idSelected="";
					}
?>
										<option value="<?= $idProfesor?>" <?= $idSelected?>><?= $primerNombreProfesor." ".$segundoNombreProfesor." ".$primerApellidoProfesor." ".$segundoApellidoProfesor.""?></option>
<?
				}
?>
									</select></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center"><input name="btn_reset" type="button" value="Restablecer" onClick="javascript: restablecerFormulario()">
							<input name="btn_modificarGrupo" type="button" value="Modificar Grupo" onClick="javascript: enviarModificar();">
							<input name="btn_finalizar" type="button" value="Cancelar" onClick="javascript:enviarCancelar()"></div></td>
					</tr>
				</table>
					<input name="hid_idGrupo" type="hidden" value="<?= $_GET['idGrupo']?>">
					<input name="hid_idProfesorAnterior" type="hidden" value="<?= $idProfesorActivo?>">
					<input name="hid_idMateria" type="hidden" value="<?= $idMateriaGrupo?>">
					<input name="hid_grupoAnterior" type="hidden" value="<?= $nombreGrupo?>">
				</form>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho">&nbsp;</td>
            <td class="tablaEspacioDerecho">&nbsp;</td>            
		</tr>
	</table>
</body>
</html>
<? 
			}
			else
			{
				redireccionar('../materias/VirVerMateriasAdministrador.php');			
			}
		}
		else
		{
			redireccionar('../login/');
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>
