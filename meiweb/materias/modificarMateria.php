<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		if(!empty($_GET['idMateria']))
		{
			$sql="SELECT mei_materia.idmateria,mei_materia.nombre,mei_materia.nombrecorto,mei_materia.codigo,mei_materia.codigosec,mei_materia.creditos, mei_materia.grupo FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idMateria'];
			$consulta=$baseDatos->ConsultarBD($sql);
			
			list($idMateria,$nombreMateria,$nombreCorto,$codigoMateria,$codigoMateriasec,$creditosMateria, $gru)=mysql_fetch_array($consulta);
		?>
			<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
				<script language="javascript">
				
					function enviarCrear()
					{
						if(comprobarDatos())
						{
							document.frm_modificarMateria.submit();
						}
					}
					
					function enviarCancelar(gru)
					{
						if (gru==1) {
							location.replace("verGruposAdministrador.php");
						}
						else
						{
							location.replace("verMateriasAdministrador.php");
						}
					}
					function comprobarDatos()
					{
						if(document.frm_modificarMateria.txt_nombreMateria.value == false)
						{
							alert("Es necesario que llene completamente la información solicitada");
						}
						else if(document.frm_modificarMateria.txt_nombreCorto.value == false)
						{
							alert("Es necesario que llene completamente la información solicitada");
						}
						else if(document.frm_modificarMateria.txt_codigoMateria.value == false)
						{
							alert("Es necesario que llene completamente la información solicitada");
						}
						else if(document.frm_modificarMateria.txt_codigoMateriasec.value == false)
						{
							alert("Es necesario que llene completamente la información solicitada");
						}
						else if(document.frm_modificarMateria.txt_creditosMateria.value == false || isNaN(parseInt(document.frm_modificarMateria.txt_creditosMateria.value)))
						{
							alert("Es necesario que llene completamente la información solicitada");
						}
						else
						{
							return true;
						}
					}
				
				</script>
			
				<style type="text/css">
				.Estilo1 {color: #FF0000}
				</style>
			</head>
			<body>
			
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
							<form action="actualizarMateria.php" method="post" name="frm_modificarMateria">
					<table width="39%" class="tablaGeneral" >
						<tr class="trTitulo">
									<td colspan="3" class="trTitulo"><img src="imagenes/modificar.gif" width="16" height="16" align="texttop"> Modificar la  Materia <?= $nombreMateria?></td>
					  </tr>
					<?
					if($_GET['error'] == '0x001')
					{
					?>
								<tr class="trAviso">
									<td width="3%"><img src="imagenes/error.gif" width="17" height="18"></td>
									<td width="97%" colspan="2"><span class="Estilo1">Error: Ya existe una materia registrada con ese c&oacute;digo</span></td>
								</tr>
					<?
					}
					?>
					<tr class="trInformacion">
						<td width="25%" colspan="2"><b>Nombre de la materia : </b></td>
						<td width="75%"><input name="txt_nombreMateria" type="text" id="txt_nombreMateria" value="<?= $nombreMateria?>" size="50"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Nombre Corto: </b></td>
						<td><input name="txt_nombreCorto" type="text" id="txt_nombreCorto" value="<?= $nombreCorto?>"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>C&oacute;digo:</b></td>
						<td><input name="txt_codigoMateria" type="text" id="txt_codigoMateria" value="<?= $codigoMateria?>"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>C&oacute;digo Secundario:</b></td>
						<td><input name="txt_codigoMateriasec" type="text" id="txt_codigoMateriasec" value="<?= $codigoMateriasec?>"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Cr&eacute;ditos:</b></td>
						<td><input name="txt_creditosMateria" type="text" id="txt_creditosMateria" value="<?= $creditosMateria?>"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center"><input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">
						<input name="btn_crearMateria" type="button" id="btn_crearMateria" value="Modificar Materia" onClick="javascript: enviarCrear();">
						<input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar('<?echo $gru;?>')"></div></td>
					</tr>
				</table>
					<input name="hid_idMateria" type="hidden" value="<?= $_GET['idMateria']?>">
				</form> 
					</td>
                    <td class="tablaEspacio">&nbsp;</td>
                    <td class="tablaDerecho">&nbsp;</td>            
                    <td class="tablaEspacioDerecho">&nbsp;</td>                                  
					</tr>
				</table>
			
			</body>
			</html>
		<? 
		}
		else
		{
			redireccionar('../materias/verMateriasAdministrador.php');
		}
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>
