<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	
	$baseDatos=new BD();
	
	if(comprobarSession())
	{
		if(isset($_POST['txt_nombreMateria']) && isset($_POST['txt_nombreCorto']) && isset($_POST['txt_codigoMateria']))
		{
			$sql="SELECT count(mei_virmateria.idvirmateria) FROM mei_virmateria WHERE mei_virmateria.codigosec='".$_POST['txt_codigoMateriasec']."'";
			$consulta=$baseDatos->ConsultarBD($sql);
			list($numeroMaterias)=mysql_fetch_array($consulta);
			if(empty($numeroMaterias))
			{
				if(!empty($_POST['chk_ofertar']))
				{
					$estado=1;
				}
				else
				{
					$estado=0;
				}	
				
				list($ano,$mes,$dia)=explode('-',$_POST['txt_fecha_ini']);
				if (($dia<10) and ($mes<10))
					$fechaInicio=$ano.'0'.$mes.'0'.$dia;
				else if (($dia<10) and ($mes>=10))
					$fechaInicio=$ano.$mes.'0'.$dia;
				else if (($dia>=10) and ($mes<10))
					$fechaInicio=$ano.'0'.$mes.$dia;
				else if (($dia>=10) and ($mes>=10))
					$fechaInicio=$ano.$mes.$dia;
					
				list($a,$m,$d)=explode('-',$_POST['txt_fecha_fin']);
				if ($d<10 and $m<10)
					$fechaFin=$a.'0'.$m.'0'.$d;
				else if ($d<10 and $m>=10)
					$fechaFin=$a.$m.'0'.$d;
				else if ($d>=10 and $m<10)
					$fechaFin=$a.'0'.$m.$d;
				else if ($d>=10 and $m>=10)
					$fechaFin=$a.$m.$d;

				if ($fechaInicio < $fechaFin)
				{
					$sql="SELECT MAX( mei_virmateria.idvirmateria) FROM mei_virmateria";
					$resultado=$baseDatos->ConsultarBD($sql);
					list($maxId)=mysql_fetch_array($resultado);
					if ($maxId<1000)
						$codigoMateria=1000;
					else
						$codigoMateria=$maxId+1;		
						
					$sql="INSERT INTO mei_virmateria (idvirmateria, nombre, nombrecorto, codigo, codigosec, requisitos, formato, numeromod, fechainicio, fechafin, introduccion, ofertar, precio)
	VALUES ('".$codigoMateria."','".strtoupper($_POST['txt_nombreMateria'])."','".strtoupper($_POST['txt_nombreCorto'])."','".$_POST['txt_codigoMateria']."','".$_POST['txt_codigoMateriasec']."','".$_POST['edt_requisitos']."','".$_POST['cbo_formato']."','".(int)$_POST['txt_modulos']."','".$_POST['txt_fecha_ini']."','".$_POST['txt_fecha_fin']."','".$_POST['edt_introduccion']."','".$estado."', '".$_POST['txt_precio']."')";
	
					$baseDatos->ConsultarBD($sql);

					redireccionar("../materias/VirVerMateriasAdministrador.php?idMateria=".$codigoMateria);
				}
				else
				{
					redireccionar("../materias/VirCrearMateria.php?error=0x002&nombreMateria=".$_POST['txt_nombreMateria']."&nombreCorto=".$_POST['txt_nombreCorto']."&codigoMateria=".$_POST['txt_codigoMateria']."&codigoMateriasec=".$_POST['txt_codigoMateriasec']."&modulos=".$_POST['txt_modulos']."&fecha_ini=".$_POST['txt_fecha_ini']."&fecha_fin=".$_POST['txt_fecha_fin']."&requisitos=".$_POST['edt_requisitos']."&introduccion=".$_POST['edt_introduccion']."&ofertar=".$estado);
				}
			}
			else
			{
				redireccionar("../materias/VirCrearMateria.php?error=0x001&nombreMateria=".$_POST['txt_nombreMateria']."&nombreCorto=".$_POST['txt_nombreCorto']."&codigoMateria=".$_POST['txt_codigoMateria']."&codigoMateriasec=".$_POST['txt_codigoMateriasec']."&modulos=".$_POST['txt_modulos']."&fecha_ini=".$_POST['hid_fecha_ini']."&fecha_fin=".$_POST['hid_fecha_fin']."&requisitos=".$_POST['edt_requisitos']."&introduccion=".$_POST['edt_introduccion']."&ofertar=".$estado);
			}
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>