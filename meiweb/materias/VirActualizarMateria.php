<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	
	$baseDatos=new BD();
	
	if(comprobarSession())
	{
		if(isset($_POST['txt_nombreMateria']) && isset($_POST['txt_nombreCorto']) && isset($_POST['txt_codigoMateria']) && isset($_POST['hid_idMateria']))
		{
			$sql="SELECT count(mei_virmateria.idvirmateria) FROM mei_virmateria WHERE mei_virmateria.idvirmateria <>".$_POST['hid_idMateria']." AND mei_virmateria.codigo='".$_POST['txt_codigoMateria']."'";
		
			$consulta=$baseDatos->ConsultarBD($sql);
			list($numeroMaterias)=mysql_fetch_array($consulta);
			if(empty($numeroMaterias))
			{
				if(!empty($_POST['chk_ofertar']))
					$estado=1;
				else
					$estado=0;
					
				list($ano,$mes,$dia)=explode('-',$_POST['txt_fecha_ini']);
				
			    if($dia<10 && strlen($dia)==2){
			    	$dia=substr($dia, 1 , -1);
			    }
			    if ($mes<10 && strlen($mes)==2) {
			    	$mes=substr($mes, 1 , -1);
			    }
				if (($dia<10) and ($mes<10)){
					$fechaInicio=$ano.'0'.$mes.'0'.$dia;
				}
				else if (($dia<10) and ($mes>=10))
					$fechaInicio=$ano.$mes.'0'.$dia;
				else if (($dia>=10) and ($mes<10))
					$fechaInicio=$ano.'0'.$mes.$dia;
				else if (($dia>=10) and ($mes>=10))
					$fechaInicio=$ano.$mes.$dia;
				//echo $fechaInicio."-";			
				list($a,$m,$d)=explode('-',$_POST['txt_fecha_fin']);
				if($m<10 && strlen($m)==2){
			    	$m=substr($m, 1 , -1);
			    }
			    if ($d<10 && strlen($d)==2) {
			    	$d=substr($d, 1 , -1);
			    }

				if ($d<10 and $m<10)
					$fechaFin=$a.'0'.$m.'0'.$d;
				else if ($d<10 and $m>=10)
					$fechaFin=$a.$m.'0'.$d;
				else if ($d>=10 and $m<10)
					$fechaFin=$a.'0'.$m.$d;
				else if ($d>=10 and $m>=10)
					$fechaFin=$a.$m.$d;
				//	echo $fechaFin;
				if ($fechaInicio < $fechaFin)
				{
					$sql="UPDATE mei_virmateria SET 
							nombre = '".strtoupper($_POST['txt_nombreMateria'])."',
							nombrecorto = '".strtoupper($_POST['txt_nombreCorto'])."',
							codigo = '".$_POST['txt_codigoMateria']."',
							codigosec = '".$_POST['txt_codigoMateriasec']."',
							requisitos = '".$_POST['edt_requisitos']."',
							formato = '".$_POST['cbo_formato']."',
							numeromod = '".(int)$_POST['txt_modulos']."',
							fechainicio = '".$_POST['txt_fecha_ini']."',
							fechafin = '".$_POST['txt_fecha_fin']."',
							introduccion = '".$_POST['edt_introduccion']."', 
							ofertar = '".$estado."',
							precio = '".$_POST['txt_precio']."'
								WHERE mei_virmateria.idvirmateria ='".$_POST['hid_idMateria']."'";
				
						$baseDatos->ConsultarBD($sql);
						redireccionar("../materias/VirVerMateriasAdministrador.php?idMateria=".$_POST['hid_idMateria']);
				}
				else
				{
					redireccionar("../materias/VirModificarMateria.php?error=0x002&idMateria=".$_POST['hid_idMateria']);
				}
			}
			else
			{
				redireccionar("../materias/VirModificarMateria.php?error=0x001&idMateria=".$_POST['hid_idMateria']);
			}
		}
	}
	else
	{
	redireccionar('../login/');
	}
?>
