<?
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ("../editor/fckeditor.php") ;
   	include_once ('../calendario/FrmCalendario.class.php');


	$baseDatos=new BD();
	$editorRequisitos=new FCKeditor('edt_requisitos' , '100%' , '200' , 'barraEstandar' , '' ) ;	
	$editorIntroduccion=new FCKeditor('edt_introduccion' , '100%' , '200' , 'barraEstandar' , '' ) ;	 

	if(comprobarSession())
	{
		if($_SESSION['banderaAdmnistrador']==1)
		{
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
	<script language="javascript">
		function enviarCrear()
		{
			if(comprobarDatos())
			{
				document.frm_crearMateria.submit();
			}
		}
					
		function enviarCancelar()
		{
			location.replace("../scripts/indexAdministrador.php");
		}
				
		function comprobarDatos()
		{
			if(document.frm_crearMateria.txt_nombreMateria.value == false)
			{
				alert("Debe ingresar el nombre del curso");
			}
			else if(document.frm_crearMateria.txt_nombreCorto.value == false)
			{
				alert("Debe ingresar el nombre corto del curso");
			}
			else if(document.frm_crearMateria.txt_codigoMateria.value == false)
			{
				alert("Debe ingresar el código del curso");
			}
			else if(document.frm_crearMateria.txt_codigoMateriasec.value == false)
			{
				alert("Debe ingresar el código secundario del curso");
			}
			else if(document.frm_crearMateria.txt_modulos.value == false || isNaN(parseInt(document.frm_crearMateria.txt_modulos.value)))
			{
				alert("Debe ingresar el número de modulos");
			}
			else if(document.frm_crearMateria.txt_fecha_ini.value == false)
			{
				alert("Debe llenar completamente la información solicitada");
			}
			else if(document.frm_crearMateria.txt_fecha_fin.value == false)
			{
				alert("Debe llenar completamente la información solicitada");
			}
			else if(document.frm_crearMateria.edt_requisitos.value == false)
			{
				alert("Debe ingresar los requisitos del curso");
			}			
			else if(document.frm_crearMateria.edt_introduccion.value == false)
			{
				alert("Debe ingresar una introducción");
			}
			else if(document.frm_crearMateria.txt_fecha_ini.value == document.frm_crearMateria.txt_fecha_fin.value)
			{
				alert("La fecha de inicio no debe ser igual a la de finalización. Ingrese un intervalo correcto.");
			}
			else
			{
				return true;
			}
		}
</script>
<script type="text/javascript" src="jquery.js" ></script>		
<script type="text/javascript">		

		
    </script>
    
    <script language="JavaScript">
//Código para colocar 
//los indicadores de miles mientras se escribe
//script por tunait!
function puntitos(donde,caracter){
	
	pat = /[\*,\+,\(,\),\?,\,$,\[,\],\^]/;
	valor = donde.value;
	largo = valor.length;
	crtr = true;
	
	//alert(donde+' '+caracter+' '+valor+' '+largo);
	
	if(isNaN(caracter) || pat.test(caracter) == true){
		if (pat.test(caracter)==true){ 
			caracter = "" + caracter;
		}
		carcter = new RegExp(caracter,"g");
		valor = valor.replace(carcter,"");
		donde.value = valor;
		crtr = false;
	}
	else{
		var nums = new Array();
		cont = 0;
		for(m=0;m<largo;m++){
			if(valor.charAt(m) == "." || valor.charAt(m) == " ")
				{continue}
			else{
				nums[cont] = valor.charAt(m);
				cont++;
			}
		}
	
	var cad1="",cad2="",tres=0;
	if(largo > 3 && crtr == true){
		for (k=nums.length-1;k>=0;k--){
			cad1 = nums[k];
			cad2 = cad1 + cad2;
			tres++;
			if((tres%3) == 0){
				if(k!=0){
					cad2 = "." + cad2;
				}
			}
		}
		donde.value = cad2;
	}
}
}
</script>
<?
			$fecha_ini=new FrmCalendario('frm_crearMateria','txt_fecha_ini','formulario','false','');	
			$fecha_fin=new FrmCalendario('frm_crearMateria','txt_fecha_fin','formulario','false','');		
?>
	<style type="text/css">
	<!--
		.Estilo1 {color: #FF0000}
	-->
	</style>
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
	        <td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><? menu(1);?></td>
            <td class="tablaEspacio">&nbsp;</td>
			<td>
				<table class="tablaGeneral">
					<form action="VirInsertarMateria.php" method="post" name="frm_crearMateria">
					<tr>
						<td colspan="3" class="trTitulo"><img src="imagenes/materias.gif" width="17" height="17">&nbsp;Crear Curso</td>
					</tr>
					<tr class="trSubTitulo">
						<td colspan="3" class="trSubTitulo">&nbsp;Ingrese la informaci&oacute;n del nuevo Curso </td>
					</tr>
<?
			if($_GET['error'] == '0x001')
			{
?>
					<tr class="trAviso">
						<td width="4%"><img src="imagenes/error.gif" width="17" height="18"></td>
						<td colspan="2"><span class="Estilo1">Error: Ya existe un curso registrado con ese c&oacute;digo</span></td>
					</tr>
<?
			}
			if($_GET['error'] == '0x002')
			{
?>
					<tr class="trAviso">
						<td width="4%"><img src="imagenes/error.gif" width="17" height="18"></td>
						<td colspan="2"><span class="Estilo1">Error: Debe ingresar un intervalo correcto de fechas</span></td>
					</tr>
<?
			}
?>
					<tr class="trInformacion">
						<td colspan="2">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Nombre del Curso:</strong></td>
						<td width="74%"><input name="txt_nombreMateria" type="text" id="txt_nombreMateria" value="<?= $_GET['nombreMateria']?>" size="50"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Nombre Corto: </strong></td>
						<td><input name="txt_nombreCorto" type="text" id="txt_nombreCorto" value="<?= $_GET['nombreCorto']?>" size="50"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;C&oacute;digo:</strong></td>
						<td><input name="txt_codigoMateria" type="text" id="txt_codigoMateria" value="<?= $_GET['codigoMateria']?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;C&oacute;digo Secundario:</strong></td>
						<td><input name="txt_codigoMateriasec" type="text" id="txt_codigoMateriasec" value="<?= $_GET['codigoMateriasec']?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Formato:</strong></td>
						<td><select name="cbo_formato">
								<option value="Semanas">Semanas</option>
								<option value="Temas">Temas</option>
							</select>
						</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;N&uacute;mero de M&oacute;dulos:</strong></td>
						<td><strong>
                        		<input name="txt_modulos" type="text" id="txt_modulos" value="<?= $_GET['modulos']?>" size="5" maxlength="5">
							</strong></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Fecha de inicio:</strong></td>
						<td><? $fecha_ini->CrearFrmCalendario(); ?></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Fecha de finalizaci&oacute;n:</strong></td>
						<td><? $fecha_fin->CrearFrmCalendario(); ?></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong>&nbsp;Precio:</strong></td>
					<td>
						<input name="txt_precio" type="text" id="txt_precio" onchange = "puntitos(this,this.value.charAt(this.value.length-1))" size="25">
						</td>
					</tr>
                    <tr class="trInformacion">
                    	<td colspan="2"><strong><center>Requisitos: </center></strong></td>
						<td>
<?
						$editorRequisitos->Value ='Aqui debe especificar los requisitos que deben cumplir los aspirantes a este curso ';
					$editorRequisitos->crearEditor();

?>
                        </td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><strong><center>Introducci&oacute;n:</center></strong></td>
						<td>
<? 
		$editorIntroduccion ->Value="Debe dar una breve introducción la cual sera presentada a todos los aspirantes al curso. ";
		$editorIntroduccion->crearEditor(); 
?>
							</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;
							<input type="checkbox" name="chk_ofertar" id="chk_ofertar" value="1" <?= $chk_ofertar?> checked>
								<strong>Ofertar </strong></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center"><input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">
							<input name="btn_crearMateria" type="button" id="btn_crearMateria" value="Crear Curso" onClick="javascript: enviarCrear();">
							<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div></td>
					</tr>
					</form>
				</table>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho">&nbsp;</td>            
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<? 
		}
		else
		{
			redireccionar('../login/');
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>