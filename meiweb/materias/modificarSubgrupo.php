<?
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
	
	$baseDatos=new BD();
	
	if(comprobarSession())
	{
		mysql_query("SET NAMES 'utf8'");
		if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{
			if(!empty($_GET['idGrupo']) && !empty($_GET['idSubGrupo']))
			{
				if(isset($_GET['error']))
				{
					$listaCodigosSeleccionados=$_GET['listaValores'];
					$listaErrores=$_GET['listaErrores'];
					$nombreSubgrupo=$_GET['nombreSubgrupo'];
					$descripcionSubGrupo=$_GET['descripcionSubGrupo'];
					$idTipoSubgrupo=$_GET['idTipoSubgrupo'];
				}
				else if(isset($_POST['hid_listaValores']))
				{
					$listaCodigosSeleccionados=$_POST['hid_listaValores'];
					$listaErrores=$_POST['hid_listaErrores'];
					$nombreSubgrupo=$_POST['txt_nombreSubgrupo'];
					$descripcionSubGrupo=$_POST['txt_descripcionSubGrupo'];
				}
				else
				{
					
					
					$sql="SELECT mei_subgrupo.nombre,mei_subgrupo.descripcion,mei_subgrupo.idtiposubgrupo 
							FROM mei_subgrupo WHERE mei_subgrupo.idsubgrupo='".$_GET['idSubGrupo']."'";
					
					$consultaSubgrupo=$baseDatos->ConsultarBD($sql);
					
					list($nombreSubgrupo,$descripcionSubGrupo,$idTipoSubgrupo)=mysql_fetch_array($consultaSubgrupo);
										
					$sql="SELECT mei_relususub.idusuario FROM mei_relususub 
							WHERE mei_relususub.idsubgrupo='".$_GET['idSubGrupo']."'";
							
					$consultaUsuarios=$baseDatos->ConsultarBD($sql);
					
					while(list($idUsuarioGrupo)=mysql_fetch_array($consultaUsuarios))
					{
						if(empty($listaCodigosSeleccionados))
						{
							$listaCodigosSeleccionados=$idUsuarioGrupo;
						}
						else
						{
							$listaCodigosSeleccionados.=";".$idUsuarioGrupo;
						}
					}							
				}
				if ($_SESSION['idtipousuario']==5)
				{
					$sql="SELECT mei_virmateria.nombre, mei_virgrupo.nombre
						FROM mei_virmateria, mei_virgrupo
							WHERE mei_virgrupo.idvirmateria = mei_virmateria.idvirmateria 
								AND mei_virgrupo.idvirgrupo='".$_GET['idGrupo']."'";
				}
				else
				{
					$sql="SELECT mei_materia.nombre, mei_grupo.nombre
						FROM mei_materia, mei_grupo
							WHERE mei_grupo.idmateria = mei_materia.idmateria 
								AND mei_grupo.idgrupo='".$_GET['idGrupo']."'";
				}
								
				$consultaGrupo=$baseDatos->ConsultarBD($sql);
				list($nombreMateria,$codigoGrupo)=mysql_fetch_array($consultaGrupo);

				if ($_SESSION['idtipousuario']==5)
					$idTipousuario=6;
				else
					$idTipousuario=3;
				$separador="[---]";
				$espacio="&nbsp;";
				
				if($_POST['cbo_ordenLista'] == 2)
				{
					if ($_SESSION['idtipousuario']==5)
					{
						$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido
								FROM mei_usuario
								WHERE (mei_usuario.idtipousuario='".$idTipousuario."' or mei_usuario.idtipousuario='7')
									AND mei_usuario.idusuario 
										IN (
											SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru 
												WHERE mei_relusuvirgru.idvirgrupo='".$_GET['idGrupo']."') ORDER BY (mei_usuario.primernombre)";
					}
					else
					{
						$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido
								FROM mei_usuario
								WHERE (mei_usuario.idtipousuario='".$idTipousuario."' or mei_usuario.idtipousuario='7')
									AND mei_usuario.idusuario 
										IN (
											SELECT mei_relusugru.idusuario FROM mei_relusugru 
												WHERE mei_relusugru.idgrupo='".$_GET['idGrupo']."') ORDER BY (mei_usuario.primernombre)";
					}
										
					$activoNombre="selected";
				
					$alumnosGrupo=$baseDatos->ConsultarBD($sql);
				
					while(list($idUsuarioGrupo,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($alumnosGrupo))
					{
						$listaAlumnos[$contAlumnos++]=$idUsuarioGrupo.$separador.$espacio.$espacio.$idUsuarioGrupo.$espacio.$espacio.$espacio.$espacio.$primerNombre." ".$segundoNombre." ".$primerApellido." ".$segundoApellido;
					}
				
					if(!empty($listaCodigosSeleccionados))
					{
						$listaCodigos=explode(";",$listaCodigosSeleccionados);
					
						foreach($listaCodigos as $datoCodigo)
						{
							if(empty($listaValoresCodigo))
							{
								$listaValoresCodigo="'".$datoCodigo."'";
							}
							else
							{
								$listaValoresCodigo.=",'".$datoCodigo."'";
							}
						}
					
						$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario
								WHERE (mei_usuario.idtipousuario='".$idTipousuario."' or mei_usuario.idtipousuario='7')
									AND mei_usuario.idusuario 
										IN (".$listaValoresCodigo.") ORDER BY (mei_usuario.primernombre)";
							
						$alumnosSeleccionados=$baseDatos->ConsultarBD($sql);
				
						while(list($idUsuarioGrupo,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($alumnosSeleccionados))
						{
							$listaSeleccionados[$contSeleccionados++]=$idUsuarioGrupo.$separador.$espacio.$espacio.$idUsuarioGrupo.$espacio.$espacio.$espacio.$espacio.$primerNombre." ".$segundoNombre." ".$primerApellido." ".$segundoApellido;
						}
					}
				}
				else if($_POST['cbo_ordenLista'] == 1)
				{
					if ($_SESSION['idtipousuario']==5)
					{
						$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
							FROM mei_usuario
							WHERE (mei_usuario.idtipousuario='".$idTipousuario."' or mei_usuario.idtipousuario='7')
								AND mei_usuario.idusuario 
									IN (
										SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru 
											WHERE mei_relusuvirgru.idvirgrupo='".$_GET['idGrupo']."'
										) ORDER BY (mei_usuario.idusuario)";
					}
					else
					{
						$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
							FROM mei_usuario
							WHERE (mei_usuario.idtipousuario='".$idTipousuario."' or mei_usuario.idtipousuario='7')
								AND mei_usuario.idusuario 
									IN (
										SELECT mei_relusugru.idusuario FROM mei_relusugru 
											WHERE mei_relusugru.idgrupo='".$_GET['idGrupo']."'
										) ORDER BY (mei_usuario.idusuario)";
					}
					
					$activoCodigo="selected";
					
					$alumnosGrupo=$baseDatos->ConsultarBD($sql);
					
					while(list($idUsuarioGrupo,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($alumnosGrupo))
					{
						$listaAlumnos[$contAlumnos++]=$idUsuarioGrupo.$separador.$espacio.$espacio.$idUsuarioGrupo.$espacio.$espacio.$espacio.$espacio.$primerNombre." ".$segundoNombre." ".$primerApellido." ".$segundoApellido;
					}
					
					if(!empty($listaCodigosSeleccionados))
					{
						$listaCodigos=explode(";",$listaCodigosSeleccionados);
						
						foreach($listaCodigos as $datoCodigo)
						{
							if(empty($listaValoresCodigo))
							{
								$listaValoresCodigo="'".$datoCodigo."'";
							}
							else
							{
								$listaValoresCodigo.=",'".$datoCodigo."'";
							}
						}
						
						$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario
								WHERE (mei_usuario.idtipousuario='".$idTipousuario."' or mei_usuario.idtipousuario='7')
									AND mei_usuario.idusuario 
										IN (".$listaValoresCodigo.") ORDER BY (mei_usuario.idusuario)";
										
						$alumnosSeleccionados=$baseDatos->ConsultarBD($sql);
						
						while(list($idUsuarioGrupo,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($alumnosSeleccionados))
						{
							$listaSeleccionados[$contSeleccionados++]=$idUsuarioGrupo.$separador.$espacio.$espacio.$idUsuarioGrupo.$espacio.$espacio.$espacio.$espacio.$primerNombre." ".$segundoNombre." ".$primerApellido." ".$segundoApellido;
						}
					}
				}
				else
				{
					if ($_SESSION['idtipousuario']==5)
					{
						$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
							FROM mei_usuario
							WHERE (mei_usuario.idtipousuario='".$idTipousuario."' or mei_usuario.idtipousuario='7')
								AND mei_usuario.idusuario 
									IN (
										SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru 
											WHERE mei_relusuvirgru.idvirgrupo='".$_GET['idGrupo']."'
										) ORDER BY (mei_usuario.primerapellido)";
					}
					else
					{
						$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
							FROM mei_usuario
							WHERE (mei_usuario.idtipousuario='".$idTipousuario."' or mei_usuario.idtipousuario='7')
								AND mei_usuario.idusuario 
									IN (
										SELECT mei_relusugru.idusuario FROM mei_relusugru 
											WHERE mei_relusugru.idgrupo='".$_GET['idGrupo']."'
										) ORDER BY (mei_usuario.primerapellido)";
					}
										
					$activoApellido="selected";
					
					$alumnosGrupo=$baseDatos->ConsultarBD($sql);
					
					while(list($idUsuarioGrupo,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($alumnosGrupo))
					{
						$listaAlumnos[$contAlumnos++]=$idUsuarioGrupo.$separador.$espacio.$espacio.$idUsuarioGrupo.$espacio.$espacio.$espacio.$espacio.$primerApellido." ".$segundoApellido." ".$primerNombre." ".$segundoNombre;
					}
					
					if(!empty($listaCodigosSeleccionados))
					{
						$listaCodigos=explode(";",$listaCodigosSeleccionados);
						
						foreach($listaCodigos as $datoCodigo)
						{
							if(empty($listaValoresCodigo))
							{
								$listaValoresCodigo="'".$datoCodigo."'";
							}
							else
							{
								$listaValoresCodigo.=",'".$datoCodigo."'";
							}
						}
					
						$sql="SELECT mei_usuario.idusuario,mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario
								WHERE (mei_usuario.idtipousuario='".$idTipousuario."' or mei_usuario.idtipousuario='7')
									AND mei_usuario.idusuario 
										IN (".$listaValoresCodigo.") ORDER BY (mei_usuario.primerapellido)";
									
						$alumnosSeleccionados=$baseDatos->ConsultarBD($sql);
					
						while(list($idUsuarioGrupo,$primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($alumnosSeleccionados))
						{
							$listaSeleccionados[$contSeleccionados++]=$idUsuarioGrupo.$separador.$espacio.$espacio.$idUsuarioGrupo.$espacio.$espacio.$espacio.$espacio.$primerApellido." ".$segundoApellido." ".$primerNombre." ".$segundoNombre;
						}
					}
				}//***
				
				if(empty($idTipoSubgrupo))
				{				
					if(isset($_POST['cbo_tipoSubgrupo']))
					{
						$idTipoSubgrupo=$_POST['cbo_tipoSubgrupo'];
					}
					else if(isset($_GET['idTipoSubgrupo']))
					{
						$idTipoSubgrupo=$_GET['idTipoSubgrupo'];
					}				
				}
				
				?>
					<html>
						<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
							<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
							<script language="javascript" src="listaSubgrupo.js"></script>
							<script language="javascript">
							
								function ordenarLista(idGrupo,idSubGrupo)
								{
									document.frm_listaSubGrupo.action="modificarSubgrupo.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idGrupo="+idGrupo+"&idSubGrupo="+idSubGrupo;
									document.frm_listaSubGrupo.submit();
								}
								
								function enviarCancelar(idGrupo,idTipoSubgrupo)
								{									
										location.replace("../materias/listarSubgrupos.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idTipoSubgrupo="+idTipoSubgrupo+"&idGrupo="+idGrupo);
								}
							
							</script>
							
							<style type="text/css">	.Estilo2 {color: #FF0000}</style>
							
						</head>
						<body>
							<table class="tablaPrincipal">
								<tr valign="top">
                                    <td class="tablaEspacio">&nbsp;</td>
                                    <td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']); ?>	</td>
                                    <td class="tablaEspacio">&nbsp;</td>
									<td>
										<form method="post" name="frm_listaSubGrupo" action="actualizarSubgrupo.php?idSubGrupo=<?= $_GET['idSubGrupo']?>&idGrupo=<?= $_GET['idGrupo']?>&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>">
											<table class="tablaGeneral">
												<tr class="trTitulo">
													<td colspan="2" class="trTitulo"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop"> Modificar  Subgrupo</td>
												</tr>
												<tr class="trSubTitulo">
													<td colspan="2" class="trSubTitulo">Modifique la informaci&oacute;n del  Subgrupo en <b><?= $nombreMateria?></b> Grupo <b><?= $codigoGrupo?></b></td>
												</tr>
												<tr class="trSubTitulo">
													<td colspan="2" >&nbsp;</td>
												</tr>
			<?
				if($_GET['error']=="0x001" || $_GET['error']=="0x003")
				{
			?>
												<tr class="trAviso">
													<td colspan="2" ><span class="Estilo2"> <img src="imagenes/error.gif" width="17" height="18" align="texttop"> Error: No se ha podido Modificar el subgrupo con el nombre <b><?= $nombreSubgrupo?></b>. Ya existe un subgrupo con ese nombre en el grupo <b><?= $codigoGrupo?></b>.</span></td>
												</tr>
			<?
				}			
				
				if($_GET['error']=="0x002" || $_GET['error']=="0x003")
				{
			?>
												<tr class="trAviso">
													<td colspan="2" ><span class="Estilo2"> <img src="imagenes/error.gif" width="17" height="18" align="texttop"> Error: No se ha podido Modificar el subgrupo con el nombre<b> <?= $nombreSubgrupo?></b>. Ha seleccionado alumnos que ya se encuentra en otro subgrupo.</span></td>
												</tr>
			<?
				}
			?>
												<tr class="trSubTitulo">
													<td><b>Nombre Subgrupo:</b></td>
													<td><input name="txt_nombreSubgrupo" type="text" size="40" value="<?= $nombreSubgrupo?>"></td>
												</tr>
												<tr class="trInformacion">
													<td><b>Descripcion:</b></td>
													<td width="77%"><input name="txt_descripcionSubGrupo" type="text" id="txt_descripcionSubGrupo" value="<?= $descripcionSubGrupo?>" size="60"></td>
												</tr>
												<tr class="trInformacion">
													<td><b>Subgrupo de :</b></td>
													<td><select name="cbo_tipoSubgrupo">
													<?
													$sql="SELECT DISTINCT mei_tiposubgrupo.idtiposubgrupo,mei_tiposubgrupo.tiposubgrupo FROM mei_tiposubgrupo";
													$tipoAlumnos=$baseDatos->ConsultarBD($sql);
													while(list($codigoTipoSubgrupo,$tipoSubgrupo)=mysql_fetch_array($tipoAlumnos))
													{
														 unset($seleccion);

														 if($idTipoSubgrupo==$codigoTipoSubgrupo)
														 {
															$seleccion="selected";
														 }
													?>
															<option value="<?=$codigoTipoSubgrupo?>" <?=$seleccion?>><?= ucwords($tipoSubgrupo)?></option>
													<?
													}
													?>
													</select></td>
												</tr>
												
												
												<tr class="trInformacion">
													<td colspan="2">
														<table width="100%" bgcolor="#DDDDDD" class="tablaPrincipal">
															<tr>
																<td colspan="4" valign="top">Seleccione de la lista los alumnos que conforman el nuevo Subgrupo: </td>
															</tr>
															<tr>
															  <td align="center"><input type="hidden" name="hid_listaValores" value="<?= $listaCodigosSeleccionados?>">
															    <input name="hid_idSubGrupo" type="hidden" id="hid_idSubGrupo" value="<?= $_GET['idSubGrupo']?>">
																 <input name="hid_listaErrores" type="hidden" id="hid_listaErrores" value="<?= $listaErrores?>"></td>
																<td align="center"><div align="center">Grupo<b> <?= $codigoGrupo?></b></div></td>
																<td align="center">Nuevo Subgrupo </td>
																<td>&nbsp;</td>
															</tr>
															<tr>
																<td valign="top">&nbsp;</td>
																<td valign="top">
																	<select name="cbo_alumnos" size="13" onChange="moverItem();">
			<?
				if(sizeof($listaAlumnos) > 0)
				{
					$cont=0;
								
					foreach($listaAlumnos as $datosAlumno)
					{
						list($idUsuarioGrupo,$datoNombre)=explode($separador,$datosAlumno);
			?>
																		<option value="<?= $idUsuarioGrupo?>"><?= $datoNombre?></option>
			<?
					}
				}
			?>
																	</select>
																</td>
																<td valign="top">
																	<select multiple name="cbo_listaSubgrupo" size="13">
			<?
			$listaCodigoErrores=explode("[$$$]",$listaErrores);	
				if(sizeof($listaSeleccionados) > 0)
				{
					$cont=0;
					
					foreach($listaSeleccionados as $datosSeleccionado)
					{
						list($idUsuarioGrupo,$datoNombre)=explode($separador,$datosSeleccionado);
						
						if(sizeof($listaCodigoErrores))
						{						
							if(in_array($idUsuarioGrupo,$listaCodigoErrores))
							{
								$class="class='Estilo2'";
							}
							else
							{
								$class="";
							}
						}
			?>
																		<option value="<?= $idUsuarioGrupo?>" <?= $class?>><?= $datoNombre?></option>
			<?
					}
				}
			?>
																	</select>
																</td>
																<td valign="top">&nbsp;</td>
															</tr>
															<tr>
																<td colspan="2" valign="top">Ordenar Listas por :
																	<select name="cbo_ordenLista" id="cbo_ordenLista" onChange="javascript: ordenarLista('<?= $_GET['idGrupo']?>','<?= $_GET['idSubGrupo']?>')">
																		<option value="1" <?= $activoCodigo?>>Codigo</option>
																		<option value="2" <?= $activoNombre?>>Nombre</option>
																		<option value="3" <?= $activoApellido?>>Apellido</option>
																	</select>
																</td>
																	<td valign="top" width="151"><input type="button" value="Eliminar seleccionados" onClick="javascript: borrarItem();" name="btn_eliminarItem"></td>
																<td valign="top" width="438">&nbsp;</td>
															</tr>
															<tr>
															  <td colspan=4 height=10>&nbsp;</td>
															</tr>
														</table>
														<table width="208" align="center">
															<tr>
																<td width="120"><div align="center"><input type="button" value="Modificar Subgrupo" onClick="javascript: guardarLista();" name="btn_valores">
																</div></td>
																<td width="76"><div align="center"><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar('<?= $_GET['idGrupo']?>','<?=$idTipoSubgrupo?>')"></div></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</form>
									</td>
                                    <td class="tablaEspacio">&nbsp;</td>
                                    <td class="tablaDerecho"><?=menu1($_SESSION['idtipousuario']); ?>	</td>            
                                    <td class="tablaEspacioDerecho">&nbsp;</td>                                     
								</tr>
							</table>
						</body>
					</html>
			<? 
				}
				else
				{
					if ($_SESSION['idtipousuario']==5)
						redireccionar("../usuario/VirListarProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
					else
						redireccionar("../usuario/listarProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
				}
			}
		else
		{
		redireccionar('../login/');
		}
	}
	else
	{
	redireccionar('../login/');
	}
?>