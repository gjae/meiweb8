<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../estadistica/Grafica.class.php');	
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{
	
	
	$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=8";
	$resultado=$baseDatos->ConsultarBD($sql);
	list($estado)=mysql_fetch_array($resultado);
	if($estado==1)
	{
	
	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
	{
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<?
		$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario ='".$_GET['idUsuario']."'";
		
		$consultaUsuario=$baseDatos->ConsultarBD($sql);

		
		list($nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($consultaUsuario);
		
		
		$sql="SELECT mei_modulo.nombre,mei_modulo.idmodulo FROM mei_modulo WHERE mei_modulo.estado=1 AND mei_modulo.estadistica=1";
		$modulos=$baseDatos->ConsultarBD($sql);
		
		$indice=0;
		$visitasTotales=0;
		
		while (list($modulo,$codigoModulo)=mysql_fetch_array($modulos))
		{
			$sql="SELECT count( * ) 
					FROM mei_bitacora
						WHERE mei_bitacora.idmodulo ='".$codigoModulo."'
							AND mei_bitacora.idusuario = '".$_GET['idUsuario']."'
								AND mei_bitacora.idtipoaccion
									IN (
											SELECT mei_tipoaccion.idtipoaccion
												FROM mei_tipoaccion
													WHERE mei_tipoaccion.estadistica =1
										) ";
						
					$visitaModulo=$baseDatos->ConsultarBD($sql);
					
					list($visitaModulo) = mysql_fetch_array($visitaModulo);
					$visitasTotales+=$visitaModulo;
					
					if(empty($listaModulos))
					{
						$listaModulos=ucwords($modulo);
						$listaValores=$visitaModulo;
					}
					else
					{
						$listaModulos.=",".ucwords($modulo);
						$listaValores.=",".$visitaModulo;
					}
					
					
			$listaVisitaModulo[$indice++]=ucwords($modulo)."[$$$]".$visitaModulo;
		}
		
		$mediaGeneral=$visitasTotales/$indice;
		
		
		?>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td >
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td colspan="2" ><img src="imagenes/estadistica.gif" width="16" height="16" align="texttop"> Estad&iacute;stica de Uso por M&oacute;dulos</td>
						</tr>
						<tr class="trListaOscuro">
						  <td bgcolor="#DDDDDD">Estad&iacute;stica del uso de los modulos de MeiWeb de <b>
						    <?= $nombre1." ".$nombre2." ".$apellido1." ".$apellido2?>
						  </b></td>
					      <td bgcolor="#DDDDDD"><div align="right"><a href="../bitacora/index.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idUsuario=<?= $_GET['idUsuario']?>" title="Ver Bitacora del uso de MeiWeb" class="link"><b><img src="../bitacora/imagenes/bitacora.gif" width="16" height="16" border="0"> Ver Bitacora </b></a></div></td>
					  </tr>
						<tr class="trListaOscuro">
						  <td colspan="2" >
					<br>
					<table width="400" align="center" bgcolor="#FFFFFF" class="tablaPrincipal" style="border-collapse:collapse; border: 1px solid #999999;">
						  
                            <tr>
                              <td align="center" class="trSubTitulo">Gr&aacute;fica General de Uso por M&oacute;dulos</td>
                            </tr>
                            <tr>
                              <td align="center">&micro;=<?= round($mediaGeneral,2)?></td>
                            </tr>
                            <tr>
                              <td align="center"><table width="200" border="0" align="left" bgcolor="#FFFFFF" class="tablaPrincipal">
                                <tr>
                                  <td width="12" height="10" bordercolor="#999999" bgcolor="#FFCC66">&nbsp;</td>
                                  <td width="178"> Valores &lt; &micro;</td>
                                </tr>
                                <tr>
                                  <td width="12" height="10" bgcolor="#FF6600">&nbsp;</td>
                                  <td>Valores &gt; &micro;</td>
                                </tr>
                              </table></td>
                            </tr>
                            <tr>
                              <td align="center">&nbsp;</td>
                            </tr>
                            <tr>
                              <td align="center">
							  <?
							  $grafica = new BAR_GRAPH("vBar");
								$grafica->values = $listaValores;
								$grafica->showValues = 1;
								$grafica->absValuesBorder = "0px solid silver";
								$grafica->absValuesBGColor = "#FFFFFF";
								$grafica->labelBGColor = "#DDDDDD";
								$grafica->labelBorder = "1px solid #A0A0A0";
								$grafica->labels = $listaModulos;
								$grafica->barLevelColor = array(1, "#FFCC66", $mediaGeneral, "#FF6600");
								$grafica->labelSpace = 5;
								print  $grafica->create();
							  ?>
							  <div align="center"></div></td>
                            </tr>
							
                          </table>
					<br></td>
					  </tr>
						<tr class="trListaOscuro">
							<td colspan="2" >
					<table class="tablaPrincipal" width="400" align="center" style="border-collapse:collapse; border: 1px solid #999999;">

						<tr class="trSubTitulo">
							<td width="60%" class="trSubTitulo"><div align="center">Modulo</div></td>
							<td width="20%" class="trSubTitulo"><div align="center">N&ordm; Visitas</div></td>
							<td width="20%" class="trSubTitulo"><div align="center">% Visitas </div></td>
							
						</tr>
						
					<?
					
					$porcentajeTotal=0;
					
					for($i=0;$i<sizeof($listaVisitaModulo);$i++)
					{
						list($modulo,$visita)=explode("[$$$]",$listaVisitaModulo[$i]);
						
						if($visitasTotales > 0)
						{
							$porcentajeVisita=100*$visita/$visitasTotales;						
						}
						else
						{
							$porcentajeVisita=0;
						}
						$porcentajeTotal+=$porcentajeVisita;
						$porcentajeVisita=round($porcentajeVisita,2);
						
												
							if($i%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}
						?>
						<tr class="<?=$color?>">
							<td  class="<?=$color?>">M&oacute;dulo <?=$modulo?></td>
							<td  class="<?=$color?>"><div align="right"><?=$visita?>
							</div></td>
							<td  class="<?=$color?>"><div align="right"><?=$porcentajeVisita?> %</div></td>
							
						</tr>
					<?					
						
					}
					?>
						<tr class="trInformacion">
							<td class="trInformacion"><div align="center"><b>Total</b></div></td>
							<td class="trInformacion"><div align="right"><b><?=$visitasTotales?>
						    </b></div></td>
							<td class="trInformacion"><div align="right"><b><?= $porcentajeTotal?> %</b></div></td>
						</tr>
					</table><br>
							
							
							</td>
						</tr>
						
				  </table>
					<br>
					
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
		</table>
		</body>
		</html>
<? 
	}

	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../scripts/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
	
?>
