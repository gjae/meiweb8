<?
	include_once('../baseDatos/BD.class.php');	
	include_once ('../librerias/estandar.lib.php');
	
	
	class Contenido
	{//clase 1
		var $m_nombreMateria;
		var $m_codigo;
		var $m_nCreditos;
		var $m_fechaModificacion;
		var $m_introduccion;
		var $m_estadoIntroduccion;
		
		var $m_longRecuperadoCorto;
		var $m_longRecuperadoLargo;
		var $m_contenidoHtml;
		var $m_saltoLinea;
		var $m_saltoParrafo;
		var $m_finLinea;
		var $m_idmateria;
		
		var $m_destinoMostrar;
		var $m_destinoMover;
		var $m_destinoEliminar;
		var $m_destinoActivar;
		var $m_destinoAgregar;
		
		var $m_colorTablaDentro;
		var $m_colorTablaFuera;
		var $m_colorTablaClic;
		
		var $m_contItems;
		var $m_limiteItems;
		var $m_baseDatos;
		
		function Contenido($a_idmateria)
		{
			$this->m_longRecuperadoCorto=85;
			$this->m_longRecuperadoLargo=300;
			$this->m_saltoLinea="<br>";
			$this->m_saltoParrafo="<br><br>";
			$this->m_finLinea="\n";
			$this->m_contenidoHtml='';
			
			$this->m_colorTablaDentro="#E0E0E0"; //cuando va pasando
			$this->m_colorTablaFuera="#EFEFEF"; //como queda despues de pasar por ahi
			$this->m_colorTablaClic="#DDEEDD";
			
			$this->m_contItems=0;
			$this->m_limiteItems=10;
			
			$this->m_idmateria=$a_idmateria;
			
			$this->m_baseDatos= new BD();
			$this->DatosMateriaContenido();			
			
			$this->m_destinoMostrar="mostrarContenido.php?idmateria=".$this->m_idmateria."&materia=".$this->m_nombreMateria;
			$this->m_destinoMover="moverContenido.php?idmateria=".$this->m_idmateria;
			$this->m_destinoEliminar="eliminarContenido.php?idmateria=".$this->m_idmateria;
			$this->m_destinoActivar="activarContenido.php?idmateria=".$this->m_idmateria;
			$this->m_destinoAgregar="agregarContenido.php?idmateria=".$this->m_idmateria;
			$this->m_destinoRechazar="rechazarContenido.php?idmateria=".$this->m_idmateria;			
		}

		function MostrarFechaContenido($a_fecha,$a_diaSemana)
		{//FUNCION MostrarFechaContenido
			list($agno,$mes,$dia)=explode('-',$a_fecha);
			
			switch ($a_diaSemana)
			{
				case 0: 
					$textA_diaSemana="Domingo";
					break;
				case 1: 
					$textA_diaSemana="Lunes";
					break;
				case 2: 
					$textA_diaSemana="Martes";
					break;
				case 3: 
					$textA_diaSemana="Miercoles";
					break;
				case 4: 
					$textA_diaSemana="Jueves";
					break;
				case 5: 
					$textA_diaSemana="Viernes";
					break;
				case 6: 
					$textA_diaSemana="Sabado";
					break;
			}
			
			switch ($mes)
			{
				case 1: 
					$textA_mes="Enero";
					break;
				case 2: 
					$textA_mes="Febrero";
					break;
				case 3: 
					$textA_mes="Marzo";
					break;
				case 4: 
					$textA_mes="Abril";
					break;
				case 5: 
					$textA_mes="Mayo";
					break;
				case 6: 
					$textA_mes="Junio";
					break;
				case 7: 
					$textA_mes="Julio";
					break;
				case 8: 
					$textA_mes="Agosto";
					break;
				case 9: 
					$textA_mes="Septiembre";
					break;
				case 10: 
					$textA_mes="Octubre";
					break;
				case 11: 
					$textA_mes="Noviembre";
					break;
				case 12: 
					$textA_mes="Diciembre";
					break;
			}
			
			return $textA_diaSemana." ".$dia." de ".$textA_mes." de ".$agno;
		}//FIN FUNCION MostrarFechaContenido

		function JavascriptIndiceContenido()
		{//FUNCION JavascriptIndiceContenido
?>
			<script language="javascript">
			
				function dentroItem(tabla,color)
				{
					if (!tabla.contains(event.fromElement))
					{ 
						tabla.bgColor = color; 
					} 
				}
				
				function fueraItem(tabla,color)
				{
					if (!tabla.contains(event.toElement)) 
					{ 
						tabla.bgColor = color ; 
					} 
				}
				
			</script>
<?
		}//FIN FUNCION JavascriptIndiceContenido
		
		function JavascriptIndiceEdicion()
		{//FUNCION JavascriptIndiceEdicion
		?>
			<script language="javascript">
			
				function dentroItemChk(tabla,color,ChkId)
				{
					var chk=document.getElementById(ChkId);
					if(chk.checked == false)	
					{
						if (!tabla.contains(event.fromElement))
						{ 
							tabla.bgColor = color; 
						}
					} 
				}
				
				function fueraItemChk(tabla,color,ChkId)
				{
					var chk=document.getElementById(ChkId);
					if(chk.checked == false)	
					{
						if (!tabla.contains(event.toElement)) 
						{ 
							tabla.bgColor = color ; 
						}
					} 
				}
				
				function clicItemChk(tabla,colorTrue,colorFalse,ChkId)
				{
					var chk=document.getElementById(ChkId);
					if(chk.checked == true)	
					{
						if (!tabla.contains(event.toElement)) 
						{ 
							tabla.bgColor = colorTrue ; 
						}
					} 
					else
					{
						if (!tabla.contains(event.toElement)) 
						{ 
							tabla.bgColor = colorFalse ; 
						}
					}
				}
				
				function enviarItem(destino,opcion)
				{
					
					var contBandera=0;
					for(i=0;i<document.frm_indiceEdicion.elements.length;i++)
					{
						if(document.frm_indiceEdicion.elements[i].checked==true)
						{
							contBandera++;
						}
					}
				
					if(contBandera == 0)
					{
						alert('Ningun item seleccionado');
					}
					else
					{
						if(opcion==1) //Eliminar
						{
							if(confirm("¿Está seguro que desea eliminar los elementos seleccionados y todo su contenido?"))
							{
								document.frm_indiceEdicion.action= destino;
								document.frm_indiceEdicion.submit();						
							}				
						}
						else if(opcion==2) // Activar
						{
							if(confirm("¿Desea activar los elementos seleccionados?"))
							{
								document.frm_indiceEdicion.action= destino+"&activar=1";
								document.frm_indiceEdicion.submit();						
							}		
						}
						else if(opcion==3) // Descactivar
						{
							if(confirm("¿Desea desactivar los elementos seleccionados?"))
							{
								document.frm_indiceEdicion.action= destino+"&desactivar=1";
								document.frm_indiceEdicion.submit();						
							}		
						}
					}
				}
				
				function enviarAgregar(destino,opcion,tema)
				{
					if(opcion==1)
					{
						//location.replace(destino+"&tema=1");						
						location.replace(destino+"&tipotema=1");						
					}
					else if(opcion==2)
					{
						//location.replace(destino+"&subTema=1&idTema="+tema);
						location.replace(destino+"&tipotema=2&idTemaPadre="+tema);
					}
				}
								
				function cambiarSeleccionItem()
				{
					for(i=0;i<document.frm_indiceEdicion.elements.length;i++)
					{
						if(document.frm_indiceEdicion.elements[i].checked==true)
						{
							document.frm_indiceEdicion.elements[i].checked=false;
						}
						else if(document.frm_indiceEdicion.elements[i].checked==false)
						{
							document.frm_indiceEdicion.elements[i].checked=true;
						}
					}
				}
				
				function enviarRechazar(destino)
				{
					if(confirm("¿Desea eliminar este elemento?"))
					{
						location.replace(destino);						
					}
				}
				var ventanaTema=false;
				function exportarContenido(idmateria, indice)
				{	
					   var izquierda = (screen.availWidth - 700) / 2; 
					   var arriba = (screen.availHeight - 600) / 2; 
					
													
					if (typeof ventanaTema.document == 'object')
					 {
						ventanaTema.close()
					 }
					 
					ventanaTema = window.open('../contenido/exportarContenido.php?idmateria='+idmateria+'&indice='+indice,'exportarContenido','width=700,height=600,left='+izquierda+',top='+arriba+',scrollbars=YES,toolbar=NO,titlebar=NO,menubar=YES,status=NO,statusbar=NO,resizable=NO,location=NO');				
				}
			</script>
		<?
		}//FIN FUNCION JavascriptIndiceEdicion
				
		function DatosMateriaContenido()
		{
			$sql = "SELECT mei_materia.nombre, mei_materia.codigo, mei_materia.creditos, mei_materia. fechacreacion, mei_materia.introduccion FROM mei_materia 
					WHERE mei_materia.idmateria=".$this->m_idmateria;

			$consulta=$this->m_baseDatos->ConsultarBD($sql);
			
			list($this->m_nombreMateria,$this->m_codigo,$this->m_nCreditos,$this->m_fechaModificacion,$this->m_introduccion,$this->m_estadoIntroduccion)=mysql_fetch_array($consulta);
		}
		
		function IndiceContenido()
		{
			$this->JavascriptIndiceContenido();			
			
?>
	<!-- 1 -->
<?
		}
//************************************************************
		function BarraRecuperacionContenido()
		{//FUNCION BarraRecuperacionContenido
			$contTemp=0;
					
			$sql="SELECT mei_tematemp.idtematemp, mei_tematemp.idtemapadre, mei_tematemp.titulo , mei_tematemp.contenido , mei_tematemp.estado, mei_tematemp.tipo , mei_tematemp.fecha , mei_tematemp.diahora FROM mei_tematemp
				  WHERE mei_tematemp.idmateria =".$this->m_idmateria." ORDER BY (mei_tematemp.fecha) DESC";
			$consultaTemaTemp=$this->m_baseDatos->ConsultarBD($sql);			
			
			while(list($idItem, $idpadreItem,$tituloItem,$contenidoItem,$estadoItem, $tipoItem,$fechaItem,$diahoraItem)=mysql_fetch_array($consultaTemaTemp))
			{							
				$idDestinoItem="&tipotema=".$tipoItem."&idTemaTemp=".$idItem."&idTemaPadre=".$idpadreItem; 
				$tipoTemp=0;
				
				list($dia,$hora)=explode('-',$diahoraItem);
				
				$titleItem=$tituloItem."\n";
				$titleItem.="Recuperado: ".$this->MostrarFechaContenido($fechaItem,$dia).", ".$hora."\n";
				$titleItem.="\nContenido: \n\n";
				
				if(strlen($contenidoItem) > $this->m_longRecuperadoLargo)
				{
					$titleItem.=substr($contenidoItem,0,$this->m_longRecuperadoLargo).'...';					
				}
				else
				{
					$titleItem.=$contenidoItem;
				}
				$titleItem=strip_tags($titleItem);
				
				$tituloContenido=$tituloItem.'[$$$]'.$contenidoItem;
				
				if(strlen($tituloContenido) > $this->m_longRecuperadoCorto)
				{
					$tituloContenido=substr($tituloContenido,0,$this->m_longRecuperadoCorto);					
					$tituloContenido.='...';
				}
				$tituloContenido=strip_tags($tituloContenido);
				
				list($tituloItem,$contenidoItem)=explode('[$$$]',$tituloContenido);				
				$contenidoItem=', '.$contenidoItem;
				
				$listaTemp[$contTemp]=$idItem.'[$$$]'.$tituloItem.'[$$$]'.$contenidoItem.'[$$$]'.$estadoItem.'[$$$]'.$fechaItem.'[$$$]'.$diahoraItem.'[$$$]'.$titleItem.'[$$$]'.$idDestinoItem.'[$$$]'.$tipoItem;
				$contTemp++;
			}
			if($contTemp > 0)
			{
						
			?>
			<table class="tablaGeneral" width="100%">
              <tr class="trTitulo">
                <td>Elementos Recuperados </td>                
              </tr>
              <tr>
                <td>
					<table class="tablaGeneral" width="100%" >
			<?
			
			foreach($listaTemp as $item)
			{
				
				list($idItem,$tituloItem,$contenidoItem,$estadoItem,$fechaItem,$diahoraItem,$titleItem,$idDestinoItem,$tipoItem)=explode('[$$$]',$item);

		?>
					  <tr>
						<td class="trSubTitulo" width="2%" valign="top">
						<?
							if($tipoItem==1)
							{
						?>
							<img src="imagenes/vinetaTemaTemp.gif" alt="Tema recuperado" width="15" height="17" border="0">
						<?
							}
							else if($tipoItem==2)
							{
							?>
							<img src="imagenes/vinetaSubTemaTemp.gif" alt="Subtema recuperado" width="15" height="17" border="0">
							<?
							}
						?>	
						</td>
						<td class="trSubTitulo" width="95%"><a href="editarContenido.php?idmateria=<?=$_GET["idmateria"]?><?= $idDestinoItem?>" target="_self" title="<?= $titleItem?>" class="link"><strong><?= $tituloItem?></strong></a><em><?= $contenidoItem?></em></td>						
						<td class="trSubTitulo" width="3%" valign="top"><a href="javascript: enviarRechazar('<?= $this->m_destinoRechazar.$idDestinoItem?>')"><img src="imagenes/rechazarContenido.gif" alt="Rechazar elemento recuperado" width="21" height="20" border="0"></a></td>
					  </tr>
		<?
			}
		?>
					</table>
				</td>
              </tr>
        </table>
	    <?		
			}	
		}//FIN FUNCION BarraRecuperacionContenido
		
		function BarraHerramientas()
		{//FUNCION BarraHerramientas
			?>
				<table width="100%" class="tablaGeneral" >					
					<tr align="center" valign="middle" class="trTitulo">
						<td width="22" height="23" align="left" bordercolor="#D6D6D6"><b><a href="javascript:enviarAgregar('<?= $this->m_destinoAgregar?>',1,false)"><img src="../contenido/imagenes/nuevoTema.gif" alt="Nuevo Tema" width="21" height="20" border="0"></a></b></td>
						<td width="6" align="left" bordercolor="#F5F5F5"><img src="imagenes/separadorLinea.gif" width="5" height="21" border="0"></td>
						<td width="22" align="left" bordercolor="#D6D6D6"><a href="javascript:cambiarSeleccionItem();"><img src="../contenido/imagenes/cambiarSeleccion.gif" alt="Cambiar Seleccion" width="22" height="20" border="0"></a></td>
						<td width="6" align="left" bordercolor="#F5F5F5"><img src="imagenes/separadorLinea.gif" width="5" height="21" border="0"></td>

						<td width="22" align="left" bordercolor="#D6D6D6" ><a href="javascript:exportarContenido(<?=$this->m_idmateria?>,'1')"><img src="imagenes/guardarTemp.gif" alt="Guardar Indice en Archivo" width="21" height="20" border="0" /></a></td>
						<td width="22" align="left" bordercolor="#D6D6D6" ><a href="javascript:exportarContenido(<?=$this->m_idmateria?>,'0')"><img src="imagenes/applix.jpg" alt="Guardar Contenido en Archivo" width="16" height="16" border="0" /></a></td>
						<td width="429" align="left" bordercolor="#F5F5F5">&nbsp;</td>
						<td width="48" align="right" bordercolor="#F5F5F5" ><a href="javascript: enviarItem('<?= $this->m_destinoEliminar?>',1)" class="link">Eliminar</a></td>
						<td width="44" align="right" bordercolor="#F5F5F5"><a href="javascript: enviarItem('<?= $this->m_destinoActivar?>',2)" class="link">Activar</a></td>
						<td width="64" align="right" bordercolor="#F5F5F5" ><a href="javascript: enviarItem('<?= $this->m_destinoActivar?>',3)" class="link">Desactivar</a></td>
				  </tr>
		</table>			
			<?
		}//FIN FUNCION BarraHerramientas
			
			
		function consulta($idTema, $titulo, $cont, $numeral,$archivo, $archivo_contenido)
		{//FUNCION CONSULTA
				$sql="SELECT MIN( mei_tema.orden ) , MAX( mei_tema.orden ) FROM mei_tema WHERE mei_tema.idtemapadre =".$idTema;
				$consultaOrdenSubtema=$this->m_baseDatos->ConsultarBD($sql);
				list($minOrdenSubTema,$maxOrdenSubTema)=mysql_fetch_array($consultaOrdenSubtema);				
				
				$sql="SELECT mei_tema.idtema , mei_tema.titulo, mei_tema.estado, mei_tema.orden, mei_tema.contenido FROM mei_tema
					  WHERE mei_tema.idtemapadre =".$idTema." ORDER BY (mei_tema.orden)";
			
				$consultaSubTema=$this->m_baseDatos->ConsultarBD($sql);					
				$contSubTemas=1;
				if ( !empty($consultaSubTema) )
				{//IF consultaSubTema
				
					while(list($idSubTema, $tituloSubTema,$estadoSubTema,$ordenSubTema,$contenido)=mysql_fetch_array($consultaSubTema))
					{//WHILE LISTconsultaSubTema
							//print $contSubTemas."<p>";
		?>				
						<table width="100%" onClick="javascript: clicItemChk(this,'<?= $this->m_colorTablaClic?>','<?= $this->m_colorTablaFuera?>','chk_subTema[<?= $idSubTema?>]')" onMouseOver="javascript: dentroItemChk(this,'<?= $this->m_colorTablaDentro?>','chk_subTema[<?= $idSubTema?>]')" onMouseOut="javascript: fueraItemChk(this,'<?= $this->m_colorTablaFuera?>','chk_subTema[<?= $idSubTema?>]')" class="tablaPrincipal">
							<tr>
							  <td width="<?=$cont?>%">&nbsp;</td>
								<td width="<?=3+$cont?>%">&nbsp;
							  </td>
							  <td width="3%"><input type="checkbox" name="chk_subTema[<?= $idSubTema?>]" id="chk_subTema<?= $idSubTema?>" value="<?= $idSubTema?>"></td>
								<td width="<?=80-(2*$cont)?>%" align="left"><a href="<?= $this->m_destinoMostrar?>&idSubTema=<?= $idSubTema?>&num=<?=$numeral.". ".$contSubTemas.". "?>" target="_self" class="link">
								<?= $numeral.". ".$contSubTemas.". ".$tituloSubTema?></a></td>
								<? 
									$espacio = "  ";
									for ($i=1;$i<=$cont;$i++)
										$espacio .= "  "; 
								fwrite($archivo, $espacio.$numeral.". ".$contSubTemas.". ".$tituloSubTema."\n");
								fwrite($archivo, "\n");
								
								fwrite($archivo_contenido, $contSubTemas.". ".$tituloSubTema."\n");
								fwrite($archivo_contenido, "\n");
								fwrite($archivo_contenido, strip_tags($contenido));
								fwrite($archivo_contenido, "\n");
								if($estadoSubTema==0 && $estadoTema==0)
								{
								?>
								<td width="9%">&nbsp;</td>
								<?
								}
								else if($estadoSubTema==1)
								{
								?>
								<td width="9%" align="right" valign="middle"><img src='imagenes/inactivo.gif' alt='Inactivo' width='16' height='16' border='0' /></td>
								<?
								}
								?>	
								<td width="2%"><a href="javascript:enviarAgregar('<?= $this->m_destinoAgregar?>',2,<?= $idSubTema?>)"><img src="../contenido/imagenes/nuevoSubTema.gif" alt="Agregar Subtema" width="16" height="17" border="0"></a></td>
								<td width="3%" valign="middle">
								<?
								if($ordenSubTema==$minOrdenSubTema)
								{
								?>
									<img src="../contenido/imagenes/arribaSubTemaClaro.gif" width="15" height="17" border="0">
									<?
								}
								else
								{
								?>									  
								  <a href="<?= $this->m_destinoMover?>&arriba=1&idTemaPadre=<?= $idTema?>&idTema=<?= $idSubTema?>&orden=<?= $ordenSubTema?>" target="_self"><img src="../contenido/imagenes/arribaSubTema.gif" alt="Mover arriba " width="15" height="17" border="0"></a>
								<?
								}
								?></td>
								<td width="3%" valign="middle">
								<?
								if($ordenSubTema>=$maxOrdenSubTema)
								{
								?>								
									<img src="../contenido/imagenes/abajoSubTemaClaro.gif" width="15" height="17" border="0">
								<?
								}
								else
								{
								?>
									<a href="<?= $this->m_destinoMover?>&abajo=1&idTemaPadre=<?= $idTema?>&idTema=<?= $idSubTema?>&orden=<?= $ordenSubTema?>" target="_self"><img src="../contenido/imagenes/abajoSubTema.gif" alt="Mover abajo" width="15" height="17" border="0"></a>
								<?
								}
								?></td>
								<td width="3%" valign="middle">&nbsp;</td>
						  </tr>
	</table>						
		<?
						
						$this->m_contItems++;
						
						$numant = $numeral;
						$numeral .=". ".$contSubTemas;			
						$cont = $cont + 2;
						$contSubTemas++;
						$this->consulta($idSubTema, $tituloSubTema, $cont, $numeral,$archivo,$archivo_contenido);
						//fwrite($archivo, "\n");
						fwrite($archivo_contenido, "\n");
						$cont = $cont - 2;
						$numeral = $numant;	
					}//FIN WHILE LISTconsultaSubTema
				}//FIN IF consultaSubTema
			}//FUNCION CONSULTA	
				
					
		function IndiceEdicion()
		{//FUNCION IndiceEdicion
			$this->JavascriptIndiceEdicion();			
			?>
		    <a name="arriba"></a>
				<table width="100%" bgcolor="#F3F3F3" class="tablaPrincipal"><!-- 1 -->
				<form name="frm_indiceEdicion" method="post">	
					<td valign="top">
					<tr class="tablaGeneral">
					  <td>
					  
					  <table width="100%" class="tablaGeneral">
                        
                        <tr class="trListaClaro">
                          <td class="trSubTitulo" width="33%" align="center" valign="middle">Codigo:<?= $this->m_codigo?></td>
                          <td class="trSubTitulo" width="33%" align="center" valign="middle">Creditos:<?= $this->m_nCreditos?></td>
                          <td class="trSubTitulo" width="33%" align="center" valign="middle">Modificado:<?= $this->m_fechaModificacion?></td>
                        </tr>
                      </table></td>
				  </tr>
					
					
					<tr>
						<td><?= $this->BarraHerramientas()?></td>						
				  </tr>
					
			<?
			if(!empty($this->m_introduccion))
			{
				$this->m_contItems++;
			?>
					<tr>
						<td>
							<table width="100%" border="0" onClick="javascript: clicItemChk(this,'<?= $this->m_colorTablaClic?>','<?= $this->m_colorTablaFuera?>','chk_introduccion')" onMouseOver="javascript: dentroItemChk(this,'<?= $this->m_colorTablaDentro?>','chk_introduccion')" onMouseOut="javascript: fueraItemChk(this,'<?= $this->m_colorTablaFuera?>','chk_introduccion')"  class="tablaGeneral">
								<tr class="trInformacion">
								  <td width="3%"><input type="checkbox" name="chk_introduccion" id="chk_introduccion" value="1"></td>
									<td width="2%"><img src="imagenes/vineta.gif" width="15" height="17" border="0"></td>
									<td width="85%"><a href="mostrarContenido.php?idmateria=<?=$this->m_idmateria?>&Introduccion=1" target="_self" class="link"><strong>Introduccion</strong></a></td>
									
									<? if($this->m_estadoIntroduccion==0)
											$estado="&nbsp;";
										else if($this->m_estadoIntroduccion==1)
											$estado="Inactivo";										
									?>		
								    <td width="6%"><?= $estado?></td>
								    <td width="2%"><img src="../contenido/imagenes/espacio15x17.gif" width="15" height="17"></td>
								    <td width="2%"><img src="../contenido/imagenes/espacio15x17.gif" width="15" height="17"></td>
							  </tr>
						  </table>						</td>
					</tr>
			<? 
			}
			$sql="SELECT MIN( mei_tema.orden ) , MAX( mei_tema.orden ) FROM mei_tema WHERE idmateria =".$this->m_idmateria." AND mei_tema.tipo = 1";
			$consultaOrdenTema=$this->m_baseDatos->ConsultarBD($sql);
			list($minOrdenTema,$maxOrdenTema)=mysql_fetch_array($consultaOrdenTema);
			
			$sql="SELECT mei_tema.idtema, mei_tema.titulo, mei_tema.estado, mei_tema.orden, mei_tema.contenido FROM mei_tema
			WHERE mei_tema.idmateria =".$this->m_idmateria." AND mei_tema.tipo = 1 ORDER BY(mei_tema.orden)";
			$consulta=$this->m_baseDatos->ConsultarBD($sql);			
			
			//***************************Indice********************************
			if ( file_exists('../../datosMEIWEB/archivosContenido/indice.doc') ) 
				unlink('../../datosMEIWEB/archivosContenido/indice.doc');
			
			$archivo = fopen('../../datosMEIWEB/archivosContenido/indice.doc', 'a');	
			fwrite($archivo, $this->m_codigo." ".$this->m_nombreMateria."\n");
			fwrite($archivo, "\n\n");
			//*********************Contenido*************************************
			if ( file_exists('../../datosMEIWEB/archivosContenido/contenido.doc') ) 
				unlink('../../datosMEIWEB/archivosContenido/contenido.doc');
			
			$archivo_contenido = fopen('../../datosMEIWEB/archivosContenido/contenido.doc', 'a');	
			//**********************************************************************			
			
			if ( mysql_num_rows($consulta) )
			{//IF mysql_num_rows($consulta)
?>
			<tr>
				<td>
				  <table width="100%" border="1" class="tablaGeneral">
                    <tr>
                      <td>
<?			
				$contTemas=1;
				while(list($idTema,$temaTitulo,$estadoTema,$ordenTema,$contenido)=mysql_fetch_array($consulta))
				{//WHILE PARA LOS TEMAS
?>					
					<table width="100%" onClick="javascript: clicItemChk(this,'<?= $this->m_colorTablaClic?>','<?= $this->m_colorTablaFuera?>','chk_tema[<?= $idTema?>]')" onMouseOver="javascript: dentroItemChk(this,'<?= $this->m_colorTablaDentro?>','chk_tema[<?= $idTema?>]')" onMouseOut="javascript: fueraItemChk(this,'<?= $this->m_colorTablaFuera?>','chk_tema[<?= $idTema?>]')" class="tablaPrincipal">
						<tr>
						  <td width="3%"><input type="checkbox" name="chk_tema[<?= $idTema?>]" id="chk_tema<?= $idTema?>" value="<?= $idTema?>"></td>
							<td width="2%"><img src="imagenes/vineta.gif" width="15" height="17" border="0"></td>
							<td width="86%">
									<a href="<?= $this->m_destinoMostrar?>&idTema=<?= $idTema?>&num=<?=$contTemas."."?>" target="_self" class="link"><strong><?= $contTemas.". ".$temaTitulo?></strong></a></td>
									<? 
										fwrite($archivo, $contTemas.". ".$temaTitulo."\n");
										fwrite($archivo, "\n");
										
										fwrite($archivo_contenido, $contTemas.". ".$temaTitulo."\n");
										fwrite($archivo_contenido, "\n");
										fwrite($archivo_contenido, strip_tags($contenido));
										fwrite($archivo_contenido, "\n\n");
									if($estadoTema==0)
									{
									?>
									<td width="9%">&nbsp;</td>
									<?
									}
									else if($estadoTema==1)
									{
									?>
									<td width="9%" align="right" valign="middle"><img src='imagenes/inactivo.gif' alt='Inactivo' width='16' height='16' border='0' /></td>
									<?
									}
									?>	
									<td width="2%" valign="top"><a href="javascript:enviarAgregar('<?= $this->m_destinoAgregar?>',2,<?= $idTema?>)"><img src="../contenido/imagenes/nuevoSubTema.gif" alt="Agregar Subtema" width="16" height="17" border="0"></a></td>
									<td width="2%" valign="middle"><a href="<?= $this->m_destinoMostrar?>&idTema=<?= $idTema?>" target="_self"></a>
									
									<?
									
									if($ordenTema==$minOrdenTema)
									{
									?>
										<img src="../contenido/imagenes/arribaTemaClaro.gif" width="15" height="17" border="0">
									<?
									}
									else
									{
									?>
										<a href="<?= $this->m_destinoMover?>&arriba=1&idTema=<?= $idTema?>&orden=<?= $ordenTema?>" target="_self"><img src="../contenido/imagenes/arribaTema.gif" alt="Mover arriba" width="15" height="17" border="0"></a>
									<?
									}
									?>									</td>
									<td width="2%" valign="middle">
									<?
									if($ordenTema>=$maxOrdenTema)
									{
									?>
										<img src="../contenido/imagenes/abajoTemaClaro.gif" width="15" height="17" border="0">
									<?
									}
									else
									{
									?>
										<a href="<?= $this->m_destinoMover?>&abajo=1&idTema=<?= $idTema?>&orden=<?= $ordenTema?>" target="_self"><img src="../contenido/imagenes/abajoTema.gif" alt="Mover abajo" width="15" height="17" border="0"></a>
									<?
									}
									?>									</td>
					  </tr>
					    </table>						
<?				
					//$this->consulta($idTema,$temaTitulo,0,$ordenTema);
					$this->consulta($idTema,$temaTitulo,0,$contTemas,$archivo,$archivo_contenido);
					fwrite($archivo, "\n");
					fwrite($archivo_contenido, "\n\n");
					$contTemas++;
					$this->m_contItems++;
				}//FIN WHILE PARA LOS TEMAS				
				
				fclose($archivo);
				fclose($archivo_contenido);
?>				  </td>
                    </tr>
                  </table>				</td>
			</tr>
			<? 
			}//IF IF mysql_num_rows($consulta)
			else 
			{//ELSE 1
			?>
			<tr align="center">
				<td>					
					<table width="90%" class="tablaGeneral" border="0" align="center">
		          		<tr class="tablaTitulo">
				            <td align="center" valign="middle">No se han definido contenidos para esta Materia</td>
				        </tr>
		        	</table></td>
			</tr>
			<?
			}//FIN ELSE 1
			$sql="SELECT mei_bibliografia.idbibliografia, mei_bibliografia.estado FROM mei_bibliografia
						WHERE mei_bibliografia.idmateria =".$this->m_idmateria;
			
			$consulta=$this->m_baseDatos->ConsultarBD($sql);
			
			list($idBibligrafia,$estadoBibliografia)=mysql_fetch_array($consulta);
			
			if(!empty($idBibligrafia))
			{
				$this->m_contItems++;
			?>
					<tr>
						<td>
							<table class="tablaGeneral" width="100%" onClick="javascript: clicItemChk(this,'<?= $this->m_colorTablaClic?>','<?= $this->m_colorTablaFuera?>','chk_bibliografia')" onMouseOver="javascript: dentroItemChk(this,'<?= $this->m_colorTablaDentro?>','chk_bibliografia')" onMouseOut="javascript: fueraItemChk(this,'<?= $this->m_colorTablaFuera?>','chk_bibliografia')">
								<tr class="trInformacion">
								  <td width="3%"><input type="checkbox" name="chk_bibliografia" id="chk_bibliografia" value="1"></td>
									<td width="2%"><img src="imagenes/vineta.gif" width="15" height="17" border="0"></td>
									<td width="86%"><a href="mostrarContenido.php?idmateria=<?=$this->m_idmateria?>&Bibliografia=1" target="_self" class="link"><strong>Bibliografia</strong></a> </td>
									
								    <td width="5%">&nbsp;</td>
								    <td width="2%"><img src="../contenido/imagenes/espacio15x17.gif" width="15" height="17"></td>
								    <td width="2%"><img src="../contenido/imagenes/espacio15x17.gif" width="15" height="17"></td>
								</tr>
						  </table>						</td>
					</tr>
			<? 
				if($this->m_contItems > $this->m_limiteItems)
				{
				?>	
					<tr>
						<td colspan="3"><?= $this->BarraHerramientas()?></td>
					</tr>
				<?
				}
			}
			?>
			</form>    
	    </table>
	<?
			$this->BarraRecuperacionContenido();
			//}
		}//FIN FUNCION IndiceEdicion
	
///////////////////////////////////////////////////////////////////////////////////////////-->

function consulta2($idTema, $titulo, $cont, $numeral)
{//FUNCION CONSULTA
	$sql="SELECT MIN( mei_tema.orden ) , MAX( mei_tema.orden ) FROM mei_tema WHERE mei_tema.idtemapadre =".$idTema;
	$consultaOrdenSubtema=$this->m_baseDatos->ConsultarBD($sql);
	list($minOrdenSubTema,$maxOrdenSubTema)=mysql_fetch_array($consultaOrdenSubtema);				
	
	$sql="SELECT mei_tema.idtema , mei_tema.titulo, mei_tema.estado, mei_tema.orden FROM mei_tema
		  WHERE mei_tema.idtemapadre =".$idTema." AND mei_tema.estado = 0 ORDER BY (mei_tema.orden)";

	$consultaSubTema=$this->m_baseDatos->ConsultarBD($sql);					
	$contSubTemas=1;
	if ( !empty($consultaSubTema) )
	{//IF consultaSubTema
		while(list($idSubTema, $tituloSubTema,$estadoSubTema,$ordenSubTema)=mysql_fetch_array($consultaSubTema))
		{//WHILE LISTconsultaSubTema
?>				
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
				<tr class="trListaOscuro">
				  <td width="<?=1+$cont?>%">&nbsp;</td>
					<td width="<?=3+$cont?>%">&nbsp;				  </td>
				  <td width="<?=80-(2*$cont)?>%" align="left"><a href="<?= $this->m_destinoMostrar?>&idSubTema=<?= $idSubTema?>&num=<?=$numeral.". ".$contSubTemas.". "?>" target="_self" class="link">
				  <?= $numeral.". ".$contSubTemas.". ".$tituloSubTema?></a></td>
					<? if($estadoSubTema==0 && $estadoTema==0)
							$estado="&nbsp;";
						else if($estadoSubTema==1)
							$estado="Inactivo";										
					?>	
			  </tr>
</table>						
<?
			
			$this->m_contItems++;
			
			$numant = $numeral;
			$numeral .=". ".$contSubTemas;			
			$cont = $cont + 2;
			$contSubTemas++;
			$this->consulta2($idSubTema, $tituloSubTema, $cont, $numeral);			
			$cont = $cont - 2;
			$numeral = $numant;	
		}//FIN WHILE LISTconsultaSubTema
	}//FIN IF consultaSubTema
}//FUNCION CONSULTA	
	function IndiceEdicionEstudiante()
	{//FUNCION IndiceEdicionEstudiante
			$this->JavascriptIndiceEdicion();			
			?>
		    <a name="arriba"></a>
				<table width="100%" bgcolor="#F3F3F3" class="tablaPrincipal"><!-- 1 -->
				<form name="frm_indiceEdicion" method="post">	
					<td valign="top">
					<tr class="tablaGeneral">
					  <td>
					  
					  <table width="100%" class="tablaGeneral">
                        
                        <tr class="trListaClaro">
                          <td class="trSubTitulo" width="33%" align="center" valign="middle">Codigo:<?= $this->m_codigo?></td>
                          <td class="trSubTitulo" width="33%" align="center" valign="middle">Creditos:<?= $this->m_nCreditos?></td>
                          <td class="trSubTitulo" width="33%" align="center" valign="middle">Modificado:<?= $this->m_fechaModificacion?></td>
                        </tr>
                      </table></td>
				  </tr>
					
					
			<?
			if(!empty($this->m_introduccion))
			{
				$this->m_contItems++;
			?>
					<tr>
						<td>
							<table width="100%" border="0" class="tablaGeneral">
								<tr class="trInformacion">
								  <td width="2%"><img src="imagenes/vineta.gif" width="15" height="17" border="0"></td>
									<td width="85%"><a href="mostrarContenido.php?idmateria=<?=$this->m_idmateria?>&Introduccion=1" target="_self" class="link"><strong>Introduccion</strong></a></td>
									
									<? if($this->m_estadoIntroduccion==0)
											$estado="&nbsp;";
										else if($this->m_estadoIntroduccion==1)
											$estado="Inactivo";										
									?>		
						      </tr>
						  </table>						</td>
					</tr>
			<? 
			}
			$sql="SELECT MIN( mei_tema.orden ) , MAX( mei_tema.orden ) FROM mei_tema WHERE idmateria =".$this->m_idmateria." AND mei_tema.tipo = 1";
			$consultaOrdenTema=$this->m_baseDatos->ConsultarBD($sql);
			list($minOrdenTema,$maxOrdenTema)=mysql_fetch_array($consultaOrdenTema);
			
			$sql="SELECT mei_tema.idtema, mei_tema.titulo, mei_tema.estado, mei_tema.orden  FROM mei_tema
			WHERE mei_tema.idmateria =".$this->m_idmateria." AND mei_tema.estado = 0 AND mei_tema.tipo = 1 ORDER BY(mei_tema.orden)";
			$consulta=$this->m_baseDatos->ConsultarBD($sql);			
			
			if ( mysql_num_rows($consulta) )
			{//IF mysql_num_rows($consulta)
?>
			<tr>
				<td>
				  <table width="100%" border="1" class="tablaGeneral">
                    <tr>
                      <td>
<?			
				$contTemas=1;
				while(list($idTema,$temaTitulo,$estadoTema,$ordenTema)=mysql_fetch_array($consulta))
				{//WHILE PARA LOS TEMAS
?>					
					<table class="tablaPrincipal">
						<tr class="trInformacion">
						  <td width="2%"><img src="imagenes/vineta.gif" width="15" height="17" border="0"></td>
							<td width="86%">
									<a href="<?= $this->m_destinoMostrar?>&idTema=<?= $idTema?>&num=<?=$contTemas."."?>" target="_self" class="link"><strong><?= $contTemas.". ".$temaTitulo?></strong></a></td>
					  </tr>
					</table>						
<?				
					//$this->consulta($idTema,$temaTitulo,0,$ordenTema);
					$this->consulta2($idTema,$temaTitulo,0,$contTemas);
					$contTemas++;
					$this->m_contItems++;
				}//FIN WHILE PARA LOS TEMAS				
?>				  </td>
                    </tr>
                  </table>				</td>
			</tr>
			<? 
			}//IF IF mysql_num_rows($consulta)
			else 
			{//ELSE 1
			?>
			<tr align="center">
				<td>					
					<table width="90%" class="tablaGeneral" border="0" align="center">
		          		<tr class="tablaTitulo">
				            <td align="center" valign="middle">No se han definido contenidos para esta Materia</td>
				        </tr>
		        	</table></td>
			</tr>
			<?
			}//FIN ELSE 1
			$sql="SELECT mei_bibliografia.idbibliografia, mei_bibliografia.estado FROM mei_bibliografia
						WHERE mei_bibliografia.idmateria =".$this->m_idmateria;
			
			$consulta=$this->m_baseDatos->ConsultarBD($sql);
			
			list($idBibligrafia,$estadoBibliografia)=mysql_fetch_array($consulta);
			
			if(!empty($idBibligrafia))
			{
				$this->m_contItems++;
			?>
					<tr>
						<td>
							<table class="tablaGeneral" width="100%" >
								<tr class="trInformacion">
								  <td width="2%"><img src="imagenes/vineta.gif" width="15" height="17" border="0"></td>
									<td width="86%"><a href="mostrarContenido.php?idmateria=<?=$this->m_idmateria?>&Bibliografia=1" target="_self" class="link"><strong>Bibliografia</strong></a> </td>
									
									<? if($estadoBibliografia==0)
											$estado="&nbsp;";
										else if($estadoBibliografia==1)
											$estado="Inactivo";										
									?>		
							    </tr>
						  </table>						</td>
					</tr>
				<?
			}
			?>
			</form>    
	    </table>			
				<a name="abajo">&nbsp;</a>
                <?
			$this->BarraRecuperacionContenido();
		}//FIN FUNCION IndiceEdicionEstudiante
	}//FIN clase 1

?>