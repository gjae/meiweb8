<?
include_once("../baseDatos/BD.class.php");
include_once("../librerias/estandar.lib.php");

$baseDatos= new BD();
unset($accion);

if(!empty($_GET['idmateria']))
{
	if(!empty($_GET['activar']))
	{
		$accion=0;
	}
	else if(!empty($_GET['desactivar']))
	{
		$accion=1;
	}
	
	if(isset($accion))
	{
	
		if(!empty($_POST['chk_tema']))
		{
			foreach($_POST['chk_tema'] as $idTema)
			{
				$sql="UPDATE mei_tema SET mei_tema.estado = '".$accion."' WHERE mei_tema.idtema=".$idTema." AND mei_tema.idmateria =".$_GET['idmateria'];	
				$baseDatos->ConsultarBD($sql);
			}
		
		}

		if(!empty($_POST['chk_subTema']))
		{
			foreach($_POST['chk_subTema'] as $idSubTema)
			{
				$sql="UPDATE mei_tema SET mei_tema.estado = '".$accion."' WHERE mei_tema.idtema=".$idSubTema;	
				$baseDatos->ConsultarBD($sql);
			}
		
		}

		if(!empty($_POST['chk_introduccion']))
		{
			$sql="UPDATE mei_materia SET mei_materia.estadointroduccion = '".$accion."' WHERE mei_materia.idmateria =".$_GET['idmateria'];
			$baseDatos->ConsultarBD($sql);
		}
			
		if(!empty($_POST['chk_bibliografia']))
		{
			$sql="UPDATE mei_bibliografia SET mei_bibliografia.estado = '".$accion."' WHERE mei_bibliografia.idmateria=".$_GET['idmateria'];
			$baseDatos->ConsultarBD($sql);
		}
	}	
	
	
}
redireccionar("mostrarIndiceEdicion.php?idmateria=".$_GET['idmateria']);
?>
