<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');
	
	if(comprobarSession())
	{	
	$baseDatos= new BD();	
	$editorContenido=new FCKeditor('edt_contenido' , '100%' , '350' , 'barraEstandar' , '' );
	
	if ($_GET["Introduccion"]==1) $word = "Introduccion";
	elseif ($_GET["Bibliografia"]==1) $word = "Bibliografia";
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
function guardarI(idmateria)
{
	document.frm_IB.action = "insertarContenido.php?idmateria="+idmateria+"&Introduccion=1";
	document.frm_IB.submit();
}
function guardarB(idmateria)
{
	document.frm_IB.action = "insertarContenido.php?idmateria="+idmateria+"&Bibliografia=1&mod=E";
	document.frm_IB.submit();
}
function eliminarI(idmateria)
{
	document.frm_IB.action = "insertarContenido.php?idmateria="+idmateria+"&Introduccion=1&mod=E";
	document.frm_IB.submit();
}

function eliminarB(idmateria)
{
	document.frm_IB.action = "insertarContenido.php?idmateria="+idmateria+"&Bibliografia=1&mod=D";
	document.frm_IB.submit();
}

</script>
</head>
<body>
	 <table class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
            <td>
            <table class="tablaGeneral" >
              <tr class="trSubTitulo">
             <?
			 $sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
						$resultado = $baseDatos->ConsultarBD($sql);
		list($materia) = mysql_fetch_row($resultado); ?>
		 <td>
         <a href="../scripts/" class="link">Inicio</a>
         <a> -> </a> 
         <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link">
		 <?=ucwords(strtolower($materia))?> </a>
         <a> -> </a> 
        <a href="../contenido/mostrarIndiceEdicion.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"> Contenidos</a> -> Ingresar <?=$word?> </a></td>
          
              </tr>
            </table>
			
		<form name="frm_IB" action="" method="post">			
			<?
				if ($_GET["Introduccion"] == 1)
				{//INTRODUCCION
					$sql = "SELECT mei_materia.introduccion FROM mei_materia WHERE mei_materia.idmateria = '".$_GET["idmateria"]."'";					
					$resultado = $baseDatos->ConsultarBD($sql);
					list($intro) = mysql_fetch_row($resultado);
			?>
		      <table width="210" class="tablaGeneral">
                <tr class="trTitulo">
                  <td colspan="5"><?=$word?></td>
                </tr>
                <tr align="left" valign="middle" class="trInformacion">
                  <td width="3%"><a href="ijavascript:guardarI(<?=$_GET["idmateria"]?>)" class="link"><img src="../contenido/imagenes/guardarContenido.gif" alt="Guardar y terminar" width="21" height="20" border="0"></a></td>
                  <td width="7%"><a href="javascript:guardarI(<?=$_GET["idmateria"]?>)" class="link">Guardar</a></td>
                  <td width="3%"><a href="javascript:eliminarI(<?=$_GET["idmateria"]?>)" class="link"><img src="../contenido/imagenes/rechazarContenido.gif" alt="Rechazar cambios" width="21" height="20" border="0"></a></td>
                  <td width="7%"><a href="javascript:eliminarI(<?=$_GET["idmateria"]?>)" class="link">Rechazar</a></td>
                  <td width="80%">&nbsp;</td>
                </tr>
                <tr class="trInformacion">
                  <td colspan="5"><? 
									$editorContenido->Value = stripslashes(base64_decode($intro));
				  					$editorContenido->crearEditor();								
								?></td>
                </tr>
              </table>
			  <?
			  	}//FIN INTRODUCCION
				elseif($_GET["Bibliografia"] == 1)
				{//BIBLIOGRAFIA
					$sql = "SELECT mei_bibliografia.titulo, mei_bibliografia.descripcion, mei_bibliografia.url, mei_bibliografia.estado 
							FROM mei_bibliografia WHERE mei_bibliografia.idbibliografia = '".$_GET["idbibliografia"]."'";
					$resultado = $baseDatos->ConsultarBD($sql);
					list($titulo, $descripcion, $url, $estado) = mysql_fetch_row($resultado);
					
			  ?>
			  <table width="210" class="tablaGeneral">
                <tr class="trTitulo">
                  <td colspan="10"><?=$word?></td>
                </tr>
                <tr align="left" valign="middle" class="trSubTitulo">
                  <td width="14%">Titulo:</td>
                  <td width="70%"><input name="txt_titulo" type="text" size="80" value="<?=$titulo?>"></td>
                  <td width="3%" ><input name="rad_activo" type="radio" value="0" <? if($estado==0) print "checked";?>></td>
                  <td width="5%" >Inactivo</td>
                  <td width="3%" ><input name="rad_activo" type="radio" value="1" <? if($estado==1) print "checked";?>></td>
                  <td width="5%" >Activo
                  <input name="hid_idbibliografia" type="hidden" id="hid_idbibliografia" value="<?=$_GET["idbibliografia"]?>"></td>
                </tr>
                <tr align="left" valign="middle" class="trSubTitulo">
                  <td class="trSubTitulo">Descripcion:</td>
                  <td colspan="5" class="trSubTitulo"><? 
				  										$editorContenido->Value = stripslashes(base64_decode($descripcion));
				  										$editorContenido->crearEditor();
													?></td>
                </tr>
                <tr align="left" valign="middle" class="trSubTitulo">
                  <td class="trSubTitulo">URL:</td>
                  <td colspan="5" class="trSubTitulo"><input name="txt_url" type="text" id="txt_url" size="100" value="<?=$url?>"></td>
                </tr>
				<tr class="trInformacion">
					<td colspan="6" align="center" valign="middle" class="trInformacion">
						<table width="159" height="27" border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
							<tr align="left" valign="middle" class="trInformacion">
							  <td width="5%"><a href="javascript:guardarB(<?=$_GET["idmateria"]?>)" class="link"><img src="../contenido/imagenes/guardarContenido.gif" alt="Guardar y terminar" width="21" height="20" border="0"></a></td>
							  <td width="45%"><a href="javascript:guardarB(<?=$_GET["idmateria"]?>)" class="link">Guardar</a></td>
							  <td width="5%"><a href="javascript:eliminarB(<?=$_GET["idmateria"]?>)" class="link"><img src="../contenido/imagenes/rechazarContenido.gif" alt="Rechazar cambios" width="21" height="20" border="0"></a></td>
							  <td width="45%"><a href="javascript:eliminarB(<?=$_GET["idmateria"]?>)" class="link">Eliminar</a></td>
						    </tr>
					</table>					</td>
				</tr>
				
                <tr class="trInformacion">
                  <td class="trInformacion"colspan="6">&nbsp;</td>
                </tr>
              </table>
			  <?
			  	}//FIN BIBLIOGRAFIA
			  ?>
		</form>	
				
			

			
			<table class="tablaGeneral" width="100%" border="0">
              <tr class="trInformacion"align="center" valign="middle">
                <td class="trListaOscuro"><a href="mostrarContenido.php?idmateria=<?=$_GET["idmateria"]?>&Bibliografia=1" class="link">Volver</a></td>
              </tr>
            </table></td>
                 <td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>