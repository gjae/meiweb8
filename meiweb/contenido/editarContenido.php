<?
	include_once("../baseDatos/BD.class.php");
	include_once("../librerias/estandar.lib.php");
	include_once("../editor/fckeditor.php");
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');

	$baseDatos= new BD();
if(comprobarSession())
{//IF COMPROBARSESSION
?>					
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">

<title>MEIWEB</title>
<script language="javascript">
				
function activarRadio(id)
{
	var radio=document.getElementById(id);
	radio.checked=true;
}

function enviarTema(destino)
{
	document.frm_agregarContenido.action=destino;
	document.frm_agregarContenido.submit();
}
					
function autoGuardar(destino,tiempo)
{								
	if(confirm("Guardado automatico de contenido, ¿Desea guardar los cambios en el documento ahora?"))
	{
		enviarTema(destino);
	}
	else
	{
		setTimeout("autoGuardar('"+destino+"',"+tiempo+")",tiempo);
	}
}
</script>
<?
	$editorContenido=new FCKeditor('edt_contenido' , '100%' , '350' , 'barraEstandar' , '' );

?>
</head>
<body>
 <table class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
       
        	<td>
	 
	    <table class="tablaGeneral" >
           <tr class="trSubTitulo">
                    <?
						if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==7)    
						{$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
						}
						else
						{
							$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
						}
		$resultado = $baseDatos->ConsultarBD($sql);
		list($materia) = mysql_fetch_row($resultado); ?>
		 <td>
         <a href="../scripts/" class="link">Inicio</a>
         <a> -> </a> 
         <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link">
		 <?=ucwords(strtolower($materia))?> </a>
         <a> -> </a> 
        <a href="../contenido/mostrarIndiceEdicion.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>" class="link"> Contenidos</a>  :  Editar Contenidos </a></td>
					</tr>
		</table>
<!--********************************************************************************************-->
<form name="frm_agregarContenido" method="post">
<?
	if(!empty($_GET['idmateria']))
	{
		$destinoGuardar="insertarContenido.php?idmateria=".$_GET['idmateria'];
		$destinoRechazar="rechazarContenido.php?idmateria=".$_GET['idmateria'];
		$destinoTemp="insertarContenidoTemp.php?idmateria=".$_GET['idmateria'];
		
		$tiempo=5;//minutos
		$tiempoAutoGuardar=60000*$tiempo;
		
		$sql="SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$consulta=$baseDatos->ConsultarBD($sql);		
		list($nombreMateria)=mysql_fetch_array($consulta);

		print "<script language='javascript'> setTimeout(\"autoGuardar('".$destinoTemp."',".$tiempoAutoGuardar.")\",".$tiempoAutoGuardar."); </script>";
		
		$radioActivo="checked";
		
		if(!empty($_GET['tipotema']))
		{			
			if(!empty($_GET['idTemaTemp']))
			{
			$sql="SELECT mei_tematemp.titulo, mei_tematemp.contenido, mei_tematemp.estado FROM mei_tematemp WHERE mei_tematemp.idtematemp=".$_GET['idTemaTemp'];
			$consulta=$baseDatos->ConsultarBD($sql);
			list($tituloItem, $contenidoItem, $estadoItem)=mysql_fetch_array($consulta);
			
			$editorContenido->Value=stripslashes(base64_decode($contenidoItem));
			
				if($estadoItem == 0)
				{
					$radioActivo="checked";
					unset($radioInactivo);
				}
				else if($estadoItem ==1)
				{
					$radioInactivo="checked";
					unset($radioActivo);
				}
			}
			
			switch ($_GET["tipotema"])
			{
				case 1://Tema
					$sql = "SELECT mei_tema.titulo, mei_tema.orden FROM mei_tema
							WHERE mei_tema.tipo = ".$_GET['tipotema']." AND 
							mei_tema.idmateria=".$_GET['idmateria']." ORDER BY(mei_tema.orden)";
				break;
				case 2;//Subtema
					$sql = "SELECT mei_tema.titulo, mei_tema.orden FROM mei_tema 
							WHERE mei_tema.tipo = ".$_GET['tipotema']." AND 
							mei_tema.idmateria=".$_GET['idmateria']." AND 
							mei_tema.idtemapadre = '".$_GET["idTemaPadre"]."' 
							ORDER BY(mei_tema.orden)";
				break;
			}			
		}
?>
		<input name="hid_idTemaTemp" type="hidden" value="<?= $_GET['idTemaTemp']?>">						
		<input name="hid_tipotema" type="hidden" value="<?= $_GET['tipotema']?>">
		<input name="hid_idTemaPadre" type="hidden" value="<?= $_GET['idTemaPadre']?>">		                
<?		
		/*print "idTemaTemp : ".$_GET['idTemaTemp']."<p>";
		print "tipotema : ".$_GET['tipotema']."<p>";
		print "idTemaPadre : ".$_GET['idTemaPadre']."<p>";
		print $sql."<p>";*/
		$consultaCombo=$baseDatos->ConsultarBD($sql);
		$contLista=0;
		while(list($tituloItemlista,$ordenItemlista)=mysql_fetch_array($consultaCombo))
		{
			$listaItems[$contLista]=$tituloItemlista.'[$$$]'.$ordenItemlista;
			$contLista++;
		}
		
		if($contLista >0 )
		{
			$radUbicacionDisable="";
		}
		else
		{
			$radUbicacionDisable="disabled";
		}
		?>
		              <table width="60%" border="0" class="tablaPrincipal">
							<tr class="trSubTitulo">
								<td><?= $nombreMateria?></td>
							</tr>
							<tr>
							  <td><? 
							  		/*$sql = "SELECT mei_tema.titulo FROM mei_tema WHERE 
											mei_tema.idtema=".$_GET["idTema"];
									$resultado = $baseDatos->ConsultarBD($sql);
									list ($nom) = mysql_fetch_row($resultado);
									print "Tema : ".$nom;*/
							  		?>
							  </td>
						  </tr>
							<tr>
								<td>
									<table width="100%" class="tablaGeneral">
										<tr class="trInformacion">
											<td width="4%">Titulo:</td>
											<td width="65%"><input name="txt_titulo" type="text" size="100" value="<?= $tituloItem?>" ></td>
											<td width="6%">&nbsp;</td>
											<td width="2%"><input name="rad_activo" type="radio" value="0" <?= $radioActivo?>></td>
											<td width="6%">Activo</td>
											<td width="2%"><input name="rad_activo" type="radio" value="1" <?= $radioInactivo?>></td>
											<td width="15%">Inactivo</td>
										</tr>
									</table>								</td>
							</tr>
							<tr>
								<td>
									<table width="100%" class="tablaGeneral" bordercolor="#F5F5F5" bgcolor="#F5F5F5">
										<tr bordercolor="#F5F5F5">
											<td width="1%" valign="middle" bordercolor="#F5F5F5"><img src="imagenes/separador.gif" width="4" height="21"></td>
											<td width="3%" valign="middle" bordercolor="#D6D6D6" background="../contenido/imagenes/fondoBoton.gif"><a href="javascript: enviarTema('<?= $destinoGuardar?>');" target="_self"><img src="../contenido/imagenes/guardarContenido.gif" alt="Guardar y terminar" width="21" height="20" border="0"></a></td>
											<td width="8%" valign="middle" bordercolor="#F5F5F5"><a href="javascript: enviarTema('<?= $destinoGuardar?>');" target="_self" class="link">Terminar</a></td>
											<td width="3%" valign="middle" bordercolor="#D6D6D6" background="imagenes/fondoBoton.gif"><a href="javascript: enviarTema('<?= $destinoTemp?>');" target="_self" class="link"><img src="imagenes/guardarTemp.gif" alt="Guardar y continuar" width="21" height="20" border="0"></a></td>
											<td width="7%" valign="middle" bordercolor="#F5F5F5"><a href="javascript: enviarTema('<?= $destinoTemp?>');" target="_self" class="link">Guardar</a></td>
											<td width="3%" valign="middle" bordercolor="#D6D6D6" background="imagenes/fondoBoton.gif"><a href="javascript: enviarTema('<?= $destinoRechazar?>');" target="_self"><img src="../contenido/imagenes/rechazarContenido.gif" alt="Rechazar cambios" width="21" height="20" border="0"></a></td>
											<td width="75%" valign="middle" bordercolor="#F5F5F5"><a href="javascript: enviarTema('<?= $destinoRechazar?>');" target="_self" class="link">Rechazar</a></td>
										</tr>
									</table>								</td>
							</tr>
							<tr>
								<td bordercolor="#FFFFFF"><? $editorContenido->crearEditor();?></td>
							</tr>
							<tr>
								<td>
									<table width="100%" class="tablaGeneral">
										<tr align="center" valign="middle" bordercolor="#F5F5F5" bgcolor="#F5F5F5">
											<td width="19%">Ubicar</td>
										  <td width="3%"><input name="rad_ubicacion" type="radio" value="0" id="fin" checked <?= $radUbicacionDisable?>></td>
											<td width="15%" align="left">Al final  </td>
										  <td width="3%" align="left"><input name="rad_ubicacion" type="radio" value="1" id="inicio" <?= $radUbicacionDisable?>></td>
											<td width="18%" align="left">Al inicio </td>
										  <td width="3%" align="left"><input name="rad_ubicacion" type="radio" value="2" id="luego" <?= $radUbicacionDisable?>></td>
											<td width="12%" align="left">Luego de </td>
											<td width="27%">
												<select name="cbo_ubicacion" id="cbo_ubicacion" onFocus="javascript:activarRadio('luego')" <?= $radUbicacionDisable?>>
		<?
		if($contLista > 0)
		{
			foreach($listaItems as $item)
			{
				list($tituloItemlista,$ordenItemlista)=explode('[$$$]',$item);
		?>
													<option value="<?= $ordenItemlista?>"><?= $tituloItemlista?></option>
		<?
			}
		}
		else
		{
		?>
													<option value="-1">Sin elementos</option>
		<?
		}
		?>
										  </select>											</td>
									  </tr>
								  </table>								</td>
							</tr>
	    </table>
<?
}//redireccionar("mostrarIndiceEdicion.php?idmateria=".$_GET['idmateria']);
?>
	   </form>
	
<!--***********************************************************************************************-->


		
	 </td>
        <td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
  </tr>
</table>
				</body>
			</html>
	<?
}
?>