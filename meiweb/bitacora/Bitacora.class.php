<?

	class Bitacora
	{
	
	
		function Bitacora()
		{
		
		}
		
		function CadenaFechaDMA($a_fecha)
		{
			list($agno,$mes,$dia)=explode("-",$a_fecha);
			$tUnix=mktime (0,0,0,$mes,$dia,$agno);
						
			list($strDia,$strMes,$strAgno)=explode("-",date("d-m-Y",$tUnix));

			return ucwords($strDia)." de ".ucwords($this->StrMes($strMes))." de ".$strAgno;
		}
		
		function CadenaFechaDsDMA($a_fecha)
		{
			list($agno,$mes,$dia)=explode("-",$a_fecha);
			$tUnix=mktime (0,0,0,$mes,$dia,$agno);
						
			list($strDiaSemana,$strDia,$strMes,$strAgno)=explode("-",date("w-d-m-Y",$tUnix));
			
			return ucwords($this->StrDiaSemana($strDiaSemana))." ".$strDia." de ".ucwords($this->StrMes($strMes))." de ".$strAgno;
		}
		
		function TiempoSegundosUnix($a_fecha,$a_hora)
		{
			list($hora,$minuto,$segundo)=explode(":",$a_hora);
			list($agno,$mes,$dia)=explode("-",$a_fecha);
		
			return mktime ($hora,$minuto,$segundo,$mes,$dia,$agno);
		}
		
		function TiempoTotalSistema($a_arrayFechaTiempo)
		{
			$indice=0;
			
			for($i=0;$i<sizeof($a_arrayFechaTiempo);$i++)
			{
				list($tipoAccion,$fecha,$hora)=explode(";",$a_arrayFechaTiempo[$i]);
				
				if($tipoAccion == 1)				
				{
					$a_arrayFechaTiempo[$i]=$a_arrayFechaTiempo[$i];
					
					list($tipoAccionAnterior,$fechaAnterior,$horaAnterior)=explode(";",$a_arrayFechaTiempo[$i-1]);
															
					if($i > 0 && $tipoAccionAnterior != 39 )
					{
						$a_arrayFechaTiempo[$i-1]=$a_arrayFechaTiempo[$i-1];
					}
				}
				else if($tipoAccion == 39)
				{
					$a_arrayFechaTiempo[$i]=$a_arrayFechaTiempo[$i];
				}
				else if($i == sizeof($a_arrayFechaTiempo)-1 || $i == 0)
				{
					$a_arrayFechaTiempo[$i]=$a_arrayFechaTiempo[$i];					
				}
				else
				{
					$a_arrayFechaTiempo[$i]=false;
				}		
				
				if($a_arrayFechaTiempo[$i] != false)
				{
					$listaTiempo[$indice++]=$a_arrayFechaTiempo[$i];
				}
				
			}		
			
			$tiempoTotal=0;
			
			for($i=0;$i<sizeof($listaTiempo)-1;$i+=2)
			{
				list($tipoAccion,$fecha,$hora)=explode(";",$listaTiempo[$i]);
				list($tipoAccionSiguiente,$fechaSiguiente,$horaSiguiente)=explode(";",$listaTiempo[$i+1]);

				$tiempoTotal+=$this->TiempoSegundosUnix($fechaSiguiente,$horaSiguiente)-$this->TiempoSegundosUnix($fecha,$hora);
				
			}		
			
			return $tiempoTotal;
			
		}
		
		function CadenaTiempo($a_segundos)
		{
			$horas=0;
			$minutos=0;
			
			if(empty($a_segundos))
			{
				$strSegundos="0 segundos";
			}
			else
			{
									
				if($a_segundos >= 3600)
				{
					$horas=(int)($a_segundos/3600);
					$a_segundos=$a_segundos%3600;
				}
				
				if($a_segundos >= 60)
				{
					$minutos=(int)($a_segundos/60);
					$a_segundos=$a_segundos%60;
				}
				
				if($horas > 1)
				{
					$strHoras=$horas." horas";
				}
				else if( $horas == 1)
				{
					$strHoras=$horas." hora";
				}
				
				if($minutos > 1)
				{
					$strMinutos=$minutos." minutos";
				}
				else if( $minutos == 1)
				{
					$strMinutos=$minutos." minuto";
				}
				
				if($a_segundos > 1)
				{
					$strSegundos=$a_segundos." segundos";
				}
				else if($a_segundos == 1)
				{
					$strSegundos=$a_segundos." segundo";
				}
			}
		
			
			return $strHoras." ".$strMinutos." ".$strSegundos;

		}
		
		
		function StrDiaSemana($a_diasSemana)
		{
			switch($a_diasSemana)
			{
				case 0:
				{
					$strDiaSemana="domingo";
				}
				break;
				
				case 1:
				{
					$strDiaSemana="lunes";
				}
				break;
				
				case 2:
				{
					$strDiaSemana="martes";
				}
				break;
				
				case 3:
				{
					$strDiaSemana="miercoles";
				}
				break;
				
				case 4:
				{
					$strDiaSemana="jueves";
				}
				break;
				
				case 5:
				{
					$strDiaSemana="viernes";
				}
				break;
				
				case 6:
				{
					$strDiaSemana="sabado";
				}
				break;
				
				case 7:
				{
					$strDiaSemana="domingo";
				}
				break;
			}
			return $strDiaSemana;
		}		
	
		
		function StrMes($a_mes)
		{
			switch(($a_mes))
			{
				case 1:
				{
					$strMes="enero";
				}
				break;
				
				case 2:
				{
					$strMes="febrero";
				}
				break;
				
				case 3:
				{
					$strMes="marzo";
				}
				break;
				
				case 4:
				{
					$strMes="abril";
				}
				break;
				
				case 5:
				{
					$strMes="mayo";
				}
				break;
				
				case 6:
				{
					$strMes="junio";
				}
				break;
				
				case 7:
				{
					$strMes="julio";
				}
				break;
				
				case 8:
				{
					$strMes="agosto";
				}
				break;
				
				case 9:
				{
					$strMes="septiembre";
				}
				break;
				
				case 10:
				{
					$strMes="octubre";
				}
				break;
				
				case 11:
				{
					$strMes="noviembre";
				}
				break;
				
				case 12:
				{
					$strMes="diciembre";
				}
				break;
			}
			
			return $strMes;
		}		
	}
?>
