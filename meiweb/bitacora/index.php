<?
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once('../bitacora/Bitacora.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
	
	if(comprobarSession())
	{
		
		mysql_query("SET NAMES 'utf8'");
		
		$baseDatos= new BD();
		$bitacora= new Bitacora();
		
		$idAccionSalida=39;
		$idAccionIngreso=1;
		
		if(isset($_GET['fechaSeleccionada']))
		{
			$fechaSeleccionada=$_GET['fechaSeleccionada'];
		}
		else
		{
			$sql="SELECT MAX(mei_bitacora.fecha) 
					FROM mei_bitacora
						WHERE mei_bitacora.idusuario = '".$_GET['idUsuario']."'";
						
		$fechaReciente=$baseDatos->ConsultarBD($sql);		
		list($fechaSeleccionada)=mysql_fetch_array($fechaReciente);
		
		}
		
		if(isset($fechaSeleccionada))
		{
			$cadenaTituloBitacora=$bitacora->CadenaFechaDsDMA($fechaSeleccionada);
		}
		
		if(empty($_POST['txt_fecha1']) || empty($_POST['txt_fecha2']))
		{
			$sql="SELECT MIN( DATE_FORMAT(mei_bitacora.fecha,'%Y-%c-%e') ) , MAX( DATE_FORMAT(mei_bitacora.fecha,'%Y-%c-%e') ) 
					FROM mei_bitacora
						WHERE mei_bitacora.idusuario = '".$_GET['idUsuario']."'";
						
			$intervaloFechas=$baseDatos->ConsultarBD($sql);
			list($fecha1,$fecha2)=mysql_fetch_array($intervaloFechas);
			
			$sql="SELECT mei_bitacora.idmodulo ,mei_bitacora.idtipoaccion ,mei_bitacora.fecha ,mei_bitacora.hora ,DATE_FORMAT(mei_bitacora.hora,'%l:%i:%s %p'),mei_bitacora.descripcion
					FROM mei_bitacora 
						WHERE mei_bitacora.idusuario='".$_GET['idUsuario']."' 
							AND mei_bitacora.fecha ='".$fechaSeleccionada."' ORDER BY(mei_bitacora.idbitacora)";
		}
		else
		{
			$fecha1=$_POST['txt_fecha1'];
			$fecha2=$_POST['txt_fecha2'];
			
			unset($fechaSeleccionada);
			
			$cadenaTituloBitacora="De ".$bitacora->CadenaFechaDsDMA($fecha1)." al ".$bitacora->CadenaFechaDsDMA($fecha2);
			
			$sql="SELECT mei_bitacora.idmodulo ,mei_bitacora.idtipoaccion ,mei_bitacora.fecha ,mei_bitacora.hora,DATE_FORMAT(mei_bitacora.hora,'%l:%i:%s %p'),mei_bitacora.descripcion
					FROM mei_bitacora 
						WHERE mei_bitacora.idusuario='".$_GET['idUsuario']."' 
							AND mei_bitacora.fecha 
								BETWEEN '".$fecha1."' 
									AND '".$fecha2."' ORDER BY(mei_bitacora.idbitacora)";
		}
		
		$consultaBitacora=$baseDatos->ConsultarBD($sql);
		
		$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
				FROM mei_usuario 
					WHERE mei_usuario.idusuario='".$_GET['idUsuario']."'";
		
		$consultaUsuario=$baseDatos->ConsultarBD($sql);
		list($nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($consultaUsuario);
		
		$nombreUsuario= $nombre1." ".$nombre2." ".$apellido1." ".$apellido2;
		
		$indice=0;
		while(list($idModulo,$idTipoAccion,$fechaAccion,$horaAccion,$horaAccionF,$descripcion)=mysql_fetch_array($consultaBitacora))
		{
			$sql="SELECT mei_modulo.nombre 
					FROM mei_modulo 
						WHERE mei_modulo.idmodulo='".$idModulo."'";
						
			$consultaModulo=$baseDatos->ConsultarBD($sql);
			list($nombreModulo)=mysql_fetch_array($consultaModulo);
			
			$sql="SELECT mei_tipoaccion.tipoaccion 
					FROM mei_tipoaccion 
						WHERE mei_tipoaccion.idtipoaccion='".$idTipoAccion."'";
						
			$consultaAccion=$baseDatos->ConsultarBD($sql);
			list($accion)=mysql_fetch_array($consultaAccion);
			
			$listaBitacora[$indice]=$idTipoAccion."[$$$]".$nombreModulo."[$$$]".$accion."[$$$]".$fechaAccion."[$$$]".$horaAccion."[$$$]".$horaAccionF;
			$listaTiempoTotal[$indice]=$idTipoAccion.";".$fechaAccion.";".$horaAccion;
			
			$indice++;
		}
		?>
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<link href="../temas/<?= recuperarVariableSistema("sistematema")?>/estilo1024x768.css" rel="stylesheet" type="text/css">
					<title>MEIWEB</title>
					<script language="javascript">
						window.name="ventanaBitacora";
						
						function mostrarBitacora()
						{
							if(validarFechas(document.frm_selectorBitacora.txt_fecha1.value,document.frm_selectorBitacora.txt_fecha2.value))
							{
								document.frm_selectorBitacora.submit();
							}
							else
							{
								alert("El intervalo de fechas seleccionado no es correcto");
								document.frm_selectorBitacora.reset();
							}
						}
						
						function validarFechas(fecha1,fecha2)
						{
							if(parseIntFecha(fecha1) <= parseIntFecha(fecha2))
							{
								return true;
							}
							else
							{
								return false;
							}
						}
						
						function parseIntFecha(fecha)
						{
							var cadenaFecha="";
							vectorFecha=fecha.split("-");
							
							for(i=0;i<vectorFecha.length;i++)
							{
								cadenaFecha+=vectorFecha[i];
							}
							
							return parseInt(cadenaFecha);
						}
					</script>
				</head>
				<body>
		<?
			$selectorFecha1=new FrmCalendario('frm_selectorBitacora','txt_fecha1','formulario','true',$fecha1);
			$selectorFecha2=new FrmCalendario('frm_selectorBitacora','txt_fecha2','formulario','true',$fecha2);
		?>
					<table class="tablaPrincipal">
						<tr valign="top">
                            <td class="tablaEspacio">&nbsp;</td>
                            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                            <td class="tablaEspacio">&nbsp;</td>
							<td>
							<form name="frm_selectorBitacora" method="post" action="<?= $PHP_SELF?>?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idUsuario=<?= $_GET['idUsuario']?>">
							<table class="tablaGeneral">
								<tr class="trSubTitulo">
									<td width="279">&nbsp;&nbsp;Mostrar Registro de Bit&aacute;cora Desde</td>
									<td width="120"><? $selectorFecha1->CrearFrmCalendario(); ?></td>
									<td width="76"><div align="center">Hasta</div></td>
									<td width="120"><? $selectorFecha2->CrearFrmCalendario(); ?></td>
									<td width="211"><input name="btn_mostrarBitacora" type="button" id="btn_mostrarBitacora" value="Mostrar Bit&aacute;cora" onClick="javascript: mostrarBitacora()"></td>
								</tr>
							</table><br>
							</form>
							<table class="tablaGeneral">
								<tr class="trTitulo">
									<td width="677"><img src="imagenes/bitacora.gif" width="16" height="16" align="texttop"> Ver Bit&aacute;cora </td>
									<td width="137">&nbsp;</td>
								</tr>
							</table>
							<table class="tablaGeneral">
								<tr class="trSubTitulo">
									<td colspan="2" class="trSubTitulo">Bit&aacute;cora de <?= $nombreUsuario?></td>
								</tr>
								<tr class="trSubTitulo">
								  <td colspan="2" class="trSubTitulo"><div align="right"><a href="../estadistica/verEstadisticaAlumno.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idUsuario=<?= $_GET['idUsuario']?>" title="Ver Estadisticas del uso de MeiWeb" class="link"><b><img src="../estadistica/imagenes/estadistica.gif" width="16" height="16" border="0" align="texttop"> Ver Estad&iacute;stica</b></a></div></td>
							  </tr>
								<tr class="trInformacion">
									<td class="trInformacion"><?= $cadenaTituloBitacora?></td>
									<td class="trInformacion"><div align="center"><b>Historial de Visitas </b></div></td>
								</tr>
								<tr valign="top" class="trListaOscuro">
								  <td>
		<?
			if(sizeof($listaBitacora) > 0)
			{
				$tiempoTotal=0;
		?>
							<table class="tablaCentral">
								<tr class="trSubTitulo">
								  <td width="17" class="trSubTitulo"><div align="right"></div></td>
									<td width="168" height="23" class="trSubTitulo"><div align="center">M&oacute;dulo Visitado </div></td>
									<td width="187" class="trSubTitulo"><div align="center">Acci&oacute;n Realizada </div></td>
									<td width="157" class="trSubTitulo"><div align="center">Fecha </div></td>
									<td width="77" class="trSubTitulo"><div align="center">Hora</div></td>
								</tr>
		<?
				for($i=0;$i<sizeof($listaBitacora);$i++)
				{
					list($idTipoAccion,$nombreModulo,$accion,$fechaAccion,$horaAccion,$horaAccionF)=explode("[$$$]",$listaBitacora[$i]);
					
					if($idTipoAccion == $idAccionIngreso || $idTipoAccion == $idAccionSalida || $i== sizeof($listaBitacora)-1 )
					{
						$tiempoAccion=0;
						unset($cadenaTiempoUso);
					}
					else
					{
						list($idTipoAccionPost,$nombreModuloPost,$accionPost,$fechaAccionPost,$horaAccionPost,$horaAccionFPost)=explode("[$$$]",$listaBitacora[$i+1]);
						$tiempoAccion=$bitacora->TiempoSegundosUnix($fechaAccionPost,$horaAccionPost)-$bitacora->TiempoSegundosUnix($fechaAccion,$horaAccion);
						$cadenaTiempoUso="Tiempo estimado de la accion: ".$bitacora->CadenaTiempo($tiempoAccion);
					}
					
					$tiempoTotal+=$tiempoAccion;
					
					if($i%2==0)
					{
						$color="trListaClaro";
					}
					else
					{
						$color="trListaOscuro";
					}
		?>
								<tr>
								  <td class="<?=$color?>"><div align="right"><?= $i+1?>
						          </div></td>
									<td height="23" class="<?=$color?>">&nbsp;&nbsp;<?=ucwords($nombreModulo)?></td>
									<td class="<?=$color?>">&nbsp;&nbsp;<?= $accion?></td>
									<td title="<?= $bitacora->CadenaFechaDsDMA($fechaAccion)?>" class="<?=$color?>">&nbsp;&nbsp;<?= $bitacora->CadenaFechaDMA($fechaAccion)?></td>
									<td title="<?= $cadenaTiempoUso?>" class="<?=$color?>">&nbsp;&nbsp;<?= $horaAccionF?></td>
								</tr>
		<?
				}
				
				
								
				if($tiempoTotal > $bitacora->TiempoTotalSistema($listaTiempoTotal))
				{
					$tiempo1=$tiempoTotal;
					$tiempo2=$bitacora->TiempoTotalSistema($listaTiempoTotal);
				}
				else
				{
					$tiempo1=$bitacora->TiempoTotalSistema($listaTiempoTotal);
					$tiempo2=$tiempoTotal;
				}
		?>
								<tr class="trInformacion">
									<td height="23" colspan="5" class="trInformacion" title="Total del tiempo de actividad en los módulos de MeiWeb"><img src="imagenes/reloj.gif" width="16" height="16" align="texttop"> Tiempo Total Estimado de Actividad en MeiWeb:<b><?= $bitacora->CadenaTiempo($tiempo2)?></b></td>
								</tr>
								<tr class="trInformacion">
								  <td height="23" colspan="5" class="trInformacion" title="Tiempo total en MeiWeb"><img src="imagenes/reloj.gif" width="16" height="16" align="texttop"> Tiempo Total en MeiWeb: <b><?= $bitacora->CadenaTiempo($tiempo1)?></b></td>
							  </tr>
							</table>

		<?
			
			
			}
			else
			{
				print "No existen registros de Bitacora";
			}
		?>
								  </td>
										<td><div align="right"><iframe scrolling="yes" width="193" height="300" src="ventanaHistorial.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idUsuario=<?= $_GET['idUsuario']?>&fechaSeleccionada=<?= $fechaSeleccionada?>"></iframe></div></td>
							  </tr>
							  
							  <tr><td class="tablaEspacio">&nbsp;</td></tr>
	   		
	   		<tr >
	   		<? if ($_SESSION['idtipousuario']==5) { ?>
			<td class="trSubTitulo" width="100%" align="center" valign="middle" > <a href="../usuario/VirListarProfesor.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"> Volver </a></td>
			 <? } else {?> 
			 <td class="trSubTitulo" width="100%" align="center" valign="middle" > <a href="../usuario/listarProfesor.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&grupo=<?=$_GET["orden"]?>" class="link"> Volver </a></td>
			 <? } ?>
			</tr>
							  </table>
							</td>
                            <td class="tablaEspacio">&nbsp;</td>
                            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                            <td class="tablaEspacioDerecho">&nbsp;</td>
						</tr>

					</table>
						
				</body>
			</html>
		<?
	}
	else
	{
		redireccionar("../login/");
	}
?>
