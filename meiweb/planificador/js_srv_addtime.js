// JavaScript Document
	function getAsincServer()
	{
		var oAscServ;
		
		if(window.XMLHttpRequest)
		{//noesIE
			oAscServ = new XMLHttpRequest();
		}
		else
		{//EsIEonotieneeloAscServeto
			try
			{
				oAscServ = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e)
			{
				alert('Navegador no soportado');
			}
		}
		return oAscServ;
	}
	
	function getAddTime(fecha_inicial, dias, id)
	{
		oXML=getAsincServer();
		//Preparamoslapeticiónconparametros
		oXML.open('GET','srv_get_addtime.php?fecha_inicial='+ fecha_inicial + '&dias=' + dias,true);
		//Preparamoslarecepción
		oXML.onreadystatechange=leerDatos;
		//Realizamoslapetición
		oXML.send(null);
		
		function leerDatos()
		{
			if(oXML.readyState == 4)
			{
				if(oXML.responseText.length > 0)
				{
					document.getElementById(id).value = oXML.responseText;
				}
			}
		}
	}