<?php
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');

	$baseDatos=new BD(); 
	
	if(comprobarSession())
	{
		if($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==7)
		{
			$periodo = (int)$_POST['hid_periodo'];
			$num_modulo = (int)$_POST['hid_modulos'];
			
			if ($_SESSION['idtipousuario']==5)
			{
				$sql="SELECT mei_planificador.idplanificador FROM mei_planificador WHERE mei_planificador.idplanificador 
					IN (SELECT mei_relplavirgru.idplanificador FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo 
					IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$_GET['idmateria']." 
					AND mei_virgrupo.idvirgrupo IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru 
					WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].")))";
			}
			else 
			{
				$sql="SELECT COUNT(mei_planificador.idplanificador) FROM mei_planificador WHERE mei_planificador.idplanificador 
					IN (SELECT mei_relplagru.idplanificador FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo 
					FROM mei_grupo WHERE mei_grupo.idmateria =".$_GET['idmateria']." AND mei_grupo.idgrupo
					IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario =  ".$_SESSION['idusuario'].")))";
			}
			$resultado=$baseDatos->ConsultarBD($sql);	
			list($numPlanificador)= mysql_fetch_array($resultado);
			if(!empty($numPlanificador))
			{
				for($i=0; $i<$num_modulo; $i++)
				{
					$sql = "UPDATE mei_planificador SET 
							fechainicio = '".$_POST['txt_fecha_inicial'.$i]."', 
							fechafin = '".$_POST['txt_fecha_final'.$i]."' 
							WHERE idplanificador = '".$_POST['hid_modulo'.$i]."'";
					
					$baseDatos->ConsultarBD($sql);

					if($_SESSION['idtipousuario']==5)
					{
																	
						$sql="SELECT mei_virgrupo.nombre, mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
						IN ( SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].") 
						AND mei_virgrupo.idvirmateria =".$_GET['idmateria'];
						$grupos=$baseDatos->ConsultarBD($sql);
					
						while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
						{
							$sql="DELETE FROM mei_relplavirgru WHERE mei_relplavirgru.idplanificador='".$_POST['hid_modulo'.$i]."' AND mei_relplavirgru.idvirgrupo='".$grupoCodigo."'";
							$baseDatos->ConsultarBD($sql);

							$sql="INSERT INTO mei_relplavirgru (idplanificador,idvirgrupo) VALUES ('".$_POST['hid_modulo'.$i]."','".$grupoCodigo."')";
							$baseDatos->ConsultarBD($sql);
						}
					}
					else 
					{
						$sql="SELECT mei_grupo.nombre, mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo IN ( SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].") AND mei_grupo.idmateria =".$_GET['idmateria'];
						$grupos=$baseDatos->ConsultarBD($sql);
						
						while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
						{
							$sql="DELETE FROM mei_relplagru WHERE mei_relplagru.idplanificador='".$_POST['hid_modulo'.$i]."'AND mei_relplagru.idgrupo='".$grupoCodigo."'";
							$baseDatos->ConsultarBD($sql);

							$sql="INSERT INTO mei_relplagru (idplanificador,idgrupo) VALUES ('".$_POST['hid_modulo'.$i]."','".$grupoCodigo."')";
							$baseDatos->ConsultarBD($sql);
						}
					}
				}
			}
			else
			{
				for($i = 0; $i < $num_modulo; $i++)
				{
					$sql="INSERT INTO mei_planificador (idplanificador,tema,objetivos,actividades,recursos,fechainicio,fechafin,
							numeromod, destino,idusuario) VALUES ('','".$_POST['edt_tema']."','".$_POST['edt_objetivos']."','".$_POST['edt_actividades']."','".$_POST['edt_recursos']."','".$_POST['txt_fecha_inicial' . $i]."','".$_POST['txt_fecha_final' . $i]."','".($i + 1)."','".$destino."','".$_SESSION['idusuario']."')";
					
					$baseDatos->ConsultarBD($sql);
					
					$idplanificador = $baseDatos->InsertIdBD();
					if($_SESSION['idtipousuario']==5)
					{
																	
						$sql="SELECT mei_virgrupo.nombre, mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo 
						IN ( SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].") 
						AND mei_virgrupo.idvirmateria =".$_GET['idmateria'];
						$grupos=$baseDatos->ConsultarBD($sql);
					
						while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
						{
							$sql="INSERT INTO mei_relplavirgru (idplanificador,idvirgrupo) VALUES ('".$idplanificador."','".$grupoCodigo."')";
							$baseDatos->ConsultarBD($sql);
						}
					}
					else 
					{
						$sql="SELECT mei_grupo.nombre, mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idgrupo IN ( SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].") AND mei_grupo.idmateria =".$_GET['idmateria'];
						$grupos=$baseDatos->ConsultarBD($sql);
						
						while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
						{
							$sql="INSERT INTO mei_relplagru (idplanificador,idgrupo) VALUES ('".$idplanificador."','".$grupoCodigo."')";
							$baseDatos->ConsultarBD($sql);
						}
					}
				}
			}
			redireccionar("../planificador/verPlanificador.php?cuales=activos&idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
		}
	}
	else
	{
		redireccionar("../login/");
	}
?>