<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ("../editor/fckeditor.php") ;
	include_once ('../menu1/Menu1.class.php');
	
	$baseDatos=new BD();
	$editorTema=new FCKeditor('edt_tema' , '100%' , '100%' , 'barraBasica' , '' ) ;
	$editorObjetivo=new FCKeditor('edt_objetivos' , '100%' , '100%' , 'barraBasica' , '' ) ;
	$editorActividades=new FCKeditor('edt_actividades' , '100%' , '100%' , 'barraBasica' , '' ) ;
    $editorRecursos=new FCKeditor('edt_recursos' , '100%' , '100%' , 'barraBasica' , '' ) ;
	

	
	if(comprobarSession())
	{
		$idmateria=$_GET['idmateria'];
	    $_GET['materia']=$_SESSION['sesMateria'];
	    $materia=$_GET['materia'];
		
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==7)
		{
	
			if(isset($_GET['action']))
			{
				if ($_GET['action']=='Finalizar') {
					$act=1;
				}
				else{
					$act=0;
				}

					$sql="UPDATE mei_planificador SET	
					desactivar= '".$act."'
					WHERE mei_planificador.idplanificador ='".$_GET['idplanificador']."'";
					$resultado=$baseDatos->ConsultarBD($sql);

			}

			if (isset($_GET['todos'])) {


				if ($_SESSION['idtipousuario']==5) {

					$sql2="SELECT DISTINCT(mei_virgrupo.idvirgrupo) FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria='".$_GET['idmateria']."'";
					$id=$baseDatos->ConsultarBD($sql2);
					list($idgru)=mysql_fetch_array($id);

				$sql1="SELECT mei_relplavirgru.idplanificador FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo='".$idgru."'";
				
				}
				else{

					$sql2="SELECT DISTINCT(mei_grupo.idgrupo) FROM mei_grupo WHERE mei_grupo.idmateria='".$_GET['idmateria']."'";
					$id=$baseDatos->ConsultarBD($sql2);
					list($idgru)=mysql_fetch_array($id);

				$sql1="SELECT mei_relplagru.idplanificador FROM mei_relplagru WHERE mei_relplagru.idgrupo='".$idgru."'";
			    }

			    if ($_GET['todos']=='activar') {
			    	$act=0;
			    }
			    else {
			    	$act=1;
			    }
			    $sql11=$baseDatos->ConsultarBD($sql1);
			    while (list($idplanificador)=mysql_fetch_array($sql11)) {
			    	$sql="UPDATE mei_planificador SET	
					desactivar= '".$act."'
					WHERE mei_planificador.idplanificador ='".$idplanificador."'";
					$resultado=$baseDatos->ConsultarBD($sql);
			    }

			}
			
			$sql="SELECT mei_virmateria.numeromod FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
			$resultado=$baseDatos->ConsultarBD($sql);
			list($modulos)=mysql_fetch_row($resultado);
			
			if ($_SESSION['idtipousuario']==5)
			{
				$num_mod = $modulos;	
			}
			else
			{
				$num_mod = 18;	
			}
			
			if(empty($_GET['periodo']))
			{
				$_GET['periodo'] = 7;
			}
			
			
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css_epoch.css" rel="stylesheet" type="text/css">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
            <script language="javascript" src="epoch_classes.js"></script>
     <script language="javascript" src="lib.srv.js"></script>
        <script language="javascript">
    
		window.onload = function (){
							<?php
								
								//for($i=0; $i < $num_mod ;$i++)
								{
								?>
									//cal_fecha_inicial<? print $i?>  = new Epoch('fecha_inicial<?php print $i?>','popup',document.getElementById('txt_fecha_inicial<?php print $i?>'));
									//cal_fecha_final<?php print $i?>  = new Epoch('fecha_final<?php print $i?>','popup',document.getElementById('txt_fecha_final<?php print $i?>'));
								<?php
								}
							?>
													
							};
    
    </script>
    
<script>
		function eliminarPlanificador(idplanificador) 
		{
		if(confirm("¿Está seguro de eliminar el planificador?"))
		
		location.replace('../planificador/eliminarPlanificador.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idplanificador='+idplanificador);
		}  // eliminarplanifi
		
		function modificarPlanificador(idplanificador)
		{
		location.replace('../planificador/modificarPlanificador.php?cuales=<?=$_GET['cuales']?>&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idplanificador='+idplanificador);
		}  //modificar
		
				
		function habilitar(idplanificador,accion,cuales)
		{
			if (accion == "Finalizar")
			{
				if (confirm ("¿Esta seguro que desea finalizar este modulo?"))
				{
					document.getElementById('frm_verPlanificador').action="verPlanificador.php?cuales="+cuales+"&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idplanificador="+idplanificador+"&action="+accion;
					document.getElementById('frm_verPlanificador').submit();
				}

				else

					document.getElementById('chk_activar_' + idplanificador).checked = 0;


			}
			else 
			{
				if (confirm ("¿Está seguro que desea activar el modulo?"))
				{
					document.getElementById('frm_verPlanificador').action="verPlanificador.php?cuales="+cuales+"&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idplanificador="+idplanificador+"&action="+accion;
					document.getElementById('frm_verPlanificador').submit();
				}
				else
					document.getElementById('chk_activar_' + idplanificador).checked = 0;
			}
		}
		
		function actualizar(indice)
		{
			var ln = parseInt('<?php print $num_mod?>');
			var periodo = parseInt('<?php print $_GET['periodo']?>');
			
			var str_get = "";
			
			for(i = 0; i < ln; i++)
			{
				str_get += "&txt_fecha_inicial" + i + "=" + document.getElementById('txt_fecha_inicial' + i).value + "&txt_fecha_final" + i + "=" + document.getElementById('txt_fecha_final' + i).value + "&idplanificador" + i + "=" + document.getElementById('hid_id' + i).value;	
			}
			str_get += "&indice=" + indice;
			str_get += "&periodo=" + periodo;
			str_get += "&longitud=" + ln;
			
			var respuesta = srvGetServer("../planificador/srv_fecha_modificar.php?", str_get);

			var array_respuesta = respuesta.split('[&&]');
			
			
			ln = array_respuesta.length;
			
			for(i = 0; i< ln; i++)
			{
				array_fecha = array_respuesta[i].split('[&]');
				
				document.getElementById('txt_fecha_inicial' + i).value = array_fecha[0];
				document.getElementById('txt_fecha_final' + i).value = array_fecha[1];
			}
		}

function eliminar(idpla, numod, idmateria, materia, total, cuales)
{
	if( confirm("Esta seguro de eliminar el módulo de la semana "+numod+"?"))
	{
		//alert(idpla + numod+ idmateria+ materia+ total);
		location.replace("eliminarModulo.php?idpla="+idpla+"&numod="+numod+"&idmateria="+idmateria+"&materia="+materia+"&total="+total+"&cuales="+cuales);
	}	
	
}
</script>
</head>
<body>
<form name="frm_verPlanificador" id="frm_verPlanificador" method="post" action="<?=$PHP_SELF?>">
	<table class="tablaPrincipal">
		<tr valign="top">
			<td height="520" class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
			<td class="tablaEspacio">&nbsp;</td>
			<td>
            <table class="tablaMarquesina" >
						<tr>
                        <?
                           if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==3  || $_SESSION['idtipousuario']==7)
		{
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		}
		else
		{
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		}
                           		$resultado = $baseDatos->ConsultarBD($sql);
		list($materia) = mysql_fetch_row($resultado); ?>
						<td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($_GET["materia"]))?></a><a> -> Ver Planificación </a></td>
					
                    </table>
                    <br>
                    <br>
                    
              <table class="tablaGeneral">
			  <tr class= "trTitulo">
					<td><img src="imagen/planificador.png" width="16" height="16" align="texttop">Ver Planificador de actividades  <?= $_GET['materia']?>&nbsp;&nbsp;&nbsp;
						<a href="verPlanificador.php?cuales=activos&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&todos=activar" class="link"><img src="imagen/vistos.png" width="16" height="16" align="texttop" title="Activar Todos"></a>&nbsp;&nbsp;
						<a href="verPlanificador.php?cuales=activos&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&todos=desactivar" class="link"><img src="imagen/faltan.png" width="16" height="16" align="texttop" title="Desactivar Todos"></a>
					</td>
					<td align="right">
                    	<a href="verPlanificador.php?cuales=todos&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagen/todos.png" width="16" height="16" align="texttop" title="Todos"></a>&nbsp;&nbsp;
                    	<a href="verPlanificador.php?cuales=finalizados&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagen/vistos.png" width="16" height="16" align="texttop" title="Finalizados"></a>&nbsp;&nbsp;
                    	<a href="verPlanificador.php?cuales=activos&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagen/faltan.png" width="16" height="16" align="texttop" title="Activos"></a>&nbsp;&nbsp;
					</td>
        		</tr>
			</table>
<?
		if ($_GET['cuales']=='finalizados')
		{
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.desactivar=1 AND mei_planificador.idplanificador IN (SELECT mei_relplavirgru.idplanificador
		FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$idmateria." AND mei_virgrupo.idvirgrupo
		IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."))) ORDER BY mei_planificador.numeromod";
			}
			else 
			{
					$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.desactivar=1 AND mei_planificador.idplanificador IN (SELECT mei_relplagru.idplanificador
FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria =".$idmateria." AND mei_grupo.idgrupo
IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario =  ".$_SESSION['idusuario'].")))ORDER BY mei_planificador.numeromod";
			}
		}
		else if ($_GET['cuales']=='todos')
		{
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplavirgru.idplanificador
		FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$idmateria." AND mei_virgrupo.idvirgrupo
		IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."))) ORDER BY mei_planificador.numeromod";
			}
			else 
			{
					$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplagru.idplanificador
FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria =".$idmateria." AND mei_grupo.idgrupo
IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario =  ".$_SESSION['idusuario'].")))ORDER BY mei_planificador.numeromod";
			}
		}
		else if ($_GET['cuales']=='activos')
		{
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.desactivar=0 AND mei_planificador.idplanificador IN (SELECT mei_relplavirgru.idplanificador
		FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$idmateria." AND mei_virgrupo.idvirgrupo
		IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."))) ORDER BY mei_planificador.numeromod";
			}
			else 
			{
					$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.desactivar=0 AND mei_planificador.idplanificador IN (SELECT mei_relplagru.idplanificador
FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria =".$idmateria." AND mei_grupo.idgrupo
IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario =  ".$_SESSION['idusuario'].")))ORDER BY mei_planificador.numeromod";
			}
		}
			$resultado1=$baseDatos->ConsultarBD($sql);	
			$numPlanificador=mysql_num_rows($resultado1);
			
			if ($_SESSION['idtipousuario']==5) {
			$sql2="SELECT  count(*) FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplavirgru.idplanificador
		FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$idmateria." AND mei_virgrupo.idvirgrupo
		IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."))) ORDER BY mei_planificador.numeromod";
			
		}

		else{	
			$sql2="SELECT  count(*) FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplagru.idplanificador
FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria =".$idmateria." AND mei_grupo.idgrupo
IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario =  ".$_SESSION['idusuario'].")))ORDER BY mei_planificador.numeromod";
			
		}
	    	$resultado2=$baseDatos->ConsultarBD($sql2);	
			list($total)=mysql_fetch_array($resultado2);
			
			if ($numPlanificador > 0)
			{
				
	$cont=0;
	$i = 0;
	while (list($idplanificador,$numeromod,$fechainicio,$fechafin,$tema,$objetivos,$actividades,$recursos,$destino,$desactivar)=mysql_fetch_array($resultado1))
			
			{
?>

  <table  class="tablaGeneral">
				
              <tr style="height: 30px; background:#99B3FF;" class="trInformacion">
              	<?
			 if($_SESSION['idtipousuario']==5)
				{
  		$sql="SELECT mei_virmateria.idvirmateria,mei_virmateria.formato FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$idmateria;		
		$resultado=$baseDatos->ConsultarBD($sql);
		list($idmateria,$formato)=mysql_fetch_row($resultado);	
		
		if ($formato=="Semanas")
			$formato="Semana";
		else if ($formato=="Temas")
			$formato="Tema"; 
?>
 <td style="width: 350px; padding-left: 20px; text-align: left; color: red;" align="center"><strong> <?= $formato?> :</strong> <?= $numeromod ?> </td>
                    
                   
<?     
			}
			
			else
			{
				
		?>
 <td style="width: 350px; padding-left: 20px; text-align: left; color: red; font-size: 14px; font-weight: bold;" align="center"><strong>Semana: </strong> <?= $numeromod ?> </td>
                    
                   
<?   
				
			}?>
     <td colspan="3"><strong>Docente: 
       <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?> </strong></td>
    <?
			
	  		if($desactivar==1)
			{
				$chk_activar=" ";
				$nombre="Activar";
				
			}
			else
			{
				$chk_activar=" ";
				$nombre="Finalizar";
 			}
 ?>
 
 
                   
         <td colspan="2" align="right"><b><?=$nombre?></b>
           <input type="checkbox" name="chk_activar_<?php print $idplanificador?>" id="chk_activar_<?php print $idplanificador?>" value="1" <?=$chk_activar ?>  onclick="javascript:habilitar('<?=$idplanificador?>','<?=$nombre?>','<?=$_GET["cuales"]?>')" >&nbsp; &nbsp; &nbsp;                 
         </td>
         <td align="right" title="Eliminar Módulo"><a style="text-decoration: none; color: red; font-size: 14px;" href="javascript:eliminar('<?=$idplanificador?>', '<?=$numeromod?>', '<?=$_GET["idmateria"]?>','<?=$_GET["materia"]?>','<?=$total?>','<?=$_GET["cuales"]?>')">Eliminar</a></td>
          </tr>
   	 		  
                   
				<tr class="trListaOscuro">
					<td colspan="2"><b>Fecha inicio:&nbsp;</b></td>
					<td><table width="100" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td><input name="txt_fecha_inicial<? print $i?>" type="text" class="cajaFecha" id="txt_fecha_inicial<? print $i?>" size="10" readonly="" value="<?php print  $fechainicio?>" /></td>
					    <td><img src="imagen/b_calendario.gif" alt="" width="16" height="16" title="Selector de fechas"  /></td>
				      </tr>
				    </table></td>
					<td colspan="3"><b>Fecha finalizaci&oacute;n:&nbsp;</b></td>
					<td><table width="100" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td width="68"><input name="txt_fecha_final<? print $i?>" type="text" class="cajaFecha" id="txt_fecha_final<? print $i?>" size="10" readonly="" value="<?php print  $fechafin?>" /></td>
					    <td><img src="imagen/b_calendario.gif" alt="" width="16" height="16" title="Selector de fechas"  /></td>
				      </tr>
				    </table></td>
					
       	      </tr>
				 <tr class="trInformacion">
				  <td colspan="7">&nbsp;</td>
		  </tr>
				<tr class="trSubtitulo">
				  <td colspan="7"><b>OBJETIVOS</b></td>
			  </tr>
				<tr class="trInformacion">
				  <td colspan="7">&nbsp;</td>
		  </tr>
				<tr class="trListaClaro">
                 <td colspan="7">
				    <?= $objetivos?>
				  </td>
			    </tr>
              
				<tr class="trInformacion">
				  <td colspan="7">&nbsp;</td>
		  </tr>
				<tr class="trSubtitulo">
					<td colspan="7"><p><b>TEMA
		           </b>
					    <input type="hidden" name="hid_id<?php print $i?>" id="hid_id<?php print $i?>" value="<?php print $idplanificador?>">
				   </p></td>
			    </tr>
				<tr class="trInformacion">
				  <td colspan="7">&nbsp;</td>
		  </tr>
                
                
				<tr class="trListaClaro">		
				  <td colspan="7"><?= $tema?></td>
			 	</tr>
				
				<tr class="trInformacion">
				  <td colspan="7">&nbsp;</td>
		  </tr>
				<tr class="trSubtitulo">
				 <td colspan="7"><b>ACTIVIDADES</b></td>
                </tr>
				<tr class="trInformacion">
				  <td colspan="7">&nbsp;</td>
		  </tr>
				<tr class="trListaClaro">
				  <td colspan="7">
				    <?= $actividades?>
				  </td>
			   </tr>
				<tr class="trInformacion">
				  <td colspan="7">&nbsp;</td>
		  </tr>
				<tr class="trSubtitulo">
					<td colspan="7"><b>RECURSOS</b></td>
				</tr>
				<tr class="trInformacion">
				  <td colspan="7">&nbsp;</td>
		  </tr>
				<tr class="trListaClaro">
				  <td colspan="7">
				    <?= $recursos?>
				  </td>
			    </tr>
  
<?
			
			
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==7)
			{			
?>
			<tr class="trInformacion">
			  <td colspan="7"><div align="center">
             <input type="button" name="btn_modificar" value="Modificar" onClick="javascript:modificarPlanificador('<?= $idplanificador ?>')">
			  </div>
			  </td>
              
		  </tr>
<?
			}
			$i++;
?>			
        </table>
  <br> 
  </br>

<?
			$cont++;
		}
			}
			else 
			{
?>
			<table class="tablaGeneral">
				<tr class="trInformacion">
					<?
					if($_GET['cuales']=='todos'){
					?>
					<td>Actualmente no existe planificador para  <?= $_GET['materia']?></td>
					<?
					}
					?>
					<?
					if($_GET['cuales']=='activos'){
					?>
					<td>Actualmente no existen módulos activos para  <?= $_GET['materia']?></td>
					<?
					}
					?>
					<?
					if($_GET['cuales']=='finalizados'){
					?>
					<td>Actualmente no existen módulos finalizados para  <?= $_GET['materia']?></td>
					<?
					}
					?>
					
				</tr>
		</table>
<?			
			}
?>
		</td>
        <td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>             
<?
		}
		else
		{
			redireccionar('../login/');
		
		}
	}
	else
	{
		redireccionar('../login/');
	}	
?>