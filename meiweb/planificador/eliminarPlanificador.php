<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
	include_once("../editor/fckeditor.php") ;
   	include_once('../calendario/FrmCalendario.class.php');
	
	$baseDatos=new BD();	
	if(comprobarSession())
	{
		$idmateria=$_GET['idmateria'];
		$materia=$_GET['materia'];
		$baseDatos= new BD();
		
			if(!empty($_GET['idplanificador']))
			{
				$sql="DELETE FROM mei_planificador WHERE idplanificador =".$_GET['idplanificador'];
				$consulta=$baseDatos->ConsultarBD($sql);
			}
			else
			{
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					$sql="DELETE FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplavirgru.idplanificador
						FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo 
						WHERE mei_virgrupo.idvirmateria =".$_GET['idmateria']." AND mei_virgrupo.idvirgrupo
						IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].")))";
				}
				else
				{
					$sql="DELETE FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplagru.idplanificador
						FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo 
						WHERE mei_grupo.idmateria =".$_GET['idmateria']." AND mei_grupo.idgrupo
						IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].")))";
				}
				$consulta=$baseDatos->ConsultarBD($sql);
			}
			redireccionar("../planificador/verPlanificador.php?cuales=todos&idmateria=".$idmateria."&materia=".$materia);		
	}

	else
	{
		redireccionar('../login/');
	}
?>