<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ("../editor/fckeditor.php") ;
	include_once ('../menu1/Menu1.class.php');
	
	$baseDatos=new BD();
	$editorTema=new FCKeditor('edt_tema' , '100%' , '100%' , 'barraBasica' , '' ) ;
	$editorObjetivo=new FCKeditor('edt_objetivos' , '100%' , '100%' , 'barraBasica' , '' ) ;
	$editorActividades=new FCKeditor('edt_actividades' , '100%' , '100%' , 'barraBasica' , '' ) ;
    $editorRecursos=new FCKeditor('edt_recursos' , '100%' , '100%' , 'barraBasica' , '' ) ;
	

	
	if(comprobarSession())
	{
		$idmateria=$_GET['idmateria'];
	    $materia=$_GET['materia'];
		
		if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6)
		{
		
		
			
			
			
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css_epoch.css" rel="stylesheet" type="text/css">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
            <script language="javascript" src="epoch_classes.js"></script>
     <script language="javascript" src="lib.srv.js"></script>

    

</head>
<body>
<form name="frm_verPlanificador" id="frm_verPlanificador" method="post" action="<?=$PHP_SELF?>">
	<table class="tablaPrincipal">
		<tr valign="top">
			<td height="520" class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
			<td class="tablaEspacio">&nbsp;</td>
			<td>
            <table class="tablaGeneral">
			  <tr class= "trTitulo">
					<td><img src="imagen/planificador.png" width="16" height="16" align="texttop">Ver Planificador de actividades  <?= $_GET['materia']?></td>
					<td align="right">
                    	<a href="verPlanificadorEstudiante.php?cuales=todos&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagen/todos.png" width="16" height="16" align="texttop" title="Todos"></a>&nbsp;&nbsp;
                    	<a href="verPlanificadorEstudiante.php?cuales=finalizados&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagen/vistos.png" width="16" height="16" align="texttop" title="Finalizados"></a>&nbsp;&nbsp;
                    	<a href="verPlanificadorEstudiante.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagen/faltan.png" width="16" height="16" align="texttop" title="Activos"></a>&nbsp;&nbsp;
					</td>
        		</tr>
			</table>
<?
		if ($_GET['cuales']=='finalizados')
		{
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.desactivar=1 AND mei_planificador.idplanificador IN (SELECT mei_relplavirgru.idplanificador
		FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$idmateria." AND mei_virgrupo.idvirgrupo
		IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."))) ORDER BY mei_planificador.numeromod";
			}
			else 
			{
					$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.desactivar=1 AND mei_planificador.idplanificador IN (SELECT mei_relplagru.idplanificador
FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria =".$idmateria." AND mei_grupo.idgrupo
IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario =  ".$_SESSION['idusuario'].")))ORDER BY mei_planificador.numeromod";
			}
		}
		else if ($_GET['cuales']=='todos')
		{
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplavirgru.idplanificador
		FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$idmateria." AND mei_virgrupo.idvirgrupo
		IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."))) ORDER BY mei_planificador.numeromod";
			}
			else 
			{
					$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplagru.idplanificador
FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria =".$idmateria." AND mei_grupo.idgrupo
IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario =  ".$_SESSION['idusuario'].")))ORDER BY mei_planificador.numeromod";
			}
		}
		else
		{
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.desactivar=0 AND mei_planificador.idplanificador IN (SELECT mei_relplavirgru.idplanificador
		FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$idmateria." AND mei_virgrupo.idvirgrupo
		IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."))) ORDER BY mei_planificador.numeromod";
			}
			else 
			{
					$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin, mei_planificador.tema, mei_planificador.objetivos, mei_planificador.actividades, mei_planificador.recursos, mei_planificador.destino,mei_planificador.desactivar FROM mei_planificador WHERE mei_planificador.desactivar=0 AND mei_planificador.idplanificador IN (SELECT mei_relplagru.idplanificador
FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo WHERE mei_grupo.idmateria =".$idmateria." AND mei_grupo.idgrupo
IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario =  ".$_SESSION['idusuario'].")))ORDER BY mei_planificador.numeromod";
			}
		}
			$resultado1=$baseDatos->ConsultarBD($sql);	
			$numPlanificador=mysql_num_rows($resultado1);
			if ($numPlanificador > 0)
			{
				
	$cont=0;
	$i = 0;
	while (list($idplanificador,$numeromod,$fechainicio,$fechafin,$tema,$objetivos,$actividades,$recursos,$destino,$desactivar)=mysql_fetch_array($resultado1))
			
			{
?>

  <table  class="tablaGeneral">
				
     <tr style="height: 30px; background:#99B3FF;" class="trInformacion">
    <?
	
				if($_SESSION['idtipousuario']==6 )
				{
	
				$sql="SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo WHERE 
						(mei_virgrupo.idvirmateria=".$_GET['idmateria']." AND mei_virgrupo.idvirgrupo IN 
						(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."))"; 
				
				}
				else
				{
					$sql="SELECT mei_grupo.idgrupo FROM mei_grupo WHERE 
						(mei_grupo.idmateria=".$_GET['idmateria']." AND mei_grupo.idgrupo IN 
						(SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']."))"; 
				
				}
				$resultado=$baseDatos->ConsultarBD($sql);
				list($grupo)=mysql_fetch_array($resultado);
				if($_SESSION['idtipousuario']==6 )
				{
				
			  $sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE
 							mei_usuario.idtipousuario=5 AND mei_usuario.idusuario IN (
							  SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$grupo.")";
				}
				else
				{
					 $sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE
 							mei_usuario.idtipousuario=2 AND mei_usuario.idusuario IN (
							  SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idgrupo=".$grupo.")";
				}
							$resultado=$baseDatos->ConsultarBD($sql);
			list($nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($resultado);
			  ?>
			  <?
			 if($_SESSION['idtipousuario']==6 )
				{
  		$sql="SELECT mei_virmateria.idvirmateria,mei_virmateria.formato FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$idmateria;		
		$resultado=$baseDatos->ConsultarBD($sql);
		list($idmateria,$formato)=mysql_fetch_row($resultado);	
		
		if ($formato=="Semanas")
			$formato="Semana";
		else if ($formato=="Temas")
			$formato="Tema"; 
?>
 <td style="width: 350px; padding-left: 20px; text-align: left; color: red; font-size: 14px; font-weight: bold;" align="center"><strong> <?= $formato?> :</strong> <?= $numeromod ?> </td>
          
<?     
			}
			
			else
			{
				
		?>
 <td style="width: 350px; padding-left: 20px; text-align: left; color: red; font-size: 14px; font-weight: bold;"  align="center"><strong>Semana: </strong> <?= $numeromod ?> </td>
                    
                   
<? 		}?>
              
     <td colspan="2"><strong>&nbsp;Docente :&nbsp;<?=$nombre1?> <?=$nombre2?> <?=$apellido1 ?> <?=$apellido2?> </strong></td>
    <?
	  		if($desactivar==1)
				$nombre="Finalizada";
			else
				$nombre="Activa";
?>
         </tr>
				<tr class="trInformacion">
				  <td>&nbsp;<b>Fecha de Inicio: </b><?=$fechainicio?> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</td>
				  <td>&nbsp;<b>Fecha de Finalizaci&oacute;n: </b>
			      <?=$fechafin?></td>
				  <td>&nbsp;<b><?=$nombre?></b></td>
			    </tr>				
				<tr class="trInformacion">
				  <td colspan="3" >&nbsp;</td>
		 		</tr>
				<tr class="trSubtitulo">
				  <td colspan="3"><b>OBJETIVOS
				  </b></td>
			  	</tr>
				<tr class="trInformacion">
				 <td colspan="3">&nbsp;</td>
				  </tr>
					<tr class="trListaClaro">
              	   <td colspan="3" >
				    <?= $objetivos?>
					  </td>
			    	</tr>
                <tr class="trInformacion">
				  <td colspan="3" >&nbsp;</td>
		  </tr>
				<tr class="trSubtitulo">
					<td colspan="3" ><p><b>TEMAS
		           </b>
					    <input type="hidden" name="hid_id<?php print $i?>" id="hid_id<?php print $i?>" value="<?php print $idplanificador?>">
				   </p></td>
			    </tr>
				<tr class="trInformacion">
				  <td  colspan="3">&nbsp;</td>
		  </tr>
                
				<tr class="trListaClaro">		
				  <td colspan="3"><?= $tema?></td>
			 	</tr>
				<tr class="trInformacion">
				  <td colspan="3">&nbsp;</td>
		  </tr>
          
				<tr class="trSubtitulo">
				 <td colspan="3"><b>ACTIVIDADES
					</b></td>
                </tr>
				<tr class="trInformacion">
				  <td colspan="3">&nbsp;</td>
		  </tr>
				<tr class="trListaClaro">
				  <td colspan="3" >
				    <?= $actividades?>
				  </td>
			   </tr>
				<tr class="trInformacion">
				  <td colspan="3" >&nbsp;</td>
		  </tr>
				<tr class="trSubtitulo">
					<td colspan="3" ><b>RECURSOS
					</b></td>
				</tr>
				<tr class="trInformacion">
				  <td colspan="3" >&nbsp;</td>
		  </tr>
				<tr class="trListaClaro">
				  <td colspan="3" >
				    <?= $recursos?>
				  </td>
			    </tr>
  </table>
  <br> 
  </br>

<?
			$cont++;
		}
			}
			else 
			{
?>
			<table class="tablaGeneral">
				<tr class="trInformacion">
	<td>Actualmente no existe planificador para  <?= $_GET['materia']?></td>
				</tr>
		</table>
<?			
			}
?>
		</td>
        <td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>             
<?
		}
		else
		{
			redireccionar('../login/');
		
		}
	}
	else
	{
		redireccionar('../login/');
	}	
?>