<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../baseDatos/BD.class.php');
	include_once ('../menu1/Menu1.class.php');
	include_once ('../planificador/add_fecha.php');
		
	$baseDatos=new BD();
	if(comprobarSession())
	{
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==7)
		{			
			$sql="SELECT mei_virmateria.numeromod FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
			$resultado=$baseDatos->ConsultarBD($sql);
			list($modulos)=mysql_fetch_row($resultado);
			
			if ($_SESSION['idtipousuario']==5)
				$num_mod = $modulos;	
			else
				$num_mod = 18;	
			
			if(empty($_GET['periodo']))
				$_GET['periodo'] = 6;
			
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="css_epoch.css" rel="stylesheet" type="text/css">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="epoch_classes_mod.js"></script>
     <script language="javascript" src="lib.srv.js"></script>
    <script language="javascript">
		window.onload = function ()
		{
<?php
			for($i=0; $i < $num_mod ;$i++)
			{
?>
				cal_fecha_inicial<?php print $i?>  = new Epoch('fecha_inicial<?php print $i?>','popup',document.getElementById('txt_fecha_inicial<?php print $i?>'));
				cal_fecha_final<?php print $i?>  = new Epoch('fecha_final<?php print $i?>','popup',document.getElementById('txt_fecha_final<?php print $i?>'));
<?php
			}
?>
		};
    </script>
	<script language="javascript">
		function enviarCancelar()
		{
			location.replace("../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>");
		}
		
		function cambiar_periodo()
		{
			var periodo = parseInt(document.getElementById('txt_periodo').value);
			
			if(!isNaN(periodo) && periodo > 0)
			{
				location.replace("<?php print $_SERVER['PHP_SELF']?>?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&periodo="+periodo);	
			}
			else
			{
				alert("El numero de periodo que ha ingresado no es válido.");	
			}
		}
		function cambiar_periodo2()
		{
			var periodo = parseInt(document.getElementById('txt_periodo2').value);
			
			if(!isNaN(periodo) && periodo > 0)
			{
				location.replace("<?php print $_SERVER['PHP_SELF']?>?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&periodo="+periodo);	
			}
			else
			{
				alert("El numero de periodo que ha ingresado no es válido.");	
			}
		}
		function actualizar(indice)
		{
			var ln = parseInt('<?php print $num_mod?>');
			var periodo = parseInt('<?php print $_GET['periodo']?>');
			
			var str_get = "";
			
			for(i = 0; i < ln; i++)
			{
				str_get += "&txt_fecha_inicial" + i + "=" + document.getElementById('txt_fecha_inicial' + i).value + "&txt_fecha_final" + i + "=" + document.getElementById('txt_fecha_final' + i).value;	
			}
			str_get += "&indice=" + indice;
			str_get += "&periodo=" + periodo;
			str_get += "&longitud=" + ln;
			
			var respuesta = srvGetServer("../planificador/srv_fecha.php?", str_get);

			var array_respuesta = respuesta.split('[&&]');
			
			
			ln = array_respuesta.length;
			
			for(i = 0; i<ln; i++)
			{
				array_fecha = array_respuesta[i].split('[&]');
				
				document.getElementById('txt_fecha_inicial' + i).value = array_fecha[0];
				document.getElementById('txt_fecha_final' + i).value = array_fecha[1];
			}
		}
		
		function enviarGuardar()
		{
			if(confirm("¿Está seguro que desea guardar la información?"))
			{
				document.getElementById('frm_planificacion').submit();
			}
		}

		function eliminar() 
		{
			if(confirm("¿Está seguro de eliminar la planificación?"))
			
			(confirm("!Perdera todo la información de la planificación¡"))
			
				location.replace("../planificador/eliminarPlanificador.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>");
		}
	</script>  
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
			<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
			<td class="tablaEspacio">&nbsp;</td>
			<td>
				<table class="tablaMarquesina" >
					<tr>
						<td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($_GET["materia"]))?></a><a> -> Planificacion </a></td>
					</tr>
				</table><br>
				<form  name="frm_planificacion" id="frm_planificacion" method="post" action="guardarAutoPlanificador.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>">
<?
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin 
					FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplavirgru.idplanificador
					FROM mei_relplavirgru WHERE mei_relplavirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo 
					WHERE mei_virgrupo.idvirmateria =".$_GET['idmateria']." AND mei_virgrupo.idvirgrupo
					IN ( SELECT mei_relusuvirgru.idvirgrupo  FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."))) 
					ORDER BY mei_planificador.numeromod";
			}
			else 
			{
					$sql="SELECT mei_planificador.idplanificador, mei_planificador.numeromod, mei_planificador.fechainicio, mei_planificador.fechafin
						FROM mei_planificador WHERE mei_planificador.idplanificador IN (SELECT mei_relplagru.idplanificador
						FROM mei_relplagru WHERE mei_relplagru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo 
						WHERE mei_grupo.idmateria =".$_GET['idmateria']." AND mei_grupo.idgrupo
						IN ( SELECT mei_relusugru.idgrupo  FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")))
						ORDER BY mei_planificador.numeromod";
			}
			$resultado=$baseDatos->ConsultarBD($sql);	
			$numPlanificador=mysql_num_rows($resultado);
			if($numPlanificador > 0)
			{
?>				
				<table class="tablaGeneral">
				  <tr>
				    <td colspan="5" class="trTitulo"><img src="imagen/planificador.png" alt="" width="16" height="16" align="texttop">&nbsp;<b> Planificaci&oacute;n de clase</b></td>
			      </tr>
				    <tr class="trInformacion" align="right">
				      <td colspan="2" align="left"><strong>&nbsp;Docente:
                          <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?>
				      </strong></td>
				       <td colspan="1" title="Descargar Planificador WORD" align="left"><a href="../planificador/exportarPlanificacionWord.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>" class="link"><b>Descargar </b><img src="imagen/exportarw.png" alt="" width="20" height="20" align="texttop">&nbsp;</a></td>
				      <td colspan="1" title="Exportar Planificador SQL" align="left"><a href="../planificador/exportarPlanificacion.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>" class="link"><b>Exportar </b><img src="imagen/exportar.png" alt="" width="20" height="20" align="texttop">&nbsp;</a></td>
			        </tr>
				    <tr class="trInformacion">
				      <td colspan="4">&nbsp;</td>
			        </tr>
				    <tr class="trInformacion">
				      <td colspan="2"><strong>&nbsp;N&uacute;mero de m&oacute;dulos: <?=$num_mod?> </strong>
				        <input name="hid_modulos" type="hidden" id="hid_modulos" value="<?php print $num_mod?>">
			          	<input name="hid_periodo" type="hidden" id="hid_periodo" value="<?php print $_GET['periodo']?>">
				      </td>
				      <td ><strong>Periodo de:&nbsp;
				          <input name="txt_periodo2" type="text" id="txt_periodo2" onChange="javascript:cambiar_periodo2()" value="<?php print $_GET['periodo']?>" size="2">
			          &nbsp;Dias</strong></td>
				      <td align="center"><input name="btn_eliminar" type="button" value="Eliminar" onClick="javascript: eliminar()"></td>
			        </tr>
				    <tr class="trInformacion">
				      <td colspan="4" >&nbsp;</td>
			        </tr>
				    <tr class="trInformacion" align="center">
				      <td><strong>&nbsp;M&oacute;dulo</strong></td>
				      <td ><strong>Fecha de inicio</strong></td>
				      <td ><strong>Fecha de finalizaci&oacute;n </strong></td>
				      <td >&nbsp;</td>
			        </tr>
<?
				$array_temp = array();
				while($assoc = mysql_fetch_assoc($resultado))
				{
					$array_temp[] = $assoc;
				}
				$indice_cont = 0;
				$fecha_inicial = $array_temp[0]['fechainicio'];
				for($i = 0; $i < $num_mod; $i ++)
				{
					$fecha_final = add_fecha($fecha_inicial, $_GET['periodo']);
?>
				    <tr class="trInformacion" align="center">
				      <td align="center"><strong> <?=$i+1 ?></strong></td>
				       <input name="hid_modulo<?php print $indice_cont?>" type="hidden" id="hid_modulo<?php print $indice_cont?>" value="<?=$idplanificador=$array_temp[$i]['idplanificador'];?>">
				      <td>
                      	<table border="0" cellspacing="0" cellpadding="0">
				        <tr>
					        <td ><input name="txt_fecha_inicial<?php print $indice_cont?>" type="text" class="cajaFecha" id="txt_fecha_inicial<?php print $indice_cont?>" size="10" readonly="" value="<?php print $fecha_inicial?>" /></td>
				          	<td><img src="imagen/b_calendario.gif" alt="" width="16" height="16" title="Selector de fechas" onClick="cal_fecha_inicial<?php print $indice_cont?>.toggle()" /></td>
			            </tr>
			          </table>
                      </td>
				      <td>
                      <table border="0" cellspacing="0" cellpadding="0">
				        <tr>
				          <td ><input name="txt_fecha_final<?php print $indice_cont?>" type="text" class="cajaFecha" id="txt_fecha_final<?php print $indice_cont?>" size="10" readonly="" value="<?php print $fecha_final?>" /></td>
				          <td><img src="imagen/b_calendario.gif" alt="" width="16" height="16" title="Selector de fechas" onClick="cal_fecha_final<?php print $indice_cont?>.toggle()" /></td>
			            </tr>
			          </table>
                      </td>
				      <td align="left" ><a href="javascript:actualizar('<?php print $indice_cont?>')">Actualizar</a></td>
			        </tr>
<?
					$fecha_inicial = add_fecha($fecha_final, 1);
					$indice_cont++;
				}
?>
				    <tr class="trInformacion">
				      <td colspan="4" align="center"><input name="btn_crearPlanificador" type="button" id="btn_crearPlanificador" value="Guardar" onClick="javascript: enviarGuardar()">
				        <input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></td>
			        </tr>
			  </table>
<?                
			}
			else
			{
?>
				<table class="tablaGeneral">
				  <tr>
				    <td colspan="5" class="trTitulo"><img src="imagen/planificador.png" alt="" width="16" height="16" align="texttop">&nbsp;<b> Planificaci&oacute;n de clase</b></td>
			      </tr>
				    <tr class="trInformacion" align="right">
				      <td colspan="3" align="left"><strong>&nbsp;Docente:
                          <?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?>
				      </strong></td>
				      <td title="Importar Planificación"><a href="../planificador/importarPlanificacion.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>" class="link"><b>Importar </b><img src="imagen/importar.png" alt="" width="20" height="20" align="texttop">&nbsp;</a></td>
			        </tr>
				    <tr class="trInformacion">
				      <td colspan="4">&nbsp;</td>
			        </tr>
				    <tr class="trInformacion">
				      <td colspan="2"><strong>&nbsp;N&uacute;mero de m&oacute;dulos: <?=$num_mod?> </strong>
				        <input name="hid_modulos" type="hidden" id="hid_modulos" value="<?php print $num_mod?>">
			          	<input name="hid_periodo" type="hidden" id="hid_periodo" value="<?php print $_GET['periodo']?>">
				      </td>
				      <td colspan="2" ><strong>Periodo de:&nbsp;
				        <input name="txt_periodo" type="text" id="txt_periodo" onChange="javascript: cambiar_periodo()" value="<?php print $_GET['periodo']?>" size="2">
				        &nbsp;Dias</strong></td>
			        </tr>
				    <tr class="trInformacion">
				      <td colspan="4">&nbsp;</td>
			        </tr>
				    <tr class="trInformacion" align="center">
				      <td ><strong>&nbsp;M&oacute;dulo</strong></td>
				      <td ><strong>Fecha de inicio</strong></td>
				      <td ><strong>Fecha de finalizaci&oacute;n</strong></td>
				      <td>&nbsp;</td>
			        </tr>
<?
		$fecha_inicial = date("Y-m-d");
		for($i = 0; $i < $num_mod; $i ++)
		{
			$fecha_final = add_fecha($fecha_inicial, $_GET['periodo']);
?>
				    <tr class="trInformacion" align="center">
				      <td align="center"><strong> <?=$i+1?></strong></td>
				      <td>
                      	<table border="0" cellspacing="0" cellpadding="0">
				        <tr>
					        <td ><input name="txt_fecha_inicial<? print $i?>" type="text" class="cajaFecha" id="txt_fecha_inicial<? print $i?>" size="10" readonly="" value="<?php print $fecha_inicial?>" /></td>
				          	<td><img src="imagen/b_calendario.gif" alt="" width="16" height="16" title="Selector de fechas" onClick="cal_fecha_inicial<? print $i?>.toggle()" /></td>
			            </tr>
			          </table>
                      </td>
				      <td>
                      <table  border="0" cellspacing="0" cellpadding="0">
				        <tr>
				          <td><input name="txt_fecha_final<? print $i?>" type="text" class="cajaFecha" id="txt_fecha_final<? print $i?>" size="10" readonly="" value="<?php print $fecha_final?>" /></td>
				          <td><img src="imagen/b_calendario.gif" alt="" width="16" height="16" title="Selector de fechas" onClick="cal_fecha_final<? print $i?>.toggle()" /></td>
			            </tr>
			          </table>
                      </td>
				      <td align="left" colspan="2"><a href="javascript:actualizar('<?php print $i?>')">Actualizar</a></td>
			        </tr>
				    <?
					
					$fecha_inicial = add_fecha($fecha_final, 1);
		}
?>
				    <tr class="trInformacion">
				      <td colspan="4" align="center"><input name="btn_crearPlanificador" type="button" id="btn_crearPlanificador" value="Guardar" onClick="javascript: enviarGuardar()">
				        <input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></td>
			        </tr>
			  </table>
<?
			}
?>   
              </form>
             </td>
  			<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
			<td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<? 
		}
	}
	else
		redireccionar('../login/');
?>