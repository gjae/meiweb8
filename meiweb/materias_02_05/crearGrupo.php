<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../librerias/vistas.lib.php');
	
	$baseDatos=new BD();
	 
	if(comprobarSession())
	{
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		if(!empty($_GET['idMateria']))
		{
			$sql="SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idMateria'];
			$consultaMateria=$baseDatos->ConsultarBD($sql);
			
			list($nombreMateria)=mysql_fetch_array($consultaMateria);
		?>
			<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">				
				<script language="javascript">
				
					function enviarCrear()
					{
						if(comprobarDatos())
						{
							document.frm_crearGrupo.submit();
						}
					}
					
					function enviarCancelar()
					{
						location.replace("verMateriasAdministrador.php");
					}
				
					function comprobarDatos()
					{
						if(document.frm_crearGrupo.txt_codigoGrupo.value == false)
						{
							alert("Debe llenar completamente la información solicitada");
						}
						else if(document.frm_crearGrupo.txt_ubicacionGrupo.value == false)
						{
							alert("Debe llenar completamente la información solicitada");
						}						
						else
						{
							return true;
						}
					}
					
					function comprobarContenido()
					{
						if(document.frm_crearGrupo.txt_horarioGrupo.value != "")
						{
							document.frm_crearGrupo.btn_borrarHorario.disabled=false;
						}
						else
						{
							document.frm_crearGrupo.btn_borrarHorario.disabled=true;
						}
					}
					
					function borrarContenido()
					{
						document.frm_crearGrupo.txt_horarioGrupo.value="";
						document.frm_crearGrupo.btn_borrarHorario.disabled=true;
					}
					
					function validarHorario()
					{
						if(parseInt(document.frm_crearGrupo.cbo_diaSemana.value) == 0 || parseInt(document.frm_crearGrupo.cbo_horaInicio.value) == 0 ||parseInt( document.frm_crearGrupo.cbo_horaFin.value == 0))
						{
							document.frm_crearGrupo.btn_agregarHorario.disabled=true;						
						}
						else
						{
							if(parseInt(document.frm_crearGrupo.cbo_horaInicio.value) < parseInt(document.frm_crearGrupo.cbo_horaFin.value))
							{
								document.frm_crearGrupo.btn_agregarHorario.disabled=false;
							}
							else
							{
								document.frm_crearGrupo.btn_agregarHorario.disabled=true;
							}
						}
					}
					
					function retornarHora(hora24)
					{
						if(parseInt(hora24) <= 12)
						{
							hora12=hora24+":00 am";
						}
						else
						{
							hora12=(parseInt(hora24)-12)+":00 pm";
						}
						return hora12;
					}
					
					function inicializarHorario()
					{
						document.frm_crearGrupo.cbo_diaSemana.value=0;
						document.frm_crearGrupo.cbo_horaInicio.value=0;
						document.frm_crearGrupo.cbo_horaFin.value=0;
						document.frm_crearGrupo.btn_agregarHorario.disabled=true;
					}					
										
					function agregarHorario()
					{
						var txtDia="";
						
						switch(parseInt(document.frm_crearGrupo.cbo_diaSemana.value))
						{
							case 1:
							{
								txtDia="Lunes";
							}
							break;
							
							case 2:
							{
								txtDia="Martes";
							}
							break;
							
							case 3:
							{
								txtDia="Miercoles";
							}
							break;
							
							case 4:
							{
								txtDia="Jueves";
							}
							break;
							
							case 5:
							{
								txtDia="Viernes";
							}
							break;
							
							case 6:
							{
								txtDia="Sabado";
							}
							break;				
						}												
					
						if(document.frm_crearGrupo.txt_horarioGrupo.value==false)
						{
							document.frm_crearGrupo.txt_horarioGrupo.value=txtDia+" de "+retornarHora(document.frm_crearGrupo.cbo_horaInicio.value)+" a "+retornarHora(document.frm_crearGrupo.cbo_horaFin.value);
						}
						else
						{
							document.frm_crearGrupo.txt_horarioGrupo.value+=",   "+txtDia+" de "+retornarHora(document.frm_crearGrupo.cbo_horaInicio.value)+" a "+retornarHora(document.frm_crearGrupo.cbo_horaFin.value);
						}
						
						comprobarContenido();
						inicializarHorario();											
					}
					
					
								
				</script>
				
				<style type="text/css">
				<!--
				.Estilo1 {color: #FF0000}
				-->
				</style>
				
				</head>
				<body>
				
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu(1);?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral">
							<form action="insertarGrupo.php?idMateria=<?= $_GET['idMateria']?>" method="post" name="frm_crearGrupo">
								<tr class="trTitulo">
									<td colspan="3" class="trTitulo">Crear nuevo Grupo en <?= $nombreMateria?> </td>
								</tr>
				<?
					if($_GET['error'] == '0x001')
					{
				?>
								<tr class="trAviso">
									<td width="3%"><img src="imagenes/error.gif" width="17" height="18"></td>
									<td width="97%" colspan="2"><span class="Estilo1">Error: Ya existe un grupo registrado con ese c&oacute;digo </span></td>
								</tr>
				<?
					}
				?>
					<tr class="trInformacion">
						<td width="25%" colspan="2"><b>C&oacute;digo del Grupo :</b></td>
						<td width="75%"><input name="txt_codigoGrupo" type="text" id="txt_codigoGrupo" value="<?= $_GET['codigoGrupo']?>" size="25"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Ubicaci&oacute;n:</b></td>
						<td><input name="txt_ubicacionGrupo" type="text" id="txt_ubicacionGrupo" value="<?= $_GET['ubicacionGrupo']?>" size="50"></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="2"><b>Horario:</b></td>
						<td><input name="txt_horarioGrupo" type="text" value="<?= $_GET['horarioGrupo']?>" size="100" readonly>
						  <table width="100%" class="tablaPrincipal">
                            <tr>
                              <td width="11%"><select name="cbo_diaSemana" onChange="javascript:validarHorario()">
                              <option value="0">Dia</option>
                              <option value="1">Lunes</option>
                              <option value="2">Martes</option>
                              <option value="3">Miercoles</option>
                              <option value="4">Jueves</option>
                              <option value="5">Viernes</option>
                              <option value="6">Sabado</option>
                                </select></td>
                              <td width="12%"> <select name="cbo_horaInicio" onChange="javascript:validarHorario()">
                                <option value="0">hora inicio</option>
                                <option value="6">6:00 am</option>
                                <option value="7">7:00 am</option>
                                <option value="8">8:00 am</option>
                                <option value="9">9:00 am</option>
                                <option value="10">10:00 am</option>
                                <option value="11">11:00 am</option>
                                <option value="12">12:00 pm</option>
                                <option value="13">1:00 pm</option>
                                <option value="14">2:00 pm</option>
                                <option value="15">3:00 pm</option>
                                <option value="16">4:00 pm</option>
                                <option value="17">5:00 pm</option>
                                <option value="18">6:00 pm</option>
                                <option value="19">7:00 pm</option>
                                <option value="20">8:00 pm</option>
                                <option value="21">9:00 pm</option>
                                <option value="22">10:00 pm</option>
                              </select></td>
                              <td width="38%"><select name="cbo_horaFin" onChange="javascript:validarHorario()">
                                <option value="0" selected>hora finalizacion</option>
                                <option value="6">6:00 am</option>
                                <option value="7">7:00 am</option>
                                <option value="8">8:00 am</option>
                                <option value="9">9:00 am</option>
                                <option value="10">10:00 am</option>
                                <option value="11">11:00 am</option>
                                <option value="12">12:00 pm</option>
                                <option value="13">1:00 pm</option>
                                <option value="14">2:00 pm</option>
                                <option value="15">3:00 pm</option>
                                <option value="16">4:00 pm</option>
                                <option value="17">5:00 pm</option>
                                <option value="18">6:00 pm</option>
                                <option value="19">7:00 pm</option>
                                <option value="20">8:00 pm</option>
                                <option value="21">9:00 pm</option>
                                <option value="22">10:00 pm</option>
                              </select></td>
                              <td width="16%"><input name="btn_borrarHorario" type="button" value="Borrar horario" onClick="javascript:borrarContenido()" disabled></td>
                              <td width="23%"><input name="btn_agregarHorario" type="button" value="Agregar horario"  onClick="javascript:agregarHorario()" disabled></td>
                            </tr>
                          </table></td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td colspan="3"><div align="center">	<input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">
						<input name="btn_crearGrupo" type="button" id="btn_crearGrupo" value="Crear Grupo" onClick="javascript: enviarCrear();">
						<input name="btn_cancelar" type="button" value="Cancelar" onClick="javascript: enviarCancelar()"></div></td>
				  </tr>
						  </form>
				  
				</table>
			  </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho">&nbsp;</td>            
                <td class="tablaEspacioDerecho">&nbsp;</td>              
					</tr>
				</table>
				</body>
			</html>
		<? 
		}
		else
		{
			redireccionar('../materias/verMateriasAdministrador.php');			
		}
	}
	else
	{
		redireccionar('../login/');
	}
	}
	else
	{
		redireccionar('../login/');
	}
	
?>
