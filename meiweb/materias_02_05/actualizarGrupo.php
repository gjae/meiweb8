<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	
	$baseDatos=new BD();
	
	if(comprobarSession())
	{
		if(isset($_POST['txt_codigoGrupo']) && isset($_POST['txt_ubicacionGrupo']) && isset($_POST['hid_idGrupo']))
		{
			if(empty($_POST['txt_horarioGrupo']))
			{
				$horarioGrupo="Sin horario";
			}
			else
			{
				$horarioGrupo=$_POST['txt_horarioGrupo'];
			}
			$sql="SELECT mei_grupo.idgrupo FROM mei_grupo 
					WHERE mei_grupo.idmateria =".$_POST['hid_idMateria']." 
						AND mei_grupo.nombre='".strtoupper($_POST['txt_codigoGrupo'])."'";
			$consultaGrupo=$baseDatos->ConsultarBD($sql);
						
			$numGrupos=mysql_num_rows($consultaGrupo);
			if($numGrupos < 1 || $_POST['txt_codigoGrupo']==$_POST['hid_grupoAnterior'])
			{ 
			
						$sql="UPDATE mei_grupo 
								SET nombre = '".strtoupper($_POST['txt_codigoGrupo'])."',
									ubicacion = '".strtoupper($_POST['txt_ubicacionGrupo'])."',
									horario = '".$horarioGrupo."' 
										WHERE idgrupo=".$_POST['hid_idGrupo'];
									
					$baseDatos->ConsultarBD($sql);
					if(!empty($_POST['hid_idProfesorAnterior']) && !empty($_POST['cbo_profesor']))
					{
						$sql="UPDATE mei_relusugru 
								SET idusuario = '".$_POST['cbo_profesor']."' 
									WHERE idusuario =".$_POST['hid_idProfesorAnterior']." AND idgrupo =".$_POST['hid_idGrupo'];	
		
					}
					else if(!empty($_POST['hid_idProfesorAnterior']) && empty($_POST['cbo_profesor']))
					{
						$sql="DELETE FROM mei_relusugru WHERE idusuario = ".$_POST['hid_idProfesorAnterior']." AND idgrupo =".$_POST['hid_idGrupo'];
		
					}
					else if(empty($_POST['hid_idProfesorAnterior']) && !empty($_POST['cbo_profesor']))
					{
						$sql="INSERT INTO mei_relusugru (idusuario,idgrupo) 
								VALUES ('".$_POST['cbo_profesor']."', '".$_POST['hid_idGrupo']."') ";
						
					}
					
					$baseDatos->ConsultarBD($sql);
					
					// Borrado de mensajes en cartelera de los usuarios sin relacion en los grupos.					
					
					$sql="DELETE FROM mei_cartelera WHERE mei_cartelera.idusuario 
						IN (
							SELECT mei_usuario.idusuario FROM mei_usuario
								WHERE mei_usuario.idusuario NOT 
									IN (
											SELECT DISTINCT mei_relusugru.idusuario FROM mei_relusugru
										)
							) ";
					$baseDatos->ConsultarBD($sql);
							
					$sql="DELETE FROM mei_calendario WHERE mei_calendario.idusuario 
						IN (
							SELECT mei_usuario.idusuario FROM mei_usuario
								WHERE mei_usuario.idusuario NOT 
									IN (
											SELECT DISTINCT mei_relusugru.idusuario FROM mei_relusugru
										)
							) ";
					$baseDatos->ConsultarBD($sql);
							
					$sql="DELETE FROM mei_salachat WHERE mei_salachat.idusuario 
						IN (
							SELECT mei_usuario.idusuario FROM mei_usuario
								WHERE mei_usuario.idusuario NOT 
									IN (
											SELECT DISTINCT mei_relusugru.idusuario FROM mei_relusugru
										)
							) ";
					$baseDatos->ConsultarBD($sql);
					
			
			if($_GET['nuevoProfesor'])
			{
				redireccionar("../usuario/crearProfesor.php?idGrupo=".$_POST['hid_idGrupo']);
			}
			else
			{
				redireccionar("../materias/verMateriasAdministrador.php");
			}
			
			}
			else
			{
				redireccionar("../materias/modificarGrupo.php?error=0x001&idGrupo=".$_POST['hid_idGrupo']."&grupoError=".$_POST['txt_codigoGrupo']);
			}
		
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>
