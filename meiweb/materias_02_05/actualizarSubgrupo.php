<?
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');

	$baseDatos=new BD();

	if(comprobarSession())
	{
		if(!empty($_GET['idGrupo']) && !empty($_POST['hid_idSubGrupo']) && !empty($_POST['hid_listaValores']) && !empty($_POST['txt_nombreSubgrupo']))
		{
			$listaSeleccionados=explode(";",$_POST['hid_listaValores']);
			$listaCodigosBusqueda=implode(",",$listaSeleccionados);

			$sql="SELECT mei_relususub.idusuario
					FROM mei_relususub
					WHERE mei_relususub.idsubgrupo <> ".$_POST['hid_idSubGrupo']." AND mei_relususub.idsubgrupo
					IN (
						SELECT mei_subgrupo.idsubgrupo
						FROM mei_subgrupo
						WHERE mei_subgrupo.idtiposubgrupo=".$_POST['cbo_tipoSubgrupo']."
						AND mei_subgrupo.idgrupo =".$_GET['idGrupo']."
					)
					AND mei_relususub.idusuario
					IN (".$listaCodigosBusqueda.")";

			$consultaAlSubGrupo=$baseDatos->ConsultarBD($sql);

			$numAlSubGrupo=mysql_num_rows($consultaAlSubGrupo);


			while(list($codigoAlumnoTemp)=mysql_fetch_array($consultaAlSubGrupo))
			{
				if(in_array($codigoAlumnoTemp,$listaSeleccionados))
				{
					$listaErrores.="[$$$]".$codigoAlumnoTemp;
				}
			}


			$sql="SELECT mei_subgrupo.nombre FROM mei_subgrupo WHERE mei_subgrupo.idtiposubgrupo= '".$_POST['cbo_tipoSubgrupo']."' AND mei_subgrupo.idsubgrupo <> '".$_POST['hid_idSubGrupo']."' AND mei_subgrupo.idgrupo='".$_GET['idGrupo']."' AND mei_subgrupo.nombre='".strtoupper($_POST['txt_nombreSubgrupo'])."'";

			$consultaSubGrupo=$baseDatos->ConsultarBD($sql);

			$numSubGrupo=mysql_num_rows($consultaSubGrupo);


			if(empty($numSubGrupo) && empty($numAlSubGrupo))
			{

				$sql="UPDATE mei_subgrupo SET mei_subgrupo.nombre = '".strtoupper($_POST['txt_nombreSubgrupo'])."', mei_subgrupo.idtiposubgrupo='".$_POST['cbo_tipoSubgrupo']."',
						mei_subgrupo.descripcion = '".$_POST['txt_descripcionSubGrupo']."' WHERE mei_subgrupo.idsubgrupo ='".$_POST['hid_idSubGrupo']."' ";

				$baseDatos->ConsultarBD($sql);




				$listaSeleccionados=explode(";",$_POST['hid_listaValores']);

				foreach($listaSeleccionados as $idUsuarioSeleccionado)
				{
					if(empty($listaValores))
					{
						$sql="SELECT mei_relususub.lider FROM mei_relususub WHERE mei_relususub.idsubgrupo = '".$_POST['hid_idSubGrupo']."'
							 AND mei_relususub.idusuario = '".$idUsuarioSeleccionado."'";

						$consultaLider= $baseDatos->ConsultarBD($sql);
						list($lider)=mysql_fetch_array($consultaLider);

						if ($lider == NULL)
							$listaValores="('".$idUsuarioSeleccionado."','".$_POST['hid_idSubGrupo']."',0)";
						else
						    $listaValores="('".$idUsuarioSeleccionado."','".$_POST['hid_idSubGrupo']."',$lider)";
					}
					else
					{
						$sql="SELECT mei_relususub.lider FROM mei_relususub WHERE mei_relususub.idsubgrupo = '".$_POST['hid_idSubGrupo']."'
							 AND mei_relususub.idusuario = '".$idUsuarioSeleccionado."'";

						$consultaLider= $baseDatos->ConsultarBD($sql);
						list($lider)=mysql_fetch_array($consultaLider);

						if ($lider == NULL)
							$listaValores.=",('".$idUsuarioSeleccionado."','".$_POST['hid_idSubGrupo']."',0)";
						else
						    $listaValores.=",('".$idUsuarioSeleccionado."','".$_POST['hid_idSubGrupo']."',$lider)";
					}
				}

				$sql="DELETE FROM mei_relususub WHERE idsubgrupo = '".$_POST['hid_idSubGrupo']."'";

				$baseDatos->ConsultarBD($sql);

				$sql="INSERT INTO mei_relususub ( idusuario , idsubgrupo , lider)
						VALUES ".$listaValores;

				$baseDatos->ConsultarBD($sql);


				redireccionar("../materias/listarSubgrupos.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idTipoSubgrupo=".$_POST['cbo_tipoSubgrupo']."&idGrupo=".$_GET['idGrupo']);

			}
			else
			{

				if(!empty($numSubGrupo) && !empty($numAlSubGrupo))
				{
					$error="0x003";
				}
				else if(!empty($numSubGrupo))
				{
					$error="0x001";
				}
				else if(!empty($numAlSubGrupo))
				{
					$error="0x002";
				}
				redireccionar("../materias/modificarSubgrupo.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idTipoSubgrupo=".$_POST['cbo_tipoSubgrupo']."&idSubGrupo=".$_POST['hid_idSubGrupo']."&idGrupo=".$_GET['idGrupo']."&error=".$error."&listaValores=".$_POST['hid_listaValores']."&nombreSubgrupo=".strtoupper($_POST['txt_nombreSubgrupo'])."&descripcionSubGrupo=".$_POST['txt_descripcionSubGrupo']."&listaErrores=".$listaErrores."&idTipoSubgrupo=".$_POST['cbo_tipoSubgrupo']);
				//redireccionar("../materias/crearSubgrupo.php?destino=".$_GET['destino']."&idGrupo=".$_GET['idGrupo']."&error=".$error."&listaErrores=".$listaErrores."&listaValores=".$_POST['hid_listaValores']."&nombreSubgrupo=".strtoupper($_POST['txt_nombreSubgrupo'])."&descripcionSubGrupo=".$_POST['txt_descripcionSubGrupo']."&idTipoSubgrupo=".$_POST['cbo_tipoSubgrupo']);

			}

		}
		else
		{
			if ($_SESSION['idtipousuario']==5)
				redireccionar("../usuario/VirListarProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);		
			else
				redireccionar("../usuario/listarProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);		
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>