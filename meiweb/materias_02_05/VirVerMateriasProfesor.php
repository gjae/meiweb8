<?
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');
	
	$baseDatos=new BD();
if(comprobarSession())
{
	if ($_SESSION['idtipousuario']==5)
	{
		$sql="SELECT DISTINCT(mei_virmateria.idvirmateria),mei_virmateria.nombre,mei_virmateria.nombrecorto,mei_virmateria.codigo,mei_virmateria.codigosec,
				mei_virmateria.formato, mei_virmateria.numeromod, mei_virmateria.fechainicio, mei_virmateria.fechafin, mei_virmateria.requisitos,
				mei_virmateria.introduccion, mei_virmateria.ofertar FROM mei_virgrupo, mei_virmateria 
				WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo 
				FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")
				AND mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria";
		$consulta=$baseDatos->ConsultarBD($sql);
		$numeroMaterias=mysql_num_rows($consulta);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>MEIWEB</title>
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">					
</head>
<body>
	<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><?=menu($_SESSION['idtipousuario']); ?>	</td>
            <td class="tablaEspacio">&nbsp;</td>
			<td>
				<table class="tablaGeneral">
					<tr class="trTitulo">
						<td colspan="4" class="trTitulo"><img src="imagenes/materias.gif" width="16" height="16" align="texttop"> Ver Cursos</td>
					</tr>
				</table><br>
<?
		if(!empty($numeroMaterias))
		{					
			while(list($idMateria,$nombreMateria,$nombreCorto,$codigoMateria,$codigoMateriasec,$formato,$modulos,$fecha_ini,$fecha_fin,$requisitos,$introduccion,$ofertar)=mysql_fetch_array($consulta))
		{
			$sql="SELECT mei_virgrupo.idvirgrupo,mei_virgrupo.nombre
					FROM mei_virgrupo, mei_virmateria WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo 
					FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")
					AND mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria AND mei_virmateria.idvirmateria=".$idMateria;
			$consultaGrupos=$baseDatos->ConsultarBD($sql);
			$numeroGrupos=mysql_num_rows($consultaGrupos);
?>
				<table class="tablaGeneral" >
					<tr class="trInformacion">
						<td width="15%"><b>Curso:</b></td>
						<td colspan="2"><b><?= $nombreMateria?></b></td>
<?	 			if($ofertar==1)
				{
?>
						<td width="23%"><b>Ofertado</b></td>
<?
				}
				else
				{
?>				
						<td width="16%"><b>Sin Ofertar</b></td>
<?
				}
?>				
					</tr>
					<tr class="trInformacion">
						<td><b>Nombre Corto:</b></td>
						<td colspan="2"><?= $nombreCorto?></td>
						<td>&nbsp;</td>
					</tr>
					<tr class="trInformacion">
						<td><b>C&oacute;digo:</b></td>
						<td width="22%"><?= $codigoMateria?></td>
						<td width="20%"><b>C&oacute;digo Secundario:</b></td>
						<td><?= $codigoMateriasec?></td>
					</tr>
					<tr class="trInformacion">
						<td><b>Formato:</b></td>
						<td><?= $formato ?></td>
						<td><b>N&deg; Modulos:</b></td>
						<td><?= $modulos?></td>
					</tr>
					<tr class="trInformacion">
						<td><b>Fecha inicio:</b></td>
						<td><?= $fecha_ini ?></td>
						<td><b>Fecha finalizaci&oacute;n:</b></td>
						<td><?= $fecha_fin ?></td>
					</tr>
					<tr class="trAviso">
					  <td><b>Requisitos:</b></td>
					  <td colspan="3" class="trListaClaro"><?= $requisitos?></td>
				 	</tr>
					<tr class="trAviso">
					  <td><b>Introducci&oacute;n:</b></td>
					  <td colspan="3" class="trListaClaro"><?= $introduccion ?></td>
				 	 </tr>
                    
<?
			if(empty($numeroGrupos))
			{
?>
					<tr class="trAviso">
						<td colspan="4" align="center">Actualmente no existen Grupos </td>
					</tr>
<?
			}
			else
			{
?>
					<tr class="trInformacion">
					  <td  colspan="4" align="center" class="trInformacion">&nbsp;</td>
				  </tr>
					<tr class="trSubTitulo">
						<td  colspan="2" class="trSubTitulo" align="center">Grupo</td>
						<td colspan="2" class="trSubTitulo" align="center">N° Estudiantes</td>
					</tr>
<?
				while(list($idGrupo,$nombreGrupo,$ubicacionGrupo,$horarioGrupo)=mysql_fetch_array($consultaGrupos))
				{
					$sql="SELECT  COUNT(mei_usuario.idusuario) FROM mei_usuario WHERE mei_usuario.idtipousuario=6 AND mei_usuario.idusuario IN (
SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo=".$idGrupo.")";					
					$resultado=$baseDatos->ConsultarBD($sql);
					list($numeroAlumnos)=mysql_fetch_array($resultado);
?>
					<tr class="trListaClaro">
						<td colspan="2" align="center" class="trListaClaro"><?= $nombreGrupo?></td>
						<td colspan="2" align="center" class="trListaClaro"><?= $numeroAlumnos?></td>
					</tr>
<?
				}
			}//
?>
				</table><br><br>
<?
		}
	}
?>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
			<td class="tablaDerecho"><?=menu1($_SESSION['idtipousuario']); ?>	</td>            
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
	}
	else
	{
		redireccionar('../login/');
	}
}
else
{
	redireccionar('../login/');
}
?>