<?
	include_once('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
	
	$baseDatos=new BD();
	
	if(comprobarSession())
	{
		/*Agregar Campo*/
		$exists = false;
		$db = "mei_materia";
		$column = "grupo";
		$column_attr = "int(11) null";
		$columns = "show columns from $db";
		$resultcolumn = $baseDatos->ConsultarBD($resultcolumn);
		while($c = mysql_fetch_assoc($resultcolumn)){
			if($c['Field'] == $column){
				$exists = true;
				break;
			}
		}        
		if(!$exists){
			$sql = "ALTER TABLE `$db` ADD `$column`  $column_attr";
		}					
		$baseDatos->ConsultarBD($sql);		
		/*Fin Agregar Campo*/	
		
		if(isset($_POST['txt_nombreMateria']) && isset($_POST['txt_nombreCorto']) && isset($_POST['txt_codigoMateria']) && isset($_POST['txt_creditosMateria']))
		{
			$sql="SELECT count(mei_materia.idmateria) FROM mei_materia WHERE mei_materia.codigosec='".$_POST['txt_codigoMateriasec']."'";
			$consulta=$baseDatos->ConsultarBD($sql);
			
			list($numeroMaterias)=mysql_fetch_array($consulta);
			
			if(empty($numeroMaterias))
			{
				$sql="INSERT INTO mei_materia ( idmateria , nombre , nombrecorto , codigo , codigosec, creditos , fechacreacion , introduccion, grupo ) 
						VALUES ('', '".strtoupper($_POST['txt_nombreMateria'])."', '".strtoupper($_POST['txt_nombreCorto'])."', '".$_POST['txt_codigoMateria']."', '".$_POST['txt_codigoMateriasec']."', '".(int)$_POST['txt_creditosMateria']."', '".date("Y-n-j")."', '', '".$_POST['txt_grupo']."');";
				$baseDatos->ConsultarBD($sql);
											
				$idMateria=$baseDatos->InsertIdBD();				
				
				redireccionar("../materias/verMateriasAdministrador.php?idMateria=".$idMateria);
			}
			else
			{
				redireccionar("../materias/crearMateria.php?error=0x001&nombreMateria=".$_POST['txt_nombreMateria']."&nombreCorto=".$_POST['txt_nombreCorto']."&codigoMateria=".$_POST['txt_codigoMateria']."&codigoMateriasec=".$_POST['txt_codigoMateriasec']."&creditosMateria=".$_POST['txt_creditosMateria']);
			}
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>
