<?PHP 
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');

	$baseDatos=new BD();
	if(comprobarSession())
	{ 
		if($_SESSION['banderaAdmnistrador']==1)
		{
			$sql="SELECT DISTINCT mei_virmateria.idvirmateria,mei_virmateria.nombre FROM mei_virmateria ORDER BY (mei_virmateria.nombre)";
			$consulta=$baseDatos->ConsultarBD($sql);
			$numeroMaterias=mysql_num_rows($consulta);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?PHP  echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">    		
</head>
<body>
	<script language="javascript">
	function activarPresencial()
	{
		document.frm_reiniciar.btn_correo.disabled=false;
		document.frm_reiniciar.btn_actividades.disabled=false;
		document.frm_reiniciar.btn_foros.disabled=false;
		document.frm_reiniciar.btn_notas.disabled=false;
		document.frm_reiniciar.btn_limpiarp.disabled=false;
		
		document.frm_reiniciar.btn_correoVirtual.disabled=true;
		document.frm_reiniciar.btn_actividadesVirtual.disabled=true;
		document.frm_reiniciar.btn_forosVirtual.disabled=true;
		document.frm_reiniciar.btn_notasVirtual.disabled=true;
		document.frm_reiniciar.btn_limpiarv.disabled=true;
		
		//var radioTodos=document.getElementById('radioT');
		var radioSelec=document.getElementById('radioS');	
		//radioTodos.disabled=true;
		radioSelec.disabled=true;
		
		for(i=0;i<document.frm_reiniciar.elements.length;i++)
		{
			if(document.frm_reiniciar.elements[i].id=="grupo")
				document.frm_reiniciar.elements[i].disabled=true;
		}	
	}
	
	function activarVirtual()
	{
		document.frm_reiniciar.btn_correo.disabled=true;
		document.frm_reiniciar.btn_actividades.disabled=true;
		document.frm_reiniciar.btn_foros.disabled=true;
		document.frm_reiniciar.btn_notas.disabled=true;
		document.frm_reiniciar.btn_limpiarp.disabled=true;
		
		document.frm_reiniciar.btn_correoVirtual.disabled=false;
		document.frm_reiniciar.btn_actividadesVirtual.disabled=false;
		document.frm_reiniciar.btn_forosVirtual.disabled=false;
		document.frm_reiniciar.btn_notasVirtual.disabled=false;
		document.frm_reiniciar.btn_limpiarv.disabled=false;
		
		//var radioTodos=document.getElementById('radioT');
		var radioSelec=document.getElementById('radioS');	
		//radioTodos.disabled=false;
		radioSelec.disabled=false;
		
		for(i=0;i<document.frm_reiniciar.elements.length;i++)
		{
			if(document.frm_reiniciar.elements[i].id=="grupo")
				document.frm_reiniciar.elements[i].disabled=false;
		}	
	}
	
	function enviar(modulo)
	{
		if (modulo==1)
		{
			if (confirm("¿Desea eliminar los Correos de todos los usuarios de Educacion Presencial?"))
			{
				document.frm_reiniciar.action="reinicioSemestre.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}
		if (modulo==2)
		{
			if (confirm("¿Desea eliminar los Foros de todos los usuarios de Educacion Presencial?"))
			{
				document.frm_reiniciar.action="reinicioSemestre.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}
		if (modulo==3)
		{
			if (confirm("¿Desea eliminar las Actividades de todos los usuarios de Educacion Presencial?"))
			{
				document.frm_reiniciar.action="reinicioSemestre.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}
		if (modulo==4)
		{
			document.frm_reiniciar.action="reinicioSemestre.php?mod="+modulo;
			document.frm_reiniciar.submit();
		}
		if (modulo==5)
		{
			if (confirm("¿Desea eliminar los archivos huerfanos?"))
			{
				document.frm_reiniciar.action="../librerias/Limpiar.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}
		if (modulo==6)
		{
			if (confirm("¿Desea cargar materias y profesores desde DB division?"))
			{
				document.frm_reiniciar.action="cargaSemestre.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}        
	}
	
	function enviarVirtual(modulo)
	{
		if (modulo==1)
		{
			if (confirm("¿Desea eliminar los correos de todos los usuarios de Educacion Virtual?"))
			{
				document.frm_reiniciar.action="VirReinicioSemestre.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}
		if (modulo==2)
		{
			if (confirm("¿Desea eliminar los foros de los grupos selecciandos de Educacion Virtual?"))
			{
				document.frm_reiniciar.action="VirReinicioSemestre.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}
		if (modulo==3)
		{
			if (confirm("¿Desea eliminar las Actividades los grupos selecciandos de Educacion Virtual?"))
			{
				document.frm_reiniciar.action="VirReinicioSemestre.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}
		if (modulo==4)
		{
			if (confirm("¿Desea eliminar los usuarios de los grupos seleccionados con sus respectivas notas?"))
			{
				document.frm_reiniciar.action="VirReinicioSemestre.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}
		if (modulo==5)
		{
			if (confirm("¿Desea eliminar los archivos huerfanos?"))
			{
				document.frm_reiniciar.action="../librerias/Limpiar.php?mod="+modulo;
				document.frm_reiniciar.submit();
			}
		}
	}
	

	function chk_deshabilitar()
	{
		for(i=0;i<document.frm_reiniciar.elements.length;i++)
		{
			if(document.frm_reiniciar.elements[i].id=="grupo")
				document.frm_reiniciar.elements[i].checked=false;
		}
	}
	
	function chk_click ()
	{
		//var radioTodos=document.getElementById('radioT');
		var radioSelec=document.getElementById('radioS');	
//		radioTodos.checked=false;
		radioSelec.checked=true;	
	}
	</script> 
<table class="tablaPrincipal">
	<tr valign="top">
    	<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaIzquierdo"><?PHP  echo menu(1);?></td>
        <td class="tablaEspacio">&nbsp;</td>
		<td><form name="frm_reiniciar" id="frm_reiniciar" method="post">
        	<table class="tablaGeneral">
				<tr class="trTitulo">
					<td class="trTitulo"><img src="imagenes/reiniciar.gif" width="16" height="16" align="texttop">&nbsp;Reinicio de M&oacute;dulos</td>
				</tr>
                <tr class="trInformacion">
                	<td>&nbsp;</td>
                </tr>
                <tr class="trSubTitulo">
					<td class="trSubTitulo">&nbsp;
						<input name="rad_reinicio" type="radio" value="1" onClick="javascript: activarPresencial()" checked>
						Reiniciar Educaci&oacute;n Presencial</td>
				</tr>
                <tr class="trInformacion">
                	<td align="center">
                    	<input name="btn_correo" type="button" id="btn_correo" value="Correo" onClick="javascript: enviar(1)">&nbsp;                	  
                    	<input name="btn_foros" type="button" id="btn_foros" value="Foros" onClick="javascript: enviar(2)">&nbsp;
                        <input name="btn_actividades" type="button" id="btn_actividades" value="Actividades" onClick="javascript: enviar(3)">&nbsp; 
                        <input name="btn_notas" type="button" id="btn_notas" value="Notas" onClick="javascript: enviar(4)">&nbsp;
						<input name="btn_limpiarp" type="button" id="btn_limpiarp" value="limpiar" onclick="javascript: enviar(5)" >&nbsp;
                        <input name="btn_carga_masiva" type="button" id="btn_cargaacademica" value="Carga Academica" title="Cargar Nuevos Profesores y materias" onclick="javascript: enviar(6)" >&nbsp;
                    </td>
           	    </tr>
                <tr class="trInformacion">
                	<td>&nbsp;</td>
                </tr>
                <tr class="trSubTitulo">
					<td class="trSubTitulo">&nbsp;
						<input name="rad_reinicio" type="radio" value="2" onClick="javascript: activarVirtual()">
						Reiniciar Educaci&oacute;n Virtual</td>
				</tr>
<?PHP                 
		if(empty($numeroMaterias))
		{
?>	
                <tr class="trSubTitulo">
					<td class="trSubTitulo">Actualmente no existen Cursos</td>
				</tr>
<?PHP 
		}
		else
		{
?>	
                <tr class="trInformacion">
                	<td align="center">
                    	<input name="btn_correoVirtual" type="button" id="btn_correoVirtual" value="Correo" onClick="javascript: enviarVirtual(1)" disabled>&nbsp;                	  
                    	<input name="btn_forosVirtual" type="button" id="btn_forosVirtual" value="Foros" onClick="javascript: enviarVirtual(2)" disabled>&nbsp;
                        <input name="btn_actividadesVirtual" type="button" id="btn_actividadesVirtual" value="Actividades" onClick="javascript: enviarVirtual(3)" disabled>&nbsp; 
                        <input name="btn_notasVirtual" type="button" id="btn_notasVirtual" value="Notas" onClick="javascript: enviarVirtual(4)" disabled>&nbsp;
						<input name="btn_limpiarv" type="button" id="btn_limpiarv" value="limpiar" onclick="javascript: enviarVirtual(5)" disabled>&nbsp;</td>
           	    </tr>
                <tr class="trInformacion">
                	<td>
                    	<table class="tablaPrincipal" width="100%" bgcolor="#DDDDDD">
                        <tr>
							<td width="15%">&nbsp;</td>
<!--							<td height="40" width="21">
                               	<input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar()" id="radioT" checked disabled></td>
							<td width="115">Todos los Grupos </td> -->
							<td width="10%"><input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()" id="radioS" disabled></td>
							<td colspan="4" width="364">Grupos Seleccionados</td>
						</tr>
<?PHP 
			$contChk=0;
			while(list($idMateria,$nombreMateria)=mysql_fetch_array($consulta))
			{
?>
						<tr>
							<td colspan="2">&nbsp;</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?PHP  echo  $nombreMateria; ?></b></td>
						</tr>
<?PHP 
				$sql="SELECT DISTINCT mei_virgrupo.idvirgrupo, mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirmateria =".$idMateria;
				$grupos=$baseDatos->ConsultarBD($sql);
				while(list($grupoCodigo,$grupoNombre)=mysql_fetch_array($grupos))
				{
?>
						<tr>
							<td colspan="2">&nbsp;</td>
							<td width="364">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            	<input id="grupo" name="<?PHP  echo "chk_idgrupo".$contChk; ?>" type="checkbox" onClick="javascript:chk_click()" value="<?PHP  echo $grupoCodigo; ?>" disabled><?PHP  echo  $grupoNombre; ?></td>
						</tr>
<?PHP 
					$contChk++;
				}
			}  
?>
						
					</table>
                   		<input type="hidden" name="hid_contGrupo" value="<?PHP  echo $contChk; ?>">
					</td>
				</tr>                        
<?PHP 				
		}
?>
            </table></form>
        </td>
        <td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho">&nbsp;</td>  
        <td class="tablaEspacioDerecho">&nbsp;</td>
	</tr>
</table>
</body>
</html>
<?PHP  
		}
		else
		{
			redireccionar('../login/');					
		}
	}
	else
	{
		redireccionar('../login/');					
	}
	
?>