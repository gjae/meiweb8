<?
	include_once ('../baseDatos/BD.class.php');  
	include_once ('../librerias/estandar.lib.php');
		
	$baseDatos=new BD();
	
	if(comprobarSession())
	{	
		if(!empty($_GET['idGrupo']) && !empty($_POST['hid_listaValores']) && !empty($_POST['txt_nombreSubgrupo']))
		{
			$listaSeleccionados=explode(";",$_POST['hid_listaValores']);
			$listaCodigosBusqueda=implode(",",$listaSeleccionados);		
				$sql="SELECT mei_relususub.idusuario
					FROM mei_relususub
					WHERE mei_relususub.idsubgrupo
					IN (
					SELECT mei_subgrupo.idsubgrupo
					FROM mei_subgrupo
					WHERE mei_subgrupo.idtiposubgrupo=".$_POST['cbo_tipoSubgrupo']." AND mei_subgrupo.idgrupo =".$_GET['idGrupo']."
					)
					AND mei_relususub.idusuario
					IN (".$listaCodigosBusqueda.")";
			
			$consultaAlSubGrupo=$baseDatos->ConsultarBD($sql);
			
			$numAlSubGrupo=mysql_num_rows($consultaAlSubGrupo);		
			
			while(list($codigoAlumnoTemp)=mysql_fetch_array($consultaAlSubGrupo))
			{
				if(in_array($codigoAlumnoTemp,$listaSeleccionados))
				{
					$listaErrores.="[$$$]".$codigoAlumnoTemp;
				}
			}
			
			$sql="SELECT mei_subgrupo.nombre FROM mei_subgrupo WHERE mei_subgrupo.idtiposubgrupo= '".$_POST['cbo_tipoSubgrupo']."' AND mei_subgrupo.idgrupo='".$_GET['idGrupo']."' AND mei_subgrupo.nombre='".strtoupper($_POST['txt_nombreSubgrupo'])."'";
			
			$consultaSubGrupo=$baseDatos->ConsultarBD($sql);
			
			$numSubGrupo=mysql_num_rows($consultaSubGrupo);
			
			
			if(empty($numSubGrupo) && empty($numAlSubGrupo))
			{
				$sql="INSERT INTO mei_subgrupo ( idsubgrupo , idtiposubgrupo ,idgrupo , nombre , descripcion ) 
						VALUES ('', '".$_POST['cbo_tipoSubgrupo']."','".$_GET['idGrupo']."', '".strtoupper($_POST['txt_nombreSubgrupo'])."', '".$_POST['txt_descripcionSubGrupo']."')";
						
				$baseDatos->ConsultarBD($sql);
				
				$idSubgrupo=$baseDatos->InsertIdBD($sql);
				
				
				$listaSeleccionados=explode(";",$_POST['hid_listaValores']);
				
				foreach($listaSeleccionados as $idUsuarioSeleccionado)
				{
					if(empty($listaValores))
					{
						$listaValores="('".$idUsuarioSeleccionado."','".$idSubgrupo."')";
					}
					else
					{
						$listaValores.=",('".$idUsuarioSeleccionado."','".$idSubgrupo."')";
					}			
				}		
							
				$sql="INSERT INTO mei_relususub ( idusuario , idsubgrupo ) 	VALUES ".$listaValores;
				
				$baseDatos->ConsultarBD($sql);
				
				
				if(empty($_POST['chk_volver']))
				{	
					redireccionar("../materias/listarSubgrupos.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idTipoSubgrupo=".$_POST['cbo_tipoSubgrupo']."&destino=".$_GET['destino']."&idGrupo=".$_GET['idGrupo']);	
				}
				else
				{
					if ($_SESSION['idtipousuario']==5)
					{
						redireccionar("../materias/VirCrearSubgrupo.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idTipoSubgrupo=".$_POST['cbo_tipoSubgrupo']."&destino=".$_GET['destino']."&idGrupo=".$_GET['idGrupo']);
					}
					else
					{
						redireccionar("../materias/crearSubgrupo.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idTipoSubgrupo=".$_POST['cbo_tipoSubgrupo']."&destino=".$_GET['destino']."&idGrupo=".$_GET['idGrupo']);
					}
				}
			}
			else
			{
				if(!empty($numSubGrupo) && !empty($numAlSubGrupo))
				{
					$error="0x003";
				}			
				else if(!empty($numSubGrupo))
				{
					$error="0x001";
				}
				else if(!empty($numAlSubGrupo))
				{
					$error="0x002";
				}
				
				if ($_SESSION['idtipousuario']==5)
				{
					redireccionar("../materias/VirCrearSubgrupo.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&destino=".$_GET['destino']."&idGrupo=".$_GET['idGrupo']."&error=".$error."&listaErrores=".$listaErrores."&listaValores=".$_POST['hid_listaValores']."&nombreSubgrupo=".strtoupper($_POST['txt_nombreSubgrupo'])."&descripcionSubGrupo=".$_POST['txt_descripcionSubGrupo']."&idTipoSubgrupo=".$_POST['cbo_tipoSubgrupo']);
				}
				else
				{
					redireccionar("../materias/crearSubgrupo.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&destino=".$_GET['destino']."&idGrupo=".$_GET['idGrupo']."&error=".$error."&listaErrores=".$listaErrores."&listaValores=".$_POST['hid_listaValores']."&nombreSubgrupo=".strtoupper($_POST['txt_nombreSubgrupo'])."&descripcionSubGrupo=".$_POST['txt_descripcionSubGrupo']."&idTipoSubgrupo=".$_POST['cbo_tipoSubgrupo']);
				}
			}
		}
		else
		{
			if ($_SESSION['idtipousuario']==5)
				redireccionar("../usuario/VirListarProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);		
			else
				redireccionar("../usuario/listarProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);		
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>