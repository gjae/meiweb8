<?php 


	include_once ('../baseDatos/BD.class.php');
	
	$baseDatos=new BD();

	function add_fecha($fecha, $dias)
	{
		$num_dias_suma = (int)$dias + 1; 
		
		list($agno,$mes,$dia) = explode('-',$fecha);
		
		$stamp = mktime(0,0,0,(int)$mes,(int)$dia,(int)$agno);
		
	  	$stamp += $num_dias_suma * 84600;
		
		return date("Y-m-d",$stamp);
	}
	

	$indice = (int)$_GET['indice'];
	$periodo = (int)$_GET['periodo'];
	$ln = (int)$_GET['longitud'];
	
	$array_salida = array();
	
	for($i = 0; $i < $ln; $i++)
	{
		if($i >= $indice)
		{
			$_GET['txt_fecha_final' .$i] = add_fecha($_GET['txt_fecha_inicial' .$i],$periodo);
			$_GET['txt_fecha_inicial' .($i +1)] = add_fecha($_GET['txt_fecha_final' .$i],1);
		}
		
		$array_salida[] = $_GET['txt_fecha_inicial' .$i]."[&]".$_GET['txt_fecha_final' .$i];		
		
		$sql = "UPDATE mei_planificador SET fechainicio = '".$_GET['txt_fecha_inicial' .$i]."', fechafin = '".$_GET['txt_fecha_final' .$i]."' WHERE idplanificador = '".$_GET['idplanificador' .$i]."'";
		$baseDatos->ConsultarBD($sql);
	}

	print implode('[&&]',$array_salida);
	
	
?>