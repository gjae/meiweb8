<?php

	function add_fecha($fecha, $dias)
	{
		$num_dias_suma = (int)$dias + 1; 
		
		list($agno,$mes,$dia) = explode('-',$fecha);
		
		$stamp = mktime(0,0,0,(int)$mes,(int)$dia,(int)$agno);
		
	  	$stamp += $num_dias_suma * 84600;
		
		return date("Y-m-d",$stamp);
	}
?>