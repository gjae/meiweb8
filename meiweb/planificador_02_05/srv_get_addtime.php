<?php
	

	if(!empty($_GET['fecha_inicial']))
	{
		$num_dias_suma = (int)$_GET['dias'] + 1; 
		
		list($agno,$mes,$dia) = explode('-',$_GET['fecha_inicial']);
		
		$stamp = mktime(0,0,0,(int)$mes,(int)$dia,(int)$agno);
		
	  	$stamp += $num_dias_suma * 84600;
		
		print date("Y-m-d",$stamp);
	}
	
?>