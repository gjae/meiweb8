// JavaScript Document
function srvGetServer(Url, GETVar)
{
	var XMLHttp = null;

	try{
		XMLHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	catch(expeption){
		XMLHttp = new XMLHttpRequest();
	}				
	
	XMLHttp.open("GET",Url + GETVar, false);
	XMLHttp.send(null);
				
	if(XMLHttp.status == 404){
		alert("La Url de conexion no es valida");
	}
	return XMLHttp.responseText;
}