<?
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once('../notas/notasClass.php');
	include_once ('../menu1/Menu1.class.php');
	if(comprobarSession())
	{	
		$baseDatos= new BD();	
?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>MEIWEB</title>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
	<script language="javascript">
        function check()
        {
            if (document.form1.backup.value != '')
            {
                document.form1.submit();
            } 
            else 
            {
                alert('No ha cargado ninguna archivo por favor verifique');
            }			  
        }
    </script>
</head>
<body>
<table class="tablaPrincipal">
  <tr valign="top">
  	<td class="tablaEspacio">&nbsp;</td>
	<td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
	<td class="tablaEspacio">&nbsp;</td>
    <td align="center" valign="top" class="tdGeneral">
		<table class="tablaMarquesina" >
			<tr>
<?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado);
?>
				<td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>" class="link"><?=ucwords(strtolower($nombre))?></a><a> -> </a><a href="../planificador/agregarPlanificador.php?estado=2&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET["materia"]?>" class="link"> Planificacion</a><a> -> Cargar Planificacion</a></td>
			</tr>
		</table>
		<table class="tablaGeneral">
			<tr class="trSubTitulo">
				<td colspan="2"><a> Cargar Planificaci&oacute;n</a></td>
			</tr>
            <div>&nbsp;&nbsp;&nbsp;&nbsp; </div>
			<tr class="trInformacion">
				<td height="10" colspan="2"></td>
			</tr>
			<tr class="trInformacion">
				<td width="20%" height="25" align="center"><strong> 
					<form action="procesoCarga.php?idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET["materia"]?>" enctype="multipart/form-data"  name="form1" id="form1" method="post" >
                        <label><br><br><input name="backup" type="file" id="backup" /></label>
                        <p>
                        <label><input align="middle" type="submit" name="Submit" value="Enviar"  onclick="javascript:check();" /></label>
                        </p>
				</form>
				</p>&nbsp;</strong></td>
			</tr>
      </table>
		<div>&nbsp;&nbsp;&nbsp;&nbsp; </div>
		<table width="569" class="tablaGeneral">
			<tr align="center" valign="middle" class="trSubTitulo">
				<td width="382" align="center">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td class="tablaEspacio">&nbsp;</td>
	<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
	<td class="tablaEspacioDerecho">&nbsp;</td>
  </tr>
</table>
</body>
</html>
<?
	}
	else 
		redireccionar('../agregarPlanificador.php?idmateria='.$_GET["idmateria"]);
?>