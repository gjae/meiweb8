<?PHP
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');

	if(comprobarSession())
	{
		$saludo=mostrarSaludo($_SESSION['idtipousuario'],$_SESSION['sexo']);
		$sistemaTema=recuperarVariableSistema("sistematema");

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Creditos MEIWEB</title>
<script>
function enviar()
{
	document.frm_verCreditos.submit();
	window.close();
}
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: x-small;
}
.Estilo4 {
	color: #333333;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<img src="../imagenes/tituloCreditos.jpg" width="660" height="54">	
<table width="95%" border="1" align="center" bordercolor="#FFFFFF">
  <tr bordercolor="#FFFFFF">
    <td colspan="3"><p align="justify" class="Estilo4"><img src="../imagenes/vineta1.gif" width="16" height="13" align="absmiddle">Cr&eacute;ditos de MeiWeb 7.0</p>
      <div align="justify">
        <blockquote><strong>MeiWeb 7.0</strong> ha sido desarrollado por estudiantes de la Escuela de Ingeniería de Sistemas e Informática de la Universidad Industrial de Santander como trabajo de grado bajo la dirección del profesor <strong>Ing. Manuel Guillermo Florez Becerra</strong>.</blockquote>
      </div>      
      <p align="justify" class="Estilo4"><img src="../imagenes/vineta1.gif" width="16" height="13" align="absmiddle">Proyectos de grado orientados al desarrollo de MeiWeb 3.0</p>
      <blockquote><p align="justify"><strong>AN&Aacute;LISIS, DISE&Ntilde;O E IMPLEMENTACI&Oacute;N DE LA PLATAFORMA MEI-WEB VERSI&Oacute;N 3.0, COMO SOPORTE DE MATERIAL EDUCATIVO INFORM&Aacute;TICO Y ESPACIO VIRTUAL DE APRENDIZAJE ENFOCADO A LA ADMINISTRACI&Oacute;N DEL SISTEMA Y A LA COMUNICACI&Oacute;N ENTRE USUARIOS.</strong></p>
        <p align="justify">Autores: <strong>Andrea Johana Duran Gomez, Oscar Javier Acelas Pe&ntilde;aloza</strong>.</p>
        <p align="justify">A&ntilde;o: <strong>2006. </strong></p>
        <p align="justify"><strong>AN&Aacute;LISIS, DISE&Ntilde;O E IMPLEMENTACI&Oacute;N DE LA PLATAFORMA MEI-WEB VERSI&Oacute;N 3.0, COMO SOPORTE DE MATERIAL EDUCATIVO INFORM&Aacute;TICO Y ESPACIO VIRTUAL DE APRENDIZAJE ENFOCADO A LA ADMINISTRACI&Oacute;N DE CONTENIDOS, EVALUACIONES Y AUTOEVALUACIONES.</strong></p>
        <p align="justify">Autores: <strong>Claudia Patricia Ni&ntilde;o Villamizar, Leonardo Favio Pradilla Perez.</strong></p>
        <p align="justify">A&ntilde;o: <strong>2006.</strong> </p>
      </blockquote>      <p align="justify">&nbsp;</p>
      <p align="justify" class="Estilo4"><img src="../imagenes/vineta1.gif" width="16" height="13" align="absmiddle">Versiones predecesoras de MeiWeb 3.0 </p>
      <div align="justify"></div>      <blockquote><p align="justify"><strong>MATERIAL EDUCATIVO INFORMATICO COMO HERRAMIENTA DE APOYO PARA EL APRENDIZAJE DE LOS SISTEMAS OPERACIONALES (MEI-SOS).</strong></p>
        <p align="justify">Autores: <strong>Jorge Alexander Salcedo Bautista, Juan Camilo Vargas Soto.</strong></p>
        <p align="justify">Director: <strong>Manuel Guillermo Florez Becerra.</strong></p>
        <p align="justify">A&ntilde;o: <strong>2001.</strong></p>
        <hr align="JUSTIFY">
      </blockquote>      <div align="justify"></div>      <blockquote><p align="justify"><strong>MATERIAL EDUCATIVO INFORMATICO (MEI) PARA EL APRENDIZAJE DE SISTEMAS OPERATIVOS MONOUSUARIOS Y MULTIUSUARIOS APLICADO A LA MEMORIA Y EL SISTEMA DE ARCHIVOS. </strong></p>
        <p align="justify">Autores: <strong>Edson Leonardo Castro Hernandez, Jairo Elias Valiente Fernandez.</strong></p>
        <p align="justify">Director: <strong>Manuel Guillermo Florez Becerra.</strong></p>
        <p align="justify">A&ntilde;o: <strong>2000.</strong></p>
        <hr align="JUSTIFY">      
        </blockquote>      <div align="justify"></div>      <blockquote><p align="justify"><strong>SOS MATERIAL EDUCATIVO INFORMATICO (MEI) PARA EL APRENDIZAJE DE SISTEMAS OPERATIVOS MONOUSUARIO Y MULTIUSUARIO APLICADO A PROCESOS, ENTRADA Y SALIDA Y CONCEPTOS GENERALES.</strong></p>
        <p align="justify">Autores: <strong>Yamile Osma Sandoval, Luz Marina Sierra Martinez.</strong></p>
        <p align="justify">Director: <strong>Manuel Guillermo Florez Becerra.</strong></p>
        <p align="justify">A&ntilde;o: <strong>2000.</strong></p>
      </blockquote>      <p align="justify">&nbsp;</p>
      <div align="justify"></div>      <blockquote><p align="justify"><strong>ANÁLISIS, DISEÑO Y DESARROLLO DE LA PLATAFORMA MEIWEB, ENFOCADO A LA IMPLEMENTACIÓN  DE UN PORTAL PARA LA ENSEÑANZA VIRTUAL VERSIÓN 4.1.</strong></p>
        <p align="justify">Autores: <strong>Lina Johanna Quintero Prada, Jaime Giovanny Garzon Gomez.</strong></p><br>
        <p align="justify">Director: <strong>Manuel Guillermo Florez Becerra.</strong></p>
        <p align="justify">A&ntilde;o: <strong>2011.</strong></p>
      </blockquote>      <p align="justify">&nbsp;</p>
      <p align="justify" class="Estilo4"><img src="../imagenes/vineta1.gif" width="16" height="13" align="absmiddle">Aviso de Privacidad </p>
      <div align="justify">
        <blockquote>
          <p>El uso y distribuci&oacute;n de este software se rige por las normas de derechos de autor estipuladas por la Universidad Industrial de Santander.</p>
        </blockquote>
      </div>      <blockquote>&nbsp;</blockquote>      <p align="justify">&nbsp;</p></td>
  </tr>
  <tr bordercolor="#666666">
    <td width="44%" bordercolor="#FFFFFF"><div align="right"></div></td>
    <td width="16%"><div align="center"><img src="../imagenes/logoApache.gif" width="104" height="58"><img src="../imagenes/logoPHP.gif" width="104" height="58"><img src="../imagenes/logoMySQL.gif" width="104" height="58"></div></td>
    <td width="40%" bordercolor="#FFFFFF"><div align="left"></div></td>
  </tr>
</table>
</body>
</html>
<?PHP 
	}
		else
			redireccionar('../login/');					
?>
