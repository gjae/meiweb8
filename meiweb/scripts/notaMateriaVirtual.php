<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu/Menu.class.php');
	include_once ('../menu1/Menu1.class.php');
	
	if(comprobarSession())
	{	
		$fecha_f=new FrmCalendario('frmu','txt_fecha_f','formulario','false','');

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>

function validarDecimal()
{ 
	valor = document.frmu.cbo_valor.value
	if ( (isNaN(valor)) || (valor<0) ) 	  
	{              
      		return false   
	}	
	else
	{      
            	return true 
      	} 
}

function verHora()
	{

		var fecha=frmu.txt_fecha_a.value.split("-");
		if(fecha[1]<10){ fecha[1]='0'+fecha[1];}
		if(fecha[2]<10){ fecha[2]='0'+fecha[2];}
		fecha=fecha[0]+"-"+fecha[1]+"-"+fecha[2];
		
		var fechaC=frmu.txt_fecha_f.value.split("-");
		if(fechaC[1]<10){ fechaC[1]='0'+fechaC[1];}
		if(fechaC[2]<10){ fechaC[2]='0'+fechaC[2];}
		fechaC=fechaC[0]+"-"+fechaC[1]+"-"+fechaC[2];
		
		// se pasan las fechas a formato aaaa-mm-dd hh:mm:ss
		
		
			
			
			if(fecha<fechaC){
				
					return true;
				}
			else{
				return false;
			}
	}

function enviar()
{
	if (validarDecimal())
	{
		if(verHora()){
		document.frmu.submit();
		
		window.close();
		}
	}
	else alert("El campo 'Valor' debe ser un número decimal positivo, entero o cero")
}

</script>
<title>MEIWEB</title>
</head>
<body onLoad="window.focus()">

<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo" valign="top"><?PHP menu($_SESSION['idtipousuario']); ?></td>
        	<td class="tablaEspacio">&nbsp;</td> 
        	<td>
        		<table class="tablaMarquesina">
                    <tr>
             <?PHP	 				
                   
                        $sql123 = "SELECT mei_virmateria.nombre, mei_virmateria.codigo FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
                   $resul = $baseDatos->ConsultarBD($sql123);
                    list($nom,$codigoMateria) = mysql_fetch_row($resul); ?>	  
					
                      <td class="BloqueClaroB">
					  <a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?PHP echo $_GET["idmateria"]; ?>&materia=<?PHP echo $_GET["materia"]; ?>" class="link"><?PHP echo ucwords(strtolower($nom)); ?></a> -> Configurar Materia Virtual
					  </td>
					  </tr>
					  </table>
        	
        	  
                  <table class="tablaMarquesina">
                    <tr>
          
                      <td class="BloqueClaroB">
					   </td>
					  </tr>
					  </table>

<form name="frmu" method="post" action="guardarNotaMinima.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>">
 

  <table class="tablaGeneral" border="0" cellpadding="0" cellspacing="0">
    <tr class="trTitulo">
      <td colspan="2" align="left" valign="middle">Configurar Materia Virtual</td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
    	<?
    	$sql = "SELECT mei_virmateria.fechafin, mei_virmateria.fechainicio , mei_virmateria.notamin
						FROM mei_virmateria WHERE `idvirmateria`='".$_GET["idmateria"]."' AND `nombre`='".$_GET["materia"]."'";
				//print $sql;
				$consulta=$baseDatos->ConsultarBD($sql);
				list($f_fecha,$a_fecha,$nota)=mysql_fetch_array($consulta);
				
				
		list($aa,$mm,$dd) = explode('-', $a_fecha);
		$a_fecha = $aa."-".$mm."-".$dd;
		
		list($aa,$mm,$dd) = explode('-', $f_fecha);
		$f_fecha = $aa."-".$mm."-".$dd;
    	?>
      <td align="left" title="Nota mínima necesaria para habilitar siguiente módulo" valign="middle"><strong>Nota mínima:</strong></td>
      <td align="left" valign="middle"><label>
        <input name="cbo_valor" type="text" id="cbo_valor" value="<?echo $nota?>" size="10" maxlength="5">
      </label></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
     <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Fecha Inicio:</strong></td>
      <td align="left" valign="middle"><label>
        <input name="txt_fecha_a" type="text" id="cbo_valor" value="<?echo $a_fecha?>" readonly size="10" maxlength="5">
      </label></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
      <td colspan="2"><b>La fecha de finalización debe ser mayor a la fecha de inicio</b></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trInformacion">
    	
      <td align="left" valign="middle"><b>Fecha Finalización:</b> </td>
      <td><?PHP 
								/*if(empty($_POST['hid_fecha']))
									$fecha_f->m_valor='';
								else
									$fecha_f->m_valor=$_POST['hid_fecha'];*/
									$fecha_f->m_valor=$f_fecha;
									$fecha_f->CrearFrmCalendario(); 
								?></td>
							</tr>
    <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trSubTitulo" align="center" valign="middle">
      <td colspan="2"><input type="button" name="Submit" value="Cambiar" onClick="javascript:enviar()"></td>
    </tr>
   <tr class="trInformacion">
      <td align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr class="trSubTitulo" align="center" valign="middle">
    <td colspan="2"><a href="homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Volver</a></td>
	</tr>
    
  </table>
  
</form>


</td>


<td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho" valign="top"><?PHP echo menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>             
			</tr>
		</table>

</body>
</html>
<?
}
else redireccionar('../login/');
?>