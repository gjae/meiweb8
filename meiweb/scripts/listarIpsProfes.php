﻿<?PHP
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/validar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');		
	
  	$baseDatos=new BD();
	if(@comprobarSession())
	{ 
	if($_SESSION['banderaAdmnistrador']==1)
	{
	
		if(@$_POST['cbo_tipoUsuario']==1)
		{
			$selectedAdmin="selected";
		}
		else if(@$_POST['cbo_tipoUsuario']==2)
		{
			$selectedAlumnos="selected";
		}
		else if(@$_POST['cbo_tipoUsuario']==3)
		{
			$selectedProfesores="selected";
		}
		else if(@$_POST['cbo_tipoUsuario']==4)
		{
			$selectedGrupos="selected";
		}
		
		else
		{
			$selectedTodos="selected";
		}
		
		
?>

		
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?PHP echo recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		</head>
		<body>
		<script> 
			function verBitacora(codigoUsuario)
			{
					location.replace("../bitacora/index.php?idUsuario="+codigoUsuario);					
			}
		
		function enviarOrdenar()
		{
			if (document.frm_buscarUsuario.cbo_tipoUsuario.value==4)
			{
				location.replace("listarGruposAdministrador.php");
			}
			else
			{
				document.frm_buscarUsuario.submit();
			}

		}
		
		function chekear()
		{
			var todo=document.getElementById('todo');
			if (todo.checked==true)
				{
					for(i=0;i<document.frm_usuario.elements.length;i++)
					{
						if(document.frm_usuario.elements[i].id=="codigoUsuario")
						document.frm_usuario.elements[i].checked=true;
					}
				}
			else
				{
					for(i=0;i<document.frm_usuario.elements.length;i++)
					{
						if(document.frm_usuario.elements[i].id=="codigoUsuario")
						document.frm_usuario.elements[i].checked=false;
					}
				}
		}
		
			<?PHPphp /*?>function eliminarUsuario(codigoUsuario)
			{
				if(confirm("¿Está seguro de eliminar este usuario del sistema?"))
				{
					location.replace("eliminarUsuario.php?codigoUsuario="+codigoUsuario);					
				}
			}<?PHPphp */?>
			function reiniciarUsuario(codigoUsuario)
			{
				if(confirm("¿Está seguro de reinicar este usuario?"))
				{
					location.replace("reiniciarUsuario.php?admin=1&codigoUsuario="+codigoUsuario);					
				}
			}
		function enviar()
		{
			document.frm_verUsuario.submit();				
		}
		function eliminar()
		{
			if(confirm("¿Está seguro de eliminar los usuarios seleccionados"))
			{
			var contador=0;
					for(i=0;i<document.frm_usuario.elements.length;i++)
					{
						if(document.frm_usuario.elements[i].id=="codigoUsuario")
						{
							if (document.frm_usuario.elements[i].checked==true)
							{
								contador++;							
							}
							
						}
					}
			if(contador>0)
				{
				document.frm_usuario.action="eliminarUsuario.php";
				document.frm_usuario.submit();
				}
			else
				alert('No hay ningun usuario seleccionado')
			}
		}
		function eliminarBuscados()
		{
			if(confirm("¿Esta seguro de eliminar los usuarios seleccionados"))
			{
			var contador=0;
					for(i=0;i<document.frm_buscarUsuario.elements.length;i++)
					{
							if (document.frm_buscarUsuario.elements[i].checked==true)
							{
								contador++;							
							}
					}
					
			if(contador>0)
				{
				document.frm_buscarUsuario.action="eliminarUsuario.php";
				document.frm_buscarUsuario.submit();
				}
			else
				alert('No ha seleccionado ningun Alumno')
			}
		}
		
		</script> 
		
		<table class="tablaPrincipal">
		<tr valign="top">
        	<td class="tablaEspacio">&nbsp;</td>
			<td class="tablaIzquierdo"><?PHP menu($_SESSION['idtipousuario']); ?></td></td>
        	<td class="tablaEspacio">&nbsp;</td> 
			<td><form name="frm_usuario" method="post">
			<?PHP
			if ($_POST['cbo_tipoUsuario']==5)
			{
				$sql="SELECT  DISTINCT ip.ipactivo, ip.idusuario,ip.ipactivo, ip.hora, ip.estado_eva, u.primerapellido, u.segundoapellido, u.primernombre, u.segundonombre
						FROM mei_ipactivo ip, mei_usuario u, mei_relusugru ug, mei_grupo gr, mei_bitacora b
						WHERE u.idtipousuario='6' AND ip.idusuario = u.idusuario AND u.idusuario=ug.idusuario AND ug.idgrupo=ug.idgrupo AND gr.idmateria=".$_GET['idmateria']." AND b.idusuario=ug.idusuario AND b.idmodulo=1 and b.idtipoaccion=1 
						GROUP BY gr.idgrupo, ip.ipactivo
						ORDER BY u.primerapellido";
						}
			else
			{
					$sql="SELECT DISTINCT ip.ipactivo, ip.idusuario,ip.ipactivo, ip.hora, ip.estado_eva , u.primerapellido, u.segundoapellido, u.primernombre, u.segundonombre
						FROM mei_ipactivo ip, mei_usuario u, mei_relusugru ug, mei_grupo gr, mei_bitacora b
						WHERE (u.idtipousuario='3' or u.idtipousuario='4' or u.idtipousuario='7') AND ip.idusuario = u.idusuario AND u.idusuario=ug.idusuario AND ug.idgrupo=ug.idgrupo AND gr.idmateria=".$_GET['idmateria']." AND b.idusuario=ug.idusuario AND b.idmodulo=1 and b.idtipoaccion=1 
						GROUP BY gr.idgrupo, ip.ipactivo
						ORDER BY u.primerapellido";
			}

			/*$sql="SELECT ip.* , u.primerapellido, u.segundoapellido, u.primernombre, u.segundonombre, gr.nombre
						FROM mei_ipactivo ip, mei_usuario u, mei_relusugru ug, mei_grupo gr, mei_bitacora b
						WHERE ip.idusuario = u.idusuario AND u.idusuario=ug.idusuario AND ug.idgrupo=ug.idgrupo AND gr.idmateria=".$_GET['idmateria']." AND b.idusuario=ug.idusuario AND b.idmodulo=1 and b.idtipoaccion=1 
						GROUP BY gr.idgrupo, ip.ipactivo
						ORDER BY u.primerapellido";*/
						
				$resultado=$baseDatos->ConsultarBD($sql);

				if(1)
				{
				?>
					<table class="tablaGeneral" >
					  <tr class="trTitulo">
					  	<td colspan="7" class="trTitulo"><img src="../evaluaciones/imagenes/usuarios.gif" width="16" height="16" align="texttop"> Ver Usuarios</td>
					  </tr>
				<?PHP														
								
								
																
								$resultado=$baseDatos->ConsultarBD($sql);
						?>
						<tr class="trSubTitulo">
						  <td class="trSubTitulo" colspan="2"><div align="center">&nbsp;&nbsp;C&oacute;digo</div></td>
							<td width="35%" class="trSubTitulo"><div align="center">Nombre del Alumno</div></td>
                            <td width="8%" class="trSubTitulo"><div align="center">Estado</div></td>	
				
											
						  <td width="18%" class="trSubTitulo"><div align="center">IP</div></td>					
						  <td width="6%" class="trSubTitulo"><div align="center">Hora</div></td>
                          <td width="7%" class="trSubTitulo"><div align="center">Tiempo Transcurrido</div></td>
						</tr>
						
							<?PHP
                                function RestarHoras($horaini,$horafin)
                                {
                                	$horai=substr($horaini,0,2);
                                	$mini=substr($horaini,3,2);
                                	$segi=substr($horaini,6,2);
                                
                                	$horaf=substr($horafin,0,2);
                                	$minf=substr($horafin,3,2);
                                	$segf=substr($horafin,6,2);
                                
                                	$ini=((($horai*60)*60)+($mini*60)+$segi);
                                	$fin=((($horaf*60)*60)+($minf*60)+$segf);
                                
                                	$dif=$fin-$ini;
                                
                                	$difh=floor($dif/3600);
                                	$difm=floor(($dif-($difh*3600))/60);
                                	$difs=$dif-($difm*60)-($difh*3600);
                                	return date("H:i:s",mktime($difh,$difm,$difs));
                                }                             
							$cont=0;
							while (list($id,$idusuario,$ip,$hora,$estadoeva, $apellido1, $apellido2, $nombre1, $nombre2)=mysql_fetch_array($resultado))
								{	
							    $horaactual = date("H:i:s");
								if($cont%2==0)
								{
									$color="trListaClaro";
								}
								else
								{
									$color="trListaOscuro";
								}
								

								?>
								<tr title="Usuario: Alumno" class="<?PHP echo $color; ?>">
									<td width="8%" align="right" class="<?PHP echo $color; ?>"><?PHP echo $cont+1; ?></td>
									<td width="18%" class="<?PHP echo $color; ?>">&nbsp;<?PHP echo $idusuario; ?></td>							
									
									<td class="<?PHP echo $color; ?>"><?PHP echo  $apellido1." ". $apellido2." ". $nombre1." ". $nombre2; ?></td>
                                    <td class="<?PHP echo $color; ?>"><?PHP echo  $estadoeva; ?></td>
									
										
									<td class="<?PHP echo $color; ?>" align="center"><?PHP echo $ip; ?></td>

											<td class="<?PHP echo $color; ?>" align="center"><a href="javascript: reiniciarUsuario('<?PHP echo  $codigoUsuario; ?>')"  class="link"></a></div>
										    <?PHP echo  $hora; ?></td>	
                                    <td class="<?PHP echo $color; ?>" align="center">
									   <?PHP
                                            $tiempotranscurrido = RestarHoras($hora,$horaactual);
                                            echo $tiempotranscurrido;
                                       ?>
                                    </td>                                             
								</tr>
								<?PHP
								$cont++;
									
								}//FIN WHILE
					  ?>
							  <tr class="trInformacion">
								<td colspan="7" class="trSubTitulo"><div align="center">&nbsp;</div></td>
							  </tr>
					  
					<br>
				<?PHP
				}
				else
				{
				?>
					<table class="tablaGeneral" >
						<tr class="trTitulo" >
						  <td><img src="../evaluaciones/imagenes/usuarios.gif" width="16" height="16">Ver Usuarios</td>
					  </tr>
					  <tr class="trAviso">
					  	<td class="trAviso">Actualmente no existen usuarios registrados</td>
					  </tr>
				  </table>
				<?PHP
				}
		
				?>
					</table>
					<input name="hid_contUsuario" type="hidden" value="<?PHP echo $cont; ?>">
					
		</form>
			  </td>
				<td class="tablaDerecho">&nbsp; <?PHP menu1($_SESSION['idtipousuario']); ?></td>
				<td class="tablaEspacio">&nbsp;</td>               
				<td class="tablaEspacioDerecho">&nbsp;</td>                                              
			</tr>
		</table>
<?PHP
	if(@$_GET['error'] == 1)
	{
	
?>
	<script language="javascript">
		alert("No se puede eliminar al profesor porque tiene grupos registrados.");
	</script>
<?PHP
	}
?>		
		</body>
		</html>
<?PHP 
	}
	else
	{
		redireccionar('../login/');					
	}
	}
	else
	{
		redireccionar('../login/');					
	}
	
?>
