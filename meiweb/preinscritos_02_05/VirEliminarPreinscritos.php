<?
	include_once('../librerias/estandar.lib.php');		
	include_once('../baseDatos/BD.class.php');
	
	if(comprobarSession())
	{
		if($_SESSION['banderaAdmnistrador']==1)
		{
			$baseDatos= new BD();
			if(!empty($_POST['hid_contUsuario']))	
			{
				for ($i=0;$i<=$_POST['hid_contUsuario'];$i++)
				{
					if(!empty($_POST['chk_usuario'.$i]))
					{
						$sql="DELETE FROM mei_preinscripcion WHERE mei_preinscripcion.registro=".$_POST['chk_usuario'.$i];
						$consulta=$baseDatos->ConsultarBD($sql);
					}
				}
			}
			redireccionar("VirPreinscritos.php?ordenar=mei_preinscripcion.registro");
		}
		else
		{
			redireccionar('../login/');
		}
	}
	else
	{
		redireccionar('../login/');
	}
?>