<?
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	

	$baseDatos=new BD();
	if(comprobarSession())
	{ 
		if($_SESSION['banderaAdmnistrador']==1)
		{
			$sql="SELECT COUNT(mei_preinscripcion.registro) FROM mei_preinscripcion WHERE mei_preinscripcion.idvirmateria=".$_GET['idmateria'];
			$resultado=$baseDatos->ConsultarBD($sql);
			list ($numRegistro)=mysql_fetch_array($resultado);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">    		
</head>
<body>
	<script>
		function enviarOrdenar()
		{
			var materia=document.frm_preinscritos.cbo_materia.value;
			if (materia!=0)
			{
				document.frm_preinscritos.action="VirPreinscritosMateria.php?ordenar=mei_preinscripcion.registro&idmateria="+materia;
				document.frm_preinscritos.submit();
			}
			else
			{
				document.frm_preinscritos.action="VirPreinscritos.php?ordenar=mei_preinscripcion.registro";				
				document.frm_preinscritos.submit();
			}
		}
	
		function chekear()
		{
			var todo=document.getElementById('todo');
			if (todo.checked==true)
			{
				for(i=0;i<=document.frm_preinscritos.elements.length;i++)
				{			
					if(document.frm_preinscritos.elements[i].id=="registro")
						document.frm_preinscritos.elements[i].checked=true;
				}
			}
			else
			{
				for(i=0;i<document.frm_preinscritos.elements.length;i++)
				{
					if(document.frm_preinscritos.elements[i].id=="registro")
						document.frm_preinscritos.elements[i].checked=false;
				}
			}
		}
		
		function eliminar()
		{
			if(confirm("¿Está seguro de eliminar los usuarios seleccionados?"))
			{
				var contador=0;
				for(i=0;i<document.frm_preinscritos.elements.length;i++)
				{
					if(document.frm_preinscritos.elements[i].id=="registro")
					{
						if (document.frm_preinscritos.elements[i].checked==true)
						{
							contador++;
						}
					}
				}
				if(contador>0)
				{
					document.frm_preinscritos.action="VirEliminarPreinscritos.php";
					document.frm_preinscritos.submit();
				}
				else
					alert('No hay ningún usuario seleccionado')
			}
		}
		
		var ventanaDocumento=false;
		function imprimirDocumento(idUsuario,idmateria)
		{
			if (typeof ventanaDocumento.document == 'object')
			{
				ventanaDocumento.close()
			}
			ventanaDocumento = window.open('../preinscritos/VirImprimirPreinscritosMateria.php?idUsuario='+idUsuario+'&idmateria='+idmateria,'ImprimirDocumento','width=700,height=650,left=10,top=10,scrollbars=YES,menubars=NO,status=NO,statusbar=NO,resizable=NO,location=NO');
		}

		var ventanaDocumento1=false;
		function imprimirDocumento1(idUsuario,idmateria)
		{
			if (typeof ventanaDocumento1.document == 'object')
			{
				ventanaDocumento1.close()
			}
			ventanaDocumento1 = window.open('../preinscritos/VirImprimirAdmitidosMateria.php?idUsuario='+idUsuario+'&idmateria='+idmateria,'ImprimirDocumento','width=700,height=650,left=10,top=10,scrollbars=YES,menubars=NO,status=NO,statusbar=NO,resizable=NO,location=NO');
		}

		function admitir()		
		{
			if(confirm("¿Está seguro que desea admitir los usuarios seleccionados?"))
			{
				var contador=0;
				for(i=0;i<document.frm_preinscritos.elements.length;i++)
				{
					if(document.frm_preinscritos.elements[i].id=="registro")
					{
						if (document.frm_preinscritos.elements[i].checked==true)
						{
							contador++;
						}
					}
				}
				if(contador>0)
				{
					document.frm_preinscritos.action="VirAdmitirPreinscritos.php";
					document.frm_preinscritos.submit();
				}
				else
					alert('No hay ningún usuario seleccionado')
			}
		}

	</script> 
<table class="tablaPrincipal">
	<tr valign="top">
    	<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaIzquierdo"><?=menu(1);?></td>
        <td class="tablaEspacio">&nbsp;</td>
		<td><form name="frm_preinscritos" method="post">
        	<table class="tablaGeneral">
				<tr class="trSubTitulo">
					<td width="25%">Ver Usuarios Preinscritos de: </td>
					<td width="75%">
                    	<select name="cbo_materia" onChange="javascript: enviarOrdenar()">
								<option class="link" value="0">Todos los Cursos</option>
<?
			$sql="SELECT mei_virmateria.idvirmateria, mei_virmateria.nombre FROM mei_virmateria";
			$resultado=$baseDatos->ConsultarBD($sql);
			while (list($idmateria,$materia)=mysql_fetch_array($resultado))
			{
				if($idmateria==$_GET['idmateria'])
					$seleccion="selected";
				else
					$seleccion=" ";
?>
								<option class="link" value="<?= $idmateria?>" <?=$seleccion?>><?=$idmateria.' '.$materia?></option>
<?
			}
?>
						</select></td>
				</tr> 
            </table> 
            <br>                 	
            <table class="tablaGeneral">
				<tr class="trTitulo">
					<td class="trTitulo" colspan="8"><img src="imagenes/usuario.gif" width="16" height="16" align="texttop">Usuarios Preinscritos</td>
				</tr>
<?
			if ($numRegistro>0)
			{
				$sql="SELECT * FROM mei_preinscripcion WHERE mei_preinscripcion.idvirmateria='".$_GET['idmateria']."' ORDER BY ".$_GET['ordenar'];
				$resultado=$baseDatos->ConsultarBD($sql);
?>
				<tr class="trInformacion">
                	<td>&nbsp;</td>
                	<td>&nbsp;</td>
                	<td>&nbsp;</td>
                	<td colspan="2" align="right"><b>
                    	<a href="javascript: imprimirDocumento1('<?=$_SESSION['idusuario']?>','<?=$_GET['idmateria']?>')" class="link">
	                    <img src="imagenes/imprimir.gif" border="0" alt="Imprimir Lista de Admitidos">Admitidos&nbsp;</a></b></td>
					<td colspan="3" align="right"><b>
                    	<a href="javascript: imprimirDocumento('<?=$_SESSION['idusuario']?>','<?=$_GET['idmateria']?>')" class="link">
	                    <img src="imagenes/imprimir.gif" border="0" alt="Imprimir Lista de Preinscritos"> Preinscritos&nbsp;&nbsp;&nbsp;</a></b></td>
				</tr>
                <tr class="trSubTitulo">
					<td width="15%" colspan="2" align="center" class="trSubTitulo">
                    	<input id="todo" type="checkbox" name="chk_todoUsuario" onClick="javascript:chekear()">
                        <a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_preinscripcion.registro&idmateria=<?=$_GET['idmateria']?>">&nbsp;&nbsp;Registro</a></td> 
					<td width="30%" class="trSubTitulo" align="center">
                    	<a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_preinscripcion.primernombre&idmateria=<?=$_GET['idmateria']?>">Usuario</a></td>
					<td width="18%" align="center" class="trSubTitulo">
                    	<a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_preinscripcion.documento&idmateria=<?=$_GET['idmateria']?>">Documento</a></td>
					<td width="10%" class="trSubTitulo" align="center">	
                    	<a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_preinscripcion.correo&idmateria=<?=$_GET['idmateria']?>">Correo</a></td>
					<td width="10%" class="trSubTitulo" align="center">
                    	<a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_preinscripcion.telefono&idmateria=<?=$_GET['idmateria']?>">Tel&eacute;fono</a></td>
					<td width="10%" class="trSubTitulo" align="center">
                    	<a class="link" href="<? echo $_SERVER['PHP_SELF'] ?>?ordenar=mei_preinscripcion.admitido&idmateria=<?=$_GET['idmateria']?>">Estado</a></td>
				</tr>
<? 
				$contador=0;
				while (list($registro,$idMateria,$tipoDoc,$documento,$nombre1,$nombre2,$apellido1,$apellido2,$correo,$telefono,$estado)=mysql_fetch_array($resultado))
				{
					if($contador%2==0)
						$color="trListaClaro";
					else
						$color="trListaOscuro";
					
					$sql="SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$idMateria;
					$consulta=$baseDatos->ConsultarBD($sql);
					while (list($materia)=mysql_fetch_array($consulta))
					{
?>
				<tr class="<?=$color?>">
					<td width="15%" class="<?=$color?>" align="center"><?=$contador+1?>
                    	<input id="registro" type="checkbox" name="<?= "chk_usuario".$contador?>" value="<?=$registro?>"></td>
					<td width="10%" class="<?=$color?>" align="center"><?=$registro?></td>                              
					<td width="30%" class="<?=$color?>">&nbsp;<?= $apellido1.' '.$apellido2.' '.$nombre1.' '.$nombre2?></td>			
					<td width="18%" align="center" class="<?=$color?>"><?=$tipoDoc?>&nbsp;<?=$documento?></td>
					<td width="10%" align="center" class="<?=$color?>">&nbsp;<a href="mailto:<?=$correo?>" class="link"><?=$correo?></a></div></td>
					<td width="10%" align="center" class="<?=$color?>">&nbsp;<?=$telefono?></td>
<? 
                    	if ($estado ==1)
						{                        
?>						
					<td width="10%" class="<?=$color?>" align="center"><img src="imagenes/admitido.png" alt="Admitido" width="16" height="16" border="0"></td>
<? 
						}
						else if ($estado==0)                                     
						{
?>
					<td width="10%" class="<?=$color?>" align="center"><img src="imagenes/desadmitido.png" alt="No Admitido" width="16" height="16" border="0"></td>
<?
						}
?>
				</tr>
<?
					}
					$contador++;
				}//FIN WHILE
?>
                <tr class="trInformacion">
                	<td colspan="8">&nbsp;</td>
                </tr>                
                <tr class="trInformacion">
                	<td colspan="8">&nbsp;&nbsp; <b>&#9733;</b> Para ver la lista en otro orden dar clic en el encabezado de cada columna.</td>
                </tr>
                <tr class="trInformacion">
                	<td colspan="8">&nbsp;</td>
                </tr>                
				<tr class="trInformacion">
					<td colspan="8" class="trInformacion" align="center">
						<input name="btn_admitir" type="button" value="Admitir Seleccionados" onClick="javascript: admitir()">                    
						<input name="btn_eliminar" type="button" value="Eliminar Seleccionados" onClick="javascript: eliminar()">
					</td>
				</tr>

<?				
			}
			else
			{
?>
				<tr class="trInformacion">
					<td class="trInformacion" align="center" colspan="8">No existen Usuarios Preinscritos</td>
				</tr>
<?
			}
?>				
			</table>
<input name="hid_contUsuario" type="hidden" value="<?=$contador?>">
			</form>
		</td>
        <td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho">&nbsp;</td> 
        <td class="tablaEspacioDerecho">&nbsp;</td> 
	</tr>
</table>
</body>
</html>
<? 
		}
		else
		{
			redireccionar('../login/');					
		}
	}
	else
	{
		redireccionar('../login/');					
	}
	
?>