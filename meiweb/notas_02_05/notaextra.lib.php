<?
include_once ('../baseDatos/BD.class.php');
include_once ('../librerias/estandar.lib.php');
include_once ('../librerias/vistas.lib.php');
include_once ('../menu/Menu.class.php');
include_once ('../menu1/Menu1.class.php');

$IDprevio = "";
$IDmateria = "";
$ESp = "";
$idgrupo = "";
$idmodC = "";
$modC = "";
$baseDatos = new BD();
$nota_Extra = array();

function notasAdicionales($Iprevio, $Imateria, $Ep, $Igrupo) {


    global $IDprevio, $IDmateria, $baseDatos, $ESp, $IDgrupo, $idmodC, $modC;
    
    $IDprevio = $Iprevio;
    $IDmateria = $Imateria;
    $ESp = $Ep;
    $baseDatos = new BD();
    $idgrupo = $Igrupo;
    if ($ESp != 'reposicion')
        $sql = "SELECT mei_evaprevio.idtiposubgrupo FROM mei_evaprevio WHERE mei_evaprevio.idprevio = '" . $IDprevio . "'";
    else
        $sql = "SELECT mei_evaquiz.idtiposubgrupo FROM mei_evaquiz WHERE mei_evaquiz.idprevio = '" . $IDprevio . "'";
    $ressubgrupo = $baseDatos->ConsultarBD($sql);
    list($idtiposubgrupo) = mysql_fetch_row($ressubgrupo);


    if ($ESp != 'reposicion') {
        if ($_SESSION['idtipousuario'] == 5) {
            $sql = "SELECT mei_tipoprevio.tipoprevio, mei_evaprevio.titulo, mei_virgrupo.nombre FROM mei_evaluacion, mei_evaprevio, mei_evavirgrupo, mei_virgrupo, mei_tipoprevio WHERE mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo AND mei_evaprevio.idprevio = '" . $IDprevio . "'";
        } else {
            $sql = "SELECT mei_tipoprevio.tipoprevio, mei_evaprevio.titulo, mei_grupo.nombre FROM mei_evaluacion, mei_evaprevio, mei_evagrupo, mei_grupo, mei_tipoprevio WHERE mei_evaprevio.idtipoprevio = mei_tipoprevio.idtipoprevio AND mei_evaprevio.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = mei_grupo.idgrupo AND mei_evaprevio.idprevio = '" . $IDprevio . "'";
        }
    } else {
        $sql = "SELECT 'Quiz de Reposicion', mei_evaquiz.titulo, '' FROM mei_evaquiz WHERE mei_evaquiz.idprevio = '" . $IDprevio . "'";
    }
    $resprevio = $baseDatos->ConsultarBD($sql);
    list($tipoprevio, $titulo, $grupo) = mysql_fetch_row($resprevio);

    if ($ESp != 'reposicion')
        $sql = "SELECT mei_evamodcalificar.idmodcalificar, mei_evamodcalificar.modcalificar, mei_evaprevio.fechafinalizacion 
			FROM mei_evaprevio, mei_evamodcalificar	WHERE mei_evaprevio.idprevio = '" . $IDprevio . "' AND
			mei_evaprevio.idmodcalificar = mei_evamodcalificar.idmodcalificar";
    else
        $sql = "SELECT mei_evamodcalificar.idmodcalificar, mei_evamodcalificar.modcalificar, mei_evaquiz.fechafinalizacion 
			FROM mei_evaquiz, mei_evamodcalificar	WHERE mei_evaquiz.idprevio = '" . $IDprevio . "' AND
			mei_evaquiz.idmodcalificar = mei_evamodcalificar.idmodcalificar";
    $resultado = $baseDatos->ConsultarBD($sql);

    list($idmod, $mod, $fecha_f) = mysql_fetch_row($resultado);
    $idmodC = $idmod;
//	echo "<script>alert(".$idmodC.")</script>";
    $modC = $mod;
    modCalificar($idgrupo);

}

function calcularNotaExtra($sql, $tipo) {
    global $IDprevio, $IDmateria, $baseDatos, $ESp, $IDgrupo, $idmodC, $modC, $nota_Extra;
    $baseDatos = new BD();
    $sql2 = "SELECT mei_evaprevio.curva, mei_evaprevio.incentivo from mei_evaprevio where idprevio='" . $IDprevio . "';";
    
    $result = $baseDatos->ConsultarBD($sql2);
    list($curva, $incentivo) = mysql_fetch_row($result);
    $resultado = $baseDatos->ConsultarBD($sql);
//echo "<hr>".$sql."<hr>";
    if ($tipo == 1) {// recive el sql donde se muestra de una vez la nota
        if ($curva == 1) {
            $maxVal = 0;
            while (list($idusu2, $pnombre2, $snombre2, $papellido2, $sapellido2, $nota2) = mysql_fetch_row($resultado)) {
                if ($nota2 > $maxVal) {
                    $maxVal = $nota2;
                }
            }
            $maxVal = number_format($maxVal, 1, '.', '');
            $decima = explode(".", $maxVal);
            $decima = $decima[1];
            if ($decima == 0) {
                $diferencia = 0;
            } else {
                $diferencia = 10 - $decima;
            }
            $bandera = 0;
            if ($diferencia > 0 && $diferencia <= 5) {// se le suma diferencia a todos
                $bandera = 1;
            } else if ($diferencia > 5 && $diferencaia <= 9) {// se la aplica la diferencia como factor a cada nota;
                $bandera = 2;
            }
            $resultados = $baseDatos->ConsultarBD($sql);
            while (list($idusu2, $pnombre2, $snombre2, $papellido2, $sapellido2, $nota2) = mysql_fetch_row($resultados)) {
                if ($bandera == 1) {// se le suma le factor
                    $nota_Extra[$idusu2] = $diferencia / 10;
                } else if ($bandera == 2) {//se le aplicara el factor a cada nota
                    $nota_Extra[$idusu2] = (($diferencia / 10) * $nota2) / 10;
                }else{
                    $nota_Extra[$idusu2] = 0;
                }
            }
        } else {//tiene nota extra";
            while (list($idusu2, $pnombre2, $snombre2, $papellido2, $sapellido2, $nota2) = mysql_fetch_row($resultado)) {
                if ($idintento != 0)
                    $nota_Extra[$idusu2] = $incentivo;
                else
                    $nota_Extra[$idusu2] = "0.00";
            }

            //print_r($nota_Extra);
        }
    }else if ($tipo == 2) {

        if ($ESp != 'reposicion')
            $sqlr = "SELECT mei_evaprevio.intentos FROM mei_evaprevio WHERE mei_evaprevio.idprevio = '" . $IDprevio . "'";
        else
            $sqlr = "SELECT mei_evaquiz.intentos FROM mei_evaquiz WHERE mei_evaquiz.idprevio = '" . $IDprevio . "'";
        $res1 = $baseDatos->ConsultarBD($sqlr);
        list($intentos) = mysql_fetch_row($res1);
        $temporal = array();
        $x = 0;
        $maxVal = 0;
		
        if ($curva == 1) {// cuandootiene curva
            $resultados = $baseDatos->ConsultarBD($sql);
            while (list($idusu, $pnombre, $snombre, $papellido, $sapellido) = mysql_fetch_row($resultados)) {
                if ($ESp != 'reposicion')
                    $sqln = "SELECT COUNT(*), SUM(mei_usuprevio.nota) FROM mei_usuprevio WHERE mei_usuprevio.idprevio = '" . $IDprevio . "'
						AND mei_usuprevio.idusuario = '" . $idusu . "' AND mei_usuprevio.idintento<='" . $intentos . "'";
                else
                    $sqln = "SELECT COUNT(*), SUM(mei_usuquiz.nota) FROM mei_usuquiz WHERE mei_usuquiz.idquiz = '" . $IDprevio . "'
						AND mei_usuquiz.idusuario = '" . $idusu . "'";
                $res = $baseDatos->ConsultarBD($sqln);
                list($cont, $sumNota) = mysql_fetch_row($res);
                if ($cont != 0) {
                    if (($sumNota / $cont) > $maxVal) {
                        $maxVal = $sumNota / $cont;
                    }
                }
            }
            //************
            $maxVal = number_format($maxVal, 1, '.', '');

            $decima = explode(".", $maxVal);

            $decima = $decima[1];
            if ($decima == 0) {
                $diferencia = 0;
            } else {
                $diferencia = 10 - $decima;
            }

            $bandera = 0;
            if ($diferencia > 0 && $diferencia <= 5) {// se le suma diferencia a todos
                $bandera = 1;
            } else if ($diferencia > 5 && $diferencaia <= 9) {// se la aplica la diferencia como factor a cada nota;
                $bandera = 2;
            }

            $resultados = $baseDatos->ConsultarBD($sql);
            while (list($idusu2, $pnombre, $snombre, $papellido, $sapellido) = mysql_fetch_row($resultados)) {
                if ($ESp != 'reposicion')
                    $sqln = "SELECT COUNT(*), SUM(mei_usuprevio.nota) FROM mei_usuprevio WHERE mei_usuprevio.idprevio = '" . $IDprevio . "'
						AND mei_usuprevio.idusuario = '" . $idusu2 . "' AND mei_usuprevio.idintento<='" . $intentos . "'";
                else
                    $sqln = "SELECT COUNT(*), SUM(mei_usuquiz.nota) FROM mei_usuquiz WHERE mei_usuquiz.idquiz = '" . $IDprevio . "'
						AND mei_usuquiz.idusuario = '" . $idusu2 . "'";

                $res = $baseDatos->ConsultarBD($sqln);
                list($cont, $sumNota) = mysql_fetch_row($res);

                if ($bandera == 1) {// se le suma le factor
                    $nota_Extra[$idusu2] = $diferencia / 10;
                } else if ($bandera == 2) {//se le aplicara el factor a cada nota
                    if ($cont != 0) {
                        $nota_Extra[$idusu2] = (($diferencia / 10) * ($sumNota / $cont)) / 10;
                        //print number_format(round(($sumNota / $cont), 1), 1, '.', '');
                    } else {
                        $nota_Extra[$idusu2] = "0.00";
                    }
                }else{
                    $nota_Extra[$idusu2] = 0;
                }
            }

            //************
        } else {// cuando tiene nota extra -
            $resultados = $baseDatos->ConsultarBD($sql);
            while (list($idusu2, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados)) {
                if ($idintento != 0)
                    $nota_Extra[$idusu2] = $incentivo;
                else
                    $nota_Extra[$idusu2] = "0.00";
            }
        }
    } else if ($tipo == 3) {// cuando es tipo 3
        //********************************************************
        $maxVal = 0;

        if ($curva == 1) {// cuandootiene curva
            $resultados = $baseDatos->ConsultarBD($sql);

            while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados)) {
                if ($ESp != 'reposicion')
                    $sql1 = "SELECT mei_usuprevio.nota FROM mei_usuprevio WHERE mei_usuprevio.idusuario = '" . $idusu . "'
						AND mei_usuprevio.idprevio = '" . $IDprevio . "' AND
						mei_usuprevio.idintento = '" . $idintento . "'";
                else
                    $sql1 = "SELECT mei_usuquiz.nota FROM mei_usuquiz WHERE mei_usuquiz.idusuario = '" . $idusu . "'
						AND mei_usuquiz.idquiz = '" . $IDprevio . "'";
                //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************
                $res = $baseDatos->ConsultarBD($sql1);
                list($nota) = mysql_fetch_row($res);
                if (($nota) > $maxVal) {
                    $maxVal = $nota;
                }
            }
            //echo "max valor".$maxVal;
            //************
            $maxVal = number_format($maxVal, 1, '.', '');

            $decima = explode(".", $maxVal);

            $decima = $decima[1];
            if ($decima == 0) {
                $diferencia = 0;
            } else {
                $diferencia = 10 - $decima;
            }


            $bandera = 0;
            if ($diferencia > 0 && $diferencia <= 5) {// se le suma diferencia a todos
                $bandera = 1;
            } else if ($diferencia > 5 && $diferencaia <= 9) {// se la aplica la diferencia como factor a cada nota;
                $bandera = 2;
            }

            $resultados = $baseDatos->ConsultarBD($sql);

            while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados)) {
                if ($ESp != 'reposicion')
                    $sql1 = "SELECT mei_usuprevio.nota FROM mei_usuprevio WHERE mei_usuprevio.idusuario = '" . $idusu . "'
						AND mei_usuprevio.idprevio = '" . $IDprevio . "' AND
						mei_usuprevio.idintento = '" . $idintento . "'";
                else
                    $sql1 = "SELECT mei_usuquiz.nota FROM mei_usuquiz WHERE mei_usuquiz.idusuario = '" . $idusu . "'
						AND mei_usuquiz.idquiz = '" . $IDprevio . "'";
                //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************
                $res = $baseDatos->ConsultarBD($sql1);
                list($nota) = mysql_fetch_row($res);

                if ($bandera == 1) {// se le suma le factor
                    if($nota>0)
                        $nota_Extra[$idusu] = $diferencia / 10;
                        else 
                        $nota_Extra[$idusu] = "0.0";
                } else if ($bandera == 2) {//se le aplicara el factor a cada nota
                    $nota_Extra[$idusu] = (($diferencia / 10) * ($nota)) / 10;
                }else{
                    $nota_Extra[$idusu] = 0;
                }
            }

            //************
        } else {// cuando tiene nota extra -
            $resultados = $baseDatos->ConsultarBD($sql);

            while (list($idusu2, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultados)) {
                if ($idintento != 0)
                    $nota_Extra[$idusu2] = $incentivo;
                else
                    $nota_Extra[$idusu2] = "0.00";
            }
        }

        //********************************************************
    }

    return $nota_Extra;
}

function mostrarLista($resultado, $nota_Extra) {
    //echo "entro a mostrar lista";
    //global $baseDatos, $idtiposubgrupo,$nota_Extra ;
    global $IDprevio, $IDmateria, $baseDatos, $ESp, $IDgrupo, $idmodC, $modC, $nota_Extra;
    $i = 0;
    while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $nota) = mysql_fetch_row($resultado)) {//mei_usuprevio.idusuario, mei_usuprevio.idintento, mei_usuprevio.nota
        if ($_POST["cbo_ordenar"] != 3)
            $ncompleto = $pnombre . " " . $snombre . " " . $papellido . " " . $sapellido;
        else
            $ncompleto = $papellido . " " . $sapellido . " " . $pnombre . " " . $snombre;

        //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************

        $sql = "SELECT mei_subgrupo.nombre FROM mei_subgrupo, mei_relususub 
						WHERE mei_relususub.idsubgrupo = mei_subgrupo.idsubgrupo AND 
						mei_subgrupo.idtiposubgrupo = '" . $idtiposubgrupo . "' AND mei_relususub.idusuario = '" . $idusu . "'";
        $res1 = $baseDatos->ConsultarBD($sql);
        list($nomSubgrupo) = mysql_fetch_row($res1);

        //******************************************************************************************
//------------------------------------------------------------------NC-----------------------------------------------------------------
/*$sql5="SELECT mei_evaprevio.curva FROM mei_evaprevio WHERE mei_evaprevio.idprevio='".$IDprevio."'";
$resu = $baseDatos->ConsultarBD($sql5);
list($curva) = mysql_fetch_row($resu);
if($curva==1)
{
	$sqln2 = "update mei_usuprevio set notaextra='" . number_format(round(($nota_Extra[$idusu]), 2), 2, '.', '') . "' where mei_usuprevio.idprevio='" . $IDprevio . "' and mei_usuprevio.idusuario='" . $idusu . "'";
        $notasextras2 = $baseDatos->ConsultarBD($sqln2);
}*/
//------------------------------------------------------------------NC-----------------------------------------------------------------
        
        ?>

                                                            <!--	<td class="<?= $clase ?>" align="center" valign="middle"><?= number_format(round(($nota_Extra[$idusu]), 2), 2, '.', '') ?>
        -->
        <?
        $i++;
    }
}

function modCalificar($idgrupo) {
    global $IDprevio, $IDmateria, $baseDatos, $ESp, $IDgrupo, $idmodC, $modC;
   
    if ($ESp != 'reposicion')
        $sql = "SELECT mei_evaprevio.intentos FROM mei_evaprevio WHERE mei_evaprevio.idprevio = '" . $IDprevio . "'";
    else
        $sql = "SELECT mei_evaquiz.intentos FROM mei_evaquiz WHERE mei_evaquiz.idprevio = '" . $IDprevio . "'";
    //echo $sql . "-----";
    $res1 = $baseDatos->ConsultarBD($sql);
    list($intentos) = mysql_fetch_row($res1);
    //echo "este es el modo de calificar".$idmodC;
    switch ($idmodC) {
        case 1://PROMEDIO
		
            /*if ($_SESSION['idtipousuario'] == 5) {
                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
						mei_usuario.segundoapellido FROM mei_usuario, mei_relusuvirgru WHERE 
						mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . " AND 
						mei_relusuvirgru.idvirgrupo='" . $IDgrupo . "' ";
            } else {
                $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
						mei_usuario.segundoapellido FROM mei_usuario, mei_relusugru WHERE 
						mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . " AND 
						mei_relusugru.idgrupo='" . $IDgrupo . "' ";
            }*/
            //print $sql."<p>";
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
						mei_usuario.segundoapellido FROM mei_usuario, mei_relusuvirgru WHERE 
						mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . " AND 
						mei_relusuvirgru.idvirgrupo='" . $idgrupo . "' ";
                } 
				else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
						mei_usuario.segundoapellido, mei_evaprevio.intentos FROM mei_usuario, mei_relusugru, mei_evaprevio WHERE 
						mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . " AND 
						mei_relusugru.idgrupo='" . $idgrupo . "' AND mei_evaprevio.idprevio = '" . $IDprevio . "' ";
                }			
            $resultado = $baseDatos->ConsultarBD($sql);
            $nota_Extra = calcularNotaExtra($sql, "2");

            $i = 0;
            while (list($idusu, $pnombre, $snombre, $papellido, $sapellido) = mysql_fetch_row($resultado)) {//mei_usuprevio.idusuario, mei_usuprevio.idintento, mei_usuprevio.nota
                if ($ESp != 'reposicion')
                    $sql = "SELECT COUNT(*), SUM(mei_usuprevio.nota) FROM mei_usuprevio WHERE mei_usuprevio.idprevio = '" . $IDprevio . "'
						AND mei_usuprevio.idusuario = '" . $idusu . "' AND mei_usuprevio.idintento<='" . $intentos . "'";
                else
                    $sql = "SELECT COUNT(*), SUM(mei_usuquiz.nota) FROM mei_usuquiz WHERE mei_usuquiz.idquiz = '" . $IDprevio . "'
						AND mei_usuquiz.idusuario = '" . $idusu . "'";
                //print $sql."<p>";
                if ($_POST["cbo_ordenar"] != 3)
                    $ncompleto = $pnombre . " " . $snombre . " " . $papellido . " " . $sapellido;
                else
                    $ncompleto = $papellido . " " . $sapellido . " " . $pnombre . " " . $snombre;
                //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************

                $sql2 = "SELECT mei_subgrupo.nombre FROM mei_subgrupo, mei_relususub 
						WHERE mei_relususub.idsubgrupo = mei_subgrupo.idsubgrupo AND 
						mei_subgrupo.idtiposubgrupo = '" . $idtiposubgrupo . "' AND mei_relususub.idusuario = '" . $idusu . "'";
                $res1 = $baseDatos->ConsultarBD($sql2);
                list($nomSubgrupo) = mysql_fetch_row($res1);

                //******************************************************************************************

                $res = $baseDatos->ConsultarBD($sql);
                list($cont, $sumNota) = mysql_fetch_row($res);
				/*$incentivo = number_format(round(($nota_Extra[$idusu]), 2), 2, '.', '');*/
				//echo "<script>alert('".$incentivo."');</script>";
//------------------------------------------------------------------NC-----------------------------------------------------------------
/*$sql5="SELECT mei_evaprevio.curva FROM mei_evaprevio WHERE mei_evaprevio.idprevio='".$IDprevio."'";
$resu = $baseDatos->ConsultarBD($sql5);
list($curva) = mysql_fetch_row($resu);
if($curva==1)
{
	$sqln2 = "update mei_usuprevio set notaextra='" . number_format(round(($nota_Extra[$idusu]), 2), 2, '.', '') . "' where mei_usuprevio.idprevio='" . $IDprevio . "' and mei_usuprevio.idusuario='" . $idusu . "'";
        $notasextras2 = $baseDatos->ConsultarBD($sqln2);
}*/
//------------------------------------------------------------------NC-----------------------------------------------------------------
                
                ?>

                                                                                                                            <!--<td class="<?= $clase ?>" align="center" valign="middle"><?= number_format(round(($nota_Extra[$idusu]), 2), 2, '.', '') ?></td>
                -->
                <?
                $i++;
            }
           
            break;
        case 2://PRIMER INTENTO
            if ($ESp != 'reposicion') {
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuprevio.nota, MIN(mei_usuprevio.idintento) FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuprevio ON mei_relusuvirgru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ";
                } else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuprevio.nota, MIN(mei_usuprevio.idintento) FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuprevio ON mei_relusugru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ";
                }
            } else {
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.nota FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuquiz ON mei_relusuvirgru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY mei_usuario.idusuario ASC";
                } else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.nota FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuquiz ON mei_relusugru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ORDER BY mei_usuario.idusuario ASC";
                }
            }
      //      print "<br><br>".$sql."<p>";
            $resultado = $baseDatos->ConsultarBD($sql);
            $nota_Extra = calcularNotaExtra($sql, "1");
            mostrarLista($resultado, $nota_Extra);

            break;
        case 3://ULTIMO INTENTO
            if ($ESp != 'reposicion') {
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuprevio.idintento),
		mei_usuprevio.nota FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuprevio ON mei_relusuvirgru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $IDprevio . " AND mei_usuprevio.idintento<='" . $intentos . "'
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_relusuvirgru.idvirgrupo=$idgrupo AND
		mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		GROUP BY mei_usuario.idusuario";
                } else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuprevio.idintento),
		mei_usuprevio.nota FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuprevio ON mei_relusugru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $IDprevio . " AND mei_usuprevio.idintento<='" . $intentos . "'
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_relusugru.idgrupo=$idgrupo AND
		mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		GROUP BY mei_usuario.idusuario ";
                }
            } else {
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.nota FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuquiz ON mei_relusuvirgru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=".$idgrupo."GROUP BY mei_usuario.idusuario ORDER BY mei_usuario.idusuario ASC";
                } else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, mei_usuquiz.nota FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuquiz ON mei_relusugru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=".$idgrupo." GROUP BY mei_usuario.idusuario ORDER BY mei_usuario.idusuario ASC";
                }
            }

            //print $sql."----caso 3 <p>";
            $resultado = $baseDatos->ConsultarBD($sql);
            $nota_Extra = calcularNotaExtra($sql, "3");

            $i = 0;
            while (list($idusu, $pnombre, $snombre, $papellido, $sapellido, $idintento) = mysql_fetch_row($resultado)) {
                if ($ESp != 'reposicion')
                    $sql1 = "SELECT mei_usuprevio.nota FROM mei_usuprevio WHERE mei_usuprevio.idusuario = '" . $idusu . "'
						AND mei_usuprevio.idprevio = '" . $IDprevio . "' AND
						mei_usuprevio.idintento = '" . $idintento . "'";
                else
                    $sql1 = "SELECT mei_usuquiz.nota FROM mei_usuquiz WHERE mei_usuquiz.idusuario = '" . $idusu . "'
						AND mei_usuquiz.idquiz = '" . $IDprevio . "'";
                //***************************SUBGRUPO AL QUE PERTENECE $idusu*******************************

                $sql = "SELECT mei_subgrupo.nombre FROM mei_subgrupo, mei_relususub 
						WHERE mei_relususub.idsubgrupo = mei_subgrupo.idsubgrupo AND 
						mei_subgrupo.idtiposubgrupo = '" . $idtiposubgrupo . "' AND mei_relususub.idusuario = '" . $idusu . "'";
                $res1 = $baseDatos->ConsultarBD($sql);
                list($nomSubgrupo) = mysql_fetch_row($res1);
                //echo $sql1;
                //******************************************************************************************
                if ($_POST["cbo_ordenar"] != 3)
                    $ncompleto = $pnombre . " " . $snombre . " " . $papellido . " " . $sapellido;
                else
                    $ncompleto = $papellido . " " . $sapellido . " " . $pnombre . " " . $snombre;


                $res = $baseDatos->ConsultarBD($sql1);
                list($nota) = mysql_fetch_row($res);
                //------------------------------------------------------------------NC-------------------------------------------------------
/*$sql5="SELECT mei_evaprevio.curva FROM mei_evaprevio WHERE mei_evaprevio.idprevio='".$IDprevio."'";
$resu = $baseDatos->ConsultarBD($sql5);
list($curva) = mysql_fetch_row($resu);
if($curva==1)
{
	$sqln2 = "update mei_usuprevio set notaextra='" . number_format(round(($nota_Extra[$idusu]), 2), 2, '.', '') . "' where mei_usuprevio.idprevio='" . $IDprevio . "' and mei_usuprevio.idusuario='" . $idusu . "'";
        $notasextras2 = $baseDatos->ConsultarBD($sqln2);
}*/
//------------------------------------------------------------------NC-----------------------------------------------------------------
                ?>

                                                                                                                    <!--	<td class="<?= $clase ?>" align="center" valign="middle"><?= number_format(round(($nota_Extra[$idusu]), 2), 2, '.', '') ?></td>
                -->
                <?
                $i++;
            }
            ?>

            <?
            break;
        //NOTA MAS BAJA
        case 4:
            if ($ESp != 'reposicion') {
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MIN(mei_usuprevio.nota) FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuprevio ON mei_relusuvirgru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ";
                } else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MIN(mei_usuprevio.nota) FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuprevio ON mei_relusugru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ";
                }
            } else {
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MIN(mei_usuquiz.nota) FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuquiz ON mei_relusuvirgru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario ";
                } else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MIN(mei_usuquiz.nota) FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuquiz ON mei_relusugru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario  ";
                }
            }

            $resultado = $baseDatos->ConsultarBD($sql);

            $nota_Extra = calcularNotaExtra($sql, "1");
            mostrarLista($resultado, $nota_Extra);
            break;
        case 5://NOTA MAS ALTA
            if ($ESp != 'reposicion') {
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuprevio.nota) FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuprevio ON mei_relusuvirgru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario  ";
                } else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuprevio.nota) FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuprevio ON mei_relusugru.idusuario = mei_usuprevio.idusuario AND
		mei_usuprevio.idprevio  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario  ";
                }
            } else {
                if ($_SESSION['idtipousuario'] == 5) {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuquiz.nota) FROM  mei_usuario, mei_relusuvirgru LEFT JOIN
		mei_usuquiz ON mei_relusuvirgru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusuvirgru.idvirgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario  ";
                } else {
                    $sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido,
		mei_usuario.segundoapellido, MAX(mei_usuquiz.nota) FROM  mei_usuario, mei_relusugru LEFT JOIN
		mei_usuquiz ON mei_relusugru.idusuario = mei_usuquiz.idusuario AND
		mei_usuquiz.idquiz  = " . $IDprevio . "
		WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>" . $_SESSION['idusuario'] . "
		AND mei_relusugru.idgrupo=$idgrupo
		GROUP BY mei_usuario.idusuario  ";
                }
            }
            //print $sql."<p>";
            $resultado = $baseDatos->ConsultarBD($sql);
            $nota_Extra = calcularNotaExtra($sql, "1");
            mostrarLista($resultado, $nota_Extra);
            break;
    }
}
?>
