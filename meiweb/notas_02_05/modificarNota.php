<?
	include_once ('../menu/Menu.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{	
	$baseDatos= new BD();	
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraBasica' , '' );
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
function enviar()
{
	document.frm_final.action = "modificarNota.php?idevaluacion=<?=$_GET["idevaluacion"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>";
	document.frm_final.submit();
}
</script>
</head>
<body>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
	      	<td valign="top" class="tdGeneral"><table class="tablaMarquesina" >
              <tr>
              <?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado);
			
		?>
                <td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a></a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>" class="link"><?=ucwords(strtolower($nombre))?></a><a> -> </a><a href="index.php?estado=2&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET["materia"]?>" class="link">Notas</a><a> -> </a><a href="verNotas.php?estado=2&idmateria=<?=$_GET["idmateria"]?>&idevaluacion=<?=$_GET["idevaluacion"]?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET["materia"]?>" class="link">Ver Notas</a> -> Modificar Notas </td>
              </tr>
            </table><br>
<?
		if ( $_GET["estado"] == 2 )
		{//if estado = 2
			list($idautor, $autor) = explode("-",$_POST["hid_autor"]);
?>
<form name="frm_final" method="post" action="guardarNotas.php?accion=MOD&idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>">
  <table class="tablaGeneral" width="440" border="0" align="center">
    <tr class="trTitulo">
      <td height="20" colspan="4" align="left" valign="middle">Ingresar Nota 
        <input name="hid_flag" type="hidden" id="hid_flag" value="1">
        <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_POST["hid_materia"]?>">
        <input name="hid_tiponota" type="hidden" id="hid_tiponota" value="<?=$_POST["hid_tiponota"]?>">
        <input name="hid_grupo" type="hidden" id="hid_grupo" value="<?=$_POST["hid_grupo"]?>">
        <input name="hid_autor" type="hidden" id="hid_autor" value="<?=$_POST["hid_autor"]?>">
        <input name="hid_idevaluacion" type="hidden" id="hid_idevaluacion" value="<?=$_POST["hid_idevaluacion"]?>"></td>
    </tr>
    <tr class="trInformacion">
      <td width="20%" align="left" valign="middle"><strong>Autor:</strong></td>
      <td colspan="3"><b>
        <?=$autor?>
      </b></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Tipo de Nota </strong></td>
      <td colspan="2"><strong>
        <?
	  		list($idtipoeva,$tipoeva) = explode("-",$_POST["hid_tiponota"]);
			print $tipoeva;
			//******************************************************************
		  	list($codmateria, $nommateria) = explode("-",$_POST["hid_materia"]);
			//******************************************************************
		?>
      </strong></td>
      <td width="60%">
        <?
			if ( ($idtipoeva==2) && ($_POST["chk_promedio"]==1))
				print "<em>Se promediar&aacute;n las Calificaciones de las Actividades Asignadas a esta Nota.</em>";
		?>
&nbsp;<input name="hid_promedio" type="hidden" id="hid_promedio" value="<? if (!empty($_POST["chk_promedio"])) print $_POST["chk_promedio"]; else print "0";?>">      </td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Nombre de la Nota : </strong></td>
      <td colspan="3"><?=$_POST["txt_nomeva"]?>
          <input name="hid_nomeva" type="hidden" id="hid_nomeva" value="<?=$_POST["txt_nomeva"]?>"></td>
    </tr>
    <tr class="trInformacion">
      <td align="left" valign="middle"><strong>Valor:</strong></td>
      <td colspan="3"><?=($_POST["cbo_valor"]*100)." %"?>
          <input name="hid_valor" type="hidden" value="<?=$_POST["cbo_valor"]?>"></td>
    </tr>
    <tr align="right" class="trInformacion">
      <td colspan="4" valign="middle"><strong>
        <?
	if ($_SESSION['idtipousuario']==5)
	{
		$sql = "SELECT SUM( valor_eval ) FROM `mei_evaluacion` , mei_evavirgrupo 
			WHERE mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND 
			mei_evaluacion.idevaluacion <> '".$_POST["hid_idevaluacion"]."' AND 
			mei_evavirgrupo.idvirgrupo =".$_POST["hid_grupo"];
	}
	else
	{
		$sql = "SELECT SUM( valor_eval ) FROM `mei_evaluacion` , mei_evagrupo 
			WHERE mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND 
			mei_evaluacion.idevaluacion <> '".$_POST["hid_idevaluacion"]."' AND 
			mei_evagrupo.idgrupo =".$_POST["hid_grupo"];
	}
	$resultado = $baseDatos->ConsultarBD($sql);
	
	list($totval) = mysql_fetch_row($resultado);
	
	$totval += $_POST["cbo_valor"];
	//print round($totval,2)."<p>";
	if ( round($totval,2) > 1 )
		print "El porcentaje total de las notas supera el 100%";
	else print "El porcentaje total de las notas es ".round($totval*100,1)."%";
?>
      </strong></td>
    </tr>
  &nbsp;
  </table>
  
  <table align="center" class="tablaGeneral">
    <tr align="center" valign="middle" class="trInformacion">
      <td ><input name="btn_editar" type="button" id="btn_editar" value="Editar" onClick="javascript:enviar()"></td>
      <?
			if ( round($totval,2) <= 1 )
			{
		?>
      <td><input name="btn_guardar" type="submit" id="btn_guardar" value="Guardar"></td>
      <?
			}
		?>
    </tr>
  </table>
</form>
<?
		}//END IF estado = 2
		else
		{//IF ESTADO INICIAL
			if ($_POST["hid_flag"] !=1 )
			{
				$sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
						mei_usuario.primerapellido, mei_usuario.segundoapellido,
						mei_evaluacion.tipoevaluacion, mei_tipoevaluacion.tipoevaluacion, 
						mei_evaluacion.nombre, mei_evaluacion.valor_eval, mei_evaluacion.promediar FROM mei_evaluacion,
						mei_usuario, mei_tipoevaluacion	WHERE mei_evaluacion.idautor = mei_usuario.idusuario AND 
						mei_evaluacion.tipoevaluacion = mei_tipoevaluacion.idtipoevaluacion AND
						mei_evaluacion.idevaluacion = '".$_GET["idevaluacion"]."'";
				
				$resultado = $baseDatos->ConsultarBD($sql);
				
				list( $idautor, $pnombre, $snombre, $papellido, $sapellido, $idtipoeva, $tipoeva, $nombre, $valor, $promediar) = mysql_fetch_row($resultado);
				$autor = $pnombre." ".$snombre." ".$papellido." ".$sapellido;
				$idevaluacion = $_GET["idevaluacion"];
			}
			else
			{
				list($idautor, $autor) = explode("-",$_POST["hid_autor"]);
		  		list($idtipoeva,$tipoeva) = explode("-",$_POST["hid_tiponota"]);
				//$tipoeva = $_POST["hid_tiponota"];
				$valor = $_POST["hid_valor"];
				$idevaluacion = $_POST["hid_idevaluacion"];
				$promediar = $_POST["hid_promedio"];
			}
?>
<form name="form1" method="post" action="modificarNota.php?estado=2&idevaluacion=<?=$_GET["idevaluacion"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>">
		<table class="tablaGeneral" width="440" border="0" align="center">
      <?
			if ( !empty($_GET["idmateria"])) $idmateria = $_GET["idmateria"];
			else $idmateria = $_POST["hid_materia"];
			if ( !empty($_GET["idgrupo"])) $idgrupo = $_GET["idgrupo"];
			else $idgrupo = $_POST["hid_grupo"];
	  ?>
                  <tr class="trTitulo">
                    <td colspan="4" align="left" valign="middle">Editar Nota
                      <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$idmateria?>">
                        <input name="hid_grupo" type="hidden" id="hid_grupo" value="<?=$idgrupo?>">
                    <input name="hid_idevaluacion" type="hidden" id="hid_idevaluacion" value="<?=$idevaluacion?>"></td>
                  </tr>
                  <tr class="trInformacion">
                    <td width="20%" align="left" valign="middle"><strong>Autor:</strong></td>
                    <td colspan="3"><strong><?=$autor?>
                      <input name="hid_autor" type="hidden" id="hid_autor" value="<?=$idautor."-".$autor?>">
                    </strong></td>
                  </tr>
                  <tr class="trInformacion">
                    <td align="left" valign="middle"><strong>Tipo de Nota </strong></td>
                    <td colspan="2"><strong>
                    <?=$tipoeva?>
                    </select>
                    <input name="hid_tiponota" type="hidden" id="hid_tiponota" value="<?=$idtipoeva."-".$tipoeva?>">
                    </strong></td>
                    <td width="60%"><div id="div_promedio" style="visibility:<? if ($idtipoeva==2) print "visible"; else print "hidden";?>">
                      <input <? if($idtipoeva==1) print "disabled "; if ($promediar==1) print "checked";  ?>  name="chk_promedio" type="checkbox"  id="chk_promedio" value="1">
                    Promediar las Calificaciones de las Actividades Asignadas a esta Nota.</div></td>
                  </tr>
                  <tr class="trInformacion">
                    <td align="left" valign="middle"><strong>Nombre de la Nota : </strong></td>
                    <td colspan="3"><input name="txt_nomeva" type="text" id="txt_nomeva" size="40" maxlength="50" 
			value="<? if (empty($_POST["hid_nomeva"])) print $nombre;
						else print $_POST["hid_nomeva"];?>"></td>
                  </tr>
                  <tr class="trInformacion">
                    <td align="left" valign="middle"><strong>Valor:</strong></td>
                    <td colspan="3"><select name="cbo_valor">
                        <?
			for ($i=0.00; $i<=1.01;$i+=0.01) 
			{
				print "<option value='$i'";
				if (round($i,2) == round($valor,2)) print " selected>";
				else print ">";
				print ($i*100)."%</option>";
			}
		?>
                      </select>                    </td>
                  </tr>
                </table>
		
				<table class="tablaGeneral" width="200" border="0" align="center">
				  <tr align="center" valign="middle" class="trInformacion">
					<td><input type="submit" name="Submit" value="Continuar">
					  <label>
					  <input type="submit" name="Submit2" value="Guardar">
				    </label></td>
				  </tr>
				</table>
<?
		}//FIN ESTADO INICIAL
?>
	        </form>
	        </td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>