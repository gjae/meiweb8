<?

	class subNota
	{
	//	var $idnota;
		var $nombre;
		var $tipo;
		var $valor;
		var $nota;
		
		function llenar_subNotas($nom, $tipo, $val, $nt)
		{
			//$this->idnota = $id;
			$this->nombre = $nom;
			$this->tipo = $tipo;
			$this->valor = $val;
			$this->nota = $nt;
		}
	}
	
	class notas
	{
	//	var $idnota;
		var $nombre;
		var $tipo;
		var $valor;
		var $nota;
		var $subNota;
		
		function llenar_notas($nom, $tipo, $val, $nt)
		{
			//$this->idnota = $id;
			$this->nombre = $nom;
			$this->tipo = $tipo;
			$this->valor = $val;
			$this->nota = $nt;
		}
		
		function agregar_subNota($id, $nom, $tipo, $val, $nt)
		{
			$this->subNota[$id] = new subNota;
			$this->subNota[$id]->llenar_subNotas($nom, $tipo, $val, $nt);
		}
	}
	
	class alumnos
	{
		var $idusuario;
		var $nombre;
		var $grupo;
		var $asignatura;
		var $subgrupo1;
		var $subgrupo2;
		var $notas;
		var $def;
		
		function nombrar($id, $nm, $grup, $asi, $ss1, $ss2)
		{
			$this->idusuario = $id;
			$this->nombre = $nm;
			$this->grupo = $grup;
			$this->asignatura = $asi;
			$this->subgrupo1 = $ss1;
			$this->subgrupo2 = $ss2;
		}
		
		function agregar_notas($id, $nom, $tipo, $val, $nt)
		{
			$this->notas[$id] = new notas;
			$this->notas[$id]->llenar_notas($nom, $tipo, $val, $nt);
		}
	}

?>