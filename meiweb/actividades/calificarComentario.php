<?
	include_once ('../librerias/estandar.lib.php');
include_once ('../menu/Menu.class.php');	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	include_once ('../librerias/vistas.lib.php');

	if(comprobarSession())
	{
		$saludo=mostrarSaludo($_SESSION['idtipousuario'],$_SESSION['sexo']);
		$sistemaTema=recuperarVariableSistema("sistematema");

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=$sistemaTema?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>

function enviar()
{
	if(validar())
	{
		document.frm_calificarForo.submit();
		window.close();
	}

}

function validar()
{
	if(document.frm_calificarForo.txt_nota.value == false && document.frm_calificarForo.txt_nota.value!= "0")
	{
				alert ("Debe Digitar una calificación al comentario");
				return false;
	}
	else
	{
		if(document.frm_calificarForo.txt_nota.value >=0 && document.frm_calificarForo.txt_nota.value <= 5)
		{

			return true;
		}
		else
		{
				alert ("Nota Erronea: Debe introducir entre 0 y 5");
				return false;
		}

	}
}
</script>
</head>

<body>
	<table class="tablaPrincipal" width="180px" >
		<tr class="trInformacion">
	 	 	<td align="center" class="trInformacion">
				<div align="center">
				  <table align="center" class="tablaPrincipal" width="100%">
		<tr class="trTitulo" >
			<td class="trTitulo" colspan="2"><div align="center"><img src="imagenes/calificar.gif" width="16" height="15">Calificar </div></td>
		</tr>

				  <form name="frm_calificarForo" method="post" action="guardarCalificacionForo.php?codigoComentario=<?=$_GET['codigoComentario']?>&codigoMensaje=<?=$_GET['codigoMensaje']?>&idmateria=<?=$_GET['idmateria']?>&idusuario=<?=$_GET['autorComentario']?>&materia=<?=$_GET["materia"]?>" target="mensajeForo">
					  <tr class="trInformacion">
					    <td>Digitar Calificaci&oacute;n:
					    <td><input type="text" name="txt_nota" size="10"></td>

				    </tr>

					  <tr class="trInformacion">
					  	<td colspan="2"><div align="center">
                          <input type="button" name="btn_calificar" value="Calificar Comentario" onClick="javascript:enviar()">
</div></td>
					  </tr>
                    
                      <tr class="trInformacion">
                        	<td colspan="2"><div align="center">
                            <a><h6 align="justify">Nota: Señor Docente al realizar este cambio en la notas, usted debe actualizar manualmente el hash para el control de seguridad e integridad de las notas.</h6></a>
</div></td>
					  </tr>
  				  </form>
			      </table>
		  </div></td>
		</tr>
	</table>
</body>
</html>
<?
	}
		else
			redireccionar('../login/');
?>
