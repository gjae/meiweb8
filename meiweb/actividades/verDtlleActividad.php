<?
   include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
   include_once ('../librerias/estandar.lib.php');
   include_once ('../librerias/vistas.lib.php');
   include_once ('../menu1/Menu1.class.php');	

/*  Nombre  : vertaller.php
	Autor   : Claudia
	Fecha   : 09/11/2005
	Detalle : Vista de Talleres
	Versión :
*/

if(comprobarSession())
	{
		if (!empty($_GET['cbo_orden'])) {
						$opcion='&cbo_orden='.$_GET['cbo_orden'];
					}	
					else{ $opcion=""; }
		if($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
		{
					$sql = "SELECT mei_usuario.primernombre, mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, mei_actividad.titulo,
							mei_virgrupo.nombre, mei_actividad.idtiposubgrupo, mei_actividad.numeromod, mei_actividad.valor, mei_evaluacion.nombre, 
							mei_tipoactividad.tipoactividad,mei_actividad.fechaactivacion, mei_actividad.fechafinalizacion,mei_actividad.archivo,
							mei_actividad.descripcion, mei_actividad.estado,mei_actividad.fechaprimfinalizacion, mei_actividad.coevaluacion FROM mei_actividad, mei_tipoactividad, mei_usuario, mei_evaluacion,
						   	mei_tiposubgrupo, mei_evavirgrupo, mei_virgrupo
						   	WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion
						   	AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo AND mei_actividad.idautor = mei_usuario.idusuario AND 
						   	mei_actividad.idtipoactividad = mei_tipoactividad.idtipoactividad AND mei_actividad.idactividad=".$_GET["idactividad"];
		}
		else
		{
					$sql = "SELECT mei_usuario.primernombre, mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido, mei_actividad.titulo,
		mei_grupo.nombre, mei_actividad.idtiposubgrupo,mei_actividad.numeromod, mei_actividad.valor, mei_evaluacion.nombre, mei_tipoactividad.tipoactividad, mei_actividad.fechaactivacion, 
		mei_actividad.fechafinalizacion,
			   mei_actividad.archivo, mei_actividad.descripcion, mei_actividad.estado,mei_actividad.fechaprimfinalizacion, mei_actividad.coevaluacion FROM mei_actividad, mei_tipoactividad, mei_usuario, mei_evaluacion,
			   mei_tiposubgrupo, mei_evagrupo, mei_grupo
			   WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion
			   AND mei_evagrupo.idgrupo = mei_grupo.idgrupo AND mei_actividad.idautor = mei_usuario.idusuario AND 
			   mei_actividad.idtipoactividad = mei_tipoactividad.idtipoactividad AND mei_actividad.idactividad=".$_GET["idactividad"];
		}
	    //$autor='1991938';//cambiar esto por autor u otro campo

			   
		$resultado=$baseDatos->ConsultarBD($sql);
		//print $sql;
		list($prinomAutor,$segnomAutor, $priapeAutor,$segapeAutor, $titulo, $nomgrupo, $idsubgrupo, $numeromod, $valor, $nomEva, $tipoactividad, $fecha_a, $fecha_f, $numtaller, $contenido, $estado, $fecha_f1, $coActividad) = mysql_fetch_row($resultado);

     $sql = "SELECT mei_relususub.lider FROM mei_relususub WHERE mei_relususub.idusuario=".$_SESSION['idusuario'];
	 $resultado2=$baseDatos->ConsultarBD($sql);
	 list($lider) = mysql_fetch_row($resultado2);

?>
<head>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MEIWEB</title>
</head>
<body>
<table class="tablaPrincipal">

			<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
            <td>
            
					<table class="tablaMarquesina" >
						<tr>
                        <?
                           if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==3)
		{
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
			$sgrupo = "SELECT mei_grupo.idgrupo FROM `mei_grupo`, mei_relusugru
						WHERE mei_grupo.idgrupo = mei_relusugru.idgrupo AND 
						mei_relusugru.idusuario=".$_SESSION["idusuario"]." AND 
						mei_grupo.idmateria=".$_GET["idmateria"];
		}
		else
		{
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		}
      	$resultado = $baseDatos->ConsultarBD($sql);
      	$rgrupo = $baseDatos->ConsultarBD($sgrupo);
		list($materia) = mysql_fetch_row($resultado);
		list($idgrupo) = mysql_fetch_row($rgrupo) ?>
	
          <?if($_SESSION['idtipousuario']==6){?>
          <td>
          	<a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($materia))?></a><a> -> </a><a href="verVirActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?><?=$opcion?>" class="link">Actividades</a> -> Ver <?=$tipoactividad;?>
          	</td>
          	<?}else{
          		?>
          		<td>
          	<a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($materia))?></a><a> -> </a><a href="verActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?><?=$opcion?>" class="link">Actividades</a> -> Ver <?=$tipoactividad;?>
          	</td>
          		<?
          	}?>
        </tr>
     </table>
        &nbsp;
        <table class="tablaGeneral" >
          <tr class="trTitulo">
            <td>Ver <?=$tipoactividad?></td>
            <?
            if (empty($_GET['cbo_orden'])) {
            	$_GET['cbo_orden']='*';
            }
            ?>
          </tr>
        </table>
		&nbsp;
        <table class="tablaGeneral" width="100%">
          <tr class="trInformacion">
            <td width="20%" align="left" valign="middle"><b>Autor:</b></td>
            <td width="80%" colspan="3"><b><?=$prinomAutor." ".$segnomAutor." ".$priapeAutor." ".$segapeAutor?></b></td>
          </tr>
          <tr class="trInformacion">
            <td width="20%" align="left" valign="middle"><b>Grupo:</b></td>
            <td width="40%"><?=$nomgrupo?></td>
            <td width="10%"><b>Subgrupo:</b></td>
             <td width="30%"><? if (empty($idsubgrupo)) print "Actividad No Grupal";
								else
								{
									$sql = "SELECT mei_tiposubgrupo.tiposubgrupo FROM mei_tiposubgrupo
											WHERE mei_tiposubgrupo.idtiposubgrupo=".$idsubgrupo;
									$rsql = $baseDatos->ConsultarBD($sql);
									list ($subgrupo) = mysql_fetch_row($rsql);
									print $subgrupo;
								}
							?></td>
           
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><b>Nota a la que pertenece:</b> </td>
            <td><?=$nomEva?></td>
            <td><strong>Co-evaluación:</strong></td>
            <td><?php if( $coActividad == 1 ){ echo "Si";} else { echo "No"; }  ?></td>
          </tr>
           <?
            if($_SESSION["idtipousuario"]==5 || $_SESSION['idtipousuario']==6)
            {
				?>
          <tr class="trInformacion">
            <td width="20%" align="left" valign="middle"><b>Numero de modulo:</b></td>
            <td colspan="3"><? print $numeromod."<br>";?></td>
          </tr>
         <? } ?>
         
          <tr class="trInformacion">
            <td width="20%" align="left" valign="middle"><b>Nombre <?=$tipoactividad?>:</b></td>
            <td colspan="3"><? print $titulo."<br>";?></td>
          </tr>
          <tr class="trInformacion">
            <td width="20%" align="left" valign="middle"><b>Valor del
            <?=$tipoactividad?>:</b></td>
            <td colspan="3"><?=($valor*100)."%"?></td>
          </tr>

          <tr class="trInformacion">
            <td width="20%" align="left" valign="middle"><b>Fecha Activaci&oacute;n: </b></td>
            <td colspan="3"><?=mostrarFechaTexto($fecha_a,1)?></td>
          </tr>
          <? if($fecha_f1!=""){?>
           <tr class="trInformacion">
            <td align="left" valign="middle"><b>Primera Fecha Finalizacion</b> </td>
            <td colspan="3"><?=mostrarFechaTexto($fecha_f1,1)?></td>
          </tr>
          <?}?>
          <tr class="trInformacion">
            <td align="left" valign="middle"><b>Fecha Finalizacion</b> </td>
            <td colspan="3"><?=mostrarFechaTexto($fecha_f,1)?></td>
          </tr>
          <tr class="trInformacion">
            <td align="left" valign="middle"><b>Estado:</b></td>
            <td colspan="3"><? if ($estado == 1) print "Activo";
							   elseif($estado == 0) print "Desactivo";
							?></td>
          </tr>
		  <?
		  	$sql = "SELECT mei_archivoactividad.idactividad, mei_archivoactividad.archivo,
					mei_archivoactividad.localizacion FROM mei_archivoactividad
					WHERE mei_archivoactividad.idactividad = '".$_GET["idactividad"]."'";
			$resArchivo = $baseDatos->ConsultarBD($sql);
			if ( mysql_num_rows($resArchivo) > 0 )
			{
		  ?>
          <tr class="trInformacion">
            <td align="left" valign="middle"><strong>Archivo(s) Adjunto(s): </strong></td>
			<?
				$cont=0;
				while ( list($idaa, $archivo, $localizacion) = mysql_fetch_row($resArchivo))
				{
					if($cont==0)
						$adjunto="<a href='descargarActividad.php?archivo=".$localizacion."&codigoArchivo=".$idaa."' class='link' target='_black'>".$archivo."</a>";
					else
						$adjunto.=" | "."<a href='descargarActividad.php?archivo=".$localizacion."&codigoArchivo=".$idaa."' class='link' target='_black'>".$archivo."</a>";
					$cont++;
				}
			?>
			<td colspan="3"><?=$adjunto?></td>
          </tr>
		  <?
		  	}
		  ?>
          <tr class="trInformacion">
            <td width="20%" align="left" valign="top"><b>Descripci&oacute;n:</b></td>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr class="trDescripcion">
            <td class="trDescripcion" colspan="4"><? print stripslashes(base64_decode($contenido));?></td>
          </tr>
	  </table>

		 <?
		 	if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
				{ //if idtipousuario = 2

				
		  ?>
&nbsp;
		  <table class="tablaGeneral" width="100%" border="0">
          <tr class="tablaMarquesina">
            <td width="20%" align="center"><a href="resultadosActividad.php?idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&idtiposubgrupo=<?=$idsubgrupo?>&materia=<?=$_GET["materia"]?><?=$opcion?>" class="link"><img src="imagenes/ver.gif" alt="Actividad" width="16" height="16" border="0"> Ver Resultados</a> </td>
            <td width="20%" align="center"><img src="imagenes/modificar.gif" width="16" height="16" border="0"> <a href="editarActividad.php?editar=1&idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$opcion?>" class="link"> Editar Actividad</a> </td>
			<?php if( $coActividad == 1 ):  ?>
				<td width="20%" align="center"><a href="calificarCoEvaluacion.php?idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&idtiposubgrupo=<?=$idsubgrupo?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_GET['cbo_orden']?>" class="link"><img src="imagenes/ver.gif" alt="Actividad" width="16" height="16" border="0">Calificadores Co-Eva</a> </td>
			<?php endif; ?>
            <td width="20%" align="center"><a onClick="return confirm('Eliminar esta Actividad. Al Eliminar esta Actividad tambien se eliminarán todas las respuestas a esta')" href="eliminarActividad.php?idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>
													  &materia=<?=$_GET["materia"]?><?=$opcion?>" class="link"><img src="imagenes/eliminar.gif" alt="eliminar" width="16" height="16" border="0"> Eliminar Actividad </a></td>
           <td width="20%" align="center"> <a href="verActividad.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_GET['cbo_orden']?>" class="link"><img src="imagenes/volver.gif" alt="eliminar" width="16" height="16" border="0"> Volver </td>
          </tr>
          <tr>
          	&nbsp;
          <tr class="tablaMarquesina" width="100%" border="0">
          	
           <td align="center" colspan="4">
           	 <a>Este componente, se muestra la lista de Direcciones IP de los usuarios conectados. <a href="listarIpsProfes.php?idmateria=<?PHP echo $_GET['idmateria']; ?>&materia=<?PHP echo $_GET["materia"];?>&idactividad=<?=$_GET["idactividad"]?>">Ver Tabla</a>. 
           	</td>
           
          </tr>
        
		
          
          </tr>
		  </table>
		  <?
		  		}
		  ?>
		 
&nbsp;
		<table class="tablaMarquesina" width="100%" border="0">
  <tr>
    <?
		if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==6  || $_SESSION['idtipousuario']==7)
		{ //if idtipousuario = 3  )
	?>
    <? if($lider==1 || empty($idsubgrupo)){?>

		  <td width="25%" align="center"><a href="responderActividad.php?idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&tipoactividad=<?=$tipoactividad?>&idtiposubgrupo=<?=$idsubgrupo?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagenes/responder.gif" alt="Actividad" width="16" height="16" border="0"> 
		  <? $sql= "SELECT mei_actividad.coevaluacion FROM mei_actividad WHERE mei_actividad.idactividad='".$_GET['idactividad']."' ";
		      $coeva=$baseDatos->ConsultarBD($sql);
		      list($idcoeva)=mysql_fetch_array($coeva);
		      if ($idcoeva==2) {
		      	?>
		      	Calificar Co-Evaluación </a></td>
		     <? }
		     else{
		   ?>
		  Responder Actividad </a></td>
          <? } ?>
          <? } else{ ?>
		  <td width="25%" align="center"><a href="verEnvioActividad.php?idactividad=<?=$_GET["idactividad"]?>&idusuario=<? print $_SESSION["idusuario"]?>&idmateria=<?=$_GET["idmateria"]?>&tipoactividad=<?=$tipoactividad?>&idtiposubgrupo=<?=$idsubgrupo?>&materia=<?=$_GET["materia"]?>" class="link"><img src="imagenes/ver.gif" alt="Actividad" width="16" height="16" border="0"> Ver Estado Actividad </a></td>
          <? } ?>
    <?
		}//fin if tipousuario = 3
	?>
	<?
		if ($_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==7)
		{ //if idtipousuario = 3  )
	?>
		<?php if(isset($_GET["cbo_orden"])): ?>
 	      <td width="25%" align="center"><a href="verActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_GET["cbo_orden"]?>" class="link"> Volver</a></td>
		<?php else: ?>
		  <td width="25%" align="center"><a href="verActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$idgrupo.'-'.$nomgrupo?>" class="link"> Volver</a></td>
		<?php endif; 
		}?>
		<?
		if ($_SESSION['idtipousuario']==6)
		{ //if idtipousuario = 3  )
	?>
		<?php if(isset($_GET["cbo_orden"])): ?>
 	      <td width="25%" align="center"><a href="verVirActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_GET["cbo_orden"]?>" class="link"> Volver</a></td>
		<?php else: ?>
		  <td width="25%" align="center"><a href="verVirActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$idgrupo.'-'.$nomgrupo?>" class="link"> Volver</a></td>
		<?php endif; 
		}?>
  </tr>
</table>




</td>
<td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
</tr>

</table>

<tr>
  <td class="tdGeneral" valign="top">
  
 
</body>
<?
  }
?>
</tr>
					</table>
</html>