<?
	include_once ('previoClass.php');
	//include_once('../evaluaciones/evaclass.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	$baseDatos= new BD();
	
if(comprobarSession())
{//if comprobar sesion
	$previo = $_SESSION['previo'];

	if ( !empty($previo->idusuario) )
	{//YA ESTA CREADO EL OBJETO*/

?>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="timer.js"></script>


</head>
<body>
<table  width="276">
  <tr>
    <td valign="top"><? menu($_SESSION['idtipousuario']); ?></td>
    <td valign="top">
		<form id="frm_previo" name="frm_previo" method="post" action="calificarEvaluacion.phpidmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>">
		<!--***********************************************************************************-->
			<!-- CAPA QUE CONTIENE EL RELOJ-->
			<?php 
			$tiempofinal = $previo->tiempoInicio+($previo->tiempoTotal);
			?>
			
			<script language="javascript" type="text/javascript">
			<!--
			/*function send_data() {
				document.forms[0].submit();
				return true;
			}*/
			
			var timesup = "Su tiempo se ha Agotado. Finalizar Evaluación";
			var quizclose = 0; // in seconds
			var quizTimerValue = <?php echo ( $tiempofinal - time() )?>; // in seconds
			parseInt(quizTimerValue);
			// -->
			</script>
					<div id="timer" style="position: relative; top: 0px; left: 675px;">
						<table>
						<tr>
							<td bgcolor="#ffffff" width="100%">
							<table class="tablaPrincipal" border="0" width="150" cellspacing="0" cellpadding="0">
							<tr class="trTitulo">
								<th width="100%">Tiempo Restante</th>
							</tr>
							<tr class="trInformacion">
								<td id="QuizTimer" align="center" width="100%">
								<input onFocus="blur()" type="text" name="time"
								style="background-color: transparent; border: none; width: 70%; font-family: sans-serif; font-size: 14pt; font-weight: bold; text-align: center;" />
								</td>
							</tr>
							</table>
							</td>
						</tr>
						</table>
					</div>
			<script language="javascript" type="text/javascript">
			<!--
			function changecolor(col) {
				// Changes the timers background color
				var d = document.getElementById('QuizTimer');
				d.style.backgroundColor = col;
			}
			
			var timerbox = getObjectById('timer');
			var theTop = 100;
			var old = theTop;
			movecounter(this);
			
			document.onload = countdown_clock();
			// -->
			</script>
			<!--FIN DE LA CAPA-->			
		<!--***********************************************************************************-->
		<table class="tablaMarquesina" >
          <tr>
            <td><a href="verEvaluacion.php?idmateria=<?=$previo->idmateria?>&materia=<?=$_GET["materia"]?>" class="link">Evaluaciones</a> ->Responder Evaluaci&oacute;n </td>
          </tr>
        </table>
        <?
			if ( count($previo->pregunta) > 0 )			
			{//IF count($previo->pregunta)
		?>&nbsp;
		<table class="tablaGeneral">
			<tr class="trTitulo">
			  <td colspan="2" align="left" valign="middle">Caracteristicas de la Evaluaci&oacute;n </td>
		  </tr>
			<tr class="trInformacion">
			  <td width="20%" align="left" valign="middle">Evaluación</td>
			  <td width="80%" align="left" valign="middle"><?=$previo->titulo?></td>
		  </tr>
			<tr class="trInformacion">
			  <td align="left" valign="middle">Intento </td>
				<td align="left" valign="middle"><?=$previo->idintento?></td>
			</tr>
			<tr class="trInformacion">
			  <td align="left" valign="middle">Valor del Intento: </td>
			  <td align="left" valign="middle"><?=($previo->valorIntento*100)."%"?></td>
		  </tr>
		  <?
		  	if ( !empty($previo->comentario) )
			{
		  ?>
			<tr class="trInformacion">
			  <td align="left" valign="middle">Comentario(s):</td>
			  <td align="left" valign="middle"><?=$previo->comentario?></td>
		  </tr>
		  <?
		  	}
		  ?>
		</table>
		<?
			
				while ( list ($numpreg) = each($previo->pregunta) )
				{//while $previo->pregunta
					$tipopreg = $previo->pregunta[$numpreg]->tipo;
		?>
&nbsp;
<table class="tablaGeneral" border="1">
				<tr class="trListaClaro">
				  <td colspan="4" align="center" valign="middle">
				  <table class="tablaGeneral">
                    <tr class="trTitulo">
                      <td align="left" valign="middle">Valor Pregunta: <?=($previo->pregunta[$numpreg]->valor_preg*100)."%"?></td>
                      <td align="right" valign="middle"><?=$previo->pregunta[$numpreg]->txt_tipo?></td>
                    </tr>
                  </table>&nbsp;
  				  <table class="tablaGeneral">
                    <tr class="trSubtitulo">
                      <td width="2%" align="right" valign="top"><?=$numpreg.". "?></td>
	  				  <? 
						if ( $tipopreg != 4 )
						{
					  ?>
                      <td width="98%" align="left" valign="middle"><?=$previo->pregunta[$numpreg]->txt_preg?></td>
					  <?
					  	}
						else
						{
							list( $afirmacion, $razon ) = explode("[$$$]",$previo->pregunta[$numpreg]->txt_preg);
					  ?>
                      <td width="98%" align="center" valign="middle"><?=$afirmacion."<P>PORQUE<P>".$razon?></td>
					  <?
					  	}
					  ?>
                    </tr>
                  </table>&nbsp;				    
				  <table width="95%" border="0" cellpadding="0" cellspacing="0" class="tablaPrincipal">
				    <?
					$i = 0;
					while ( list($numresp) = each($previo->resp_bd[$numpreg]) )
					{
						if ( ($i%2)==0 ) $clase = "trListaOscuro";
						else $clase = "trInformacion";
		?>
				    <tr class="<?=$clase?>">
				      <td width="2%">
				        <?
							if ( ($tipopreg == 1) or ($tipopreg == 3) or ($tipopreg == 4) ) 
							{
		?>
				        <input type="radio" name="rad_resp<?=$numpreg?>" value="<?=$numresp?>">
				        <?
							}elseif ( $tipopreg == 2 ) 
							{
		?>
				        <input type="checkbox" name="chk_resp[]" value="<?=$numresp?>">
			          <?
							}
		?>					  </td>
				      <td width="2%"><?=$numresp.". "?></td>
				      <td width="92%"><?=$previo->resp_bd[$numpreg][$numresp]->txt_resp?></td>          
				      <td width="4%" align="right" valign="middle"><? //=$previo->resp_bd[$numpreg][$numresp]->valor_resp?></td>		
				    </tr>
				    <?
						$i++;
					}
		?>
			      </table>				  				  </td>
			    </tr>
		  </table>
		<?
				}//fin while $previo->pregunta
		?>
			&nbsp;
			<table class="tablaGeneral">
				<tr class="trSubTitulo">
					<td align="center" valign="middle"><input type="submit" name="Submit" value="Terminar" onClick="return confirm('Terminar la Evaluación')"></td>
				</tr>
			</table>
		<?
			}//FIN IF count($previo->pregunta)
		?>
		</form>
	</td>
  </tr>
</table>
</body>
</html>
<?
	}//FIN YA ESTA CREADO EL OBJETO*/
	else redireccionar('../scripts/homeUsuario.php');
}//fin if comprobar sesion
else redireccionar('../login/');
?>		

