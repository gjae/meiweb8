<?
   include_once ('../librerias/estandar.lib.php');
   include_once('../baseDatos/BD.class.php');  
   include_once ('../librerias/vistas.lib.php');
   include_once("../editor/fckeditor.php") ;
   include_once('../calendario/FrmCalendario.class.php');
   include_once ('../menu1/Menu1.class.php');
   
/*  Nombre  : vertaller.php
	Autor   : Claudia
	Fecha   : 06/03/2006
	Detalle : Editar Actividad
	Versión :  
*/ $baseDatos=new BD();
    
	// FCKeditor([Nombre del recurso] , [Tamaño horizontal] , [Tamaño vertical] , [Barra de herramientas] ,
	$editor=new FCKeditor('edt_actividad' , '100%' , '200' , 'barraCorreo' , '' ) ;

//************************************************************************
if ( !empty($_GET["idmateria"]) )
{
	$idmateria = $_GET["idmateria"];
	$materia = $_GET["materia"];
}
elseif ( !empty($_POST["hid_materia"]) ) 
	list ($idmateria, $materia) = explode("*", $_POST["hid_materia"]);
//***********************************************************************

if(comprobarSession())
{
	
	if ( $_GET["editar"]==1 )
	{
		if ($_SESSION["idtipousuario"]==5)
		{
		$sql = "SELECT mei_virgrupo.nombre, mei_evavirgrupo.idvirgrupo, mei_actividad.idevaluacion, mei_actividad.idtipoactividad, mei_actividad.idtiposubgrupo,
				mei_actividad.valor, mei_actividad.idautor, mei_actividad.titulo, mei_actividad.fechaactivacion, mei_actividad.fechafinalizacion, mei_actividad.descripcion, mei_actividad.estado ,mei_actividad.fechaprimfinalizacion 
				FROM mei_actividad, mei_evaluacion, mei_evavirgrupo, mei_virgrupo WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion
				AND mei_evavirgrupo.idevaluacion = mei_evaluacion.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo 
				AND mei_actividad.idactividad = ".$_GET["idactividad"];
		}
		else
		{
		$sql = "SELECT mei_grupo.nombre, mei_evagrupo.idgrupo, mei_actividad.idevaluacion, mei_actividad.idtipoactividad, mei_actividad.idtiposubgrupo,
				mei_actividad.valor, mei_actividad.idautor, mei_actividad.titulo, mei_actividad.fechaactivacion, mei_actividad.fechafinalizacion, mei_actividad.descripcion, mei_actividad.estado,mei_actividad.fechaprimfinalizacion 
				FROM mei_actividad, mei_evaluacion, mei_evagrupo, mei_grupo WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion
				AND mei_evagrupo.idevaluacion = mei_evaluacion.idevaluacion AND mei_evagrupo.idgrupo = mei_grupo.idgrupo 
				AND mei_actividad.idactividad = ".$_GET["idactividad"];
		}
		$resultado = $baseDatos->ConsultarBD($sql);
		//print $sql;
		list ($grupo,$idgrupo,$idnota,$tipoactividad,$idsubgrupo,$valor,$idautor,$titulo,$a_fecha,$f_fecha,$descripcion,$estado,$fechaprimfinalizacion) = mysql_fetch_row($resultado);
		$a_fecha = addGuiones($a_fecha);
		$f_fecha = addGuiones($f_fecha);
		$f1_fecha = addGuiones($fechaprimfinalizacion);
		$tienefecha1=false;
		if($f1_fecha!="-0-0-0-0" ){
			$tienefecha1=true;
		}else{
			$f1_fecha="0000-00-00-00-00";
		}
		list($aa,$mm,$dd,$hora_f1,$min_f1) = explode('-', $f1_fecha);
		$f1_fecha = $aa."-".$mm."-".$dd;
	
		list($aa,$mm,$dd,$hora_a,$min_a) = explode('-', $a_fecha);
		$a_fecha = $aa."-".$mm."-".$dd;
		list($aa,$mm,$dd,$hora_f,$min_f) = explode('-', $f_fecha);
		$f_fecha = $aa."-".$mm."-".$dd;
		/*print "a_: ".$a_fecha;
		print "f_: ".$f_fecha;*/
	}
	else//if ($_GET["editar"]==2)
	{
		//print "popo<p>";
		//*******************************************************************************					
		$idnota = $_POST["hid_nota"];
		list($idgrupo, $grupo) = explode("-", $_POST["hid_grupo"]);
		$tipoactividad = $_POST["hid_tipoactividad"];
		$valor = $_POST["hid_valor"];
		$titulo = $_POST["hid_titulo"];
		$fechaActivacon = $_POST["hid_fecha"];
		$descripcion = $_POST["hid_editor"];
		$idsubgrupo = $_POST["hid_subgrupo"];
		//******************GRUPO*************************
		/*if (!empty($_POST["cbo_grupo"]))
			$opcion = $_POST["cbo_grupo"];
		else $opcion = $_POST["hid_grupo"];
		
		*/
		//************************************************
	}
	if ($_SESSION["idtipousuario"]==5)
		{
	$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion, mei_evavirgrupo WHERE 
			mei_evavirgrupo.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.tipoevaluacion = 2 AND
			mei_evaluacion.promediar = 1 AND mei_evavirgrupo.idvirgrupo='".$idgrupo."'";
		}
		else
		{
			$sql = "SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion, mei_evagrupo WHERE 
			mei_evagrupo.idevaluacion = mei_evaluacion.idevaluacion AND mei_evaluacion.tipoevaluacion = 2 AND
			mei_evaluacion.promediar = 1 AND mei_evagrupo.idgrupo='".$idgrupo."'";
		}
		
	$resgrupo = $baseDatos->ConsultarBD($sql);		
	if ($_SESSION["idtipousuario"]==5)
		{
	$sql = "SELECT mei_actividad.idevaluacion, SUM(mei_actividad.valor) FROM mei_actividad, mei_evaluacion, mei_evagrupo 
			WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."' AND
			mei_actividad.idactividad <> '".$_GET["idactividad"]."' GROUP BY mei_actividad.idevaluacion";
		}
		else
		{
				$sql = "SELECT mei_actividad.idevaluacion, SUM(mei_actividad.valor) FROM mei_actividad, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."' AND
			mei_actividad.idactividad <> '".$_GET["idactividad"]."' GROUP BY mei_actividad.idevaluacion";
		}
	//print $sql."<p>";
	$resultado = $baseDatos->ConsultarBD($sql);
	if ($_SESSION["idtipousuario"]==5)
		{
	
	$sql = "SELECT mei_foro.idevaluacion, SUM(mei_foro.valor) FROM mei_foro, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_foro.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."'
			GROUP BY mei_foro.idevaluacion";
	//print $sql."<p>";
		}
		else
		
		{
	
	$sql = "SELECT mei_foro.idevaluacion, SUM(mei_foro.valor) FROM mei_foro, mei_evaluacion, mei_evagrupo 
			WHERE mei_foro.idevaluacion = mei_evaluacion.idevaluacion AND 
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."'
			GROUP BY mei_foro.idevaluacion";
	//print $sql."<p>";
		}
	$resforo = $baseDatos->ConsultarBD($sql);
	
	$i=0;
	?>
	<script language="javascript">
	
	function verFecha(){
		if(document.frm_ingresarTaller.Preentrega.checked){
			x=document.getElementById("primfecha");
			x.style.visibility="visible";
		}else{
			x=document.getElementById("primfecha");
			x.style.visibility="hidden";
		}
	}
		function val_porcentaje()
		{
			var notaValor = new Array(<?=mysql_num_rows($resultado)?>)
			idnota = document.frm_ingresarTaller.cbo_nota.value
			valor = document.frm_ingresarTaller.cbo_valor.value
			//alert(document.frm_ingresarTaller.hid_valor.value)
	<?
	while ( list($id, $avalor) = mysql_fetch_row($resultado) )
	{
		if ( round($avalor,2)>=1 )
			$inact[$i] = $id;
		else $vact[$id] = $avalor;
			
	?>
			notaValor[<?=$id?>] = <?=round($avalor,2)?>;
	<?		
		$i++;
	}
	?>
	
	<?
	while ( list($id, $avalor) = mysql_fetch_row($resforo) )
	{
		$vact[$id] += round($avalor,2);
		if ( round($vact[$id],2)>=1 )
			$inact[$i] = $id;
		//else $vact[$i] = $avalor;
	?>
			if (notaValor[<?=$id?>]>0)
				notaValor[<?=$id?>] = notaValor[<?=$id?>] + <?=round($avalor,2)?>;
			else notaValor[<?=$id?>] = <?=round($avalor,2)?>;
	<?		
		$i++;
	}
	?>	
			if (notaValor[idnota]>0)
				total = parseFloat(notaValor[idnota])+parseFloat(valor);
			else total = parseFloat(valor);
			//alert(total)
			//document.frm_ingresarTaller.hid_valor.value = total;
			if (total>1)
			{
				//alert('El porcentajes de las Actividades asiganadas a la Nota Seleccionada excede en 100%')
				return false
			}
			else return true
		}
		
		function comprobarNota(idnota)
		{
			var notaProm = new Array(<?=mysql_num_rows($resgrupo)?>)
			
			if (idnota==0) idnota = document.frm_ingresarTaller.cbo_nota.value
			//alert(idnota)
		<?
			while( list($id) = mysql_fetch_row($resgrupo) )
			{
		?>
				notaProm[<?=$id?>] = 1
		<?
			}
		?>
			if (notaProm[idnota]==1)
			{
				document.frm_ingresarTaller.cbo_valor.disabled = true
				//document.frm_ingresarTaller.cbo_valor.style.visibility = 'hidden'
				document.getElementById('div_etiqueta').style.visibility = 'visible'
			}
			else
			{
				document.frm_ingresarTaller.cbo_valor.disabled = false
				//document.frm_ingresarTaller.cbo_valor.style.visibility = 'visible'
				document.getElementById('div_etiqueta').style.visibility = 'hidden'
			}
		}		
	</script>
	<?
	
	if ( count($inact)>0 )
		$idact_ex = implode(',', $inact);
	else $idact_ex = 0;
	/*print "lista: ".$idact_ex."<p>";
	print "count(vact): ".count($inact)."<p>";	*/
	
	
?>	
<head>
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MEIWEB</title>
<?
	$fecha_a=new FrmCalendario('frm_ingresarTaller','txt_fecha_a','formulario','false','');	
	$fecha_f=new FrmCalendario('frm_ingresarTaller','txt_fecha_f','formulario','false','');	
	$fecha_f1=new FrmCalendario('frm_ingresarTaller','txt_fecha_f1','formulario','false','');	
?>
<script>
	function verHora()
	{	
		var fecha=frm_ingresarTaller.txt_fecha_a.value.split("-");
		if(fecha[1]<10){ fecha[1]='0'+fecha[1];}
		if(fecha[2]<10){ fecha[2]='0'+fecha[2];}
		fecha=fecha[0]+"-"+fecha[1]+"-"+fecha[2];
		
		var fechaC=frm_ingresarTaller.txt_fecha_f.value.split("-");
		if(fechaC[1]<10){ fechaC[1]='0'+fechaC[1];}
		if(fechaC[2]<10){ fechaC[2]='0'+fechaC[2];}
		fechaC=fechaC[0]+"-"+fechaC[1]+"-"+fechaC[2];
		
		if(document.frm_ingresarTaller.Preentrega.checked){
				var fechaC1=frm_ingresarTaller.txt_fecha_f1.value.split("-");
				if(fechaC1[1]<10){ fechaC1[1]='0'+fechaC1[1];}
				if(fechaC1[2]<10){ fechaC1[2]='0'+fechaC1[2];}
				fechaC1=fechaC1[0]+"-"+fechaC1[1]+"-"+fechaC1[2];
				var hora_f1 = document.frm_ingresarTaller.cbo_hora_f1.value;
				var min_f1= document.frm_ingresarTaller.cbo_minuto_f1.value;
				fechaC1=fechaC1+" "+hora_f1+":"+min_f1+":00";
		}else{
			fechaC1="";
			document.frm_ingresarTaller.txt_fecha_f1.value="";
		}
		// se pasan las fechas a formato aaaa-mm-dd hh:mm:ss
		var hora_a = document.frm_ingresarTaller.cbo_hora_a.value;
		var min_a= document.frm_ingresarTaller.cbo_minuto_a.value;
		var hora_f = document.frm_ingresarTaller.cbo_hora_f.value; 
		var min_f= document.frm_ingresarTaller.cbo_minuto_f.value;
		
			fecha=fecha+" "+hora_a+":"+min_a+":00";
			fechaC=fechaC+" "+hora_f+":"+min_f+":00";
			
			if(fecha<fechaC){
				if(document.frm_ingresarTaller.Preentrega.checked){
					if(fechaC1<fechaC && fechaC1>fecha){
						return true; 
					}else{
						alert("la Primera fecha de Finalizacion debe estar entre la fecha de activacion y la fecha de finalizacion");
						return false;
					}
				}else{
					return true;
				}
			}else{
				alert("la fecha de Activacion debe ser menor q la fecha de Finalizacion");
				return false;
			}
	
		
	}



	function validar()
	{
		if ( document.frm_ingresarTaller.txt_titulo.value!="" )
		{	
			if ( val_porcentaje() )
				//alert('good')//
				return true;
			else alert('El Valor de las Actividades Asiganadas a la Nota Seleccionada Excede en 100%')
		}
		else 
			{
				alert('Debe llenar todos los campos')
				return false;
			}
	}

	function guardartaller()
	{ 
		if (validar())
		{
			if (verHora())
			{
				
				document.frm_ingresarTaller.submit();
				//alert('good')
			}
		}
	}			
	function cancelar(id, mat,materia)
			{
				document.frm_ingresarTaller.action="verDtlleActividad.php?materia=<?=$_GET["materia"]?>&idactividad="+id+"&idmateria="+mat+"&materia="+materia;
				document.frm_ingresarTaller.submit();
			}	
	function guardar()
	{
		document.frm_confirmar.action="guardarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&modo=E";
		document.frm_confirmar.submit();
	}
	function editar(idmateria,idactividad)
	{
		document.frm_confirmar.action="editarActividad.php?materia=<?=$_GET["materia"]?>&idmateria="+idmateria+"&idactividad="+idactividad;
		document.frm_confirmar.submit();
	}
	

	function ver(estado)
	{
	var nca = new Object();
	nca = document.getElementById("nombre")
	
		if (estado == 'visible')
		{
			//Propiedades capa absoluta	
			caw = nca.offsetWidth
			//*************************
	
			//Propiedades capa relativa
			crpx = ancla.offsetLeft;
			crpy = ancla.offsetTop;
			crh = ancla.offsetHeight;
			crw = ancla.offsetWidth
			//*************************
			//alert(crw)
			with (nca.style)
			{
				left = crpx + crw; 
				top = crpy //+ crh
				visibility = estado;
			}
		}else nca.style.visibility = estado;
	}
	
	function cambiar(tec){
			for (var i=0; i < 20; i++) {
			  tec.value = tec.value.replace("-", "_");
			};
	
}	
	
</script>

</head>

<body  onLoad="javascript:comprobarNota(0)">
<table class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
            <td>
            	
					<table class="tablaMarquesina" >
						<tr>
                          <? 
						  $sql233="SELECT mei_tipoactividad.tipoactividad FROM mei_tipoactividad ";
							$resul=$baseDatos->ConsultarBD($sql233);
							list($nombreAct) = mysql_fetch_row($resul);
							 if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==3)
							{
								
						  $sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
							}
							else
							{
								$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
							}
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado); ?>
							<td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($nombre))?></a><a> -> </a><a href="verActividad.php?idmateria=<?=$idmateria?>&materia=<?=$materia?>" class="link">Actividad</a><a> -> </a><a href="verDtlleActividad.php?idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Ver  <?=$nombreAct;?></a> -> Editar Actividad</td>
						</tr>
					</table><br>		
                        
                        
                        
                        
                        		
				<? 
			  	//list($idgrupo, $grupo) = explode("-", $_POST["hid_grupo"]);
				//*******************************************************************************
				
				//************************************************
				 if ($_SESSION["idtipousuario"]==5)
				 {
				$sql = "SELECT mei_actividad.idevaluacion, SUM(mei_actividad.valor) FROM mei_actividad, mei_evaluacion, mei_evavirgrupo 
						WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND 
						mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND  mei_evavirgrupo.idvirgrupo='".$idgrupo."' AND 
						mei_actividad.idactividad <> '".$_GET["idactividad"]."'
						GROUP BY mei_actividad.idevaluacion";
				//print $sql."<p>";
				 }
				 else
				  {
				$sql = "SELECT mei_actividad.idevaluacion, SUM(mei_actividad.valor) FROM mei_actividad, mei_evaluacion, mei_evagrupo 
						WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND 
						mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND  mei_evagrupo.idgrupo='".$idgrupo."' AND 
						mei_actividad.idactividad <> '".$_GET["idactividad"]."'
						GROUP BY mei_actividad.idevaluacion";
				//print $sql."<p>";
				 }
				 
				$resultado = $baseDatos->ConsultarBD($sql);
				$i=0;
				while ( list($id, $avalor) = mysql_fetch_row($resultado) )
				{
					if ( round($avalor,2)>=1 )
						$vact[$i] = $id;
					$i++;
				}
				if ( count($vact)>0 )
					$idact_ex = implode(',', $vact);
				else $idact_ex = 0;
				//print $idact_ex."<p>";
				//************************************************
				
				 if ($_SESSION["idtipousuario"]==5)
				 {
				$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval 
				 		FROM mei_evaluacion, mei_evavirgrupo
						WHERE mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo=".$idgrupo." AND 
						mei_evaluacion.tipoevaluacion=2 AND mei_evaluacion.idevaluacion NOT IN (".$idact_ex.")";
				 }
				 else
				 {
				$sql = "SELECT mei_evaluacion.idevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval 
				 		FROM mei_evaluacion, mei_evagrupo
						WHERE mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo=".$idgrupo." AND 
						mei_evaluacion.tipoevaluacion=2 AND mei_evaluacion.idevaluacion NOT IN (".$idact_ex.")";
				 }
				//print $sql;			
				$resultado = $baseDatos->ConsultarBD($sql);	
							
				if ( mysql_num_rows($resultado) > 0 )
				{//IF RESULTADO		
				
					$sql = "SELECT * FROM mei_tiposubgrupo";
					$resultado2 = $baseDatos->ConsultarBD($sql);	
					
			?>
<form name="frm_ingresarTaller" enctype="multipart/form-data" method="post" action="guardarActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&modo=E&cbo_orden=<?=$idgrupo."-".$grupo?>">
			<!---->
			<div id="nombre" align="center" style="background-color:#FFFFFF; BORDER-RIGHT: 2px ridge;
				BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; 
				WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute; width:450; height:120">
				
				<div align="center" style="height:10px" class="trTitulo">Adjuntar:</div>
				  <table class="tablaPrincipal" width="417" >
					<tr class="trInformacion">
					  <td width="20%">Archivo 1: </td>
					  <td width="80%"><input name="fil_archivo1" type="file" size="40"></td>
					</tr>
					<tr  class="trListaOscuro">
					  <td>Archivo 2: </td>
					  <td><input name="fil_archivo2" type="file" size="40"></td>
					</tr>
					<tr class="trInformacion">
					  <td>Archivo 3:</td>
					  <td><input name="fil_archivo3" type="file" size="40"></td>
					</tr>
					<tr  class="trListaOscuro">
					  <td>Archivo 4:</td>
					  <td><input name="fil_archivo4" type="file" size="40"></td>
					</tr>
					<tr  class="trListaOscuro">
					  <td>Archivo 5:</td>
					  <td><input name="fil_archivo5" type="file" size="40"></td>
					</tr>
				  </table>
				<div align="center" style="height:5px" class="trInformacion">
                  <a href="javascript:ver('hidden')" class="link"><h2>Enviar</h2></a>
				</div>
			</div>
			<!---->
					  <table class="tablaGeneral" width="100%" >
                        <tr class="trTitulo">
                          <td colspan="4" align="left" valign="middle"><b>Editar Actividad  </b>
                            <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_GET["idmateria"]?>">
                          <input name="hid_actividad" type="hidden" id="hid_actividad" value="<?=$_GET["idactividad"]?>"></td>
                        </tr>
						  <tr class="trInformacion">
                          <td width="20%"><b>Autor:</b></td>
                          <td width="80%" colspan="3"><?= $_SESSION['nombreusuario']." ".$_SESSION['apellidousuario']?>						  </td>
                        </tr>
                          <tr class="trInformacion">
                            <td width="20%"><b>Grupo:</b></td>
                            <td width="40%"><?=$grupo?>
                            <input name="hid_grupo" type="hidden" id="hid_grupo" value="<?=$idgrupo."-".$grupo?>"></td>
                            <td width="20%"><b>Subgrupo:</b></td>
                            <td width="30%"><select name="cbo_subgrupo" id="cbo_subgrupo">
                              <option value="NULL">Actividad No Grupal</option>
                              <?
							while ( list($idtipo, $subgrupo) = mysql_fetch_row($resultado2) )
							{
								print "<option class='link' value='$idtipo' ";
								if ( $idtipo == $idsubgrupo )
									print "selected>";
								else print ">";
								print "$subgrupo</option>";
							}
						?>
                            </select></td>
                          </tr>
                          
                          <tr class="trInformacion">
                            <td><b>Nota a la que pertenece: </b></td>
                            <td colspan="3"><select name="cbo_nota"  onChange="javascript:comprobarNota(this.value)">
                              <?
									while ( list($ideva, $nomeva, $val_eval) = mysql_fetch_row($resultado) )
									{//WHILE RESULTADO
										print "<option value='$ideva' ";
										if ( $ideva == $idnota )
											print "selected>";
										else print ">";
										print "$nomeva --- ".($val_eval*100)."%</option>";
									}//FIN WHILE RESULTADO
							  ?>
                            </select></td>
                          </tr> <?
                          if ($_SESSION["idtipousuario"]==5)
				 {?>
                                <tr class="trInformacion">
                          <td><strong>Actividad:</strong></td>
                          <td><?
  							$sql="SELECT mei_tipoactividad.idtipoactividad,mei_tipoactividad.tipoactividad FROM mei_tipoactividad ";
							$resultado=$baseDatos->ConsultarBD($sql);
						  ?><select onchange="verificaractividad()" id="cbo_actividad" name="cbo_actividad">
                            <?
							while (list($idtipoactividad,$nomactividad)=mysql_fetch_array($resultado))
							{

								 if($nomactividad == "Actividad"){$nomactividad = "Co-Evaluacion";}
								print "<option class='link' value='$idtipoactividad' ";
								if ( $idtipoactividad == $tipoactividad )
									print "selected>";
								else print ">";
								print "$nomactividad</option>";
							}
							?>
                          </select>						</td>
						  <td></td>
						  <td>
                            <input name="coevaluacion" type="hidden" id="coevaluacion" value="<?=$_GET["coeva"]?>">
							<input name="hid_coevaluacion2" type="hidden" id="hid_coevaluacion2" value="<?=$_POST["hid_coevaluacion"]?>">
							<input name="hid_idcoevaluacion2" type="hidden" id="hid_idcoevaluacion2" value="<?=$_POST["hid_idcoevaluacion"]?>">
						  </td>
                        </tr>
                         <? }
                          else
                          {?>
                        <tr class="trInformacion">
                          <td><b>Actividad:</b></td>
                          <td colspan="3"><?
  							$sql="SELECT mei_tipoactividad.idtipoactividad,mei_tipoactividad.tipoactividad FROM mei_tipoactividad ";
							$resultado=$baseDatos->ConsultarBD($sql);
						  ?><select name="cbo_actividad" >
                            <option class="link" value="actividad">seleccione actividad...</option>
                            <?
							while (list($idtipoactividad,$nomactividad)=mysql_fetch_array($resultado))
							{
							     if($nomactividad == "Actividad"){$nomactividad = "Co-Evaluacion";}
								print "<option class='link' value='$idtipoactividad' ";
								if ( $idtipoactividad == $tipoactividad )
									print "selected>";
								else print ">";
								print "$nomactividad</option>";
							}
							?>
                          </select>						</td>
                        </tr>
                       <? } ?>
                        <tr class="trInformacion">
                          <td><b>Valor de la Actividad:</b></td>
                          <td colspan="3"><div id="div_cbo_valor" style="float:left"><select name="cbo_valor" id="select">                         
                            <?
							for ($i=0;$i<=100;$i=$i+1) 
							{
								print "<option value='".($i/100)."'";								
								if ($i == ($valor*100))
									print "selected>";
								else print ">";
								
								print "$i%</option>";
						    }
						  ?>
                          </select></div>
                          <div id="div_etiqueta" style="visibility:hidden;"><em>La Calificacion de esta actividad se promediada con las dem&aacute;s actividades asignadas a esta Nota</em></div></td>
                        </tr>
						 <tr class="trInformacion">
                          <td><b>Nombre: </b> </td>
						  <td colspan="3"><input name="txt_titulo" type="text" id="txt_titulo" size="100" onChange="cambiar(this)"
						  value="<?=$titulo //if (!empty($_POST["hid_titulo"]) ) print $_POST["hid_titulo"];?>"></td>
                        </tr>
                        <tr class="trInformacion">
                          <td><b>Fecha Activaci&oacute;n:</b></td>
                          <td><? 
								/*if(empty($_POST['hid_fecha']))
									$calendario->m_valor='';
								else*/					
									
									$fecha_a->m_valor=$a_fecha;
									$fecha_a->CrearFrmCalendario(); 
								?></td>
                          <td><b>Hora Activaci&oacute;n </b></td>
                          <td><select name="cbo_hora_a" id="cbo_hora_a">
                            <?
								for ($i=0;$i<=23;$i++)
								{
									if ($i<10) $i = "0".$i;
									print "<option value='$i' ";
									if ($i == $hora_a) print "selected>";
									else print ">";
										
									print "$i</option>";
								}
							?>
                          </select>
							:
						<select name="cbo_minuto_a" id="cbo_minuto_a">
  <?
								for ($i=0;$i<=45;$i=$i+15)
								{
									if ($i<10) $i = "0".$i;	
									print "<option value='$i' ";
									if ($i == $min_a) print "selected>";
									else print ">";									
									print "$i</option>";
								}
							?>
						</select></td>
                        </tr>
                   
                        <tr class="trInformacion">
                          <td><b>Fecha Finalizaci&oacute;n: </b></td>
                          <td><? 
								/*if(empty($_POST['hid_fecha']))
									$calendario->m_valor='';
								else*/
									$fecha_f->m_valor=$f_fecha;
									$fecha_f->CrearFrmCalendario(); 
								?></td>
                          <td><b>Hora Finalizaci&oacute;n </b></td>
                          <td><select name="cbo_hora_f" id="select2">
                            <?
								for ($i=0;$i<=23;$i++)
								{
									if ($i<10) $i = "0".$i;
									print "<option value='$i' ";
									if ($i == $hora_f) print "selected>";
									else print ">";
										
									print "$i</option>";
								}
							?>
                            </select>
						:
						<select name="cbo_minuto_f" id="select3">
						  <?
								for ($i=0;$i<=45;$i=$i+15)
								{
									if ($i<10) $i = "0".$i;
									print "<option value='$i' ";
									if ($i == $min_f) print "selected>";
									else print ">";										
									print "$i</option>";
								}
							?>
					</select></td>
                        </tr>
                        <tr>
                        	<td align="center" ><input type="checkbox" <?if($tienefecha1)print"checked";?> value="pre" onchange="verFecha()" name="Preentrega" /></td>
                        	<td colspan="">
                        		<b>	Asignar fecha de Pre-entrega </b> 
                        	</td>
                        	<td colspan="2">Debe estar entre la Fecha Activaci&oacute;n y la Fecha Finalizaci&oacute;n</td>
                        </tr>
                             <tr class="trInformacion" id="primfecha" <?if(	!$tienefecha1)print"style=\" visibility: hidden;\"";?> >
                          <td><b> Primera Fecha Finalizaci&oacute;n: </b></td>
                          <td><? 
								/*if(empty($_POST['hid_fecha']))
									$calendario->m_valor='';
								else*/
									$fecha_f1->m_valor=$f1_fecha;
									$fecha_f1->CrearFrmCalendario(); 
								?></td>
                          <td><b>Hora Finalizaci&oacute;n <?print $horas;?></b></td>
                          <td><select name="cbo_hora_f1" id="select21">
                            <?
								for ($i=0;$i<=23;$i++)
								{
									if ($i<10) $i = "0".$i;
									print "<option value='$i' ";
									if ($i == $hora_f1) print "selected>";
									else print ">";
										
									print "$i</option>";
								}
							?>
                            </select>
						:
						<select name="cbo_minuto_f1" id="select31">
						  <?
								for ($i=0;$i<=45;$i=$i+15)
								{
									if ($i<10) $i = "0".$i;
									print "<option value='$i' ";
									if ($i == $min_f1) print "selected>";
									else print ">";										
									print "$i</option>";
								}
							?>
					</select></td>
                        </tr>
                        <tr class="trInformacion">
                          <td><b>Estado:</b></td>
                          <td colspan="3"><select name="cbo_estado" id="cbo_estado" class="link">
						  <?
						  	$v_estados[1]= "Activo";
							$v_estados[0]= "Desactivo";
							
							for ($i=(count($v_estados)-1);$i>=0;$i--)
							{
								print "<option value='$i'";
								if ($i==$estado) print " selected>";
								else print ">";
								print $v_estados[$i]."</option>";
							}						  	
						  ?>
                          </select></td>
                        </tr>
                        <tr class="trListaOscuro">
                          <td><strong>Archivo(s) Adjunto(s): </strong></td>
                          <td colspan="3"><div id="ancla" style="position:relative; width:130px">
						  <a href="javascript:ver('visible')" class="link"><img src="imagenes/adjuntar.gif" width="16" height="16" border="0">Adjuntar Archivos </a>
						  </div>
						<?
							$sql = "SELECT mei_archivoactividad.idarchivo, mei_archivoactividad.archivo, 
									mei_archivoactividad.localizacion FROM mei_archivoactividad 
									WHERE mei_archivoactividad.idactividad = '".$_GET["idactividad"]."'";
							$resArchivo = $baseDatos->ConsultarBD($sql);
							
							if ( mysql_num_rows($resArchivo) )
							{//IF HAY ARCHIVOS
						?>						  
							  <table class="tablaPrincipal">
						<?
								while ( list($idarchivo, $archivo, $local) = mysql_fetch_row($resArchivo) )
								{
						?>
								<tr>
								  <td><input type="checkbox" name="chk_archivo[]" value="<?=$idarchivo?>" checked="checked"></td>
								  <td><a href="../../datosMEIWEB/archivosActividades/<?=$local?>" class="link" target="_blank"><?=$archivo?></a></td>
								</tr>
						<?
								}						
						?>
							  </table>
						<?
							}//IF HAY ARCHIVOS
						?>
						  </td>
                        </tr>
                       <tr class="trInformacion">
						  <td colspan="4"><?								 
								 //if (!empty($_POST["hid_editor"]) ) 
								 $editor->Value = stripslashes(base64_decode($descripcion));
								 $editor->crearEditor();
							?>						  </td>
					    </tr>
                     </table>
					 <table class="tablaGeneral">
						  <tr class="trInformacion">
							<td width="50%" align="center" valign="middle"><div align="center">
							  <input type="button" name="btn_guardarTaller" value="Aceptar" onClick="javascript:guardartaller()">						    
						    </div></td>
							<td width="50%" align="center" valign="middle"><div align="center">
							  <input type="button" name="btn_cancelar" value="Cancelar" 
							  onClick="javascript:cancelar('<?=$_GET["idactividad"]?>', '<?=$_GET["idmateria"]?>','<?=$_GET["materia"]?>')">
							</div></td>
						  </tr>
					</table>

			      </form>
				  <?
					}//FIN IF RESULTADO
					else
					{
				  ?>
  				  <table class="tablaGeneral" width="200">
				  	<tr>
						<td colspan="2" align="center" valign="middle">No existen Notas para Actividades definidas para el grupo <strong><?=$grupo?></strong>. Para crear Notas haga click <a href="../notas/index.php?idmateria=<?=$_POST["hid_materia"]?>&materia=<?=$_GET["materia"]?>" class="link">aqui</a> </td>
					</tr>
                  </table>&nbsp;
   				  <table class="tablaGeneral" width="200">
                    <tr align="center" valign="middle">
                    	<td><a href="ingresarActividad.php?idmateria=<?=$_POST["hid_materia"]?>&materia=<?=$_GET["materia"]?>" class="link">Volver</a></td>
                    </tr>
                  </table>
				  <?
					}
				//}
				  ?>
				  &nbsp;
				  </td>
                         <td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
				  </tr>
				  </table>
				
				</body>

<? 
  }
?>
</html>

