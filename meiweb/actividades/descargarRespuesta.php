<?
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
    
    function download_file($archivo, $downloadfilename = null) {
    
        if (file_exists($archivo)) {
            $downloadfilename = $downloadfilename !== null ? $downloadfilename : basename($archivo);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $downloadfilename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($archivo));
    
            ob_clean();
            flush();
            readfile($archivo);
            exit;
        }
    
    }	
	if(comprobarSession())
	{
		/*registrarBitacora(2,5);
		registrarBitacora(2,1);*/
		
		/*redireccionar("../../datosMEIWEB/archivosBiblioteca/".$_GET['archivo']);*/
        $archivourl = "../../datosMEIWEB/archivosActividades/archivosRespuestas/".$_GET['archivo'];
        $codigo = $_GET['idactividad'];
        $archivoconcodigo = $_GET['archivo'];
        $usuario = $_GET['codigo'];
        $arrnombrearchivo = explode("_".$codigo."_".$usuario."_",$archivoconcodigo);
        $nombrearchivo = $arrnombrearchivo[1];
        download_file($archivourl,$nombrearchivo);	
		//descargar_archivo("archivos/".$_GET['archivo']);
		/*$sql="UPDATE mei_biblioteca SET mei_biblioteca.numdescargas=mei_biblioteca.numdescargas+1
			  WHERE mei_biblioteca.idarchivo=".$_GET['codigoArchivo'];
		$resultado=$baseDatos->ConsultarBD($sql);

		/*@header ("Content-Disposition: attachment; filename=".$_GET['archivo']."\n\n"); 
		@header ("Content-Type: application/octet-stream");
		@header ("Content-Length: ".filesize("archivos/".$_GET['archivo']));
		readfile("archivos/".$_GET['archivo']);			*/
	}
	else
	{
		redireccionar('../login/');					
	}
	

?>