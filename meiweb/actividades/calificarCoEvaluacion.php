<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('classActividadGrupal.php'); 
	   include_once ('../menu1/Menu1.class.php');
	
$baseDatos= new BD();	
if(comprobarSession())
{	
	mysql_query("SET NAMES 'utf8'");
	
   if(isset($_SESSION['cbo_ordenar']))
   {
    $_SESSION['cbo_ordenar'];    
   }
  
  if(empty($_POST["cbo_ordenar"]) && empty($_SESSION["cbo_ordenar"]))
  {
      $_POST["cbo_ordenar"]=3;
  }
  else if(empty($_POST["cbo_ordenar"]) && !empty($_SESSION["cbo_ordenar"]))
  {
    $_POST["cbo_ordenar"]=$_SESSION["cbo_ordenar"];
  }
  
	switch($_POST['cbo_ordenar'])
	{
		case 1:
			$orden = "mei_usuario.idusuario";
			$activoCodigo = "selected";
			$_SESSION['cbo_ordenar']=1;
		break;
		default:
		
			$orden = "mei_usuario.primernombre";
			$activoNombre = "selected";	
			$_SESSION['cbo_ordenar']=2;
		break;
		case 3:
			$orden = "mei_usuario.primerapellido";	
			$activoApellido = "selected"; 
            $_SESSION['cbo_ordenar']=3;     	
	}


?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script language="javascript">
	window.name = "envioActividad";
	function calificar(mod, idusuario, idactividad, idmateria, nota, idsubgrupo, idtiposubgrupo)
	{
	   var izquierda = (screen.availWidth - 200) / 2; 
	   var arriba = (screen.availHeight - 180) / 2; 

		calificacion = window.open("calificarActividad.php?mod="+mod+"&idusuario="+idusuario+"&idactividad="+idactividad+"+&idmateria="+idmateria+"&nota="+nota+"&idsubgrupo="+idsubgrupo+"&idtiposubgrupo="+idtiposubgrupo+"","Calificar","width=250,height=250,left="+izquierda+",top="+arriba+",scrollbars=NO,menubars=NO,status=NO,statusbar=NO,status=NO,resizable=NO,location=NO")
	}

	function eliminar(mod, idusuario, idactividad, idmateria, nota, idsubgrupo, idtiposubgrupo)
	{
		location.replace("guardarCalificacion.php?materia=<?=$_GET["materia"]?>&modo="+mod+"&idusuario="+idusuario+"&idactividad="+idactividad+"+&idmateria="+idmateria+"&nota="+nota+"&idsubgrupo="+idsubgrupo+"&idtiposubgrupo="+idtiposubgrupo);
	}
	
	function chekear()
	{
		var todo=document.getElementById('chk_grupo');
		if (todo.checked==true)
			{
				for(i=0;i<document.frm_lista.elements.length;i++)
				{
					if(document.frm_lista.elements[i].id=='chk_lista')
					document.frm_lista.elements[i].checked=true;
				}
			}
		else
			{
				for(i=0;i<document.frm_lista.elements.length;i++)
				{
					if(document.frm_lista.elements[i].id=='chk_lista')
					document.frm_lista.elements[i].checked=false;
				}
			}
	}
	
	function eliminarV(mod, idusuario, idactividad, idmateria, nota, idsubgrupo, idtiposubgrupo)
	{
		if(confirm("Se Eliminarón las Respuestas de los Estudiantes Seleccionados"))
		{
		var contador=0;
				for(i=0;i<document.frm_lista.elements.length;i++)
				{
						if (document.frm_lista.elements[i].checked==true)
						{
							contador++;							
						}
				}
		if(contador>0)
		{
			document.frm_lista.action="guardarCalificacion.php?materia=<?=$_GET["materia"]?>&modo="+mod+"&idusuario="+idusuario+"&idactividad="+idactividad+"+&idmateria="+idmateria+"&nota="+nota+"&idsubgrupo="+idsubgrupo+"&idtiposubgrupo="+idtiposubgrupo;
			document.frm_lista.submit();
		}
		else
			alert('No ha seleccionado ningún Alumno')
		}
	}
</script>
<?
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraEstudiante' , '' );
?>
<?php
	/*CREAR TABLA CO-EVALUACIÓN*/
	$tabla="CREATE TABLE IF NOT EXISTS `mei_coevaluacion` (
		  `idcoevaluacion` int(11) NOT NULL AUTO_INCREMENT,
		  `idactividad` int(11) NOT NULL,
		  `idactividadevaluar` int(11) NULL,
		  `idestudiante` int(11) NOT NULL,
		  `idestudiantecalifar` int(11) NOT NULL,
		  `fechacalificar` varchar(11) NULL,
		  PRIMARY KEY (`idcoevaluacion`)
		)";
	$rescreartabla = $baseDatos->ConsultarBD($tabla);
	/*FIN CREAR TABLA CO-EVALUACIÓN*/
?>
</head>
<body>
<? if($_SESSION['idtipousuario']==5)
{
	$sql = "SELECT mei_tipoactividad.tipoactividad, mei_actividad.titulo, mei_virgrupo.nombre FROM mei_tipoactividad, 
			mei_actividad, mei_virgrupo, mei_evaluacion, mei_evavirgrupo WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion 
			AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo 
			AND mei_actividad.idtipoactividad = mei_tipoactividad.idtipoactividad AND mei_actividad.idactividad = '".$_GET["idactividad"]."'";
			
}
else
{
	$sql = "SELECT mei_tipoactividad.tipoactividad, mei_actividad.titulo, mei_grupo.nombre FROM mei_tipoactividad, 
			mei_actividad, mei_grupo, mei_evaluacion, mei_evagrupo WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion 
			AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = mei_grupo.idgrupo 
			AND mei_actividad.idtipoactividad = mei_tipoactividad.idtipoactividad AND mei_actividad.idactividad = '".$_GET["idactividad"]."'";
			
}
			
	$resprevio = $baseDatos->ConsultarBD($sql);
	list($tipoactividad,$titulo,$grupo) = mysql_fetch_row($resprevio);
?>
	<table class="tablaPrincipal">

			<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
            <td>
            
					<table class="tablaMarquesina" >
						<tr>
                        <?
                           if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==3)
		{
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		}
		else
		{
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		}
         $resultado = $baseDatos->ConsultarBD($sql);
		list($materia) = mysql_fetch_row($resultado);
		?>
                 <td><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"><?=ucwords(strtolower($materia))?></a><a> -> </a><a href="verActividad.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"> Actividades</a><a> -> </a><a href="verDtlleActividad.php?idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"> Ver  <?=$tipoactividad;?></a> -> Co-Evaluación </td>
              </tr>
            </table>&nbsp;
	          <table class="tablaGeneral">
		        <form name="frm_orden" method="post" action="resultadosActividad.php?idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&idtiposubgrupo=<?=$_GET["idtiposubgrupo"]?>&materia=<?=$_GET["materia"]?>">
                <tr class="trTitulo">
                  <td colspan="2">Calificar: <em><?=$titulo?> </em> del Grupo <?=$grupo?></td>
                  </tr>
                <tr class="trSubTitulo">
                  <td width="20%">Ordenar Lista Por: </td>
                  <td width="80%"><select name="cbo_ordenar" onChange="javascript:document.frm_orden.submit()">
                    <option value="1" <?=$activoCodigo?>>C&oacute;digo</option>
                    <option value="2" <?=$activoNombre?>>Nombre</option>
                    <option value="3" <?=$activoApellido?>>Apellido</option>
                  </select></td>
                </tr>
	            </form>
              </table>
<form name="frm_lista" method="post" action="">
<?
	
function verNoGrupal($sql)
{//FUNCION verNoGrupal
	global $baseDatos;
	$resultado = $baseDatos->ConsultarBD($sql);	
	$resultado2 = $baseDatos->ConsultarBD($sql);	
	
	if ( mysql_num_rows($resultado) > 0 )
	{//IF mysql_num_rows($resultado) > 0
	
	if( $_SESSION["idtipousuario"]==5)
	{
			$sql = "SELECT  mei_actividad.titulo FROM mei_tipoactividad, 
			mei_actividad, mei_virgrupo, mei_evaluacion, mei_evavirgrupo WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion 
			AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo 
			AND mei_actividad.idtipoactividad = mei_tipoactividad.idtipoactividad AND mei_actividad.idactividad = '".$_GET["idactividad"]."'";
		
	}
	else
	{
			$sql = "SELECT  mei_actividad.titulo FROM mei_tipoactividad, 
			mei_actividad, mei_grupo, mei_evaluacion, mei_evagrupo WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion 
			AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = mei_grupo.idgrupo 
			AND mei_actividad.idtipoactividad = mei_tipoactividad.idtipoactividad AND mei_actividad.idactividad = '".$_GET["idactividad"]."'";
	}			
	$resprevio = $baseDatos->ConsultarBD($sql);
	list($titulo2) = mysql_fetch_row($resprevio);
	
	?>
	<table border="1" class="tablaGeneral">
	  <tr class="trTitulo">
	    <td colspan="9" align="left" valign="middle" class="trSubTitulo"><img src="imagenes/usuarios.gif" width="16" height="16">Lista de Alumnos  </td>
	    </tr>
	  
	  <tr class="trSubTitulo">
	    <td colspan="9" align="left" valign="middle" class="trSubTitulo">
		<input name="chk_grupo" type="checkbox" id="chk_grupo" onClick="javascript:chekear()" value="checkbox">
		Todo el Grupo </td>
	    </tr>
	  <tr class="trSubTitulo">
		<td colspan="2" align="center" valign="middle" class="trSubTitulo">C&oacute;digo</td>
		<td class="trSubTitulo" width="32%" align="center" valign="middle">Alumno(s)</td>
		<td class="trSubTitulo" width="33%" align="center" valign="middle">Calificador </td>
		<td class="trSubTitulo" width="33%" align="center" valign="middle">Nota de Calificador </td>
	    <td class="trSubTitulo" width="5%" align="center" valign="middle">Editar</td>
   	    <td class="trSubTitulo" width="5%" align="center" valign="middle">Comentar Nota   </td>
   	    <!--<td class="trSubTitulo" width="5%" align="center" valign="middle">Agregar Nota   </td>-->
	  </tr>
	  <?
			$flag=0;
			$repetido=0;
			$sql6="SELECT mei_coevaluacion.idestudiantecalifar FROM mei_coevaluacion WHERE mei_coevaluacion.idactividad=".$_GET['idactividad'];
			$consulta=$baseDatos->ConsultarBD($sql6);
			while (list($idus1)=mysql_fetch_array($consulta))
			{	

				$contador[]= $idus1;  		
	  			
			}	

			$total=count($contador);

			for ($m=0; $m < $total; $m++) { 

				for ($n=0; $n < $total; $n++) { 
					
					if ($contador[$m]==$contador[$n] && $m != $n) {
						
						$repetido=1;
					}
				}
		
			}


			$sql6="SELECT mei_coevaluacion.idestudiante FROM mei_coevaluacion WHERE mei_coevaluacion.idactividad=".$_GET['idactividad'];
			$consulta=$baseDatos->ConsultarBD($sql6);
			while (list($iduser)=mysql_fetch_array($consulta))
			{		
				$estudiantes[]=$iduser;  		
	  			
			}	
			$varia = count($estudiantes);
			for ($l=0; $l <$varia ; $l++) { 
				$sql3="SELECT mei_coevaluacion.nota FROM mei_coevaluacion WHERE mei_coevaluacion.idestudiante=".$estudiantes[$l]." AND mei_coevaluacion.idactividad=".$_GET['idactividad'];
	  			$consul=$baseDatos->ConsultarBD($sql3);
	  			list($notan)=mysql_fetch_array($consul);
	  			if (!empty($notan)) {
	  				$flag=1;
	  			}
			}

			if ($repetido==1) {
				$_SESSION['repetido']=1;
				$idcodigo = $estudiantes;
				shuffle($estudiantes);
				$cantidad = count($estudiantes);
				for($a=0; $a<$cantidad;$a++){
					if($idcodigo[$a]==$estudiantes[$a]){
						shuffle($estudiantes);
						$a = 0;
					}
				}
				for ($z=0; $z < $cantidad; $z++) { 
					$idest=$estudiantes[$z];
					$idcod=$idcodigo[$z];
					
					$sql="UPDATE mei_coevaluacion SET  mei_coevaluacion.idestudiantecalifar =".$idest." WHERE mei_coevaluacion.idestudiante =".$idcod." AND mei_coevaluacion.idactividad =".$_GET['idactividad'];
					$baseDatos->ConsultarBD($sql);  
				}
			}

			$i = 0;
			while ( list($idusu2, $pnombre2, $snombre2, $papellido2, $sapellido2, $fechares2, $nota2) = mysql_fetch_row($resultado2) ){

				$sql6="SELECT mei_actividad.idevaluacion FROM mei_actividad WHERE mei_actividad.idactividad=".$_GET['idactividad']."";
				$consulta=$baseDatos->ConsultarBD($sql6);
				list($ideval)=mysql_fetch_array($consulta);

				$sql7="SELECT mei_evaluacion.idtema FROM mei_evaluacion WHERE mei_evaluacion.idevaluacion='$ideval'";
				$consulta2=$baseDatos->ConsultarBD($sql7);
				list($limite)=mysql_fetch_array($consulta2);

				$sql1="SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema WHERE mei_tema.idmateria =".$_GET['idmateria']."
				AND mei_tema.tipo = 1 AND estado=0 ORDER BY(mei_tema.orden)";
				$consultatemas=$baseDatos->ConsultarBD($sql1);
				while(list($idtema,$nomtema)=mysql_fetch_array($consultatemas)){
				
				$promedio=0;
				
				$sql1="SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion
				WHERE mei_evaluacion.idtema='".$idtema."'";
				$consulta=$baseDatos->ConsultarBD($sql1);
				list($ideva)=mysql_fetch_array($consulta);
				
				$sql1="SELECT COUNT(mei_actividad.idactividad) FROM mei_actividad 
				WHERE mei_actividad.idevaluacion='".$ideva."' AND mei_actividad.visibilidad!=1";
				$consulta1=$baseDatos->ConsultarBD($sql1);
				list($cant1)=mysql_fetch_array($consulta1);
				$sql1="SELECT COUNT(mei_evaprevio.idprevio) FROM mei_evaprevio 
				WHERE mei_evaprevio.idevaluacion='".$ideva."' AND mei_evaprevio.visibilidad!=1";
				$consulta2=$baseDatos->ConsultarBD($sql1);
				list($cant2)=mysql_fetch_array($consulta2);
				
				$cantidad=$cant1+$cant2;
				
				
				$sql11="SELECT mei_actividad.idactividad, mei_actividad.valor FROM mei_actividad 
				WHERE mei_actividad.idevaluacion='".$ideva."' AND mei_actividad.visibilidad!=1";
				$consultaactividades=$baseDatos->ConsultarBD($sql11);
				
				$sql22="SELECT mei_evaprevio.idprevio, mei_evaprevio.valor FROM mei_evaprevio 
				WHERE mei_evaprevio.idevaluacion='".$ideva."' AND mei_evaprevio.visibilidad!=1";
				$consultaprevios=$baseDatos->ConsultarBD($sql22);
				
				while(list($idactividad,$valoractividad)=mysql_fetch_array($consultaactividades)){
					$sql2="SELECT mei_usuactividad.nota FROM mei_usuactividad WHERE 
					mei_usuactividad.idactividad='".$idactividad."' AND mei_usuactividad.idusuario='".$idusu2."'";
					$consulta=$baseDatos->ConsultarBD($sql2);
					list($notaactividad)=mysql_fetch_array($consulta);
					if($valoractividad==0){
						$promedio+=$notaactividad*(1/$cantidad);
					}
					else {
						$promedio+=$notaactividad*$valoractividad;
					}
					
				}
				while(list($idprevio,$valorprevio)=mysql_fetch_array($consultaprevios)){
					$sql2="SELECT MAX(mei_usuprevio.nota), mei_usuprevio.notaextra FROM mei_usuprevio WHERE 
					mei_usuprevio.idprevio='".$idprevio."' AND mei_usuprevio.idusuario='".$idusu2."'";
					$consulta=$baseDatos->ConsultarBD($sql2);
					list($notaprevio)=mysql_fetch_array($consulta);
					if($valorprevio==0){
						$promedio+=$notaprevio*(1/$cantidad);
					}
					else {
						$promedio+=$notaprevio*$valorprevio;
					}
					
				}
				$promediotemas[$idtema]=$promedio;
				
			}
$cont=0;
$var=0;
$sql2="SELECT mei_tema.idtema, mei_tema.notamin FROM mei_tema WHERE mei_tema.idmateria=".$_GET['idmateria']." AND mei_tema.tipo = 1 AND estado=0 ORDER BY(mei_tema.orden)";
$consultanota=$baseDatos->ConsultarBD($sql2);
$l=0;
while(list($temaid,$notamini)=mysql_fetch_array($consultanota)){


	if($temaid==$limite && $cont==0){
		$var = 1;
	}

	if($temaid<$limite){
		
		if($promediotemas[$temaid]>=$notamini){
			$var = 1;
		}
		else{	
			$var = 0;
		}
	}
	$cont++;	
}


				if($var==1 || $_SESSION['idtipousuario']==2){
				$idcodigocalificar[] = $idusu2;
			}
			
		//Modificación para que no quede calificador a uno mismo	

		}

			$idcodigos = $idcodigocalificar;
			shuffle($idcodigocalificar);
			$cantidadalumnos = count($idcodigocalificar);
			for($j=0; $j<$cantidadalumnos;$j++){
				if($idcodigos[$j]==$idcodigocalificar[$j]){
					shuffle($idcodigocalificar);
					$j = 0;
				}
			}
		
		//fin modificación	
		
			while ( list($idusu, $pnombre, $snombre, $papellido, $sapellido, $fechares, $nota) = mysql_fetch_row($resultado) )
			{//WHILE 1

				$sql6="SELECT mei_actividad.idevaluacion FROM mei_actividad WHERE mei_actividad.idactividad=".$_GET['idactividad']."";
			$consulta=$baseDatos->ConsultarBD($sql6);
			list($ideval)=mysql_fetch_array($consulta);

$sql7="SELECT mei_evaluacion.idtema FROM mei_evaluacion WHERE mei_evaluacion.idevaluacion='$ideval'";
$consulta2=$baseDatos->ConsultarBD($sql7);
			list($limite)=mysql_fetch_array($consulta2);

$sql1="SELECT mei_tema.idtema, mei_tema.titulo FROM mei_tema WHERE mei_tema.idmateria =".$_GET['idmateria']."
			AND mei_tema.tipo = 1 AND estado=0 ORDER BY(mei_tema.orden)";
			$consultatemas=$baseDatos->ConsultarBD($sql1);
			while(list($idtema,$nomtema)=mysql_fetch_array($consultatemas)){
				
				$promedio=0;
				
				$sql1="SELECT mei_evaluacion.idevaluacion FROM mei_evaluacion
				WHERE mei_evaluacion.idtema='".$idtema."'";
				$consulta=$baseDatos->ConsultarBD($sql1);
				list($ideva)=mysql_fetch_array($consulta);
				
				$sql1="SELECT COUNT(mei_actividad.idactividad) FROM mei_actividad 
				WHERE mei_actividad.idevaluacion='".$ideva."' AND mei_actividad.visibilidad!=1";
				$consulta1=$baseDatos->ConsultarBD($sql1);
				list($cant1)=mysql_fetch_array($consulta1);
				$sql1="SELECT COUNT(mei_evaprevio.idprevio) FROM mei_evaprevio 
				WHERE mei_evaprevio.idevaluacion='".$ideva."' AND mei_evaprevio.visibilidad!=1";
				$consulta2=$baseDatos->ConsultarBD($sql1);
				list($cant2)=mysql_fetch_array($consulta2);
				
				$cantidad=$cant1+$cant2;
				
				
				$sql11="SELECT mei_actividad.idactividad, mei_actividad.valor FROM mei_actividad 
				WHERE mei_actividad.idevaluacion='".$ideva."' AND mei_actividad.visibilidad!=1";
				$consultaactividades=$baseDatos->ConsultarBD($sql11);
				
				$sql22="SELECT mei_evaprevio.idprevio, mei_evaprevio.valor FROM mei_evaprevio 
				WHERE mei_evaprevio.idevaluacion='".$ideva."' AND mei_evaprevio.visibilidad!=1";
				$consultaprevios=$baseDatos->ConsultarBD($sql22);
				
				while(list($idactividad,$valoractividad)=mysql_fetch_array($consultaactividades)){
					$sql2="SELECT mei_usuactividad.nota FROM mei_usuactividad WHERE 
					mei_usuactividad.idactividad='".$idactividad."' AND mei_usuactividad.idusuario='".$idusu."'";
					$consulta=$baseDatos->ConsultarBD($sql2);
					list($notaactividad)=mysql_fetch_array($consulta);
					if($valoractividad==0){
						$promedio+=$notaactividad*(1/$cantidad);
					}
					else {
						$promedio+=$notaactividad*$valoractividad;
					}
					
				}
				while(list($idprevio,$valorprevio)=mysql_fetch_array($consultaprevios)){
					$sql2="SELECT MAX(mei_usuprevio.nota), mei_usuprevio.notaextra FROM mei_usuprevio WHERE 
					mei_usuprevio.idprevio='".$idprevio."' AND mei_usuprevio.idusuario='".$idusu."'";
					$consulta=$baseDatos->ConsultarBD($sql2);
					list($notaprevio)=mysql_fetch_array($consulta);
					if($valorprevio==0){
						$promedio+=$notaprevio*(1/$cantidad);
					}
					else {
						$promedio+=$notaprevio*$valorprevio;
					}
					
				}
				$promediotemas[$idtema]=$promedio;
				
			}
$cont=0;
$var=0;
$sql2="SELECT mei_tema.idtema, mei_tema.notamin FROM mei_tema WHERE mei_tema.idmateria=".$_GET['idmateria']." AND mei_tema.tipo = 1 AND estado=0 ORDER BY(mei_tema.orden)";
$consultanota=$baseDatos->ConsultarBD($sql2);
$l=0;
while(list($temaid,$notamini)=mysql_fetch_array($consultanota)){


	if($temaid==$limite && $cont==0){
		$var = 1;
	}

	if($temaid<$limite){
		
		if($promediotemas[$temaid]>=$notamini){
			$var = 1;
		}
		else{	
			$var = 0;
		}
	}
	$cont++;	
}

				if($var==1 || $_SESSION['idtipousuario']==2){
		
				if ($_POST["cbo_ordenar"] !=3 ) $ncompleto = $pnombre." ".$snombre." ".$papellido." ".$sapellido;
				else $ncompleto = $papellido." ".$sapellido." ".$pnombre." ".$snombre;
			
				if ( ($i%2)==0 ) $clase = "trListaOscuro";
				else $clase = "trListaClaro";
	  ?>
	  <tr class="<?=$clase?>">
		<td width="6%" align="center" valign="middle" class="<?=$clase?>"><?=$i+1?>
		  <input name="chk_estudiante[]" type="checkbox" id="chk_lista" value="<?=$idusu?>">		</td>
		<td width="7%" align="center" valign="middle" class="<?=$clase?>"><?=$idusu?></td>
		<td class="<?=$clase?>" align="left" valign="middle"><?=$ncompleto?></td>
		<td class="<?=$clase?>" align="center" valign="middle">
			<?php 
			
			
			$sqlcoeva_verificar = "SELECT* FROM mei_coevaluacion WHERE mei_coevaluacion.idactividad =".$_GET["idactividad"]." AND mei_coevaluacion.idestudiante =".$idusu;
			$datoverificar_coeva = $baseDatos->ConsultarBD($sqlcoeva_verificar);
			
			if(mysql_num_rows($datoverificar_coeva) == 0 && $flag == 0){ 
				$codiigoaacalificar = $idcodigocalificar[$i];
				$sqlasignar_codigo = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
				mei_usuario.primerapellido, mei_usuario.segundoapellido FROM mei_usuario 
				WHERE mei_usuario.idusuario = $codiigoaacalificar ";			
				$datosestudianteacalificar = $baseDatos->ConsultarBD($sqlasignar_codigo);
				list($idusucalificar, $pnombrecalificar, $snombrecalificar, $papellidocalificar, $sapellidocalificar) = mysql_fetch_row($datosestudianteacalificar);
				echo $ncompletocalificar = $papellidocalificar." ".$sapellidocalificar." ".$pnombrecalificar." ".$snombrecalificar;
				
				$sqlinsert_codigo = "INSERT INTO mei_coevaluacion ( idactividad  , idestudiante, idestudiantecalifar)
				VALUES ('".$_GET["idactividad"]."','".$idusu."','".$codiigoaacalificar."')";
				$baseDatos->ConsultarBD($sqlinsert_codigo);				
			}
			else{ 
				$sqlasignar_codigo = "SELECT mei_coevaluacion.idestudiantecalifar FROM mei_coevaluacion WHERE mei_coevaluacion.idactividad =".$_GET["idactividad"]." AND mei_coevaluacion.idestudiante = $idusu ";	
				$consulta_sqlasignar_codigo = $baseDatos->ConsultarBD($sqlasignar_codigo);				
				list($codiigoaacalificar) = mysql_fetch_row($consulta_sqlasignar_codigo);

				$sqlasignar_codigo = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
				mei_usuario.primerapellido, mei_usuario.segundoapellido FROM mei_usuario 
				WHERE mei_usuario.idusuario = $codiigoaacalificar ";			
				$datosestudianteacalificar = $baseDatos->ConsultarBD($sqlasignar_codigo);
				list($idusucalificar, $pnombrecalificar, $snombrecalificar, $papellidocalificar, $sapellidocalificar) = mysql_fetch_row($datosestudianteacalificar);
				echo $ncompletocalificar = $papellidocalificar." ".$sapellidocalificar." ".$pnombrecalificar." ".$snombrecalificar;			
			}
		
					
			?>		
		</td>
		<td>
		<?php
			$sqlasignar_nota = "SELECT mei_coevaluacion.nota FROM mei_coevaluacion WHERE mei_coevaluacion.idestudiante =".$idusu." AND  mei_coevaluacion.idactividad=".$_GET["idactividad"];	
			$consulta_sqlasignar_nota = $baseDatos->ConsultarBD($sqlasignar_nota);				
			list($notaaacalificar) = mysql_fetch_row($consulta_sqlasignar_nota);	
			echo $notaaacalificar;
		?>
		</td>
		<?
			if (!empty($fechares))
			{
		?>	   
  
	    <td class="<?=$clase?>" align="center" valign="middle">
		<a href="javascript:calificar('', '<?=$idusu?>','<?=$_GET["idactividad"]?>', '<?=$_GET["idmateria"]?>','<?=$nota?>','','')" class="link">
		<img src="imagenes/modificar.gif" alt="Editar Nota" width="16" height="16" border="0">		</a>		</td>
        <td class="<?=$clase?>" align="center" valign="middle"><? if (isset($nota)) {?><a href=" ../correo/enviarComentarioNota.php?destinatario=<?=$idusu.'@meiweb'?>&nombreactividad=<? print $titulo2?>&modo=UNO&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link" target="_blank"> <img src="imagenes/Comentario.gif" alt="Comentar Nota" width="16" height="16" border="0">		</a>
																	<? }else print "---";?></td>
				

		<?
			}
			else 
			{
		?>
	    <td class="<?=$clase?>" align="center" valign="middle">---</td>	   
	    <td class="<?=$clase?>" align="center" valign="middle">
		<a href="javascript:calificar('INS', '<?=$idusu?>','<?=$_GET["idactividad"]?>', '<?=$_GET["idmateria"]?>','<?=$nota?>','','')" class="link">   
		<img src="../evaluaciones/imagenes/undo.jpg" alt="Insertar Nota" width="16" height="16" border="0">		</a>		</td>
		<?
			}
		?>
		<!--<td  align="center" valign="middle" class="<?=$clase?>"><input name="btn_asignarnota" type="button" id="btn_asignarnota" value="Asignar Nota" onClick="javascript:asignarnota('<?=$idusu?>','<?=$notaaacalificar?>')"></td>-->
	  </tr>
	  <?
				$i++;
			}
			}//FIN WHILE 1
	  ?>
	  <tr class="trSubTitulo">
		<td colspan="9" align="center" valign="middle" class="trSubTitulo"><input name="btn_eliminar" type="button" id="btn_eliminar" value="Eliminar" onClick="javascript:eliminarV('DEL', '','<?=$_GET["idactividad"]?>', '<?=$_GET["idmateria"]?>','','','')"></td>		
		</tr>
	</table>
     
    
			
           <? 

 
		}//FIN IF mysql_num_rows($resultado) > 0
		else
		{
	?>
	<table class="tablaTitulo">
			<tr>
				<td align="center" valign="middle">No se han recibido respuestas a esta actividad</td>
			</tr>
	</table>
	<?
		}
}//FIN FUNCION verNoGrupal

function verGrupal($sql)
{//FUNCION VerGrupal

	global $baseDatos, $orden;
	$resultado = $baseDatos->ConsultarBD($sql);
	if($_SESSION["idtipousuario"]==5)
	{
		$sql = "SELECT  mei_actividad.titulo FROM mei_tipoactividad, 
			mei_actividad, mei_virgrupo, mei_evaluacion, mei_evavirgrupo WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion 
			AND mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_virgrupo.idvirgrupo 
			AND mei_actividad.idtipoactividad = mei_tipoactividad.idtipoactividad AND mei_actividad.idactividad = '".$_GET["idactividad"]."'";
			
	}
	else
	{
		$sql = "SELECT  mei_actividad.titulo FROM mei_tipoactividad, 
			mei_actividad, mei_grupo, mei_evaluacion, mei_evagrupo WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion 
			AND mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = mei_grupo.idgrupo 
			AND mei_actividad.idtipoactividad = mei_tipoactividad.idtipoactividad AND mei_actividad.idactividad = '".$_GET["idactividad"]."'";
			
	}
	$resprevio = $baseDatos->ConsultarBD($sql);
	list($titulo3) = mysql_fetch_row($resprevio);
?>
    <table class="tablaGeneral" border="1">
		<tr class="trSubTitulo">
		  <td colspan="9" align="left" valign="middle" class="trTitulo"><img src="imagenes/usuarios.gif" width="16" height="16">Lista de Alumnos</td>
	    </tr>
		
		<tr class="trSubTitulo">
		  <td colspan="9" align="left" valign="middle" class="trSubTitulo">
		  <input name="chk_grupo" type="checkbox" id="chk_grupo" onClick="javascript:chekear()" value="checkbox">
		  Todo el Grupo		  </td>
	    </tr>
		<tr class="trSubTitulo">
			<td colspan="2" align="center" valign="middle" class="trSubTitulo">Subgrupo</td>
			<td class="trSubTitulo" width="38%" align="center" valign="middle">Alumno(s)</td>
			<td class="trSubTitulo" width="30%" align="center" valign="middle">Fecha de Env&igrave;o </td>
			<td class="trSubTitulo" width="5%" align="center" valign="middle">Nota</td>		
		    <td class="trSubTitulo" width="5%" align="center" valign="middle">Ver Env&iacute;o </td>
		    <td class="trSubTitulo" width="5%" align="center" valign="middle">Editar</td>
            <td class="trSubTitulo" width="5%" align="center" valign="middle">Comentar Nota</td>
     	    <td class="trSubTitulo" width="5%" align="center" valign="middle">Eliminar</td>
		</tr>
<?	

					
		
		$i = 0;
		while ( list($idsubgrupo, $subgrupo, $idgrupo, $idtiposubgrupo) = mysql_fetch_array($resultado) )
		
		{//WHILE 1
		
		     $sql = "SELECT   mei_relususub.idusuario, mei_usuactividad.fecharespuesta FROM mei_usuario, mei_relususub 
					LEFT JOIN mei_usuactividad ON mei_relususub.idusuario = mei_usuactividad.idusuario 
					AND mei_usuactividad.idactividad = ".$_GET["idactividad"]."
					WHERE mei_usuario.idusuario = mei_relususub.idusuario AND 
					mei_usuario.idusuario<>".$_SESSION['idusuario']."  AND mei_relususub.lider=1 AND
					mei_relususub.idsubgrupo = ".$idsubgrupo;
					$resulta = $baseDatos->ConsultarBD($sql);
					list($idlider,$fecha_r_lider)= mysql_fetch_array($resulta);
					
			$sql = "SELECT mei_relususub.idsubgrupo, mei_relususub.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre,
					mei_usuario.primerapellido, mei_usuario.segundoapellido,
					mei_usuactividad.fecharespuesta, mei_usuactividad.nota FROM mei_usuario, mei_relususub 
					LEFT JOIN mei_usuactividad ON mei_relususub.idusuario = mei_usuactividad.idusuario 
					AND mei_usuactividad.idactividad = ".$_GET["idactividad"]."
					WHERE mei_usuario.idusuario = mei_relususub.idusuario AND 
					mei_usuario.idusuario<>".$_SESSION['idusuario']."  AND
					mei_relususub.idsubgrupo = '".$idsubgrupo."' ORDER BY ".$orden." ASC ";
					
			$sql2 = "SELECT  mei_usuactividad.nota FROM mei_usuario, mei_relususub 
					LEFT JOIN mei_usuactividad ON mei_relususub.idusuario = mei_usuactividad.idusuario 
					AND mei_usuactividad.idactividad = ".$_GET["idactividad"]."
					WHERE mei_usuario.idusuario = mei_relususub.idusuario AND 
					mei_usuario.idusuario<>".$_SESSION['idusuario']."  AND
					mei_relususub.idsubgrupo = '".$idsubgrupo."' ORDER BY ".$orden." ASC ";		
			//print $sql."<p>";
			$result2 = $baseDatos->ConsultarBD($sql);
			$result3 = $baseDatos->ConsultarBD($sql2);
			if ( ($i%2)==0 ) $clase = "trListaClaro";
			else $clase = "trListaOscuro";
			$res = $baseDatos->ConsultarBD($sql);
			list($nota) = mysql_fetch_row($res);				
?>
      <tr class="<?=$clase?>">
        <td width="6%" rowspan="<?=mysql_num_rows($result2)?>" align="center" valign="middle" class="<?=$clase?>"><?=$i+1?>
		<input name="chk_subgrupo[]" type="checkbox" id="chk_lista" value="<?=$idsubgrupo?>">		</td>
        <td width="7%" rowspan="<?=mysql_num_rows($result2)?>" align="center" valign="middle" class="<?=$clase?>"><?=$subgrupo?></td>
        <?
        $notaGrupo=-1;
			while ( $nota2 = mysql_fetch_array($result3) )
			{
				if($nota2[0]!=0){
					$notaGrupo=$nota2[0];
				}
			}
			
			
			$cont = 0;
			while ( list( $idsubgru, $idusu, $pnombre, $snombre, $papellido, $sapellido, $fecha_r, $nota) = mysql_fetch_array($result2) )
			
			{//WHILE 2
			
				if ($_POST["cbo_ordenar"] !=3 ) $ncompleto = $pnombre." ".$snombre." ".$papellido." ".$sapellido;
				else $ncompleto = $papellido." ".$sapellido." ".$pnombre." ".$snombre;
				
				if ($cont==0)
				{
				
?>
        <td class="<?=$clase?>" align="left" valign="middle"><?=$idusu." ".$ncompleto?></td>
        <td class="<?=$clase?>" rowspan="<?=mysql_num_rows($result2)?>"align="center" valign="middle" ><? 
							if (!empty($fecha_r_lider)) print mostrarFechaTexto($fecha_r_lider,1);
							else print "No hay Respuesta";
		?>		</td>
        <td class="<?=$clase?>" rowspan="<?=mysql_num_rows($result2)?>"align="center" valign="middle">
		<? 
							if ($notaGrupo>=0) print $notaGrupo;
							else print "---";?>		</td>
				
		<? 
							if (!empty($fecha_r_lider))
							{
		?>
        <td class="<?=$clase?>" rowspan="<?=mysql_num_rows($result2)?>"align="center" valign="middle">
		<a href="verEnvioActividad.php?idusuario=<?=$idlider?>&idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&idsubgrupo=<?=$idsubgrupo?>&idtiposubgrupo=<?=$idtiposubgrupo?>&materia=<?=$_GET["materia"]?>" class="link">
		Ver		</a>		</td>
        <td class="<?=$clase?>" rowspan="<?=mysql_num_rows($result2)?>"align="center" valign="middle">		
		<a href="javascript:calificar('<?=$mod?>', '<?=$idlider?>','<?=$_GET["idactividad"]?>', '<?=$_GET["idmateria"]?>','<?=$nota?>','<?=$idsubgrupo?>','<?=$idtiposubgrupo?>')" class="link">
		<img src="imagenes/modificar.gif" alt="Editar Nota" width="16" height="16" border="0"></a></td>
        <td class="<?=$clase?>"  rowspan="<?=mysql_num_rows($result2)?>" align="center" valign="middle"><? if (isset($nota)) {?><a href=" ../correo/enviarComentarioNota.php?idsubgrupo=<?=$idsubgru?>&nombreactividad=<? print $titulo3?>&modo=DOS&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link" target="_blank"> <img src="imagenes/Comentario.gif" alt="comentar Nota" width="16" height="16" border="0">		</a>
																	<? }else print "---";?></td>
        <td class="<?=$clase?>" rowspan="<?=mysql_num_rows($result2)?>" align="center" valign="middle">
		<a href="javascript:eliminar('DEL', '<?=$idusu?>','<?=$_GET["idactividad"]?>', '<?=$_GET["idmateria"]?>','<?=$nota?>','<?=$idsubgrupo?>','<?=$idtiposubgrupo?>')" class="link" onClick="return confirm('Se borrara la Respuesta de este Subgrupo.')">
		<img src="../preguntasFrecuentes/imagenes/eliminar.gif" alt="Eliminar Nota" width="16" height="16" border="0">		</a>		</td>
		<?
							}
							else 
							{
		?>
        <td class="<?=$clase?>" rowspan="<?=mysql_num_rows($result2)?>"align="center" valign="middle">---</td>
        <td class="<?=$clase?>" rowspan="<?=mysql_num_rows($result2)?>"align="center" valign="middle">
		<a href="javascript:calificar('INS', '<?=$idusu?>','<?=$_GET["idactividad"]?>', '<?=$_GET["idmateria"]?>','<?=$nota?>','<?=$idsubgrupo?>','<?=$idtiposubgrupo?>')" class="link">
		<img src="../evaluaciones/imagenes/undo.jpg" alt="Insertar Nota" width="16" height="16" border="0"></a></td>
        <td class="<?=$clase?>" rowspan="<?=mysql_num_rows($result2)?>" align="center" valign="middle">---</td>
		<?
							}
							
		?>
      </tr>
<?
				}//IF mysql_num_rows($result2)==1
				else
				{
					/*for ($i=0;$i<mysql_num_rows($result2)-1;$i++)
					{*/
	?>
				  <tr class="<?=$clase?>">
					<td class="<?=$clase?>"><?=$idusu." ".$ncompleto?></td>
				  </tr>
	<?
					//}
				}
				$cont++;
			}//FIN WHILE 2
				$i++;
		}//FIN WHILE 1

?>
		<tr class="trSubTitulo">
			<td colspan="9" align="center" valign="middle" class="trSubTitulo"><input name="btn_eliminar" type="button" id="btn_eliminar" value="Eliminar" onClick="javascript:eliminarV('DEL', '','<?=$_GET["idactividad"]?>', '<?=$_GET["idmateria"]?>','1','','1')"></td>
		  </tr>
	</table>
<?




 //*****************************INICIO CALCULO HASH AUTOMATICO ********************************//
	
			 $sql12="SELECT mei_usuactividad.nota 
			 FROM mei_usuactividad 
			 WHERE mei_usuactividad.idactividad='".$_GET['idactividad']."'";
			 $resultado= $baseDatos->ConsultarBD($sql12);
			 while(list($nota)= mysql_fetch_array($resultado))
			 {
				// echo $nota;
				     	 if(empty($notastodos))
						{
							
							$notastodos=$nota;
						
						}
						else
						{
						
							$notastodos.=";".$nota;
                        }
			 		 
			 }
			 
			 $hashAuto=md5($notastodos);
			 
			 
             $sql1="SELECT mei_integridadant.hash 
			 FROM mei_integridadant 
			 WHERE mei_integridadant.idactividad='".$_GET['idactividad']."'";
			 $resultado= $baseDatos->ConsultarBD($sql1);
			 list($hashActividad)= mysql_fetch_array($resultado);
			 
			 if(empty($hashActividad))
			{	
					$sql = "INSERT INTO mei_integridadant ( idactividad  , hash)
					VALUES ('".$_GET["idactividad"]."','".$hashAuto."')";
					$baseDatos->ConsultarBD($sql);
					
					$sql = "INSERT INTO mei_integridadnew ( idactividad , hash)
					VALUES ('".$_GET["idactividad"]."','".$hashAuto."')";
					$baseDatos->ConsultarBD($sql);
			}
			else
			{						
		
					$sql = "UPDATE mei_integridadant  SET 	hash='".$hashAuto."' 
					WHERE mei_integridadant.idactividad='".$_GET["idactividad"]."'";
					$baseDatos->ConsultarBD($sql);
			}
			
			$sql2="SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idactividad='".$_GET['idactividad']."'";
			 $resultado= $baseDatos->ConsultarBD($sql2);
			 list($hashManual)= mysql_fetch_array($resultado);	
			 if($hashAuto!=$hashManual)
			 {

			?>
            
            &nbsp;
	<table class="tablaMarquesina">
	              <tr align="center" valign="middle">
                  
	                <td><a>Verificación Integridad Automatico: </a><input type="text" name="hashAuto" size="36" value="<? echo $hashAuto?>"></td>
	              </tr>
	            </table>
				
              <?  $sql2="SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idactividad='".$_GET['idactividad']."'";
			 $resultado= $baseDatos->ConsultarBD($sql2);
			 list($hashManual)= mysql_fetch_array($resultado);	
		
			 ?>
             
                &nbsp;
	<table class="tablaMarquesina">
	              <tr align="center" valign="middle">
	                <td><a href="calcularhash.php?ident=1&idtiposubgrupo=<?=$_GET["idsubgrupo"]?>&idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" title"clic para calcular nuevo hash"><blink>Verificación Integridad Manual:</blink></a><input type="text" name="hashManual" size="36" value="<? echo $hashManual?>"></td>
	              </tr>
	            </table>
            
            <?
			 }
			 $sql2="SELECT mei_integridadnew.hash 
			 FROM mei_integridadnew 
			 WHERE mei_integridadnew.idactividad='".$_GET['idactividad']."'";
			 $resultado= $baseDatos->ConsultarBD($sql2);
			 list($hashManual)= mysql_fetch_array($resultado);	
			?>
            
                   &nbsp;
	<table class="tablaMarquesina">
	              <tr align="center" valign="middle">
	                <td><a><? if($hashAuto==$hashManual){?> Las notas estan correctas<? }else {?> Se ha detectado una Violacion de Seguridad, por favor verifique su backup de notas <? }?></a></td>
	              </tr>
	            </table>
		
           <? 
 //*****************************FIN CALCULO HASH AUTOMATICO ********************************//
}//FIN FUNCION VerGrupal

if ( !empty($_GET["idtiposubgrupo"]) )
{
	if($_SESSION["idtipousuario"]==5)
	{
		$sql = "SELECT mei_subgrupo.idsubgrupo, mei_subgrupo.nombre, mei_evavirgrupo.idvirgrupo, mei_actividad.idtiposubgrupo
			FROM mei_subgrupo, mei_actividad, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_subgrupo.idtiposubgrupo = mei_actividad.idtiposubgrupo AND mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND
			mei_evaluacion.idevaluacion = mei_evavirgrupo.idevaluacion AND mei_evavirgrupo.idvirgrupo = mei_subgrupo.idgrupo AND
			mei_actividad.idactividad = ".$_GET["idactividad"];		
	}
	else
	{
		$sql = "SELECT mei_subgrupo.idsubgrupo, mei_subgrupo.nombre, mei_evagrupo.idgrupo, mei_actividad.idtiposubgrupo
			FROM mei_subgrupo, mei_actividad, mei_evaluacion, mei_evagrupo 
			WHERE mei_subgrupo.idtiposubgrupo = mei_actividad.idtiposubgrupo AND mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND
			mei_evaluacion.idevaluacion = mei_evagrupo.idevaluacion AND mei_evagrupo.idgrupo = mei_subgrupo.idgrupo AND
			mei_actividad.idactividad = ".$_GET["idactividad"];		
	}
			
	//print $sql."<p>";
	verGrupal($sql);	
}
else 
{	
if($_SESSION["idtipousuario"]==5)
	{
		$sql = "SELECT mei_evavirgrupo.idvirgrupo FROM mei_actividad, mei_evaluacion, mei_evavirgrupo 
			WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND
			mei_evavirgrupo.idevaluacion = mei_evaluacion.idevaluacion AND	
			mei_actividad.idactividad = ".$_GET["idactividad"];	
	}
	else
	{
		$sql = "SELECT mei_evagrupo.idgrupo FROM mei_actividad, mei_evaluacion, mei_evagrupo 
			WHERE mei_actividad.idevaluacion = mei_evaluacion.idevaluacion AND
			mei_evagrupo.idevaluacion = mei_evaluacion.idevaluacion AND	
			mei_actividad.idactividad = ".$_GET["idactividad"];	
	}

					
	//print $sql."<p>";
	
	list ($idgrupo) = mysql_fetch_row($baseDatos->ConsultarBD($sql));
	
	if($_SESSION["idtipousuario"]==5)
	{
			$sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
			mei_usuario.primerapellido, mei_usuario.segundoapellido, mei_usuactividad.fecharespuesta, 
			mei_usuactividad.nota FROM mei_relusuvirgru, mei_usuario LEFT JOIN
			mei_usuactividad ON mei_usuario.idusuario = mei_usuactividad.idusuario AND 
			mei_usuactividad.idactividad  = ".$_GET["idactividad"]."
			WHERE mei_usuario.idusuario = mei_relusuvirgru.idusuario AND mei_usuario.idusuario<>".$_SESSION['idusuario']." AND 
			mei_relusuvirgru.idvirgrupo=$idgrupo AND mei_usuario.idtipoalumno!=3 ORDER BY ".$orden." ASC";
	
	}
	else
	{
			$sql = "SELECT mei_usuario.idusuario, mei_usuario.primernombre, mei_usuario.segundonombre, 
			mei_usuario.primerapellido, mei_usuario.segundoapellido, mei_usuactividad.fecharespuesta, 
			mei_usuactividad.nota FROM mei_relusugru, mei_usuario LEFT JOIN
			mei_usuactividad ON mei_usuario.idusuario = mei_usuactividad.idusuario AND 
			mei_usuactividad.idactividad  = ".$_GET["idactividad"]."
			WHERE mei_usuario.idusuario = mei_relusugru.idusuario AND mei_usuario.idusuario<>".$_SESSION['idusuario']." AND 
			mei_relusugru.idgrupo=$idgrupo AND mei_usuario.idtipoalumno!=3 ORDER BY ".$orden." ASC";
	
	}
	

	//print $sql."<p>";
	verNoGrupal($sql);
}
?>
            </form>
<form name="frm_cocalificar" id="frm_cocalificar" method="post" action="">			
	<input name="cbo_calificacion" type="hidden" id="cbo_calificacion" value="">
</form>
<?php
/*INFORMACION DE ACTIVIDAD*/
$sql = "SELECT  mei_actividad.idevaluacion FROM mei_actividad WHERE mei_actividad.idactividad = ".$_GET["idactividad"];
$resultado = $baseDatos->ConsultarBD($sql);
list ($idevaluacion) = mysql_fetch_row($resultado);
/*FIN INFORMACION DE ACTIVIDAD*/
?>
<form name="frm_ingresarTaller" id="frm_ingresarTaller" method="post" enctype="multipart/form-data" action="">			
	<input name="fil_archivo1" type="hidden" id="fil_archivo1" value="">
	<input name="fil_archivo2" type="hidden" id="fil_archivo2" value="">
	<input name="fil_archivo3" type="hidden" id="fil_archivo3" value="">
	<input name="fil_archivo4" type="hidden" id="fil_archivo4" value="">
	<input name="fil_archivo5" type="hidden" id="fil_archivo5" value="">
	
	<input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_GET["idmateria"]?>">
	<input name="hid_grupo" type="hidden" id="hid_grupo" value="<?="$idgrupo-$grupo"?>">
	<input name="cbo_subgrupo" type="hidden" id="cbo_subgrupo" value="NULL">
	
	<input name="cbo_nota" type="hidden" id="cbo_nota" value="<?=$idevaluacion?>">
	
	<input name="cbo_actividad" type="hidden" id="cbo_actividad" value="5*Actividad">
	<input name="coevaluacion" type="hidden" id="coevaluacion" value="0">
	<input name="hid_coevaluacion2" type="hidden" id="hid_coevaluacion2" value="2">
	<input name="hid_idcoevaluacion2" type="hidden" id="hid_idcoevaluacion2" value="<?=$_GET["idactividad"]?>">
	<input name="cbo_valor" type="hidden" id="cbo_valor" value="0">
	<input name="txt_nombreTaller" type="hidden" id="txt_nombreTaller" value="RESP. <?=$titulo?>">
	<input name="txt_fecha_a" type="hidden" id="txt_fecha_a" value="<?php echo date("Y-n-j"); ?>">
	<input name="cbo_hora_a" type="hidden" id="cbo_hora_a" value="00">
	<input name="cbo_minuto_a" type="hidden" id="cbo_minuto_a" value="00">
	<input name="txt_fecha_f" type="hidden" id="txt_fecha_f" value="<?php echo date("Y-n-j"); ?>">
	<input name="cbo_hora_f" type="hidden" id="cbo_hora_f" value="23">
	<input name="cbo_minuto_f" type="hidden" id="cbo_minuto_f" value="00">
	<input name="txt_fecha_f1" type="hidden" id="txt_fecha_f1" value="">
	<input name="cbo_hora_f1" type="hidden" id="cbo_hora_f1" value="00">
	<input name="cbo_minuto_f1" type="hidden" id="cbo_minuto_f1" value="00">
	<input name="cbo_estado" type="hidden" id="cbo_estado" value="0">
	<input name="chk_calendario" type="hidden" id="chk_calendario" value="checkbox">
	<input name="edt_actividad" type="hidden" id="edt_actividad" value="<p style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;'>Mire la soluci&oacute;n de la actividad &nbsp;segun archivo adjunto por el PROFESOR (Solucion) y proceda a &nbsp;realizar la calificacion, descargando el &nbsp;archivo enviado por el ESTUDIANTE &nbsp;que lo encuentra en el boton CALIFICAR COEVALUACION, coloque &nbsp;en la caja de texto la nota de cada punto en la escala de 0-1; &nbsp;cada punto vale maximo 1. y la definitiva de la coevaluacion coloquela &nbsp;usando el selector<span style='color: rgb(255, 0, 0);'>&nbsp;NOTA</span></p>
<p style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;'>Calcular la nota final = &nbsp;( sumaPuntosExamen) &nbsp;* &nbsp;5 / (Total de puntos) y colocarla en&nbsp;<span style='color: rgb(255, 0, 0);'>Nota</span>&nbsp;de la coevaluaci&oacute;n ( No dentro de la caja de texto )</p>
<p style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;'>NOTA1: No confundir la Solucion de la coevaluacion con la&nbsp;<strong>respuesta</strong>&nbsp;enviada por el estudiante.<br />
NOTA2: Si despues de ingresar a CALIFICAR COEVALUACION no aparece &nbsp;archivo adjunto, significa que el estudiante no respondi&oacute; esta actividad, entonces cancele y salga.</p>
<p style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;'>&nbsp;</p>
<p>&nbsp;</p>">
</form>


<script>
	function asignarnota(idusuario,vlrnota){
		document.getElementById("frm_cocalificar").action = 'guardarCalificacion.php?modo=&idusuario='+idusuario+'&idactividad='+<?=$_GET["idactividad"]?>+'&idmateria='+<?=$_GET["idmateria"]?>;
		document.getElementById("cbo_calificacion").value = vlrnota;
		document.getElementById("frm_cocalificar").submit();
	}
	function crearrespcoevaluacion(){
		document.frm_ingresarTaller.action="guardarActividad.php?materia=<?=$_GET["materia"]?>&idmateria=<?=$_GET["idmateria"]?>&cbo_orden=<?=$_GET['cbo_orden']?>";

		document.getElementById("frm_ingresarTaller").submit();
	}	
</script>  
&nbsp;
<?php
$sql = "SELECT  * FROM mei_actividad WHERE mei_actividad.idcoevaluacion= ".$_GET["idactividad"];
$resultado = $baseDatos->ConsultarBD($sql);
$numfilacoeva = mysql_num_rows($resultado);
if( $numfilacoeva == 0 ):
?>
<table width="100%" border="0" class="tablaMarquesina">
  <tbody><tr class="">
    <td valign="middle" align="center">
	<a class="link" href="#" onclick="crearrespcoevaluacion()"><img border="0" alt="Taller" src="imagenes/ver.gif"> Crear Respuesta Co-Evaluación </a></td>
  </tr>
</tbody></table>		
<?php endif; ?>	
&nbsp;

<table class="tablaMarquesina">
              <tr align="center" valign="middle">
              <? if ($_SESSION['idtipousuario']==5) {
              	?><td><a href="<?echo $_SERVER['PHP_SELF']; ?>?idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&idtiposubgrupo=<?=$_GET['idtiposubgrupo']?>&materia=<?=$_GET["materia"]?>&cbo_orden=<?=$_GET["cbo_orden"]?>" class="link">Cambiar Calificadores</a></td>
             <? } ?>
              
                <td><a href="verDtlleActividad.php?idactividad=<?=$_GET["idactividad"]?>&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link">Volver</a></td>
              </tr>
            </table>
			
			</td>
             <td class="tablaEspacio">&nbsp;</td>
		<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
        <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>