<?PHP session_start();
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link rel="stylesheet" href="css/style.css">
<title>MEIWEB</title>

<script language="javascript">

	function enviarRegistro()
	{
		if(validarDatos())
		{
			document.frm_ingresoAdmin.submit();
		}
	}
	
	function validarDatos()
	{
		if(document.frm_ingresoAdmin.txt_clave.value ==false || document.frm_ingresoAdmin.txt_admin.value ==false) 
		{
			alert("Datos Incorrectos");
			return false;			
		}
		else
		{
			return true;
		}
	}
	
	function enviarCancelar()
	{
		location.replace("../login/");	
	}



</script>

</head>

<body>
<div class="login-page">
  <div class="form">	
  	<img src="../portal/imagenes/logo.png" style="width:180px; height: 40px;"/>
	<img src="../portal/imagenes/UIScom.png" style="width: 80px; height: 40px;"/>
	<br><br>	
    <form action="loginRegistroAdmin.php" method="post" name="frm_ingresoAdmin" id="frm_ingresoAdmin">
    <input type="text" placeholder="Administrador" name="txt_admin"></td>
    <input name="txt_clave" placeholder="Digite su contraseña" type="password" id="txt_clave"></td>
    <input name="btn_ingresar" type="button" id="btn_ingresar" value="Ingresar" onClick="javascript: enviarRegistro();">
    <input name="btn_reset" type="reset" id="btn_reset" value="Restablecer">
    <input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript: enviarCancelar();">
    <p class="message"><a href="recordarClaveAdmin.php" title="Recordar Contraseña">¿Ha olvidado su contraseña?</a></p>
        
        <?PHP
			  if($_SESSION['errorAdminClave']=="0x001")
			  {
			?>
        <p class="message">Datos Incorrectos</p>
        
        <?PHP
			  }
			?>
     </div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>
	
</body>
</html>
