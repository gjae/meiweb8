<?PHP	session_start();
	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');  $baseDatos=new BD();
	
	
if(isset($_GET["entrar"]) && $_GET["entrar"]==1)
{

	if(isset($_SERVER['errorsistema']))
	{	
		$mensaje=mostrarError($_SESSION['errorsistema']);	
		$error=$_SESSION['errorsistema'];	
	} else { $error = 0; $mensaje=""; }


?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon"  href="../temas/favicon.ico" />
<link rel="stylesheet" href="css/style.css">
<title>MEIWEB</title>
	<script Language='JavaScript'>
            <!-- Esconde esto de navegadores antiguos
              if (window != window.top)
                 top.location.href = location.href;
            // --> 
			
			function borrarFormulario()
			{
				document.frm_registro.reset();			
				document.frm_registro.txt_idUsuario.value="";
			}
			
			function display_error_ip()
			{
				alert("Ha iniciado sesion en un equipo diferente");
			}
			
	</script> 
	
    <link rel="stylesheet" type="text/css" href="tecvir/keyboard.css" />
 
    <script type="text/javascript" src="tecvir/keyboard.js" charset="UTF-8"></script> 
 <script type="text/javascript" src="tecvir/kb.js" charset="UTF-8"></script> 
</head>

<body>

<div class="login-page">
	
  <div class="form">
  	<img src="../portal/imagenes/logo.png" style="width:180px; height: 40px;"/>
	<img src="../portal/imagenes/UIScom.png" style="width: 80px; height: 40px;"/>
	<br><br>	
	<form class="login-form" name="frm_registro" method="post" action="loginRegistro.php">
      <input type="password" placeholder="Usuario" class="link keyboardInput" lang="es" name="txt_idUsuario" value="" title="Su login registrado"/><br><br>
      <input type="password" placeholder="Contraseña" class="link keyboardInput" lang="es" name="txt_clave" title="Contraseña registrada"/><br><br>
      <input type="submit" name="sub_registro" value="Ingresar" >
      <br><br>
      <input type="button" name="btn_reset" id="btn_reset" value="Restablecer" onClick="javascript: borrarFormulario();">
      <p class="message"><?PHP echo $mensaje; ?></p>
       <?PHP
			  	if($error == 3)
				{
			  ?>
			  	 <a href="../login/desbloquearUsuario.php">Desbloquear Usuario</a>
			  <?PHP
			  }
			  ?>
			  <?PHP
		  if($error != 3)
		  {
		  ?>
      <p class="message">Olvidó la contraseña? <a href="../login/recordarClave.php" title="Recordar Contraseña">Recordar</a></p>
      <?PHP
			}
		?>
    </form>



 </div>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>



<?PHP
	if($error == 5)
	{
?>
	<script language="javascript">
		alert("Actualmente no se encuentra registrado en ningn grupo. Solicite al administrador de MeiWeb su registro en un grupo.");
	</script>
<?PHP
	}
	else if($error == 3)
	{
	
	?>
	<script language="javascript">
			  alert("Demasiados intentos de ingresos fallidos a MeiWeb. Su cuenta ha sido bloqueada por seguridad.");
	 </script>
	 <?PHP
	}
	else if($error == 6)
	{
	
	?>
	<script language="javascript">
			  alert("Se ha desbloqueado satisfactoriamente el usuario. Puede ingresar normalmente a MeiWeb.");
	 </script>
	 <?PHP
	}
else if($error == 22)
	{
	
	?>
	<script language="javascript">
			  alert("Sus datos fueron enviados a su correo");
	 </script>
	 <?PHP
	}
?>
</body>
</html>
<?PHP
	if(isset($_GET['error_ip']))
	{
	?>
	<script>
    	display_error_ip();
    </script>
	<?PHP
	}
}
else
{
	redireccionar('../portal/');
}
?>




