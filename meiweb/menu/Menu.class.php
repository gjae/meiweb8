<?PHP
function menu($tipo)
{	
	include_once ('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	$baseDatos= new BD();
	
	$sql="SELECT mei_modulo.nombre FROM mei_modulo WHERE mei_modulo.estado=1 ORDER BY (mei_modulo.orden)";
	$modulos=$baseDatos->ConsultarBD($sql);  // obtiene los modulos que estan activos 
	echo '<script type="text/javascript" src="../menu/jquery1-3-2.js"></script>';
    echo '<script src="../menu/jquery.shortcuts.js" type="text/javascript"></script>';
    if($_SESSION['idtipousuario'] == 2 || $_SESSION['idtipousuario'] == 5){
    	if(!empty($_GET['idmateria'])){
    echo "<script>
    		$.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+M',
                handler: function() {
                    window.location.href = '../notas/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."';
                }
            }).start();  
    		$.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+B',
                handler: function() {
                    window.location.href = '../evaluaciones/verPregunta.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."';
                }
            }).start();  
    		$.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+Alt+P',
                handler: function() {
                    window.location.href = '../planificador/verPlanificador.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&cuales=activos';
                }
            }).start();  
    		$.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+Alt+C',
                handler: function() {
                    window.location.href = '../contenido/mostrarIndiceEdicion.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."';
                }
            }).start();
			$.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+M+P',
                handler: function() {
                    window.location.href = '../planificador/agregarPlanificador.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."';
                }
            }).start();    
    		$.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+R',
                handler: function() {
                    window.location.href = '../correo/redactarCorreo.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."';
                }
            }).start();    
            $.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+home',
                handler: function() {
                    window.location.href = '../scripts/';
                }
            }).start();    
            $.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+A',
                handler: function() {
                    window.location.href = '../actividades/verActividad.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."';
                }
            }).start(); 
            $.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+E',
                handler: function() {
                    window.location.href = '../evaluaciones/verEvaluacion.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."';
                }
            }).start();                                   
    </script>";
    if ($_SESSION['idtipousuario']==2) {
    	echo "<script>$.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+L',
                handler: function() {
                    window.location.href = '../usuario/listarProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."';
                }
            }).start();
            </script>";
        }
        else{
        	echo "<script>
        	$.Shortcuts.add({
                type: 'down',
                mask: 'Ctrl+L',
                handler: function() {
                    window.location.href = '../usuario/VirListarProfesor.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."';
                }
            }).start();
            </script>";
        }
    }
    }
	print "<table class='tablaIzquierdo'><tr><td style='position:fixed;'>";
	print "<script type='text/javascript' language='JavaScript1.2' src='../menu/stm31.js'></script>
	<script type='text/javascript' language='JavaScript1.2'>
	stm_bm(['menu2b94',430,'','../menu/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0],this);
	stm_bp('p10',[1,4,0,0,2,3,22,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,0,0,'#FFFFFF']);";
	

	if($tipo==1)
	{
		//modulos
		print "stm_ai('p0i0',[1,'Modulos','','',-1,-1,0,'../configuracion/configurarModulos.php','_self','','Modulos','../menu/modulos.gif','../menu/modulos.gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
		print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']); 
				stm_ep();";
		//usuarios			
		print "stm_ai('p0i0',[1,'Usuarios','','',-1,-1,0,'','_self','','Usuarios','../menu/usuarios.gif','../menu/usuarios.gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
		print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
				stm_aix('p1i1','p0i0',[1,'Usuarios Presenciales','','',-1,-1,0,'../usuario/listarUsuariosAdministrador.php','_self','','Editar Usuarios Presenciales','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
				stm_aix('p1i2','p0i0',[1,'Usuarios Virtuales','','',-1,-1,0,'../usuario/VirListarUsuariosAdministrador.php','_self','','Editar Usuarios Virtuales','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);						
				stm_aix('p1i3','p0i0',[1,'Crear Profesor','','',-1,-1,0,'../usuario/crearProfesor.php','_self','','Crear Profesor','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
				stm_aix('p1i4','p0i0',[1,'Crear Profesor Virtual','','',-1,-1,0,'../usuario/VirCrearProfesor.php','_self','','Crear Profesor Educación Virtual','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
		print "stm_ep();";
		//grupos
		print "stm_ai('p0i0',[1,'Grupos','','',-1,-1,0,'../materias/verGruposAdministrador.php','_self','','Ver Grupos','../menu/materias.gif','../menu/materias.gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
		print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
			   stm_ep();";			
		//materias
		print "stm_ai('p0i0',[1,'Materias','','',-1,-1,0,'','_self','','Materias','../menu/materias.gif','../menu/materias.gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
		print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
				stm_aix('p1i1','p0i0',[1,'Ver Materias Presenciales','','',-1,-1,0,'../materias/verMateriasAdministrador.php','_self','','Editar Materia','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
				stm_aix('p1i2','p0i0',[1,'Ver Cursos Virtuales','','',-1,-1,0,'../materias/VirVerMateriasAdministrador.php','_self','','Editar Cursos','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
                stm_aix('p1i1','p0i0',[1,'Ver Materias con Recursos','','',-1,-1,0,'../materias/verMateriaRecursos.php','_self','','Editar Materia','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
				stm_aix('p1i3','p0i0',[1,'Crear Materia Presencial','','',-1,-1,0,'../materias/crearMateria.php','_self','','Agregar Materia','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
				stm_aix('p1i4','p0i0',[1,'Crear Curso Virtual','','',-1,-1,0,'../materias/VirCrearMateria.php','_self','','Agregar Curso','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
		print "stm_ep();";
		//Reiniciar			
		print "stm_ai('p0i0',[1,'Reiniciar','','',-1,-1,0,'../materias/VirReinicioModulos.php','_self','','Reiniciar','../menu/reiniciar.gif','../menu/reiniciar.gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
		print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
				stm_ep();";
		//Seguridad			
		print "stm_ai('p0i0',[1,'Seguridad','','',-1,-1,0,'../seguridad/','_self','','Copias de Seguridad','../menu/seguridad.gif','../menu/seguridad.gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
		print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
				stm_ep();";
		//Restaurar					
		print "stm_ai('p0i0',[1,'Restaurar Contenidos','','',-1,-1,0,'../scorm/listodescomprimir.php','_self','',' SCORM','../menu/scorm.gif','../menu/scorm.gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
		print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
				stm_ep();";	
		//Herramientas					
		print "stm_ai('p0i0',[1,'Herramientas','','',-1,-1,0,'','_self','','Herramientas','../menu/herramientas.gif','../menu/herramientas.gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
		print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
				stm_aix('p1i1','p0i0',[1,'Agregar Herramienta','','',-1,-1,0,'../herramientas/agregarHerramientaAdmin.php?ordenar=mei_herramientas.idherramienta','_self','','Agregar Herramienta','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
				stm_aix('p1i2','p0i0',[1,'Ver Repositorios','','',-1,-1,0,'../herramientas/verRepositorio.php?ordenar=mei_herramientas.idherramienta','_self','','Editar Repositorios','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);						
				stm_aix('p1i3','p0i0',[1,'Ver URL','','',-1,-1,0,'../herramientas/verURL.php?ordenar=mei_herramientas.idherramienta&order=mei_usuario.primernombre','_self','','Editar URL','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);											
				stm_ep();";	
		//preinscritos
		print "stm_ai('p0i0',[1,'Preinscritos','','',-1,-1,0,'../preinscritos/VirPreinscritos.php?ordenar=mei_preinscripcion.registro','_self','','Alumnos Preinscritos','../menu/cargar.png','../menu/cargar.png',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
		print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']); 
				stm_ep();";			
	}
	else
	{
		while (list($modulo)=mysql_fetch_array($modulos))
		{
			if ($modulo=="materias")
			{
				if ($tipo==2 || $tipo==3 || $tipo==7)
				{
					print "stm_ai('p0i0',[1,'Materiass','','',-1,-1,0,'','_self','','Materiass','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
					print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);";
					$sql="SELECT DISTINCT(mei_materia.idmateria),mei_materia.nombre 
							FROM mei_grupo, mei_materia WHERE mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo 
							FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")
							AND mei_grupo.idmateria=mei_materia.idmateria AND( mei_materia.grupo = 0 OR mei_materia.grupo is null ) ORDER BY (mei_materia.nombre)";
					$resultado=$baseDatos->ConsultarBD($sql);
					while (list($idmateria,$nombre)=mysql_fetch_array($resultado))
					{
						print "stm_aix('p1i1','p0i0',[1,'".$nombre."','','',-1,-1,0,'../scripts/homeMateria.php?idmateria=$idmateria&materia=$nombre','_self','','".$nombre."','../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,'sdf','sf',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
					} 
					print "stm_ep();";
					print " stm_ai('p0i1',[6,3,'#000000','',-1,-1,0]);";
                    
                    /*Grupos*/
					
					$sql="SELECT DISTINCT(mei_materia.idmateria),mei_materia.nombre 
							FROM mei_grupo, mei_materia WHERE mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo 
							FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")
							AND mei_grupo.idmateria=mei_materia.idmateria AND mei_materia.grupo = 1 ORDER BY (mei_materia.nombre)";
					$resultado=$baseDatos->ConsultarBD($sql);
                    if(mysql_num_rows($resultado) > 0){
                        print "stm_ai('p0i0',[1,'Grupos','','',-1,-1,0,'','_self','','Grupos','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
    					print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);";                    
    					while (list($idmateria,$nombre)=mysql_fetch_array($resultado))
    					{
    						print "stm_aix('p1i1','p0i0',[1,'".$nombre."','','',-1,-1,0,'../scripts/homeMateria.php?idmateria=$idmateria&materia=$nombre','_self','','".$nombre."','../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,'sdf','sf',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
    					} 
    					print "stm_ep();";
    					print " stm_ai('p0i1',[6,3,'#000000','',-1,-1,0]);";                        
                    }
                    
				}
				else if ($tipo==5 || $tipo==6)
				{
					print "stm_ai('p0i0',[1,'Mis Cursos..','','',-1,-1,0,'','_self','','Cursos','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
					print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);";
					$sql="SELECT DISTINCT(mei_virmateria.idvirmateria),mei_virmateria.nombre 
							FROM mei_virgrupo, mei_virmateria WHERE mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo 
							FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario'].")
							AND mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria ORDER BY (mei_virmateria.nombre)";
					$resultado2=$baseDatos->ConsultarBD($sql);
					while (list($idmateria,$nombre)=mysql_fetch_array($resultado2))
					{
						print "stm_aix('p1i1','p0i0',[1,'".$nombre."','','',-1,-1,0,'../scripts/homeMateria.php?idmateria=$idmateria&materia=$nombre','_self','','".$nombre."','../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,'sdf','sf',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
					}
					print "stm_ep();";
					print " stm_ai('p0i1',[6,3,'#000000','',-1,-1,0]);";
				}			
			}			
			if (!empty($_GET['idmateria'])) //si no esta vacio 
			{	
				if(comprobarSession())
				{
					$idmateria=$_GET['idmateria'];		
					$materia=$_GET['materia'];
					
					$sqlgrupo="SELECT mei_materia.grupo FROM mei_materia WHERE mei_materia.idmateria=".$idmateria;
					$resultadogrupo=$baseDatos->ConsultarBD($sqlgrupo);					
					list($grupocomunicacion)=mysql_fetch_array($resultadogrupo);
					if($grupocomunicacion != 1){
						if ($modulo=="herramientas") //para todos,  
						{
							if ($tipo==2 || $tipo==5) //profesor
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_aix('p1i0','p0i0',[1,'Herramientas','','',-1,-1,0,'../herramientas/verHerramienta.php?idmateria=$idmateria&materia=$materia','_self','','Ver Herramientas','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
									stm_aix('p1i1','p0i0',[1,'Agregar Herramienta','','',-1,-1,0,'../herramientas/agregarHerramienta.php?idmateria=$idmateria&materia=$materia','_self','','Agregar Herramientas','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
									stm_ep();";
							}
							else if($tipo==3 || $tipo==6 || $tipo==7) //estudiante solo ver las herramientas
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'../herramientas/verHerramienta.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
						}
						if ($modulo=="informacion")	//para el informacion del curso 
						{
							if($tipo==2 || $tipo==5) //profesor 
							{
								print "stm_ai('p0i0',[1,'Contenidos','','',-1,-1,0,'../contenido/mostrarIndiceEdicion.php?idmateria=$idmateria&materia=$materia','_self','','Contenidos','../menu/".$modulo.".png','../menu/".$modulo.".png',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
							else if($tipo==3 || $tipo==6 || $tipo==7) //estudiante la info del curso 
							{
								print "stm_ai('p0i0',[1,'Contenidos','','',-1,-1,0,'../contenido/mostrarIndiceEdicion.php?idmateria=$idmateria&materia=$materia','_self','','Contenidos','../menu/".$modulo.".png','../menu/".$modulo.".png',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
						}
						if ($modulo=="planificador")	//para el planificador 
						{
							if($tipo==2 || $tipo==5 || $tipo==7) //profesor 
							{
								print "stm_ai('p0i0',[1,'Planificación','','',-1,-1,0,'','_self','','Planificación','../menu/".$modulo.".png','../menu/".$modulo.".png',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_aix('p1i0','p0i0',[1,'Ver Planificación','','',-1,-1,0,'../planificador/verPlanificador.php?cuales=activos&idmateria=$idmateria&materia=$materia','_self','','Ver Planificación','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
									stm_aix('p1i1','p0i0',[1,'Gestionar Planificación','','',-1,-1,0,'../planificador/agregarPlanificador.php?idmateria=$idmateria&materia=$materia','_self','','Crear Planificación','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
									stm_ep();";
							}
							else if($tipo==3 || $tipo==6) //estudiante solo ver planificador
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'../planificador/verPlanificadorEstudiante.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".png','../menu/".$modulo.".png',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
						}					
						if ($modulo=="biblioteca")	//material del curso  FALTA INCLUIRLA DENTRO DE CADA MATERIA 
						{
							if($tipo==2 || $tipo==5 || $tipo==7)//profesor ver y cargar archivos
							{
								print "stm_ai('p0i0',[1,'Material Curso','','',-1,-1,0,'','_self','','Material Curso','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);";
								print "stm_aix('p1i0','p0i0',[1,'Ver Archivos','','',-1,-1,0,'../biblioteca/index.php?idmateria=$idmateria&	materia=$materia','_self','','Ver Archivos','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
								   stm_aix('p1i2','p0i0',[1,'Cargar Archivo','','',-1,-1,0,'../biblioteca/cargarArchivo.php?idmateria=$idmateria&materia=$materia','_self','','Cargar Archivo','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
								print "stm_ep();";
							}
							//se quita la pestaña material del curso en la vista de estudiantes
							/*else if($tipo==3 || $tipo==6) //estudiante solo ver archivos 
							{
								print "stm_ai('p0i0',[1,'Material Curso','','',-1,-1,0,'../biblioteca/index.php?idmateria=$idmateria&	materia=$materia','_self','','Material Curso','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);";
								print "stm_ep();";
							}*/
						}		
						if ($modulo=="actividades")	//para actividades
						{
							if($tipo==2 || $tipo==5 || $tipo==7) //profesor 
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_aix('p1i0','p0i0',[1,'Ver Actividades','','',-1,-1,0,'../actividades/verActividad.php?idmateria=$idmateria&materia=$materia','_self','','Ver Actividades','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
									stm_aix('p1i1','p0i0',[1,'Crear Actividad','','',-1,-1,0,'../actividades/ingresarActividad.php?idmateria=$idmateria&materia=$materia','_self','','Crear Actividad','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
									stm_ep();";
							}
							else if($tipo==3) //estudiante solo ver las actividades
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'../actividades/verActividad.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
							else if($tipo==6) //estudiante solo ver las actividades
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'../actividades/verVirActividad.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
							
						}
						if ($modulo=="evaluaciones")	//para evaluaciones= crear evaluacion y banco de preguntas 
						{
							if($tipo==2) //profesor 
							{
								print "stm_ai('p0i0',[1,'Evaluaciones','','',-1,-1,0,'../evaluaciones/verEvaluacion.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
								
                                print "stm_ai('p0i0',[1,'Banco de Preguntas','','',-1,-1,0,'../evaluaciones/verPregunta.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";                                    
							}
							else if($tipo==5) //profesor 
							{
								print "stm_ai('p0i0',[1,'Evaluaciones','','',-1,-1,0,'../evaluaciones/verEvaluacion.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
								
                                print "stm_ai('p0i0',[1,'Banco de Preguntas','','',-1,-1,0,'../evaluaciones/verPregunta.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";                                    
							}
							else if($tipo==3 || $tipo==7) //estudiante solo ver la cartelera
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'../evaluaciones/verEvaluacion.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
							else if($tipo==6) //estudiante solo ver la cartelera
							{
								print "stm_ai('p0i0',[1,'Evaluaciones','','',-1,-1,0,'../evaluaciones/verEvaluacion.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
							
						}
						if ($modulo=="notas") //Calificaciones
						{
							print "stm_ai('p0i0',[1,'Calificaciones','','',-1,-1,0,'../notas/index.php?idmateria= $idmateria&materia=$materia','_self','','Calificaciones','../menu/notas.gif','../menu/notas.gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
							print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							print " stm_ai('p0i1',[6,3,'#000000','',-1,-1,0]);";
						}
						if ($modulo=="glosario") //muestra de una todas las letras de la materia 
						{
							print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'../glosario/index.php?idmateria=$idmateria&materia=$materia','_self','','Glosario','../menu/glosario.gif','../menu/glosario.gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
							print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
						}
						if ($modulo=="faq") //para preguntas frecuentes 
						{
							print "stm_ai('p0i0',[1,'FAQ','','',-1,-1,0,'../preguntasFrecuentes/index.php?idmateria=$idmateria&materia=$materia','_self','','Preguntas Frecuentes','../menu1/perfil.gif','../menu1/perfil.gif',22,22,0,'../menu1/arrow_r.gif','../menu1/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
							print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
						}
						if ($modulo=="lanzadores" ) 
						{
							print " stm_ai('p0i1',[6,3,'#000000','',-1,-1,0]);";
							if($tipo==2 || $tipo==5)
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);";
								print "stm_aix('p1i0','p0i0',[1,'Ver Lanzadores','','',-1,-1,0,'../lanzadores/index.php?idmateria=$idmateria&materia=$materia','_self','','Ver Lanzadores','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
									stm_aix('p1i2','p0i0',[1,'Agregar Lanzador','','',-1,-1,0,'../lanzadores/agregarLanzador.php?idmateria=$idmateria&materia=$materia','_self','','Agregar Lanzador','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
	/*							if ($tipo==2)		
								{
									$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.idusuario,mei_lanzador.titulo,mei_lanzador.descripcion,mei_lanzador.tamano,mei_lanzador.lanzador,mei_lanzador.ubicacion,mei_lanzador.tipo FROM mei_lanzador WHERE mei_lanzador.idlanzador IN (SELECT mei_rellangru.idlanzador FROM mei_rellangru WHERE mei_rellangru.idgrupo IN (SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']."))";
								}
								else if ($tipo==5)
								{
									$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.idusuario,mei_lanzador.titulo,mei_lanzador.descripcion,mei_lanzador.tamano,mei_lanzador.lanzador,mei_lanzador.ubicacion,mei_lanzador.tipo FROM mei_lanzador WHERE mei_lanzador.idlanzador IN (SELECT mei_rellanvirgru.idlanzador FROM mei_rellanvirgru WHERE mei_rellanvirgru.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."))";
								}
								$resultado=$baseDatos->ConsultarBD($sql);
								while (list($codigoLanzador,$autor,$titulo,$descripcion,$tamano,$lanzador,$ubicacion,$tipoLanzador)=mysql_fetch_array($resultado))
								{
									if((file_exists($ubicacion) && $tipoLanzador == 0) || $tipoLanzador == 1) //0=archivo cargado, 1=link internet
									{
										$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
												FROM mei_usuario WHERE mei_usuario.idusuario='".$autor."'";
										$autor=$baseDatos->ConsultarBD($sql);
										list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
										$tituloAlt= "Autor: " .$nombre1." ".$nombre2." ".$apellido1." ".$apellido2;
											
										if(!empty($descripcion))	//si NO esta vacio entonces 
										{
											$tituloAlt="Descripcion: ".$descripcion;
										}									
										if($tipoLanzador == 1)									
										{
											$imagen="www.gif";
											$link=$lanzador;
										}
										else
										{
											$imagen="lanzador.gif";
											$link=$ubicacion;
										}									
										print "stm_aix('p1i1','p0i0',[1,'<img align=texttop src=../menu/".$imagen."> ".$titulo."','','',-1,-1,0,'".$link."','_black','','".$tituloAlt."','../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,'sdf','sf',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
									}
									else
									{
										$sql="DELETE FROM mei_lanzador WHERE mei_lanzador.idlanzador = '".$codigoLanzador."'";
										$consulta=$baseDatos->ConsultarBD($sql);
									}
								}
		*/						print "stm_ep();";
							}
							else if($tipo==3 || $tipo==6 || $tipo==7)
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'../lanzadores/index.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".gif','../menu/".$modulo.".gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);";
				//				print "stm_aix('p1i0','p0i0',[1,'Ver Lanzadores','','',-1,-1,0,'../lanzadores/index.php?idmateria=$idmateria&materia=$materia','_self','','Ver Lanzadores','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
		/*						if ($tipo==3)
								{
									$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.idusuario,mei_lanzador.titulo,mei_lanzador.descripcion,mei_lanzador.tamano,
										mei_lanzador.lanzador,mei_lanzador.ubicacion,mei_lanzador.tipo 
										FROM mei_lanzador WHERE mei_lanzador.idlanzador IN 
										(SELECT mei_rellangru.idlanzador FROM mei_rellangru WHERE mei_rellangru.idgrupo IN 
										(SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']."))";
								}
								else if ($tipo==6)
								{
									$sql="SELECT mei_lanzador.idlanzador,mei_lanzador.idusuario,mei_lanzador.titulo,mei_lanzador.descripcion,mei_lanzador.tamano,
										mei_lanzador.lanzador,mei_lanzador.ubicacion,mei_lanzador.tipo 
										FROM mei_lanzador WHERE mei_lanzador.idlanzador IN 
										(SELECT mei_rellanvirgru.idlanzador FROM mei_rellanvirgru WHERE mei_rellanvirgru.idvirgrupo IN 
										(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."))";
								}
								$resultado=$baseDatos->ConsultarBD($sql);
								while (list($codigoLanzador,$autor,$titulo,$descripcion,$tamano,$lanzador,$ubicacion,$tipoLanzador)=mysql_fetch_array($resultado))
								{
									if((file_exists($ubicacion) && $tipoLanzador == 0) || $tipoLanzador == 1)
									{
										$sql="SELECT mei_usuario.primernombre,mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido 
												FROM mei_usuario WHERE mei_usuario.idusuario='".$autor."'";
										$autor=$baseDatos->ConsultarBD($sql);
										list($nombre1,$nombre2,$apellido1,$apellido2) = mysql_fetch_array($autor);
										if($tipoLanzador == 1)									
										{
											$imagen="www.gif";
										}
										else
										{
											$imagen="lanzador.gif";
										}
										print "stm_aix('p1i1','p0i0',[1,'<img align=texttop src=../menu/".$imagen."> ".$titulo."','','',-1,-1,0,'".$ubicacion."','_black','','Autor: ".$nombre1.' '.$nombre2.' '.$apellido1.' '.$apellido2."  Descripcion: ".$descripcion."','../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,'sdf','sf',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
									}
									else
									{
										$sql="DELETE FROM mei_lanzador WHERE mei_lanzador.idlanzador = '".$codigoLanzador."'";
										$consulta=$baseDatos->ConsultarBD($sql);
									}
								}
		*/						print "stm_ep();";
							}
						}
						if ($modulo=="scorm") //SCORM
						{
							if($tipo==2 || $tipo==5)
							{
								print "stm_ai('p0i0',[1,'SCORM','','',-1,-1,0,'../scorm/opcionScorm.php?idmateria=$idmateria&materia=$materia','_self','','Exportar a ".ucwords($modulo)."','../menu/notas.gif','../menu/notas.gif',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
						}
					}	
					elseif($grupocomunicacion == 1){
						if ($modulo=="informacion")	//para el informacion del curso 
						{
							if($tipo==2 || $tipo==5) //profesor 
							{
								print "stm_ai('p0i0',[1,'Contenidos','','',-1,-1,0,'../contenido/mostrarIndiceEdicion.php?idmateria=$idmateria&materia=$materia','_self','','Contenidos','../menu/".$modulo.".png','../menu/".$modulo.".png',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
							else if($tipo==3 || $tipo==6 || $tipo==7) //estudiante la info del curso 
							{
								print "stm_ai('p0i0',[1,'Contenidos','','',-1,-1,0,'../contenido/mostrarIndiceEdicion.php?idmateria=$idmateria&materia=$materia','_self','','Contenidos','../menu/".$modulo.".png','../menu/".$modulo.".png',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
						}
						if ($modulo=="planificador")	//para el planificador 
						{
							if($tipo==2 || $tipo==5 || $tipo==7) //profesor 
							{
								print "stm_ai('p0i0',[1,'Planificación','','',-1,-1,0,'','_self','','Planificación','../menu/".$modulo.".png','../menu/".$modulo.".png',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_aix('p1i0','p0i0',[1,'Ver Planificación','','',-1,-1,0,'../planificador/verPlanificador.php?idmateria=$idmateria&materia=$materia','_self','','Ver Planificación','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
									stm_aix('p1i1','p0i0',[1,'Gestionar Planificación','','',-1,-1,0,'../planificador/agregarPlanificador.php?idmateria=$idmateria&materia=$materia','_self','','Crear Planificación','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);
									stm_ep();";
							}
							else if($tipo==3 || $tipo==6) //estudiante solo ver planificador
							{
								print "stm_ai('p0i0',[1,'".ucwords($modulo)."','','',-1,-1,0,'../planificador/verPlanificadorEstudiante.php?idmateria=$idmateria&materia=$materia','_self','','".ucwords($modulo)."','../menu/".$modulo.".png','../menu/".$modulo.".png',22,22,0,'../menu/arrow_r.gif','../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
								print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);
									stm_ep();";
							}
						}	                        
					}
				}
			}
		}
	}
	if ($tipo!=1)
	{
		if (!empty($_GET['idmateria']))
		{	
			$longitudNombreDirectorio=10;
			if ($tipo==2 || $tipo==3 || $tipo==7)
			{
/*				$sql="SELECT mei_directorio.iddirectorio, mei_directorio.directorio, mei_directorio.ubicacion, mei_directorio.nombre
					FROM mei_directorio WHERE mei_directorio.idusuario IN 
					(SELECT mei_relusugru.idusuario FROM mei_relusugru WHERE mei_relusugru.idgrupo IN
					(SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario=".$_SESSION['idusuario']."))"; */

				$sql="SELECT mei_directorio.iddirectorio, mei_directorio.directorio, mei_directorio.ubicacion, mei_directorio.nombre
						FROM mei_directorio WHERE mei_directorio.iddirectorio IN (SELECT mei_reldirgru.iddirectorio FROM mei_reldirgru
						WHERE mei_reldirgru.idgrupo IN ( SELECT mei_grupo.idgrupo FROM mei_grupo 
						WHERE mei_grupo.idmateria =".$_GET["idmateria"]." AND mei_grupo.idgrupo IN (SELECT mei_relusugru.idgrupo
						FROM mei_relusugru WHERE mei_relusugru.idusuario =".$_SESSION['idusuario'].")))";					
			}
			else if ($tipo==5 || $tipo==6)
			{
	/*			$sql="SELECT mei_directorio.iddirectorio, mei_directorio.directorio, mei_directorio.ubicacion, mei_directorio.nombre
					FROM mei_directorio WHERE mei_directorio.idusuario IN 
					(SELECT mei_relusuvirgru.idusuario FROM mei_relusuvirgru WHERE mei_relusuvirgru.idvirgrupo IN
					(SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario=".$_SESSION['idusuario']."))"; */
				
				$sql="SELECT mei_directorio.iddirectorio, mei_directorio.directorio, mei_directorio.ubicacion, mei_directorio.nombre
						FROM mei_directorio WHERE mei_directorio.iddirectorio IN (SELECT mei_reldirvirgru.iddirectorio FROM mei_reldirvirgru
						WHERE mei_reldirvirgru.idvirgrupo IN ( SELECT mei_virgrupo.idvirgrupo FROM mei_virgrupo 
						WHERE mei_virgrupo.idvirmateria =".$_GET["idmateria"]." AND mei_virgrupo.idvirgrupo IN (SELECT mei_relusuvirgru.idvirgrupo
						FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario'].")))";					
			}
			$directorios=$baseDatos->ConsultarBD($sql);
			while (list($idDirectorio,$directorio,$ubicacion,$nombre)=mysql_fetch_array($directorios))//directorio=nombre 
			{	
				$dirNombreUbicacion=$ubicacion."/".$directorio;
				if(file_exists($dirNombreUbicacion))
				{
					if (strlen($nombre)>$longitudNombreDirectorio)
					{
						$nombreDirectorio=substr($nombre,0,$longitudNombreDirectorio)."...";	
					}
					else
					{
						$nombreDirectorio=$nombre;
					}
					print "stm_ai('p0i0',[1,'".$nombreDirectorio."','','',-1,-1,0,'','_self','','".ucwords($directorio)."','../menu/directorio.gif','../menu/directorio.gif',22,22,0,'../menu/../menu/arrow_r.gif','../menu/../menu/arrow_r.gif',7,7,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,1,1,'#000000','#000000','#000000','#000000','8pt Verdana','8pt Verdana',0,0]);";
					print "	stm_bp('p1',[1,2,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);";
						
					if($tipo==2 || $tipo==5)
					{
						print "stm_aix('p1i0','p0i0',[1,'Editar Directorio','','',-1,-1,0,'../lanzadores/editarDirectorio.php?idmateria=$idmateria&materia=$materia&idDirectorio=$idDirectorio','_self','','Editar Directorio','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
					}
					
					$sql="SELECT mei_archivos.idarchivo,mei_archivos.titulo,mei_archivos.descripcion,mei_archivos.tamano,mei_archivos.fechacarga,mei_archivos.archivo FROM mei_archivos WHERE mei_archivos.iddirectorio='".$idDirectorio."'";
					$archivos=$baseDatos->ConsultarBD($sql);	
					while (list($idArchivo,$titulo,$descripcion,$tamano,$fecha,$archivo)=mysql_fetch_array($archivos))
					{
						if(file_exists($ubicacion.$directorio."/".$archivo))
						{
							print "stm_aix('p1i1','p0i0',[1,'<img align=texttop src=../menu/archivo.gif> ".$titulo."','','',-1,-1,0,'".$ubicacion.$directorio."/".$archivo."','_black','','Descripcion:".$descripcion."','','',0,0,0,'','',0,0,0,0,1,'#FFFFFF',0,'#CCCCCC',0,'','',3,3,0,0,'#000000','#990000']);";
						}
						else
						{
							$sql="DELETE FROM mei_archivos WHERE mei_archivos.idarchivo = '".$idArchivo."'";
							$consulta=$baseDatos->ConsultarBD($sql);
						}
					}
					print "stm_ep();";
				}
				else
				{
					$sql="DELETE FROM mei_directorio WHERE mei_directorio.iddirectorio = '".$idDirectorio."'";
					$consulta=$baseDatos->ConsultarBD($sql);
				}
			}
		}
	}
	print "stm_em();
	</script>";
	print "</td></tr></table>";
}
?>