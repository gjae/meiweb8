<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{	
	$baseDatos= new BD();	
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraBasica' , '' );
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
<script>
	function enviar()
	{
		document.frm_eliminar.submit();
	}
</script>
</head>
<body>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
	      	<td valign="top" class="tdGeneral">
            	<table class="tablaMarquesina" >
             	 <tr>
<?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado);
			
?>
                <td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a><a> -> </a><a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>" class="link"><?=ucwords(strtolower($_GET['materia']))?></a><a> -> </a><a href="index.php?estado=2&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET["materia"]?>" class="link">Notas</a> -> Reinciar Notas </td>
              </tr>
            </table>&nbsp;
<form name="frm_eliminar" method="post" action="guardarNotas.php?accion=REI&idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>">
		      <table class="tablaGeneral">
                <tr class="trSubTitulo">
                  <td width="100%" colspan="2" align="center" valign="middle"><h3>Advertencia&iexcl;&iexcl;&iexcl;</h3>
                    <input name="hid_materia" type="hidden" id="hid_materia" value="<?=$_GET["idmateria"]?>">
                  <input name="hid_grupo" type="hidden" id="hid_grupo" value="<?=$_GET["idgrupo"]?>"></td>
                </tr>
                <tr class="trListaOscuro">
                  <td colspan="2" align="center" valign="middle">&nbsp;</td>
                </tr>
                <tr class="trListaOscuro">
                  <td colspan="2" align="center" valign="middle">Se eliminará todas las Notas de este grupo; Se recomineda sacar una Copia de Seguridad. </td>
                </tr>
                <tr class="trListaOscuro">
                  <td colspan="2" align="center" valign="middle">&nbsp;</td>
                </tr>
              </table>
		      
		      <table class="tablaGeneral">
                <tr align="center" valign="middle" class="trSubTitulo">
                  <td><a href="javascript:enviar()" class="link">Continuar</a></td>
                  <td><a href="index.php?estado=2&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>&materia=<?=$_GET['materia']?>" class="link">Volver</a></td>
                </tr>
              </table>
</form>
			  </td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>