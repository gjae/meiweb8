<?

	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../menu1/Menu1.class.php');	
	if(comprobarSession())
	{	
	$baseDatos= new BD();	
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraBasica' , '' );
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
</head>
<body>
<?
	$sql = "SELECT mei_usuario.primernombre, mei_usuario.segundonombre, mei_usuario.primerapellido, mei_usuario.segundoapellido,
			mei_evaluacion.tipoevaluacion, mei_tipoevaluacion.tipoevaluacion, mei_evaluacion.nombre, mei_evaluacion.valor_eval, 
			mei_evaluacion.promediar FROM mei_evaluacion,
			mei_usuario, mei_tipoevaluacion	WHERE mei_evaluacion.idautor = mei_usuario.idusuario AND 
			mei_evaluacion.tipoevaluacion = mei_tipoevaluacion.idtipoevaluacion AND
			mei_evaluacion.idevaluacion = '".$_GET["idevaluacion"]."'";
	//print $sql;
	$resultado = $baseDatos->ConsultarBD($sql);
	
	list( $pnombre, $snombre, $papellido, $sapellido, $idtipoeva, $tipoeva, $nombre, $valor, $promediar) = mysql_fetch_row($resultado);
?>
	<table height="260" class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
		    <td valign="top" class="tdGeneral"><table class="tablaMarquesina" >
              <tr>
<?
		if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			$sql = "SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$_GET['idmateria'];
		else
			$sql = "SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$_GET['idmateria'];
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombre) = mysql_fetch_row($resultado);
			
		?>
                <td class="BloqueClaroB"><a href="../scripts/" class="link">Inicio</a>
                <a> -> </a>
                <a href="../scripts/homeMateria.php?idmateria=<?=$_GET['idmateria']?>" class="link"><?=ucwords(strtolower($nombre))?></a>
                <a> -> </a>
                <?
				  	if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==5)
					{
				  ?>
				  <a href="index.php?estado=2&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>" class="link">Notas</a>
				  <?
				  	}
					else
					{
				  ?>
				  <a href="notasAlumno.php?idmateria=<?=$_GET["idmateria"]?>" class="link">Notas</a>
				  <?
				  	}
				  ?>
                  <a> -> Ver Notas </a></td>
              </tr>
            </table>&nbsp;			
		      <table class="tablaGeneral" border="0" align="center">
                <?
	  	if ( !empty($_GET["idmateria"])) $idmateria = $_GET["idmateria"];
		else $idmateria = $_POST["hid_materia"];
	  	if ( !empty($_GET["idgrupo"])) $idgrupo = $_GET["idgrupo"];
		else $idgrupo = $_POST["hid_grupo"];
		$sql="SELECT mei_grupo.nombre FROM mei_grupo WHERE mei_grupo.idgrupo=".$idgrupo;
		$resultado = $baseDatos->ConsultarBD($sql);
		list($nombregrupo) = mysql_fetch_row($resultado);
	  ?>
                <tr class="trTitulo">
                  <td colspan="4" align="left" valign="middle">Carasteristicas de la Nota</td>
                </tr>
                <tr class="trInformacion">
                  <td width="20%" align="left" valign="middle"><strong>Autor:</strong></td>
                  <td colspan="3" align="left" valign="middle"><strong>
                  <?=$pnombre." ".$snombre." ".$papellido." ".$sapellido?>
                  </strong></td>
                </tr>
                <tr class="trInformacion">
                  <td align="left" valign="middle"><strong>Tipo de Nota </strong></td>
                  <td colspan="3" align="left" valign="middle"><?=$tipoeva?></td>
                </tr>
                <tr class="trInformacion">
                  <td align="left" valign="middle"><strong>Nombre de la Materia : </strong></td>
                  <td colspan="3" align="left" valign="middle"><?=$nombre?></td>
                </tr>
                <tr class="trInformacion">
                  <td align="left" valign="middle"><strong>Nombre del Grupo : </strong></td>
                  <td colspan="3" align="left" valign="middle"><?=$nombregrupo?></td>
                </tr>
                <tr class="trInformacion">
                  <td align="left" valign="middle"><strong>Valor:</strong></td>
                  <?
                  $valor=100*$valor;
				  ?>
                  <td colspan="3" align="left" valign="middle"><?=$valor."%"?></td>
                </tr>
              </table>
			  <?
			  	if ($idtipoeva == 1)
				{//TIPO PREVIO
					$sql = "SELECT mei_evaprevio.idprevio, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion,
							mei_evaprevio.fechafinalizacion FROM mei_evaprevio WHERE 
							mei_evaprevio.idevaluacion = '".$_GET["idevaluacion"]."'";
					$url = "../evaluaciones/verDtlleEvaluacion.php?idmateria=".$_GET['idmateria']."&idgrupo=".$_GET["idgrupo"]."&idprevio=";
					$frase = "No hay Previos asignados a esta Nota";
					$param = 1;
				}
				elseif ($idtipoeva == 2) 
				{
					$sql = "SELECT mei_actividad.idactividad, mei_actividad.titulo, mei_actividad.fechaactivacion,
							mei_actividad.fechafinalizacion, mei_actividad.valor, mei_actividad.visibilidad	FROM mei_actividad 
							WHERE mei_actividad.idevaluacion = '".$_GET["idevaluacion"]."' ORDER BY mei_actividad.visibilidad DESC";
					$url = "../actividades/verDtlleActividad.php?idmateria=".$_GET['idmateria']."&idactividad=";

					$sql2 = "SELECT mei_foro.idforo, mei_foro.mensaje, mei_foro.fechaactivacion,mei_foro.fechacaducidad, 
							mei_foro.valor	
							FROM mei_foro WHERE mei_foro.idevaluacion = '".$_GET["idevaluacion"]."' AND 
							mei_foro.calificable = 1";
					//print $sql2;		
					$resultado2 = $baseDatos->ConsultarBD($sql2);
					
					$sql3 = "SELECT mei_evaprevio.idprevio, mei_evaprevio.titulo, mei_evaprevio.fechaactivacion,
							mei_evaprevio.fechafinalizacion, mei_evaprevio.valor, mei_evaprevio.visibilidad FROM mei_evaprevio WHERE 
							mei_evaprevio.idevaluacion = '".$_GET["idevaluacion"]."' AND mei_evaprevio.idtipoprevio = 2 ORDER BY mei_evaprevio.visibilidad ASC";
					//print $sql2;		
					$resultado3 = $baseDatos->ConsultarBD($sql3);
					
					$frase = "No hay Actividades asignadas a esta Nota";
					$param = 0;
				}
				$resultado = $baseDatos->ConsultarBD($sql);
				
				if ($promediar==1)
				{
					$den = mysql_num_rows($resultado) + mysql_num_rows($resultado2) + mysql_num_rows($resultado3);
					if ($den>0)
						$valorProm = round( (1/$den) ,2);
					else $valorProm = 1;
				}				
				if ((@mysql_num_rows($resultado) > 0) || (@mysql_num_rows($resultado2) > 0)|| (@mysql_num_rows($resultado3) > 0))
				{//HAY PREVIOS/ACTIVIDADES				
			  ?>
			  &nbsp;
		      <table class="tablaGeneral">
                <tr class="trTitulo">
                  <td colspan="4"><?=$tipoeva?>                     
                  Asignados a esta Nota </td>
                </tr>
                <tr align="center" valign="middle" class="trSubTitulo">
                  <td width="25%" class="trSubTitulo">Nombre</td>
                  <td width="35%" class="trSubTitulo">Fecha Activaci&oacute;n </td>
                  <td width="35%" class="trSubTitulo">Fecha Finalizaci&oacute;n </td>
				  <td width="5%" class="trSubTitulo">Valor</td>
                  <? /*
				  	if ($_SESSION["idtipousuario"]==3)
					{
				  ?>
				  <td width="5%" class="trSubTitulo">Nota</td>
				  <?
				  	}*/
				  ?>
                </tr>
				<?
					while (	list( $id, $titulo, $fecha_a, $fecha_f, $valor, $visibilidad) = mysql_fetch_row($resultado) )
					{
						/*$sql = "SELECT mei_usuactividad.nota FROM mei_usuactividad WHERE mei_usuactividad.idactividad='".$id."'
								AND mei_usuactividad.idusuario = '".$_SESSION["idusuario"]."'";
						$res1 = $baseDatos->ConsultarBD($sql);
						list($usuNota) = mysql_fetch_row($res1);*/
				?>
                <tr align="center" valign="middle" class="trListaClaro" <?php if($visibilidad == 1){ echo "style='color:#999999;'"; } ?>>
                  <td class="trListaClaro">
				  <?
				  	if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==5)
					{
				  ?>
				  <a href="<?=$url.$id?>" class="link" <?php if($visibilidad == 1){ echo "style='color:#999999 !important;'"; } ?>><?=$titulo?></a>
				  <?
				  	}else print $titulo;
				  ?>				  </td>
                  <td class="trListaClaro"><?=mostrarFechaTexto($fecha_a,1)?></td>                 
				  <td class="trListaClaro"><?=mostrarFechaTexto($fecha_f,1)?></td>
				  <td class="trListaClaro">
				  <? 
				  	if ($idtipoeva==2) 
					{
						if ($promediar==1)	print ($valorProm*100)."%"; 
						else print ($valor)."%"; 
					}
					else print "100%"
				  ?>				  
				  </td>
                  <? /*
				  	if ($_SESSION["idtipousuario"]==3)
					{
				  ?>
                  <td class="trListaClaro"><? if(usuNota>=0) print $usuNota; else print "---";?></td>
				  <?
				  	}*/
				  ?>
                </tr>
				<?
					
					}

					while (	list( $id, $mensaje, $fecha_a,$fecha_f, $valor) = @mysql_fetch_row($resultado2) )
					{//while $resultado2
						/*$sql = "SELECT mei_comentarioforo.calificacion FROM mei_comentarioforo WHERE mei_comentarioforo.idforo='".$id."'
								AND mei_comentarioforo.idusuario = '".$_SESSION["idusuario"]."'";
						$res1 = $baseDatos->ConsultarBD($sql);
						list($usuNota) = mysql_fetch_row($res1);*/
				?>
                <tr align="center" valign="middle" class="trListaClaro">
                  <td class="trListaClaro">
				  <?
				  	if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==5)
					{
				  ?>
				  <a href="../foro/verMensajeForo.php?idmateria=<?=$_GET["idmateria"]?>&codigoMensaje=<?=$id?>" class="link">
					<?
							$mensaje=strip_tags(stripslashes(base64_decode($mensaje)));
							if (strlen($mensaje)>20)
								print substr($mensaje,0,20)."...";
							else print $mensaje;
						
					?></a>
				  <?
				  	}else
						{
							$mensaje=strip_tags(stripslashes(base64_decode($mensaje)));
							if (strlen($mensaje)>20)
								print substr($mensaje,0,20)."...";
							else print $mensaje;
						} 
				  ?>				  </td>
                  <td class="trListaClaro"><?=mostrarFecha($fecha_a,false)?></td>                  
				  <td class="trListaClaro"><?=mostrarFecha($fecha_f,false)?></td>
                  <td class="trListaClaro">
				  <?
						if ($promediar==1)	print ($valorProm*100)."%"; 
						else print ($valor*100)."%"; 
				  ?>
				  </td>
                  <? /*
				  	if ($_SESSION["idtipousuario"]==3)
					{
				  ?>
                  <td class="trListaClaro"><? if(usuNota>=0) print $usuNota; else print "---";?></td>
				  <?
				  	}*/
				  ?>
                </tr>
				<?
					}//fin while $resultado2
					while (	list( $id, $titulo, $fecha_a,$fecha_f, $valor, $visibilidad) = @mysql_fetch_row($resultado3) )
					{//while $resultado2
						/*$sql = "SELECT mei_usuprevio.nota FROM mei_usuprevio WHERE mei_usuprevio.idprevio='".$id."'
								AND mei_usuprevio.idusuario = '".$_SESSION["idusuario"]."'";
						$res1 = $baseDatos->ConsultarBD($sql);
						list($usuNota) = mysql_fetch_row($res1);*/
				?>
                <tr align="center" valign="middle" class="trListaClaro" <?php if($visibilidad == 1){ echo "style='color:#999999;'"; } ?>>
                  <td class="trListaClaro">
				  <?
				  	if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==5)
					{
				  ?>
				  <a href="../evaluaciones/verDtlleEvaluacion.php?idmateria=<?=$_GET["idmateria"]?>&idprevio=<?=$id?>&idgrupo=<?=$_GET['idgrupo']?>" class="link" <?php if($visibilidad == 1){ echo "style='color:#999999 !important;'"; } ?>>					
					<?
							if (strlen($titulo)>20)
								print substr($titulo,0,20)."...";
							else print $titulo;
						
					?></a>
				  <?
				  	}else
						{
							if (strlen($titulo)>20)
								print substr($titulo,0,20)."...";
							else print $titulo;
						}
				  ?>				  </td>
                  <td class="trListaClaro"><?=mostrarFechaTexto($fecha_a,1)?></td>                  
				  <td class="trListaClaro"><?=mostrarFechaTexto($fecha_f,1)?></td>
                  <td class="trListaClaro">
				  <?
						if ($promediar==1)	print ($valorProm*100)."%"; 
						else print ($valor)."%"; 

				  ?></td>
                  <? /*
				  	if ($_SESSION["idtipousuario"]==3)
					{
				  ?>
                  <td class="trListaClaro"><? if(usuNota>=0) print $usuNota; else print "---";?></td>
				  <?
				  	}*/
				  ?>
                </tr>
				<?
					}//fin while $resultado2
				?>				
              </table>
			  <?
				}//FIN HAY PREVIOS/ACTIVIDADES				  
				else
				{
			  ?>&nbsp;
			  <table class="tablaGeneral">
                <tr align="center" valign="middle" class="trListaOscuro">
                  <td>&nbsp;</td>
                </tr>
                <tr align="center" valign="middle" class="trListaOscuro">
                  <td><strong>
                  <?=$frase?>
                  </strong></td>
                </tr>
                <tr align="center" valign="middle" class="trListaOscuro">
                  <td>&nbsp;</td>
                </tr>
              </table>
			  <?
			  	}
				if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==5)
				{
			  ?>&nbsp;
		      <table class="tablaGeneral">
                <tr align="center" valign="middle" class="trInformacion">
                  <td width="50%"><a href="modificarNota.php?idevaluacion=<?=$_GET["idevaluacion"]?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>" class="link"><img src="imagenes/modificar.gif" width="16" height="16" border="0">Editar</a></td>
                  <td width="50%"><a href="eliminarNota.php?idevaluacion=<?=$_GET["idevaluacion"]?>&tipoeva=<?=$idtipoeva?>&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>" class="link"><img src="imagenes/eliminar.gif" width="16" height="16" border="0">Eliminar</a></td>
                </tr>
              </table>
			  <?
			  	}
			  ?>
			  &nbsp;
			  
		      <table class="tablaGeneral">
                <tr align="center" valign="middle" class="trSubTitulo">
                  <td>
				  <?
				  	if ($_SESSION["idtipousuario"]==2 || $_SESSION["idtipousuario"]==5)
					{
				  ?>
				  <a href="index.php?estado=2&idmateria=<?=$_GET["idmateria"]?>&idgrupo=<?=$_GET["idgrupo"]?>" class="link">Volver</a>
				  <?
				  	}
					else
					{
				  ?>
				  <a href="notasAlumno.php?idmateria=<?=$_GET["idmateria"]?>" class="link">Volver</a>
				  <?
				  	}
				  ?>				 
				  </td>
                </tr>
              </table>
              </td>
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?
}
else redireccionar('../login/');
?>