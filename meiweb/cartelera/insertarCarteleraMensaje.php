<?

	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');
	
	if(comprobarSession())
	{
			
	$baseDatos= new BD();

	if(!empty($_POST['txt_mensajeCartelera']))
		{	
			if($_POST['rad_grupo']=="todos")
			{
				$destino=0;			
			}
			else if($_POST['rad_grupo']=="seleccionados")
			{
				$destino=1;
			}
			else if($_POST['rad_grupo']=="general")
			{
				$destino=2;
			}
			
			
			if(!comprobarEditor($_POST['edt_descripcionCartelera']))
			{
					redireccionar("agregarCarteleraMensaje.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&error=0x001&titulo=".$_POST['txt_mensajeCartelera']);
			}
			else
			{
			
					if($_POST['rad_caducidad']== 1)
					{
						$fechaCaducidad=$_POST['txt_fecha'];
						$caducidad=1;
					}
					else if($_POST['rad_caducidad']== 2)
					{
						$fechaCaducidad="0000-00-00";
						$caducidad=2;
					} 
					else if($_POST['rad_caducidad']== 3)
					{
						$fechaCaducidad="0000-00-00";
						$caducidad=3;
					}
					
					registrarBitacora(3,9,false);
					
					$sql="INSERT INTO mei_cartelera ( idcartelera , mensaje , estado , idusuario , fechacreacion , fechaactivacion , caducidad , fechacaducidad , destino) 
						VALUES ('', '".base64_encode($_POST['txt_mensajeCartelera']."[$$$]". eliminarEspacios($_POST['edt_descripcionCartelera']))."', '0', '".$_SESSION['idusuario']."', '".date('Y-n-j')."', '".date('Ymd').date("GH")."', '".$caducidad."', '".$fechaCaducidad."' , ".$destino.")";
									
					$baseDatos->ConsultarBD($sql);		
					
					$idCartelera=$baseDatos->InsertIdBD();				
					
					
					if($_POST['rad_grupo'] =="general")
					{
						if ($_SESSION['idtipousuario']==5)
							$sql="SELECT DISTINCT mei_virgrupo.idvirgrupo FROM mei_virgrupo";
						else
							$sql="SELECT DISTINCT mei_grupo.idgrupo FROM mei_grupo";
						$consultaGrupo=$baseDatos->ConsultarBD($sql);	
						
						while(list($idGrupo)=mysql_fetch_array($consultaGrupo))						
						{
							if ($_SESSION['idtipousuario']==5)
								$sql="INSERT INTO mei_relcarvirgru ( idcartelera , idvirgrupo ) VALUES ('".$idCartelera."','".$idGrupo."')";
							else
								$sql="INSERT INTO mei_relcargru ( idcartelera , idgrupo ) VALUES ('".$idCartelera."','".$idGrupo."')";
							$baseDatos->ConsultarBD($sql);						
						}
					}
					else
					{
					
					$valores='';				
								
						
					
					if($_POST['rad_caducidad']== 1)
					{
						for($i=0;$i<$_POST['hid_contGrupo'];$i++)
						{	
							if(!empty($_POST['chk_idgrupo'.$i]))
							{
								if(empty($valores))
									$valores="('".$idCartelera."', '".$_POST['chk_idgrupo'.$i]."')"; 
								else
									$valores.=" , ('".$idCartelera."', '".$_POST['chk_idgrupo'.$i]."')"; 	
							}					
							
						}
						if ($_SESSION['idtipousuario']==5)
							$sql="INSERT INTO mei_relcarvirgru ( idcartelera , idvirgrupo ) VALUES ".$valores;				
						else
							$sql="INSERT INTO mei_relcargru ( idcartelera , idgrupo ) VALUES ".$valores;				
					}
					else if($_POST['rad_caducidad']== 2)
					{ 
						for($i=0;$i<$_POST['hid_contGrupo'];$i++)
						{	
							if(!empty($_POST['chk_idgrupo'.$i]))
							{
								if(empty($valores1))
									$valores1="('".$idCartelera."', '".$_POST['chk_idgrupo'.$i]."')"; 
								else
									$valores1.=" , ('".$idCartelera."', '".$_POST['chk_idgrupo'.$i]."')"; 	
							}					
							
						}
						if ($_SESSION['idtipousuario']==5)
							$sql1="INSERT INTO mei_relcarvirgru ( idcartelera , idvirgrupo ) VALUES ".$valores1;				
						else
							$sql1="INSERT INTO mei_relcargru ( idcartelera , idgrupo ) VALUES ".$valores1;
						
						$consulta1=$baseDatos->ConsultarBD($sql1);
						unset($valores1);

						for($i=0;$i<$_POST['hid_contGrupo'];$i++)
						{	
							if(!empty($_POST['chk_idgrupo'.$i]))
							{
								if(empty($valores))
								{
									if ($_SESSION['idtipousuario']==5){
										$valores="mei_relusuvirgru.idvirgrupo='".$_POST['chk_idgrupo'.$i]."'"; 
									}
									else{
										$valores="mei_relusugru.idgrupo='".$_POST['chk_idgrupo'.$i]."'"; 
									}
								}
								else
								{
									if ($_SESSION['idtipousuario']==5){
										$valores.=" OR mei_relusuvirgru.idvirgrupo='".$_POST['chk_idgrupo'.$i]."'"; 	
									}
									else{
										$valores.=" OR mei_relusugru.idgrupo='".$_POST['chk_idgrupo'.$i]."'"; 	
									}
								}
							}					
							
						}

						if ($_SESSION['idtipousuario']==5)
							$sql="SELECT DISTINCT(mei_relusuvirgru.idusuario) FROM mei_relusuvirgru WHERE ".$valores;
						else
							$sql="SELECT DISTINCT(mei_relusugru.idusuario) FROM mei_relusugru WHERE ".$valores;
						$consulta=$baseDatos->ConsultarBD($sql);
						
						$valores='';
						
						while(list($idUsuario)=mysql_fetch_array($consulta))
						{
							if(empty($valores))
									$valores="('".$idCartelera."', 0 , '".$idUsuario."')"; 
								else
									$valores.=" , ('".$idCartelera."', 0 , '".$idUsuario."')";					
						}
						
						$sql="INSERT INTO mei_relcarusu ( idcartelera , estado , idusuario ) VALUES ".$valores;
					}
					
					if($_POST['rad_caducidad']== 3)
					{
						for($i=0;$i<$_POST['hid_contGrupo'];$i++)
						{	
							if(!empty($_POST['chk_idgrupo'.$i]))
							{
								if(empty($valores))
									$valores="('".$idCartelera."', '".$_POST['chk_idgrupo'.$i]."')"; 
								else
									$valores.=" , ('".$idCartelera."', '".$_POST['chk_idgrupo'.$i]."')"; 	
							}					
							
						}
						if ($_SESSION['idtipousuario']==5)
							$sql="INSERT INTO mei_relcarvirgru ( idcartelera , idvirgrupo ) VALUES ".$valores;				
						else
							$sql="INSERT INTO mei_relcargru ( idcartelera , idgrupo ) VALUES ".$valores;				
					}
				}	
							
					$baseDatos->ConsultarBD($sql);
			}
		}
	
			redireccionar("../cartelera/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
			}
else
	redireccionar('../login/');
	
?>