<?
	include_once ('../menu1/Menu1.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../librerias/vistas.lib.php');
	
	
	if(comprobarSession())
	{
	$baseDatos= new BD();

	
	if(!empty($_GET['idCartelera']))
	{
		$sql="SELECT mei_cartelera.mensaje, mei_cartelera.caducidad, mei_cartelera.fechacaducidad , mei_cartelera.destino FROM mei_cartelera 
					WHERE mei_cartelera.idcartelera =".$_GET['idCartelera'];
		
		$consulta=$baseDatos->ConsultarBD($sql);
		list($mensaje,$caducidad,$fechacaducidad, $destino)= mysql_fetch_array($consulta);
		
			if($destino==0)
			{
				$destinoTodos="checked";
				$gruposCheck="checked";
			}
			else if($destino==1)
			{
				$destinoSeleccionados="checked";
				
				if ($_SESSION['idtipousuario']==5)
					$sql="SELECT mei_relcarvirgru.idvirgrupo FROM mei_relcarvirgru WHERE mei_relcarvirgru.idcartelera =".$_GET['idCartelera'];
				else
					$sql="SELECT mei_relcargru.idgrupo FROM mei_relcargru WHERE mei_relcargru.idcartelera =".$_GET['idCartelera'];
				$consulta=$baseDatos->ConsultarBD($sql);
				
				$cont=0;
				
				while(list($datoGrupo)=mysql_fetch_array($consulta))
				{
					$listaGrupos[$cont++]=$datoGrupo;
				}					
							
			}
			else if($destino==2)
			{
				$destinoGeneral="checked";
				$gruposCheck="checked";
			}
		
		
		
		if($caducidad==1) // Caducidad por fecha
		{
			$fechaInicial=$fechacaducidad;
			$checkedFecha="checked";
			$checkedVisita=" ";
		}
		else if($caducidad==2) // Caducidad por visita
		{
			$agnoInicial=date('Y');
			$mesInicial=date('n');
			$diaInicial=date('j');
			$mesInicial+=1;
			
			$fechaInicial=$agnoInicial.'-'.$mesInicial.'-'.$diaInicial;
						
			$checkedVisita="checked";
		}
		else if($caducidad==3) // Caducidad por visita
		{
			$agnoInicial=date('Y');
			$mesInicial=date('n');
			$diaInicial=date('j');
			$mesInicial+=1;
			
			$fechaInicial=$agnoInicial.'-'.$mesInicial.'-'.$diaInicial;
						
			$checkedInfinita="checked";
		}
	
?>
	<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		<style type="text/css">
		
		<!--
		body,td,th {
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-size: 11px;
		}
		
		.tabla{
		border-collapse:collapse;
		}
.Estilo1 {color: #FF0000}
		-->
		</style>
		
		<script language="javascript">
		
			function enviarCancelar()
			{
				location.replace("../cartelera/index.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&cbo_materia=<?=$_GET['cbo_materia']?>");		
			}
			
			function chk_habilitar()
			{
				for(i=0;i<document.frm_cartelera.elements.length;i++)
				{
					if(document.frm_cartelera.elements[i].id=="grupo")
						document.frm_cartelera.elements[i].disabled=false;
				}
			}
			
			function chk_deshabilitar()
			{
				for(i=0;i<document.frm_cartelera.elements.length;i++)
				{
					if(document.frm_cartelera.elements[i].id=="grupo")
						document.frm_cartelera.elements[i].checked=true;
				}
			}
			 
			function chk_click ()
			{
				var radioTodos=document.getElementById('radioT');
				var radioSelec=document.getElementById('radioS');
				radioTodos.checked=false;
				radioSelec.checked=true;
			}
			 
			function enviar()
			{
				var contBandera=0;
				for(i=0;i<document.frm_cartelera.elements.length;i++)
				{
					if(document.frm_cartelera.elements[i].checked==true && document.frm_cartelera.elements[i].id=="grupo")
						contBandera++;
				}
				
				if(contBandera == 0)
				{
					alert("Debe seleccionar un por lo menos un grupo destino");
				}
				else if(document.frm_cartelera.txt_mensajeCartelera.value == false)
				{
					alert("Debe llenar completamente la información solicitada");
				}
				else
				{
					document.frm_cartelera.submit(); 
				}
			}
			 
			function activarFecha()
			{
				document.frm_cartelera.txt_fecha.disabled=false;  
			}
			 
			function desactivarFecha()
			{
				document.frm_cartelera.txt_fecha.disabled=true;  
			}
		
		</script>
		</head>
		<body>
		<?
		$mensaje=stripslashes(base64_decode($mensaje));
		list($tituloMensaje,$descripcion)=explode("[$$$]",$mensaje);
		
		$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraCartelera' , $descripcion) ;
		
		?>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td valign="top" class="tdGeneral">
					<form action="actualizarCarteleraMensaje.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&cbo_materia=<?=$_GET['cbo_materia']?>" name="frm_cartelera" method="post" >
					<table class="tablaGeneral">
						<tr class="trTitulo">
							<td colspan="4" class="trTitulo"><img src="imagenes/cartelera.gif" width="16" height="16" align="texttop"> Modificar Mensaje</td>
						</tr>
						<?
							if($_GET['error'] == '0x001')
							{
						?>
								<tr class="trAviso">
									<td colspan="4"><img src="imagenes/error.gif" width="17" height="18" align="texttop"><span class="Estilo1"> Error: Debe escribir la descripción del Mensaje</span></td>
								</tr>
						<?
							}
						?>
						<tr class="trInformacion">
							<td width="26%"><b>T&iacute;tulo Mensaje:</b></td>
							<td colspan="3"><b><input name="txt_mensajeCartelera" type="text" size="50" value="<?= $tituloMensaje?>"></b></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4"><b>Mensaje:</b></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4"><? $editor->crearEditor();?></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4"><b>Opciones de Publicacion :</b> </td>
						</tr>
						<tr class="trInformacion">
							<td width="26%"><input name="rad_caducidad" type="radio" id="fecha" onClick="javascript:activarFecha()" value="1" <?= $checkedFecha?>>
								Ocultar Despues de la Fecha
							</td>
							<td width="18%"><? 
								$calendario=new FrmCalendario('frm_cartelera','txt_fecha','formulario','false',$fechaInicial);
								$calendario->CrearFrmCalendario(); 
								?></td>
							<td width="32%"><input name="rad_caducidad" type="radio" id="visita" onClick="javascript:desactivarFecha()" value="2" <?= $checkedVisita?>>
								Ocultar Despues de ser Leido
							</td>
						    <td width="24%"><input name="rad_caducidad" type="radio" value="3" onClick="javascript:desactivarFecha()" <?= $checkedInfinita?>>Siempre Visible </td>
						</tr>
						<tr class="trInformacion">
						<td colspan="4"><div align="center">
				 		<table width="269" border="0">
                   		<tr>
                      	<td width="89"><div align="right">
                        <input name="btn_agregar" type="button" value="Guardar Mensaje" onClick="javascript:enviar()">
                        </div></td>
                        <td width="141"><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript:enviarCancelar()"></td>
                        </tr>
                        </table>
				        </div></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4" valign="top"><table bgcolor="#DDDDDD" class="tablaPrincipal">
                              <tr>
                                <td colspan="5"><b>Destino:</b></td>
                                <td width="260" colspan="3">&nbsp;</td>
                              </tr>
                              <tr>
                                <td width="20"><input name="rad_grupo" type="radio" value="general" <?= $destinoGeneral?>></td>
<?                        
			if ($_SESSION['idtipousuario']==5)
			{
?>				
                              <td width="208">Todos los Grupos Virtuales</td>
<?                        
			}
			else
			{
?>	
                              <td width="208">Todos los Grupos Presenciales</td>
<?                        
			}
?>
                                <td width="20"><input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar()" <?= $destinoTodos?> id="radioT"></td>
                                <td width="168">Todos mis Grupos </td>
                                <td width="20"><input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()" id="radioS" <?= $destinoSeleccionados?>></td>
                                <td colspan="3">Grupos Seleccionados</td>
                              </tr>
                              <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td colspan="4"><table class="tablaPrincipal">
                                    <?
								$contChk=0;
								if ($_SESSION['idtipousuario']==5)
								{
									$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria 
											WHERE mei_virmateria.idvirmateria
												IN (
													SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo 
															WHERE mei_virgrupo.idvirgrupo
																IN (
																	SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
																			WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
																	)
													) GROUP BY (mei_virmateria.idvirmateria)"; 
								}
								else
								{
									$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia 
											WHERE mei_materia.idmateria
												IN (
													SELECT mei_grupo.idmateria FROM mei_grupo 
															WHERE mei_grupo.idgrupo
																IN (
																	SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																			WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
																	)
													) GROUP BY (mei_materia.idmateria)"; 
								}
								
								$materias=$baseDatos->ConsultarBD($sql);
								
								while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
								{
								?>
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td colspan="3"><b><?= $materiaNombre?></b></td>
                                    </tr>
                                    <?
								if ($_SESSION['idtipousuario']==5)
								{
									$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo 
												WHERE mei_virgrupo.idvirgrupo 
													IN (
														SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
																WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
														) AND mei_virgrupo.idvirmateria =".$materiaCodigo;
								}
								else
								{
									$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo 
												WHERE mei_grupo.idgrupo 
													IN (
														SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
														) AND mei_grupo.idmateria =".$materiaCodigo;
								}
									
									$grupos=$baseDatos->ConsultarBD($sql);
									
									while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
									{
									
										if(sizeof($listaGrupos) > 0)
										{
											if(in_array($grupoCodigo,$listaGrupos))
											{
												$checkedGrupo="checked";
											}
											else
											{
												$checkedGrupo=" ";
											}
										}
									
									?>
                                    <tr>
                                      <td width="26">&nbsp;</td>
                                      <td width="26">&nbsp;</td>
                                      <td width="33"><input id="grupo" name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>" <?= $checkedGrupo?> <?= $gruposCheck?>></td>
                                      <td width="280"><?= $grupoNombre?></td>
                                    </tr>
                                    <?
									$contChk++;
									}
								}  
								?>
                                </table></td>
                                <?
								$contChk=0;
								if ($_SESSION['idtipousuario']==5)
								{
									$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria 
											WHERE mei_virmateria.idvirmateria
												IN (
													SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo 
															WHERE mei_virgrupo.idvirgrupo
																IN (
																	SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
																			WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
																	)
													) GROUP BY (mei_virmateria.idvirmateria)"; 
								}
								else
								{
									$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia 
											WHERE mei_materia.idmateria
												IN (
													SELECT mei_grupo.idmateria FROM mei_grupo 
															WHERE mei_grupo.idgrupo
																IN (
																	SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																			WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
																	)
													) GROUP BY (mei_materia.idmateria)"; 
								}
								
								$materias=$baseDatos->ConsultarBD($sql);
								
								while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
								{
									if ($_SESSION['idtipousuario']==5)
									{
										$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo 
												WHERE mei_virgrupo.idvirgrupo 
													IN (
														SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
																WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
														) AND mei_virgrupo.idvirmateria =".$materiaCodigo;
									}
									else
									{
										$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo 
												WHERE mei_grupo.idgrupo 
													IN (
														SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
														) AND mei_grupo.idmateria =".$materiaCodigo;
									}
									
									$grupos=$baseDatos->ConsultarBD($sql);
									
									while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
									{
									?>
                                <?
									$contChk++;
									}
								}  
								?>
                                                        </table></td>
					    </tr>
						<tr class="trInformacion">
							<input type="hidden" name="hid_idCartelera" value="<?= $_GET['idCartelera']?>">
							<input name="hid_contGrupo" type="hidden" value="<?= $contChk?>">
							<td colspan="4"><div align="center">
							  <table width="269" border="0">
                                <tr>
                                  <td width="25"><input name="hid_contGrupo" type="hidden" value="<?= $contChk?>"></td>
                                  <td width="89"><div align="right">
                                    <input name="btn_agregar" type="button" value="Modificar Mensaje" onClick="javascript:enviar()">
</div></td>
                                  <td width="141"><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript:enviarCancelar()"></td>
                                </tr>
                              </table>
							</div></td>
						</tr>
					</table>
				</form>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
		
		<p>&nbsp;	    </p>
		<p>
		  <script language="javascript">
		
			var radioVisita=document.getElementById("visita");
			if(radioVisita.checked==true)
			{
				desactivarFecha();
			}
		
		</script>
        </p>
		</body>
	</html>
		<?
		
		}//else
		
		}
else
	redireccionar('../login/');
	
?>
