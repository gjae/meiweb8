<?
	include_once ('../menu1/Menu1.class.php');	
	include_once ('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	
	include_once ('../librerias/vistas.lib.php');
	$baseDatos= new BD();
	
	if(comprobarSession())
	{
		if (!empty($_GET['cbo_materia'])) {
			$_POST['cbo_materia']=$_GET['cbo_materia'];
		}

		$sql="SELECT mei_modulo.estado FROM  mei_modulo WHERE  mei_modulo.idmodulo=3";
		$resultado=$baseDatos->ConsultarBD($sql);
		list($estado)=mysql_fetch_array($resultado);
		if($estado==1)
		{
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">		
		<script language="javascript">
		
			function eliminarMensaje(idCartelera)
			{
				if(confirm("¿Está seguro de eliminar este Mensaje?"))
				{
					location.replace("eliminarCarteleraMensaje.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idCartelera="+idCartelera+"&cbo_materia=<?=$_POST["cbo_materia"]?>");					
				}
			}
			
			function enviar()
		{
			document.frm_calendario.submit();				
		}
		</script>
		</head>
		<body>
		
<?
	
	if(!empty($_GET['idCartelera']))			
	{
		registrarBitacora(3,8,false);
		
		if($_GET['visitado'] == 1)
		{
			$sql="UPDATE mei_relcarusu SET estado = '1' WHERE idcartelera = ".$_GET['idCartelera']."  AND `idusuario` = ".$_SESSION['idusuario'];
			$consulta=$baseDatos->ConsultarBD($sql);
		}		
		
		$sql="SELECT mei_cartelera.idcartelera , mei_cartelera.mensaje , mei_cartelera.idusuario , mei_cartelera.fechacreacion , mei_cartelera.caducidad , 	
				mei_cartelera.fechacaducidad FROM mei_cartelera WHERE mei_cartelera.idcartelera=".$_GET['idCartelera'];
					
	}
	else
	{		
		registrarBitacora(3,7,false);
		if (empty($_POST['cbo_materia']))
		{
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_cartelera.idcartelera, mei_cartelera.mensaje, mei_cartelera.idusuario, mei_cartelera.fechacreacion, mei_cartelera.caducidad,
					mei_cartelera.fechacaducidad FROM mei_cartelera WHERE
						( 
							(mei_cartelera.caducidad =1 OR mei_cartelera.caducidad =2 OR mei_cartelera.caducidad =3) AND mei_cartelera.idcartelera
								IN (
									SELECT mei_relcarvirgru.idcartelera
										FROM mei_relcarvirgru WHERE mei_relcarvirgru.idvirgrupo
											IN (
												SELECT mei_relusuvirgru.idvirgrupo
													FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario = '".$_SESSION['idusuario']."'
												)
									)
						)
					 ORDER BY (mei_cartelera.fechacreacion) DESC";
			}
			else
			{
				$sql="SELECT mei_cartelera.idcartelera, mei_cartelera.mensaje, mei_cartelera.idusuario, mei_cartelera.fechacreacion, mei_cartelera.caducidad,
					mei_cartelera.fechacaducidad FROM mei_cartelera WHERE
						( 
							(mei_cartelera.caducidad =1 OR mei_cartelera.caducidad =2  OR  mei_cartelera.caducidad =3) AND mei_cartelera.idcartelera
								IN (
									SELECT mei_relcargru.idcartelera
										FROM mei_relcargru WHERE mei_relcargru.idgrupo
											IN (
												SELECT mei_relusugru.idgrupo
													FROM mei_relusugru WHERE mei_relusugru.idusuario = '".$_SESSION['idusuario']."'
												)
									)
						)
					 ORDER BY (mei_cartelera.fechacreacion) DESC";
			}			
		} 
		else if (!empty($_POST['cbo_materia']))
		{
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_cartelera.idcartelera, mei_cartelera.mensaje, mei_cartelera.idusuario, mei_cartelera.fechacreacion, mei_cartelera.caducidad,
					mei_cartelera.fechacaducidad
				FROM mei_cartelera WHERE
					( 
						(mei_cartelera.caducidad =1 OR mei_cartelera.caducidad =2  OR mei_cartelera.caducidad =3) AND mei_cartelera.idcartelera
							IN (
								SELECT mei_relcarvirgru.idcartelera
									FROM mei_relcarvirgru WHERE mei_relcarvirgru.idvirgrupo
										IN (
												SELECT mei_relusuvirgru.idvirgrupo
													FROM mei_relusuvirgru, mei_virgrupo
														WHERE mei_relusuvirgru.idusuario = '".$_SESSION['idusuario']."' AND mei_relusuvirgru.idvirgrupo = mei_virgrupo.idvirgrupo
                                                          AND mei_virgrupo.idvirmateria='".$_POST['cbo_materia']."' 
												)
								)
					)
				 ORDER BY (mei_cartelera.fechacreacion) DESC";
			}
			else
			{
				$sql="SELECT mei_cartelera.idcartelera, mei_cartelera.mensaje, mei_cartelera.idusuario, mei_cartelera.fechacreacion, mei_cartelera.caducidad,
					mei_cartelera.fechacaducidad
				FROM mei_cartelera WHERE
					( 
						(mei_cartelera.caducidad =1 OR mei_cartelera.caducidad =2  OR mei_cartelera.caducidad =3) AND mei_cartelera.idcartelera
							IN (
								SELECT mei_relcargru.idcartelera
									FROM mei_relcargru WHERE mei_relcargru.idgrupo
										IN (
												SELECT mei_relusugru.idgrupo
													FROM mei_relusugru, mei_grupo
														WHERE mei_relusugru.idusuario = '".$_SESSION['idusuario']."' AND mei_relusugru.idgrupo = mei_grupo.idgrupo
                                                          AND mei_grupo.idmateria='".$_POST['cbo_materia']."' 
												)
								)
					)
				 ORDER BY (mei_cartelera.fechacreacion) DESC";
			}
		}

	}

				
		$consulta=$baseDatos->ConsultarBD($sql);			
			
		$contRegistro=0;		
		while(list($idCartelera , $mensajeDescripcion , $autor , $publicacion , $caducidad , $fechacaducidad)=mysql_fetch_array($consulta))
		{
			$sql="SELECT mei_usuario.primernombre, mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario = '".$autor."'";
			$datosAutor=$baseDatos->ConsultarBD($sql);						
			
			list($nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($datosAutor);			
			
			$nombreAutor=$nombre1." ".$nombre2;
			$apellidoAutor=$apellido1." ".$apellido2;
			
			$mensajeDescripcion=stripslashes(base64_decode($mensajeDescripcion));
									
			$listaMensajes[$contRegistro]=$idCartelera.'[$$$]'.$autor.'[$$$]'.$publicacion.'[$$$]'.$caducidad.'[$$$]'.$mensajeDescripcion.'[$$$]'.$nombreAutor.'[$$$]'.$apellidoAutor.'[$$$]'.$fechacaducidad;
			$contRegistro++;
		} 
						
			
			?>
		<table class="tablaPrincipal">
			<tr valign="top">
                <td class="tablaEspacio">&nbsp;</td>
                <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
                <td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td colspan="2"><img src="imagenes/cartelera.gif" width="16" height="16" align="texttop"> Ver Mensajes</td>
							<? if (empty($_GET['idmateria'])) {?>
								<td colspan="2" align="right"> <a href="../scripts/homeUsuario.php" class="link"> Volver  </td>..
						<?	} else {?>
							<td colspan="2" align="right"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"> Volver  </td>
							<?}?>
						</tr>
					</table>
                    
<? 
		     if( $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
			 {
?> 
                       <form name="frm_calendario"  method="post" action="<?=$_SERVER['PHP_SELF']?>">         
			<br>         
          <table class="tablaGeneral" width="200" border="0">
            <tr class="trTitulo">
<? 
				 if( $_SESSION['idtipousuario']==2)
				 {
?>          
                <td> Materia:  </td>
<? 
				 }
				 else if( $_SESSION['idtipousuario']==5)
				 {
?> 
                <td> Curso:  </td>
<?
				 }	
?>
                <td><select name="cbo_materia"   onChange="javascript:enviar()">
<? 
				 if( $_SESSION['idtipousuario']==2)
				 {
?>          
                	<option class='link' value="0">--Seleccione Materia--</option>
<? 
				 }
				 else if( $_SESSION['idtipousuario']==5)
				 {
?> 
                	<option class='link' value="0">--Seleccione Curso--</option>
<?
				 }
                    
				 if( $_SESSION['idtipousuario']==5)
				 {
					$sql132 = "SELECT DISTINCT(mei_virmateria.idvirmateria), mei_virmateria.nombre FROM mei_virmateria, mei_virgrupo,
						mei_relusuvirgru WHERE mei_virmateria.idvirmateria = mei_virgrupo.idvirmateria AND
						mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo 	AND 
						mei_relusuvirgru.idusuario =".$_SESSION['idusuario'];
				 }
				 else
				 {
					$sql132 = "SELECT DISTINCT(mei_materia.idmateria), mei_materia.nombre FROM mei_materia, mei_grupo,
						mei_relusugru WHERE mei_materia.idmateria = mei_grupo.idmateria AND
						mei_grupo.idgrupo = mei_relusugru.idgrupo 	AND 
						mei_relusugru.idusuario =".$_SESSION['idusuario'];
				 }
					
				$resultado22 = $baseDatos->ConsultarBD($sql132);
				while ( list($idmateria2, $nommateria) = mysql_fetch_row($resultado22) )
				{
					 unset($seleccion); 
					   if( $idmateria2== $_POST['cbo_materia'])
					   {
					  
					   $seleccion="selected";
					   }  			
			
				?>
    
    				<option class='link' value="<? print $idmateria2?>" <? print $seleccion ?>><? print $nommateria?></option>
    	
			
    <?
				mysql_free_result($resultado);
				}
		?>
       
  </select> </td>
            </tr>
          </table>
      </form>
        <?
         	 }
		if(!empty($listaMensajes))
		{
		
		foreach($listaMensajes as $datos)
		{
			list($idCartelera,$autor,$publicacion,$caducidad,$mensaje,$descripcion,$nombreAutor,$apellidoAutor,$fechacaducidad)=explode("[$$$]",$datos);
			

			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					
					$sql="SELECT mei_relcarvirgru.idvirgrupo FROM mei_relcarvirgru WHERE mei_relcarvirgru.idcartelera=".$idCartelera."";
					$consulta=$baseDatos->ConsultarBD($sql);
					list($idgru)= mysql_fetch_row($consulta);
			
					
					$sql="SELECT mei_virgrupo.idvirmateria, mei_virgrupo.nombre FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo=".$idgru."";
					$consulta=$baseDatos->ConsultarBD($sql);
					list($idmat,$nombregrupo)= mysql_fetch_row($consulta);

					$sql="SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria=".$idmat."";
					$consulta=$baseDatos->ConsultarBD($sql);
					list($nombremateria)= mysql_fetch_row($consulta);

					if (empty($nombremateria)) {
						$nombremateria='TODOS';
				}
			}
				else
				{


					$sql="SELECT count(mei_relcargru.idgrupo) FROM mei_relcargru WHERE mei_relcargru.idcartelera=".$idCartelera."";
					$consulta=$baseDatos->ConsultarBD($sql);
					list($cant)= mysql_fetch_row($consulta);
					if ($cant==1){	

					$sql="SELECT mei_relcargru.idgrupo FROM mei_relcargru WHERE mei_relcargru.idcartelera=".$idCartelera."";
					$consulta=$baseDatos->ConsultarBD($sql);
					list($idgru)= mysql_fetch_row($consulta);		
					
					$sql="SELECT mei_grupo.idmateria, mei_grupo.nombre FROM mei_grupo WHERE mei_grupo.idgrupo=".$idgru."";
					$consulta=$baseDatos->ConsultarBD($sql);
					list($idmat,$nombregrupo)= mysql_fetch_row($consulta);

					$sql="SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria=".$idmat."";
					$consulta=$baseDatos->ConsultarBD($sql);
					list($nombremateria)= mysql_fetch_row($consulta);

					}
					else{
						$nombremateria='GRUPOS';
						$nombregrupo=' ';
					}

				}


		?>	
			  <table class="tablaGeneral" >
<tr class="trInformacion">
			  		<td style="font-weight: bold;" width="10%"><img src="imagenes/cartelera.gif" width="16" height="16" align="texttop">Cartelera</td>
			  		<td width="25%"></td>
			  		<td width="25%"></td>
			  		<td width="25%"></td>
			  		</tr>
						<tr class="trInformacion">
			<?			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
?>
							<td width="10%"><b>Curso: </b></td>
<?
					}
					else
					{
?>
							<td width="10%"><b>Materia: </b></td>
<?
					} ?> 

					<td width="25%"><?= $nombremateria ?></td>
							<td width="25%"><div align="right"><b>Grupo:</b></div></td>
							<td width="25%"><div align="right"><?=$nombregrupo;?></div><div align="right"></div></td>
						<tr  class="trInformacion">
                             <td width="25%"><b>Autor Cita: </b></td>
                              <td width="25%"><?= $nombreAutor." ".$apellidoAutor ?></td>
                              <td width="25%"><div align="right"><b>Fecha de Publicaci&oacute;n</b>: </div></td>
                              <td width="25%"><div align="right"><?= mostrarFecha($publicacion,false);?>
                              </div></td>
						</tr>
					  	<tr class="trInformacion">
							<td><b>T&iacute;tulo Mensaje:</b></td>
							<td style="font-weight: bold; width="25%" colspan="3"><?= $mensaje?></td>
						</tr>
						
						
						<tr  class="trInformacion">
							<td colspan="4"><b>Descripci&oacute;n:</b></td>
						</tr>
						<tr class="trDescripcion">
						  	<td colspan="4" class="trDescripcion"><?= $descripcion?></td>
						</tr>
						
						<tr class="trInformacion">
							<?
							if($caducidad==1)
							{
							?>
    						<td colspan="4">Este Mensaje Caduca el <?= mostrarFecha($fechacaducidad,false);?>   						  </td>
                          
							<?
							}
							else if($caducidad==2)
							{		
							?>
					 		<td colspan="4"><b>Este Mensaje Caduca al ser Leido</b></td>
							<?
							}
							?>
		 				</tr><br>
												<tr class="trInformacion">
							<td colspan="4" >
							<?
							if($_SESSION['idusuario']==$autor && ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5))
							{			
							?>
				
				<table class="tablaOpciones" align="center">
                  	<tr>
						<td width="25%"></td>
						<td width="25%"><div align="center"><a href="modificarCarteleraMensaje.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idCartelera=<?= $idCartelera?>&cbo_materia=<?=$_POST["cbo_materia"]?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Mensaje" width="16" height="16" border="0"></a></div></td>
						<td width="25%"><div align="center"><a href="javascript: eliminarMensaje(<?= $idCartelera?>)"  class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Mensaje" width="16" height="16" border="0"></a></div></td>
						<td width="25%"></td>
                    </tr>
                    <tr>
						<td></td>
                      	<td><div align="center"><a href="modificarCarteleraMensaje.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&idCartelera=<?= $idCartelera?>&cbo_materia=<?=$_POST["cbo_materia"]?>" class="link">Modificar</a></div></td>
                      	<td><div align="center"><a href="javascript: eliminarMensaje(<?= $idCartelera?>)"  class="link">Eliminar</a></div></td>
						<td></td>
                    </tr>
				</table>		
				<?
				}
				?>					
							
							
							</td>
						</tr>

	<?
		}
		?>
						
					</table>
		<?
		}
		else
		{
		?>
			<table class="tablaGeneral">
				<tr class="trAviso">
				<td>Actualmente no existen Mensajes.</td>
				</tr>
			</table>
		
		<?
		}#end if
	?>
			</td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
					
	</table>
	
</body>
</html>
<?
}
else
	redireccionar('../scripts/');
}
else
	redireccionar('../login/');
	
?>
