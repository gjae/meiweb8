<?
	include_once ('../menu1/Menu1.class.php');	
	include_once("../editor/fckeditor.php") ;
	include_once('../calendario/FrmCalendario.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	include_once ('../menu/Menu.class.php');	include_once ('../librerias/vistas.lib.php');
	
	
	if(comprobarSession())
	{
	
	$baseDatos= new BD();
	
	$editor=new FCKeditor('edt_descripcionCartelera' , '100%' , '200' , 'barraCartelera' , '' );
?>

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">	
	<script language="javascript">
	
		function enviarCancelar()
		{
			location.replace("../cartelera/index.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>");		
		}
	
		function chk_habilitar()
		{
			for(i=0;i<document.frm_cartelera.elements.length;i++)
			{
				if(document.frm_cartelera.elements[i].id=="grupo")
				document.frm_cartelera.elements[i].disabled=false;
			}	
		}
		
		function chk_deshabilitar(id)
		{
			var radioid=document.getElementById(id);
			var radioSelec=document.getElementById('radioS');
			radioid.checked=false;
			radioSelec.checked=true;

			for(i=0;i<document.frm_cartelera.elements.length;i++)
			{
				if(document.frm_cartelera.elements[i].id=="grupo")
				document.frm_cartelera.elements[i].checked=true;
			}
		}
		 
		function chk_click ()
		{
			var radioTodos=document.getElementById('radioT');
			var radioSelec=document.getElementById('radioS');	
			radioTodos.checked=false;
			radioSelec.checked=true;	
		}

		function enviar()
		{
			var contBandera=0;	
			for(i=0;i<document.frm_cartelera.elements.length;i++)
			{
				if(document.frm_cartelera.elements[i].checked==true && document.frm_cartelera.elements[i].id=="grupo")
				contBandera++;
			}
			
			if(contBandera == 0)
			{
				alert("Debe seleccionar por lo menos un grupo destino");
			}
			else if(document.frm_cartelera.txt_mensajeCartelera.value == false)
			{
				alert("Debe llenar completamente la información solicitada");
			}
			else
			{
				document.frm_cartelera.submit();
			}
		}
		 		 
		function activarFecha()
		{
			document.frm_cartelera.txt_fecha.disabled=false;  
		}
		
		function desactivarFecha()
		{
			document.frm_cartelera.txt_fecha.disabled=true;  
		}
	
	</script>
				<style type="text/css">
				<!--
				.Estilo1 {color: #FF0000}
				-->
				</style>
	    </head>
	<body>
<?
								$agnoInicial=date('Y');
								$mesInicial=date('n');
								$diaInicial=date('j');
								$mesInicial+=1;
								
	$calendario=new FrmCalendario('frm_cartelera','txt_fecha','formulario','false',$agnoInicial.'-'.$mesInicial.'-'.$diaInicial);
?>
	<table class="tablaPrincipal">
		<tr valign="top">
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacio">&nbsp;</td>
			  <td>
					<table class="tablaGeneral" >
					
					<form action="insertarCarteleraMensaje.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" name="frm_cartelera" method="post" >
						<tr class="trTitulo">
						  <td colspan="4"><img src="imagenes/cartelera.gif" width="16" height="16" align="texttop"> Agregar Mensaje </td>
					  </tr>
						<?
							if($_GET['error'] == '0x001')
							{
						?>
								<tr class="trAviso">
									<td colspan="4"><img src="imagenes/error.gif" width="17" height="18" align="texttop"><span class="Estilo1"> Error: Debe escribir la descripción del Mensaje</span></td>
								</tr>
						<?
							}
						?>
					  
						<tr class="trInformacion">
							<td width="26%"><b>T&iacute;tulo Mensaje: </b></td>
							<td colspan="3"><input name="txt_mensajeCartelera" type="text" size="50" value="<?=$_GET['titulo']?>"></td>
						</tr>
						<tr class="trInformacion">
						  <td colspan="4" valign="middle"><b>Descripci&oacute;n:</b></td>
					  </tr>
						<tr class="trInformacion">
							<td colspan="4" valign="middle"><? $editor->crearEditor();?></td>
						</tr>
						<tr class="trInformacion">
							<td colspan="4"><b>Opciones de Publicaci&oacute;n : </b></td>
						</tr>
						<tr class="trInformacion">
							<td width="26%"><input name="rad_caducidad" type="radio" onClick="javascript:activarFecha()" value="1">
							Ocultar Despues de la Fecha:
							</td>
							<td width="18%"><? 
															
								$calendario->CrearFrmCalendario(); 	
								?></td>
							<td width="32%"><input name="rad_caducidad" type="radio" value="2" onClick="javascript:desactivarFecha()">
							Ocultar Despues de ser Leido</td>
						    <td width="24%"><input name="rad_caducidad" type="radio" onClick="javascript:desactivarFecha()" value="3" checked> 
						      Siempre Visible </td>
						</tr>
						<tr class="trInformacion">
				<td colspan="4"><div align="center">
				  <table width="269" border="0">
                    <tr>
                      <td width="89"><div align="right">
                        <input name="btn_agregar" type="button" value="Guardar Mensaje" onClick="javascript:enviar()">
                      </div></td>
                      <td width="141"><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript:enviarCancelar()"></td>
                    </tr>
                  </table>
				  </div></td>
			</tr>
						<tr class="trInformacion">
						  <td colspan="4" valign="top"><table width="100%" bgcolor="#DDDDDD" class="tablaPrincipal">
                            <tr>
                              <td colspan="5"><b>Destino:</b></td>
                              <td width="260" colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                              <td width="20"><input name="rad_grupo" type="radio" value="general" id="radiog" onClick="javascript:chk_deshabilitar(this.id)"></td>
<?                        
			if ($_SESSION['idtipousuario']==5)
			{
?>				
                              <td width="208">Todos los Grupos Virtuales</td>
<?                        
			}
			else
			{
?>	
                              <td width="208">Todos los Grupos Presenciales</td>
<?                        
			}
?>
                              <td width="20"><input name="rad_grupo" type="radio" value="todos" onClick="javascript:chk_deshabilitar(this.id)"  id="radioT"></td>
                              <td width="168">Todos mis Grupos </td>
                              <td width="20"><input name="rad_grupo" type="radio" value="seleccionados" onClick="javascript:chk_habilitar()"  checked id="radioS"></td>
                              <td colspan="3">Grupos Seleccionados</td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td colspan="4"><table class="tablaPrincipal">
                                  <?
								$contChk=0;

								if ($_SESSION['idtipousuario']==5)
								{
									$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria 
											WHERE mei_virmateria.idvirmateria
												IN (
													SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo 
															WHERE mei_virgrupo.idvirgrupo
																IN (
																	SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
																			WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
																	)
													) GROUP BY (mei_virmateria.idvirmateria)"; 
								}
								else
								{
									$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia 
											WHERE mei_materia.idmateria
												IN (
													SELECT mei_grupo.idmateria FROM mei_grupo 
															WHERE mei_grupo.idgrupo
																IN (
																	SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																			WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
																	)
													) GROUP BY (mei_materia.idmateria)"; 
								}
								
								$materias=$baseDatos->ConsultarBD($sql);
								
								while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
								{
								?>
                                  <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3"><b><?= $materiaNombre?></b></td>
                                  </tr>
                                  <?
								if ($_SESSION['idtipousuario']==5)
								{
									$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo 
												WHERE mei_virgrupo.idvirgrupo 
													IN (
														SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
																WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
														) AND mei_virgrupo.idvirmateria =".$materiaCodigo;
								}
								else
								{
									$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo 
												WHERE mei_grupo.idgrupo 
													IN (
														SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
														) AND mei_grupo.idmateria =".$materiaCodigo;
								}
									
									$grupos=$baseDatos->ConsultarBD($sql);
									
									while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
									{
									?>
                                  <tr>
                                    <td width="26">&nbsp;</td>
                                    <td width="26">&nbsp;</td>
                                    <td width="33"><input id="grupo" name="<?= "chk_idgrupo".$contChk?>" type="checkbox" onClick="javascript:chk_click()" value="<?= $grupoCodigo?>"></td>
                                    <td width="280"><?= $grupoNombre?></td>
                                  </tr>
                                  <?
									$contChk++;
									}
								}  
								?>
                              </table></td>
                              <?
								$contChk=0;
								if ($_SESSION['idtipousuario']==5)
								{
									$sql="SELECT mei_virmateria.nombre, mei_virmateria.idvirmateria FROM mei_virmateria 
											WHERE mei_virmateria.idvirmateria
												IN (
													SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo 
															WHERE mei_virgrupo.idvirgrupo
																IN (
																	SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
																			WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
																	)
													) GROUP BY (mei_virmateria.idvirmateria)"; 
								}
								else
								{
									$sql="SELECT mei_materia.nombre, mei_materia.idmateria FROM mei_materia 
											WHERE mei_materia.idmateria
												IN (
													SELECT mei_grupo.idmateria FROM mei_grupo 
															WHERE mei_grupo.idgrupo
																IN (
																	SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																			WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
																	)
													) GROUP BY (mei_materia.idmateria)"; 
								}
								
								$materias=$baseDatos->ConsultarBD($sql);
								
								while(list($materiaNombre,$materiaCodigo)=mysql_fetch_array($materias))
								{
									if ($_SESSION['idtipousuario']==5)
									{
										$sql="SELECT mei_virgrupo.nombre , mei_virgrupo.idvirgrupo FROM mei_virgrupo 
												WHERE mei_virgrupo.idvirgrupo 
													IN (
														SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru 
																WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
														) AND mei_virgrupo.idvirmateria =".$materiaCodigo;
									}
									else
									{
										$sql="SELECT mei_grupo.nombre , mei_grupo.idgrupo FROM mei_grupo 
												WHERE mei_grupo.idgrupo 
													IN (
														SELECT mei_relusugru.idgrupo FROM mei_relusugru 
																WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
														) AND mei_grupo.idmateria =".$materiaCodigo;
									}
									
									$grupos=$baseDatos->ConsultarBD($sql);
									
									while(list($grupoNombre,$grupoCodigo)=mysql_fetch_array($grupos))
									{
									?>
                              <?
									$contChk++;
									}
								}  
								?>
                            </table></td>
					  </tr>
			<tr class="trInformacion">
				<td colspan="4"><div align="center">
				  <table width="269" border="0">
                    <tr>
                      <td width="25"><input name="hid_contGrupo" type="hidden" value="<?= $contChk?>"></td>
                      <td width="89"><div align="right">
                        <input name="btn_agregar1" type="button" value="Guardar Mensaje" onClick="javascript:enviar()">
                      </div></td>
                      <td width="141"><input name="btn_cancelar" type="button" id="btn_cancelar" value="Cancelar" onClick="javascript:enviarCancelar()"></td>
                    </tr>
                  </table>
				  </div></td>
			</tr>
	</form>	
			
		</table>

            </td>
            <td class="tablaEspacio">&nbsp;</td>
            <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
            <td class="tablaEspacioDerecho">&nbsp;</td>
		</tr>
	</table>
</body>
</html>
<?

}
else
	redireccionar('../login/');

?>
