<?
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/estandar.lib.php');
	
	
	class Cartelera
	{
		var $m_anchoMarq;
		var $m_altoMarq;
		var $m_anchoTablaInterna;
		var $m_longDescripcion;
		var $m_velNormal;
		var $m_velMax;
		
		function Cartelera()
		{
			$this->m_anchoMarq='100%';
			$this->m_altoMarq=recuperarVariableSistema('sistemaaltocartelera');
			$this->m_anchoTablaInterna='95%';
			$this->m_velNormal=recuperarVariableSistema('sistemavelcartelera');
			$this->m_velMax=recuperarVariableSistema('sistemavelmaxcartelera');
			$this->m_longDescripcion=recuperarVariableSistema('sistemalongmaxcartelera');
			
			$this->m_baseDatos= new BD();
			
			// Inicializacion de la cartelera
			$this->DesactivarMensajes();
			$this->InicializarBandejaCartelera();
		}
		
		function InicializarBandejaCartelera()
		{
			?>
				<script language="javascript">
				
					function marqSubir(velMax)
					{
						var marq=document.getElementById('bandeja');
						marq.direction='down';
						marq.scrollAmount=velMax;
						marq.start();
					}
					
					function marqBajar(velMax)
					{
						var marq=document.getElementById('bandeja');
						marq.direction='up';
						marq.scrollAmount=velMax;
						marq.start();
					}
					
					function marqMover(velNormal)
					{
						var marq=document.getElementById('bandeja');
						marq.direction='up';
						marq.scrollAmount=velNormal;
						marq.start();
					}
					
					function marqParar()
					{
						var marq=document.getElementById('bandeja');
						marq.stop();
					}
					
					function iniciarVentanaChat(idUsuario,idSalaChat)
					{
						if (typeof ventanaChat.document == 'object')
						{
							ventanaChat.close()
						}
						ventanaChat = window.open("../chat/ventanaChat.php?idSalaChat="+idSalaChat,"ventanaSalaChat"+idUsuario,"width=800,height=530,left=150,top=150,scrollbars=NO,menubars=NO,status=NO,statusbar=NO,resizable=NO,location=NO")
					}
					var ventanaChat=false
				
				</script>
			<?
		}
		
		function MostrarFecha($a_fecha)
		{
			list($agno,$mes,$dia)=explode('-',$a_fecha);
			
			switch ($mes)
			{
				case 1: 
					$textMes="Enero";
					break;
				case 2: 
					$textMes="Febrero";
					break;
				case 3: 
					$textMes="Marzo";
					break;
				case 4: 
					$textMes="Abril";
					break;
				case 5: 
					$textMes="Mayo";
					break;
				case 6: 
					$textMes="Junio";
					break;
				case 7: 
					$textMes="Julio";
					break;
				case 8: 
					$textMes="Agosto";
					break;
				case 9: 
					$textMes="Septiembre";
					break;
				case 10: 
					$textMes="Octubre";
					break;
				case 11: 
					$textMes="Noviembre";
					break;
				case 12: 
					$textMes="Diciembre";
					break;
			}
			
			return (int)$dia." de ".$textMes." de ".$agno;
		}
			
		function DesactivarMensajes()
		{
							
			$sql="UPDATE mei_cartelera SET estado = '1' WHERE caducidad=1 AND fechacaducidad < '".date('Y-n-j')."'";
			$this->m_baseDatos->ConsultarBD($sql);
					
			$sql="UPDATE mei_calendario SET estado = '1' WHERE fechamensaje < '".date('Y-n-j')."'";
			$this->m_baseDatos->ConsultarBD($sql);
		}
		
		function CrearListaMensajes()
		{
			
			
			$sqlAutor="SELECT mei_usuario.primernombre , mei_usuario.segundonombre, mei_usuario.primerapellido, mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario=";
			$sqlFechaActivacion="SELECT mei_evaprevio.fechaactivacion WHERE ";
			$contRegistro=0;
			# Crear lista de Mensajes Calendario
			
			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_calendario.idcalendario, mei_calendario.mensaje, mei_calendario.fechamensaje, mei_calendario.idusuario FROM mei_calendario
						WHERE mei_calendario.cartelera =1 AND mei_calendario.estado =0 AND mei_calendario.idcalendario
							IN (
								SELECT mei_relcalvirgru.idcalendario FROM mei_relcalvirgru
									WHERE mei_relcalvirgru.idvirgrupo
										IN (
											SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru
												WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
											)
								) ORDER BY( mei_calendario.fechamensaje )";
			}
			else
			{
				$sql="SELECT mei_calendario.idcalendario, mei_calendario.mensaje, mei_calendario.fechamensaje, mei_calendario.idusuario FROM mei_calendario
						WHERE mei_calendario.cartelera =1 AND mei_calendario.estado =0 AND mei_calendario.idcalendario
							IN (
								SELECT mei_relcalgru.idcalendario FROM mei_relcalgru
									WHERE mei_relcalgru.idgrupo
										IN (
											SELECT mei_relusugru.idgrupo FROM mei_relusugru
												WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
											)
								) ORDER BY( mei_calendario.fechamensaje )";
			}
			
			$consulta=$this->m_baseDatos->ConsultarBD($sql);
			
			while(list($idCalendario,$mensajeDescripcion,$fechaMensaje,$autor)=mysql_fetch_array($consulta))
			{
				$consultaAutor=$this->m_baseDatos->ConsultarBD($sqlAutor."'".$autor."'");
							
				list($primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($consultaAutor);	
				$autor=$primerNombre." ".$segundoNombre." ".$primerApellido." ".$segundoApellido;
				
				$mensajeDescripcion=stripslashes(base64_decode($mensajeDescripcion));
				list($titulo,$descripcion)=explode('[$$$]',$mensajeDescripcion);			
				$titulo.=', '.$this->MostrarFecha($fechaMensaje).'.';
				
				list($agnoCal,$mesCal,$diaCal)=explode('-',$fechaMensaje);
											
				if(date('Y-m-d') == $fechaMensaje)
				{
					$vineta="vinetaCalendarioTit.gif";
					$titulo='Hoy, '.$titulo;
				}
				else if((date('Y') == $agnoCal) && (date('m') == $mesCal) && ((int)date('d') == (int)$diaCal-1))
				{
					$vineta="vinetaCalendarioTit.gif";
					$titulo='Mañana, '.$titulo;
				}
				else
				{
					$vineta="vinetaCalendario.gif";
				}				
							
				if(strlen($descripcion) > $this->m_longDescripcion)
				{
					$descripcion=substr($descripcion,0,$this->m_longDescripcion).'...';
				}
			
				$enlace="../calendario/mostrarCalendarioMensaje.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idCalendario=".$idCalendario."&fechaCalendario=".$this->MostrarFecha($fechaMensaje,false);
				
				$listaMensajes[$contRegistro]=$titulo.'[$$$]'."&nbsp;".'[$$$]'.$autor.'[$$$]'.$enlace.'[$$$]'.$vineta;			
				$contRegistro++;
			}
			
			# Crear lista de Mensajes de chat
			
			$tiempoActivo=60;
	
			$sql="DELETE FROM mei_relsalusu WHERE TIME_TO_SEC(TIMEDIFF('".date('Y-n-d H:i:s')."', fechaingreso)) > ".$tiempoActivo;
			$this->m_baseDatos->ConsultarBD($sql);

			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_salachat.idsalachat, mei_salachat. nombresala , mei_salachat.descripcion , mei_salachat.idusuario FROM mei_salachat
						WHERE mei_salachat.cartelera =1 AND mei_salachat.estado =0 AND mei_salachat.idsalachat
							IN (
								SELECT mei_relsalvirgru.idsalachat FROM mei_relsalvirgru
									WHERE mei_relsalvirgru.idvirgrupo
										IN (
											SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru
												WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
											)
								) ORDER BY( mei_salachat.fechacreacion )";
			}
			else
			{
				$sql="SELECT mei_salachat.idsalachat, mei_salachat. nombresala , mei_salachat.descripcion , mei_salachat.idusuario FROM mei_salachat
						WHERE mei_salachat.cartelera =1 AND mei_salachat.estado =0 AND mei_salachat.idsalachat
							IN (
								SELECT mei_relsalgru.idsalachat FROM mei_relsalgru
									WHERE mei_relsalgru.idgrupo
										IN (
											SELECT mei_relusugru.idgrupo FROM mei_relusugru
												WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
											)
								) ORDER BY( mei_salachat.fechacreacion )";
			}
			
			$consulta=$this->m_baseDatos->ConsultarBD($sql);
			
			while(list($idSalaChat,$nombreSala,$descripcion,$autor)=mysql_fetch_array($consulta))
			{
				$consultaAutor=$this->m_baseDatos->ConsultarBD($sqlAutor."'".$autor."'");			
								
				list($primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($consultaAutor);	
				$autor=$primerNombre." ".$segundoNombre." ".$primerApellido." ".$segundoApellido;
				
				$sql="SELECT count( mei_relsalusu.idusuario )FROM mei_relsalusu WHERE mei_relsalusu.idsalachat =".$idSalaChat;

				list($numeroUsuariosSala)=mysql_fetch_array($this->m_baseDatos->ConsultarBD($sql));
				
				if($numeroUsuariosSala == 1)
				{
					$usuariosActivos=". ".$numeroUsuariosSala." usuario activo.";
				}
				else
				{
					$usuariosActivos=". ".$numeroUsuariosSala." usuarios activos.";
				}
				
				$tituloSala="Sala de Chat: ";
										
				if(strlen($descripcion) > $this->m_longDescripcion)
				{
					$descripcion=substr($descripcion,0,$this->m_longDescripcion).'...';
				}
				
				$vineta="vinetaChat.gif";
				
				$nombreSala=$tituloSala.$nombreSala.$usuariosActivos;
				$enlace="javascript:iniciarVentanaChat('".$_SESSION['idusuario']."','".$idSalaChat."')";
				
				$descripcion=stripslashes(base64_decode($descripcion));
				$listaMensajes[$contRegistro]=$nombreSala.'[$$$]'.$descripcion.'[$$$]'.$autor.'[$$$]'.$enlace.'[$$$]'.$vineta;			
				$contRegistro++;
			}
			
			# Crear lista de Mensajes de Cartelera Activos

			if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
			{
				$sql="SELECT mei_cartelera.idcartelera, mei_cartelera.mensaje, mei_cartelera.idusuario, mei_cartelera.fechaactivacion, mei_cartelera.caducidad
						FROM mei_cartelera WHERE
						( 
							mei_cartelera.caducidad =1 AND mei_cartelera.estado =0 AND mei_cartelera.idcartelera
								IN (
									SELECT mei_relcarvirgru.idcartelera
										FROM mei_relcarvirgru WHERE mei_relcarvirgru.idvirgrupo
											IN (
												SELECT mei_relusuvirgru.idvirgrupo
													FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario = '".$_SESSION['idusuario']."'
												)
									)
						)
					OR 
						( 
							mei_cartelera.caducidad =2 AND mei_cartelera.idcartelera
								IN (
									SELECT mei_relcarusu.idcartelera
										FROM mei_relcarusu WHERE mei_relcarusu.estado =0 AND mei_relcarusu.idusuario = '".$_SESSION['idusuario']."'
									)
						)
					OR
						(
							mei_cartelera.caducidad =3 AND mei_cartelera.estado =0 AND mei_cartelera.idcartelera
								IN (
									SELECT mei_relcarvirgru.idcartelera
										FROM mei_relcarvirgru WHERE mei_relcarvirgru.idvirgrupo
											IN (
												SELECT mei_relusuvirgru.idvirgrupo
													FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario = '".$_SESSION['idusuario']."'
												)
									)
						)
					";
			}
			else
			{
				$sql="SELECT mei_cartelera.idcartelera, mei_cartelera.mensaje, mei_cartelera.idusuario, mei_cartelera.fechaactivacion, mei_cartelera.caducidad
						FROM mei_cartelera WHERE
						( 
							mei_cartelera.caducidad =1 AND mei_cartelera.estado =0 AND mei_cartelera.idcartelera
								IN (
									SELECT mei_relcargru.idcartelera
										FROM mei_relcargru WHERE mei_relcargru.idgrupo
											IN (
												SELECT mei_relusugru.idgrupo
													FROM mei_relusugru WHERE mei_relusugru.idusuario = '".$_SESSION['idusuario']."'
												)
									)
						)
					OR 
						( 
							mei_cartelera.caducidad =2 AND mei_cartelera.idcartelera
								IN (
									SELECT mei_relcarusu.idcartelera
										FROM mei_relcarusu WHERE mei_relcarusu.estado =0 AND mei_relcarusu.idusuario = '".$_SESSION['idusuario']."'
									)
						)
					OR
						(
							mei_cartelera.caducidad =3 AND mei_cartelera.estado =0 AND mei_cartelera.idcartelera
								IN (
									SELECT mei_relcargru.idcartelera
										FROM mei_relcargru WHERE mei_relcargru.idgrupo
											IN (
												SELECT mei_relusugru.idgrupo
													FROM mei_relusugru WHERE mei_relusugru.idusuario = '".$_SESSION['idusuario']."'
												)
									)
						)
					";
			}
			
			$consulta=$this->m_baseDatos->ConsultarBD($sql);
			
			$consulta_quices_evaluaciones =$this->m_baseDatos->ConsultarBD($sql);
			/***********************************************************************************************************************/
			
			while(list($idCartelera , $mensajeDescripcion , $autor, $fechaactivacion ,$caducidad)=mysql_fetch_array($consulta))
			{
				$consultaAutor=$this->m_baseDatos->ConsultarBD($sqlAutor."'".$autor."'");
				$consultafechaactivacion=$this->m_baseDatos->ConsultarBD($sqlFechaActivacion."'".$fechaactivacion."'");
				
				list($primerNombre,$segundoNombre,$primerApellido,$segundoApellido)=mysql_fetch_array($consultaAutor);
		
				$autor=$primerNombre." ".$segundoNombre." ".$primerApellido." ".$segundoApellido;
						
				$mensajeDescripcion=stripslashes(base64_decode($mensajeDescripcion))."<br>".mostrarFechaTexto($fechaactivacion,1);
				list($titulo,$descripcion)=explode('[$$$]',$mensajeDescripcion);			
				
				if(strlen($descripcion) > $this->m_longDescripcion)
				{
					$descripcion=substr($descripcion,0,$this->m_longDescripcion).'...';
				}
							
				if($caducidad == 1)
				{
					$enlace="../cartelera/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idCartelera=".$idCartelera;
				}
				else if($caducidad == 2)
				{
					$enlace="../cartelera/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idCartelera=".$idCartelera."&visitado=1";
				}
				else if($caducidad == 3)
				{
					$enlace="../cartelera/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&idCartelera=".$idCartelera;
				}
				
				$vineta="vinetaCartelera.gif";
				
				$listaMensajes[$contRegistro]=$titulo.'[$$$]'.$descripcion.'[$$$]'.$autor.'[$$$]'.$enlace.'[$$$]'.$vineta;
				$contRegistro++;
			}
					
			return $listaMensajes;
		}
		
		function BandejaCartelera()
		{
			$listaMensajes=$this->CrearListaMensajes();
			
			if(!empty($listaMensajes))
			{		
				?>
		<table class="tablaCentral">
			<tr class="trTitulo">
				<td><img src="../imagenes/cartelera.gif" alt="Cartelera" align="texttop"> Cartelera</div></td>
			</tr>
			<tr class="trSubTitulo">
				<td class="trSubTitulo"><div align="right"><img src="../cartelera/imagenes/abajo.gif" alt="Bajar Mensajes" width="9" height="9" onMouseDown="marqSubir(<?= $this->m_velMax?>)" onMouseUp="marqParar()" onMouseOut="marqMover(<?= $this->m_velNormal?>)">
				<img src="../cartelera/imagenes/arriba.gif" alt="Subir Mensajes" width="9" height="9"  onMouseDown="marqBajar(<?= $this->m_velMax?>)" onMouseUp="marqParar()" onMouseOut="marqMover(<?= $this->m_velNormal?>)"></div></td>
			</tr>
		
			<tr class="tdTitulo">
				<td>
				<marquee  id="bandeja" onmouseover=this.stop() onmouseout=this.start() scrollAmount=<?= $this->m_velNormal?> direction="up" scrollDelay=5 height="<?= $this->m_altoMarq?>" width="<?= $this->m_anchoMarq?>" bgcolor="#FFFFFF">	
				<?
				foreach($listaMensajes as $datos)
				{
					list($titulo,$descripcion,$autor,$enlace,$vineta)=explode('[$$$]',$datos);
				?>
					<table width="<?= $this->m_anchoTablaInterna?>" align="center" class="tablaprincipal"> 
						<tr>
							<td width="11"><img src="../cartelera/imagenes/<?= $vineta?>"></td>
							<td><div align="justify"><b><a href="<?= $enlace?>" class="link"><?= trim($titulo)?></a></b></div></td>
						</tr>
						<tr>
						  <td width="11"><p align="right">&nbsp;</p></td>
							<td><div align="justify"><a href="<?= $enlace?>" class="link" title="ver este mensaje"><?= trim($descripcion)?></a></div></td>
						</tr>
						<tr>
							<td width="11">&nbsp;</td>
							<td><div align="right"><?= $autor?></div></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>  
					</table>
				<?
				}
				?>    
				  </marquee> 
				</td>
			</tr>
		</table><br>
		<?
		}
		else
		{
		?>
		<table class="tablaCentral">
			<tr class="trTitulo">
				<td><img src="../imagenes/cartelera.gif" width="16" height="16" align="texttop"> Cartelera</td>
			</tr>
			<tr class="trAviso">
				<td class="trAviso">Actualmente no existen Mensajes</td>
			</tr>
		</table><br>
				<?
		}
	}
} // Fin de la clase Cartelera
?>
