<?

	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');


if(comprobarSession())
	{

	$baseDatos= new BD();

			$p='1';

			if(!empty($_POST['hid_idCalendario']))
			{
				if($_POST['rad_grupo']=="todos")
				{
					$destino=0;
				}
				else if($_POST['rad_grupo']=="seleccionados")
				{
					$destino=1;
				}


				if(!comprobarEditor($_POST['edt_descripcionCalendario']))
				{
					redireccionar("modificarCalendarioMensaje.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&error=0x001&idCalendario=".$_POST['hid_idCalendario']);
				}

				if(empty($_POST['chk_cartelera']))
				{
					$cartelera=0;
				}
				else
				{
					$cartelera=1;
				}

				registrarBitacora(2,5,false);

				$sql="UPDATE `mei_calendario`
						SET `mensaje` = '".base64_encode($_POST['txt_mensajeCalendario']."[$$$]".eliminarEspacios($_POST['edt_descripcionCalendario']))."',
							`fechamensaje` = '".$_POST['txt_fecha']."',
							`estado` = '0',
							`idusuario` = '".$_SESSION['idusuario']."',
							`fechacreacion` = '".date('Y-n-j')."' ,
							`cartelera` = '".$cartelera."' ,
							`destino` = '".$destino."'
						WHERE `idcalendario` =".$_POST['hid_idCalendario'];

				$consulta=$baseDatos->ConsultarBD($sql);

				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					$sql="SELECT mei_relcalvirgru.idprevio, mei_relcalvirgru.idactividad FROM mei_relcalvirgru WHERE mei_relcalvirgru.idcalendario=".$_POST['hid_idCalendario'];
				else
					$sql="SELECT mei_relcalgru.idprevio, mei_relcalgru.idactividad FROM mei_relcalgru WHERE mei_relcalgru.idcalendario=".$_POST['hid_idCalendario'];
				$resultado=$baseDatos->ConsultarBD($sql);
              	list ($idPrevio, $idActividad) = mysql_fetch_row($resultado);

				if(empty($idPrevio) && empty($idActividad))
				{
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
						$sql="DELETE FROM `mei_relcalvirgru` WHERE `idcalendario` = ".$_POST['hid_idCalendario']." AND `idvirgrupo` IN
						(SELECT mei_relusuvirgru.idvirgrupo FROM `mei_relusuvirgru` WHERE mei_relusvirgru.idusuario=".$_SESSION['idusuario'].")";
					}
					else
					{
						$sql="DELETE FROM `mei_relcalgru` WHERE `idcalendario` = ".$_POST['hid_idCalendario']." AND `idgrupo` IN
						(SELECT mei_relusugru.idgrupo FROM `mei_relusugru` WHERE mei_relusugru.idusuario=".$_SESSION['idusuario'].")";
					}
					$consulta=$baseDatos->ConsultarBD($sql);

					for($i=0;$i<$_POST['hid_contGrupo'];$i++)
					{
						if(!empty($_POST['chk_idgrupo'.$i]))
						{
							if(empty($valores))
								$valores="('".$_POST['hid_idCalendario']."', '".$_POST['chk_idgrupo'.$i]."')";
							else
								$valores.=" , ('".$_POST['hid_idCalendario']."', '".$_POST['chk_idgrupo'.$i]."')";
						}

					}
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
						$sql="INSERT INTO `mei_relcalvirgru` ( `idcalendario` , `idvirgrupo` ) VALUES ".$valores;
					else
						$sql="INSERT INTO `mei_relcalgru` ( `idcalendario` , `idgrupo` ) VALUES ".$valores;
					$baseDatos->ConsultarBD($sql);
                }
			}
			redireccionar("../calendario/mostrarCalendarioMensaje.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&cbo_materia=".$_GET['cbo_materia']."&calendarioGeneral=".$p);

	}
else
	redireccionar('../login/');

?>

