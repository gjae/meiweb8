<?
	include_once ('../librerias/estandar.lib.php');
    include_once ('../menu/Menu.class.php');
	include_once('../baseDatos/BD.class.php');
	include_once ('../librerias/vistas.lib.php');
	include_once ('../menu1/Menu1.class.php');	

	if(comprobarSession())
	{
		if (!empty($_GET['cbo_materia']) && empty($_POST['cbo_materia'])) {
			$_POST['cbo_materia']=$_GET['cbo_materia'];	
		}

		$basedatos= new BD();

		$destino=0;

		if(!empty($_GET['calendarioGeneral']))
		{

			if(empty($_POST['cbo_materia'] ))
			{
				registrarBitacora(2,3,false);
				$destino=1;
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					$sql="SELECT mei_calendario.idcalendario, mei_calendario.idusuario, mei_calendario.fechamensaje, mei_calendario.mensaje
						FROM mei_calendario
							WHERE mei_calendario.idcalendario
								IN (
									SELECT mei_relcalvirgru.idcalendario
										FROM mei_relcalvirgru
											WHERE mei_relcalvirgru.idvirgrupo
												IN (
													SELECT mei_relusuvirgru.idvirgrupo
														FROM mei_relusuvirgru
															WHERE mei_relusuvirgru.idusuario = '".$_SESSION['idusuario']."'
													)
									) ORDER BY (mei_calendario.fechamensaje) desc ";
				}
				else
				{
					$sql="SELECT mei_calendario.idcalendario, mei_calendario.idusuario, mei_calendario.fechamensaje, mei_calendario.mensaje
						FROM mei_calendario
							WHERE mei_calendario.idcalendario
								IN (
									SELECT mei_relcalgru.idcalendario
										FROM mei_relcalgru
											WHERE mei_relcalgru.idgrupo
												IN (
													SELECT mei_relusugru.idgrupo
														FROM mei_relusugru
															WHERE mei_relusugru.idusuario = '".$_SESSION['idusuario']."'
													)
									) ORDER BY (mei_calendario.fechamensaje) desc ";
				}
			}
			else if(!empty($_POST['cbo_materia'] ))
			{
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					$sql="SELECT mei_calendario.idcalendario, mei_calendario.idusuario, mei_calendario.fechamensaje, mei_calendario.mensaje
						FROM mei_calendario
							WHERE mei_calendario.idcalendario
								IN (
									SELECT mei_relcalvirgru.idcalendario
										FROM mei_relcalvirgru
											WHERE mei_relcalvirgru.idvirgrupo
												IN (
													SELECT mei_relusuvirgru.idvirgrupo
														FROM mei_relusuvirgru, mei_virgrupo
															WHERE mei_relusuvirgru.idusuario = '".$_SESSION['idusuario']."' AND mei_relusuvirgru.idvirgrupo = mei_virgrupo.idvirgrupo
	 AND mei_virgrupo.idvirmateria='".$_POST['cbo_materia']."'
													)
									) ORDER BY (mei_calendario.fechamensaje) desc";
				}
				else
				{
					$sql="SELECT mei_calendario.idcalendario, mei_calendario.idusuario, mei_calendario.fechamensaje, mei_calendario.mensaje
						FROM mei_calendario
							WHERE mei_calendario.idcalendario
								IN (
									SELECT mei_relcalgru.idcalendario
										FROM mei_relcalgru
											WHERE mei_relcalgru.idgrupo
												IN (
													SELECT mei_relusugru.idgrupo
														FROM mei_relusugru, mei_grupo
															WHERE mei_relusugru.idusuario = '".$_SESSION['idusuario']."' AND mei_relusugru.idgrupo = mei_grupo.idgrupo
	 AND mei_grupo.idmateria='".$_POST['cbo_materia']."'
													)
									) ORDER BY (mei_calendario.fechamensaje) desc";
				}
			}
		}
		else if(!empty($_GET['idCalendario']) && !empty($_GET['fechaCalendario']))
		{
			$sql="SELECT mei_calendario.idcalendario , mei_calendario.idusuario, mei_calendario.fechamensaje , mei_calendario.mensaje FROM mei_calendario WHERE mei_calendario.idcalendario = '".$_GET['idCalendario']."'";
			registrarBitacora(2,4,false);
		}
		else if(!empty($_GET['fechaCalendario']) && empty($_GET['idCalendario']))
		{
			if ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5) {

				$sql="SELECT mei_calendario.idcalendario , mei_calendario.idusuario, mei_calendario.fechamensaje , mei_calendario.mensaje FROM mei_calendario WHERE mei_calendario.fechamensaje = '".$_GET['fechaCalendario']."'";
			registrarBitacora(2,4,false);
			}

			else {

				if ($_SESSION['idtipousuario']==3) 
				{
				

			$sql="SELECT mei_relusugru.idgrupo FROM mei_relusugru WHERE mei_relusugru.idusuario='".$_SESSION['idusuario']."'";
			$consulta1 = $basedatos->ConsultarBD($sql);
			list($id) = mysql_fetch_array($consulta1);


			$sql="SELECT mei_calendario.idcalendario , mei_calendario.idusuario, mei_calendario.fechamensaje , mei_calendario.mensaje FROM mei_calendario, mei_relcalgru WHERE mei_calendario.fechamensaje = '".$_GET['fechaCalendario']."' AND mei_relcalgru.idgrupo ='".$id."' AND mei_calendario.idcalendario=mei_relcalgru.idcalendario";
			registrarBitacora(2,4,false);
				}
				else
				{
					$sql="SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru WHERE mei_relusuvirgru.idusuario='".$_SESSION['idusuario']."'";
					$consulta1 = $basedatos->ConsultarBD($sql);
					list($id) = mysql_fetch_array($consulta1);

					$sql="SELECT mei_calendario.idcalendario , mei_calendario.idusuario, mei_calendario.fechamensaje , mei_calendario.mensaje FROM mei_calendario, mei_relcalvirgru WHERE mei_calendario.fechamensaje = '".$_GET['fechaCalendario']."' AND mei_relcalvirgru.idvirgrupo ='".$id."' AND mei_calendario.idcalendario=mei_relcalvirgru.idcalendario";
					registrarBitacora(2,4,false);
				}
		}
		}
		$consulta=$basedatos->ConsultarBD($sql);

		$contRegistro=0;
		while(list($idCalendario,$autor,$publicacion,$mensajeDescripcion)=mysql_fetch_array($consulta))
		{

			$sql="SELECT mei_usuario.primernombre, mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario = '".$autor."'";
			$datosAutor=$basedatos->ConsultarBD($sql);

			list($nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($datosAutor);

			$listaMensajes[$contRegistro]=$idCalendario.'[$$$]'.$autor.'[$$$]'.$publicacion.'[$$$]'.stripslashes(base64_decode($mensajeDescripcion)).'[$$$]'.$nombreAutor.'[$$$]'.$apellidoAutor;
			$contRegistro++;
		}

		if($contRegistro > 1)
		{
			$destinoEliminar="fechaCalendario=".$_GET['fechaCalendario'];
		}
?>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="../temas/<?=recuperarVariableSistema("sistematema"); ?>/estilo1024x768.css" rel="stylesheet" type="text/css">
		<script language="javascript">
			function eliminarMensaje(idCalendario,destinoEliminar)
			{
				if(confirm("¿Está seguro de eliminar esta Cita?"))
				{
					location.replace("eliminarCalendarioMensaje.php?idmateria=<?=$_GET['idmateria']?>&materia=<?=$_GET['materia']?>&idCalendario="+idCalendario+"&destino="+destinoEliminar+"&cbo_materia=<?=$_POST['cbo_materia']?>");
				}
			}
			function enviar()
			{
				document.frm_calendario.submit();
			}
		</script>

		<style type="text/css">
<!--
.Estilo1 {color: #000000}
-->
        </style>
		</head>
		<body>
		<table class="tablaPrincipal">
			<tr valign="top">
				<td class="tablaEspacio">&nbsp;</td>
				<td class="tablaIzquierdo"><? menu($_SESSION['idtipousuario']); ?></td>
				<td class="tablaEspacio">&nbsp;</td>
				<td>
					<table class="tablaGeneral" >
						<tr class="trTitulo">
							<td colspan="2"><img src="imagenes/calendario.gif" width="16" height="16" align="texttop"> Ver Cita</td>
							<td colspan="2" align="right"> <a href="../calendario/index.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"> Volver  </td>
			</tr>
						</tr>
					</table><br>
					<form name="frm_calendario"  method="post">
<?
		if( $_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)
		{
?>
					<table class="tablaGeneral" width="200" border="0">
						<tr class="trTitulo">
<?               
			if ($_SESSION['idtipousuario']==5)
			{
?>
							<td> Curso:  </td>
<?                        
			}
			else
			{
?>	
							<td> Materia:  </td>
<?                        
			}
?>
							<td><select name="cbo_materia"   onChange="javascript:enviar()">
<?                        
			if ($_SESSION['idtipousuario']==5)
			{
?>
								<option class='link' value="0">--Seleccione Curso--</option>
<?
			}
			else
			{
?>	
								<option class='link' value="0">--Seleccione Materia--</option>
<?
			}
			if ($_SESSION['idtipousuario']==5)
			{
				$sql132 = "SELECT DISTINCT(mei_virmateria.idvirmateria), mei_virmateria.nombre FROM mei_virmateria, mei_virgrupo,
						mei_relusuvirgru WHERE mei_virmateria.idvirmateria = mei_virgrupo.idvirmateria AND
						mei_virgrupo.idvirgrupo = mei_relusuvirgru.idvirgrupo AND
						mei_relusuvirgru.idusuario =".$_SESSION['idusuario'];
			}
			else
			{
				$sql132 = "SELECT DISTINCT(mei_materia.idmateria), mei_materia.nombre FROM mei_materia, mei_grupo,
					mei_relusugru WHERE mei_materia.idmateria = mei_grupo.idmateria AND
					mei_grupo.idgrupo = mei_relusugru.idgrupo AND
					mei_relusugru.idusuario =".$_SESSION['idusuario'];
			}
			$resultado22 = $basedatos->ConsultarBD($sql132);
			while ( list($idmateria2, $nommateria) = mysql_fetch_row($resultado22) )
			{
				unset($seleccion);
				if( $idmateria2== $_POST['cbo_materia'])
				{
					$seleccion="selected";
				}
?>
								<option class='link' value="<? print $idmateria2?>" <? print $seleccion ?>><? print $nommateria?></option>
<?
				mysql_free_result($resultado);
			}
?>
							</select></td>
						</tr>
					</table>
<?
		}
?>
					</form>
<?
		if(empty($listaMensajes))
		{
?>
					<table class="tablaGeneral" >
						<tr class="trAviso">
							<td>Actualmente no existen Citas</td>
						</tr>
					</table>
<?
		}
		else
		{
			foreach($listaMensajes as $datos)
			{
				list($idCalendario,$autor,$publicacion,$mensaje,$descripcion,$nombreAutor,$apellidoAutor)=explode("[$$$]",$datos);
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					$sql="SELECT count(mei_relcalvirgru.idvirgrupo) FROM mei_relcalvirgru WHERE mei_relcalvirgru.idcalendario=".$idCalendario."";
					$num = $basedatos->ConsultarBD($sql);
					list($cant) = mysql_fetch_row($num);

					$sql32="SELECT mei_relcalvirgru.idprevio, mei_relcalvirgru.idactividad, mei_virgrupo.nombre, mei_virmateria.nombre
					FROM mei_relcalvirgru, mei_virgrupo, mei_virmateria WHERE mei_relcalvirgru.idcalendario=".$idCalendario."  and mei_relcalvirgru.idvirgrupo=mei_virgrupo.idvirgrupo and mei_virgrupo.idvirmateria=mei_virmateria.idvirmateria";
				}
				else
				{
					$sql="SELECT count(mei_relcalgru.idgrupo) FROM mei_relcalgru WHERE mei_relcalgru.idcalendario=".$idCalendario."";
					$num = $basedatos->ConsultarBD($sql);
					list($cant) = mysql_fetch_row($num);
					
					

					$sql32="SELECT mei_relcalgru.idprevio, mei_relcalgru.idactividad, mei_grupo.nombre, mei_materia.nombre
					FROM mei_relcalgru, mei_grupo, mei_materia WHERE mei_relcalgru.idcalendario=".$idCalendario."  and mei_relcalgru.idgrupo=mei_grupo.idgrupo and mei_grupo.idmateria=mei_materia.idmateria";


				}
				$verificar = $basedatos->ConsultarBD($sql32);
				list ($idPrevio, $idActividad,$nombregrupo,$nombremateria) = mysql_fetch_row($verificar);
				////////////////////////////////////////////////////////////////////////////////////////////////
				if ($cant>1) {
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6) {
						
						$sql="SELECT mei_relcalvirgru.idvirgrupo FROM mei_relcalvirgru WHERE mei_relcalvirgru.idcalendario=".$idCalendario."";
					$num = $basedatos->ConsultarBD($sql);
					$i=0;
					$j=0;
					$cont=0;
					while(list($idgrupo)=mysql_fetch_array($num)){

						$sql1="SELECT mei_virgrupo.idvirmateria FROM mei_virgrupo WHERE mei_virgrupo.idvirgrupo='".$idgrupo."'";
						$idmat = $basedatos->ConsultarBD($sql1);
						list($idmateria)=mysql_fetch_row($idmat);
						$mater[$i]=$idmateria;
						if ($j>0) {
							if ($mater[$i]!=$mater[$i-1]) {
								$cont++;
							}
						}
						$i++;
						$j++;
						if ($cont>0) {
					$nombremateria='VARIAS';
					$nombregrupo='GRUPOS';
				    }
				    else
				    {
				    	$sql10="SELECT mei_virmateria.nombre FROM mei_virmateria WHERE mei_virmateria.idvirmateria='".$idmateria."' ";
				    	$mat = $basedatos->ConsultarBD($sql10);
				    	list($nombremateria)=mysql_fetch_row($mat);
				    	$nombregrupo='GRUPOS';

				    }
					}
					}
					else{
					$sql="SELECT mei_relcalgru.idgrupo FROM mei_relcalgru WHERE mei_relcalgru.idcalendario=".$idCalendario."";
					$num = $basedatos->ConsultarBD($sql);
					$i=0;
					$j=0;
					$cont=0;
					while(list($idgrupo)=mysql_fetch_array($num)){

						$sql1="SELECT mei_grupo.idmateria FROM mei_grupo WHERE mei_grupo.idgrupo='".$idgrupo."'";
						$idmat = $basedatos->ConsultarBD($sql1);
						list($idmateria)=mysql_fetch_row($idmat);
						$mater[$i]=$idmateria;
						if ($j>0) {
							if ($mater[$i]!=$mater[$i-1]) {
								$cont++;
							}
						}
						$i++;
						$j++;
					}
					if ($cont>0) {
					$nombremateria='VARIAS';
					$nombregrupo='GRUPOS';
				    }
				    else
				    {
				    	$sql10="SELECT mei_materia.nombre FROM mei_materia WHERE mei_materia.idmateria='".$idmateria."' ";
				    	$mat = $basedatos->ConsultarBD($sql10);
				    	list($nombremateria)=mysql_fetch_row($mat);
				    	$nombregrupo='GRUPOS';

				    }
						}
				}
				//////////////////////////////////////////////////////////////////////////////////////////
				if(!empty($idActividad))
				{
					$sql33="SELECT mei_actividad.fechaactivacion, mei_actividad.fechafinalizacion
						FROM mei_actividad WHERE mei_actividad.idactividad ='".$idActividad."' ORDER BY (mei_actividad.fechaactivacion) desc";
					$fecha = $basedatos->ConsultarBD($sql33);
					list ($fechaI, $fechaF) = mysql_fetch_row($fecha);
?>

					<table class="tablaGeneral" >
					<tr class="trInformacion">
			  		<td style="font-weight: bold;" width="10%"><img src="imagenes/calendario.gif" width="16" height="16" align="texttop">Calendario</td>
			  		<td width="25%"></td>
			  		<td width="25%"></td>
			  		<td width="25%"></td>
			  		</tr>
						<tr  class="trInformacion">
<?
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
?>
							<td width="10%"><b>Curso: </b></td>
<?
					}
					else
					{
?>
							<td width="10%"><b>Materia: </b></td>
<?
					}
?>
							<td width="25%"><?= $nombremateria ?></td>
							<td width="25%"><div align="right"><b>Grupo:</b></div></td>
							<td width="25%"><div align="right"><?=$nombregrupo;?></div><div align="right"></div></td>
						</tr>
						<?php

						$sql="SELECT mei_usuario.primernombre, mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario = '".$autor."'";
                			$datosAutor=$basedatos->ConsultarBD($sql);

			                list($nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($datosAutor);

						?>
						<tr  class="trInformacion">
							<td width="10%"><b>Autor: </b></td>
							<td width="25%"><?= $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2 ?></td>
							<td width="25%"><div align="right"><b>Fecha de Activaci&oacute;n:</b></div></td>
							<td width="25%"><div align="right"><?=mostrarFechaTexto($fechaI,1);?></div><div align="right"></div></td>
						</tr>
						<tr  class="trInformacion">
							<td width="10%"><b>T&iacute;tulo:</b></td>
							<td style="font-weight: bold; width="25%" width="25%"><?= $mensaje?></td>
							<td width="25%"><div align="right"><b>Fecha de Entrega:</b></div></td>
							<td width="25%" align="right"><?=mostrarFechaTexto($fechaF,1);?></td>
						</tr>
<?
				}
				elseif (!empty($idPrevio)) {

					$sql="SELECT mei_evaprevio.fechaactivacion, mei_evaprevio.fechafinalizacion
						FROM mei_evaprevio WHERE mei_evaprevio.idprevio ='".$idPrevio."' ORDER BY (mei_evaprevio.fechaactivacion) desc";
					$fecha1 = $basedatos->ConsultarBD($sql);
					list ($fechaI, $fechaF) = mysql_fetch_row($fecha1);
					
?>						
						<table class="tablaGeneral" >
<tr class="trInformacion">
			  		<td style="font-weight: bold;" width="10%"><img src="imagenes/calendario.gif" width="16" height="16" align="texttop">Calendario</td>
			  		<td width="25%"></td>
			  		<td width="25%"></td>
			  		<td width="25%"></td>
			  		</tr>
						<tr  class="trInformacion">
<?
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
?>				
							<td width="10%"><b>Curso: </b></td>
<?
					}
					else
					{
?>
							<td width="10%"><b>Materia: </b></td>
<?
					}
?>
							<td width="25%"><?= $nombremateria ?></td>
							<td width="25%"><div align="right"><b>Grupo:</b></div></td>
							<td width="25%"><div align="right"><?=$nombregrupo;?></div><div align="right"></div></td>
						</tr>
						<?php

						$sql="SELECT mei_usuario.primernombre, mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario = '".$autor."'";
                			$datosAutor=$basedatos->ConsultarBD($sql);

			                list($nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($datosAutor);
 
						
						?>
						<tr  class="trInformacion">
							<td width="10%"><b>Autor Cita: </b></td>
							<td width="25%"><?= $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2  ?></td>
							<td width="25%"><div align="right"><b>Fecha de Activaci&oacute;n:</b></div></td>
							<td width="25%"><div align="right"><?=mostrarFechaTexto($fechaI,1);?></div><div align="right"></div></td>
						</tr>
						<tr  class="trInformacion">
							<td><b>T&iacute;tulo Cita :</b></td>
							<td style="font-weight: bold; width="25%" colspan="3"><?= $mensaje?></td>
						</tr>
				
<?
				}

				else
				{
?>
					<table class="tablaGeneral" >
<tr class="trInformacion">
			  		<td style="font-weight: bold;" width="10%"><img src="imagenes/calendario.gif" width="16" height="16" align="texttop">Calendario</td>
			  		<td width="25%"></td>
			  		<td width="25%"></td>
			  		<td width="25%"></td>
			  		</tr>
						<tr  class="trInformacion">
<?
					if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
?>				
							<td width="10%"><b>Curso: </b></td>
<?
					}
					else
					{
?>
							<td width="10%"><b>Materia: </b></td>
<?
					}
?>
							<td width="25%"><?= $nombremateria ?></td>
							<td width="25%"><div align="right"><b>Grupo:</b></div></td>
							<td width="25%"><div align="right"><?=$nombregrupo;?></div><div align="right"></div></td>
						</tr>
						<?php

						$sql="SELECT mei_usuario.primernombre, mei_usuario.segundonombre,mei_usuario.primerapellido,mei_usuario.segundoapellido FROM mei_usuario WHERE mei_usuario.idusuario = '".$autor."'";
                			$datosAutor=$basedatos->ConsultarBD($sql);

			                list($nombre1,$nombre2,$apellido1,$apellido2)=mysql_fetch_array($datosAutor);
 
						?>
						<tr  class="trInformacion">
							<td width="10%"><b>Autor Cita: </b></td>
							<td width="25%"><?= $nombre1.' '. $nombre2.' '.$apellido1.' '.$apellido2  ?></td>
							<td width="25%"><div align="right"><b>Fecha de la Cita:</b></div></td>
							<td width="25%"><div align="right"><?= mostrarFecha($publicacion,false);?></div><div align="right"></div></td>
						</tr>
						<tr  class="trInformacion">
							<td><b>T&iacute;tulo Cita :</b></td>
							<td style="font-weight: bold; width="25%" colspan="3"><?= $mensaje?></td>
						</tr>
<?
				}
?>
						<tr  class="trInformacion">
							<td colspan="4"><b>Descripci&oacute;n:</b></td>
						</tr>
						<tr  class="trDescripcion">
							<td colspan="4" class="trDescripcion"> <?= $descripcion?></td>
						</tr>
						<tr  class="trInformacion">
							<td colspan="4">
<?
					if(($_SESSION['idusuario']==$autor && ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5)) || $_SESSION['idusuario']==$autor || ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5))
					{
					/*	if (empty($idPrevio) && empty($idActividad))
						{ Solo muestra modificar y eliminar para mensajes creados, 
						    para actividades o evaluaciones no deja eliminar
						*/
?>
								<table class="tablaOpciones" align="center">
									<tr>
										<td width="25%"></td>
										<td width="25%"><div align="center"><a href="modificarCalendarioMensaje.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&fechaCalendario=<?= $_GET['fechaCalendario']?>&idCalendario=<?= $idCalendario?>&cbo_materia=<?=$_POST['cbo_materia']?>" class="link"><img src="imagenes/modificar.gif" alt="Modificar Mensaje" width="16" height="16" border="0"></a></div></td>
										<td width="25%"><div align="center"><a href="javascript:eliminarMensaje('<?= $idCalendario?>','<?= $destino?>')" class="link"><img src="imagenes/eliminar.gif" alt="Eliminar Mensaje" width="16" height="16" border="0"></a></div></td>
										<td width="25%"></td>
									</tr>
									<tr>
										<td></td>
										<td><div align="center"><a href="modificarCalendarioMensaje.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>&fechaCalendario=<?= $_GET['fechaCalendario']?>&idCalendario=<?= $idCalendario?>&cbo_materia=<?=$_POST['cbo_materia']?>" class="link">Modificar</a></div></td>
										<td><div align="center"><a href="javascript:eliminarMensaje('<?= $idCalendario?>','<?= $destino?>')" class="link">Eliminar</a></div></td>
										<td></td>
									</tr>
								</table>
<?
					//	}
					}
?>
							</td>
						</tr>
					</table>
					<br>
<?
			}
		}#end if
?>
				</td>
				<td class="tablaEspacio">&nbsp;</td>
				<td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
				<td class="tablaEspacioDerecho">&nbsp;</td>
			</tr>
			</table>
</body>
</html>
	<?
	}
	else
		redireccionar('../login/');
	?>
