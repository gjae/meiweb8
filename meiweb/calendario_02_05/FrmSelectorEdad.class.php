<?
	include_once("../baseDatos/BD.class.php");
	
		
	class FrmSelectorEdad
	{
		var $m_agnoActual;
		var $m_agnoFinal;
		var $m_numeroAgnos;
		var $m_agnoNacimiento;
		var $m_mesNacimiento;
		var $m_diaNacimiento;
		var $m_baseDatos;
		
		function FrmSelectorEdad()
		{
			$this->m_baseDatos=new BD();
			
			$this->m_numeroAgnos=70;
			$this->m_agnoActual=date("Y")-1;
			$this->m_agnoInicial=$this->m_agnoActual-$this->m_numeroAgnos;
			
			$this->FechaNacimiento();
			$this->JavascripttEdad();
			
		}
		
		function FechaNacimiento()
		{
			if(!empty($_SESSION['idusuario']))
			{
				$sql="SELECT mei_usuario.fechanacimiento FROM mei_usuario WHERE mei_usuario.idusuario='".$_SESSION['idusuario']."'";
					
				$consultaFecha=$this->m_baseDatos->ConsultarBD($sql);	
						
				list($fechaNacimiento)=mysql_fetch_array($consultaFecha);
								
				list($this->m_agnoNacimiento,$this->m_mesNacimiento,$this->m_diaNacimiento)=explode("-",$fechaNacimiento);
				
				$this->m_agnoNacimiento=(int)$this->m_agnoNacimiento;
				$this->m_mesNacimiento=(int)$this->m_mesNacimiento;
				$this->m_diaNacimiento=(int)$this->m_diaNacimiento;			
				
			}
		}
		
		function JavascripttEdad()
		{
			print "<script language='javascript'>
			
			function comprobarFecha(agno,mes,dia)
			{
				if(agno < ".$this->m_agnoActual." && agno > 0 && dia >0 && mes >0 )
				{
					if ((mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) && dia <= 31)
					{
						return true;
					}
					else if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia <= 30)
					{
						return true;
					}
					else if (mes == 2)
					{
						if(agno%4==0 && dia <= 29)
						{
							return true;
						}
						else if (dia <= 28)
						{
							return true;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			
			</script>";
		}

		function SelectorEdar()
		{
			?><select name="cbo_dia">
				<option value="0">dia</option><?
				
			unset($seleccionado);
			
			for($dia=1; $dia <=31;$dia++)
			{
				if($dia == $this->m_diaNacimiento)
				{
					$seleccionado="selected";
				}
				else
				{
					$seleccionado="";
				}						
					?><option value="<?= $dia?>" <?= $seleccionado?>><?= $dia?></option><?
			}
			
			?></select>				
				<select name="cbo_mes">
					<option value="0">mes</option><?
					
			unset($seleccionado);
			
			for($mes=1; $mes <=12;$mes++)
			{
				if($mes == $this->m_mesNacimiento)
				{
					$seleccionado="selected";
				}
				else
				{
					$seleccionado="";
				}
				
				?><option value="<?= $mes?>" <?= $seleccionado?>><?= $this->CadenaMes($mes)?></option><?
			}
			?> </select>				
				<select name="cbo_agno">
					<option value="0">año</option>
			 <?
			 
			unset($seleccionado);
			 
			for($agno=$this->m_agnoActual; $agno > $this->m_agnoInicial;$agno--)
			{
				if($agno == $this->m_agnoNacimiento)
				{
					$seleccionado="selected";
				}
				else
				{
					$seleccionado="";
				}
					?><option value="<?= $agno?>" <?= $seleccionado?>><?= $agno?></option><?
			}
			?></select><?
		}
		
		function CadenaMes($a_mes)
		{
			switch($a_mes)
			{
				case 1:
				{
					$cadenaMes="Enero";
				}
				break;
				
				case 2:
				{
					$cadenaMes="Febrero";
				}
				break;
				
				case 3:
				{
					$cadenaMes="Marzo";
				}
				break;
				
				case 4:
				{
					$cadenaMes="Abril";
				}
				break;
				
				case 5:
				{
					$cadenaMes="Mayo";
				}
				break;
				
				case 6:
				{
					$cadenaMes="Junio";
				}
				break;
				
				case 7:
				{
					$cadenaMes="Julio";
				}
				break;
				
				case 8:
				{
					$cadenaMes="Agosto";
				}
				break;
				
				case 9:
				{
					$cadenaMes="Septiembre";
				}
				break;
				
				case 10:
				{
					$cadenaMes="Octubre";
				}
				break;
				
				case 11:
				{
					$cadenaMes="Noviembre";
				}
				break;
				
				case 12:
				{
					$cadenaMes="Diciembre";
				}
				break;
			}
			return $cadenaMes;
		}
	}
?>
