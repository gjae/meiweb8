<?
	include_once ('../librerias/vistas.lib.php');
	include_once('../baseDatos/BD.class.php');
		
	class Calendario
	{
	
		var $m_agnoActual;
		var $m_mesActual;
		var $m_diaActual;
		var $m_diaInicioMes;
		var $m_fechaSiguiente;
		var $m_fechaAnterior;
		var $m_longTexto;
		var $m_contadorMensajesMes;
		var $m_agnoInicial;
		var $m_numeroAgnosFuturo;
		var $m_paginaDestinoMostrar;
		var $m_paginaDestinoAgregar;
		var $m_baseDatos;
				
		
		function Calendario()
		{
			$this->m_paginaDestinoMostrar="../calendario/mostrarCalendarioMensaje.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia'];
			$this->m_paginaDestinoAgregar="../calendario/agregarCalendarioMensaje.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia'];
			
			$this->m_longTexto=recuperarVariableSistema('sistematextocalendario');
			$this->m_agnoInicial=recuperarVariableSistema('sistemaagnoinicial');
			$this->m_numeroAgnosFuturo=recuperarVariableSistema('sistemaagnosfuturo');
			
			$this->m_baseDatos=new BD();
			
			$this->iniciarCalendario();
				
		}
		
		function iniciarCalendario()
		{
			?>
			<script language="javascript" src="../calendario/ventanaTip.js"></script>
			<link rel="stylesheet" type="text/css" href="../calendario/estiloCalendario.css">
			<div id="ventana" ></div>
			<?
		}
		
		function CrearListaAgnos()
		{
											
			$agnoFinal=date('Y')+$this->m_numeroAgnosFuturo;
			
			?><select name="cbo_agnoCalendario" onChange="javascript: document.frm_calendario.submit();"><?
			
			for($agno=$this->m_agnoInicial;$agno<$agnoFinal;$agno++)
			{
				if($agno==$this->m_agnoActual)
				{
					?><option selected><?= $agno?></option><?
				}
				else
				{
					?><option><?= $agno?></option><?
				}
					
			}
			?></select><?
		}
		
		function CrearListaMeses()
		{
			$listaMeses=array(  1   => 'Enero',
			                    2 	=> 'Febrero',
		                    	3 	=> 'Marzo',
			               		4 	=> 'Abril',
								5 	=> 'Mayo',
								6 	=> 'Junio',
								7 	=> 'Julio',
								8 	=> 'Agosto',
								9 	=> 'Septiembre',
								10 	=> 'Octubre',
								11 	=> 'Noviembre',
								12 	=> 'Diciembre');
				
			?>
			
			<select name="cbo_mesCalendario" onChange="javascript: document.frm_calendario.submit();"><?
								
						
			for($mes=1;$mes<=12;$mes++)
			{
			    if($this->m_mesActual==$mes)				
				{
					?><option value ="<?= $mes?>" selected ><?= $listaMeses[$mes]?></option><?
				}
				else
				{
					?><option value ="<?= $mes ?>"><?= $listaMeses[$mes]?></option><?
				}
			}
			
			?></select><?		
		}
		
		
		function MostrarFecha($a_agnoActual,$a_mesActual)
		{
			switch ($a_mesActual)
			{
				case 1: 
					$textMes="Enero";
					break;
				case 2: 
					$textMes="Febrero";
					break;
				case 3: 
					$textMes="Marzo";
					break;
				case 4: 
					$textMes="Abril";
					break;
				case 5: 
					$textMes="Mayo";
					break;
				case 6: 
					$textMes="Junio";
					break;
				case 7: 
					$textMes="Julio";
					break;
				case 8: 
					$textMes="Agosto";
					break;
				case 9: 
					$textMes="Septiembre";
					break;
				case 10: 
					$textMes="Octubre";
					break;
				case 11: 
					$textMes="Noviembre";
					break;
				case 12: 
					$textMes="Diciembre";
					break;
			}
			return $textMes." de ".$a_agnoActual;
		}
		
		function FechaFlechas($a_fechaActual)
		{
			if(!empty($_POST['cbo_mesCalendario']) && !empty($_POST['cbo_agnoCalendario']))
			{
				$this->m_agnoActual=$_POST['cbo_agnoCalendario'];
				$this->m_mesActual=$_POST['cbo_mesCalendario'];				
			}
			else
			{
				list($this->m_agnoActual,$this->m_mesActual,$this->m_diaActual)=explode('-',$a_fechaActual);
			}
				
			$this->m_diaActual=date('j');
			
			if(empty($this->m_agnoActual) || empty($this->m_mesActual) || empty($this->m_diaActual))
			{
				list($this->m_agnoActual,$this->m_mesActual,$this->m_diaActual)=explode('-',date('Y-n-j'));
			}
			
			
						
			if($this->m_mesActual < 12)
			{
				$mesSiguiente=$this->m_mesActual+1;
				$agnoSiguiente=$this->m_agnoActual;
			}
			else
			{
				$mesSiguiente=1;
				$agnoSiguiente=$this->m_agnoActual+1;
			}
			
			if($this->m_mesActual > 1)
			{
				$mesAnterior=$this->m_mesActual-1;
				$agnoAnterior=$this->m_agnoActual;
			}
			else
			{
				$mesAnterior=12;
				$agnoAnterior=$this->m_agnoActual-1;
			}
			
			$this->m_fechaSiguiente=$agnoSiguiente.'-'.$mesSiguiente.'-'.$this->m_diaActual;
			$this->m_fechaAnterior=$agnoAnterior.'-'.$mesAnterior.'-'.$this->m_diaActual; 
				
		}
				
		
		function CrearListaDias()
		{
			$horaUnix=mktime (0,0,0,$this->m_mesActual,1,$this->m_agnoActual);
			$this->m_diaInicioMes=date("w",$horaUnix);
			$numeroDiasMes=date("t",$horaUnix);
			
			$listaDias=array();
			$listaDias=array_pad($listaDias,42,'&nbsp;');
			
				for($i=$this->m_diaInicioMes;$i<$numeroDiasMes+$this->m_diaInicioMes;$i++)
				{
					$listaDias[$i]=$i+1-$this->m_diaInicioMes.'[:::]';
				}
				
			return $listaDias;
		}
		
		
		function CrearListaMensajes()
		{
			$listaMensajes=$this->CrearListaDias();
			
			if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7 )
			{
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
				{
					$sql="SELECT DISTINCT(DATE_FORMAT( mei_calendario.fechamensaje, '%Y-%c-%e' )) FROM mei_calendario 
					WHERE DATE_FORMAT( mei_calendario.fechamensaje, '%Y-%c-%e' ) LIKE '".$this->m_agnoActual."-".$this->m_mesActual."-%' AND mei_calendario.idcalendario
						IN (				
							SELECT mei_relcalvirgru.idcalendario	FROM mei_relcalvirgru
								WHERE mei_relcalvirgru.idvirgrupo
								IN (					
									SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru
										WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
									)
							)";
				}
				else
				{
					$sql="SELECT DISTINCT(DATE_FORMAT( mei_calendario.fechamensaje, '%Y-%c-%e' )) FROM mei_calendario 
					WHERE DATE_FORMAT( mei_calendario.fechamensaje, '%Y-%c-%e' ) LIKE '".$this->m_agnoActual."-".$this->m_mesActual."-%' AND mei_calendario.idcalendario
						IN (				
							SELECT mei_relcalgru.idcalendario	FROM mei_relcalgru
								WHERE mei_relcalgru.idgrupo
								IN (					
									SELECT mei_relusugru.idgrupo FROM mei_relusugru
										WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
									)
							)";
				}
											
			}
			else
			{
				$sql="SELECT DISTINCT(DATE_FORMAT( mei_calendario.fechamensaje, '%Y-%c-%e' )) FROM mei_calendario 
					WHERE DATE_FORMAT( mei_calendario.fechamensaje, '%Y-%c-%e' ) LIKE '".$this->m_agnoActual."-".$this->m_mesActual."-%' AND mei_calendario.idcalendario
						IN (				
							SELECT mei_relcalgru.idcalendario	FROM mei_relcalgru
								WHERE mei_relcalgru.idgrupo
								IN (					
									SELECT mei_relusugru.idgrupo FROM mei_relusugru
										
									)
							)";
										
			}
			$consulta1=$this->m_baseDatos->ConsultarBD($sql);
			
			$contMensajes=0;
			while(list($fechamensaje)=mysql_fetch_array($consulta1))
			{	
																
				list($agnoBD,$mesBD,$diaBD)=explode('-',$fechamensaje);
				if($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==3 || $_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6 || $_SESSION['idtipousuario']==7)
				{
					if($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					{
						$sql="SELECT mei_calendario.mensaje, mei_calendario.idcalendario FROM mei_calendario
								WHERE DATE_FORMAT( mei_calendario.fechamensaje, '%Y-%c-%e' ) = '".$this->m_agnoActual."-".$this->m_mesActual."-".$diaBD."' 
								AND mei_calendario.idcalendario
									IN (				
										SELECT mei_relcalvirgru.idcalendario	FROM mei_relcalvirgru
											WHERE mei_relcalvirgru.idvirgrupo
											IN (					
												SELECT mei_relusuvirgru.idvirgrupo FROM mei_relusuvirgru
													WHERE mei_relusuvirgru.idusuario =".$_SESSION['idusuario']."
												)
										)";
					}
					else
					{
						$sql="SELECT mei_calendario.mensaje, mei_calendario.idcalendario FROM mei_calendario
								WHERE DATE_FORMAT( mei_calendario.fechamensaje, '%Y-%c-%e' ) = '".$this->m_agnoActual."-".$this->m_mesActual."-".$diaBD."' 
								AND mei_calendario.idcalendario
									IN (				
										SELECT mei_relcalgru.idcalendario	FROM mei_relcalgru
											WHERE mei_relcalgru.idgrupo
											IN (					
												SELECT mei_relusugru.idgrupo FROM mei_relusugru
													WHERE mei_relusugru.idusuario =".$_SESSION['idusuario']."
												)
										)";
					}
				}
				else
				{
					$sql="SELECT mei_calendario.mensaje, mei_calendario.idcalendario FROM mei_calendario
							WHERE DATE_FORMAT( mei_calendario.fechamensaje, '%Y-%c-%e' ) = '".$this->m_agnoActual."-".$this->m_mesActual."-".$diaBD."' AND mei_calendario.idcalendario
								IN (				
									SELECT mei_relcalgru.idcalendario	FROM mei_relcalgru
										WHERE mei_relcalgru.idgrupo
										IN (					
											SELECT mei_relusugru.idgrupo FROM mei_relusugru
											)
									)";
				}
				
				$consulta2=$this->m_baseDatos->ConsultarBD($sql);

				while(list($mensajeBD, $idCalendario)=mysql_fetch_array($consulta2))
				{
					$listaMensajes[$diaBD+$this->m_diaInicioMes-1].=$idCalendario."[+++]".$mensajeBD."[:-:]";
					$contMensajes++;
				}
				
			}
			$this->m_contadorMensajesMes=$contMensajes;
			
			return  $listaMensajes;
		}
		
				
		function BandejaCalendario($a_fechaActual)
		{
			$this->FechaFlechas($a_fechaActual);
			
?> 
				<table class="tablaDerecho" style="margin-top:270px;">
					<tr class="trTitulo">
						<td><div align="center"><a href="<?= $_SERVER['PHP_SELF']?>?fechaActual=<?= $this->m_fechaAnterior?>" target="_self"><img src="../calendario/imagenes/botonAnterior.gif" alt="Mes Anterior" width="18" height="18" border="0"></a></div></td>
						<td><?= $this->MostrarFecha($this->m_agnoActual,$this->m_mesActual)?> <div align="center"></div></td>
						<td><div align="center"><a href="<?= $_SERVER['PHP_SELF']?>?fechaActual=<?= $this->m_fechaSiguiente?>" target="_self"><img src="../calendario/imagenes/BotonSiguiente.gif" alt="Mes Siguiente" width="18" height="18" border="0"></a></div></td>
					</tr>
					<tr>
						<td colspan="3" class="trSubTitulo">
							<div align="center">
							  <table class="tablaPrincipal">
								  <tr class="trSubTitulo">
									  <td>D</td>
									  <td>L</td>
									  <td>M</td>
									  <td>M</td>
									  <td>J</td>
									  <td>V</td>
									  <td>S</td>
								  </tr>
<?
			
			$lista=$this->CrearListaMensajes();
			$contDias=0;
			
			while($contDias<42)
			{
								?><tr><?
								
				for($k=0;$k<7;$k++)
				{
					list($dia,$mensajes)=explode('[:::]',$lista[$contDias]);
					
					if($dia==$this->m_diaActual)
					{
						$class="tdCalendarioBandejaHoy";
					}
					else
					{
						$class="tdCalendarioBandeja";
					}
					
										
					if(!empty($mensajes))
					{
						$textoMensaje=explode('[:-:]',$mensajes);
						
						$tabla= "<table border=1 class=tablaventana>";
						$tabla.="<tr><td class=tablaventana><b>".$this->m_agnoActual."-".$this->m_mesActual."-".$dia."</b></td></tr>";
												
						foreach($textoMensaje as $idTextoDescripcion)
						{
							list($idCalendario,$textoDescripcion)=explode('[+++]',$idTextoDescripcion);
							
							if(!empty($textoDescripcion))
							{	
								$textoDescripcion=stripslashes(base64_decode($textoDescripcion));
								list($texto,$descripcion)=explode('[$$$]',$textoDescripcion);
								$tabla.= "<tr><td class=tdventana><li>".$texto."</li></td></tr>" ;
							}
						}
						
						$tabla.= "</table>";
						
										?><td class="<?= $class?>"><a  href="<?= $this->m_paginaDestinoMostrar?>&fechaCalendario=<?= $this->m_agnoActual."-".$this->m_mesActual."-".$dia ?>" onMouseOver="muestraTip('<?=$tabla?>')" onMouseOut="ocultaTip()"  class="linkCalendario"><?= $dia ?></a></td><?
					}
					else
					{
										?><td class="<?= $class?>"><?= $dia?></td><?
					}
					$contDias++;
				}
									?></tr><?
			}
			
			?>
							  </table>
					    </div></td>
					</tr>
				</table>
			<?
		}
		
		
		function VentanaCalendario($a_fechaActual)
		{
			$this->FechaFlechas($a_fechaActual);
			$lista=$this->CrearListaMensajes();		
			$contDias=0;
			
			if($this->m_contadorMensajesMes == 1)
			{
				$mensajeContador=$this->m_contadorMensajesMes." Mensaje";
			}
			else
			{
				$mensajeContador=$this->m_contadorMensajesMes." Mensajes";
			}
			
			?>

			
					<tr class="trTitulo">
						<td colspan="2" class="trTitulo"><img src="imagenes/calendario.gif" align="texttop"> Calendario</td>
						<? if (empty($_GET['idmateria'])) {?>
								<td colspan="2" align="right"> <a href="../scripts/homeUsuario.php" class="link"> Volver  </td>..
						<?	} else {?>
							<td colspan="2" align="right"> <a href="../scripts/homeMateria.php?idmateria=<?=$_GET["idmateria"]?>&materia=<?=$_GET["materia"]?>" class="link"> Volver  </td>
							<?}?>
						<td colspan="3" align="right" class="trTitulo">Hay <?= $mensajeContador?>&nbsp;&nbsp;</td>
					</tr>
					<tr>
                        <td colspan="7" class="trSubTitulo">
                            <table width="100%" class="tablaPrincipal">
                                <tr class="trSubTitulo">
                                    <td colspan="2"><?= $this->MostrarFecha($this->m_agnoActual,$this->m_mesActual)?></td>
                                    <td width="18" valign="middle"><a href="<?= $_SERVER['PHP_SELF']?>?fechaActual=<?= $this->m_fechaAnterior?>" target="_self"><img src="../calendario/imagenes/botonAnterior.gif" alt="Mes Anterior" width="18" height="18" border="0"></a></td>
            
                                <form action="<?= $PHP_SELF ?>" name="frm_calendario" target="_self" method="post">
                                    <td width="17"><? $this->CrearListaMeses();?></td>
                                    <td width="17"><? $this->CrearListaAgnos()?></td>
                                </form>
                        
                                    <td width="20" valign="middle"><a href="<?= $_SERVER['PHP_SELF']?>?fechaActual=<?= $this->m_fechaSiguiente?>" target="_self"><img src="../calendario/imagenes/BotonSiguiente.gif" alt="Mes Siguiente" width="18" height="18" border="0"></a></td>
                                </tr>
                            
                            </table>
                        
                        </td>
					</tr>
					<tr>
						<td colspan="7" class="trSubTitulo">
							<table border="0" class="tablaCalendario">
								<tr align="center">
									<td width="14%"><b>Domingo</b></td>
									<td width="14%"><b>Lunes</b></td>
									<td width="14%"><b>Martes</b></td>
									<td width="14%"><b>Miercoles</b></td>
									<td width="14%"><b>Jueves</b></td>
									<td width="14%"><b>Viernes</b></td>
									<td width="14%"><b>Sabado</b></td>
								</tr>
			<?
						
			while($contDias<42)
			{
?>
								<tr height="55">
<?
				for($i=0;$i<7;$i++)
				{
					list($dia,$mensajes)=explode('[:::]',$lista[$contDias]);
					
					
					
					if(($this->m_agnoActual.'-'.$this->m_mesActual.'-'.$this->m_diaActual)==date('Y-n-').$dia)					
					{ 
						
?>					
									<td valign="top" class="tdCalendarioHoy">
										<table width="100%" class="tablaPrincipal">
											<tr valign="top">
												<td valign="top"><?= 'Hoy'?></td>
<?
											
					}	
					else
					{
						
?>
												<td valign="top" class="tdCartelera">
													<table width="100%" class="tablaPrincipal">
														<tr valign="top">
															<td valign="top">&nbsp;</td>
<?
					}				
					
					if($dia != "&nbsp;" && ($_SESSION['idtipousuario']==2 || $_SESSION['idtipousuario']==5))
					{
?>
															<td align="right"><a href="<?= $this->m_paginaDestinoAgregar?>&fechaCalendario=<?= $this->m_agnoActual."-".$this->m_mesActual."-".$dia?>" title=" Agregar mensaje " class="link"><?= $dia?></a></td>
														</tr>
<?
					}
					else
					{
?>
															<td align="right" valign="top"><?= $dia?></a></td>
														</tr>
<?
					
					}
					
					$listaMensajes=explode('[:-:]',$mensajes);
					
						foreach($listaMensajes as $idTextoDescripcion)
						{
							if(!empty ($idTextoDescripcion))
							{
							
								list($idCalendario,$textoDescripcion)=explode('[+++]',$idTextoDescripcion);		
																
								$textoDescripcion=stripslashes(base64_decode($textoDescripcion));
								list($texto,$descripcion)=explode('[$$$]',$textoDescripcion);		
								
								if(strlen($texto) > $this->m_longTexto)
								{
									$textoMensaje=substr($texto,0,$this->m_longTexto).'...';
								}
								else
								{
									$textoMensaje=$texto;
								}
								
								?>
														<tr>
															<td valign="top"><a href="<?= $this->m_paginaDestinoMostrar?>&idCalendario=<?= $idCalendario;?>&fechaCalendario=<?= $this->m_agnoActual."-".$this->m_mesActual."-".$dia?>" onMouseOver="muestraTip('<table border=1 class=tablaventana><tr><td class=tablaventana><b><?= $this->m_agnoActual."-".$this->m_mesActual."-".$dia?></b></td></tr><tr><td class=tdventana><b>Titutlo Cita:</b></td></tr><tr><td class=tdventana><?= $texto?></td></tr></table>')" onMouseOut="ocultaTip()"  class="link"><li><?= $textoMensaje?></li></a></td>
														</tr>
								<?	
							}
						}
					?>
												  </table>
											  </td>
					<?
					$contDias++;
				}
				
											?></tr><?
			}
			
			?>
								</table>
							</td>
						</tr>
					</table>
						
						</td>
                        <td class="tablaEspacio">&nbsp;</td>
                        <td class="tablaDerecho"><? menu1($_SESSION['idtipousuario']); ?></td>
                        <td class="tablaEspacioDerecho">&nbsp;</td>
					</tr>
				</table>
			<?
			}
	}
	
	
?>