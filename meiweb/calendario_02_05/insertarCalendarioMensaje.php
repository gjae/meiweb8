<?

	include_once('../librerias/estandar.lib.php');
	include_once('../baseDatos/BD.class.php');
	
	if(comprobarSession())
	{
	$baseDatos=new BD();
	
		if(!empty($_POST['txt_mensajeCalendario']))		
		{	
			if($_POST['rad_grupo']=="todos")
			{
				$destino=0;			
			}
			else if($_POST['rad_grupo']=="seleccionados")
			{
				$destino=1;
			}
		
			if(!comprobarEditor($_POST['edt_descripcionCalendario']))
			{
					redireccionar("agregarCalendarioMensaje.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']."&error=0x001&titulo=".$_POST['txt_mensajeCalendario']);
			}
			else
			{
			
				if(empty($_POST['chk_cartelera']))
				{
					$cartelera=0;
				}
				else
				{
					$cartelera=1;
				}
				
				registrarBitacora(2,2,false);
				
				$sql="INSERT INTO mei_calendario ( idcalendario , mensaje , fechamensaje , estado , idusuario , fechacreacion , cartelera, destino)	
				VALUES ('', '".base64_encode($_POST['txt_mensajeCalendario']."[$$$]". eliminarEspacios($_POST['edt_descripcionCalendario']))."', '".$_POST['txt_fecha']."', '0', '".$_SESSION['idusuario']."', '".date('Y-n-j')."' , ".$cartelera.", ".$destino." )";
				$baseDatos->ConsultarBD($sql);					
				
				$valores='';				
							
				$idCalendario=$baseDatos->InsertIdBD();			
				
				for($i=0;$i<$_POST['hid_contGrupo'];$i++)
				{	
					if(!empty($_POST['chk_idgrupo'.$i]))
					{
						if(empty($valores))
							$valores="('".$idCalendario."', '".$_POST['chk_idgrupo'.$i]."')"; 
						else
							$valores.=" , ('".$idCalendario."', '".$_POST['chk_idgrupo'.$i]."')"; 	
					}					
					
				}
				if ($_SESSION['idtipousuario']==5 || $_SESSION['idtipousuario']==6)
					$sql="INSERT INTO `mei_relcalvirgru` ( `idcalendario` , `idvirgrupo` ) VALUES ".$valores;
				else
					$sql="INSERT INTO `mei_relcalgru` ( `idcalendario` , `idgrupo` ) VALUES ".$valores;
				$baseDatos->ConsultarBD($sql);
		
			}
		}
	redireccionar("../calendario/index.php?idmateria=".$_GET['idmateria']."&materia=".$_GET['materia']);
	
	}
else
	redireccionar('../login/');
?>